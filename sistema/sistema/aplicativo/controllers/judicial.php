<?php

class Judicial extends Controller {

	function Judicial()
	{
            parent::Controller();
            

	}
	
	function _remap($link)
	{
            
            $this->data['title']="Recupera :: Judicial";

            $this->inicore->addcss(array('reset','style','style_fixed','colors/blue')); // CSS HOME

            $this->load->helper("funcoes_helper");

            $this->load->model('judicial_model', 'md');

            $this->inicore->addjs(array('jquery-1.4.2.min.js','jquery-ui-1.8.custom.min.js','jquery.ui.selectmenu.js','jquery.flot.min.js','tiny_mce/jquery.tinymce.js','smooth.js','smooth.menu.js','smooth.table.js','smooth.form.js','smooth.dialog.js','smooth.autocomplete.js','plugin/jquery.maskedinput'));
//------------------------------------------------------------------------------
            include 'testar_conexao.php';
//------------------------------------------------------------------------------
            //IR PARA O RESPECTIVO FLUXO DA URL
            if($link == "listar") $this->_listar();
            if($link == "acordoext") $this->_acordoext();
			if($link == "importar") $this->_importar();
			if($link == "importarxls") $this->_importarxls();
			if($link == "lancarprocesso") $this->_lancarprocesso();
			if($link == "processodesconhecido") $this->_processodesconhecido();

	}

        function _listar()
        {   //ESCOLHE QUAL MENU FICARA SELECIONADO
            $this->session->set_userdata('menusel', '24');
            $this->inicore->loadSidebar();
//==============================================================================
            $this->data['filtro'] = ''; //usado para selecionar os recebimentos de um credor espec�fico ou todos os credores
            $this->data['aba'] = ''; // A ABA  QUE FICARA SELECIONADA NA VIEW
            if ($this->input->post('filtro', true) != '') {// SE O USUARIO ESCOLHER ALGUM FILTRO
                //DETERMINA A ABA QUE FICARA SELECIONADA NA VIEW
                $this->data['aba'] = $this->input->post('aba', true);
                // PASSA O FILTRO DIGITADO PELO USUARIO PAR AUMA VARIAVEL
                $filtro = $this->input->post('filtro');
                //MONTA O FILTRO QUE SERA PASSADO PARA O BANCO
                $filtro = "AND (I.ina_nome LIKE '%$filtro%' OR CR.cre_nome_fantasia LIKE '%$filtro%')";
            } else {
                $filtro = "";
            }
//==============================================================================
            //BUSCA NO BANCO AS COBRAN�AS JUDICIAIS QUE EST�O SOLICITANDO DOCUMENTOS
            $this->data['solicitandoDocs'] = $this->md->getJudSolDoc($filtro);
            // BUSCA NO BANCO AS COBRAN�AS JUDICIAIS QUE EST�O COM O STATUS AGUARDANDO
            $this->data['aguardando'] = $this->md->getJudAguardando($filtro);
            // BUSCA NO BANCO AS COBRAN�AS JUDICIAIS QUE ESTAO COMO STATUS AJUIZADO
            $this->data['ajuizado'] = $this->md->getJudAjuizado($filtro);
            // BUSCA NO BANCO AS COBRAN�AS JUDICIAIS QUE ESTAO COMO STATUS ANDAMENTO
            $this->data['andamento'] = $this->md->getJudAndamento($filtro);
            //BUSCA NO BANCO OS ACORDOS APARTIR DE COBRAN�AS JUDICIAIS
            $this->data['acordos'] = $this->md->getJudAcordos($filtro);
            //BUSCA NO BANCO OS ACORDOS ATRASADOS APARTIR DE COBRAN�AS JUDICIAIS
            $aux = $this->data['acordosAtr'] = $this->md->getAcAtrasados(date('Y-m-d'),$filtro);
            //BUSCA A PRIMEIRA PARCELA ATRASADA DE CADA ACORDO COM PARCELAS ATRASADAS
            foreach ($aux as $a){
                $this->data['parcelas'][$a->cob_cod] = $this->md->getParAtrasada(date('Y-m-d'),$a->cob_cod);
            }
            // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
            $this->inicore->loadview('judicial_listar',$this->data);
        }

		function _lancarprocesso() {
			
			$cob_cod = $this->input->post('cob_cod');
			$texto = $this->input->post('texto');
			
			//LAN�ANDO RO
			$ret = $this->db->insert('ros', array(
                'cobranca_cob_cod' => $cob_cod,
                'operacoes_ope_cod' => '32',
                'usuarios_usu_cod' => $this->session->userdata('usucod'),
                'ros_data' => date('Y-m-d'),
                'ros_hora' => date('H:i:s'),
                'ros_detalhe' => $texto
            ));
			if ($ret!=false) {
				//ATUALIZANDO TABELAS
				$ros_cod = $this->db->insert_id();
				$this->db->update('cobrancas', array('last_ros_cod' => $ros_cod, 'last_ros_update' => date('y-m-d H:i:s')), array('cob_cod'=>$cob_cod));
			}
			echo json_encode(array('cob_cod'=>$cob_cod,'status'=>$ret));
			die();
		}
		
		function _processodesconhecido() {
			
			$cob_cod = $this->input->post('cob_cod');
			$texto = $this->input->post('texto');
			$processo_num = $this->input->post('processo_num');
			$processo_ano = $this->input->post('processo_ano');
			$processo_forum = $this->input->post('processo_forum');
			
			//ATUALIZANDO A COBRAN�A
			$this->db->update('cobrancas', array(
				'processo_num'=>$processo_num,
				'processo_ano'=>$processo_ano,
				'processo_forum'=>$processo_forum
			), array('cob_cod'=>$cob_cod));			
			
			//LAN�ANDO RO
			$ret = $this->db->insert('ros', array(
                'cobranca_cob_cod' => $cob_cod,
                'operacoes_ope_cod' => '32',
                'usuarios_usu_cod' => $this->session->userdata('usucod'),
                'ros_data' => date('Y-m-d'),
                'ros_hora' => date('H:i:s'),
                'ros_detalhe' => $texto
            ));
			if ($ret!=false) {
				//ATUALIZANDO LAST RO
				$ros_cod = $this->db->insert_id();
				$this->db->update('cobrancas', array('last_ros_cod' => $ros_cod, 'last_ros_update' => date('y-m-d H:i:s')), array('cob_cod'=>$cob_cod));
			}
			echo json_encode(array('cob_cod'=>$cob_cod,'status'=>$ret));
			die();
		}

        function _acordoext()
        {
            //DETERMINA O MENU QUE FICARA SELECIONADO
            $this->session->set_userdata('menusel', '24');
            $this->inicore->loadSidebar();
            $this->data['aba'] = '';
            if ($this->input->post('filtro',true)){//SE O USUARIO USOU ALGUM FILTRO
                // PASSA O FILTRO PARA UMA VARIAVEL AUXILIAR
                $aux = $this->input->post('filtro',true);
                //MONTA O FILTRO QUE SERA PASSADO PARA O BANCO DE DADOS
                $filtro = "AND (CR.cre_nome_fantasia LIKE '%$aux%' OR I.ina_nome LIKE '%$aux%')";
                // PASSA PARA A VIEW QUAL A ABA Q DEVERA FICAR SELECIONADA
                $this->data['aba'] = $this->input->post('aba');
            }else{
                $filtro='';
            }
            // BUSCA OS ACORDOS EXTINTOS POR PAGAMENTO INTEGRAL
            $this->data['acordosPagInt'] = $this->md->getAcPagInt($filtro);
            // BUSCA OS ACORDOS EXTINTOS POR SOLICITA��O DO CREDOR
            $this->data['acordosSolCred'] = $this->md->getAcSolCred($filtro);
            // BUSCA OS ACORDOS EXTINTOS POR OUTROS MOTIVOS
            $this->data['acordosOutros'] = $this->md->getAcOutros($filtro);
            // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
            $this->inicore->loadview('judicial_acextinto',$this->data);
        }

        function _importar()
        {
	        $this->session->set_userdata('menusel', '24'); //DETERMINA O MENU QUE FICARA ABERTO
			$this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue'));
			//$this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput','jquery.accordion.js'));
	        $this->inicore->loadSidebar(); //barra lateral
	        $this->inicore->loadview('judicial_importar', $this->data);
        }

        function _importarxls()
        {
        	
			$linhaProc='';
			$linhaExc='';
	    	
			try {
				
		        // Pasta onde o arquivo vai ser salvo
		        $_UP['pasta'] = APPPATH . 'upload/';
		        // Tamanho maximo do arquivo (em Bytes)
		        $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
		        // Array com as extensoes permitidas
		        $_UP['extensoes'] = array('xls','xlsx');
		        $_UP['renomeia'] = true;
				
		        // Array com os tipos de erros de upload do PHP
		        $_UP['erros'][0] = 'N�o houve erro';
		        $_UP['erros'][1] = 'O arquivo no upload � maior do que o limite do PHP';
		        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
		        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
		        $_UP['erros'][4] = 'N�o foi feito o upload do arquivo';
				
		        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
		        if ($_FILES['arquivo']['error'] != 0) {
		        	throw new Exception("N�o foi poss�vel fazer o upload, erro:<br />" . $_UP['erros'][$_FILES['arquivo']['error']], 1);
		        }
		        // Caso script chegue a esse ponto, nao houve erro com o upload e o PHP pode continuar
		        // Faz a verificacao da extensao do arquivo
		        $extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
		
		        if (array_search($extensao, $_UP['extensoes']) === false) {
		        	throw new Exception("Por favor, envie arquivos com as seguintes extens�es: .XLS (Microsoft Excel)", 1);
		        }
		        // Faz a verificacaoo do tamanho do arquivo
		        else if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
		        	throw new Exception("O arquivo enviado � muito grande, envie arquivos de at� 2Mb.", 1);
		        }
		        // O arquivo passou em todas as verifica��es, hora de tentar mov�-lo para a pasta
		        else {
		        	
		            $_FILES['arquivo']['name'] = url_amigavel($_FILES['arquivo']['name']); //remove caracteres especiais
		            // Primeiro verifica se deve trocar o nome do arquivo
		
		            if ($_UP['renomeia'] == true) {
		                // Cria um nome baseado no UNIX TIMESTAMP atual
		                $nome_final = time() . $_FILES['arquivo']['name'];
		            } else {
		                // Mant�m o nome original do arquivo
		                $nome_final = $_FILES['arquivo']['name'];
		            }
		
		            // Depois verifica se � poss�vel mover o arquivo para a pasta escolhida
		            $destFile = $_UP['pasta'] . $nome_final;
		            if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $destFile)) {
		                // Upload efetuado com sucesso
		                chmod($destFile, 0777); //SETA PERMISS�ES NO ARQUIVO
		                //exibe uma mensagem e um link para o arquivo
		            } else {
		                // Nao foi possivel fazer o upload, provavelmente a pasta est� incorreta
		                throw new Exception("N�o foi poss�vel enviar o arquivo, erro interno de permiss�o do servidor, por favor entre em contato com o administrador do sistema.", 1);
		            }
					
				}
	
		        if (!isset($destFile) && empty($destFile) && !is_readable($destFile) ) {
		        	throw new Exception("Desculpe, ocorreu um erro ao acessar o arquivo ou voc� n�o tem permiss�o para ler esse arquivo. Consulte o Administrador do sistema. ", 1);
		        }
				
				//CARREGANDO PHPEXCEL
				
				$this->load->library('Excel_reader2');
				$this->excel_reader2->read($destFile);
				$worksheet = $this->excel_reader2->sheets[0];
				
				//echo '<pre>'; var_dump($worksheet); echo '</pre>';
				
				if (empty($worksheet['cells'])) {
					throw new Exception('Ocorreu um erro ao efetuar a leitura da arquivo. Por favor verifique se ele � um arquivo no FORMATO Microsoft Excel 97/2000/XP/2003 (XLS)', 1);	
				}
				
				foreach($worksheet['cells'] as $l=>$linha) {
					if ($l==1) {
						$linhaExc[] = $linha;
						continue; //TITULO
					}
					//echo $linha[2]. ' '. $linha[4] . '<hr>';
					
					$procFormats = array(
						'/(\d{7})[\-|\.](\d{2})\.(\d{4})\.(\d{1})\.(\d{2})\.(\d{4})/', //N�MERO PADR�O CNJ
						//'/(\d{3})\.(\d{2})\.(\d{4})\.(\d{6})\-(\d{1})/', //1� Inst�ncia: SISTEMA PRODESP
						//'/(\d{3})\.(\d{2})\.(\d{6})\-(\d{1})/', //2� INSTANCIA: SISTEMA SAJ
						//'/(\d{3})\.(\d{2})\.(\d{6})\-(\d{1})\/(\d{5})/' //2� INSTANCIA: SISTEMA SAJ - SUBPROCESSO
					);
					$matches='';
					foreach($procFormats as $pattern) {
						if (!empty($matches)) continue;
						preg_match($pattern, $linha[4], $matches);
						//echo '<pre>'; var_dump($matches);						
					}
					if (!empty($matches)) {
						$processo_num = $matches[0];
						
						$cobrow = $this->md->getCobPorProcesso($processo_num);
						
						//PROCURAR PROCESSO NO BANCO DE DADOS
						if (!empty($cobrow)) {
							if (!isset($linha[7])) $linha[7]='';
							$linha[8] = $processo_num;
							$tmpLinha = array_combine(array('jornal','data','origem','intimacao','arq','pub','comentario','processo'),array_values($linha));							
							$linhaProc[] = array(
								'cobranca'=>$cobrow->cob_cod,
								'processo'=>$processo_num,
								'inadimplente'=>$cobrow->ina_nome,
								'credor'=>$cobrow->cre_nome_fantasia,
								'data'=>$tmpLinha['data'],
								'intimacao'=> str_replace('&','E',removeCE($tmpLinha['intimacao'])),
								'jornal'=>$tmpLinha['jornal'],
								'origem'=>$tmpLinha['origem'],
								'arqpub'=>$tmpLinha['arq'].'/'.$tmpLinha['pub']
								
							);
						} else {
							if (!isset($linha[7])) $linha[7]='';
							$linhaExc[] = $linha;	
						}
					} else {
						if (!isset($linha[7])) $linha[7]='';
						$linhaExc[] = $linha;
					}

				} //ENDFOREACH
				
				/*
				echo '<pre>'; var_dump($linhaProc); echo '</pre>';
				echo '********************';
				echo '<pre>'; var_dump($linhaExc); echo '</pre>';
				 */
				
			} catch(Exception $e) {
				$this->data['mensagem'] = $e->getMessage();
	            $this->inicore->setMensagem('notice', 'Ocorreu um erro: '.$e->getMessage(), true);
	            redirect(base_url() . 'judicial/importar');
			}	
			
			$this->data['registros'] = $linhaProc;
			$this->data['desconhecidos'] = $linhaExc;
	        //$this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput'));
	        $this->session->set_userdata('menusel', '24'); //DETERMINA O MENU QUE FICARA ABERTO
			$this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue'));
			//$this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput','jquery.accordion.js'));
	        $this->inicore->loadSidebar(); //barra lateral
	        $this->inicore->loadview('judicial_importarxls', $this->data);
	    }


}
