<?php

class Recebimento extends Controller {

    function Pesquisar() {
        parent::Controller();
    }

    function _remap($link) {

        $this->data['title'] = "Recupera :: Recebimentos";

        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME

        $this->load->helper("funcoes_helper");

        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js','plugin/jquery.maskedinput'));

        $this->load->model('recebimentos_model', 'md');
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
//IR PARA O RESPECTIVO FLUXO DA URL
        if ($link == "meus")
            $this->_meus();
        if ($link == "todos")
            $this->_todos();
        if ($link == "previsoes")
            $this->_previsoes();
        if ($link == "filtrar")
            $this->_filtrar();
    }

    function _meus() {//meus recebimentos
        $this->session->set_userdata('menusel', '17');
        $codUsuarioSessao = $this->session->userdata('usucod'); //cod usu�rio da sess�o
        $dataDeHoje = date("Y-m-d", time());
        $primeiroDiaDaSemana = getPDS(); //m�todo do helper
        $ultimoDiaDaSemana = getUDS(); //m�todo do helper
        $mesAtual = date("m", time()); //vari�vel usada para pegar todos os recebimentos do m�s
        $anoAtual = date("Y"); //vari�vel usada para pegar todos os recebimentos do m�s e do pr�ximo m�s
        $proximoMes = $mesAtual - 1; // vari�vel usada para pegar os recebimentos do pr�ximo m�s
        $this->data['filtroCredor'] = ''; //usado para selecionar os recebimentos de um credor espec�fico ou todos os credores
        $this->data['aba'] = '';
        if ($this->input->post('filtroCredor', true) == 'todos') {
            $filtroCredor = "WHERE R.usuarios_usu_cod = $codUsuarioSessao";
            $this->data['aba'] = $this->input->post('aba', true);
        } else if ($this->input->post('filtroCredor', true) != '') {
            $this->data['aba'] = $this->input->post('aba', true);
            $filtroCredor = $this->input->post('filtroCredor');
            $filtroCredor = "WHERE R.usuarios_usu_cod=$codUsuarioSessao AND R.credor_cre_cod=" . $this->input->post('filtroCredor');
            $this->data['filtroCredor'] = $this->input->post('filtroCredor');
        } else {

            $filtroCredor = "WHERE R.usuarios_usu_cod = $codUsuarioSessao";
        }
        $this->data['todosCredores'] = $this->md->getCredores(); //usado para colocar os credores no combo box credores(campo selecionar credor na view).
        $this->data['meusRecebimentosHoje'] = $this->md->getMeusRecebimentosHoje($filtroCredor, $dataDeHoje);
        $this->data['meusRecebimentosSemana'] = $this->md->getMeusRecebimentosSemana($filtroCredor, $primeiroDiaDaSemana, $ultimoDiaDaSemana);
        $this->data['meusRecebimentosMes'] = $this->md->getMeusRecebimentosMes($filtroCredor, $mesAtual, $anoAtual);
        if($proximoMes == 0){
            $proximoMes = 12;
            $anoAtual = $anoAtual - 1;
        }
        $this->data['meusRecebimentosProximoMes'] = $this->md->getMeusRecebimentosProximoMes($filtroCredor, $proximoMes, $anoAtual);

        $this->inicore->loadSidebar();//menu (lateral esquerda).
        $this->inicore->loadview('rec_listar', $this->data);
    }

    function _todos() {//recebimentos de todos os usu�rios
        $this->session->set_userdata('menusel', '17');
        $dataDeHoje = date("Y-m-d", time());
        $primeiroDiaDaSemana = getPDS(); //m�todo do helper
        $ultimoDiaDaSemana = getUDS(); //m�todo do helper
        $mesAtual = date("m", time()); //vari�vel usada para pegar todos os recebimenos do m�s
        $anoAtual = date("Y"); //vari�vel usada para pegar todos os recebimentos do m�s e do pr�ximo m�s
        $proximoMes = $mesAtual - 1;//vari�vel usada para pegar todos os recebimentos do pr�ximo m�s
        $this->data['filtroCredor'] = '';//usado para selecionar os recebimentos de um credor espec�fico ou todos os credores

        /*----------------------------------------------------------------------
         * Testa se o campo 'filtroCredor'(selecionar credor "na view") est� vazio ou como 'todos'(--Todos-- "na view"),
         * se for vazio ou 'todos' seleciona tudo e popula as tabelas das abas(hoje, semana, m�s, pr�ximo m�s "na view")
         *   sen�o pega o c�digo do credor campo 'filtroCredor'(selecionar credor "na view") seleciona os recebimentos
         *     do credor especificado e popula as tabelas das abas(hoje, semana, m�s, pr�ximo m�s "na view")
         */
        if ($this->input->post('filtroCredor', true) == '' || $this->input->post('filtroCredor', true) == 'todos') {            
            $filtroCredor = "WHERE R.reb_data";
        } else {
            $codCredor = $this->input->post('filtroCredor');
            $filtroCredor = "WHERE C.cre_cod = $codCredor AND R.reb_data";
            $this->data['filtroCredor'] = $this->input->post('filtroCredor');
        }
        /*----------------------------------------------------------------------*/

        $this->data['todosCredores'] = $this->md->getCredores();
        $this->data['todosRecebimentosHoje'] = $this->md->getTodosRecebimentosHoje($filtroCredor, $dataDeHoje);
        $this->data['todosRecebimentosSemana'] = $this->md->getTodosRecebimentosSemana($filtroCredor, $primeiroDiaDaSemana, $ultimoDiaDaSemana);
        $this->data['todosRecebimentosMes'] = $this->md->getTodosRecebimentosMes($filtroCredor, $mesAtual, $anoAtual);
        if($proximoMes == 0){
            $proximoMes = 12;
            $anoAtual = $anoAtual - 1;
        }
        $this->data['todosRecebimentosProxMes'] = $this->md->getTodosRecebimentosProxMes($filtroCredor, $proximoMes, $anoAtual);

        $this->inicore->loadSidebar();//menu (lateral esquerda).
        $this->inicore->loadview('rec_listar_todos', $this->data);
    }

    function _previsoes() {//todas previs�es(n�o � separada por usu�rio).
        $this->session->set_userdata('menusel', '17');
        $dataDeHoje = date("Y-m-d", time());
        $primeiroDiaDaSemana = getPDS(); //m�todo do helper.
        $ultimoDiaDaSemana = getUDS(); //m�todo do helper.
        $mesAtual = date("m", time()); //vari�vel usada para pegar todas as previs�es do m�s.
        $anoAtual = date("Y"); //vari�vel usada para pegar todas as previs�es do m�s e do pr�ximo m�s.
        if($mesAtual == 12){
            $proximoMes = 1;
            $anoAtual = $anoAtual +1;
        }else{
            $proximoMes = $mesAtual + 1;
        }
        
        $this->data['filtroCredor'] = '';

        /*--------------------------------------
         * se o campo 'filtroCredor'(selecionar credor "na view") chegar vazio ou 'todos' (--Todos-- "na view")
         *     o select no model pegar� todos os registro da tabela.(� separado por "dia, semana, m�s, pr�ximo m�s" na view)
         *   sen�o o select pegar� todos os registro da tabela pelo c�digo do credor
         */
        if ($this->input->post('filtroCredor', true) == '' || $this->input->post('filtroCredor', true) == 'todos') {
            $filtroCredor = '';
        } else {
            $codCredor = $this->input->post('filtroCredor');
            $filtroCredor = "AND C.credor_cre_cod = $codCredor";
            $this->data['filtroCredor'] = $this->input->post('filtroCredor');
        }
        //--------------------------------------

        $this->data['todosCredores'] = $this->md->getCredores();
        $this->data['previsoesHoje'] = $this->md->getPrevisoesHoje($filtroCredor, $dataDeHoje);
        $this->data['previsoesSemana'] = $this->md->getPrevisoesSemana($filtroCredor, $primeiroDiaDaSemana, $ultimoDiaDaSemana);
        $this->data['previsoesMes'] = $this->md->getPrevisoesMes($filtroCredor, $mesAtual, $anoAtual);
        $this->data['previsoesProxMes'] = $this->md->getPrevisoesProxMes($filtroCredor, $proximoMes, $anoAtual);

        $this->inicore->loadSidebar();//menu (lateral esquerda).
        $this->inicore->loadview('rec_listar_previsoes', $this->data);
    }

}
