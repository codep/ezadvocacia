<?php

class Carteira extends Controller {

	function Carteira()
	{
            parent::Controller();
            

	}
	
	function _remap($link)
	{

            $this->data['title']="Recupera :: Remanejar Credores";
            $this->inicore->addcss(array('reset','style','style_fixed','colors/blue')); // CSS HOME
            $this->load->helper("funcoes_helper");
            $this->load->model('Credormodel', 'mdcredor');
            $this->load->model('Dividamodel', 'mddivida');
            $this->inicore->addjs(array('jquery-1.4.2.min.js','jquery-ui-1.8.custom.min.js','jquery.ui.selectmenu.js','jquery.flot.min.js','tiny_mce/jquery.tinymce.js','smooth.js','smooth.menu.js','smooth.table.js','smooth.form.js','smooth.dialog.js','smooth.autocomplete.js','plugin/jquery.maskedinput'));

            //monta a array com as permiss�es do usuario para testar se tem  permiss�o de acesso aos recursos
            //escolhendo o menu que ficara selecionado;
            $menus1=explode(",",$this->session->userdata('menu1'));
            $menus2=explode(",",$this->session->userdata('menu2'));
            $menus3=explode(",",$this->session->userdata('menu3'));

//------------------------------------------------------------------------------
            include 'testar_conexao.php';
//------------------------------------------------------------------------------
            //IR PARA O RESPECTIVO FLUXO DA URL
            if($link == "listar")
            {
                if (array_search('11',$menus2,TRUE)!='')
                {
                    //FUN��O RESPONSAVEL POR ABRIR A TELA DE NOVO CREDOR
                    $this->_listar();
                }
                else
                {
                    $this->inicore->setMensagem('error','Erro! Voc� n�o tem permiss�o para acessar este recurso');
                    redirect(base_url().'home');
                }
            }
            else if($link == "listar/filtro")
            {
                if (array_search('11',$menus2,TRUE)!=''){
                    //FUN��O RESPONSAVEL POR ABRIR A TELA DE NOVO CREDOR
                    $this->_filtro();
                }
                else
                {
                    $this->inicore->setMensagem('error','Erro! Voc� n�o tem permiss�o para acessar este recurso');
                    redirect(base_url().'home');
                }
            }
            else if($link == "remanejar_cre")
            {
                if (array_search('11',$menus2,TRUE)!='')
                {
                    $this->_remanejar_credor();
                }
                else
                {
                    $this->inicore->setMensagem('error','Erro! Voc� n�o tem permiss�o para acessar este recurso');
                    redirect(base_url().'home');
                }
            }
            else if($link == "remanejar_ina")
            {
                if (array_search('11',$menus2,TRUE)!='')
                {
                    $this->_remanejar_ina();
                }else{
                    $this->inicore->setMensagem('error','Erro! Voc� n�o tem permiss�o para acessar este recurso');
                    redirect(base_url().'home');
                }
            }
            else
            {
                $this->inicore->setMensagem('warning','Recurso ainda n�o implementado, entre em contato com o administrador do sistema');
                redirect(base_url().'home');
            }
	}

        function _listar()
        {

            $this->inicore->loadSidebar();
            
            $this->data['usuarios']=$this->mdcredor->getUsuariosNomeCodAtivosRemanejar();

            //RECUPERADOR PADR�O
            $rec_cod = $this->input->post('rec_padrao_cod');
            $rec_nome = $this->input->post('rec_padrao_nome');

            //NOVO RECUPERADOR (REMANEJADO)
            $rec_novo = $this->input->post('rec_novo_cod');
            $rec_novo_nome = $this->input->post('rec_novo_nome');
            if($rec_cod != '')
            {
                $this->data['credores']=$this->mdcredor->getCredorPorRec($rec_cod);
                if(sizeof($this->data['credores']) == 0)
                {
                    $this->inicore->setMensagem('warning','Nenhum credor atribuido ao recuperador: ['.$rec_cod.'] '.strtoupper($rec_nome));
                    redirect(base_url().'carteira/listar');
                }
            }
            else
            {
                $this->data['credores'] = array();
            }

            //SE acionado o bot�o Remanejar
            if($this->input->post('remanejar', true))
            {
                $aux = 1;

                //Query para o novo recuperador padr�o dos credores selecionados na view
                $query = "UPDATE credores set usuarios_responsavel_cod = $rec_novo WHERE ";

                //Para cada credor selecionado
                foreach ($_POST['credores_selecionados'] as $chave => $credor)
                {
                    //concatena a string
                    //se for o primeiro registro adiciona s� o c�digo do primeiro, para os demais acrescente o "or cre_cod..."
                     $query.= $aux==1?"cre_cod = $credor":" or cre_cod = $credor";
                     $aux++;
                }
                //SE DER UPDATE NA TABELA CREDOR DE TODOS OS CREDORES (setar novo recuperador padrao)
                if($this->mdcredor->remanejar($query))
                {                  
                    //PARA CADA CREDOR, REMANEJAR TODAS AS COBRAN�AS
                    foreach ($_POST['credores_selecionados'] as $chave => $credor)
                    {
                        $cobrancas = $this->mddivida->getCobrancas($credor);
                        foreach ($cobrancas as $cob)
                        {
                            $this->mddivida->remanejarPorCredor($rec_novo, $cob->cob_cod);

                        }
                    }
                    $this->inicore->setMensagem('success','Todas as cobran�as de '.strtoupper($rec_nome).' foram remanejadas para '.strtoupper($rec_novo_nome));
                    redirect(base_url().'carteira/listar');
                }
                else{
                    $this->inicore->setMensagem('error','N�o foi poss�vel realizar o remanejamento do credor. Entre em contato com o administrador do sistema');
                    redirect(base_url().'carteira/listar');
                }
                
            }

            $this->data['usu_filtro'] = $rec_cod;
            $this->data['usu_nome'] = $rec_nome;
            $this->inicore->loadview('ger_rem_cre',$this->data);
        }

        function _filtro($valor)
        {
            
        }

        function _remanejar_cre()
        {
            die('Cheguei no CRE');
        }

        function _remanejar_ina()
        {
            die('Cheguei no INA');
        }
}
