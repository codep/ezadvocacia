<?php

class Ro extends Controller {

    function Ro() {
        parent::Controller();
    }

    function _remap($link) {

        $this->data['title'] = "Recupera :: Registro de opera��es";

        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME

        $this->load->helper("funcoes_helper");

        $this->load->model('Romodel', 'md');

        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput'));
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        //IR PARA O RESPECTIVO FLUXO DA URL
        if ($link == "temp") {
            $this->_temp();
        } else if ($link == "novo") {
            $this->_novo();
        } else if ($link == 'incluir') {
            $this->_incluir();
        }
    }

    function _temp() {
        $this->inicore->loadSidebar();
        // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
        $this->inicore->loadview('divida_ro_cd_temp', $this->data);
    }

    function _novo() {
        $cod = get('cod');
        $dividas = explode('-', $cod);

        $this->data['credor'] = $this->md->getInfoCredor($dividas[0]);
        $this->data['inadimplente'] = $this->md->getInfoInadimplente($dividas[0]);

        $filtro = 'WHERE (C.cob_cod=' . $dividas[0];

        foreach ($dividas as $divida) {
            if (($divida != '') && ($divida != $dividas[0])) {
                $filtro .= ' OR C.cob_cod=' . $divida;
            }
        }
        $filtro .=')';
        $this->data['ros'] = $this->md->getRos($filtro);

        $acordos = $this->md->getAcordos($filtro);

        if ((isset($acordos)) && ($acordos != null)) {
            $cods = $acordos[0]->aco_cob_cod_geradora;
            $dividas = explode('-', $cods);

            $filtro = 'WHERE (C.cob_cod=' . $dividas[0];

            foreach ($dividas as $divida) {
                if (($divida != '') && ($divida != $dividas[0])) {
                    $filtro .= ' OR C.cob_cod=' . $divida;
                }
            }
            $filtro .=')';

            foreach ($acordos as $acordo) {
                if (($acordo->aco_cob_cod_geradora != '') && ($acordo->aco_cob_cod_geradora != $acordos[0]->aco_cob_cod_geradora)) {
                    $filtro .= ' OR C.cob_cod=' . $acordo->aco_cob_cod_geradora;
                }
            }
            $this->data['rosAntigos'] = $this->md->getRos($filtro);
        }

        $this->data['operacoes'] = $this->md->getOperacoes();
        $this->data['cobrancas'] = $cod;

        $this->inicore->loadSidebar();
        // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
        $this->inicore->loadview('ro_novo', $this->data);
    }

    function _incluir() {



        if (
                ($this->input->post('cobrancas', true) == null) ||
                ($this->input->post('usuario', true) == null) ||
                ($this->input->post('data', true) == null) ||
                ($this->input->post('hora', true) == null) ||
                ($this->input->post('operacao', true) == null)
        ) {
            //dados obrigat�rios
            $this->inicore->setMensagem('error', 'Dados insuficientes para o cadastro', true);
            redirect(base_url() . 'ro/novo/cod:' . $this->input->post('cobrancas', true));
        }

        $operacao = $this->input->post('operacao', true);
        $rosDetalhes = removeCE_Upper(trim($this->input->post('obs', true)));


        $dividas = explode('-', $this->input->post('cobrancas', true));
        foreach ($dividas as $divida) {
            if (($divida != '')) {

                if ($operacao == '67') {//NEGATIVA��O
                    $inadCod = $this->input->post('inadCod', true);

                    $data = array(
                        'ina_negativado' => '1'
                    );

                    $this->db->where('ina_cod', $inadCod);
                    $this->db->update('inadimplentes', $data);

                    $dados = array(
                        'cobranca_cob_cod' => $divida,
                        'operacoes_ope_cod' => $operacao,
                        'usuarios_usu_cod' => $this->input->post('usuario', true),
                        'ros_data' => utf8_encode($this->input->post('data', true)),
                        'ros_hora' => utf8_encode($this->input->post('hora', true)),
                        'ros_detalhe' => $rosDetalhes
                    );
                    $this->db->insert('ros', $dados);
                } else if ($operacao == '66') {//NEGATIVA��O
                    $inadCod = $this->input->post('inadCod', true);

                    $data = array(
                        'ina_negativado' => '0'
                    );

                    $this->db->where('ina_cod', $inadCod);
                    $this->db->update('inadimplentes', $data);

                    $dados = array(
                        'cobranca_cob_cod' => $divida,
                        'operacoes_ope_cod' => $operacao,
                        'usuarios_usu_cod' => $this->input->post('usuario', true),
                        'ros_data' => utf8_encode($this->input->post('data', true)),
                        'ros_hora' => utf8_encode($this->input->post('hora', true)),
                        'ros_detalhe' => removeCE_Upper(trim($this->input->post('obs', true)))
                    );
                    $this->db->insert('ros', $dados);
                } else if ($operacao == '19') {//Aguardando
                    $data = array(
                        'cob_status' => '2'
                    );

                    $this->db->where('cob_cod', $divida);
                    $this->db->update('cobrancas', $data);

                    $dados = array(
                        'cobranca_cob_cod' => $divida,
                        'operacoes_ope_cod' => $operacao,
                        'usuarios_usu_cod' => $this->input->post('usuario', true),
                        'ros_data' => utf8_encode($this->input->post('data', true)),
                        'ros_hora' => utf8_encode($this->input->post('hora', true)),
                        'ros_detalhe' => removeCE_Upper(trim($this->input->post('obs', true)))
                    );
                    $this->db->insert('ros', $dados);
                } else if ($operacao == '32') {//Ajuizado
                    $data = array(
                        'cob_status' => '4'
                    );

                    $this->db->where('cob_cod', $divida);
                    $this->db->update('cobrancas', $data);

                    $dados = array(
                        'cobranca_cob_cod' => $divida,
                        'operacoes_ope_cod' => $operacao,
                        'usuarios_usu_cod' => $this->input->post('usuario', true),
                        'ros_data' => utf8_encode($this->input->post('data', true)),
                        'ros_hora' => utf8_encode($this->input->post('hora', true)),
                        'ros_detalhe' => removeCE_Upper(trim($this->input->post('obs', true)))
                    );
                    $this->db->insert('ros', $dados);
                } else if ($operacao == '21') {//Ajuizado
                    $data = array(
                        'cob_status' => '3'
                    );

                    $this->db->where('cob_cod', $divida);
                    $this->db->update('cobrancas', $data);

                    $dados = array(
                        'cobranca_cob_cod' => $divida,
                        'operacoes_ope_cod' => $operacao,
                        'usuarios_usu_cod' => $this->input->post('usuario', true),
                        'ros_data' => utf8_encode($this->input->post('data', true)),
                        'ros_hora' => utf8_encode($this->input->post('hora', true)),
                        'ros_detalhe' => removeCE_Upper(trim($this->input->post('obs', true)))
                    );
                    $this->db->insert('ros', $dados);
                } else {
                    $dados = array(
                        'cobranca_cob_cod' => $divida,
                        'operacoes_ope_cod' => $operacao,
                        'usuarios_usu_cod' => $this->input->post('usuario', true),
                        'ros_data' => utf8_encode($this->input->post('data', true)),
                        'ros_hora' => utf8_encode($this->input->post('hora', true)),
                        'ros_detalhe' => removeCE_Upper(trim($this->input->post('obs', true)))
                    );
                    $this->db->insert('ros', $dados);
                }
            }
        }

        if (($this->input->post('data_agendamento', true) != null) && ($this->input->post('tarefa', true) != null)) {

            $data = $this->input->post('data_agendamento', true); //recebe a data que o usu�rio digita no campo data do formul�rio de nova tarefa
            $dia = substr($data, 0, -8); //pega s� o dia da data
            $mes = substr($data, 3, -5); //pega s� o m�s da data
            $ano = substr($data, -4); //pega s� o ano data
            $agendada = $ano . '-' . $mes . '-' . $dia;

            $dados = array(
                'usuarios_usu_cod' => $this->input->post('usuario', true),
                'tar_criacao' => date('Y-m-d'),
                'tar_agendada' => $agendada,
                'tar_titulo' => 'TAREFA AUTOMATICA PRE AGENDADA',
                'tar_criador' => $this->session->userdata('usuwho'),
                'tar_descricao' => removeCE_Upper($this->input->post('tarefa', true)),
                'tar_automatica' => '1'
            );
            $this->db->insert('tarefas', $dados);
        }

        $this->inicore->setMensagem('success', 'Registro de opera��o cadastrado com sucesso', true);
        redirect(base_url() . 'ro/novo/cod:' . $this->input->post('cobrancas', true));
    }

}
