<?php

class Cadopr extends Controller {

    function Home() {
        parent::Controller();
    }

    function _remap($link)
	{
            //passa o titulo da p�gina
            $this->data['title']="Recupera :: Cadastro de tipos de opera��o";
            //adiciona o Css
            $this->inicore->addcss(array('reset','style','style_fixed','colors/blue')); // CSS HOME
            //carrega o helper
            $this->load->helper("funcoes_helper");
            //carrega o model
            $this->load->model('operacoes_model','md'); //carregando o model

            $this->inicore->addjs(array('jquery-1.4.2.min.js','jquery-ui-1.8.custom.min.js','jquery.ui.selectmenu.js','jquery.flot.min.js','tiny_mce/jquery.tinymce.js','smooth.js','smooth.menu.js','smooth.table.js','smooth.form.js','smooth.dialog.js','smooth.autocomplete.js','plugin/jquery.maskedinput'));
            //monta a array com as permiss�es do usuario para testar se tem  permiss�o de acesso aos recursos
            $menus1 = explode(",", $this->session->userdata('menu1'));
            $menus2 = explode(",", $this->session->userdata('menu2'));
            $menus3 = explode(",", $this->session->userdata('menu3'));
//------------------------------------------------------------------------------
            include 'testar_conexao.php';
//------------------------------------------------------------------------------
            //IR PARA O RESPECTIVO FLUXO DA URL
            if($link == "novo") {
                if (array_search('6',$menus2,TRUE)!=''){
                    // fun��o responsavel por abrir a tela de novo bem
                    $this->_novo();
                }else{
                    $this->inicore->setMensagem('error','Erro! Voc� n�o tem permiss�o para acessar este recurso');
                    redirect(base_url().'home');
                }
            }else if($link == "incluir") {
                if (array_search('6',$menus2,TRUE)!=''){
                    //fun�ao responsavel por incluir dados no banco
                    $this->_incluir();
                }else{
                    $this->inicore->setMensagem('error','Erro! Voc� n�o tem permiss�o para acessar este recurso');
                    redirect(base_url().'home');
                }
            }else{
                $this->inicore->setMensagem('warning','Recurso indispon�vel');
                redirect(base_url().'home');
            }

	}

        function _novo()
        {
            //seta o menu q ficara selecionado
            $this->session->set_userdata('menusel', '1');
            $this->inicore->loadSidebar();
            $this->data['operacoes']=$this->md->getOpera��es();
            // CARREGANDO A VIEW DE CADSTRO DE OPERA��ES
            $this->inicore->loadview('cad_operacao',$this->data);
        }

        function _incluir()
        {
            // MONTA UMA ARRAY COM OS DADOS QUE VEM DO POST
            $dados = array(
                'ope_nome' => removeCE_Upper($this->input->post('nome',true)),
                'ope_automatica' => '0'
            );
            // INSERE OS DADOS NO BANCO
            if($this->db->insert('operacoes', $dados)){
                $this->inicore->setMensagem('success','Opera��o cadastrada com sucesso',true);
                redirect(base_url().'cadopr/novo');
            }else{
                $this->inicore->setMensagem('error','Erro ao cadastrar a opera��o, tente novamente; Se o erro persistir entre em contato com a administrador do sistema',true);
                redirect(base_url().'cadopr/novo');
            }

        }

}
