<?php

class Inadimplente extends Controller {

    function Inadimplente() {
        parent::Controller();
    }

    function _remap($link) {

        $this->data['title'] = "Recupera :: Gerenciar inadimplentes";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->load->helper("funcoes_helper");
        $this->load->model('inadimplente_model', 'md');
        $this->load->model('dividamodel', 'md_divida');

        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput', 'jquery.funcoes.js','jquery.cookie.js','plugin/jquery.quicksearch.min.js'));
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        $menus1 = explode(",", $this->session->userdata('menu1'));
        $menus2 = explode(",", $this->session->userdata('menu2'));
        $menus3 = explode(",", $this->session->userdata('menu3'));

        //IR PARA O RESPECTIVO FLUXO DA URL
        if ($link == "listar") {
            //FUN��O QUE MONTA A VIEW COM A LISTAGEM DOS INADIMPLENTES
            $this->_listar();
        } else if ($link == "incluir") {
            //FUN��O RESPONSAVEL POR EXECUTAR O INSERT NO BANCO DE DADOS
            if (array_search('3', $menus2, TRUE) != '') {
                $this->_incluir();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "novo") {
            //FUN��O RESPONSAVEL POR ABRIR A TELA DE NOVO INADIMPLENTE
            if (array_search('3', $menus2, TRUE) != '') {
                $this->_novo();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "remanejar") {
            //FUN��O RESPONSAVEL POR ABRIR A TELA DE REMANEJAR O INADIMPLENTE
            if (array_search('12', $menus2, TRUE) != '') {
                $this->_remanejar();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "editar") {
            //FUN��O RESPONSAVEL POR ABRIR A TELA DE EDI��O DO INADIMPLENTE
            $this->_editar();
        } else if ($link == "excluir") {
            //FUN��O RESPONSAVEL POR ABRIR A TELA DE EXCLUSAO DE INADIMPLENTE
            if (array_search('3', $menus2, TRUE) != '') {
                $this->_excluir();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "delete") {
            //FUN��O RESPONSAVEL POR EXECUTAR O DELETE NO BANCO DE DADOS
            if (array_search('3', $menus2, TRUE) != '') {
                $this->_delete();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "listar_cidades") {
            //FUN��O RESPONSAVEL POR LISTAR AS CIDADES PARA O AJAX
            $this->_listar_cidades();
        } else if ($link == "listar_pesquisa") {
            //FUN��O RESPONSAVEL POR FUN��O USADA NO AJAX PARA RETORNAR A PESQUISA DE INADIMPLENTES
            $this->_listar_pesquisa();
        } else if ($link == "listar_pesquisa_blur") {
            //FUN��O RESPONSAVEL POR FUN��O USADA NO AJAX PARA RETORNAR A PESQUISA DE INADIMPLENTES
            $this->_listar_pesquisa_blur();
        } else if ($link == "update") {
            //FUN��O RESPONSAVEL POR REALIZAR O UPDATE DOS DADOS NO BANCO DE DADOS
            $this->_update();
        } else if ($link == "novo_parente") {
            //FUN��O RESPONSAVEL POR CADASTRAR UM NOVO PARENTE
            $this->_novo_parente();
        } else if ($link == "atualizar_parente") {
            //FUN��O RESPONSAVEL POR CADASTRAR UM NOVO PARENTE
            $this->_atualizar_parente();
        } else if ($link == "cadBem") {
            //FUN��O RESPONSAVEL POR CADASTRAR UM NOVO BEM NO BANCO DE DADOS PARA O INADIMPLENTE
            $this->_cadBem();
        } else if ($link == "atualizaBem") {
            //FUN��O RESPONSAVEL POR ATUALIZAR O BEM DO INADIMPLENE QUE ESTA SENDO EDITADO
            $this->_atualizaBem();
        } else if ($link == "remanjarInadimplente") {
            $this->_remanjarInadimplente();
        } else if ($link == "inadDadosImportados") {
            $this->_visualizarImportacaoInad();
        } else {
            $this->inicore->setMensagem('warning', 'Recurso ainda n�o implementado, entre em contato com o administrador do sistema');
            redirect(base_url() . 'home');
        }
    }

    function _listar() {
        //SETA O MENU QUE DEVERA FICAR ABERTO E CARREGA A SIDEBAR
        $this->session->set_userdata('menusel', '9');
        $this->inicore->loadSidebar();
        //INICIA UMA VARIAVEL QUE SERA PASSADA PRA A VIEW E SERVIRA PARA DEIXAR
        //A OP��O SELECIONADA DO FILTRO
        
        $filtro='';

        if ($this->input->post('filtro1_desc', true) != "") {//SE VIR ALGO NO FILTRO 1
            if ($this->input->post('filtro1', true) == 'telefone') {//SE FOR UM TELEFONE, FAZ-SE UM TRATAMENTO ESPECIAL
                $aux = $this->input->post('filtro1_desc', true); // PASSA OS DADOS DO FILTRO PARA UMA VARIAVEL AUXILIAR
                $filtro .=" I.ina_foneres LIKE '%$aux%' OR I.ina_fonerec LIKE '%$aux%'
                         OR I.ina_fonecom LIKE '%$aux%' OR I.ina_cel1 LIKE '%$aux%' OR I.ina_cel2 LIKE '%$aux%'
                         OR I.ina_cel3 LIKE '%$aux%'"; // BUSCA EM TODOS OS CAMPOS DE TELEFONE DO INADIMPLENTE
            } else if ($this->input->post('filtro1', true) == 'telenonePAR') {//SE FOR UM TELEFONE DE PARENTE TAMBEM TEM UM TRATAMENTO ESPECIAL
                $aux = $this->input->post('filtro1_desc', true); //PASSA O FILTRO PARA UMA VARIAVEL AUXILIAR
                $filtro .="I.ina_mae_fone LIKE '%$aux%'
                        OR I.ina_pai_fone LIKE '%$aux%'
                        OR I.ina_conj_fone LIKE '%$aux%'"; // BSCA EM TODOS OS CAMPOS DE TELEFONES DE PARENTES
            } else if ($this->input->post('filtro1', true) == 'parentes') {//SE A PESQUISA FOR POR PARENTES, TAMBEM TEM UM TRATAMENTO ESPECIAL
                $aux = $this->input->post('filtro1_desc', true);
                $filtro .="I.ina_conj_nome LIKE '%$aux%'
                            OR I.ina_mae_nome LIKE '%$aux%'
                            OR I.ina_pai_nome LIKE '%$aux%'"; // BUSCA EM TODOS OS CAMPOS DE PARENTES
            } else {// SE N�O TIVER NENHUM TRATAMENTO ESPECIAL, MONTA UM FILTRO GEN�RICO COM O CAMPO SELECIONADO E O FILTRO DIGITADO
                $aux = $this->input->post('filtro1_desc', true);
                $aux2 = $this->input->post('filtro1', true);
                $filtro .=" I.$aux2 LIKE '%$aux%'";
            }
            $filtroAux = $this->input->post('filtro1', true); // DEPENDENDO DO QUE VIER DO SELECT MANDARA UMA COISA PARA A VIEW
            switch ($filtroAux) {
                case 'ina_nome':
                    $filtroAux = "Nome";
                    break;
                case 'telefone':
                    $filtroAux = "Telefone";
                    break;
                case 'ina_cidade':
                    $filtroAux = "Cidade";
                    break;
                case 'telenonePAR':
                    $filtroAux = "Telefone (parentes)";
                    break;
                case 'ina_endereco':
                    $filtroAux = "Endere�o";
                    break;
                case 'ina_cpf_cnpj':
                    $filtroAux = "CPF";
                    break;
                case 'parentes':
                    $filtroAux = "Parentes";
                    break;
                case 'ina_mae_nome':
                    $filtroAux = "Nome da M�e";
                    break;
                case 'ina_pai_nome':
                    $filtroAux = "Nome do Pai";
                    break;
            }
            $this->data['filtro1'] = $filtroAux; //PASSA PARA A VIEW O FILTRO SELECIONADO
            $this->data['filtro1_desc'] = $this->input->post('filtro1_desc', true); //PASSA PARA A VIEW O FILTRO DIGITADO
        }
    
        
/*
        //TESTE PARA VER SE VEIO ALGUMA COISA POR POST NO FILTRO
        if ($this->input->post('cidade', true) != '') {
            //SE VIR UMA CIDADE MONTA O FILTRO COM O NOME DA MESMA
            $filtro = 'WHERE ina_ativo=\'1\' AND ina_cidade=\'' . utf8_encode($this->input->post('cidade', true)) . '\'';
            //PASSA O FILTRO PARA A SESS�O PARA QUANDO O USUARIO CLICAR NA PAGINA��O
            //O FILTRO QUE VEM POR POST N�O SE PERDER
            $this->session->set_userdata('filtro', utf8_encode($this->input->post('cidade', true)));
            //PASSA O FILTRO PARA DEIXAR A CIDADE SELECIONADA NA VIEW
            $this->data['filtro'] = $this->input->post('cidade', true);
        } else {
            //CASO N�O VENHA NADA POR POST NO FILTRO SETA O FILTRO PADR�O
            $filtro = 'WHERE ina_ativo=\'1\'';
        }
        //SE N�O VEIO NADA POR POST TESTA PARA SABER SE TEM ALGUM FILTRO NA SESS�O
        if ($this->session->userdata('filtro') != '') {
            //SE TIVER FILTRO NA SESS�O ATRIBUI A VARIAVEL PARA PASSA-LO PARA O
            //MODEL E SETA A VARIAVEL QUE VAI PARA A VIEW
            $filtro = 'WHERE ina_ativo=\'1\' AND ina_cidade=\'' . $this->session->userdata('filtro') . '\'';
            $this->data['filtro'] = $this->session->userdata('filtro');
        }
        //SE O FILTRO QUE VIR POR POST FOR ZERAR, ZERA O FILTRO E SETA O FILTRO PADR�O
        if ($this->input->post('cidade', true) == 'zerar') {
            $filtro = 'WHERE ina_ativo=\'1\'';
            $this->session->unset_userdata('filtro');
        }
 */
 
 		if (!empty($filtro)) $filtro='WHERE '.$filtro;

        // CARREGA P�GINA��O
        $this->load->library('pagination');
        //CONFIGURA��ES DE PAGINA��O PODEM SER ENCONTRADAS EM http://codeigniter.com/user_guide/libraries/pagination.html
        $config['base_url'] = base_url() . 'inadimplente/listar';
        $config['total_rows'] = $this->md->getRows($filtro);
        $config['per_page'] = '15';
        $config['uri_segment'] = 3;
        $config['last_link'] = '&gt;&gt;';
        $config['first_link'] = '&lt; &lt;';

        $this->pagination->initialize($config);
        $this->data['paginacao'] = $this->pagination->create_links();
        $pag = $this->uri->segment($config['uri_segment']);
        if (!isset($pag) || $pag == '' || !is_numeric($pag))
            $pag = 0;
        //BUSCA NO BANCO AS INFORMA��ES PARA MONTAR A VIEW
        $this->data['cidades'] = $this->md->getCidades();
        $this->data['total'] = $this->md->getRows($filtro);
        $this->data['inadimplentes'] = $this->md->getInadLista($pag, $config['per_page'], $filtro);
        $this->inicore->loadview('list_inadimplente', $this->data);
    }

    function _novo() {
        //SELECIONA O MENU QUE FICARA ABERTO E CARREGA A BARRA
        $this->session->set_userdata('menusel', '1');
        $this->inicore->loadSidebar();
        //PEGA OS DADOS QUE SER�O NECESSARIOS PARA A P�GINA
        $this->data['estados'] = $this->md->getEstados();
        // CARREGANDO A VIEW DE CADASTRO DE INADIMPLENTE
        $this->inicore->loadview('cad_inadimplente', $this->data);
    }

    function _editar() {
        //SELECIONA O MENU QUE FICARA ABERTO E CARREGA A BARRA
        $this->session->set_userdata('menusel', '1');
        //PEGA O CODIGO DO INADIMPLENTE A SER EDITADO POR GET
        $cod = get('cod');
        //PEGA OS DADOS QUE SER�O NECESSARIOS PARA A P�GINA
        $this->data['bem'] = $this->md->getBem($cod);
        $this->data['pesquisas'] = $this->md->getPesquisas($cod);
        $this->data['parentes'] = $this->md->getParentes($cod);
        $this->data['estados'] = $this->md->getEstados();
        $this->data['inadDados'] = $this->md->getInadDados($cod);
        $this->data['existeParente'] = 0;
        $this->data['existeImportacao'] = $this->md->existeImportacao($cod); //verifica se tem dados na tabela import_inadimplentes
        if (get('parCod')) {
            $parCod = get('parCod');
            $this->data['parDados'] = $this->md->getParDados($parCod);
            $this->data['existeParente'] = 1;
        }


        $this->inicore->loadSidebar();
        // CARREGANDO A VIEW DE EDI��O DE INADIMPLENTE
        $this->data['usuarioDados'] = $this->md_divida->getInfoUsuario($this->session->userdata('usucod'));
        $this->inicore->loadview('inad_editar', $this->data);
    }

    function _excluir() {
        $cod = get('cod');
        /* A fun��o excluir possui uma tela de confirma��o de exclus�o que
         * serve para qualque exclus�o no sistema, para que esta tela funcione
         * corretamente deve ser passados algumas vari�veis que ser�o utilizadas
         * na constru��o da p�gina, as vari�veis necess�rias s�o:
         * $deletado-----------------------O que est� sendo deletado
         * $codigo-------------------------c�digo do que est� sendo deletado
         * $tabela-------------------------Tabela que ter� o item excluido
         * $chave--------------------------Chave primaria da tabela para o delete
         * $acaocancel---------------------A��o executada quando clicar em cancelar
         * $acaook-------------------------A��o executada quando clicar em Apagar
         */
        $this->data['title'] = "Recupera :: Confirmar exclus�o";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->inicore->addjs(array('smooth.form.js'));
        //PASSA OS DADOS QUE SER�O NECESSARIOS PARA CARREGAR A TELA
        $this->data['deletado'] = 'Inadimplente';
        $this->data['codigo'] = $cod;
        $this->data['tabela'] = 'inadimplentes';
        $this->data['chave'] = 'ina_cod';
        $this->data['acaocancel'] = 'inadimplente/listar';
        $this->data['acaook'] = 'inadimplente/delete';
        //CARREGA A TELA DE CONFIRMA��OD E EXCLUS�O
        $this->inicore->loadview('conf_exclusao', $this->data);
    }

    function _delete() {
        //pega a senha inserida pelo usu�rio e criptografa a mesma seguindo uma regra pre estabelecida
        $senha = sha1('senhamaster+' . $this->input->post('senha', true));
        //pega a senha orginal no banco de dados
        $sql = "SELECT cod, senha FROM senha_master";
        $query = $this->db->query($sql);
        //testa se as senhas s�o iguais
        if ($query->row()->senha == $senha) {
            $this->db->where($this->input->post('chave', true), $this->input->post('codigo', true));
            //se as senhas estiverem iguais apaga o usu�rio e exibe mensagem de acordo com o ocorrifo
            $dados = array(
                'ina_ativo' => '0'
            );
            if ($this->db->update('inadimplentes', $dados)) {
                $this->inicore->setMensagem('notice', 'Inadimplente apagado com sucesso', true);
            } else {
                $this->inicore->setMensagem('error', 'Erro ao apagar o inadimplente, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
            }

            auditar('e', 'inadimplentes', $this->session->userdata('usucod'), 'EXCLUIDO O INADIMPLENTE COD: ' . $this->input->post('codigo', true));

            //redireciona para onde deve ser redirecionado :)
            redirect($this->input->post('voltar', true));
            // caso a senha estiver errada monta a tela de exclus�o novamente  e
            // exibe mensagem de erro para o usu�rio
        } else {
            $this->data['title'] = "Recupera :: Confirmar exclus�o";
            $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
            $this->inicore->addjs(array('smooth.form.js'));
            $this->data['deletado'] = 'Inadimplente';
            $this->data['codigo'] = $this->input->post('codigo', true);
            $this->data['tabela'] = 'inadimplentes';
            $this->data['chave'] = 'ina_cod';
            $this->data['acaocancel'] = 'inadimplente/listar';
            $this->data['acaook'] = 'inadimplente/delete';
            $this->inicore->setMensagem('error', 'SENHA incorreta, favor verificar e digitar novamente', false);
            $this->inicore->loadview('conf_exclusao', $this->data);
        }
    }

    function _remanejar() {//carrega a view remanejar inadimplente
        $this->session->set_userdata('menusel', '9'); //DETERMINA O MENU QUE FICARA ABERTO
        $this->inicore->loadSidebar(); //carrega a barra lateral
        $this->data['recuperadores'] = $this->md->getRecuperadoresRemanejar(); //pega os recuperadores do sistema.
        $this->inicore->loadview('ger_rem_inad', $this->data);
    }

    function _remanjarInadimplente() {//recebe os dados da view e remaneja o inadimplente
        $inad_cod = $this->input->post('inad_cod');
        $novo_recuperador = $this->input->post('recuperador'); //recebe o c�d do recuperador

        if ($novo_recuperador == 'nulo') {
            $this->inicore->setMensagem('error', 'Voc� deve selecionar um recuperador!', true);
            redirect(base_url() . 'inadimplente/remanejar');
        }

        if ($inad_cod == '' || !is_numeric($inad_cod)) {
//            die("Aviso para o pr�ximo programador: Voc� deve enviar um n�mero");
            $this->inicore->setMensagem('error', 'Dados insuficientes para realizar uma pesquisa!', true);
            redirect(base_url() . 'inadimplente/remanejar');
        } else if ($inad_cod != '' && is_numeric($inad_cod)) {
            $remanejado = $this->md->setRemanejarInad($inad_cod, $novo_recuperador);
            if ($remanejado == 0) {
                $this->inicore->setMensagem('error', 'O inadimplente n�o foi remanejado. Ou o inadimplente n�o possui nenhuma cobran�a ativa ou o recuperador escolhido j� � o recuperador padr�o.', true);
                redirect(base_url() . 'inadimplente/remanejar');
            }
            $this->inicore->setMensagem('success', 'Inadimplente remanejado com sucesso', true);
            redirect(base_url() . 'inadimplente/remanejar');
        }
    }

    /* FUN��O AJAX PARA LISTAR CIDADES BRASILEIRAS POR UF
     * v1 ------- dado com a UF enviada da view
     */

    function _listar_cidades() {
        //VALOR DO POST ENVIADO PELA VIEW
        $uf = $this->input->post('v1', true);
        if ($uf != 'UF') {
            //VARI�VEL CONTENDO TODAS AS CIDADES DA UF
            $retorno = $this->md->getCidadesBrasil($uf);
            //POPULANDO O ARRAY $cidades COM OS DADOS RECEBIDOS NA VARI�VEL $retorno
            foreach ($retorno as $cidadeatual) {
                $cidades[] = array(
                    'cidadeatual' => rawurlencode(utf8_decode($cidadeatual->cid_nome))
                );
            }
            //MATA O FLUXO RETORNANDO UM OBJETO DO TIPO JSON de nome "cidadesbrasil" CONTENDO O ARRAY $cidades
            die(
                    json_encode(array(
                        "cidadesbrasil" => $cidades
                    ))
            );
        } else {
            $cidades[] = array('cidadeatual' => '');

            die(
                    json_encode(array(
                        "cidadesbrasil" => $cidades
                    ))
            );
        }
    }

    /* FUN��O AJAX PARA PESQUISAR POR NOME OU CPF_CNPJ
     * Esta fun��o recebe os dados por post (entidade, pessoa, tipodado, valor) e os repassa para
     * uma outra fun��o do arquivo funcoes_helper.php que, por sua vez, executa as queries necess�rias
     * para retornar os dados de acordo com os par�metros enviados
     * SIGNIFICADO DOS DADOS RECEBIDOS
     * $entidade -------- i=inadimplentes, c=credores
     * $pessoa ---------- j=jur�dica, f=f�sica
     * $tipoDado -------- n=nome, c=cpf_cnpj
     * $valor ----------- valor do nome ou cpf_cnpj
     */

    function _listar_pesquisa() {
        //RECEBENDO OS DADOS POST
        $entidade = $this->input->post('entidade', true);
        $pessoa = $this->input->post('pessoa', true);
        $tipoDado = $this->input->post('tipodado', true);
        $valor = removeCE_Upper(utf8_decode($this->input->post('valor', true)));

        //ATRIBUINDO A VARI�VEL $retorno OS DADOS RECEBIDOS DA FUN��O pesquisaIC
        $retorno = pesquisaIC($entidade, $pessoa, $tipoDado, $valor);
        //CASO O RETORNO CONTENHA ALGO FA�A
        if ($retorno != false) {
            //POPULANDO O ARRAY $dados COM OS DADOS RECEBIDOS NA VARI�VEL $retorno
            foreach ($retorno as $dadoatual) {
                $dados[] = array(
                    'codigo' => rawurlencode(utf8_decode($dadoatual->codigo)),
                    'nome' => rawurlencode(utf8_decode($dadoatual->nome)),
                    'cpf_cnpj' => rawurlencode(utf8_decode($dadoatual->cpf_cnpj)),
                    'endereco' => rawurlencode(utf8_decode($dadoatual->endereco)),
                    'cidade' => rawurlencode(utf8_decode($dadoatual->cidade)),
                    'fone' => rawurlencode(utf8_decode($dadoatual->fone)),
                );
            }
            //MATA O FLUXO RETORNANDO UM OBJETO DO TIPO JSON de nome "resultado" CONTENDO O ARRAY $dados
            die(
                    json_encode(array(
                        "resultado" => $dados
                    ))
            );
        }
        //CASO A VARI�VEL $retorno SEJA DO TIPO false, MATA O FLUXO E RETORNA UM OBJETO DO TIPO JSON VAZIO
        else {
            $dados[] = array('dadoatual' => '');

            die(
                    json_encode(array(
                        "resultado" => $dados
                    ))
            );
        }
    }

    function _listar_pesquisa_blur() {
        //RECEBENDO OS DADOS POST
        $entidade = $this->input->post('entidade', true);
        $pessoa = $this->input->post('pessoa', true);
        $tipoDado = $this->input->post('tipodado', true);
        $valor = removeCE_Upper(utf8_decode($this->input->post('valor', true)));

        //ATRIBUINDO A VARI�VEL $retorno OS DADOS RECEBIDOS DA FUN��O pesquisaIC
        $retorno = pesquisaICBlur($entidade, $pessoa, $tipoDado, $valor);
        //CASO O RETORNO CONTENHA ALGO FA�A
        if ($retorno != false) {
            //POPULANDO O ARRAY $dados COM OS DADOS RECEBIDOS NA VARI�VEL $retorno
            foreach ($retorno as $dadoatual) {
                $dados[] = array(
                    'codigo' => rawurlencode(utf8_decode($dadoatual->codigo)),
                    'nome' => rawurlencode(utf8_decode($dadoatual->nome)),
                    'cpf_cnpj' => rawurlencode(utf8_decode($dadoatual->cpf_cnpj)),
                    'endereco' => rawurlencode(utf8_decode($dadoatual->endereco)),
                    'cidade' => rawurlencode(utf8_decode($dadoatual->cidade)),
                    'fone' => rawurlencode(utf8_decode($dadoatual->fone)),
                );
            }
            //MATA O FLUXO RETORNANDO UM OBJETO DO TIPO JSON de nome "resultado" CONTENDO O ARRAY $dados
            die(
                    json_encode(array(
                        "resultado" => $dados
                    ))
            );
        }
        //CASO A VARI�VEL $retorno SEJA DO TIPO false, MATA O FLUXO E RETORNA UM OBJETO DO TIPO JSON VAZIO
        else {
            $dados[] = array('dadoatual' => '');

            die(
                    json_encode(array(
                        "resultado" => $dados
                    ))
            );
        }
    }

    /* FUN��O PARA PERSISTIR OS DADOS DE UM NOVO INADIMPLENTE NO BANCO DE DADOS
     * UTILIZA��O DE STORED PROCEDURE PARA A PERSIST�NCIA DOS DADOS
     *
     */

    function _incluir() {
        $query = '';
        if
        (
        /*         * *** DADOS OBRIGAT�RIOS ***** */
                ($this->input->post('pessoa', true) != null) &&
                ($this->input->post('nome_fantasia', true) != null)
        ) {
            //MONTA UMA QUERY COM TODOS OS DADOS EM ORDEM PARA A STORED PROCEDURE
            $query .= '\'' . $this->session->userdata('usucod') . '\',';
            $query .= '\'' . $this->input->post('pessoa', true) . '\',';
            $query .= '\'' . $this->input->post('sexo', true) . '\',';
            $query .= $this->input->post('estado_civil', true) . ',';
            $query .= '\'' . removeCE_Upper($this->input->post('nome_fantasia', true)) . '\',';
            $query .= '\'' . $this->input->post('cpf_cnpj', true) . '\',';
            $query .= '\'' . $this->input->post('rg_ie', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('endereco', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('bairro', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('complemento', true)) . '\',';
            $query .= '\'' . $this->input->post('cep', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('cidade', true)) . '\',';
            $query .= '\'' . $this->input->post('uf', true) . '\',';
            $query .= '\'' . $this->input->post('foneres', true) . '\',';
            $query .= '\'' . $this->input->post('fonerec', true) . '\',';
            $query .= '\'' . $this->input->post('fonecom', true) . '\',';
            $query .= '\'' . $this->input->post('cel1', true) . '\',';
            $query .= '\'' . $this->input->post('cel2', true) . '\',';
            $query .= '\'' . $this->input->post('cel3', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('info', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('conj_nome', true)) . '\',';
            $query .= '\'' . $this->input->post('conj_fone', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('conj_endereco', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('conj_cidade', true)) . '\',';
            $query .= '\'' . $this->input->post('conj_uf', true) . '\',';
            $query .= '\'' . $this->input->post('conj_cod', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('pai_nome', true)) . '\',';
            $query .= '\'' . $this->input->post('pai_fone', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('pai_endereco', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('pai_cidade', true)) . '\',';
            $query .= '\'' . $this->input->post('pai_uf', true) . '\',';
            $query .= '\'' . $this->input->post('pai_cod', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('mae_nome', true)) . '\',';
            $query .= '\'' . $this->input->post('mae_fone', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('mae_endereco', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('mae_cidade', true)) . '\',';
            $query .= '\'' . $this->input->post('mae_uf', true) . '\',';
            $query .= '\'' . $this->input->post('mae_cod', true) . '\'';
            /* INSERINDO NO BANCO ATRAV�S DE STORED PROCEDURE (SP)
             * FUNCIONA APENAS COM O DRIVER MYSQLI
             * � NECESS�RIO ALTERAR O ARQUIVO database.php //modificar para// $db['default']['dbdriver'] = "mysqli";
             */
            //VARI�VEL RESPONS�VEL POR ARMAZENARO O SQL RETORNADO PELA SP
            //A SP RETORNA UM ROW COM ALIAS "msg" CONTENDO UMA STRING DE VALOR "CADASTRADO" OU COM O VALOR DA MENSAGEM DE ERRO

            $resultado = $this->db->query("CALL SP_CADASTRA_INAD(" . $query . ");");


            //CONTENDO O VALOR CADASTRADO SETA A MENSAGEM DE SUCESSO E RETORNA A VIEW
            if (utf8_decode($resultado->row()->msg) == 'CADASTRADO') {
                //SETA A MENSAGEM ABAIXO E RETORNA PARA A VIEW
//                auditar('c','inadimplentes',$this->session->userdata('usucod'),'CADASTRADO O INADIMPLENTE NOME: '.$this->input->post('nome_fantasia', true).' E CPF: '.$this->input->post('cpf_cnpj', true));

                $this->inicore->setMensagem('success', 'Inadimplente ' . removeCE_Upper($this->input->post('nome_fantasia', true)) . ' cadastrado com sucesso');
                redirect(base_url() . 'inadimplente/novo');
            }
            //SE A SP RETORNAR OUTRO VALOR MATA O FLUXO COM O VALOR RETORNADO
            else {
                die(utf8_decode($resultado->row()->msg));
            }
        } else {
            //SE N�O ATENDER AOS DADOS OBRIGAT�RIOS (NOT NULL NO BANCO DE DADOS)
            //SETA A MENSAGEM ABAIXO E RETORNA PARA A VIEW
            $this->session->setMensagem('error', 'Verifique os dados obrigat�rios.');
            redirect(base_url() . 'inadimplente/novo');
        }
    }

    function _update() {
        //C�DIGO DO INADIMPLENTE ENVIADO POR POST
        $cod = $this->input->post('cod', true);
		$nome_fantasia = $this->input->post('nome_fantasia', true);
		$pessoa = $this->input->post('pessoa', true);
		if ($nome_fantasia==false || $pessoa==false) {
			$this->inicore->setMensagem('error', 'Informe o tipo PESSOA e/ou o Nome', true);
		} else {
        	//MONTA A ARRAY COM OS DADOS NECESS�RIOS PARA O STORED PROCEDURE
            $dados = array(
                'ina_pessoa' => $this->input->post('pessoa', true),
                'ina_sexo' => $this->input->post('sexo', true),
                'ina_estado_civil' => $this->input->post('estado_civil', true),
                'ina_nome' => removeCE_Upper($this->input->post('nome_fantasia', true)),
                'ina_cpf_cnpj' => $this->input->post('cpf_cnpj', true),
                'ina_rg_ie' => $this->input->post('rg_ie', true),
                'ina_endereco' => removeCE_Upper($this->input->post('endereco', true)),
                'ina_bairro' => removeCE_Upper($this->input->post('bairro', true)),
                'ina_complemento' => removeCE_Upper($this->input->post('complemento', true)),
                'ina_cep' => $this->input->post('cep', true),
                'ina_cidade' => removeCE_Upper($this->input->post('cidade', true)),
                'ina_uf' => $this->input->post('uf', true),
                'ina_foneres' => $this->input->post('foneres', true),
                'ina_fonerec' => $this->input->post('fonerec', true),
                'ina_fonecom' => $this->input->post('fonecom', true),
                'ina_cel1' => $this->input->post('cel1', true),
                'ina_cel2' => $this->input->post('cel2', true),
                'ina_cel3' => $this->input->post('cel3', true),
                'ina_info' => removeCE_Upper($this->input->post('info', true)),
                'ina_conj_nome' => removeCE_Upper($this->input->post('conj_nome', true)),
                'ina_conj_fone' => $this->input->post('conj_fone', true),
                'ina_conj_endereco' => removeCE_Upper($this->input->post('conj_endereco', true)),
                'ina_conj_cidade' => utf8_encode(removeCE_Upper($this->input->post('conj_cidade', true))),
                'ina_conj_uf' => $this->input->post('conj_uf', true),
                'ina_conj_cod' => $this->input->post('conj_cod', true),
                'ina_pai_nome' => removeCE_Upper($this->input->post('pai_nome', true)),
                'ina_pai_fone' => $this->input->post('pai_fone', true),
                'ina_pai_endereco' => removeCE_Upper($this->input->post('pai_endereco', true)),
                'ina_pai_cidade' => utf8_encode(removeCE_Upper($this->input->post('pai_cidade', true))),
                'ina_pai_uf' => $this->input->post('pai_uf', true),
                'ina_pai_cod' => $this->input->post('pai_cod', true),
                'ina_mae_nome' => removeCE_Upper($this->input->post('mae_nome', true)),
                'ina_mae_fone' => $this->input->post('mae_fone', true),
                'ina_mae_endereco' => removeCE_Upper($this->input->post('mae_endereco', true)),
                'ina_mae_cidade' => utf8_encode(removeCE_Upper($this->input->post('mae_cidade', true))),
                'ina_mae_uf' => $this->input->post('mae_uf', true),
                'ina_mae_cod' => $this->input->post('mae_cod', true)
            );
        
	        //PREPARA A CONDI��O DO UPDATE
	        $this->db->where('ina_cod', $cod);
	        //SE ATUALIZAR OS DADOS COM SUCESSO
	        if ($this->db->update('inadimplentes', $dados)) {
	            //SETAR A MENSAGEM DE QUE FOI ATUALIZADO COM SUCESSO
	            auditar('a', $this->session->userdata('usucod'), 'inadimplentes', 'ALTERADO O INADIMPLENTE COD: ' . $cod);
	            $this->inicore->setMensagem('notice', 'Inadimplente ' . $this->input->post('nome_fantasia', true) . ' atualizado com sucesso', true);
	        }
	        //SE N�O OBTIVER SUCESSO NO UPDATE
	        else {
	            ////SETAR A MENSAGEM DE ERRO
	            $this->inicore->setMensagem('error', 'Erro ao atualizar o usu�rio, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
	        }
        }
        //REDIRECIONA PARA A VIEW "inadimplente_listar"
        redirect(base_url() . 'inadimplente/listar');
    }

    function _novo_parente() {
        if
        (
        /*         * *** DADOS OBRIGAT�RIOS ***** */
                ($this->input->post('parentesco_01_nome', true) != null)
        ) {
            //MONTA UMA ARRAY COM OS DADOS DO PARENTE
//            $dados = array
//                (
//                'inadimplentes_ina_cod' => $this->input->post('cod', true),
//                'par_nome' => removeCE_Upper($this->input->post('parentesco_01_nome', true)),
//                'par_parentesco' => $this->input->post('parentesco_01', true),
//                'par_fone1' => $this->input->post('parentesco_01_fone', true),
//                'par_fone2' => $this->input->post('parentesco_01_fone2', true),
//                'par_fone3' => $this->input->post('parentesco_01_fone3', true),
//                'par_fone4' => $this->input->post('parentesco_01_fone4', true),
//                'par_inad_cod' => $this->input->post('inad_cod', true) == '' ? '0' : $this->input->post('inad_cod', true)
//            );

            $ina_cod = ($this->input->post('inad_cod', true) == '' ? 0 : $this->input->post('inad_cod', true));

            $query = '';

            $query .= '\'' . $this->session->userdata('usucod') . '\',';
            $query .= '\'' . $this->input->post('cod', true) . '\',';
            $query .= '\'' . $this->input->post('parentesco_01', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('parentesco_01_nome', true)) . '\',';
            $query .= '\'' . $this->input->post('parentesco_01_fone', true) . '\',';
            $query .= '\'' . $this->input->post('parentesco_01_fone2', true) . '\',';
            $query .= '\'' . $this->input->post('parentesco_01_fone3', true) . '\',';
            $query .= '\'' . $this->input->post('parentesco_01_fone4', true) . '\',';
            $query .= '\'' . $ina_cod . '\'';

            $resultado = $this->db->query("CALL SP_CADASTRA_PARENTE(" . $query . ");");
        }
        //EXECUTA UM INSERT NO BANCO DE DADOS
        if (utf8_decode($resultado->row()->msg) == 'CADASTRADO') {
            //SETAR A MENSAGEM DE QUE FOI ATUALIZADO COM SUCESSO
//            auditar('c','parentes',$this->session->userdata('usucod'),'ADICIONADO PARARENTE NOME: '.$this->input->post('parentesco_01_nome', true).' PARA O INADIMPLENTE COD: '.$this->input->post('cod', true));
            $this->inicore->setMensagem('notice', 'Registro adicionado com sucesso', true);
        }
        //SE N�O OBTIVER SUCESSO NO UPDATE
        else {
            ////SETAR A MENSAGEM DE ERRO
            $this->inicore->setMensagem('error', 'Erro ao cadastrar o parente, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
        }
        //REDIRECIONA PARA A VIEW "inad_editar"
        redirect(base_url() . 'inadimplente/editar/cod:' . $this->input->post('cod', true));
    }

    function _atualizar_parente() {
        if
        (
        /*         * *** DADOS OBRIGAT�RIOS ***** */
                ($this->input->post('parentesco_01_nome', true) != null)
        ) {
            //MONTA UMA ARRAY COM OS DADOS DO PARENTE
            $dados = array
                (
                'par_nome' => removeCE_Upper($this->input->post('parentesco_01_nome', true)),
                'par_parentesco' => $this->input->post('parentesco_01', true),
                'par_fone1' => $this->input->post('parentesco_01_fone', true),
                'par_fone2' => $this->input->post('parentesco_01_fone2', true),
                'par_fone3' => $this->input->post('parentesco_01_fone3', true),
                'par_fone4' => $this->input->post('parentesco_01_fone4', true),
                'par_inad_cod' => $this->input->post('inad_cod', true) == '' ? '0' : $this->input->post('inad_cod', true)
            );
        }
        //EXECUTA UM UPDATE NO BANCO DE DADOS
        $this->db->where('par_cod', $this->input->post('cod', true));
        //SE ATUALIZAR OS DADOS COM SUCESSO
        if ($this->db->update('parentes', $dados)) {
            //SETAR A MENSAGEM DE QUE FOI ATUALIZADO COM SUCESSO
            auditar('a', 'parentes', $this->session->userdata('usucod'), 'ALTERADO PARARENTE NOME: ' . $this->input->post('parentesco_01_nome', true) . ' E COD: ' . $this->input->post('cod', true));
            $this->inicore->setMensagem('notice', 'Parente atualizado com sucesso', true);
        }
        //SE N�O OBTIVER SUCESSO NO UPDATE
        else {
            ////SETAR A MENSAGEM DE ERRO
            $this->inicore->setMensagem('error', 'Erro ao atualizar o parente, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
        }
        //REDIRECIONA PARA A VIEW "inad_editar"
        redirect(base_url() . 'inadimplente/editar/cod:' . $this->input->post('codIna', true));
    }

    function _cadBem() {
        if
        (
        /*         * *** DADOS OBRIGAT�RIOS ***** */
                ($this->input->post('tipo', true) != null) &&
                ($this->input->post('titulo', true) != null)
        ) {
//            $dados = array
//                (// MONTA UMA ARRAY COM OS DADOS DO BEM
//                'inadimplentes_ina_cod' => $this->input->post('cod', true),
//                'ben_tipo' => $this->input->post('tipo', true),
//                'ben_titulo' => removeCE_Upper($this->input->post('titulo', true)),
//                'ben_detalhes' => removeCE_Upper($this->input->post('detalhes', true))
//            );

            $query = '';

            $query .= '\'' . $this->session->userdata('usucod') . '\',';
            $query .= '\'' . $this->input->post('cod', true) . '\',';
            $query .= '\'' . $this->input->post('tipo', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('titulo', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('detalhes', true)) . '\'';

            $resultado = $this->db->query("CALL SP_CADASTRA_BEM(" . $query . ");");
        }
//        // EXECUTA UM INSERT NO BANCO
//        if ($this->db->insert('bens', $dados)) {
//            //SETAR A MENSAGEM DE QUE FOI ATUALIZADO COM SUCESSO
//            auditar('c','bens',$this->session->userdata('usucod'),'CADASTRADO NOVO BEM: '.removeCE_Upper($this->input->post('titulo', true)).' PARA O INADIMPLEMTE COD: '.$this->input->post('cod', true));
//            $this->inicore->setMensagem('notice', 'Registro adicionado com sucesso', true);
//        }
//
        //SE A SP RETORNAR 'CADASTRADO'
        if (utf8_decode($resultado->row()->msg) == 'CADASTRADO') {
            $this->inicore->setMensagem('notice', 'Registro adicionado com sucesso', true);
        }
        //SE N�O OBTIVER SUCESSO NO UPDATE
        else {
            ////SETAR A MENSAGEM DE ERRO
            $this->inicore->setMensagem('error', 'Erro ao cadastrar o bem, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema' . utf8_decode($resultado->row()->msg), true);
        }

        //REDIRECIONA PARA A VIEW "EDITAR INADIMPLENTE"
        redirect(base_url() . 'inadimplente/editar/cod:' . $this->input->post('cod', true));
    }

    function _atualizaBem() {
        if
        (
        /*         * *** DADOS OBRIGAT�RIOS ***** */
                ($this->input->post('tipo', true) != null) &&
                ($this->input->post('titulo', true) != null)
        ) {

            $dados = array
                (//MONTA UMA ARRAY COM OS DADOS QUE SER�O USADOS NO UPDATE
                'inadimplentes_ina_cod' => $this->input->post('cod', true),
                'ben_tipo' => $this->input->post('tipo', true),
                'ben_titulo' => removeCE_Upper($this->input->post('titulo', true)),
                'ben_detalhes' => removeCE_Upper($this->input->post('detalhes', true))
            );
        }
        $cod = $this->input->post('bem_cod', true);
        //PREPARA A CONDI��O DO UPDATE
        $this->db->where('ben_cod', $cod);
        //SE ATUALIZAR OS DADOS COM SUCESSO
        if ($this->db->update('bens', $dados)) {
            //SETAR A MENSAGEM DE QUE FOI ATUALIZADO COM SUCESSO
            auditar('a', $this->session->userdata('usucod'), 'bens', 'ALTERADO O BEM: ' . removeCE_Upper($this->input->post('titulo', true)) . ' COM O COD: ' . $cod);
            $this->inicore->setMensagem('notice', 'Bem atualizado com sucesso', true);
        }
        //SE N�O OBTIVER SUCESSO NO UPDATE
        else {
            ////SETAR A MENSAGEM DE ERRO
            $this->inicore->setMensagem('error', 'Erro ao atualizar o bem, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
        }
        //REDIRECIONA PARA A VIEW "credor_listar"
        redirect(base_url() . 'inadimplente/editar/cod:' . $this->input->post('cod', true));
    }

    function _visualizarImportacaoInad() {
        $inaCod = get('inaCod');
        $dadosImportados = "";

        if (is_numeric($inaCod)) {
            $dadosImportados = $this->md->getDadosImportInade($inaCod);
            if (sizeof($dadosImportados) == 0) {
                die("Nada Encontrado");
            }
        } else {
            die("comportamento inesperado");
        }

        foreach ($dadosImportados as $dados) {
            echo "<b>Nome: </b>" . $dados->ina_nome . "<br/>";
            echo "<b>CPF/CNPJ: </b>" . $dados->ina_cpf_cnpj . "<br/>";
            echo "<b>RG: </b>" . $dados->ina_rg_ie . "<br/>";
            echo "<b>Pessoa: </b>" . $dados->pessoa . "<br/>";
            echo "<b>Sexo: </b>" . $dados->sexo . "<br/>";
            echo "<b>Estado Civil: </b>" . $dados->estado_civil . "<br/>";
            echo "<b>Endere�o: </b>" . $dados->ina_endereco . "<br/>";
            echo "<b>Bairro: </b>" . $dados->ina_bairro . "<br/>";
            echo "<b>Complemento: </b>" . $dados->ina_complemento . "<br/>";
            echo "<b>CEP: </b>" . $dados->ina_cep . "<br/>";
            echo "<b>Cidade: </b>" . $dados->ina_cidade . "<br/>";
            echo "<b>UF: </b>" . $dados->ina_uf . "<br/>";
            echo "<b>Fone Residencial: </b>" . $dados->ina_foneres . "<br/>";
            echo "<b>Fone Recado: </b>" . $dados->ina_fonerec . "<br/>";
            echo "<b>Fone Comercial: </b>" . $dados->ina_fonecom . "<br/>";
            echo "<b>Cel 1: </b>" . $dados->ina_cel1 . "<br/>";
            echo "<b>Cel 2: </b>" . $dados->ina_cel2 . "<br/>";
            echo "<b>Cel 3: </b>" . $dados->ina_cel3 . "<br/>";
            echo "<b>Informa��es: </b>" . $dados->ina_info . "<br/>";
            echo "<b>C�njuge Nome: </b>" . $dados->ina_conj_nome . "<br/>";
            echo "<b>C�njuge Fone: </b>" . $dados->ina_conj_fone . "<br/>";
            echo "<b>C�njuge Endere�o: </b>" . $dados->ina_conj_endereco . "<br/>";
            echo "<b>C�njuge Cidade: </b>" . $dados->ina_conj_cidade . "<br/>";
            echo "<b>C�njuge UF: </b>" . $dados->ina_conj_uf . "<br/>";
            echo "<b>Nome Pai: </b>" . $dados->ina_pai_nome . "<br/>";
            echo "<b>Fone Pai: </b>" . $dados->ina_pai_fone . "<br/>";
            echo "<b>Endere�o Pai: </b>" . $dados->ina_pai_endereco . "<br/>";
            echo "<b>Cidade Pai: </b>" . $dados->ina_pai_cidade . "<br/>";
            echo "<b>UF Pai: </b>" . $dados->ina_pai_uf . "<br/>";
            echo "<b>Nome M�e: </b>" . $dados->ina_mae_nome . "<br/>";
            echo "<b>Fone M�e: </b>" . $dados->ina_mae_fone . "<br/>";
            echo "<b>Endere�o M�e: </b>" . $dados->ina_mae_endereco . "<br/>";
            echo "<b>Cidade M�e: </b>" . $dados->ina_mae_cidade . "<br/>";
            echo "<b>UF M�e: </b>" . $dados->ina_mae_uf . "<br/>";
            echo "<b>Duplicidade Inadimplente: </b>" . $dados->ina_duplicado . "<br/>";
            echo "<b>Inadimplente C�digo Original: </b>" . $dados->ina_inad_original_cod . "<br/>";
        }

        die();
    }

}