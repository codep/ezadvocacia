<?php

class Cadbem extends Controller {

	function Cadbem()
	{
            parent::Controller();
            

	}
	
	function _remap($link)
	{
            
            $this->data['title']="Recupera :: Cadastro de Bens";

            $this->inicore->addcss(array('reset','style','style_fixed','colors/blue')); // CSS HOME

            $this->load->helper("funcoes_helper");

            $this->inicore->addjs(array('jquery-1.4.2.min.js','jquery-ui-1.8.custom.min.js','jquery.ui.selectmenu.js','jquery.flot.min.js','tiny_mce/jquery.tinymce.js','smooth.js','smooth.menu.js','smooth.table.js','smooth.form.js','smooth.dialog.js','smooth.autocomplete.js','plugin/jquery.maskedinput'));
//------------------------------------------------------------------------------
            include 'testar_conexao.php';
//------------------------------------------------------------------------------
            //IR PARA O RESPECTIVO FLUXO DA URL
            if($link == "novo") $this->_novo();
            if($link == "incluir") $this->_incluir();

	}

        function _novo()
        {
            $this->inicore->loadSidebar();
            // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
            $this->inicore->loadview('cad_bem',$this->data);
        }

        function _incluir()
        {
            $this->inicore->loadSidebar();
            // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
            $this->inicore->loadview('ag_novatarefa',$this->data);
        }
}
