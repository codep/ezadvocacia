<?php

class Impressao extends Controller {

    function Impressao() {
        parent::Controller();
    }

    function _remap($link) {



        $this->inicore->addcss(array('impressao')); // CSS HOME
        $this->load->model('Impressoes_model', 'md');
        $this->load->helper("funcoes_helper");
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        //IR PARA O RESPECTIVO FLUXO DA URL
        if ($link == "recibo") {
            //FUN��O RESPONSAVEL POR IMPRIMIR O RECIBO NA TELA
            $this->_recibo();
        } else if ($link == "incluir") {
            $this->_incluir();
        } else if ($link == "recibo2") {
            $this->_recibo2();
        }
    }

    function _recibo() {
        $this->data['title'] = "Recupera :: Recibo";
        // PEGA O CODIGO QUE VEM POR GET
        $cod = get('cod');
        //BUSCA NO BANCO OS DADOS REFERENTES AOS RECIBOS
        $this->data['recDados'] = $this->md->getRecDados($cod);
        
        $parcelas = explode('-',$this->data['recDados']->reb_parcelas);
        
        if(isset ($parcelas) && $parcelas != ''){
            $filtroParcelas = 'WHERE PA.paa_cod = '.$parcelas[0];
            
            foreach ($parcelas as $parcela){
                if ($parcela != ''){
                    $filtroParcelas .= ' OR PA.paa_cod = '.$parcela;
                }
            }
            $this->data['parcelasInfo'] = $this->md->getParcelasInfo($filtroParcelas);
        }
        if ((isset($this->data['recDados'])) && (sizeof($this->data['recDados']) != 0)) {
            //CARREGA A VIEW COM O LAYOUT DE IMPRESS�O DOS RECIBOS
            $this->inicore->loadview('imp_recibo', $this->data);
        }else{
            $this->inicore->setMensagem('error', 'Erro ao acessar o recibo, tente novamente');
            redirect(getBackURL());
        }
    }

    function _incluir() {
        
    }
    
    function _recibo2() {
        $this->data['title'] = "Recupera :: Recibo";
        $this->data['segundavia'] = '1';
        // PEGA O CODIGO QUE VEM POR GET
        $cod = get('cod');
        //BUSCA NO BANCO OS DADOS REFERENTES AOS RECIBOS
        $this->data['recDados'] = $this->md->getRecDados($cod);
        
        $parcelas = explode('-',$this->data['recDados']->reb_parcelas);
        
        if(isset ($parcelas) && $parcelas != ''){
            $filtroParcelas = 'WHERE PA.paa_cod = '.$parcelas[0];
            
            foreach ($parcelas as $parcela){
                if ($parcela != ''){
                    $filtroParcelas .= ' OR PA.paa_cod = '.$parcela;
                }
            }
            $this->data['parcelasInfo'] = $this->md->getParcelasInfo($filtroParcelas);
        }
        if ((isset($this->data['recDados'])) && (sizeof($this->data['recDados']) != 0)) {
            //CARREGA A VIEW COM O LAYOUT DE IMPRESS�O DOS RECIBOS
            $this->inicore->loadview('imp_recibo', $this->data);
        }else{
            $this->inicore->setMensagem('error', 'Erro ao acessar o recibo, tente novamente');
            redirect(getBackURL());
        }
    }

}
