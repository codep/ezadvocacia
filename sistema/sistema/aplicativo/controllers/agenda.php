<?php

class Agenda extends Controller {

    function Agenda() {
        parent::Controller();
    }

    function _remap($link) {

        $this->data['title'] = "Recupera :: Agenda";

        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME

        $this->load->helper("funcoes_helper");

        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'jquery.maxlength.js','plugin/jquery.maskedinput'));
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        //IR PARA O RESPECTIVO FLUXO DA URL
        if ($link == "listar"){
            $this->_listar();
        }else if ($link == "incluir"){
            $this->_incluir();
        }else if ($link == "novaTarefa"){
            $this->_novaTarefa();
        }else if ($link == "excluirTarefa"){
            $this->_excluirTarefa();
        }
    }

    function _listar() {//lista as tarefas usadas na view ag_listar
        /* ----------------------------------------------------- */
        //DECLARACAO DE VARIAVEIS
        $codUsuarioSessao = $this->session->userdata('usucod'); //vari�vel usada para pegar todas as tarefas de hoje do usu�rio e todas as tarefas do usu�rio.
        $dataDeHoje = date("Y-m-d", time()); //vair�vel usada para pegar todas as tarefas de hoje
        $mesAtual = date("m", time()); //vari�vel usada para pegar todas as tarefas do m�s
        $anoAtual = date("Y"); //vari�vel usada para pegar todas as tarefas do m�s
        $primeiroDiaDaSemana = getPDS(); //m�todo do helper
        $ultimoDiaDaSemana = getUDS(); //m�todo do helper
        /* ------------------------------------------------------ */

        $this->inicore->loadSidebar(); //menu vertical da esquerda

        $this->load->model('tarefa_model'); //carregando o model
        /* -----VARI�VEIS QUE SER�O USADAS NA VIEW ag_listar----- */
        $this->data['tarefasDeHojeUsuSessao'] = $this->tarefa_model->getMinhasTarefasDeHoje($codUsuarioSessao, $dataDeHoje);
        $this->data['tarefasDaSemanaUsuSessao'] = $this->tarefa_model->getMinhasTarefasDaSemana($codUsuarioSessao, $primeiroDiaDaSemana, $ultimoDiaDaSemana);
        $this->data['tarefasDoMesUsuSessao'] = $this->tarefa_model->getMinhasTarefasDoMes($codUsuarioSessao, $mesAtual, $anoAtual);
        $this->data['todasTarefasUsuSessao'] = $this->tarefa_model->getTodasMinhasTarefas($codUsuarioSessao);
        $this->data['listaDeTarefas'] = $this->tarefa_model->getTarefasTodosUsuarios();
        /* ----------------------------------------------- */

        // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
        $this->inicore->loadview('ag_listar', $this->data);
    }

    function _incluir() {//inclui os usu�rio dos sistema no campo respons�vel do form nova tarefa
        $this->load->model('tarefa_model'); //carega o model
        //pega todos os usu�rios do sistema  para jogar na view (no campo respons�vel).
        $this->data['usuUsuarioSis'] = $this->tarefa_model->getUsuarios('usu_ativo=1','usu_usuario_sis'); //chama o m�todo do model

        $this->data['usuSessao'] = $this->session->userdata('usuwho');
        $this->inicore->loadSidebar(); //menu vertical da esquerda
        // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
        $this->inicore->loadview('ag_novatarefa', $this->data);
    }

    function _novaTarefa() {//inlclui novas tarefas
        /* Esse m�todo pega os dados que vem do formul�rio de nova tarefa, trata a data de dd/mm/aaaa para aaaa-mm-dd
         * e grava a tarefa no banco de dados */
        $this->load->library('form_validation');
        $dados = array(
            array(
                'field' => 'data',
                'label' => 'Data',
                'rules' => 'required'
            ),
            array(
                'field' => 'titulo',
                'label' => 'T�tulo',
                'rules' => 'required'
            ),
            array(
                'field' => 'responsavel',
                'label' => 'Respons�vel',
                'rules' => 'required'
            ),
            array(
                'field' => 'descricao',
                'label' => 'Descri��o',
                'rules' => 'required'
            ));
        $this->form_validation->set_message('required', "O campo %s � obrigat�rio");
        $this->form_validation->set_rules($dados);

        if ($this->form_validation->run() == FALSE) {

            $mensagem = validation_errors();
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'agenda/incluir');
        } else {

            /* Pega a data que vem do form nesse formato, dd/mm/aaaa e passa para aaaa-mm-dd */
            $data = $this->input->post('data'); //recebe a data que o usu�rio digita no campo data do formul�rio de nova tarefa
            $dia = substr($data, 0, -8); //pega s� o dia da data
            $mes = substr($data, 3, -5); //pega s� o m�s da data
            $ano = substr($data, -4); //pega s� o ano data
            $tarefaAgendadaPara = $ano . '-' . $mes . '-' . $dia; //atribui a uma vari�vel no formato aaaa-mm-dd
            /* ----------------------------------------- */

            /* ---------------PEGA O RESTO DOS DADOS DO FORMULARIO NOVA TAREFA------------------- */
            $dataCriacaoTarefa = date("Y-m-d", time()); //data atual, pega do SO
            $nomeUsuarioSessao = $this->session->userdata('usuwho'); //criador da tarefa(nome do usu�rio da sess�o)
            $codResponsavelPelaTarefa = $this->input->post('responsavel'); //campo respons�vel do fomul�rio nova tarefa(pega s� o c�d do respons�vel
            /* ----------------------------------------------------------------------------------------- */
            /* -----TODOS OS DADOS DA NOVA TAREFA----- */
            $dados = array(
                'usuarios_usu_cod' => $this->input->post('responsavel'), //cod respons�vel pela tarefa
                'tar_criacao' => ($dataCriacaoTarefa), //pega do sistema operacional
                'tar_agendada' => ($tarefaAgendadaPara), //data que o usu�rio digitou (j� formatada aaaa-mm-dd)
                'tar_titulo' => utf8_encode(removeCE_Upper($this->input->post('titulo'))), //titulo da tarefa
                'tar_criador' => ($nomeUsuarioSessao), //criador da tarefa
                'tar_descricao' => utf8_encode(removeCE_Upper($this->input->post('descricao'))), //descricao da tarefa
                    /* ----------------------------------------------- */
            );
            /* -----GRAVANDO A TAREFA NO BANCO */
            $this->load->model('tarefa_model'); //carrega o model
            $this->tarefa_model->setNovaTarefa($dados); //chama o m�todo seNovaTarefa do model
            $this->inicore->setMensagem('success', 'Tarefa cadastrada com sucesso',true);

            redirect(base_url() . 'agenda/listar'); //redireciona para lista de tarefas
            /* ------------------------------------ */
        }
    }

    function _excluirTarefa() {
        /* Exclui a tarefa pelo seu c�digo */
        $codTarefa = get('cod');

        if(is_numeric($codTarefa))//se $codTarefa for do tipo num�rico
            {
            
            $this->load->model('tarefa_model'); //carrega o model
            $row = $this->tarefa_model->excluirTarefa($codTarefa); //chama o m�todo excluirTarefa do model que retorna o n�mero de linhas afetadas
                if($row != 0)//se o n�mero de linhas afetadas for diferente de 0 d� uma mensagem de sucesso.
                    {
                    $this->inicore->setMensagem('notice', 'Tarefa excluida com sucesso',true);
                    redirect(base_url() . 'agenda/listar'); //redireciona para lista de tarefas
                    }
                        else// se o n�mero de linhas afetadas for igual a 0 d� uma mensagem de erro.
                           {
                           $this->inicore->setMensagem('error', 'Erro ao excluir tarefa!',true);
                           redirect(base_url() . 'agenda/listar'); //redireciona para lista de tarefas
                           }
            }
            else// se $codTarefa n�o for do tipo num�rico d� uma mensagem de erro.
                {
                $this->inicore->setMensagem('error', 'Opera��o n�o autorizada!',true);
                redirect(base_url() . 'agenda/listar'); //redireciona para lista de tarefas
                }
    }

}

