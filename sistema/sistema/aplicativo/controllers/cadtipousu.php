<?php

class Cadtipousu extends Controller {

    function Cadtipousu() {
        parent::Controller();
    }

    function _remap($link) {
        //Adiciona o titulo da p�gina
        $this->data['title'] = "Recupera :: Grupos de usu�rios";
        //carrega o Css
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        //carrega o helper de fun��es que ser� usado no script
        $this->load->helper("funcoes_helper");
        //carrega o model  e passa md como o nome que ser� usado para referencia-lo
        $this->load->model('grupo_usuarios_model', 'md');
        //carrego os java scripts
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput'));
        //escolhendo o menu que ficara selecionado;
        $this->session->set_userdata('menusel', '1');
        //gera as permiss�es de acesso do susario que ser�o usadas para saber se
        //o mesmo tem acesso a determinados recursos
        $menus1 = explode(",", $this->session->userdata('menu1'));
        $menus2 = explode(",", $this->session->userdata('menu2'));
        $menus3 = explode(",", $this->session->userdata('menu3'));
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        //Redireciona o fluxo de execu��o de acordom com o passado na URL
        //pega a fun��o que esta sendo chamada pela URL
        if ($link == "listar") {
            //testa para saber se o usu�rio tem permiss�o de acesso a aquela fun��o
            if (array_search('8', $menus2, TRUE) != '') {
                //fun��o responsavel por listar todos os tipos de usu�rios
                $this->_listar();
            } else {//se n�o tiver permiss�od e acesso redireciona para a home e exibe mensagem
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "novo") {
            if (array_search('8', $menus2, TRUE) != '') {
                //fun��o responsavel por carregar a tela de novo tipod e usuario
                $this->_novo();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "incluir") {
            if (array_search('8', $menus2, TRUE) != '') {
                //fun��o responsavel por incluir no banco as informa��es passadas pelo usu�rio
                $this->_incluir();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "editar") {
            if (array_search('8', $menus2, TRUE) != '') {
                //fun��o que abre a tela de edi��o de usu�rio
                $this->_editar();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "update") {
            if (array_search('8', $menus2, TRUE) != '') {
                //fun��o que executa um update no usu�rio escolhido
                $this->_update();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "excluir") {
            if (array_search('8', $menus2, TRUE) != '') {
                // fun��o que chama a tela de exlcus�o de usuario
                $this->_excluir();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "delete") {
            if (array_search('8', $menus2, TRUE) != '') {
                // fun��o que executa a exclus�o no banco de dados
                $this->_delete();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else {
            //se n�o for encontrado a fun��o solicitada, redireciona para home informando o ocorrido
            $this->inicore->setMensagem('warning', 'Recurso ainda n�o implementado, entre em contato com o administrador do sistema');
            redirect(base_url() . 'home');
        }
    }

    function _listar() {
        $this->inicore->loadSidebar();
        $this->data['grupos'] = $this->md->getGrupos();
        $this->inicore->loadview('gru_listar', $this->data);
    }

    function _novo() {
        $this->inicore->loadSidebar();
        $this->inicore->loadview('cad_tipousuario', $this->data);
    }

    function _delete() {
        //pega a senha que o sus�rio informar e criptogra-a seguindo regra pre estabelecida
        $senha = sha1('senhamaster+' . $this->input->post('senha', true));
        //seleciona a senha original no banco de dados
        $sql = "SELECT cod, senha FROM senha_master";
        $query = $this->db->query($sql);
        //testa para ver se a senha informada � igual a original
        if ($query->row()->senha == $senha) {
            //se for igual deleta o grupo de usu�rios em quest�i
            $this->db->where($this->input->post('chave', true), $this->input->post('codigo', true));
            if ($this->db->delete($this->input->post('tabela', true))) {
                $this->inicore->setMensagem('notice', 'Grupo de usu�rios excluido com sucesso', true);
            } else {
                $this->inicore->setMensagem('error', 'Erro ao tentar excluir o grupo de usu�rios, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
            }
            redirect($this->input->post('voltar', true));
        } else {//caso a senha esteja errada, monta novamente a tela de confirma��o de exclus�o
            $this->data['title'] = "Recupera :: Confrimar exclus�o";
            $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
            $this->inicore->addjs(array('smooth.form.js'));
            $this->data['deletado'] = 'Grupo de usu�rios';
            $this->data['codigo'] = $this->input->post('codigo', true);
            $this->data['tabela'] = 'grupo_usuarios';
            $this->data['chave'] = 'gru_cod';
            $this->data['acaocancel'] = 'cadtipousu/listar';
            $this->data['acaook'] = 'cadtipousu/delete';
            $this->inicore->setMensagem('error', 'SENHA incorreta, favor verificar e digitar novamente', true);
            $this->inicore->loadview('conf_exclusao', $this->data);
        }
    }

    function _excluir() {
        $cod = get('cod');
        /* A fun��o excluir possui uma tela de confirma��o de exclus�o que
         * serve para qualque exclus�o no sistema, para que esta tela funcione
         * corretamente deve ser passados algumas vari�veis que ser�o utilizadas
         * na constru��o da p�gina, as vari�veis necess�rias s�o:
         * $deletado-----------------------O que est� sendo deletado
         * $codigo-------------------------c�digo do que est� sendo deletado
         * $tabela-------------------------Tabela que ter� o item excluido
         * $chave--------------------------Chave primaria da tabela para o delete
         * $acaocancel---------------------A��o executada quando clicar em cancelar
         * $acaook-------------------------A��o executada quando clicar em Apagar
         */
        $this->data['title'] = "Recupera :: Confrimar exclus�o";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->inicore->addjs(array('smooth.form.js'));
        $this->data['deletado'] = 'Grupo de usu�rios';
        $this->data['codigo'] = $cod;
        $this->data['tabela'] = 'grupo_usuarios';
        $this->data['chave'] = 'gru_cod';
        $this->data['acaocancel'] = 'cadtipousu/listar';
        $this->data['acaook'] = 'cadtipousu/delete';
        $this->inicore->loadview('conf_exclusao', $this->data);
    }

    function _editar() {
        $this->inicore->loadSidebar();
        //pega as informa��es do grupo que sera editado no banco
        $cod = get('cod');
        $dados = $this->data['dados'] = $this->md->getGrupoInfo($cod);
        //gera uma array com as permiss�es do usu�rio que ser�o utilizadas na view
        $this->data['menus1'] = explode(",", $dados->gru_per_menu_nivel1);
        $this->data['menus2'] = explode(",", $dados->gru_per_menu_nivel2);
        $this->data['menus3'] = explode(",", $dados->gru_per_menu_nivel3);
        $this->inicore->loadview('edi_tipousuario', $this->data);
    }

    function _update() {
        if ($this->input->post('gruponome', true) == null) {
            //dados obrigat�rios
            $this->inicore->setMensagem('error', 'Dados insuficientes para o cadastro', true);
            redirect(base_url() . 'cadrepasse/novo');
        }
        //pega os dados do post
        $cod = $this->input->post('grupocod', true);
        $nome = removeCE_Upper(trim($this->input->post('gruponome', true)));
        //cria strings que ser�o utilizadas para guardas as permiss�es do usu�rio
        $menus1 = '';
        $menus2 = '';
        $menus3 = '';
        //MONTA AS STRINGS DE PERMISS�O DE ACESSO DE ACORDO COM O QUE FOI PASSADO NA TELA DE USU�RIO
        /*          Menus Principais (Cadastros, Gerenciamento, Cobran�as ADM, Cobran�as
         *          Judiciais, Pesquisar) */
        $this->input->post('cadastro', true) == 1 ? $menus1.=',1' : '';
        $this->input->post('gerenciamento', true) == 1 ? $menus1.=',9' : '';
        $this->input->post('cobAdm', true) == 1 ? $menus1.=',17' : '';
        $this->input->post('cobJud', true) == 1 ? $menus1.=',24' : '';
        $this->input->post('relatorios', true) == 1 ? $menus1.=',31' : '';
        $this->input->post('pesquisa', true) == 1 ? $menus1.=',27' : '';
        /*          Menus Secundarios (Nova d�vida, Novo inadimplente,Novo credor,Novo
         *          Bem,Tipo de opera��o,Usu�rios,Grupo de usu�rios,Repasse,Remanejar
         *          credor,Remanejar inadimplente,Listar,Minhas cobran�as,Listar todas,
         *          Recebimentos,Listar,Acordos Extintos,Cobran�as,Inadimplentes,Credores */
        $this->input->post('cad_divida', true) == 1 ? $menus2.=',2' : '';
        $this->input->post('cad_inad', true) == 1 ? $menus2.=',3' : '';
        $this->input->post('cad_cred', true) == 1 ? $menus2.=',4' : '';
        $this->input->post('cad_bem', true) == 1 ? $menus2.=',5' : '';
        $this->input->post('cad_opr', true) == 1 ? $menus2.=',6' : '';
        $this->input->post('cad_usu', true) == 1 ? $menus2.=',7' : '';
        $this->input->post('cad_gru', true) == 1 ? $menus2.=',8' : '';
        $this->input->post('listar_imp', true) == 1 ? $menus2.=',38' : '';
        $this->input->post('cartas', true) == 1 ? $menus2.=',43' : '';
        $this->input->post('ger_repasse', true) == 1 ? $menus2.=',10' : '';
        $this->input->post('ger_remCred', true) == 1 ? $menus2.=',11' : '';
        $this->input->post('ger_remInad', true) == 1 ? $menus2.=',12' : '';
        $this->input->post('ger_listar', true) == 1 ? $menus2.=',13' : '';
        $this->input->post('ger_devolucao', true) == 1 ? $menus2.=',44' : '';
        $this->input->post('cobAdm_minhas', true) == 1 ? $menus2.=',18' : '';
        $this->input->post('cobAdm_todas', true) == 1 ? $menus2.=',19' : '';
        $this->input->post('cobAdm_receb', true) == 1 ? $menus2.=',20' : '';
        $this->input->post('cobJud_listar', true) == 1 ? $menus2.=',25' : '';
        $this->input->post('cobJud_acordosExt', true) == 1 ? $menus2.=',26' : '';
		$this->input->post('cobJud_importar', true) == 1 ? $menus2.=',47' : '';
        $this->input->post('relatAdm', true) == 1 ? $menus2.=',32' : '';
        $this->input->post('relatJud', true) == 1 ? $menus2.=',33' : '';
        $this->input->post('relatRo', true) == 1 ? $menus2.=',34' : '';
        $this->input->post('relatGer', true) == 1 ? $menus2.=',35' : '';
        $this->input->post('relatPC', true) == 1 ? $menus2.=',36' : '';
        $this->input->post('relatAces', true) == 1 ? $menus2.=',37' : '';
        $this->input->post('relatAcesUsu', true) == 1 ? $menus2.=',45' : '';
        $this->input->post('pesquisa_cob', true) == 1 ? $menus2.=',28' : '';
        $this->input->post('pesquisa_inad', true) == 1 ? $menus2.=',29' : '';
        $this->input->post('pesquisa_cred', true) == 1 ? $menus2.=',30' : '';
		$this->input->post('pesquisa_juri', true) == 1 ? $menus2.=',46' : '';
		$this->input->post('pesquisa_fiscal', true) == 1 ? $menus2.=',48' : '';
        $this->input->post('relatResultContat', true) == 1 ? $menus2.=',41' : '';
        $this->input->post('relatImportacao', true) == 1 ? $menus2.=',39' : '';
        $this->input->post('ger_backup', true) == 1 ? $menus2.=',42' : '';
        /*          Menus Terciarios(Todas cobran�as,Todos inadimplentes,Todos credores,
          Meus,Todos,Previs�es */
        $this->input->post('ger_list_cob', true) == 1 ? $menus3.=',14' : '';
        $this->input->post('ger_list_inad', true) == 1 ? $menus3.=',15' : '';
        $this->input->post('ger_list_cred', true) == 1 ? $menus3.=',16' : '';
        $this->input->post('cobAdm_receb_meus', true) == 1 ? $menus3.=',21' : '';
        $this->input->post('cobAdm_receb_todos', true) == 1 ? $menus3.=',22' : '';
        $this->input->post('cobAdm_receb_prev', true) == 1 ? $menus3.=',23' : '';
        //monta uma array com os dados tratados acima
        $dados = array(
            'gru_titulo' => utf8_encode($nome),
            'gru_per_menu_nivel1' => $menus1,
            'gru_per_menu_nivel2' => $menus2,
            'gru_per_menu_nivel3' => $menus3
        );
        //executa um update no banco e exibe mensagem informando se deu tudo certo
        $this->db->where('gru_cod', $cod);
        if ($this->db->update('grupo_usuarios', $dados)) {
            $this->inicore->setMensagem('notice', $this->input->post('gruponome', true) . ' atualizado com sucesso', true);
        } else {
            $this->inicore->setMensagem('error', 'Erro ao tentar atualizar o grupo de usu�rios, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
        }
        redirect(base_url() . 'cadtipousu/listar');
    }

    function _incluir() {
        //pega os dados d post
        if ($this->input->post('gruponome', true) == null) {
            //dados obrigat�rios
            $this->inicore->setMensagem('error', 'Dados insuficientes para o cadastro', true);
            redirect(base_url() . 'cadrepasse/novo');
        }
        $nome = removeCE_Upper(trim($this->input->post('gruponome', true)));
        //cria 3 strings para guardar as informa��es de permiss�o de usu�rios
        $menus1 = '';
        $menus2 = '';
        $menus3 = '';
        //MONTA AS STRINGS DE PERMISS�O DE ACORDO COM O INFORMADO PELO USU�RIO NO POST
        /*          Menus Principais (Cadastros, Gerenciamento, Cobran�as ADM, Cobran�as
         *          Judiciais, Pesquisar) */
        $this->input->post('cadastro', true) == 1 ? $menus1.=',1' : '';
        $this->input->post('gerenciamento', true) == 1 ? $menus1.=',9' : '';
        $this->input->post('cobAdm', true) == 1 ? $menus1.=',17' : '';
        $this->input->post('cobJud', true) == 1 ? $menus1.=',24' : '';
        $this->input->post('relatorios', true) == 1 ? $menus1.=',31' : '';
        $this->input->post('pesquisa', true) == 1 ? $menus1.=',27' : '';
        /*          Menus Secundarios (Nova d�vida, Novo inadimplente,Novo credor,Novo
         *          Bem,Tipo de opera��o,Usu�rios,Grupo de usu�rios,Repasse,Remanejar
         *          credor,Remanejar inadimplente,Listar,Minhas cobran�as,Listar todas,
         *          Recebimentos,Listar,Acordos Extintos,Cobran�as,Inadimplentes,Credores */
        $this->input->post('cad_divida', true) == 1 ? $menus2.=',2' : '';
        $this->input->post('cad_inad', true) == 1 ? $menus2.=',3' : '';
        $this->input->post('cad_cred', true) == 1 ? $menus2.=',4' : '';
        $this->input->post('cad_bem', true) == 1 ? $menus2.=',5' : '';
        $this->input->post('cad_opr', true) == 1 ? $menus2.=',6' : '';
        $this->input->post('cad_usu', true) == 1 ? $menus2.=',7' : '';
        $this->input->post('cad_gru', true) == 1 ? $menus2.=',8' : '';
        $this->input->post('listar_imp', true) == 1 ? $menus2.=',38' : '';
        $this->input->post('cartas', true) == 1 ? $menus2.=',43' : '';
        $this->input->post('ger_repasse', true) == 1 ? $menus2.=',10' : '';
        $this->input->post('ger_remCred', true) == 1 ? $menus2.=',11' : '';
        $this->input->post('ger_remInad', true) == 1 ? $menus2.=',12' : '';
        $this->input->post('ger_listar', true) == 1 ? $menus2.=',13' : '';
        $this->input->post('ger_devolucao', true) == 1 ? $menus2.=',44' : '';
        $this->input->post('cobAdm_minhas', true) == 1 ? $menus2.=',18' : '';
        $this->input->post('cobAdm_todas', true) == 1 ? $menus2.=',19' : '';
        $this->input->post('cobAdm_receb', true) == 1 ? $menus2.=',20' : '';
        $this->input->post('cobJud_listar', true) == 1 ? $menus2.=',25' : '';
        $this->input->post('cobJud_acordosExt', true) == 1 ? $menus2.=',26' : '';
		$this->input->post('cobJud_importar', true) == 1 ? $menus2.=',47' : '';
        $this->input->post('relatAdm', true) == 1 ? $menus2.=',32' : '';
        $this->input->post('relatJud', true) == 1 ? $menus2.=',33' : '';
        $this->input->post('relatRo', true) == 1 ? $menus2.=',34' : '';
        $this->input->post('relatGer', true) == 1 ? $menus2.=',35' : '';
        $this->input->post('relatPC', true) == 1 ? $menus2.=',36' : '';
        $this->input->post('relatAces', true) == 1 ? $menus2.=',37' : '';
        $this->input->post('relatAcesUsu', true) == 1 ? $menus2.=',45' : '';
        $this->input->post('pesquisa_cob', true) == 1 ? $menus2.=',28' : '';
        $this->input->post('pesquisa_inad', true) == 1 ? $menus2.=',29' : '';
        $this->input->post('pesquisa_cred', true) == 1 ? $menus2.=',30' : '';
        /*          Menus Terciarios(Todas cobran�as,Todos inadimplentes,Todos credores,
          Meus,Todos,Previs�es */
        $this->input->post('ger_list_cob', true) == 1 ? $menus3.=',14' : '';
        $this->input->post('ger_list_inad', true) == 1 ? $menus3.=',15' : '';
        $this->input->post('ger_list_cred', true) == 1 ? $menus3.=',16' : '';
        $this->input->post('cobAdm_receb_meus', true) == 1 ? $menus3.=',21' : '';
        $this->input->post('cobAdm_receb_todos', true) == 1 ? $menus3.=',22' : '';
        $this->input->post('cobAdm_receb_prev', true) == 1 ? $menus3.=',23' : '';
        //monta a array que sera salva no banco
        $dados = array(
            'gru_titulo' => utf8_encode($nome),
            'gru_per_menu_nivel1' => $menus1,
            'gru_per_menu_nivel2' => $menus2,
            'gru_per_menu_nivel3' => $menus3
        );
        // executa um isnert no banco e exibe ao usu�rio uma mensagem informando o ocorrido
        if ($this->db->insert('grupo_usuarios', $dados)) {
            $this->inicore->setMensagem('success', $this->input->post('gruponome', true) . ' cadastrado com sucesso', false);
        } else {
            $this->inicore->setMensagem('error', 'Erro ao tentar cadastrar o grupo de usu�rios, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', false);
        }
        // envia a mensagem setada e as op��es de cadastrar novo ou voltar para
        // a tela de mensagens padr�es
        $this->data['novobtn'] = base_url() . 'cadtipousu/novo';
        $this->data['voltarbtn'] = base_url() . 'cadtipousu/listar';
        $this->inicore->loadview('mens_padrao', $this->data);
    }

}