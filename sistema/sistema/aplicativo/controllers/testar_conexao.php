<?php
//Este � o controlador respons�vel por checar se o usu�rio pode acessar o sistema,
//e checar o andamento da sess�o
//            Testar se o status da sess�o � true, o que significa que o usu�rio esta
//            logado, caso n�o for true, redireciona para a p�gina de login
            if ($this->session->userdata('status')!=true) {
                $this->inicore->setMensagem('error','Acesso bloqueado, Efetue login!',true);
                redirect(base_url().'login');
            }

//            nas proximaas 5 linhas o script pega os dados da sess�o referente a
//            hora de entrada e hora de saida, e transforma tudo em minutos, para
//            depois testar a hr aatual e ver se o usu�rio n�o esta acessando o
//            sistema em hor�rio indevido
            $hrentrada = $this->session->userdata('hrentrada');
            $hrsaida = $this->session->userdata('hrsaida');
            $hrentrada = (substr($hrentrada, 0, 2)*60)+(substr($hrentrada, 3, 2));
            $hrsaida = (substr($hrsaida, 0, 2)*60)+(substr($hrsaida, 3, 2));
            $hratual = (substr(date('H:i'), 0, 2)*60)+(substr(date('H:i'), 3, 2));

//            aqui � testado a hr atual com a hr permitida do usuario, se a hr de
//            entrada for maior que a hr atual quer dizer q ainda � cedo de mais
//            para o usuario acessar o sistema, ou se a hora de saida for menor que
//            a hora atual quer dizer que o � muito tarde para o usu�rio acessar o
//            sistema
            if(($hrentrada>$hratual)||($hrsaida<$hratual)){
                $this->session->sess_destroy();
                redirect(base_url().'login');
            }
            
?>