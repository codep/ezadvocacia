<?php

class Usuarios extends Controller {

    function Home() {
        parent::Controller();
    }

    function _remap($link) {
        //adiciona o titulo na p�gina
        $this->data['title'] = "Recupera :: Gerenciamento de usu�rios";
        //adiciona os CSS
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue'));
        //carrega os helpers
        $this->load->helper("funcoes_helper");
        //carrega o model
        $this->load->model('usuarios_model', 'md');
        //adiciona os javascripts
        $this->inicore->addjs(array('jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput','plugin/jquery.quicksearch.min.js'));
		
        //monta a array com as permiss�es do usuario para testar se tem  permiss�o de acesso aos recursos
        //escolhendo o menu que ficara selecionado;
        $this->session->set_userdata('menusel', '1');
        $menus1 = explode(",", $this->session->userdata('menu1'));
        $menus2 = explode(",", $this->session->userdata('menu2'));
        $menus3 = explode(",", $this->session->userdata('menu3'));
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        //Redireciona o fluxo da url de acordo com o que for solicitado
        if ($link == "listar") {
            //testa para ver se o usu�rio tem permiss�o de acesso ao recurso
            if (array_search('7', $menus2, TRUE) != '') {
                //fun��o respons�vel por listar os usu�rios
                $this->_listar();
            } else {
                //caso n�o tenha permiss�o de acesso redireciona para a home com mensagem de erro
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "novo") {
            if (array_search('7', $menus2, TRUE) != '') {
                //fun��o respons�vel por abrir a tela de novo usu�rio
                $this->_novo();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "incluir") {
            if (array_search('7', $menus2, TRUE) != '') {
                //fun��o que insere no banco de dados as informa��es de um novo usuario
                $this->_incluir();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "minhaconta") {
            //fun��o responsavel por exibir as informa��es de usuarios no menu "minhas contas"
            $this->_conta();
        } else if ($link == "credores") {
            //fun��o responsavel por exibir as informa��es de usuarios no menu "minhas contas"
            $this->_credores();
        } else if ($link == "editar") {
            if (array_search('7', $menus2, TRUE) != '') {
                //fun��o respons�vel por abrir a tela de edi��o de usu�rio
                $this->_editar();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "update") {
            if (array_search('7', $menus2, TRUE) != '') {
                //fun��o respons�vel por atualizar as informa��es do usu�rio no banco
                $this->_update();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "excluir") {
            if (array_search('7', $menus2, TRUE) != '') {
                //fun��o reposns�vel por abrir a tela de exclus�o de usu�rio
                $this->_excluir();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "delete") {
            if (array_search('7', $menus2, TRUE) != '') {
                //fun��o que executa a exclus�o do usu�rio
                $this->_delete();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "mudaSenha") {
            if (array_search('7', $menus2, TRUE) != '') {
                //fun��o respons�vel por mudar a senha do usuario na view de minha conta
                $this->_mudaSenha();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else {
            $this->inicore->setMensagem('warning', 'Recurso ainda n�o implementado, entre em contato com o administrador do sistema');
            redirect(base_url() . 'home');
        }
    }

    function _listar() {
        $this->inicore->loadSidebar();
        $this->data['usuarios'] = $this->md->getUsuarios();
        $this->inicore->loadview('usu_listar', $this->data);
    }

    function _conta() {
        $this->inicore->loadSidebar();
        $this->data['dadosConta'] = $this->md->getContasDetalhes($this->session->userdata('usucod'));
        $this->inicore->loadview('usu_minhaconta', $this->data);
    }
	
    function _credores() {
        $this->inicore->loadSidebar();
        $this->data['resultados'] = $this->md->getCredores($this->session->userdata('usucod'));
        $this->inicore->loadview('usu_credores', $this->data);
    }

    function _incluir() {
        if (
                ($this->input->post('nome', true) == null) ||
                ($this->input->post('tipo_usuario', true) == null) ||
                ($this->input->post('usuario', true) == null) ||
                ($this->input->post('senha', true) == null) ||
                ($this->input->post('hr_entrada', true) == null) ||
                ($this->input->post('hr_saida', true) == null)
        ) {
            //dados obrigat�rios
            $this->inicore->setMensagem('error', 'Dados insuficientes para o cadastro', true);
            redirect(base_url() . 'usuarios/novo');
        }
        //pega o u usu�rio e asenha para criptografar a senha antes de passala para o banco
        $senha = $this->input->post('senha', true);
        $usuario = strtolower($this->input->post('usuario', true));
        //criptografa a senha concatenada ao nome do usuario para garantir mais seguran�a
        $senha = sha1($usuario . $senha);
        //monta o array com os dados oriundos do post
        $dados = array(
            'grupo_usuarios_gru_cod' => $this->input->post('tipo_usuario', true),
            'usu_sexo' => $this->input->post('sexo', true),
            'usu_nome' => removeCE_Upper(trim($this->input->post('nome', true))),
            'usu_cpf' => utf8_encode($this->input->post('cpf', true)),
            'usu_rg' => utf8_encode($this->input->post('rg', true)),
            'usu_endereco' => removeCE_Upper(trim($this->input->post('endereco', true))),
            'usu_bairro' => removeCE_Upper(trim($this->input->post('bairro', true))),
            'usu_complemento' => removeCE_Upper(trim($this->input->post('complemento', true))),
            'usu_cep' => utf8_encode($this->input->post('cep', true)),
            'usu_cidade' => removeCE_Upper(trim($this->input->post('cidade', true))),
            'usu_uf' => utf8_encode($this->input->post('estado', true)),
            'usu_fone' => utf8_encode($this->input->post('telefone', true)),
            'usu_celular' => utf8_encode($this->input->post('celular', true)),
            'usu_email' => utf8_encode($this->input->post('email', true)),
            'usu_funcoes' => removeCE_Upper(trim($this->input->post('funcoes', true))),
            'usu_usuario_sis' => $usuario,
            'usu_senha_sis' => $senha,
            'usu_hr_sis_entrada' => utf8_encode($this->input->post('hr_entrada', true)),
            'usu_hr_sis_saida' => utf8_encode($this->input->post('hr_saida', true))
        );
        //tenta inserir os dados no banc, se funcionar exibe mensagem de sucesso
        //se n�o funcionar exibe mensagem de erro
        if ($this->db->insert('usuarios', $dados)) {
            $this->inicore->setMensagem('success', 'Usu�rio ' . $this->input->post('nome', true) . ' cadastrado com sucesso', false);
        } else {
            $this->inicore->setMensagem('success', 'Erro ao cadastrar o usu�rio, tente novamente; Se o erro persistir entre em contato com a administrado do sistema', false);
        }
        // envia a mensagem setada e as op��es de cadastrar novo ou voltar para
        // a tela de mensagens padr�es
        $this->data['novobtn'] = base_url() . 'usuarios/novo';
        $this->data['voltarbtn'] = base_url() . 'usuarios/listar';
        $this->inicore->loadview('mens_padrao', $this->data);
    }

    function _novo() {
        $this->inicore->loadSidebar();
        $this->data ['grupos'] = $this->md->getGrupos();
        $this->data['estados'] = $this->md->getEstados();
        $this->inicore->loadview('cad_usuario', $this->data);
    }

    function _editar() {
        $this->inicore->loadSidebar();
        $cod = get('cod');
        $this->data['dados'] = $this->md->getUsuario($cod);
        $this->data ['grupos'] = $this->md->getGrupos();
        $this->data['estados'] = $this->md->getEstados();
        $this->inicore->loadview('usu_editar', $this->data);
    }

    function _update() {
        if (
                ($this->input->post('nome', true) == null) ||
                ($this->input->post('tipo_usuario', true) == null) ||
                ($this->input->post('usuario', true) == null) ||
                ($this->input->post('hr_entrada', true) == null) ||
                ($this->input->post('hr_saida', true) == null)
        ) {
            //dados obrigat�rios
            $this->inicore->setMensagem('error', 'Dados insuficientes para o cadastro', true);
            redirect(base_url() . 'cadrepasse/novo');
        }
        //recebe os dados do post
        $cod = $this->input->post('cod', true);
        //se o usu�rio tiver inserido uma senha ent�o a senha do banco deve ser mudada
        if ($this->input->post('senha', true) != '') {
            $senha = $this->input->post('senha', true);
            $usuario = strtolower($this->input->post('usuario', true));
            $senha = sha1($usuario . $senha);
            //monta a array de dados do usuario com a senha
            $dados = array(
                'grupo_usuarios_gru_cod' => $this->input->post('tipo_usuario', true),
                'usu_sexo' => $this->input->post('sexo', true),
                'usu_nome' => removeCE_Upper(trim($this->input->post('nome', true))),
                'usu_cpf' => utf8_encode($this->input->post('cpf', true)),
                'usu_rg' => utf8_encode($this->input->post('rg', true)),
                'usu_endereco' => removeCE_Upper(trim($this->input->post('endereco', true))),
                'usu_bairro' => removeCE_Upper(trim($this->input->post('bairro', true))),
                'usu_complemento' => removeCE_Upper(trim($this->input->post('complemento', true))),
                'usu_cep' => utf8_encode($this->input->post('cep', true)),
                'usu_cidade' => removeCE_Upper(trim($this->input->post('cidade', true))),
                'usu_uf' => utf8_encode($this->input->post('estado', true)),
                'usu_fone' => utf8_encode($this->input->post('telefone', true)),
                'usu_celular' => utf8_encode($this->input->post('celular', true)),
                'usu_email' => utf8_encode($this->input->post('email', true)),
                'usu_funcoes' => removeCE_Upper(trim($this->input->post('funcoes', true))),
                'usu_usuario_sis' => $usuario,
                'usu_senha_sis' => $senha,
                'usu_hr_sis_entrada' => utf8_encode($this->input->post('hr_entrada', true)),
                'usu_hr_sis_saida' => utf8_encode($this->input->post('hr_saida', true))
            );
            //caso a senha n�o tenha sido inserida
        } else {
            $usuario = strtolower($this->input->post('usuario', true));
            //monta uma array com os dados do usu�rio sem a senha
            $dados = array(
                'grupo_usuarios_gru_cod' => $this->input->post('tipo_usuario', true),
                'usu_sexo' => $this->input->post('sexo', true),
                'usu_nome' => removeCE_Upper(trim($this->input->post('nome', true))),
                'usu_cpf' => utf8_encode($this->input->post('cpf', true)),
                'usu_rg' => utf8_encode($this->input->post('rg', true)),
                'usu_endereco' => removeCE_Upper(trim($this->input->post('endereco', true))),
                'usu_bairro' => removeCE_Upper(trim($this->input->post('bairro', true))),
                'usu_complemento' => removeCE_Upper(trim($this->input->post('complemento', true))),
                'usu_cep' => utf8_encode($this->input->post('cep', true)),
                'usu_cidade' => removeCE_Upper(trim($this->input->post('cidade', true))),
                'usu_uf' => utf8_encode($this->input->post('estado', true)),
                'usu_fone' => utf8_encode($this->input->post('telefone', true)),
                'usu_celular' => utf8_encode($this->input->post('celular', true)),
                'usu_email' => utf8_encode($this->input->post('email', true)),
                'usu_funcoes' => removeCE_Upper(trim($this->input->post('funcoes', true))),
                'usu_usuario_sis' => $usuario,
                'usu_hr_sis_entrada' => utf8_encode($this->input->post('hr_entrada', true)),
                'usu_hr_sis_saida' => utf8_encode($this->input->post('hr_saida', true))
            );
        }
        //executa um update no banco para o usuario que esta sendo editado, se
        //ocorrer tudo certo retorna uma mensagem de sucesso, caso contrario
        //retorna uma mensagem informando que houve um erro
        $this->db->where('usu_cod', $cod);
        if ($this->db->update('usuarios', $dados)) {
            $this->inicore->setMensagem('notice', 'Usu�rio ' . $this->input->post('nome', true) . ' atualizado com sucesso', true);
        } else {
            $this->inicore->setMensagem('success', 'Erro ao atualizar o usu�rio, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
        }
        redirect(base_url() . 'usuarios/listar');
    }

    function _excluir() {
        $cod = get('cod');
        /* A fun��o excluir possui uma tela de confirma��o de exclus�o que
         * serve para qualque exclus�o no sistema, para que esta tela funcione
         * corretamente deve ser passados algumas vari�veis que ser�o utilizadas
         * na constru��o da p�gina, as vari�veis necess�rias s�o:
         * $deletado-----------------------O que est� sendo deletado
         * $codigo-------------------------c�digo do que est� sendo deletado
         * $tabela-------------------------Tabela que ter� o item excluido
         * $chave--------------------------Chave primaria da tabela para o delete
         * $acaocancel---------------------A��o executada quando clicar em cancelar
         * $acaook-------------------------A��o executada quando clicar em Apagar
         */
        $this->data['title'] = "Recupera :: Confirmar exclus�o";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->inicore->addjs(array('smooth.form.js'));
        $this->data['deletado'] = 'Usu�rio';
        $this->data['codigo'] = $cod;
        $this->data['tabela'] = 'usuarios';
        $this->data['chave'] = 'usu_cod';
        $this->data['acaocancel'] = 'usuarios/listar';
        $this->data['acaook'] = 'usuarios/delete';

        $this->inicore->loadview('conf_exclusao', $this->data);
    }

    function _delete() {
        //pega a senha inserida pelo usu�rio e criptografa a mesma seguindo uma regra pre estabelecida
        $senha = sha1('senhamaster+' . $this->input->post('senha', true));
        //pega a senha orginal no banco de dados
        $sql = "SELECT cod, senha FROM senha_master";
        $query = $this->db->query($sql);
        //testa se as senhas s�o iguais
        if ($query->row()->senha == $senha) {
           $data = array(
               'usu_ativo' => '0'
            );
           
            $this->db->where($this->input->post('chave', true), $this->input->post('codigo', true));
            $this->db->update('usuarios', $data);
            //se as senhas estiverem iguais apaga o usu�rio e exibe mensagem de acordo com o ocorrido
            if ($this->db->set($this->input->post('tabela', true))) {
                $this->inicore->setMensagem('notice', 'Usu�rio apagado com sucesso', true);
            } else {
                $this->inicore->setMensagem('error', 'Erro ao apagar o usu�rio, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
            }
            //redireciona para onde deve ser redirecionado :)
            redirect($this->input->post('voltar', true));
            // caso a senha estiver errada monta a tela de exclus�o novamente  e
            // exibe mensagem de erro para o usu�rio
        } else {
            $this->data['title'] = "Recupera :: Confrimar exclus�o";
            $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
            $this->inicore->addjs(array('smooth.form.js'));
            $this->data['deletado'] = 'Usu�rio';
            $this->data['codigo'] = $this->input->post('codigo', true);
            $this->data['tabela'] = 'usuarios';
            $this->data['chave'] = 'usu_cod';
            $this->data['acaocancel'] = 'usuarios/listar';
            $this->data['acaook'] = 'usuarios/delete';
            $this->inicore->setMensagem('error', 'SENHA incorreta, favor verificar e digitar novamente', false);
            $this->inicore->loadview('conf_exclusao', $this->data);
        }
    }

    function _mudaSenha() {
        //se o usu�rio enviou senha para trocar a sua atual pega os dados da
        //senha criptografa e realiza um update no banco de dados
        if ($this->input->post('senha', true)) {
            $senha = $this->input->post('senha', true);
            $usuario = $this->input->post('usuario', true);
            $senha = sha1($usuario . $senha);
            $cod = $this->input->post('cod');
            $dados = array(
                'usu_senha_sis' => $senha
            );
            //executa um update no banco e exibe mensagem de acordo com o ocorrido
            $this->db->where('usu_cod', $cod);
            if ($this->db->update('usuarios', $dados)) {
                $this->inicore->setMensagem('notice', 'Usu�rio ' . $this->input->post('usuario', true) . ' atualizado com sucesso', true);
            } else {
                $this->inicore->setMensagem('success', 'Erro ao atualizar o usu�rio, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
            }
            redirect(base_url() . 'home');
            //se o usuario n�o mandou nova senha,apenas redireciona para o controlador  Home
        } else {
            redirect(base_url() . 'home');
        }
    }
}