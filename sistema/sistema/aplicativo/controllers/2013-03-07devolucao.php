<?php

class Devolucao extends Controller {

    function Listadivida() {
        parent::Controller();
    }

    function _remap($link) {

        $this->data['title'] = "Recupera :: Gerenciamento";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->load->helper("funcoes_helper");
        $this->load->model('Devolucaomodel', 'md');
//        $this->load->model('inadimplente_model', 'inad_model');
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput', 'jquery.alphanumeric.js', 'jquery.moeda.js'));
        //monta a array com as permiss�es do usuario para testar se tem  permiss�o de acesso aos recursos
        //escolhendo o menu que ficara selecionado;
        $menus1 = explode(",", $this->session->userdata('menu1'));
        $menus2 = explode(",", $this->session->userdata('menu2'));
        $menus3 = explode(",", $this->session->userdata('menu3'));

//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        //IR PARA O RESPECTIVO FLUXO DA URL
        if ($link == "listar") {
            if (array_search('44', $menus2, TRUE) != '') {
                $this->_listar();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "devolver") {
            if (array_search('44', $menus2, TRUE) != '') {
                // FUN��O RESPONSAVEL POR ABRIR A TELA DE EXCLUS�OD E D�VIDA
                $this->_devolver();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else {
            $this->inicore->setMensagem('warning', 'Recurso ainda n�o implementado, entre em contato com o administrador do sistema');
            redirect(base_url() . 'home');
        }
    }

    //listar todas as d�vidas (gerenciamento/listar/todas cobrancas)
    function _listar() {
//        die($this->input->post('filtro1'));
        $this->session->set_userdata('menusel', '9');
//        if ($this->input->post('filtro1', true) != '') {
//            $aux = $this->input->post('filtro1');
//            if ($this->input->post('filtro1', true) == 'c.cob_cod' && $this->input->post('nome', true) == '') {
//                $this->inicore->setMensagem('error', 'Digite um c�digo v�lido', true);
//                redirect(base_url() . 'divida/listar');
//            } else if ($this->input->post('filtro1', true) == 'c.cob_cod' && !is_numeric($this->input->post('nome'))) {
//                $this->inicore->setMensagem('error', 'Digite um c�digo v�lido', true);
//                redirect(base_url() . 'divida/listar');
//            } else if ($this->input->post('filtro1', true) == 'ina_nome' && $this->input->post('nome', true) == '') {
//                $this->inicore->setMensagem('error', 'Digite um nome', true);
//                redirect(base_url() . 'divida/listar');
//            } else if ($this->input->post('filtro1', true) == 'div_emissao' && $this->input->post('nome', true) == '') {
//                $this->inicore->setMensagem('error', 'Digite a data de emiss�o', true);
//                redirect(base_url() . 'divida/listar');
//            } else if ($this->input->post('filtro1', true) == 'div_emissao' && $this->input->post('nome', true) != '') {
//                $data = $this->input->post('nome');
//                $dia = substr($data, 0, -8); //pega s� o dia da data
//                $mes = substr($data, 3, -5); //pega s� o m�s da data
//                $ano = substr($data, -4); // pega s� o ano da data
//                $aux2 = $ano . '-' . $mes . '-' . $dia;
//                $filtro = "$aux = '$aux2' AND";
//            } else {
//                if ($this->input->post('filtro1') == 'c.cob_cod') {
//                    $filtro = "cob.cob_cod = " . $this->input->post('nome') . " AND";
//                } else if ($this->input->post('filtro1') == 'ina_nome' && strlen($this->input->post('nome')) >= 2) {
//                    $aux2 = $this->input->post('nome');
//                    $filtro = "$aux like '%$aux2%' AND";
//                } else {
//                    $this->inicore->setMensagem('error', 'Dados insuficiente para pesquisa!', true);
//                    redirect(base_url() . 'divida/listar');
//                }
//            }
//        } else {
        $aux = '';
        $filtro = "";
//        }

        $this->data['dividasEAcordos'] = $this->md->getListarDividas($filtro);
        $this->data['aux'] = $aux;

        $this->inicore->loadSidebar();
        $this->inicore->loadview('devolucao_listar_todas', $this->data);
    }

    function _devolver() {
        $codCob = get('codCobranca');
        $inad = get('inad');

        $data = array(
            'cob_remocao' => '2'
        );

        $this->db->where('cob_cod', $codCob);
        $this->db->update('cobrancas', $data);

        $data = array(
            'cobrancas_cob_cod' => $codCob,
            'dev_data' => date('Y-m-d')
        );

        $this->db->insert('devolucoes', $data);

        $this->inicore->setMensagem('success', 'D�vida c�digo: ' . $codCob . ' do inadimplente: ' . $inad . ' foi devolvida com sucesso!');
        redirect(base_url() . 'devolucao/listar');
    }

}