<script type="text/javascript">
        $(document).ready(function(){
            $('.editausu').click(function(e){
                    link = $(this).attr('id');
                    window.location.href=link;
            });
        });
</script>
<html>
    <body>
        <div id="header">
            <div id="header-outer">
                <div id="logo">
                    <h1><a href="" title="Smooth Admin"><img src="<?php echo $img.'logo.png'?>" alt="Smooth Admin" /></a></h1>
                </div>
                <ul id="user">
                    <li class="first">Ol� <b>Elis�ngela</b></li>
                    <li><?php echo dataex(date('Y-m-d')); ?></li>
                </ul>
                <div id="header-inner">
                    <div id="home">
                        <a href="<?php echo base_url() ?>"></a>
                    </div>
                    <ul id="quick">
                        <li>
                            <a href="<?php echo base_url().'agenda/listar' ?>" title="Agenda"><span class="icon"><img src="<?php echo $img.'icons/calendar.png'?>" alt="Agenda" /></span><span>Agenda</span></a>
                            <ul>
                                <li><a href="<?php echo base_url().'agenda/incluir' ?>">Nova tarefa</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="" title="Mensagens"><span class="icon"><img src="<?php echo $img.'icons/page_white_copy.png'?>" alt="Mensagens" /></span><span>Mensagens(10)</span></a>
                            <ul>
                                <li><a href="#">Nova Mensagem</a></li>
                                <li><a href="#">Entrada(10)</a></li>
                                <li><a href="#">Sa�da</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="" title="Minha conta"><span class="icon"><img src="<?php echo $img.'icons/cog.png'?>" alt="Minha conta" /></span><span>Minha conta</span></a>
                        </li>
                        <li>
                            <a href="" title="Sair"><span class="icon"><img src="<?php echo $img.'icons/exit16.png'?>" alt="Sair" /></span><span>Sair</span></a>
                        </li>
                    </ul>
                    <div class="corner tl"></div>
                    <div class="corner tr"></div>
                </div>
            </div>
        </div>
        <div id="content">

        <?php echo $sidebar; ?>

            <div id="right">
                <div class="box">
                    <div class="title">
                        <h5>Lista de usu�rios cadastrados</h5>
                    </div>
                    <form id="form" action="" method="post">
                        <div class="form">
                            <div class="fields">
                                <div class="table">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left">Nome</th>
                                                <th>Fun��es</th>
                                                <th>Grupo</th>
                                                <th class="last">Excluir</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id="<?php echo base_url().'usuarios/incluir'?>" class="editausu">
                                                <td class="title">Fulano de tal</td>
                                                <td>Cobrador</td>
                                                <td>Cobradores</td>
                                                <td style="text-align: center; vertical-align: middle;" class="last">
                                                    <a href="#">
                                                        <img src="<?php echo $img.'excluir.png'?>" alt="+"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr id="<?php echo base_url().'usuarios/incluir'?>" class="editausu">
                                                <td class="title">Fulano de tal</td>
                                                <td>Supervisor</td>
                                                <td>Administrador</td>
                                                <td style="text-align: center; vertical-align: middle;" class="last">
                                                    <a href="#">
                                                        <img src="<?php echo $img.'excluir.png'?>" alt="+"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr id="<?php echo base_url().'usuarios/incluir'?>" class="editausu">
                                                <td class="title">Fulano de tal</td>
                                                <td>Cobrador</td>
                                                <td>Cobradores</td>
                                                <td style="text-align: center; vertical-align: middle;" class="last">
                                                    <a href="#">
                                                        <img src="<?php echo $img.'excluir.png'?>" alt="+"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr id="<?php echo base_url().'usuarios/incluir'?>" class="editausu">
                                                <td class="title">Fulano de tal</td>
                                                <td>Estagi�rio</td>
                                                <td>Estagi�rios</td>
                                                <td style="text-align: center; vertical-align: middle;" class="last">
                                                    <a href="#">
                                                        <img src="<?php echo $img.'excluir.png'?>" alt="+"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr id="<?php echo base_url().'usuarios/incluir'?>" class="editausu">
                                                <td class="title">Fulano de tal</td>
                                                <td>Supervisor</td>
                                                <td>Administrador</td>
                                                <td style="text-align: center; vertical-align: middle;" class="last">
                                                    <a href="#">
                                                        <img src="<?php echo $img.'excluir.png'?>" alt="+"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr id="<?php echo base_url().'usuarios/incluir'?>" class="editausu">
                                                <td class="title">Fulano de tal</td>
                                                <td>Atendente</td>
                                                <td>Atendentes</td>
                                                <td style="text-align: center; vertical-align: middle;" class="last">
                                                    <a href="#">
                                                        <img src="<?php echo $img.'excluir.png'?>" alt="+"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr id="<?php echo base_url().'usuarios/incluir'?>" class="editausu">
                                                <td class="title">Fulano de tal</td>
                                                <td>Estagi�rio</td>
                                                <td>Cobradores</td>
                                                <td style="text-align: center; vertical-align: middle;" class="last">
                                                    <a href="#">
                                                        <img src="<?php echo $img.'excluir.png'?>" alt="+"/>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>