<?php

class Relatorios extends Controller {

    function Relatorios() {
        parent::Controller();
    }

    function _remap($link) {
        $this->load->helper("funcoes_helper");
        $this->load->model('relatorios_model', 'md');
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        //IR PARA O RESPECTIVO FLUXO DA URL
        if ($link == "rel_adm")
            $this->_rel_adm();
        else if ($link == "rel_adm01")
            $this->_rel_adm01();
        else if ($link == "rel_adm02")
            $this->_rel_adm02();
        else if ($link == "rel_adm03")
            $this->_rel_adm03();
        else if ($link == "rel_adm04")
            $this->_rel_adm04();
        else if ($link == "rel_jur")
            $this->_rel_jur();
        else if ($link == "rel_jur01")
            $this->_rel_jur01();
        else if ($link == "rel_jur02")
            $this->_rel_jur02();
        else if ($link == "rel_jur03")
            $this->_rel_jur03();
        else if ($link == "rel_ro")
            $this->_rel_ro();
        else if ($link == "rel_ro01")
            $this->_rel_ro01();
        else if ($link == "rel_ger")
            $this->_rel_ger();
        else if ($link == "rel_ger01")
            $this->_rel_ger01();
        else if ($link == "rel_ger02")
            $this->_rel_ger02();
        else if ($link == "rel_ger03")
            $this->_rel_ger03();
        else if ($link == 'rel_ger04')
            $this->_rel_ger04();
        else if ($link == 'rel_ger05')
            $this->_rel_ger05();
        else if ($link == 'rel_ger06')
            $this->_rel_ger06();
        else if ($link == "rel_ger07")
            $this->_rel_ger07();
        else if ($link == "rel_ger08")
            $this->_rel_ger08();
        else if ($link == 'rel_prest')
            $this->_rel_prest();
        else if ($link == 'rel_prest01')
            $this->_rel_prest01();
        else if ($link == "rel_prest03")
            $this->_rel_prest03();
        else if ($link == "rel_ace_cre")
            $this->_rel_ace_cre();
        else if ($link == "rel_ace_usu")
            $this->_rel_ace_usu();
        else if ($link == "rel_ace01")
            $this->_rel_ace01();
        else if ($link == "rel_ace02")
            $this->_rel_ace_02();
        else if ($link == "rel_import")
            $this->_rel_import();
        else if ($link == "rel_import1")
            $this->_rel_import1();
        else if ($link == "rel_contat")
            $this->_rel_contat();
        else if ($link == "rel_contat01")
            $this->_rel_contat01();
    }

    //|--------------------------------------------------------------------------------------|
    //|----------------------------------- RELET�RIOS ADM -----------------------------------|
    //|--------------------------------------------------------------------------------------|
    function _rel_adm() {//Relat�rios de acordos e previs�es adm em atraso
        $this->data['title'] = "Recupera :: Relat�rio de acordos e previs�es em atraso";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput'));
        $this->session->set_userdata('menusel', '31'); //DETERMINA O MENU QUE FICARA ABERTO
        $this->inicore->loadSidebar(); //barra lateral
        $this->data['credores'] = $this->md->getCredores(); //pega todos os credores e seus c�digos.(tabela credores)
        $this->inicore->loadview('rel_adm', $this->data);
    }

    function _rel_adm01() {
        $dataHoje = date('Y-m-d');
        $codUsuLog = $this->session->userdata('usucod'); //c�d do usu�rio da sess�o
        $filtroCredor = $this->input->post('cre_filtro01'); //credor espec�fico
        $creCodigo = $this->input->post('cre_cod01'); // campo Nome: (da view rel_adm). Recebe o c�d do credor selecionado
        $aux = $this->input->post('de01'); //data selecionada pelo usu�rio. data inicial
        $dataDe = convDataParaDb($aux); //converte a data para o formato aaaa-mm-dd
        $aux = $this->input->post('ate01'); //data selecionada pelo usu�rio. data final
        $dataAte = convDataParaDb($aux); // data final" converte a data para o formato aaaa-mm-dd
        $tipo = $this->input->post('tipo01'); //acordos, previs�es ou os dois tipos

        if ($filtroCredor == 'todos' && $tipo == 'todos' && $dataDe != '--' && $dataAte != '--') {
            //die("acordos e previs�es de todos credores na data especificada");
            $filtro = "AND par_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte'";
        } else if ($filtroCredor == 'todos' && $tipo == 'acordos' && $dataDe != '--' && $dataAte != '--') {
            //die("todos acordos de todos credores na data especificada");
            $tipoAcordo = 1;
            $filtro = "AND ac.aco_tipo = $tipoAcordo AND par_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte'";
        } else if ($filtroCredor == 'todos' && $tipo == 'previsoes' && $dataDe != '--' && $dataAte != '--') {
            //die("todas previs�es de todos credores na  data especificada");
            $tipoAcordo = 2;
            $filtro = "AND ac.aco_tipo = $tipoAcordo AND par_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte'";
        } else if ($filtroCredor == 'credor_especifico' && $creCodigo != "" && $dataDe != '--' && $dataAte != '--' && $tipo == 'todos' && is_numeric($creCodigo)) {
            //die("todos os acordos e previs�es do credor especificado na data especificada ");
            $filtro = " AND cre.cre_cod = $creCodigo AND par_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte'";
        } else if ($filtroCredor == 'credor_especifico' && $creCodigo != "" && $dataDe != '--' && $dataAte != '--' && $tipo == 'acordos' && is_numeric($creCodigo)) {
            //die("todos os acordos do credor especificado na data especificada");
            $tipoAcordo = 1;
            $filtro = "AND cre.cre_cod = $creCodigo AND ac.aco_tipo = $tipoAcordo AND par_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte'";
        } else if ($filtroCredor == 'credor_especifico' && $creCodigo != "" && $dataDe != '--' && $dataAte != '--' && $tipo == 'previsoes' && is_numeric($creCodigo)) {
            //die("todas as previs�es do credor especificado na data especificada");
            $tipoAcordo = 2;
            $filtro = "AND cre.cre_cod = $creCodigo AND ac.aco_tipo = $tipoAcordo AND par_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte' ";
        } else {
            $mensagem = "Dados insuficientess para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_adm');
        }

        $resultPesquisa = $this->data['acordPrevAtra'] = $this->md->rel_adm01($codUsuLog, $dataHoje, $filtro);

        if (sizeof($resultPesquisa) == 0) {
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_adm');
        }
        $this->data['title'] = 'Recupera :: Relat�rio de Acordos e Previs�es em Atraso';
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->data['recuperador'] = $this->session->userdata('usuwho');
        $this->inicore->loadview('rel_adm01', $this->data);
    }

    function _rel_adm02() {
        $codUsuLog = $this->session->userdata('usucod'); //c�d do usu�rio da sess�o
        $creCodigo = $this->input->post('cre_cod02'); // campo Nome: (da view rel_adm). Aqui recebe o c�d do credor
        $filtroCredor = $this->input->post('cre_filtro02'); //credor espec�fico
        $aux = $this->input->post('de02'); //data selecionada pelo usu�rio. data inicial
        $dataDe = convDataParaDb($aux); //converte a data para o formato aaaa-mm-dd
        $aux = $this->input->post('ate02'); //data selecionada pelo usu�rio. data final
        $dataAte = convDataParaDb($aux); // data final" converte a data para o formato aaaa-mm-dd
        $tipo = $this->input->post('tipo02'); //acordos, previs�es ou os dois tipos

        if ($filtroCredor == 'todos' && $tipo == 'todos' && $dataDe != '--' && $dataAte != '--') {
            //die("Filtro 1 Padr�o: Acordos e previs�es ADM de todos os credores na data especificada");
            $filtro = "AND par_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte'";
        } else if ($filtroCredor == 'todos' && $tipo == 'acordos' && $dataDe != '--' && $dataAte != '--') {
            //die("Filtro 2: Acordos ADM de todos os credores na data especificada");
            $tipoAcordo = 1;
            $filtro = "AND par_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte' AND ac.aco_tipo = $tipoAcordo";
        } else if ($filtroCredor == 'todos' && $tipo == 'previsoes' && $dataDe != '--' && $dataAte != '--') {
            //die("Fitro3: Previs�es ADM de todos os credores na data especificada");
            $tipoAcordo = 2;
            $filtro = "AND par_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte' AND ac.aco_tipo = $tipoAcordo";
        } else if ($filtroCredor == 'credor_especifico' && $creCodigo != "" && $dataDe != '--' && $dataAte != '--' && $tipo == 'todos' && is_numeric($creCodigo)) {
            //die("Filtro 4: Acordos e previs�es ADM do credor selecionado na data especificada");
            $filtro = "AND par_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte' AND co.credor_cre_cod = $creCodigo";
        } else if ($filtroCredor == 'credor_especifico' && $creCodigo != "" && $dataDe != '--' && $dataAte != '--' && $tipo == 'acordos' && is_numeric($creCodigo)) {
            //die("Filtro 5: Acordos ADM do credor selecionado na data selecionada");
            $tipoAcordo = 1;
            $filtro = "AND par_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte' AND ac.aco_tipo = $tipoAcordo AND co.credor_cre_cod = $creCodigo";
        } else if ($filtroCredor == 'credor_especifico' && $creCodigo != "" && $dataDe != '--' && $dataAte != '--' && $tipo == 'previsoes' && is_numeric($creCodigo)) {
            //die("Filtro 6: Previs�es ADM do credor selecionado na data especificada");
            $tipoAcordo = 2;
            $filtro = "AND par_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte' AND ac.aco_tipo = $tipoAcordo AND co.credor_cre_cod = $creCodigo ";
        } else {
            $mensagem = "Dados insuficientess para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_adm');
        }

        $resultPesquisa = $this->data['acordPrevVenc'] = $this->md->rel_adm02($codUsuLog, $filtro); //acordos e previs�es adm a vencer

        if (sizeof($resultPesquisa) == 0) {
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_adm');
        }

        $this->data['title'] = 'Recupera :: Relat�rio de Acordos e Previs�es a vencer ';
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->data['recuperador'] = $this->session->userdata('usuwho');
        $this->inicore->loadview('rel_adm02', $this->data);
    }

    function _rel_adm03() {

        $codUsuLog = $this->session->userdata('usucod'); //c�d do usu�rio da sess�o
        $filtroCredor = $this->input->post('cre_filtro03'); //credor espec�fico
        $creCodigo = $this->input->post('cre_cod03'); // campo Nome: (da view rel_adm). Recebe o c�d do credor selecionado
        $aux = $this->input->post('de03'); //data selecionada pelo usu�rio. data inicial
        $dataDe = convDataParaDb($aux); //converte a data para o formato aaaa-mm-dd
        $aux = $this->input->post('ate03'); //data selecionada pelo usu�rio. data final
        $dataAte = convDataParaDb($aux); // data final" converte a data para o formato aaaa-mm-dd
        $tipo = $this->input->post('tipo03'); //acordos, previs�es ou os dois tipos

        if ($filtroCredor == 'todos' && $dataDe != '--' && $dataAte != '--') {
            //die("Filtro 1: Todas cobra�as sem acordos de todos os credores no per�odo selecionado. OBS: Do usu�rio logado");
            $filtro = "AND d.div_cadastro BETWEEN '$dataDe' AND '$dataAte'";
        } else if ($filtroCredor == 'credor_especifico' && is_numeric($creCodigo) && $creCodigo != "" && $dataDe != '--' && $dataAte != '--') {
            //die("Filtro 2: Todas as cobran�as sem acordos do credor selecionado no per�odo selecionado. OBS: Do usu�rio logado");
            $filtro = "AND d.div_cadastro BETWEEN '$dataDe' AND '$dataAte'
                                 AND co.credor_cre_cod = $creCodigo";
        } else {
            $mensagem = "Dados insuficientess para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_adm');
        }
        $resultPesquisa = $this->data['cobsSemAcords'] = $this->md->rel_adm03($codUsuLog, $filtro); //cobran�as sem acordos

        if (sizeof($resultPesquisa) == 0) {
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_adm');
        }

        $this->data['title'] = 'Recupera :: Cobran�as sem Acordos';
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->data['recuperador'] = $this->session->userdata('usuwho');
        $this->inicore->loadview('rel_adm03', $this->data);
    }

    function _rel_adm04() {
        if ($this->input->post('de04') != '') {

            $filtro = 'WHERE';
            $i = 0;
            $filtroCob = '';
            $filtroVirgem = "WHERE D.div_virgem='1'";
            $data = convDataParaDb($this->input->post('de04'));
//            $filtroCob .="WHERE ROS.ros_data < '$data' ";
            $filtroVirgem .=" AND D.div_cadastro < '$data'";

            if ($this->input->post('cre_filtro04') != 'todos') {

                $credor = $this->input->post('cre_cod04'); //QUAL A ID�IA DE TER UM CREDOR PARA "TODOS"???
                $filtroCob .="AND C.credor_cre_cod='$credor' ";
                $filtroVirgem .= " AND C.credor_cre_cod='$credor'";
            }

            $usuario = $this->session->userdata('usucod');
            $filtroCob .="AND C.usuarios_usu_cod='$usuario'";
            $filtroVirgem .= " AND C.usuarios_usu_cod='$usuario'";

            $cobrancas = $this->md->getCobrancasOciosas($filtroCob, $data);


            if (sizeof($cobrancas) == '') {
                $mensagem = "N�o foram encontrados dados para esta pesquisa";
                $this->inicore->setMensagem('error', $mensagem);
                redirect(base_url() . 'relatorios/rel_adm');
            } else {

                foreach ($cobrancas as $cob) {
                    if ($i == 0) {
                        $filtro .= " C.cob_cod='$cob->cob_cod'";
                    } else {
                        $filtro .= " OR C.cob_cod='$cob->cob_cod'";
                    }
                    $i++;
                }
//die('asadsad>> '.$filtro);
                $this->data['dividasOciosas'] = $this->md->getDividasOciosas($filtro);
                $this->data['acordosOciosos'] = $this->md->getAcordosOciosos($filtro);
                $this->data['dividasVirgensOciosas'] = $this->md->getDividasVirgensOciosas($filtroVirgem);

                $this->data['title'] = 'Recupera :: Cobran�a por per�odo de ociosidade';
                $this->inicore->addcss(array('relatorios')); // CSS
                $this->data['recuperador'] = $this->session->userdata('usuwho');
                $this->inicore->loadview('rel_adm04', $this->data);
            }
        } else {
            $mensagem = "Dados insuficientess para realizar a pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_adm');
        }
    }

//|-------------------------------------------------------------------------------------------------|
//|------------------------------------  RELAT�RIOS JUR�DICOS  -------------------------------------|
//|-------------------------------------------------------------------------------------------------|
    function _rel_jur() {
        $this->data['title'] = "Recupera :: Relat�rio de acordos jur�dicos em atraso";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput'));
        $this->session->set_userdata('menusel', '31'); //DETERMINA O MENU QUE FICARA ABERTO
        $this->inicore->loadSidebar(); //barra lateral
        //$this->data['operacoes'] = $this->md->getOperacoes(); //todas os tipos de opera��es
        $this->inicore->loadview('rel_jur', $this->data);
    }

    function _rel_jur01() {
        /* Pega os acordos judici�is em atraso no per�odo selecionado, se na coluna 1� venc. n�o aparecer nenhuma data � porque
         * a cobran�a ter� alguma parcela em atraso no per�odo selecionado (se o inadimplente n�o pag�-la at� o vencimento), mas
         * at� a data atual n�o existe nenhuma parcela em atraso.
         */
        $dataAtual = date('Y-m-d'); //dia de hoje
        //$codUsuLog = $this->session->userdata('usucod'); //c�d do usu�rio da sess�o
        $filtroCredor = $this->input->post('cre_filtro01'); //credor espec�fico
        $creCodigo = $this->input->post('cre_cod01'); // campo Nome: (da view rel_jur). Recebe o c�d do credor selecionado
        $aux = $this->input->post('de01'); //data selecionada pelo usu�rio. data inicial
        $dataDe = convDataParaDb($aux); //converte a data para o formato aaaa-mm-dd
        //$aux = $this->input->post('ate01'); //data selecionada pelo usu�rio. data final
        //$dataAte = convDataParaDb($aux); // data final" converte a data para o formato aaaa-mm-dd
        //$tipo = $this->input->post('tipo01'); //acordos, previs�es ou os dois tipos
        if ($filtroCredor == 'todos' && $dataDe != '--') {
            //die("cobran�as de todos os credores desde a data selecionada at� hoje");
            $filtro = "WHERE ros.ros_data BETWEEN '$dataDe' AND '$dataAtual' GROUP BY co.cob_cod ORDER BY multa, i.ina_nome";
        } else if ($filtroCredor == 'credor_especifico' && $dataDe != '--' && $creCodigo != '' && is_numeric($creCodigo)) {
            //die("Cobran�as do credor selecionado desde a data selecionada at� hoje");
            $filtro = "WHERE cre.cre_cod = $creCodigo AND (ros.ros_data BETWEEN '$dataDe' AND '$dataAtual') GROUP BY co.cob_cod ORDER BY multa, i.ina_nome";
        } else {
            $mensagem = "Dados insuficientes para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_jur');
        }

        $resultPesquisa = $this->data['acordsJudAtras'] = $this->md->getRelJur01($filtro, $dataAtual); //acordsJudAtras -> acordos jur�dicos em atraso

        if (sizeof($resultPesquisa) == 0) {
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_adm');
        }
        $this->data['title'] = 'Recupera :: Acordos Jur�dicos em Atraso';
        $this->inicore->addcss(array('relatorios')); // CSS
        //$this->data['recuperador'] = $this->session->userdata('usuwho');
        $this->inicore->loadview('rel_jur01', $this->data);
    }

    function _rel_jur02() {//Relat�rio de Acordos jur�dicos a Vencer
        $filtroCredor = $this->input->post('cre_filtro02'); //credor espec�fico
        $creCodigo = $this->input->post('cre_cod02'); // campo Nome: (da view rel_jur). Recebe o c�d do credor selecionado
        $dataDe = convDataParaDb($this->input->post('data_de02')); //recebe a data e converte para o formato aaaa-mm-dd
        $dataAte = convDataParaDb($this->input->post('data_ate02')); //recebe a data e converte para o formato aaaa-mm-dd

        if ($filtroCredor != '' && $filtroCredor == 'todos' && $dataDe != '--' && $dataAte != '--') {
            //die("todos os acordos jur�dicos a vencer de todos os credores no per�odo selecionado");
            $filtroCredor = ""; //recebe "" para pesquisar de todos os credores
            $filtro = " WHERE par_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte'";
        } else if ($filtroCredor != '' && $filtroCredor == 'credor_especifico' && $creCodigo != '' && is_numeric($creCodigo) && $dataDe != '--' && $dataAte != '--') {
            //die("todos os acordos jur�dicos a vencer do credor selecionado no per�odo selecionado");
            $filtroCredor = "AND cre.cre_cod = $creCodigo";
            $filtro = " WHERE par_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte'";
        } else {
            $mensagem = "Dados insuficientes para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_jur');
        }

        $resultPesquisa = $this->data['acordsJudAVenc'] = $this->md->getRelJur02($filtroCredor, $filtro); //acordsJudAVenc -> acordos jur�dicos a vencer

        if (sizeof($resultPesquisa) == 0) {
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_jur');
        }

        $this->data['title'] = 'Recupera :: Acordos Jur�dicos a Vencer';
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->inicore->loadview('rel_jur02', $this->data);
    }

    function _rel_jur03() {//cobran�a jur�dica por per�odo de ociosidade (n�o tiveram RO no per�odo selecionado).
        $filtroCredor = $this->input->post('cre_filtro03'); //credor espec�fico ou todos os credores
        $creCodigo = $this->input->post('cre_cod03'); // campo Nome: (da view rel_jur). Recebe o c�d do credor selecionado
        $dataDe = convDataParaDb($this->input->post('de03')); //recebe a data e converte para o formato aaaa-mm-dd
        $dataAte = convDataParaDb($this->input->post('ate03')); //recebe a data e converte para o formato aaaa-mm-dd
        $filtro = "";

        if ($filtroCredor == 'todos' && $dataDe != '--' && $dataAte != '--') {
            //die("cobran�as jur�dicas sem movimenta��o de RO no per�odo selecionado de todos os credores");
            $filtro = "WHERE ros.ros_data BETWEEN '$dataDe' AND '$dataAte'";
            $creCodigo = 0; //n�o existe credor de c�digo 0 por isso estou atribuindo esse valor para pegar de todos os credores
        } else if ($filtroCredor == 'credor_especifico' && $creCodigo != '' && is_numeric($creCodigo) && $dataDe != '--' && $dataAte != '--') {
            //die("cobran�as jur�dicas sem movimenta��o de RO no per�odo selecionado e do credor selecionado");
            $filtro = "INNER JOIN credores cre ON(co.credor_cre_cod = cre.cre_cod) WHERE ros.ros_data BETWEEN '$dataDe' AND '$dataAte' AND (cre.cre_cod = $creCodigo)";
        } else {
            $mensagem = "Dados insuficientes para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_jur');
        }

        $resultadoPesquisa = $this->data['cobsJudOciosas'] = $this->md->getRelJur03($filtro, $creCodigo); //cobsJudOciosas = cobran�as jur�dicas sem movimenta��o de RO no per�odo selecionado

        if (sizeof($resultadoPesquisa) == 0) {
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_jur');
        }

        $this->data['title'] = 'Recupera :: Cobran�as por Per�odo de Ociosidade';
        $this->inicore->addcss(array('relatorios')); //CSS
        $this->inicore->loadview('rel_jur03', $this->data);
    }

    //|--------------------------------------------------------------------------------------------------------|
    //|------------------------------------------- RELAT�RIO POR RO -------------------------------------------|
    //|--------------------------------------------------------------------------------------------------------|
    function _rel_ro() {
        $this->data['title'] = "Recupera :: Relat�rio por RO";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput'));
        $this->session->set_userdata('menusel', '31'); //DETERMINA O MENU QUE FICARA ABERTO
        $this->inicore->loadSidebar(); //barra lateral
        $this->data['operacoes'] = $this->md->getOperacoes(); //todos os tipos de opera��es
        $this->inicore->loadview('rel_ro', $this->data);
    }

    function _rel_ro01() {//Relat�rio de Cobran�as por RO
        $dataAtual = date('Y-m-d'); //dia de hoje
        $codTipoOperacao = $this->input->post('comboTipoOperacao01'); //c�digo do tipo de opera��o.(combobox da view)
        $agendOuCadast = $this->input->post('rdbTipoOperacao'); //$agendOuCadast agendado para ou cadastrado em
        $dataDe = convDataParaDb($this->input->post('data_de')); //recebe a data e converte para o formato aaaa-mm-dd
        $dataAte = convDataParaDb($this->input->post('data_ate')); //recebe a data e converte para o formato aaaa-mm-dd
        $dataDesde = convDataParaDb($this->input->post('data_desde')); //recebe a data e converte para o formato aaaa-mm-dd

        if ($codTipoOperacao != '' && is_numeric($codTipoOperacao) && $agendOuCadast == 'agendado_para' && $dataDe != '--' && $dataAte != '--') {
            //die("Cobran�as com RO agendado de(data selecionada) at�(data selecionada)");
            $filtro = " AND R.ros_agendado BETWEEN '$dataDe' AND '$dataAte' ORDER BY R.ros_agendado ASC";
            $agendOuCadast = "Agendamento";
        } else if ($codTipoOperacao != '' && is_numeric($codTipoOperacao) && $agendOuCadast == 'cadastrado_em' && $dataDesde != '--') {
            //die("Cobran�as com RO cadastrado desde a data selecionada at� a data atual");
            $filtro = " AND R.ros_data BETWEEN '$dataDesde' AND '$dataAtual' ORDER BY R.ros_data ASC";
            $agendOuCadast = "Cadastramento";
        } else {
            //die("Dados insuficientes para realizar uma pesquisa");
            $mensagem = "Dados insuficientes para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ro');
        }

        $resultPesquisa = $this->data['relPorRo'] = $this->md->getRelRo01($filtro, $codTipoOperacao); //relPorRo -> relat�rio por RO

        if (sizeof($resultPesquisa) == 0) {
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ro');
        }

        $this->data['title'] = 'Recupera :: Relat�rio de Cobran�as por RO';
        $this->data['agendOuCadast'] = $agendOuCadast;
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->inicore->loadview('rel_ro01', $this->data);
    }

    //|--------------------------------------------------------------|
    //|------------------- RELAT�RIOS GERENCIAIS -------------------|
    //|--------------------------------------------------------------|
    function _rel_ger() {
        $this->data['title'] = "Recupera :: Relat�rios Gerenciais";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput'));
        $this->session->set_userdata('menusel', '31'); //DETERMINA O MENU QUE FICARA ABERTO
        $this->inicore->loadSidebar(); //barra lateral
        //$this->data['credores'] = $this->md->getCredores(); //pega todos os credores e seus c�digos.(tabela credores)
        $this->data['recuperadores'] = $this->md->getUsuarios(); //pega todos os usu�rios e seus c�digo. (tabela usu�rios), para usar no filtro de recuperador
        $this->data['operacoes'] = $this->md->getOperacoes(); //pega os tipos de opera��es (tabela operacoes), imprimir no combo do relat�rio rel_ger08
        $this->inicore->loadview('rel_ger', $this->data);
    }

    function _rel_ger01() {//Relat�rio de Cobran�as Atrasadas
        $dataDe = convDataParaDb($this->input->post('de01')); //recebe a data e converte para o formato aaaa-mm-dd
        $dataAte = convDataParaDb($this->input->post('ate01')); //recebe a data e converte para o formato aaaa-mm-dd
        $filtroCredor = $this->input->post('cre_filtro01'); //todos ou credor_especifico
        $codCredor = $this->input->post('cre_cod01'); //campo nome da view, aqui tem que chegar o c�d do credor.
        $codRecuperador = $this->input->post('recuperador01'); //todos os recuperadores ou recuperador espec�fico. combobox na view. Aqui recebe o c�digo do recuperador
        $filtro = "";

        if ($filtroCredor == "todos" && $codRecuperador == "todos" && $dataDe != '--' && $dataAte != '--') {
//            die("filtro 1: acordos e previs�es de todos os credores e todos os recuperadores");
            $filtro = "";
        } else if ($filtroCredor == "todos" && is_numeric($codRecuperador) && $dataDe != '--' && $dataAte != '--') {
            //die("filtro 2: acordos e previs�es de todos os credores do recuperador selecionado");
            $filtro = "WHERE cre.usuarios_responsavel_cod = $codRecuperador";
        } else if ($filtroCredor == "credor_especifico" && $codCredor != "" && is_numeric($codCredor) && $dataDe != '--' && $dataAte != '--') {
            //die("filtro 3: acordos e previs�es do credor espec�fico");
            $filtro = "WHERE cre.cre_cod = $codCredor";
        } else {
            //die("Dados insuficientes para realizar uma pesquisa");
            $mensagem = "Dados insuficientes para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ger');
        }

        $resultPesquisa = $this->data['cobsAtras'] = $this->md->getRelGer01($filtro, $dataDe, $dataAte); //cobsAtras -> cobran�as atrasadas

        if (sizeof($resultPesquisa) == 0) {
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ger');
        }

        $this->data['title'] = 'Recupera :: Relat�rio de Cobran�as Atrasadas';
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->inicore->loadview('rel_ger01', $this->data);
    }

    function _rel_ger02() {//Relat�rio de Acordos e Previs�es a Vencer
        $dataDe = convDataParaDb($this->input->post('de02')); //recebe a data e converte para o formato aaaa-mm-dd
        $dataAte = convDataParaDb($this->input->post('ate02')); //recebe a data e converte para o formato aaaa-mm-dd
        $filtroCredor = $this->input->post('cre_filtro02'); //todos ou credor_especifico
        $codCredor = $this->input->post('cre_cod02'); //campo nome da view, aqui tem que chegar o c�d do credor.
        $codRecuperador = $this->input->post('recuperador02'); //todos os recuperadores ou recuperador espec�fico. combobox na view. Aqui recebe o c�digo do recuperador
        $filtro = "";

        if ($filtroCredor == "todos" && $codRecuperador == "todos" && $dataDe != '--' && $dataAte != '--') {
            //die("filtro 1: acordos e previs�es de todos os credores e todos os recuperadores");
            $filtro = "";
        } else if ($filtroCredor == "todos" && is_numeric($codRecuperador) && $dataDe != '--' && $dataAte != '--') {
            //die("filtro 2: acordos e previs�es de todos os credores do recuperador selecionado");
            $filtro = "WHERE cre.usuarios_responsavel_cod = $codRecuperador";
        } else if ($filtroCredor == "credor_especifico" && $codCredor != "" && is_numeric($codCredor) && $dataDe != '--' && $dataAte != '--') {
            //die("filtro 3: acordos e previs�es do credor espec�fico");
            $filtro = "WHERE cre.cre_cod = $codCredor";
        } else {
            //die("Dados insuficientes para realizar uma pesquisa");
            $mensagem = "Dados insuficientes para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ger');
        }

        $resultPesquisa = $this->data['acoPrevVencer'] = $this->md->getRelGer02($filtro, $dataDe, $dataAte); //AcoPrevVencer -> Acordos e previs�es a vencer

        if (sizeof($resultPesquisa) == 0) {
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ger');
        }

        $this->data['title'] = 'Relat�rio de Acordos e Previs�es a Vencer';
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->inicore->loadview('rel_ger02', $this->data);
    }

    function _rel_ger03() {//Cobran�as sem Acordo
        $dataDe = convDataParaDb($this->input->post('de03')); //recebe a data e converte para o formato aaaa-mm-dd
        $dataAte = convDataParaDb($this->input->post('ate03')); //recebe a data e converte para o formato aaaa-mm-dd
        $filtroCredor = $this->input->post('cre_filtro03'); //todos ou credor_especifico
        $codCredor = $this->input->post('cre_cod03'); //campo nome da view, aqui tem que chegar o c�d do credor.
        $codRecuperador = $this->input->post('recuperador03'); //todos os recuperadores ou recuperador espec�fico. combobox na view. Aqui recebe o c�digo do recuperador
        $filtro = "";

        if ($filtroCredor == "todos" && $codRecuperador == "todos" && $dataDe != '--' && $dataAte != '--') {
            //die("Cobran�as sem acordo de todos os credores e todos os recuperadores no per�odo especificado");
            $filtro = "";
        } else if ($filtroCredor == "todos" && is_numeric($codRecuperador) && $dataDe != '--' && $dataAte != '--') {
            //die("filtro 2: Cobran�as sem acordo de todos os credores do recuperador selecionado no per�odo selecionado");
            $filtro = "WHERE cre.usuarios_responsavel_cod = $codRecuperador";
        } else if ($filtroCredor == "credor_especifico" && $codCredor != "" && is_numeric($codCredor) && $dataDe != '--' && $dataAte != '--') {
            //die("filtro 3: Cobran�as sem acordo do credor selecionado no per�odo selecionado");
            $filtro = "WHERE cre.cre_cod = $codCredor";
        } else {
            //die("Dados insuficientes para realizar uma pesquisa");
            $mensagem = "Dados insuficientes para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ger');
        }
        $resultPesquisa = $this->md->getRelGer03($filtro, $dataDe, $dataAte);

        if ($resultPesquisa == 0) {//Se n�o encontrar nada na pesquisa
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ger');
        }

        $this->data['title'] = 'Recupera :: Cobran�a sem Acordo';
        $this->data['cobsSemAcord'] = $resultPesquisa[0]; //cobsSemAcord = cobran�as sem acordo.
        $this->data['dezUltimosRos'] = $resultPesquisa[1]; //Os 10 �ltimos ROS de cada cobran�as.
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->inicore->loadview('rel_ger03', $this->data);
    }

    function _rel_ger04() {//Pega as d�vidas virgens e trabalhadas,todos os acordos. A pesquisa � feita pela data de cadastro da d�vida e do acordo
        $dataDe = convDataParaDb($this->input->post('de04')); //recebe a data e converte para o formato aaaa-mm-dd
        $dataAte = convDataParaDb($this->input->post('ate04')); //recebe a data e converte para o formato aaaa-mm-dd
        $filtroCredor = $this->input->post('cre_filtro04'); //todos ou credor_especifico
        $codCredor = $this->input->post('cre_cod04'); //campo nome da view, aqui tem que chegar o c�d do credor.
        $codRecuperador = $this->input->post('recuperador04'); //todos os recuperadores ou recuperador espec�fico. combobox na view. Aqui recebe o c�digo do recuperador
        $trabalhada = $this->input->post('trabalhada04'); //values = trabalhada ou ""
        $filtro = "";

        if ($filtroCredor == "todos" && $codRecuperador == "todos" && $dataDe != '--' && $dataAte != '--') {
            //die("Filtro 1: (todos os credores e todos os recuperadores) D�vidas virgens, d�vidas trabalhas e acordos trabalhados, todos tem a data de cadastro no per�odo selecionado.");
            $filtro = "";
        } else if ($filtroCredor == "todos" && is_numeric($codRecuperador) && $dataDe != '--' && $dataAte != '--') {
            //die("Filtro 2: (todos os credores do recuperador selecionado) D�vidas virgens, d�vidas trabalhas e acordos trabalhados, todos tem a data de cadastro no per�odo selecionado.");
            $filtro = "WHERE cre.usuarios_responsavel_cod = $codRecuperador";
        } else if ($filtroCredor == "credor_especifico" && $codCredor != "" && is_numeric($codCredor) && $dataDe != '--' && $dataAte != '--') {
            //die("Filtro 3: (credor espec�fico) D�vidas virgens, d�vidas trabalhas e acordos trabalhados, todos tem a data de cadastro no per�odo selecionado.");
            $filtro = "WHERE cre.cre_cod = $codCredor";
        } else {
            //die("Dados insuficientes para realizar uma pesquisa");
            $mensagem = "Dados insuficientes para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ger');
        }
        $resultPesquisa = $this->md->getRelGer04($filtro, $dataDe, $dataAte, $trabalhada);

        $this->data['dividasVirgens'] = $dividasVirgens = ($resultPesquisa["dividasVirgens"]);
        $this->data['dividasTrabalhadas'] = $dividasTrabalhadas = ($resultPesquisa["dividasTrabalhadas"]);
        $this->data['todosAcordos'] = $todoAcordos = ($resultPesquisa["todosAcordos"]);
        $this->data['dezUltimosRos'] = $dezUltimosRos = ($resultPesquisa["dezUltimosRos"]);
        $this->data['trabalhada'] = $trabalhada;

        if (sizeof($dividasVirgens) == 0 && sizeof($dividasTrabalhadas) == 0 && sizeof($todoAcordos) == 0 && sizeof($dezUltimosRos) == 0) {//se n�o encontrar nada na pesquisa
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ger');
        }

        $this->data['title'] = 'Cobran�a por Per�odo';
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->inicore->loadview('rel_ger04', $this->data);
    }

    function _rel_ger05() {//Todas as cobran�as que n�o receberam movimenta��o de ro no per�odo selecionado.
        $dataDe = convDataParaDb($this->input->post('de05')); //recebe a data e converte para o formato aaaa-mm-dd
        $dataAte = convDataParaDb($this->input->post('ate05')); //recebe a data e converte para o formato aaaa-mm-dd
        $filtroCredor = $this->input->post('cre_filtro05'); //todos ou credor_especifico
        $codCredor = $this->input->post('cre_cod05'); //campo nome da view, aqui tem que chegar o c�d do credor.
        $codRecuperador = $this->input->post('recuperador05'); //todos os recuperadores ou recuperador espec�fico. combobox na view. Aqui recebe o c�digo do recuperador
        $filtro = "";

        if ($dataDe == '' || $dataAte == '') {
            //die("Dados insuficientes para realizar uma pesquisa");
            $mensagem = "Dados insuficientes para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ger');
        }

        if (is_numeric($codRecuperador)) {
            $filtro .= " AND CR.usuarios_responsavel_cod = $codRecuperador";
        }
        if ($filtroCredor == "credor_especifico" && $codCredor != "" && is_numeric($codCredor)) {
            $filtro .= " AND C.credor_cre_cod = $codCredor";
        }

        $resultPesquisa = $this->md->getRelGer05($filtro, $codCredor, $codRecuperador, $dataDe, $dataAte);

        foreach ($resultPesquisa as $cobranca) {
            $dezUltimosRos[$cobranca->cob_cod] = $this->md->getDezUltimosRos($cobranca->cob_cod);
        }

//        echo '<pre>';
//        print_r($dezUltimosRos);
//        die('');

        if (sizeof($resultPesquisa) == 0) {//Se n�o encontrar nada na pesquisa
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ger');
        }

        $this->data['title'] = 'Recupera :: Cobran�a por per�odo de ociosidade';
        $this->data['cobsOciosas'] = $resultPesquisa; //cobsOciosas = cobran�as ociosas
        $this->data['dezUltimosRos'] = $dezUltimosRos; //Os 10 �ltimos ROS de cada cobran�as.
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->inicore->loadview('rel_ger05', $this->data);
    }

    function _rel_ger06() {
        $dataDe = convDataParaDb($this->input->post('de06')); //recebe a data e converte para o formato aaaa-mm-dd
        $dataAte = convDataParaDb($this->input->post('ate06')); //recebe a data e converte para o formato aaaa-mm-dd
        $filtroCredor = $this->input->post('cre_filtro06'); //todos ou credor_especifico
        $codCredor = $this->input->post('cre_cod06'); //campo nome da view, aqui tem que chegar o c�d do credor.
        $codRecuperador = $this->input->post('recuperador06'); //todos os recuperadores ou recuperador espec�fico. combobox na view. Aqui recebe o c�digo do recuperador
        $filtro = "";

        if ($filtroCredor == "todos" && $codRecuperador == "todos" && $dataDe != '--' && $dataAte != '--') {
            //die("Filtro 1: Todos os recebimento de todos os credores e todos os recuperadores no per�odo selecionado");
            $filtro = "";
        } else if ($filtroCredor == "todos" && is_numeric($codRecuperador) && $dataDe != '--' && $dataAte != '--') {
            //die("Filtro 2: Todos os recebimentos de todos os credores do recuperador selecionado no per�odo selecionado");
            $filtro = "AND rece.usuarios_usu_cod = $codRecuperador";
        } else if ($filtroCredor == "credor_especifico" && $codCredor != "" && is_numeric($codCredor) && $dataDe != '--' && $dataAte != '--') {
            //die("Filtro 3: Todos os recebimentos do credor selecionado");
            $filtro = "AND rece.credor_cre_cod = $codCredor";
        } else {
            //die("Dados insuficientes para realizar uma pesquisa");
            $mensagem = "Dados insuficientes para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ger');
        }
        $pesquisa = $this->md->getRelGer06($filtro, $dataDe, $dataAte);

        if ($pesquisa == 0) {//Se n�o encontrar nada na pesquisa
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ger');
        }

        $recebimentos = $pesquisa["recebimentos"];
        $parcelasRecebidas = $pesquisa["parcelasRecebidas"];
        $this->data['title'] = 'Recupera :: Rendimento por Recuperador';
        $this->data['recebimentos'] = $recebimentos;
        $this->data['parcelasRecebidas'] = $parcelasRecebidas;
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->inicore->loadview('rel_ger06', $this->data);
    }

    function _rel_ger07() {
        $dataDe = convDataParaDb($this->input->post('de07')); //recebe a data e converte para o formato aaaa-mm-dd
        $dataAte = convDataParaDb($this->input->post('ate07')); //recebe a data e converte para o formato aaaa-mm-dd
        $filtroCredor = $this->input->post('cre_filtro07'); //todos ou credor_especifico
        $codCredor = $this->input->post('cre_cod07'); //campo nome da view, aqui tem que chegar o c�d do credor.
        $codRecuperador = $this->input->post('recuperador07'); //todos os recuperadores ou recuperador espec�fico. combobox na view. Aqui recebe o c�digo do recuperador
        $filtroInadimplente = $this->input->post('ina_filtro07'); //todos ou ina_especifico
        $inaCod = $this->input->post('ina_nome07'); //campo nome do inadimplente na view, aqui tem que chegar o c�d do inadimplente
        $filtro = "";


        if ($filtroCredor == "todos" && $codRecuperador == "todos" && $dataDe != '--' && $dataAte != '--' && $filtroInadimplente == "ina_todos") {
            //die("Filtro 1: todos credores, todos recuperadores, todos inadimplentes. (per�odo selecionado)");
            $filtro = "";
        } else if ($filtroCredor == "todos" && is_numeric($codRecuperador) && $dataDe != '--' && $dataAte != '--') {
            //die("Filtro 2: todos os credores do recuperador selecionado. (per�odo selecionado)");
            $filtro = "AND (ace.usuarios_responsavel_cod = $codRecuperador)";
        } else if ($filtroCredor == "credor_especifico" && $codCredor != "" && is_numeric($codCredor) && $dataDe != '--' && $dataAte != '--') {
            //die("Filtro 3: credor selecionado. (per�odo selecionado)");
            $filtro = "AND (cre.cre_cod = $codCredor)";
        } else if ($filtroInadimplente == "ina_especifico" && is_numeric($inaCod) && $dataDe != '--' && $dataAte != '--') {
            //die("Filtro 4: inadimplente. (per�odo selecionado)");
            $filtro = "AND (i.ina_cod = $inaCod)";
        } else {
            $mensagem = "Dados insuficientes para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ger');
        }

        $acessosCredores = $this->md->getRelGer07($filtro, $dataDe, $dataAte);

        if (sizeof($acessosCredores) == 0) {//se n�o encontar nada na pesquisa
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ger');
        }
        $this->data['title'] = 'Recupera :: Relat�rios de Acessos dos Credores';
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->data['acessosCredores'] = $acessosCredores;
        $this->inicore->loadview('rel_ger07', $this->data);
    }

    //|---------------------------------------------------------------------------------|
    //|----------------------- RELAT�RIOS DE PRESTA��O DE CONTAS -----------------------|
    //|---------------------------------------------------------------------------------|

    function _rel_prest() {//View com os relatorios de presta��o de contas
        $this->data['title'] = "Recupera :: Relat�rios de presta��o de contas";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput'));
        $this->session->set_userdata('menusel', '31'); //DETERMINA O MENU QUE FICARA ABERTO
        $this->inicore->loadSidebar(); //barra lateral
        $this->data['credores'] = $this->md->getCredores(); //pega todos os credores e seus c�digos.(tabela credores)
        $this->inicore->loadview('rel_prest', $this->data);
    }

    function _rel_prest01() {

        $filtroCredor = $this->input->post('cre_filtro01'); //credor espec�fico
        $creCodigo = $this->input->post('cre_cod01'); // campo Nome: (da view rel_adm). Recebe o c�d do credor selecionado
        $dataDe = convDataParaDb($this->input->post('de01')); //recebe a data e converte para o formato aaaa-mm-dd
        $dataAte = convDataParaDb($this->input->post('ate01')); //recebe a data e converte para o formato aaaa-mm-dd
//        echo "$filtroCredor".'<br/>'. "$dataDe".'<br/>'."$dataAte";die();
        if ($creCodigo != "" && is_numeric($creCodigo) && $dataDe != '--' && $dataAte != '--') {
            $filtro = " AND R.credor_cre_cod = $creCodigo";
        } else if ($filtroCredor == "todos" && $dataDe != '--' && $dataAte != '--') {
            $filtro = '';
        } else {
            $mensagem = "Dados insuficientes para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_adm');
        }

        $resultPesquisa = $this->md->rel_prest01($filtro, $dataDe, $dataAte);

        foreach ($resultPesquisa as $resultado) {
            $parcelas = explode('-', $resultado->reb_parcelas);

            $i = 0;

            foreach ($parcelas as $parcela) {
                if ($parcela != '') {
                    $parcelasPagas[$resultado->reb_cod][$i] = $this->md->getParcInfo($parcela);

                    $i++;
                }
            }
        }

        if (sizeof($resultPesquisa) == 0) {
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_adm');
        }
        $this->data['title'] = 'Recupera :: Relat�rio de presta��o de contas';
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->data['pesqRes'] = $resultPesquisa;
        $this->data['ParPagas'] = isset($parcelasPagas) ? $parcelasPagas : '*Nenhuma parcela especificada*';
        $this->data['credor'] = $this->md->getCredInfo($creCodigo);
        $this->data['recuperador'] = $this->session->userdata('usuwho');
        $this->inicore->loadview('rel_prest01', $this->data);
    }

    function _rel_prest03() {
        /* Mostra os acordos e previs�es de hoje at� a data selecionada.
         * OBS: N�o pega as cobran�as atrasadas. */

        $dataDe = date("Y-m-d", time()); //data de hoje, pega do sistema operacional no formato aaa-mm-dd
        $dataAte = convDataParaDb($this->input->post('ate03')); //recebe a data e converte para o formato aaaa-mm-dd
        $filtroCredor = $this->input->post('cre_filtro03'); //todos ou credor_especifico
        $codCredor = $this->input->post('cre_cod03'); //campo nome da view, aqui tem que chegar o c�d do credor.
        $filtro = "";

        /* ##################### Verificando se os campos est�o preenchidos corretamente ##################### */
        /* OBS: O c�digo do credor pode ser vazio que n�o interfere na verifica��o de se � num�rio ou n�o */
        if ($dataAte == '--' || $filtroCredor == '' || ($filtroCredor != 'todos' && $filtroCredor != 'credor_especifico') || $codCredor != is_numeric($codCredor)) {
            $mensagem = "Dados insuficientes ou fora de padr�o, para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_prest');
        }
        /* ##################### Fim do verificando se os campos est�o preenchidos corretamente ##################### */

        /* ++++++++++++++++++++ Verificando se a data at� � menor que a data de hoje ++++++++++++++++++++ */
        //removendo os "-" das datas para compar�-las.
        $De = str_replace('-', '', $dataDe);
        $Ate = str_replace('-', '', $dataAte);

        if ($De > $Ate) {
            $mensagem = "A data final n�o pode ser menor que a data atual";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_prest');
        }
        /* ++++++++++++++++++++ Fim do verificando se a data at� � menor que a data de hoje ++++++++++++++++++++ */

        if ($filtroCredor == "todos") {
            //die("todos os credores");
            $filtro = "";
        } else if ($filtroCredor == "credor_especifico") {
            //die("credor selecionado");
            if ($codCredor == '') {
                $mensagem = "Informe um credor!";
                $this->inicore->setMensagem('error', $mensagem);
                redirect(base_url() . 'relatorios/rel_prest');
            }
            $filtro = "WHERE cre.cre_cod = $codCredor";
        }

        $resultPesquisa = $this->data['acoPrevVencer'] = $this->md->getRelPrest03($filtro, $dataDe, $dataAte); //AcoPrevVencer -> Acordos e previs�es a vencer

        if (sizeof($resultPesquisa) == 0) {
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_prest');
        }

        $this->data['title'] = 'Recupera :: Relat�rio de Presta��o de Contas dos Acordos e Previs�es a Vencer';
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->inicore->loadview('rel_prest03', $this->data);
    }

    //|----------------------------------------------------------------------------------|
    //|------------------------- RELAT�RIOS DE ACESSO DO CREDOR -------------------------|
    //|----------------------------------------------------------------------------------|
    function _rel_ace_cre() {
        $this->data['title'] = "Recupera :: Relat�rio de acessos dos credores";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput'));
        $this->session->set_userdata('menusel', '31'); //DETERMINA O MENU QUE FICARA ABERTO
        $this->inicore->loadSidebar(); //barra lateral
        //$this->data['credores'] = $this->md->getCredores(); //pega todos os credores e seus c�digos.(tabela credores)
        $this->data['recuperadores'] = $this->md->getUsuarios(); //pega todos os usu�rios e seus c�digo. (tabela usu�rios), para usar no filtro de recuperador
        $this->data['tipo'] = 'credor';
        $this->inicore->loadview('rel_ace', $this->data);
    }
    
    function _rel_ace_usu() {
        $this->data['title'] = "Recupera :: Relat�rio de acessos dos credores";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput'));
        $this->session->set_userdata('menusel', '31'); //DETERMINA O MENU QUE FICARA ABERTO
        $this->inicore->loadSidebar(); //barra lateral
        //$this->data['credores'] = $this->md->getCredores(); //pega todos os credores e seus c�digos.(tabela credores)
        $this->data['recuperadores'] = $this->md->getUsuarios(); //pega todos os usu�rios e seus c�digo. (tabela usu�rios), para usar no filtro de recuperador
        $this->data['tipo'] = 'usuario';
        $this->inicore->loadview('rel_ace', $this->data);
    }

    function _rel_ace01() {
        $dataDe = convDataParaDb($this->input->post('de07')); //recebe a data e converte para o formato aaaa-mm-dd
        $dataAte = convDataParaDb($this->input->post('ate07')); //recebe a data e converte para o formato aaaa-mm-dd
        $filtroCredor = $this->input->post('cre_filtro07'); //todos ou credor_especifico
        $codCredor = $this->input->post('cre_cod07'); //campo nome da view, aqui tem que chegar o c�d do credor.
        $filtroInadimplente = $this->input->post('ina_filtro07'); //todos ou ina_especifico
        $inaCod = $this->input->post('ina_cod07'); //campo nome do inadimplente na view, aqui tem que chegar o c�d do inadimplente
        $filtro = "";


        if ($filtroCredor == "todos" && $dataDe != '--' && $dataAte != '--' && $filtroInadimplente == "ina_todos") {
            //die("Filtro 1: todos credores todos os inadimplentes (per�odo selecionado)");
            $filtro = "";
        } else if ($filtroCredor == "credor_especifico" && is_numeric($codCredor) && $dataDe != '--' && $dataAte != '--' && $filtroInadimplente == "todos") {
            //die("Filtro 2: credor selecionado(per�odo selecionado) [combina��o na view credor selecionado, todos inadimplentes]");
            $filtro = "AND (cre.cre_cod = $codCredor)";
        } else if ($filtroCredor == "todos" && $dataDe != '--' && $dataAte != '--' && $filtroInadimplente == "ina_especifico" && is_numeric($inaCod)) {
            //die("Filtro 3: inadimplente selecionado. (per�odo selecionado) [combina��o na view todos credores, inadimplentes selecionado]");
            $filtro = "AND (i.ina_cod = $inaCod)";
        } else {
            $mensagem = "Dados insuficientes para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ace');
        }

        $acessosCredores = $this->md->getRelAce01($filtro, $dataDe, $dataAte);

        if (sizeof($acessosCredores) == 0) {
            $mensagem = "Nada encontrado";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ace');
        }

        $this->data['title'] = 'Recupera :: Relat�rios de Acessos dos Credores';
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->data['acessosCredores'] = $acessosCredores;
        $this->inicore->loadview('rel_ace01', $this->data);
    }

    //|---------------------------------------------------------------------------------------|
    //|------------------------- RELAT�RIOS DE ACESSO DO RECUPERADOR -------------------------|
    //|---------------------------------------------------------------------------------------|
    function _rel_ace_02() {

        $dataDe = convDataParaDb($this->input->post('de01')); //recebe a data e converte para o formato aaaa-mm-dd
        $dataAte = convDataParaDb($this->input->post('ate01')); //recebe a data e converte para o formato aaaa-mm-dd
        $recup_filtro01 = $this->input->post('recuperador01') == 'todos' ? 'todos' : 'recuperador_especifico'; //todos ou recuperador_especifico
        $recup_nome01 = $this->input->post('recuperador01'); //campo nome da view, aqui tem que chegar o c�d do recuperador.
        $filtro; //O valor dessa variavel sera definido nas verificacoes if else
        $relAcessosRecuperadores; // vai receber o resultado da consulta sql

        if ($recup_filtro01 == "todos" && $dataDe != '--' && $dataAte != '--' && ($dataDe <= $dataAte)) {
//                die("Acesso de todos os recuperadores no periodo selecionado");
            $filtro = ""; //Recebe vazio para pesquisar os acessos de todos os recuperadores
        } else if ($recup_filtro01 == "recuperador_especifico" && is_numeric($recup_nome01) && $dataDe != '--' && $dataAte != '--' && ($dataDe <= $dataAte)) {
//                die("Acesso do recuperador selecionado no periodo selecionado");
            $filtro = " AND acu.usuarios_usu_cod = $recup_nome01"; //recebe a condicao para pesquisar os acessos do recuperador selecionado
        } else {
            /* Se a data de for maior que a data ate, tambem vai entrar aqui */
            $mensagem = "Dados insuficientes ou fora do padr�o para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ace');
        }

        $relAcessosRecuperadores = $this->md->_acessosRecuperadores($dataDe, $dataAte, $filtro); //fazendo a consulta sql

        if (sizeof($relAcessosRecuperadores) == 0) {//se nao encontrar nada redireciona e da uma mensagem
            $this->inicore->setMensagem('error', 'Nada encontrado.');
            redirect(base_url() . 'relatorios/rel_ace');
        }
        $this->data['relAcessosRecuperadores'] = $relAcessosRecuperadores;
        $this->data['title'] = 'Recupera :: Relat�rios de Acessos dos Recuperadores';
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->inicore->loadview('rel_ace02', $this->data);
    }

    function _rel_import() {

        $this->data['title'] = "Recupera :: Relat�rio de Importa��o";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput'));
        $this->session->set_userdata('menusel', '31'); //DETERMINA O MENU QUE FICARA ABERTO
        $this->inicore->loadSidebar(); //barra lateral
        //$this->data['credores'] = $this->md->getCredores(); //pega todos os credores e seus c�digos.(tabela credores)
//        $this->data['recuperadores'] = $this->md->getUsuarios(); //pega todos os usu�rios e seus c�digo. (tabela usu�rios), para usar no filtro de recuperador
        $this->inicore->loadview('rel_import', $this->data);
    }

    function _rel_import1() {

        /* Se for pesquisar por credor ou inadimplente, aqui deve chergar o c�d,
         * se n�o chegar o c�d, n�o vai entrar na condi��o para (AND cod_credor = algum c�d) ou inadimplente
         * e vai trazer todos
         */

        $filtro = "";
        $filtroImportComErros = "";
        $importacoes = "";

        $creCod = ""; //caso escolha um credor para pesquisar
        $inaCod = ""; //caso escolha um inadimplente para pesquisar
        $dataDe = "";
        $dataAte = "";
        $tipo = ""; //todos, sucesso ou erro. (importa��o)

        $creCod = $this->input->post('cre_cod01');
        $inaCod = $this->input->post('ina_cod01');
        $dataDe = $this->input->post('de01');
        $dataAte = $this->input->post('ate01');
        $tipo = $this->input->post('tipo01');

        if ($dataDe != "" && $dataAte != "" && strtotime($dataDe) <= strtotime($dataAte)) {

            $dataDe = convDataParaDb($dataDe);
            $dataAte = convDataParaDb($dataAte);
            $filtro = "(i.imp_cadastro BETWEEN '$dataDe' AND '$dataAte')";
            $filtroImportComErros = "er.data BETWEEN '$dataDe' AND '$dataAte'";


            if (is_numeric($creCod)) {
                $filtro .= " AND (imp_div.div_cre_cod = $creCod)";
                $filtroImportComErros .= " AND er.cre_cod = $creCod";
            }

            if (is_numeric($inaCod)) {
                $filtro .= " AND (i.imp_inad_cod = $inaCod)";
                $filtroImportComErros .= " AND er.ina_cod = $inaCod";
            }

            $importacoes = $this->md->relImp1($filtro, $filtroImportComErros, $tipo);
        } else {
            $this->inicore->setMensagem('error', 'Dados insuficientes ou fora de padr�o para realizar uma pesquisa');
            redirect(base_url() . 'relatorios/rel_import');
        }

        $importacoesSucesso = $importacoes['credoresComImportacao'];
        $importacoesErro = $importacoes['credoresImportErros'];

        if ($tipo == 99 && $importacoesSucesso == "" && $importacoesErro == "") {
            $this->inicore->setMensagem('error', 'Nada encontrado.');
            redirect(base_url() . 'relatorios/rel_import');
        } else if ($tipo == 0 && $importacoesSucesso == "") {
            $this->inicore->setMensagem('error', 'Nada encontrado.');
            redirect(base_url() . 'relatorios/rel_import');
        } else if ($tipo == 1 && $importacoesErro == "") {
            $this->inicore->setMensagem('error', 'Nada encontrado.');
            redirect(base_url() . 'relatorios/rel_import');
        }

        $this->data['importacoes'] = $importacoesSucesso;
        $this->data['importacoesErro'] = $importacoesErro;
        $this->data['title'] = 'Recupera :: Relat�rios de Importa��es';
        $this->inicore->addcss(array('relatorios')); // CSS
        $this->inicore->loadview('rel_import01', $this->data);
    }

    /* --------------------------------------------------------------------- */
    /* --------------- RELATORIOS DE REULTADOS DE CONTATOS --------------- */
    /* --------------------------------------------------------------------- */

    function _rel_contat() {
        $this->data['title'] = "Recupera :: Relat�rios de Resultados de contatos";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput'));
        $this->session->set_userdata('menusel', '31'); //DETERMINA O MENU QUE FICARA ABERTO
        $this->inicore->loadSidebar(); //barra lateral
        //$this->data['credores'] = $this->md->getCredores(); //pega todos os credores e seus c�digos.(tabela credores)
        $this->data['recuperadores'] = $this->md->getUsuarios(); //pega todos os usu�rios e seus c�digo. (tabela usu�rios), para usar no filtro de recuperador
        $this->data['operacoes'] = $this->md->getOperacoes(); //pega os tipos de opera��es (tabela operacoes), imprimir no combo do relat�rio rel_ger08
        $this->inicore->loadview('rel_contat', $this->data);
    }

    function _rel_contat01() {

        if ($this->input->post('de08') != '' && $this->input->post('ate08') != '') {

            $credores = '';
            $usuariosComRoRecuperador = '';
            $rosUsuarioCredor = '';
            $dataDe = $this->input->post('de08');
            $dataAte = $this->input->post('ate08');
            
            $dataDe = convDataParaDb($dataDe);
            $dataAte = convDataParaDb($dataAte);
//            $creCod = $this->input->post('ate08'); // 4
//            $usuCod = $this->input->post('ate08'); // 2 

            if ($this->input->post('cre_filtro08') != 'todos') {
                $filtroCredor = " AND CR.cre_cod = '" . $this->input->post('cre_cod08') . "'";
            } else {
                $filtroCredor = '';
            }

            if ($this->input->post('recuperador08') != 'todos') {
                $filtroUsuarios = "AND R.usuarios_usu_cod = '" . $this->input->post('recuperador08') . "'";
            } else {
                $filtroUsuarios = '';
            }

            if (($this->input->post('ina_filtro08') != 'ina_todos') && ($this->input->post('ina_cod08') != '')) {
                $filtroInadimplente = "AND C.inadimplentes_ina_cod = '" . $this->input->post('ina_cod08') . "'";
            } else {
                $filtroInadimplente = '';
            }

            if ($this->input->post('tipoOperacao08') != '0') {
                $filtroOperacao = "AND R.operacoes_ope_cod = '" . $this->input->post('tipoOperacao08') . "'";
            } else {
                $filtroOperacao = '';
            }

            $credores = $this->md->getCredoresGer08($dataDe, $dataAte, $filtroCredor, $filtroInadimplente, $filtroOperacao, $filtroUsuarios);

            foreach ($credores as $credor) {
                $usuariosComRoRecuperador[$credor->credor_cre_cod] = $this->md->getUsuariosRos($dataDe, $dataAte, $credor->credor_cre_cod, $filtroUsuarios, $filtroInadimplente, $filtroOperacao);
            }

            foreach ($credores as $credor) {
                foreach ($usuariosComRoRecuperador[$credor->credor_cre_cod] as $usuario) {
                    $rosUsuarioCredor[$credor->credor_cre_cod . $usuario->usuarios_usu_cod] = $this->md->getRosCredorUsuario($dataDe, $dataAte, $credor->credor_cre_cod, $usuario->usuarios_usu_cod, $filtroInadimplente, $filtroOperacao);
                }
            }

            if (($credores != '') && ($usuariosComRoRecuperador != '') && ($rosUsuarioCredor != '')) {

                $this->data['credores'] = $credores;
                $this->data['recuperadoresComRo'] = $usuariosComRoRecuperador;
                $this->data['roUsuariosCredor'] = $rosUsuarioCredor;

                $this->data['title'] = 'Recupera :: Relat�rio de Resultados de Contatos';
                $this->inicore->addcss(array('relatorios')); // CSS
                $this->inicore->loadview('rel_ger08', $this->data);
            } else {
                $mensagem = "N�o foram encontrados dados para esta pesquisa!";
                $this->inicore->setMensagem('error', $mensagem);
                redirect(base_url() . 'relatorios/rel_contat');
            }
        } else {
            $mensagem = "Dados insuficientes para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_contat');
        }
    }

    function _rel_ger08() {

        if ($this->input->post('de08') != '' && $this->input->post('ate08') != '') {

            $credores = '';
            $usuariosComRoRecuperador = '';
            $rosUsuarioCredor = '';
            $dataDe = $this->input->post('de08');
            $dataAte = $this->input->post('ate08');
            
            $dataDe = convDataParaDb($dataDe);
            $dataAte = convDataParaDb($dataAte);

//            $creCod = $this->input->post('ate08'); // 4
//            $usuCod = $this->input->post('ate08'); // 2 

            if ($this->input->post('cre_filtro08') != 'todos') {
                $filtroCredor = " AND CR.cre_cod = '" . $this->input->post('cre_cod08') . "'";
            } else {
                $filtroCredor = '';
            }

            if ($this->input->post('recuperador08') != 'todos') {
                $filtroUsuarios = "AND R.usuarios_usu_cod = '" . $this->input->post('recuperador08') . "'";
            } else {
                $filtroUsuarios = '';
            }

            if (($this->input->post('ina_filtro08') != 'ina_todos') && ($this->input->post('ina_cod08') != '')) {
                $filtroInadimplente = "AND C.inadimplentes_ina_cod = '" . $this->input->post('ina_cod08') . "'";
            } else {
                $filtroInadimplente = '';
            }

            if ($this->input->post('tipoOperacao08') != '0') {
                $filtroOperacao = "AND R.operacoes_ope_cod = '" . $this->input->post('tipoOperacao08') . "'";
            } else {
                $filtroOperacao = '';
            }

            $credores = $this->md->getCredoresGer08($dataDe, $dataAte, $filtroCredor, $filtroInadimplente, $filtroOperacao, $filtroUsuarios);
//
//                   echo '<pre>';
//                   print_r($credores);
//                   die('Fecha;');
//            
            foreach ($credores as $credor) {
                $usuariosComRoRecuperador[$credor->credor_cre_cod] = $this->md->getUsuariosRos($dataDe, $dataAte, $credor->credor_cre_cod, $filtroUsuarios, $filtroInadimplente, $filtroOperacao);
            }

            foreach ($credores as $credor) {
                foreach ($usuariosComRoRecuperador[$credor->credor_cre_cod] as $usuario) {
                    $rosUsuarioCredor[$credor->credor_cre_cod . $usuario->usuarios_usu_cod] = $this->md->getRosCredorUsuario($dataDe, $dataAte, $credor->credor_cre_cod, $usuario->usuarios_usu_cod, $filtroInadimplente, $filtroOperacao);
                }
            }

            if (($credores != '') && ($usuariosComRoRecuperador != '') && ($rosUsuarioCredor != '')) {

                $this->data['credores'] = $credores;
                $this->data['recuperadoresComRo'] = $usuariosComRoRecuperador;
                $this->data['roUsuariosCredor'] = $rosUsuarioCredor;

                $this->data['title'] = 'Recupera :: Relat�rio de presta��o de contas';
                $this->inicore->addcss(array('relatorios')); // CSS
                $this->inicore->loadview('rel_ger08', $this->data);
            } else {
                $mensagem = "N�o foram encontrados dados para esta pesquisa!";
                $this->inicore->setMensagem('error', $mensagem);
                redirect(base_url() . 'relatorios/rel_ger');
            }
        } else {
            $mensagem = "Dados insuficientes para realizar uma pesquisa!";
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'relatorios/rel_ger');
        }


    }

}