<?php

class Pesquisar extends Controller {

    function Pesquisar() {
        parent::Controller();
    }

    function _remap($link) {
        $this->data['title'] = "Recupera :: Pesquisa";

        $this->inicore->addcss(array('reset', 'style', 'style_fixed','colors/blue','chosen.min')); // CSS HOME
        //CARREGA O HELPER
        $this->load->helper("funcoes_helper");
        //carrega o model
        $this->load->model('pesquisa_model', 'md');
		$this->load->model('Credormodel', 'mdcredor');
        //CARREGA OS JAVASCRIPTS
        //'jquery-1.4.2.min.js', 
        //'jquery-ui-1.8.custom.min.js'
        $this->inicore->addjs(array('jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js','plugin/chosen.jquery.min.js','plugin/chosen.proto.min.js','jquery.cookie.js','plugin/jquery.quicksearch.min.js'));
		
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        //IR PARA O RESPECTIVO FLUXO DA URL
        if ($link == "inadimplente") {
            //FUN��O RESPONSAVEL POR CARREGAR A VIEW DE PESQUISA DE INADIMPLENTE
            $this->_inadimplente();
        } else if ($link == "pesinadres") {
            //FUN��O RESPONSAVEL POR CARREGAR O RESULTADO DA PESQUISA DE INADIMPLENTE
            $this->_pesinadres();
        } else if ($link == "credores") {
            // FUN��O RESPOANSAVEL POR CARREGAR A VIEW DA PESQUISA DE CREDORES
            $this->_credores();
        } else if ($link == "juridico") {
            $this->_juridico();
        } else if ($link == "fiscal") {
            $this->_fiscal();
        } else if ($link == "cobrancas") {
            $this->_cobrancas();
        } else if ($link == "pesCobranca") {
            $this->_pesCobranca();
        } else if ($link == "pesJuridico") {
            $this->_pesJuridico();
        } else if ($link == "pesFiscal") {
            $this->_pesFiscal();
        } else if ($link == "fiscal_setData") {
            $this->_fiscal_setData();
        } else if ($link == "fiscal_setStatus") {
            $this->_fiscal_setStatus();
        } else if ($link == "buscaRapida") {
            $this->_buscaRapida();
        } else if ($link == "pescredres") {
            // FUN��O RESPONSAVEL POR CARREGAR O RESULTADO PARA A PESQUISA DE CREDORES
            $this->_pescredres();
        }
    }

	function _fiscal_setData() {
		
		$cob_cod = $this->input->post('cob_cod');
		$fiscal_data = $fData = $this->input->post('fiscal_data');
		
		if (!empty($fiscal_data)) {
			$tmpData = preg_replace('/[^0-9]/', '', $fiscal_data); 
			if (empty($tmpData) || !is_numeric($tmpData) || strlen($tmpData)!=8) {
				$fiscal_data = null;
			} else {
				$fiscal_data = convDataParaDb($fiscal_data);
			}
		}
		
		$ret = false;
		$cob_cod = strpos($cob_cod,',')!==false ? explode(',',$cob_cod) : array($cob_cod);
		foreach($cob_cod as $cc) {
			$ret = $this->md->setFiscalData($cc,$fiscal_data);
			if ($ret!=false && !empty($ret)) {
				//GERAR RO AUTOMATICO PARA ESSA COBRAN�A
	            $this->db->insert('ros', array(
	                'cobranca_cob_cod' => $cc,
	                'operacoes_ope_cod' => '91',
	                'usuarios_usu_cod' => $this->session->userdata('usucod'),
	                'ros_data' => date('Y-m-d'),
	                'ros_hora' => date('H:i:s'),
	                'ros_detalhe' => utf8_encode('DATA DO SETOR FISCAL ALTERADA PARA '.$fData)
	            ));
				//ATUALIZANDO TABELAS
				$ros_cod = $this->db->insert_id();
				$this->db->update('cobrancas', array('last_ros_cod' => $ros_cod, 'last_ros_update' => date('y-m-d H:i:s')), array('cob_cod'=>$cc));
			}
		}
		$json = json_encode(array_merge($_POST,array('status'=>$ret)));
		echo $json; die();
	}

	function _fiscal_setStatus() {
		
		$cob_cod = $this->input->post('cob_cod');
		$fiscal_status = $this->input->post('fiscal_status');
		
		$arrSF = $this->_statusFiscalCod2Arr($fiscal_status);
		
		$ret = false; $arrSFA=array('fiscal_status_desc'=>'','fiscal_status_class'=>'');
		$cob_cod = strpos($cob_cod,',')!==false ? explode(',',$cob_cod) : array($cob_cod);
		foreach($cob_cod as $cc) {
			
			$fiscal_status_atual = $this->md->getFiscalStatus($cc);
			if (is_numeric($fiscal_status_atual)) $arrSFA = $this->_statusFiscalCod2Arr($fiscal_status_atual);			
			
			$ret = $this->md->setFiscalStatus($cc,$fiscal_status);
			if ($ret!=false && !empty($ret)) {
				
				//GERAR RO AUTOMATICO PARA ESSA COBRAN�A
	            $this->db->insert('ros', array(
	                'cobranca_cob_cod' => $cc,
	                'operacoes_ope_cod' => '91',
	                'usuarios_usu_cod' => $this->session->userdata('usucod'),
	                'ros_data' => date('Y-m-d'),
	                'ros_hora' => date('H:i:s'),
	                'ros_detalhe' => utf8_encode('STATUS DO SETOR FISCAL ALTERADO '.(!empty($arrSFA['fiscal_status_desc'])?'DE '.$arrSFA['fiscal_status_desc']:'').' PARA '.$arrSF['fiscal_status_desc'])
	            ));
				//ATUALIZANDO TABELAS
				$ros_cod = $this->db->insert_id();
				$this->db->update('cobrancas', array('last_ros_cod' => $ros_cod, 'last_ros_update' => date('y-m-d H:i:s')), array('cob_cod'=>$cc));
			}
		}
		$json = json_encode(array_merge($_POST,$arrSF,array('status'=>$ret)));
		echo $json; die();
	}

    function _cobrancas() {
        //DETERMINA O MENU QUE FICARA ABERTO
        $this->session->set_userdata('menusel', '27');
        $this->inicore->loadSidebar();
		
		//TODOS OS CREDORES
		$this->data['credores']=$this->mdcredor->getCredores();
		
		//LISTA DE CREDORES DA CARTEIRA
		//$this->data['credores']=$this->mdcredor->getCredorPorRec($this->session->userdata('usucod'));
		
        // CARREGANDO A VIEW DE PESQUISA DE COBRAN�A
        $this->inicore->loadview('pes_cobranca', $this->data);
    }

    function _pesCobranca() {
//        $this->benchmark->mark('code_start');

		//VERIFICA O CODIGO DO CREDOR
		$cre_cod = $this->input->post('credorFiltro','');
		$tipos = $this->input->post('tipoFiltro','');
		$setores = $this->input->post('tipoSetor','');
		$agrupar = $this->input->post('agrupar','');
		if (empty($cre_cod) || $cre_cod==false || empty($tipos) || $tipos==false || empty($setores) || $setores==false) {
            $this->inicore->setMensagem('notice', 'Selecione o credor, os tipos de cobran�a e os setores em que efetuar a pesquisa.');
            redirect(base_url() . 'pesquisar/cobrancas');
		}
		
		//CREDOR
		$rsCredor = $this->md->pesquisarCre('WHERE cre_cod="'.$cre_cod.'"');
		if (!empty($rsCredor)) $rsCredor=reset($rsCredor);
		$this->data['credor'] = $rsCredor;
		 
		/*
		$tipoFiltro = $this->input->post('tipoFiltro');
		if (!empty($tipoFiltro)) {
			$filtro.=" AND co.`cob_setor` = '$tipoFiltro' ";
		}
		*/

		//PESQUISANDO CADA TIPO DE COBRAN�A PARA GERAR A LISTA.
		$cobrancas = '';
		foreach($tipos as $tipo) {
			$cobs = $this->md->pesDivCredor($cre_cod, $tipo, $setores, $agrupar);
			if (!empty($cobs)) $cobrancas[$tipo] = $cobs;
		}
		
		if (!empty($agrupar)) {
			$newCobs = '';
			if ($agrupar=='INA') {
				foreach($cobrancas as $tipo=>$cobranca_tipo) {
					foreach($cobranca_tipo as $cobranca) {
						if (!isset($newCobs[$cobranca->ina_cod])) {
							$newCobs[$cobranca->ina_cod] = $cobranca;
						}
						$newCobs[$cobranca->ina_cod]->valores[] = (isset($cobranca->valor2) && !empty($cobranca->valor2) ? $cobranca->valor2 : $cobranca->valor);
						$newCobs[$cobranca->ina_cod]->valores = array_unique($newCobs[$cobranca->ina_cod]->valores);
						
						$newCobs[$cobranca->ina_cod]->tipos[] = $tipo;
						$newCobs[$cobranca->ina_cod]->tipos = array_unique($newCobs[$cobranca->ina_cod]->tipos);
						
						$newCobs[$cobranca->ina_cod]->setores[] = $cobranca->setor;
						$newCobs[$cobranca->ina_cod]->setores = array_unique($newCobs[$cobranca->ina_cod]->setores);
					}
				} 
			}
			usort($newCobs, function($a, $b) {
   				return strcmp($a->ina_nome, $b->ina_nome);
			});
			$cobrancas = $newCobs;
			$layout_res = 'pes_cobranca_res_agr';
		} else {
			$layout_res = 'pes_cobranca_res';
		}
		
		$this->data['cobrancas'] = $cobrancas;
        //----------------------------------------------------------

        /* se $cobDiv na posi��o 0 ou na posi��o 1 for igual 0, significa que a consulta n�o retornou nenhum resultado */
        if (empty($cobrancas)) {
            $this->inicore->setMensagem('notice', 'A pesquisa n�o retornou resultados.', true);
            redirect(base_url() . 'pesquisar/cobrancas');
        } else {//se a consulta retornar algum resultado
            $this->inicore->loadSidebar();
            $this->inicore->loadview($layout_res, $this->data);
        }

        
    }

    function _juridico() {
        //DETERMINA O MENU QUE FICARA ABERTO
        $this->session->set_userdata('menusel', '27');
        $this->inicore->loadSidebar();
		
		//TODOS OS CREDORES
		$this->data['credores']=$this->mdcredor->getCredores();
		
		//LISTA DE CREDORES DA CARTEIRA
		//$this->data['credores']=$this->mdcredor->getCredorPorRec($this->session->userdata('usucod'));
		
        // CARREGANDO A VIEW DE PESQUISA DE COBRAN�A
        $this->inicore->loadview('pes_juridico', $this->data);
    }
	
    function _pesJuridico() {
//        $this->benchmark->mark('code_start');

		$filtro_credor = $this->input->post('tipo_filtro_credor','');
		$filtro_multa = $this->input->post('tipo_filtro_multa','');
		$filtro_radio = strtoupper($this->input->post('tipo_filtro_radio',''));
		$filtro_vencimento = $this->input->post('vencimento_data','');
		
		if (!empty($filtro_credor)) $cre_cod = $this->input->post('credor','');
		else $cre_cod=0;
		
		if (!empty($filtro_multa)) $acordo_multa = $this->input->post('acordo_multa','');
		else $acordo_multa='';
		
		$vencimento_data_de = convDataParaDb($this->input->post('vencimento_data_de',''));
		$vencimento_data_ate = convDataParaDb($this->input->post('vencimento_data_ate',''));
		$ociosidade_data_dias = $this->input->post('ociosidade_data_dias','');
		
		$processo_radio = $this->input->post('processo_radio','');
		$processo_num = $this->input->post('processo_num','');
		$processo_ano = $this->input->post('processo_ano','');
		$processo_forum = $this->input->post('processo_forum','');
		
		if ($vencimento_data_de=='--') $vencimento_data_de='';
		if ($vencimento_data_ate=='--') $vencimento_data_ate='';
		
		if (empty($filtro_credor) && empty($filtro_radio)) {
            $this->inicore->setMensagem('notice', 'Selecione pelo menos um tipo de filtro para pesquisar.');
            redirect(base_url() . 'pesquisar/juridico');
		}

		if (empty($filtro_radio)) {
            $this->inicore->setMensagem('notice', 'Selecione um tipo de filtro por data');
            redirect(base_url() . 'pesquisar/juridico');
		}
		
		if (!empty($filtro_credor) && empty($cre_cod)) {
            $this->inicore->setMensagem('notice', 'Voc� ativou o filtro por credor mas n�o selecionou um credor para pesquisar.');
            redirect(base_url() . 'pesquisar/juridico');
		}
		
		if (!empty($filtro_radio) && $filtro_radio=='V' && empty($filtro_vencimento)) {
            $this->inicore->setMensagem('notice', 'Voc� ativou o filtro por vencimento mas n�o informou se quiser pesquisar por intervalo de datas ou todas.');
            redirect(base_url() . 'pesquisar/juridico');
		}
		
		if (!empty($filtro_radio) && $filtro_radio=='V' && $filtro_vencimento=='I' && empty($vencimento_data_de) && empty($vencimento_data_ate)) {
            $this->inicore->setMensagem('notice', 'Voc� ativou o filtro por vencimento mas n�o informou as datas que deseja pesquisar.');
            redirect(base_url() . 'pesquisar/juridico');
		}
		
		if (!empty($filtro_radio) && $filtro_radio=='O' && empty($ociosidade_data_dias)) {
            $this->inicore->setMensagem('notice', 'Voc� ativou o filtro por ociosidade mas n�o informou os n�mero de dias que deseja pesquisar.');
            redirect(base_url() . 'pesquisar/juridico');
		}
		
		if (!empty($filtro_radio) && $filtro_radio=='P' && empty($processo_radio) ) {
            $this->inicore->setMensagem('notice', 'Voc� ativou o filtro por processo mas n�o informou uma op��o para pesquisa.');
            redirect(base_url() . 'pesquisar/juridico');
		}
		
		//CREDOR
		$rsCredor = $this->md->pesquisarCre('WHERE cre_cod="'.$cre_cod.'"');
		if (!empty($rsCredor)) $rsCredor=reset($rsCredor);
		$this->data['credor'] = $rsCredor;
		
		//PESQUISA POR DATA DE VENCIMENTO
		if (!empty($filtro_radio) && $filtro_radio=='V' && $filtro_vencimento=='I') {
			$cobrancas = $this->md->pesAcoJuridicoVencimento($vencimento_data_de,$vencimento_data_ate,$cre_cod,$acordo_multa);
			$loadView = 'pes_juridico_venc_res';
		}
		//PESQUISA POR TODAS AS DATAS
		if (!empty($filtro_radio) && $filtro_radio=='V' && $filtro_vencimento=='T') {
			$cobrancas = $this->md->pesAcoJuridicoVencimentoTodas($cre_cod,$acordo_multa);
			$loadView = 'pes_juridico_venc_res_todas';
		}
		//PESQUISAR POR OCIOSIDADE
		if (!empty($filtro_radio) && $filtro_radio=='O') {
			$cobrancas = $this->md->pesAcoJuridicoOciosidade($ociosidade_data_dias,$cre_cod,$acordo_multa);
			$loadView = 'pes_juridico_ocio_res';
		}
		
		//PESQUISAR POR PROCESSO
		if (!empty($filtro_radio) && $filtro_radio=='P') {
			
			if ($processo_radio=='A') {
				$cobrancas = $this->md->pesProcArquivamento($processo_num,$processo_ano,$processo_forum,$cre_cod);
				$loadView = 'pes_juridico_arq_res';
			} else if ($processo_radio=='D') {
				$cobrancas = $this->md->pesProcDesentranhamento($processo_num,$processo_ano,$processo_forum,$cre_cod);
				$loadView = 'pes_juridico_des_res';
			} else {
				$cobrancas = $this->md->pesPorProcesso($processo_num,$processo_ano,$processo_forum,$cre_cod);
				$loadView = 'pes_juridico_proc_res';
			}
			
		}
		
		$total_cobrancas = count($cobrancas);
		if ($total_cobrancas>2000) { //2000
            $this->inicore->setMensagem('notice', 'Sua pesquisa retornou mais de 2000 resultados, por favor selecione mais filtros.');
            redirect(base_url() . 'pesquisar/juridico');
		}

		$this->data['cobrancas'] = $cobrancas;
        //----------------------------------------------------------

        //se $cobDiv na posi��o 0 ou na posi��o 1 for igual 0, significa que a consulta n�o retornou nenhum resultado
        if (empty($cobrancas)) {
            $this->inicore->setMensagem('notice', 'A pesquisa n�o retornou resultados.', true);
            redirect(base_url() . 'pesquisar/juridico');
        } elseif (!isset($loadView) || empty($loadView)) {
            $this->inicore->setMensagem('notice', 'N�o foi poss�vel identificar um caminho v�lido para exibi��o da pesquisa.', true);
            redirect(base_url() . 'pesquisar/juridico');
        } else {//se a consulta retornar algum resultado
            $this->inicore->loadSidebar();
			$this->inicore->loadview($loadView, $this->data);
        }

        
    }

    function _fiscal() {
        //DETERMINA O MENU QUE FICARA ABERTO
        $this->session->set_userdata('menusel', '27');
        $this->inicore->loadSidebar();
		
		//TODOS OS CREDORES
		$this->data['credores']=$this->mdcredor->getCredores();
		
		//LISTA DE CREDORES DA CARTEIRA
		//$this->data['credores']=$this->mdcredor->getCredorPorRec($this->session->userdata('usucod'));
		
        // CARREGANDO A VIEW DE PESQUISA DE COBRAN�A
        $this->inicore->loadview('pes_fiscal', $this->data);
    }
	
    function _pesFiscal() {
//        $this->benchmark->mark('code_start');

		$filtro_credor = $this->input->post('tipo_filtro_credor','');
		$filtro_data = $this->input->post('tipo_filtro_data','');
		$filtro_setor = $this->input->post('tipo_filtro_setor','');
		$fiscal_status = $this->input->post('fiscal_status','');
		$setor = $this->input->post('setor','');
		
		if (!empty($filtro_credor)) $cre_cod = $this->input->post('credor','');
		else $cre_cod=0;
		
		//FILTRO DATA
		if (empty($filtro_data)) {
			$fiscal_data_de=$fiscal_data_ate='';
		} else {
			$fiscal_data_de = convDataParaDb($this->input->post('fiscal_data_de',''));
			$fiscal_data_ate = convDataParaDb($this->input->post('fiscal_data_ate',''));
			
			if ($fiscal_data_de=='--') $fiscal_data_de='';
			if ($fiscal_data_ate=='--') $fiscal_data_ate='';
		}
		
		if (!empty($filtro_credor) && empty($cre_cod)) {
            $this->inicore->setMensagem('notice', 'Voc� ativou o filtro por credor mas n�o selecionou um credor para pesquisar.');
            redirect(base_url() . 'pesquisar/fiscal');
		}
		
		if (!empty($filtro_data) && (empty($fiscal_data_de) || empty($fiscal_data_ate))) {
            $this->inicore->setMensagem('notice', 'Voc� ativou o filtro por data mas n�o selecionou as DUAS datas corretamente.');
            redirect(base_url() . 'pesquisar/fiscal');
		}
		
		if (!empty($filtro_setor) && empty($setor)) {
            $this->inicore->setMensagem('notice', 'Voc� ativou o filtro por setor mas n�o selecionou um setor para pesquisar.');
            redirect(base_url() . 'pesquisar/fiscal');
		}
		
		//CREDOR
		$rsCredor = $this->md->pesquisarCre('WHERE cre_cod="'.$cre_cod.'"');
		if (!empty($rsCredor)) $rsCredor=reset($rsCredor);
		$this->data['credor'] = $rsCredor;
		
		//SETOR
		if (empty($filtro_setor)) $setor='';
		
		//PESQUISAR COBRAN�AS
		$cobrancas = $this->md->pesFiscal($fiscal_status,$fiscal_data_de,$fiscal_data_ate,$cre_cod,$setor);
		
		//AJUSTANDO COBRAN�AS PARA EVITAR DUPLICA��O DE INADIMPLENTES
		//echo '<pre>'; var_dump($cobrancas); die();
		$valorGeral = 0;
		$newCobs = '';		
		foreach($cobrancas as $i=>$cobranca) {
			$newCod = $cobranca->ina_cod.'#'.$cobranca->cre_cod;
			
			$arrSF = $this->_statusFiscalCod2Arr($cobranca->fiscal_status);
			
			if (!isset($newCobs[$newCod])) {
				$newCobs[$newCod] = array(
					'cre_cod'=>$cobranca->cre_cod,
					'cre_nome'=>$cobranca->cre_nome_fantasia,
					'ina_cod'=>$cobranca->ina_cod,
					'ina_nome'=>$cobranca->ina_nome,
					'fiscal_status'=>$cobranca->fiscal_status,
					'fiscal_status_class'=>$arrSF['fiscal_status_class'],
					'fiscal_status_desc'=>$arrSF['fiscal_status_desc'],
					'fiscal_data'=>$cobranca->fiscal_data,
					'valor_total'=>0,
					'codigos'=>'',
					'setores'=>'',
					'cobrancas'=>''
				);
			}
			
			$tmpValor = (isset($cobranca->valor) && !empty($cobranca->valor) ? $cobranca->valor : $cobranca->valor2);			
			$newCobs[$newCod]['valor_total'] += floatVal($tmpValor);
			$valorGeral += floatVal($tmpValor);
			
			//PARA USAR NO BOT�O RO
			$newCobs[$newCod]['codigos'][] = $cobranca->cob_cod;
			$newCobs[$newCod]['codigos'] = array_unique($newCobs[$newCod]['codigos']);
			
			$newCobs[$newCod]['setores'][] = $cobranca->setor;
			$newCobs[$newCod]['setores'] = array_unique($newCobs[$newCod]['setores']);
			
			$cobranca->fiscal_status_class = $arrSF['fiscal_status_class'];
			$newCobs[$newCod]['cobrancas'][] = $cobranca;

		}
		usort($newCobs, function($a, $b) {
			return strcmp($a['ina_nome'], $b['ina_nome']);
		});
		$cobrancas = $newCobs;
		
		$this->data['cobrancas'] = $cobrancas;
		$this->data['valor_geral'] = $valorGeral;
        //----------------------------------------------------------

        //se $cobDiv na posi��o 0 ou na posi��o 1 for igual 0, significa que a consulta n�o retornou nenhum resultado
        if (empty($cobrancas)) {
            $this->inicore->setMensagem('notice', 'A pesquisa n�o retornou resultados.', true);
            redirect(base_url() . 'pesquisar/fiscal');
        } else {//se a consulta retornar algum resultado
            $this->inicore->loadSidebar();
			$this->inicore->loadview('pes_fiscal_res', $this->data);
        }
        
    }

    function _buscaRapida() {
        $this->session->set_userdata('menusel', '27');
        
        if ($this->input->post('rbPesquiPor', true) == 'fone' && ($this->input->post('inputBusRap', true))) {

            $this->data['pesquisaPor'] = $this->input->post('rbPesquiPor');
            $valorDigitado = $this->input->post('inputBusRap');
            $this->data['inadimplentes'] = $this->md->buscInaFone($valorDigitado);

            $this->data['valorDigitado'] = $valorDigitado;

            $this->inicore->loadSidebar();
            $this->inicore->loadview('pes_busca_rapida_res',$this->data);
            
        } else if ($this->input->post('rbPesquiPor', true) == 'parente' && ($this->input->post('inputBusRap', true))) {

            $this->data['pesquisaPor'] = $this->input->post('rbPesquiPor');
            $valorDigitado = removeCE_Upper($this->input->post('inputBusRap'));
            $resultado = $this->md->buscInaPar($valorDigitado);

            $this->data['valorDigitado'] = $valorDigitado;
            
            foreach ($resultado as $res){
                
                $nome = $res->ina_nome;
                $aux = strpos($nome, ' ');
                $nome = substr($nome, 0, $aux);
                
                $res->ina_pri_nome = $nome;
                
                $nome = $res->par_nome;
                $aux = strpos($nome, ' ');
                $nome = substr($nome, 0, $aux);
                
                $res->par_pri_nome = $nome;
            }
            
            $this->data['inadimplentes'] = $resultado;
            
            $this->inicore->loadSidebar();
            $this->inicore->loadview('pes_busca_rapida_res_parente',$this->data);
            
        } else {
            $this->inicore->setMensagem('error', 'Dados insuficientes para realizar uma pesquisa!', true);
            redirect(base_url());
        }
    }

    function _inadimplente() {
        //DETERMINA O MENU QUE FICARA ABERTO
        $this->session->set_userdata('menusel', '27');
        $this->inicore->loadSidebar();
        // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
        $this->inicore->loadview('pes_inadimplente', $this->data);
    }

    function _pesinadres() {
        //DETERMINA O MENU QUE FICARA ABERTO
        $this->session->set_userdata('menusel', '27');
        $this->inicore->loadSidebar();
        if (// VE SE TEM ALGUMA INFORMA��O NOS FILTROS
                ($this->input->post('filtro1_desc', true) != "") ||
                ($this->input->post('filtro2_desc', true) != "") ||
                ($this->input->post('filtro3_desc', true) != "")
        ) {
            // COME�A O MONTAR O FILTRO
            $filtro = $join = "";

            if ($this->input->post('filtro1_desc', true) != "") {//SE VIR ALGO NO FILTRO 1
                if ($this->input->post('filtro1', true) == 'telefone') {//SE FOR UM TELEFONE, FAZ-SE UM TRATAMENTO ESPECIAL
                    $aux = $this->input->post('filtro1_desc', true); // PASSA OS DADOS DO FILTRO PARA UMA VARIAVEL AUXILIAR
                    $filtro .=" I.ina_foneres LIKE '%$aux%' OR I.ina_fonerec LIKE '%$aux%'
                             OR I.ina_fonecom LIKE '%$aux%' OR I.ina_cel1 LIKE '%$aux%' OR I.ina_cel2 LIKE '%$aux%'
                             OR I.ina_cel3 LIKE '%$aux%'"; // BUSCA EM TODOS OS CAMPOS DE TELEFONE DO INADIMPLENTE
                } else if ($this->input->post('filtro1', true) == 'telenonePAR') {//SE FOR UM TELEFONE DE PARENTE TAMBEM TEM UM TRATAMENTO ESPECIAL
                    $aux = $this->input->post('filtro1_desc', true); //PASSA O FILTRO PARA UMA VARIAVEL AUXILIAR
                    $filtro .="I.ina_mae_fone LIKE '%$aux%'
                            OR I.ina_pai_fone LIKE '%$aux%'
                            OR I.ina_conj_fone LIKE '%$aux%'"; // BSCA EM TODOS OS CAMPOS DE TELEFONES DE PARENTES
                } else if ($this->input->post('filtro1', true) == 'parentes') {//SE A PESQUISA FOR POR PARENTES, TAMBEM TEM UM TRATAMENTO ESPECIAL
                    $aux = $this->input->post('filtro1_desc', true);
                    $filtro .="I.ina_conj_nome LIKE '%$aux%'
                                OR I.ina_mae_nome LIKE '%$aux%'
                                OR I.ina_pai_nome LIKE '%$aux%'"; // BUSCA EM TODOS OS CAMPOS DE PARENTES
                } else {// SE N�O TIVER NENHUM TRATAMENTO ESPECIAL, MONTA UM FILTRO GEN�RICO COM O CAMPO SELECIONADO E O FILTRO DIGITADO
                    $aux = $this->input->post('filtro1_desc', true);
                    $aux2 = $this->input->post('filtro1', true);
                    $filtro .=" I.$aux2 LIKE '%$aux%'";
                }
                $filtroAux = $this->input->post('filtro1', true); // DEPENDENDO DO QUE VIER DO SELECT MANDARA UMA COISA PARA A VIEW
                switch ($filtroAux) {
                    case 'ina_nome':
                        $filtroAux = "Nome";
                        break;
                    case 'telefone':
                        $filtroAux = "Telefone";
                        break;
                    case 'ina_cidade':
                        $filtroAux = "Cidade";
                        break;
                    case 'telenonePAR':
                        $filtroAux = "Telefone (parentes)";
                        break;
                    case 'ina_endereco':
                        $filtroAux = "Endere�o";
                        break;
                    case 'ina_cpf_cnpj':
                        $filtroAux = "CPF";
                        break;
                    case 'parentes':
                        $filtroAux = "Parentes";
                        break;
                    case 'ina_mae_nome':
                        $filtroAux = "Nome da M�e";
                        break;
                    case 'ina_pai_nome':
                        $filtroAux = "Nome do Pai";
                        break;
                }
                $this->data['filtro1'] = $filtroAux; //PASSA PARA A VIEW O FILTRO SELECIONADO
                $this->data['filtro1_desc'] = $this->input->post('filtro1_desc', true); //PASSA PARA A VIEW O FILTRO DIGITADO
            }
            //REPETE OS PROCEDIMENTOS DO PRIMEIRO FILTRO E FAZ OS MESMOS
            //TESTES E PREPARA��ES PARA O FILTO 2
            if ($this->input->post('filtro2_desc', true) != "") {
                if ($this->input->post('filtro2', true) == 'telefone') {
                    $aux = $this->input->post('filtro2_desc', true);
                    $filtro .=" AND I.ina_foneres LIKE '%$aux%' OR I.ina_fonerec LIKE '%$aux%'
                             OR I.ina_fonecom LIKE '%$aux%' OR I.ina_cel1 LIKE '%$aux%' OR I.ina_cel2 LIKE '%$aux%'
                             OR I.ina_cel3 LIKE '%$aux%'";
                } else if ($this->input->post('filtro2', true) == 'telenonePAR') {
                    $aux = $this->input->post('filtro2_desc', true);
                    $filtro .=" AND I.ina_mae_fone LIKE '%$aux%'
                            OR I.ina_pai_fone LIKE '%$aux%'
                            OR I.ina_conj_fone LIKE '%$aux%'";
                } else if ($this->input->post('filtro2', true) == 'parentes') {
                    $aux = $this->input->post('filtro2_desc', true);
                    $filtro .=" AND I.ina_conj_nome LIKE '%$aux%'
                                OR I.ina_mae_nome LIKE '%$aux%'
                                OR I.ina_pai_nome LIKE '%$aux%'";
                } else {
                    $aux = $this->input->post('filtro2_desc', true);
                    $aux2 = $this->input->post('filtro2', true);
                    $filtro .=" AND I.$aux2 LIKE '%$aux%'";
                }

                $filtroAux = $this->input->post('filtro2', true);
                switch ($filtroAux) {
                    case 'ina_nome':
                        $filtroAux = "Nome";
                        break;
                    case 'telefone':
                        $filtroAux = "Telefone";
                        break;
                    case 'ina_cidade':
                        $filtroAux = "Cidade";
                        break;
                    case 'telenonePAR':
                        $filtroAux = "Telefone (parentes)";
                        break;
                    case 'ina_endereco':
                        $filtroAux = "Endere�o";
                        break;
                    case 'ina_cpf_cnpj':
                        $filtroAux = "CPF";
                        break;
                    case 'parentes':
                        $filtroAux = "Parentes";
                        break;
                    case 'ina_mae_nome':
                        $filtroAux = "Nome da M�e";
                        break;
                    case 'ina_pai_nome':
                        $filtroAux = "Nome do Pai";
                        break;
                }
                $this->data['filtro2'] = $filtroAux;
                $this->data['filtro2_desc'] = $this->input->post('filtro2_desc', true);
            }
            //REPETE OS PROCEDIMENTOS DO PRIMEIRO FILTRO E FAZ OS MESMOS
            //TESTES E PREPARA��ES PARA O FILTO 3
            if ($this->input->post('filtro3_desc', true) != "") {
                if ($this->input->post('filtro3', true) == 'telefone') {
                    $aux = $this->input->post('filtro3_desc', true);
                    $filtro .=" AND I.ina_foneres LIKE '%$aux%' OR I.ina_fonerec LIKE '%$aux%'
                             OR I.ina_fonecom LIKE '%$aux%' OR I.ina_cel1 LIKE '%$aux%' OR I.ina_cel2 LIKE '%$aux%'
                             OR I.ina_cel3 LIKE '%$aux%'";
                } else if ($this->input->post('filtro3', true) == 'telenonePAR') {
                    $aux = $this->input->post('filtro3_desc', true);
                    $filtro .=" AND I.ina_mae_fone LIKE '%$aux%'
                            OR I.ina_pai_fone LIKE '%$aux%'
                            OR I.ina_conj_fone LIKE '%$aux%'";
                } else if ($this->input->post('filtro3', true) == 'parentes') {
                    $aux = $this->input->post('filtro3_desc', true);
                    $filtro .=" AND I.ina_conj_nome LIKE '%$aux%'
                                OR I.ina_mae_nome LIKE '%$aux%'
                                OR I.ina_pai_nome LIKE '%$aux%'";
                } else {
                    $aux = $this->input->post('filtro3_desc', true);
                    $aux2 = $this->input->post('filtro3', true);
                    $filtro .=" AND I.$aux2 LIKE '%$aux%'";
                }

                $filtroAux = $this->input->post('filtro3', true);
                switch ($filtroAux) {
                    case 'ina_nome':
                        $filtroAux = "Nome";
                        break;
                    case 'telefone':
                        $filtroAux = "Telefone";
                        break;
                    case 'ina_cidade':
                        $filtroAux = "Cidade";
                        break;
                    case 'telenonePAR':
                        $filtroAux = "Telefone (parentes)";
                        break;
                    case 'ina_endereco':
                        $filtroAux = "Endere�o";
                        break;
                    case 'ina_cpf_cnpj':
                        $filtroAux = "CPF";
                        break;
                    case 'parentes':
                        $filtroAux = "Parentes";
                        break;
                    case 'ina_mae_nome':
                        $filtroAux = "Nome da M�e";
                        break;
                    case 'ina_pai_nome':
                        $filtroAux = "Nome do Pai";
                        break;
                }
                $this->data['filtro3'] = $filtroAux;
                $this->data['filtro3_desc'] = $this->input->post('filtro3_desc', true);
            }
        } else {// CASO O USUARIO N�O ESPECIFIQUE OS FILTROS, RETORNA MESAGEM DE ERRO
            $this->inicore->setMensagem('error', 'Dados insuficientes para a busca', true);
            redirect(base_url() . 'pesquisar/inadimplente');
        }
		
		//FILTROS ADICIONAIS
		$extras = $this->input->post('extraFiltro','');
		$setores = $this->input->post('tipoSetor','');
		if (!empty($extras) && $extras!=false || !empty($setores) && $setores!=false) {
			$join.= ' INNER JOIN cobrancas C ON (C.inadimplentes_ina_cod=I.ina_cod) ';
			$filtro.= ' AND C.cob_remocao NOT IN (1,2,4) '; //AND C.cob_recem_enviada = 0
			if (!empty($extras)) {
				foreach($extras as $extra) {
					switch ($extra):
						case 'divida':
							$join.= ' INNER JOIN dividas DI ON (C.cob_cod = DI.cobranca_cob_cod) ';
							break;
						case 'acordo':
							$join.= ' INNER JOIN acordos ACO ON (C.cob_cod = ACO.cobranca_cob_cod) ';
							break;
					endswitch;
				}
			}
			if (is_array($setores)) $setores = implode('","', $setores);
			if (!empty($setores))  $filtro .= ' AND C.cob_setor IN ("'.$setores.'") ';
		}
		
		//N�O PODE VER 2=DOCUMENTO COMPLETO,8=Gerado Border� de Devolu��o
		//$filtro.=' AND C.fiscal_status NOT IN(2,8) '; //REUNI�O DIA 05/12/2014 PEDIU PARA REMOVER ISSO!
		
        //BUSCA NO BANCO OS RESULTADOS DA PESQUISA PASSANDO O FILTRO MONTADO ACIMA
        //echo $join.' <hr>';
        //echo $filtro;
        $this->data['resultados'] = $this->md->pesquisar($filtro,$join);

        $this->inicore->loadview('pes_inadimplente_res', $this->data);
    }

    function _credores() {
        //DETERMINA O MENU QUE FICARA SELECIONADO
        $this->session->set_userdata('menusel', '27');
        $this->inicore->loadSidebar();
        // CARREGANDO A VIEW DE PESQUISA DE PESQUISA DE CREDOR
        $this->inicore->loadview('pes_credor', $this->data);
    }

    function _pescredres() {
        //DETERMINA O MENU QUE FICARA SELECIONADO 
        $this->session->set_userdata('menusel', '27');
        $this->inicore->loadSidebar();
        if (// VE SE TEM ALGUMA INFORMA��O NOS FILTROS
                ($this->input->post('filtro1_desc', true) != "") ||
                ($this->input->post('filtro2_desc', true) != "") ||
                ($this->input->post('filtro3_desc', true) != "")
        ) {
            // COME�A O MONTAR O FILTRO
            $filtro = "WHERE ";
            if ($this->input->post('filtro1_desc', true) != "") {//SE VIR ALGO NO FILTRO 1
                if ($this->input->post('filtro1', true) == 'telefone') {//SE FOR UM TELEFONE, FAZ-SE UM TRATAMENTO ESPECIAL
                    $aux = $this->input->post('filtro1_desc', true); // PASSA OS DADOS DO FILTRO PARA UMA VARIAVEL AUXILIAR
                    $filtro .=" C.cre_fone1 LIKE '%$aux%' OR C.cre_fone2 LIKE '%$aux%'
                             OR C.cre_fax LIKE '%$aux%'"; // BUSCA EM TODOS OS CAMPOS DE TELEFONE DO CREDOR
                } else {// SE N�O TIVER NENHUM TRATAMENTO ESPECIAL, MONTA UM FILTRO GEN�RICO COM O CAMPO SELECIONADO E O FILTRO DIGITADO
                    $aux = $this->input->post('filtro1_desc', true);
                    $aux2 = $this->input->post('filtro1', true);
                    $filtro .=" C.$aux2 LIKE '%$aux%'";
                }
                $filtroAux = $this->input->post('filtro1', true);
                // DEPENDENDO DO QUE VIER DO SELECT MANDARA UMA COISA PARA A VIEW
                switch ($filtroAux) {
                    case 'cre_razao_social':
                        $filtroAux = "Raz�o social";
                        break;
                    case 'cre_nome_fantasia':
                        $filtroAux = "Nome fantasia";
                        break;
                    case 'cre_cidade':
                        $filtroAux = "Cidade";
                        break;
                    case 'telenone':
                        $filtroAux = "Telefone";
                        break;
                    case 'cre_endereco':
                        $filtroAux = "Endere�o";
                        break;
                }
                $this->data['filtro1'] = $filtroAux; //PASSA PARA A VIEW O FILTRO SELECIONADO
                $this->data['filtro1_desc'] = $this->input->post('filtro1_desc', true); //PASSA PARA A VIEW O FILTRO DIGITADO
            }
            //REPETE OS MESMOS PASSOS DO FILTRO UM PARA O FILTRO 2
            if ($this->input->post('filtro2_desc', true) != "") {
                if ($this->input->post('filtro2', true) == 'telefone') {
                    $aux = $this->input->post('filtro2_desc', true);
                    $filtro .=" OR C.cre_fone1 LIKE '%$aux%' OR C.cre_fone2 LIKE '%$aux%'
                             OR C.cre_fax LIKE '%$aux%'";
                } else {
                    $aux = $this->input->post('filtro2_desc', true);
                    $aux2 = $this->input->post('filtro2', true);
                    $filtro .=" OR C.$aux2 LIKE '%$aux%'";
                }
                $filtroAux = $this->input->post('filtro2', true);
                switch ($filtroAux) {
                    case 'cre_razao_social':
                        $filtroAux = "Raz�o social";
                        break;
                    case 'cre_nome_fantasia':
                        $filtroAux = "Nome fantasia";
                        break;
                    case 'cre_cidade':
                        $filtroAux = "Cidade";
                        break;
                    case 'telenone':
                        $filtroAux = "Telefone";
                        break;
                    case 'cre_endereco':
                        $filtroAux = "Endere�o";
                        break;
                }
                $this->data['filtro2'] = $filtroAux;
                $this->data['filtro2_desc'] = $this->input->post('filtro2_desc', true);
            }
            //REPETE OS MESMOS PASSOS DO FILTRO UM PARA O FILTRO 3
            if ($this->input->post('filtro3_desc', true) != "") {
                if ($this->input->post('filtro3', true) == 'telefone') {
                    $aux = $this->input->post('filtro3_desc', true);
                    $filtro .= " OR C.cre_fone1 LIKE '%$aux%' OR C.cre_fone2 LIKE '%$aux%'
                             OR C.cre_fax LIKE '%$aux%'";
                } else {
                    $aux = $this->input->post('filtro3_desc', true);
                    $aux2 = $this->input->post('filtro3', true);
                    $filtro .=" OR C.$aux2 LIKE '%$aux%'";
                }
                $filtroAux = $this->input->post('filtro3', true);
                switch ($filtroAux) {
                    case 'cre_razao_social':
                        $filtroAux = "Raz�o social";
                        break;
                    case 'cre_nome_fantasia':
                        $filtroAux = "Nome fantasia";
                        break;
                    case 'cre_cidade':
                        $filtroAux = "Cidade";
                        break;
                    case 'telenone':
                        $filtroAux = "Telefone";
                        break;
                    case 'cre_endereco':
                        $filtroAux = "Endere�o";
                        break;
                }
                $this->data['filtro3'] = $filtroAux;
                $this->data['filtro3_desc'] = $this->input->post('filtro3_desc', true);
            }
        } else {// CASO O USUARIO N�O ESPECIFIQUE OS FILTROS, RETORNA MESAGEM DE ERRO
            $this->inicore->setMensagem('error', 'Dados insuficientes para a busca', true);
            redirect(base_url() . 'pesquisar/inadimplente');
        }
        // BUSCA NO BANCO O RESULTADO DA PESQUISA PASSANDO O FILTRO MONTADO ACIMA
        $this->data['resultados'] = $this->md->pesquisarCre($filtro);

        $this->inicore->loadview('pes_credores_res', $this->data);
    }

	function _statusFiscalCod2Arr($cod='') {
		//TIPO FISCAL
		switch ($cod) {
			default:
			case 0: $arr=array('fiscal_status_desc'=>'Cobranca sem status fiscal','fiscal_status_class'=>'all'); break;
			case 1: $arr=array('fiscal_status_desc'=>'Aguardando Documento','fiscal_status_class'=>'ado'); break;
			case 2: $arr=array('fiscal_status_desc'=>'Documento Completo','fiscal_status_class'=>'dco'); break;
			case 3: $arr=array('fiscal_status_desc'=>'Documento Incompleto','fiscal_status_class'=>'din'); break;
			case 4: $arr=array('fiscal_status_desc'=>'Documento Completo com Acordo','fiscal_status_class'=>'dca'); break;
			case 5: $arr=array('fiscal_status_desc'=>'Documento Incompleto com Acordo','fiscal_status_class'=>'dia'); break;
			case 6: $arr=array('fiscal_status_desc'=>'Para Estornar','fiscal_status_class'=>'pes'); break;
			case 7: $arr=array('fiscal_status_desc'=>'Aguardando Devolucao','fiscal_status_class'=>'ade'); break;
			case 8: $arr=array('fiscal_status_desc'=>'Gerado Bordero de Devolucao','fiscal_status_class'=>'gbd'); break;
		} 	
		return $arr;
	}

}
