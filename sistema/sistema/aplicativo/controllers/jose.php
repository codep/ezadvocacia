<table width="450" cellspacing="1" cellpadding="4" border="0" bgcolor="#CCCCCC">
    <tbody>
        <tr class="theme">
            <th width="550" colspan="2" class="small" scope="col"><span class="themebody">Especifica��es t�cnicas</span></th>
        </tr>
        <tr bgcolor="#E7E7E7">
            <td width="140" valign="top" scope="row">Portas<br></td>
            <td width="401" valign="top">8 portas 10/100 RJ-45 com detec��o autom�tica (10Base-T tipo IEEE 802.3, 100Base-TX tipo IEEE 802.3u), Tipo de m�dia: Auto-MDIX, Duplex: half ou full<br></td>
        </tr>
        <tr bgcolor="#FFFFFF">
            <td width="140" valign="top" scope="row">Mem�ria e processador<br></td>
            <td width="401" valign="top">tamanho do buffer de pacotes: 512 KB<br></td>
        </tr>
        <tr bgcolor="#E7E7E7">
            <td width="140" valign="top" scope="row">Lat�ncia<br></td>
            <td width="401" valign="top">Lat�ncia de 100 Mb: < 5 �s<br></td>
        </tr>
        <tr bgcolor="#FFFFFF">
            <td width="140" valign="top" scope="row">Capacidade de produ��o<br></td>
            <td width="401" valign="top">1,2 milh�o de pps<br></td>
        </tr>
        <tr bgcolor="#E7E7E7">
            <td width="140" valign="top" scope="row">Capacidade de routing/switching<br></td>
            <td width="401" valign="top">1,6 Gbps<br></td>
        </tr>
    </tbody>
</table>

<table width="450" cellspacing="1" cellpadding="4" border="0" bgcolor="#CCCCCC">
    <tbody>
        <tr class="theme">
            <th width="550" colspan="2" class="small" scope="col"><span class="themebody">Requisitos de energia e opera��o</span></th>
        </tr>
        <tr bgcolor="#E7E7E7">
            <td width="140" valign="top" scope="row">Voltage de entrada<br></td>
            <td width="401" valign="top">100 a 240 VAC<br></td>
        </tr>
        <tr bgcolor="#FFFFFF">
            <td width="140" valign="top" scope="row">Frequ�ncia de entrada<br></td>
            <td width="401" valign="top">50 / 60 Hz<br></td>
        </tr>
        <tr bgcolor="#E7E7E7">
            <td width="140" valign="top" scope="row">Seguran�a<br></td>
            <td width="401" valign="top">UL 60950; IEC 60950-1; EN 60950-1; CAN/CSA-C22.2 N� 60950-1-03<br></td>
        </tr>
        <tr bgcolor="#FFFFFF">
            <td width="140" valign="top" scope="row">Compatibilidade eletromagn�tica<br></td>
            <td width="401" valign="top">FCC parte 15 Classe A; VCCI Classe A; CISPR 22 Classe A; EN 55024; EN 55022 1998 Classe A; EN 61000-3-2 2000, 61000-3-3; ICES-003 Classe A<br></td>
        </tr>
        <tr bgcolor="#E7E7E7">
            <td width="140" valign="top" scope="row">Gama de temperaturas de funcionamento<br></td>
            <td width="401" valign="top">0 a 40� C<br></td>
        </tr>
        <tr bgcolor="#FFFFFF">
            <td width="140" valign="top" scope="row">Intervalo de umidade para funcionamento<br></td>
            <td width="401" valign="top">10 a 90% (sem condensa��o)<br></td>
        </tr>
    </tbody>
</table>

<table width="450" cellspacing="1" cellpadding="4" border="0" bgcolor="#CCCCCC">
    <tbody>
        <tr class="theme">
            <th width="550" colspan="2" class="small" scope="col"><span class="themebody">Dimens�es e peso</span></th>
        </tr>
        <tr bgcolor="#E7E7E7">
            <td width="140" valign="top" scope="row">Dimens�es (largura x profundidade x altura)<br></td>
            <td width="401" valign="top">8,13 x 14,22 x 2,29 cm<br></td>
        </tr>
        <tr bgcolor="#FFFFFF">
            <td width="140" valign="top" scope="row">Peso<br></td>
            <td width="401" valign="top">0.2 kg<br></td>
        </tr>
    </tbody>
</table>

<table width="450" cellspacing="1" cellpadding="4" border="0" bgcolor="#CCCCCC"><tbody><tr class="theme"><th width="550" colspan="2" class="small" scope="col"><span class="themebody">Itens inclu�dos</span></th></tr><tr bgcolor="#E7E7E7"><td width="140" valign="top" scope="row">Garantia<br></td><td width="401" valign="top">3 anos, substitui��o avan�ada, pr�ximo dia �til, suporte telef�nico,<br></td></tr></tbody></table>