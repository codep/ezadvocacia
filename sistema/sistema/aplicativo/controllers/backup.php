<?php

class Backup extends Controller {

    function Backup() {
        parent::Controller();
    }

    function _remap($link) {

        $this->data['title'] = "Recupera :: Backup";

        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME

        $this->load->helper("funcoes_helper");

        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'jquery.maxlength.js','plugin/jquery.maskedinput'));
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        if ($link == "executar"){
            $this->_executar();
        }
        else {
            $codUsuarioSessao = $this->session->userdata('usucod');

            $this->inicore->loadSidebar(); //menu vertical da esquerda
            
            
            //TESTANDO NOVOS M�TODOS. ESTUDOS BACKUP.
            $this->data['bancos'] = '';
            $this->data['tabelas'] = '';

            // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
            $this->inicore->loadview('backup', $this->data);
        }
        
        
    }

    function _executar() {//lista as tarefas usadas na view ag_listar
        /* ----------------------------------------------------- */
        //DECLARACAO DE VARIAVEIS
         //vari�vel usada para pegar todas as tarefas de hoje do usu�rio e todas as tarefas do usu�rio.
//        $dataDeHoje = date("Y-m-d", time()); //vair�vel usada para pegar todas as tarefas de hoje
//        $mesAtual = date("m", time()); //vari�vel usada para pegar todas as tarefas do m�s
//        $anoAtual = date("Y"); //vari�vel usada para pegar todas as tarefas do m�s
//        $primeiroDiaDaSemana = getPDS(); //m�todo do helper
//        $ultimoDiaDaSemana = getUDS(); //m�todo do helper
        /* ------------------------------------------------------ */

        $this->inicore->loadSidebar(); //menu vertical da esquerda
        
        $this->db = $this->load->database('backup', TRUE);
        
        
        // Load the DB utility class
        $this->load->dbutil();
        
        $dbs = $this->dbutil->list_databases();

       
        $this->data['bancos'] = $dbs;
        
        $this->data['tabelas'] = $this->db->list_tables();
        
        $prefs = array(
                'tables'      => $this->db->list_tables(),  // Array of tables to backup.
                'ignore'      => array(),           // List of tables to omit from the backup
                'format'      => 'txt',             // gzip, zip, txt
                'filename'    => 'mybackup.sql',    // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
                'newline'     => "\n"               // Newline character used in backup file
              );


        echo '<pre>';
        print_r($prefs);
        die();
        // Backup your entire database and assign it to a variable
        
        $backup =& $this->dbutil->backup($prefs); 
        
        // Load the download helper and send the file to your desktop
        $this->load->helper('download');
        force_download(date('d-m-Y\ \(H\hi\ms\s\)').'backup.txt', $backup);

        $this->inicore->setMensagem('success', 'Guarde bem este arquivo! Contate o Administrador do Sistema para a restaura��o do backup.');
        //redirect(base_url().'backup');
        
        // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
        $this->inicore->loadview('backup', $this->data);
    }


}

