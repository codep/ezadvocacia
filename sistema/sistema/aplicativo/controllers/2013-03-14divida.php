<?php

class Divida extends Controller {

    function Listadivida() {
        parent::Controller();
    }

    function _remap($link) {

        $this->data['title'] = "Recupera :: D�vida";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->load->helper("funcoes_helper");
        $this->load->model('Dividamodel', 'md');
        $this->load->model('inadimplente_model', 'inad_model');
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput', 'jquery.alphanumeric.js', 'jquery.moeda.js'));
        //monta a array com as permiss�es do usuario para testar se tem  permiss�o de acesso aos recursos
        //escolhendo o menu que ficara selecionado;
        $menus1 = explode(",", $this->session->userdata('menu1'));
        $menus2 = explode(",", $this->session->userdata('menu2'));
        $menus3 = explode(",", $this->session->userdata('menu3'));

//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        //IR PARA O RESPECTIVO FLUXO DA URL
        if ($link == "nova") {
            //FUN��O RESPONSAVEL POR ABRIR A TELA DE NOVA D�VIDA
            if (array_search('2', $menus2, TRUE) != '') {
                $this->_nova();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "incluir") {
            //FUN��O RESPONS�VEL POR INSERIR A D�VIDA NO BANCO DE DADOS
            if (array_search('2', $menus2, TRUE) != '') {
                $this->_incluir();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "listar") {
            if (array_search('16', $menus3, TRUE) != '') {
                $this->_listar();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "excluir") {
            if (array_search('4', $menus2, TRUE) != '') {
                // FUN��O RESPONSAVEL POR ABRIR A TELA DE EXCLUS�OD E D�VIDA
                $this->_excluir();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "extAcordo") {
            if (array_search('4', $menus2, TRUE) != '') {
                // FUN��O RESPONSAVEL POR ABRIR A TELA DE EXCLUS�OD E D�VIDA
                $this->_extAcordo();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "editar") {
            if (array_search('4', $menus2, TRUE) != '') {
                $this->_editar();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "update") {
            if (array_search('4', $menus2, TRUE) != '') {
                $this->_update();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "delete") {
            if (array_search('4', $menus2, TRUE) != '') {
                $this->_delete();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "atualizarInadimplente") {
//            if (array_search('4', $menus2, TRUE) != '') {
            $this->_atualizarInadimplente();
//            } else {
//                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
//                redirect(base_url() . 'home');
//            }
        } else if ($link == "receber") {
            $this->_receber();
        } else if ($link == "listaradm") {
            $this->_listaradm('minhas');
        } else if ($link == "listartodasadm") {
            $this->_listaradm('todas');
        } else if ($link == "ficha") {
            $this->_ficha();
        } else if ($link == "retornoAdm") {
            $this->_retornoAdm();
        } else if ($link == "detalhesCobranca") {
            $this->_detalhesCobranca();
        } else if ($link == "enviaJud") {
            $this->_enviaJud();
        } else if ($link == "retornaRecemEnviadoaoAdm") {
            $this->_retornaRecemEnviadoaoAdm();
        } else if ($link == "enviaAcoJud") {
            $this->_enviaAcoJud();
        } else {
            $this->inicore->setMensagem('warning', 'Recurso ainda n�o implementado, entre em contato com o administrador do sistema');
            redirect(base_url() . 'home');
        }
    }

    function _ficha() {
        $this->inicore->loadSidebar();
        $cod = get('cod');

        $this->data['inadDados'] = $this->md->getInadDados($cod);

        if ($this->data['inadDados'] == null) {
            $this->inicore->setMensagem('error', 'C�digo de usu�rio inv�lido', true);
            redirect(base_url());
        }

        $this->data['inaParentes'] = $this->md->getInadParentes($cod);
        $this->data['cobradores'] = $this->md->getUsuariosNomeCod();
        $this->data['estados'] = $this->md->getEstados();
        $this->inicore->addjs(array('jquery.accordion.js'));
//----------------------------- Dividas ---------------------------------------

        $credores = $this->md->getCredoresComDividas($cod);

        foreach ($credores as $credor) {
            $dividas[$credor->credor_cre_cod] = $this->md->getDividasCredInad($cod, $credor->credor_cre_cod);
        }
        
        foreach ($credores as $credor) {
            foreach ($dividas[$credor->credor_cre_cod] as $divida) {
                $parcelasDiv[$divida->div_cod] = $this->md->getParcelasDividas($divida->div_cod);
            }
        }
                       
        /*********************** KONRRADO **********************/
        if(!empty($credores))
        {
            $cobrancasGeradas = $this->md->getDividasCredInadGeradas($dividas[$credor->credor_cre_cod][0]->cobranca_cob_cod);

            $codigosCob = "";

            foreach ($cobrancasGeradas as $cobTodas)
            { 
                $codigosCob .= $cobTodas->cobranca_cob_cod."-";
            }

            $this->data['cobrancasGeradas'] = $codigosCob;
        }
        /****************************************************/
             
        $this->data['credores'] = isset($credores) ? $credores : '';
        $this->data['dividas'] = isset($dividas) ? $dividas : '';
        $this->data['parDividas'] = isset($parcelasDiv) ? $parcelasDiv : '';
        
       
//       echo '<pre>';
//       print_r($dividas[$credor->credor_cre_cod][0]);
//       die('parado!');


//----------------------------- Fim de Dividas ---------------------------------
//------------------------------- Acordos --------------------------------------

        $credoresAC = $this->md->getCredoresComAcordos($cod);

        foreach ($credoresAC as $credor) {
            $dividasAC[$credor->credor_cre_cod] = $this->md->getAcordosCredInad($cod, $credor->credor_cre_cod);
        }

        foreach ($credoresAC as $credor) {
            foreach ($dividasAC[$credor->credor_cre_cod] as $divida) {
                $parcelasDivAC[$divida->aco_cod] = $this->md->getParcelasAcordos($divida->aco_cod);
            }
        }
        
        /*********************** KONRRADO *****************************/
        if(!empty($credoresAC))
        {
            $cobrancasGeradas = $this->md->getDividasCredInadGeradas($dividasAC[$credor->credor_cre_cod][0]->cobranca_cob_cod);

            $codigosCob = "";

            foreach ($cobrancasGeradas as $cobTodas)
            { 
                $codigosCob .= $cobTodas->cobranca_cob_cod."-";
            }

            $this->data['cobrancasGeradasAC'] = $codigosCob;
        }
        /***************************************************************/
        
        $this->data['credoresAC'] = isset($credoresAC) ? $credoresAC : '';
        $this->data['dividasAC'] = isset($dividasAC) ? $dividasAC : '';
        $this->data['parDividasAC'] = isset($parcelasDivAC) ? $parcelasDivAC : '';

//---------------------------- Fim deAcordos -----------------------------------
//------------------------------- Judicial --------------------------------------

        $credoresJUD = $this->md->getCredoresComJudicial($cod);

        foreach ($credoresJUD as $credor) {
            $acordosJUD[$credor->credor_cre_cod] = $this->md->getAcordosCredInadJUD($cod, $credor->credor_cre_cod);
            $dividasJUD[$credor->credor_cre_cod] = $this->md->getDividasCredInadJUD($cod, $credor->credor_cre_cod);
        }

        foreach ($credoresJUD as $credor) {
            foreach ($dividasJUD[$credor->credor_cre_cod] as $divida) {
                $parcelasDivJUD[$divida->div_cod] = $this->md->getParcelasDividas($divida->div_cod);
            }
            foreach ($acordosJUD[$credor->credor_cre_cod] as $divida) {
                $parcelasAcoJUD[$divida->aco_cod] = $this->md->getParcelasAcordos($divida->aco_cod);
            }
        }

        $this->data['credoresJUD'] = isset($credoresJUD) ? $credoresJUD : '';
        $this->data['dividasJUD'] = isset($dividasJUD) ? $dividasJUD : '';
        $this->data['acordosJUD'] = isset($acordosJUD) ? $acordosJUD : '';
        $this->data['parDividasJUD'] = isset($parcelasDivJUD) ? $parcelasDivJUD : '';
        $this->data['parAcordosJUD'] = isset($parcelasAcoJUD) ? $parcelasAcoJUD : '';

//---------------------------- Fim de Judicial ---------------------------------
//        foreach ($credoresAC as $credor){
//            echo 'Credor: '.$credor->credor_cre_cod;
//            foreach ($dividasAC[$credor->credor_cre_cod] as $divida){
//                echo '<br/>--Divida Cod: '.$divida->aco_cod.'<br/>';
//                foreach ($parcelasDivAC[$divida->aco_cod] as $parcelas){
//                    echo '----Cod: '.$parcelas->paa_cod.'<br/>';
////                    echo '----Doc: '.$parcelas->paa_doc_num.'<br/>';
//                    echo '----Val: '.$parcelas->paa_valor.'<br/> ----------------- <br/>';
//                }
//            }
//        }
//
//        die('lol');

        $this->data['JUDRecemCriados'] = $this->md->getAcordosJudRecemCriados($cod);
        $this->data['usuarioDados'] = $this->md->getInfoUsuario($this->session->userdata('usucod'));
        $this->data['existeImportacao'] = $this->inad_model->existeImportacao($cod); //verifica se tem dados na tabela import_inadimplentes

        $this->inicore->loadview('divida_ficha', $this->data);
    }

    function _listaradm($origem) {
        $this->session->set_userdata('menusel', '17');
        /* Lista as cobran�as do usu�rio da sess�o (Cobran�as ADM/Minhas cobran�as) e
         * todas as cobran�as de todos os usu�rio (Cobran�as ADM/Listar todas) */

        /* Se a origem for de Cobran�as ADM/Minhas cobran�as */
        if ($origem == 'minhas') {
            /* MINHAS COBRAN�AS ADM */
            $codUsuSessao = $this->session->userdata('usucod'); //para pegar apenas as cobran�as do usu�rio logado
            $dataHoje = date("Y-m-d", time()); // para pegar as cobran�as vencidas
            $minhaOuTodas = "AND co.usuarios_usu_cod = '$codUsuSessao'"; //pega o c�d do usu�rio logado para selecionar s� as d�vidas dele

            /* Se o text box de pesquisa vier "cheio" use o conte�do para pesquisar (pesquisa por credores ou inadimplentes) */
            if ($this->input->post('pesquisar', true) != '') {
                $pesquisarPor = removeCE_Upper($this->input->post('pesquisar'));
                $this->data['aba'] = $this->input->post('aba', true);
                /* Sen�o (se o text box de pesquisa vier "vazio" selecione de todos (credores ou inadimplentes) */
            } else {
                $pesquisarPor = ''; //recebe "vazio" para pesquisar por todos credores ou inadimplentes
            }
            /* Chamando os m�todos e carregando a view */
            $this->data['minhasCobrancasVirgensAdm'] = $this->md->getMinhasOuTodasCobrancasVirgensAdm($pesquisarPor, $minhaOuTodas);
            $this->data['minhasCobrancasEmAndamentoAdm'] = $this->md->getMinhasOuTodasCobrancasEmAndamentoAdm($pesquisarPor, $minhaOuTodas);
            $this->data['minhasCobrancasEmAcordoAdm'] = $this->md->getMinhasOuTodasCobrancasEmAcordoAdm($pesquisarPor, $minhaOuTodas);
            $this->data['minhasCobrancasEmAtrasoAdm'] = $this->md->getMinhasOuTodasCobrancasEmAtrasoAdm($pesquisarPor, $minhaOuTodas, $dataHoje);
            $this->inicore->loadSidebar();
            $this->inicore->loadview('divida_listar_adm', $this->data);
        } else if ($origem == 'todas') {/* Sen�o, se  a origem for de Cobran�as ADM/Listar todas */
            /* TODAS COBRAN�AS ADM */
            $dataHoje = date("Y-m-d", time()); // para pegar as cobran�as vencidas
            $minhaOuTodas = ''; // recebe "vazio" para pegar as cobran�as de todos usu�rios do sistema
            /* Se o text box de pesquisa vier "cheio" use o conte�do para pesquisar (pesquisa por credores ou inadimplentes) */
            if ($this->input->post('pesquisar', true) != '') {
                $pesquisarPor = removeCE_Upper($this->input->post('pesquisar'));
                $this->data['aba'] = $this->input->post('aba', true);
                /* Sen�o (se o text box de pesquisa vier "vazio" selecione de todos (credores ou inadimplentes) */
            } else {
                $pesquisarPor = ''; //recebe "vazio" para pegar de todos (credores ou inadimplentes")
            }
            /* Camando os m�todos do model e carregando a view */
            $this->data['todasCobrancasVirgensAdm'] = $this->md->getMinhasOuTodasCobrancasVirgensAdm($pesquisarPor, $minhaOuTodas);
            $this->data['todasCobrancasEmAndamentoAdm'] = $this->md->getMinhasOuTodasCobrancasEmAndamentoAdm($pesquisarPor, $minhaOuTodas);
            $this->data['todasCobrancasEmAcordoAdm'] = $this->md->getMinhasOuTodasCobrancasEmAcordoAdm($pesquisarPor, $minhaOuTodas);
            $this->data['todasCobrancasEmAtrasoAdm'] = $this->md->getMinhasOuTodasCobrancasEmAtrasoAdm($pesquisarPor, $minhaOuTodas, $dataHoje);
            $this->inicore->loadSidebar();
            $this->inicore->loadview('divida_listar_adm_todas');
        }
    }

    //listar todas as d�vidas (gerenciamento/listar/todas cobrancas)
    function _listar() {
//        die($this->input->post('filtro1'));
        $this->session->set_userdata('menusel', '9');
        if ($this->input->post('filtro1', true) != '') {
            $aux = $this->input->post('filtro1');
            if ($this->input->post('filtro1', true) == 'c.cob_cod' && $this->input->post('nome', true) == '') {
                $this->inicore->setMensagem('error', 'Digite um c�digo v�lido', true);
                redirect(base_url() . 'divida/listar');
            } else if ($this->input->post('filtro1', true) == 'c.cob_cod' && !is_numeric($this->input->post('nome'))) {
                $this->inicore->setMensagem('error', 'Digite um c�digo v�lido', true);
                redirect(base_url() . 'divida/listar');
            } else if ($this->input->post('filtro1', true) == 'ina_nome' && $this->input->post('nome', true) == '') {
                $this->inicore->setMensagem('error', 'Digite um nome', true);
                redirect(base_url() . 'divida/listar');
            } else if ($this->input->post('filtro1', true) == 'div_emissao' && $this->input->post('nome', true) == '') {
                $this->inicore->setMensagem('error', 'Digite a data de emiss�o', true);
                redirect(base_url() . 'divida/listar');
            } else if ($this->input->post('filtro1', true) == 'div_emissao' && $this->input->post('nome', true) != '') {
                $data = $this->input->post('nome');
                $dia = substr($data, 0, -8); //pega s� o dia da data
                $mes = substr($data, 3, -5); //pega s� o m�s da data
                $ano = substr($data, -4); // pega s� o ano da data
                $aux2 = $ano . '-' . $mes . '-' . $dia;
                $filtro = "$aux = '$aux2' AND";
            } else {
                if ($this->input->post('filtro1') == 'c.cob_cod') {
                    $filtro = "cob.cob_cod = " . $this->input->post('nome') . " AND";
                } else if ($this->input->post('filtro1') == 'ina_nome' && strlen($this->input->post('nome')) >= 2) {
                    $aux2 = $this->input->post('nome');
                    $filtro = "$aux like '%$aux2%' AND";
                } else {
                    $this->inicore->setMensagem('error', 'Dados insuficiente para pesquisa!', true);
                    redirect(base_url() . 'divida/listar');
                }
            }
        } else {
            $aux = '';
            $filtro = "";
        }

        $this->data['dividasEAcordos'] = $this->md->getListarDividas($filtro);
        $this->data['aux'] = $aux;

        $this->inicore->loadSidebar();
        $this->inicore->loadview('divida_listar_todas', $this->data);
    }

    function _incluir() {
        //TESTA SE OS DADOS OBRIGAT�RIOS EST�O VINDO DO BANCO
        if
        (
        /*         * *** DADOS OBRIGAT�RIOS ***** */
                ($this->input->post('cre_cod', true) != null) &&
                ($this->input->post('ina_cod', true) != null) &&
                ($this->input->post('repasses', true) != '') &&
                ($this->input->post('recuperador', true) != null) &&
                ($this->input->post('prazo', true) != null) &&
                ($this->input->post('emissao', true) != null) &&
                ($this->input->post('qtd_parcela', true) != null) &&
                ($this->input->post('venc_inicial', true) != null)
        ) {//SE TIVER TUDO OK ENT�O
            //CRIA UMA VARIAVEL COM A HORA EM MICROSSEGUNDOS PARA A SINCRONIA DAS TABELAS NO BANCO
            $sync = microtime();
            //MONTA UMA ARRAY COM OS DADOS REFERENTES A COBRAN�AS
            $dados = array(
                'repasses_rep_cod' => $this->input->post('repasses', true),
                'inadimplentes_ina_cod' => $this->input->post('ina_cod', true),
                'credor_cre_cod' => $this->input->post('cre_cod', true),
                'usuarios_usu_cod' => $this->input->post('recuperador', true),
                'cob_setor' => 'ADM',
                'cob_prazo' => '40',
                'cob_sync' => $sync
            );
            //INSERE OS DADOS NO BANCO
            $this->db->insert('cobrancas', $dados);
            //USA A VARIAVEL $sync PARA BUSCAR O REGISTRO INSERIDO NO BANCO
            $cob_cod = $this->md->getCobCad($sync);
            //AGORA COME�A A PREPARAR OS DADOS PARA A TABELA D�VIDA
            $data = $this->input->post('emissao', true); //recebe a data que o usu�rio digita no campo data do formul�rio de nova tarefa
            $dia = substr($data, 0, -8); //pega s� o dia da data
            $mes = substr($data, 3, -5); //pega s� o m�s da data
            $ano = substr($data, -4); //pega s� o ano data
            $emissao = $ano . '-' . $mes . '-' . $dia; //atribui a uma vari�vel no formato aaaa-mm-dd

            $data = $this->input->post('venc_inicial', true); //recebe a data que o usu�rio digita no campo data do formul�rio de nova tarefa
            $dia = substr($data, 0, -8); //pega s� o dia da data
            $mes = substr($data, 3, -5); //pega s� o m�s da data
            $ano = substr($data, -4); //pega s� o ano data
            $vencInicial = $ano . '-' . $mes . '-' . $dia; //atribui a uma vari�vel no formato aaaa-mm-dd
            //COME�A A MONTAR A ARRAY DE PARCELAS... FUNCIONA DA SEGUINTE MANEIRA:
            /* PRIMEIRAMENTE EU TENHO UMA VARIAVEL i Q SERVIRA DE INDICE DA MINHA ARRAY
             * DEPOIS PARA CADA TIPO DE DADOS REFERENTES A PARCELA EU CRIO UM FOREACH
             * QUE IRA BUSCAR TODOS OS CAMPOS DE CADA PARCELA INSERIDA NA VIEW DE NOVA
             * DIVIDA, E INSERI-LOS COM UMA NOVA ORDEM EM UMA ARRAY DE DADOS
             */
            if ($this->input->post('doc_par', true)) {
                $i = 0;
                foreach ($this->input->post('doc_par', true) as $doc_par):
                    $parcelas[$i]["doc"] = $doc_par;
                    $i++;
                endforeach;

                $i = 0;

                foreach ($this->input->post('num_par', true) as $num_par):
                    $parcelas[$i]["num"] = $num_par;
                    $i++;
                endforeach;

                $i = 0;
                $total_divida = 0.0;
                foreach ($this->input->post('valor_par', true) as $val_par):
                    $val_par = str_replace('.', '', $val_par);
                    $parcelas[$i]["val"] = str_replace(',', '.', $val_par);
                    $total_divida += $parcelas[$i]["val"];
                    $i++;
                endforeach;

                $i = 0;

                foreach ($this->input->post('venc_par', true) as $venc_par):
                    $data = $venc_par; //recebe a data que o usu�rio digita no campo data do formul�rio de nova tarefa
                    $dia = substr($data, 0, -8); //pega s� o dia da data
                    $mes = substr($data, 3, -5); //pega s� o m�s da data
                    $ano = substr($data, -4); //pega s� o ano data
                    $venc = $ano . '-' . $mes . '-' . $dia; //atribui a uma vari�vel no formato aaaa-mm-dd
                    $parcelas[$i]["venc"] = $venc;
                    $i++;
                endforeach;
            }


            //MONTA UMA ARRAY COM OS DADOS DA TABELA D�VIDA
            $dadosDiv = array(
                'cobranca_cob_cod' => $cob_cod->cob_cod,
                'div_documento' => $this->input->post('documento', true),
                'div_banco' => $this->input->post('banco', true),
                'div_agencia' => $this->input->post('agencia', true),
                'div_alinea' => $this->input->post('alinea', true),
                'div_emissao' => $emissao,
                'div_cadastro' => date('Y-m-d'),
                'div_qtd_parc' => $this->input->post('qtd_parcela', true),
                'div_venc_inicial' => $vencInicial,
                'div_info' => removeCE_Upper($this->input->post('info', true)),
                'div_total' => $total_divida,
                'div_prazo' => '40'
            );
            //INSERTA NO BANCO OS DADOS DA D�VIDA
            $this->db->insert('dividas', $dadosDiv);
            //RECUPERA O CODIGO DA DIVIDA INSERIDA POR MEIO DO CODIGO DA COBRAN�A
            $div_cod = $this->md->getDivCad($cob_cod->cob_cod);

            // AGORA PARA CADA PARCELA QUE FOI MONTADA EXECUTA-SE UM INSERT NO
            //BANCO DE DADOS COM OS DADOS DA MESMA
            foreach ($parcelas as $par) {
                $dadosPar = array(
                    'dividas_div_cod' => $div_cod->div_cod,
                    'pad_doc_num' => $par["doc"],
                    'pad_par_num' => $par["num"],
                    'pad_vencimento' => $par["venc"],
                    'pad_valor' => $par["val"]
                );

                $this->db->insert('par_dividas', $dadosPar);
            }

            $data = array(
                'cobranca_cob_cod' => $cob_cod->cob_cod,
                'operacoes_ope_cod' => '53',
                'usuarios_usu_cod' => $this->session->userdata('usucod'),
                'ros_data' => date('Y-m-d'),
                'ros_hora' => date('H:i:s'),
                'ros_detalhe' => utf8_encode('COBRAN�A CADASTRADA NO SISTEMA'),
            );

            $this->db->insert('ros', $data);

            //SE TUDO OCORRER BEM, O SISTEMA RETORNA PARA O USU�RIO UMA MENSAGEM DE
            //SUCESSO E VOLTA PARA A TELA DE NOV DIVIDA(POR ENQUANTO)
            $this->inicore->setMensagem('success', 'D�vida cadastrada com sucesso', true);
            redirect(base_url() . 'divida/nova');
        } else {
            //SE N�O ATENDER AOS DADOS OBRIGAT�RIOS (NOT NULL NO BANCO DE DADOS)
            //SETA A MENSAGEM ABAIXO E RETORNA PARA A VIEW
            $this->inicore->setMensagem('error', 'Dados obrigat�rios n�o atendidos', true);
            redirect(base_url() . 'divida/nova');
        }
    }

    function _nova() {
        $this->session->set_userdata('menusel', '1');
        $this->inicore->loadSidebar();
        //APENAS SETA O MENU QUE DEVER� FICAR ABERTO E PEGA OS DADOS NECESSARIOS DO BANCO
        $this->data['usuarios'] = $this->md->getUsuariosNomeCod();
        $this->data['bancos'] = $this->md->getBancos();
        $this->inicore->loadview('cad_divida', $this->data);
    }

    function _detalhes() {
        $this->inicore->loadSidebar();
        // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
        $this->inicore->loadview('divida_detalhes', $this->data);
    }

    function _delete() {
        //die("chegou");
        //pega a senha inserida pelo usu�rio e criptografa a mesma seguindo uma regra pre estabelecida
        $senha = sha1('senhamaster+' . $this->input->post('senha', true));
        //pega a senha orginal no banco de dados
        $sql = "SELECT cod, senha FROM senha_master";
        $query = $this->db->query($sql);
        //testa se as senhas s�o iguais
        if ($query->row()->senha == $senha) {
            $this->db->where($this->input->post('chave', true), $this->input->post('codigo', true));
            //se as senhas estiverem iguais apaga o usu�rio e exibe mensagem de acordo com o ocorrido
            $dados = array(
                'cob_remocao' => '1'
            );
            if ($this->db->update('cobrancas', $dados)) {

                $data = array(
                    'cobranca_cob_cod' => $this->input->post('codigo', true),
                    'operacoes_ope_cod' => '52',
                    'usuarios_usu_cod' => $this->session->userdata('usucod'),
                    'ros_data' => date('Y-m-d'),
                    'ros_hora' => date('H:i:s'),
                    'ros_detalhe' => utf8_encode('DIVIDA EXCLUIDA'),
                );

                $this->db->insert('ros', $data);

                $this->inicore->setMensagem('notice', 'D�vida removida com sucesso', true);
            } else {
                $this->inicore->setMensagem('error', 'Erro ao remover d�vida, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
            }
            //redireciona para onde deve ser redirecionado :)
            redirect(base_url() . 'divida/listar');
            // caso a senha estiver errada monta a tela de exclus�o novamente  e
            // exibe mensagem de erro para o usu�rio
        } else {
            $this->data['title'] = "Recupera :: Confrimar exclus�o";
            $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
            $this->inicore->addjs(array('smooth.form.js'));
            $this->data['deletado'] = 'D�vida';
            $this->data['codigo'] = $this->input->post('codigo', true);

            $obs = $this->md->getDetalhesDividaExclusao($this->input->post('codigo', true));
            $obs = 'Cobran�a pertencente ao inadimplente: <b>' . $obs->ina_nome . '</b> e ao credor: <b>' . $obs->cre_nome_fantasia . '</b>';

            $this->data['observacoes'] = $obs;
            $this->data['tabela'] = 'cobrancas';
            $this->data['chave'] = 'cob_cod';
            $this->data['acaocancel'] = 'divida/listar';
            $this->data['acaook'] = 'divida/delete';
            $this->inicore->setMensagem('error', 'SENHA incorreta, favor verificar e digitar novamente', false);
            $this->inicore->loadview('conf_exclusao', $this->data);
        }
    }

    function _excluir() {
        $codCob = get('codCobranca');
        //echo "cod divida = $cod"; echo "<br>"; echo "cod cobranca = $codCob"; die();
        /* A fun��o excluir possui uma tela de confirma��o de exclus�o que
         * serve para qualque exclus�o no sistema, para que esta tela funcione
         * corretamente deve ser passados algumas vari�veis que ser�o utilizadas
         * na constru��o da p�gina, as vari�veis necess�rias s�o:
         * $deletado-----------------------O que est� sendo deletado
         * $codigo-------------------------c�digo do que est� sendo deletado
         * $tabela-------------------------Tabela que ter� o item excluido
         * $chave--------------------------Chave primaria da tabela para o delete
         * $acaocancel---------------------A��o executada quando clicar em cancelar
         * $acaook-------------------------A��o executada quando clicar em Apagar
         */
        $this->data['title'] = "Recupera :: Confirmar exclus�o";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->inicore->addjs(array('smooth.form.js'));
        //PASSA OS DADOS QUE SER�O NECESSARIOS PARA CARREGAR A TELA
        $this->data['deletado'] = 'D�vida';
        $this->data['codigo'] = "$codCob";
        $this->data['tabela'] = 'cobrancas';

        $obs = $this->md->getDetalhesDividaExclusao($codCob);
        $obs = 'Cobran�a pertencente ao inadimplente: <b>' . $obs->ina_nome . '</b> e ao credor: <b>' . $obs->cre_nome_fantasia . '</b>';

        $this->data['observacoes'] = $obs;
        $this->data['chave'] = 'cob_cod';
        $this->data['acaocancel'] = 'divida/listar';
        $this->data['acaook'] = 'divida/delete';
        //CARREGA A TELA DE CONFIRMA��OD E EXCLUS�O
        $this->inicore->loadview('conf_exclusao', $this->data);
    }

    function _atualizarInadimplente() {

        ob_start(); //nunca retire esta linha para que voc� n�o sofra um mal terrivel
        //C�DIGO DO INADIMPLENTE ENVIADO POR POST
        $cod = $this->input->post('cod', true);
        //SE TODOS OS DADOS OBRIGAT�RIOS N�O FOREM NULOS
//        die(' dado: '.$this->input->post('uf', true));
        if
        (
        /*         * *** DADOS OBRIGAT�RIOS ***** */
                ($this->input->post('nome_fantasia', true) != null) &&
//                ($this->input->post('cidade', true) != null) &&
                ($this->input->post('pessoa', true) != null)
//                ($this->input->post('uf', true) != null)
        ) {

            $dados = array
                (//MONTA A ARRAY COM OS DADOS NECESS�RIOS PARA O STORED PROCEDURE
                'ina_pessoa' => $this->input->post('pessoa', true),
                'ina_sexo' => $this->input->post('sexo', true),
                'ina_estado_civil' => $this->input->post('estado_civil', true),
                'ina_nome' => removeCE_Upper($this->input->post('nome_fantasia', true)),
                'ina_cpf_cnpj' => $this->input->post('cpf_cnpj', true),
                'ina_rg_ie' => $this->input->post('rg_ie', true),
                'ina_endereco' => removeCE_Upper($this->input->post('endereco', true)),
                'ina_bairro' => removeCE_Upper($this->input->post('bairro', true)),
                'ina_complemento' => removeCE_Upper($this->input->post('complemento', true)),
                'ina_cep' => $this->input->post('cep', true),
                'ina_cidade' => removeCE_Upper($this->input->post('cidade', true)),
                'ina_uf' => $this->input->post('uf', true),
                'ina_foneres' => $this->input->post('foneres', true),
                'ina_fonerec' => $this->input->post('fonerec', true),
                'ina_fonecom' => $this->input->post('fonecom', true),
                'ina_cel1' => $this->input->post('cel1', true),
                'ina_cel2' => $this->input->post('cel2', true),
                'ina_cel3' => $this->input->post('cel3', true),
                'ina_info' => removeCE_Upper($this->input->post('info', true))
//                'ina_conj_nome' => removeCE_Upper($this->input->post('conj_nome', true)),
//                'ina_conj_fone' => $this->input->post('conj_fone', true),
//                'ina_conj_endereco' => removeCE_Upper($this->input->post('conj_endereco', true)),
//                'ina_conj_cidade' => utf8_encode(removeCE_Upper($this->input->post('conj_cidade', true))),
//                'ina_conj_uf' => $this->input->post('conj_uf', true),
//                'ina_conj_cod' => $this->input->post('conj_cod', true),
//                'ina_pai_nome' => removeCE_Upper($this->input->post('pai_nome', true)),
//                'ina_pai_fone' => $this->input->post('pai_fone', true),
//                'ina_pai_endereco' => removeCE_Upper($this->input->post('pai_endereco', true)),
//                'ina_pai_cidade' => utf8_encode(removeCE_Upper($this->input->post('pai_cidade', true))),
//                'ina_pai_uf' => $this->input->post('pai_uf', true),
//                'ina_pai_cod' => $this->input->post('pai_cod', true),
//                'ina_mae_nome' => removeCE_Upper($this->input->post('mae_nome', true)),
//                'ina_mae_fone' => $this->input->post('mae_fone', true),
//                'ina_mae_endereco' => removeCE_Upper($this->input->post('mae_endereco', true)),
//                'ina_mae_cidade' => utf8_encode(removeCE_Upper($this->input->post('mae_cidade', true))),
//                'ina_mae_uf' => $this->input->post('mae_uf', true),
//                'ina_mae_cod' => $this->input->post('mae_cod', true)
            );
        }
        //PREPARA A CONDI��O DO UPDATE
        $this->db->where('ina_cod', $cod);
        //SE ATUALIZAR OS DADOS COM SUCESSO
        if ($this->db->update('inadimplentes', $dados)) {
            //SETAR A MENSAGEM DE QUE FOI ATUALIZADO COM SUCESSO
            $this->inicore->setMensagem('notice', 'Inadimplente ' . $this->input->post('nome_fantasia', true) . ' atualizado com sucesso', true);
        }
        //SE N�O OBTIVER SUCESSO NO UPDATE
        else {
            ////SETAR A MENSAGEM DE ERRO
            $this->inicore->setMensagem('error', 'Erro ao atualizar o usu�rio, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
        }
        //REDIRECIONA PARA A VIEW "inadimplente_listar"
        redirect(base_url() . 'divida/ficha/cod:' . $cod);
    }

    function _retornoAdm() {

        if (
                ($this->input->post('cobCod', true) == null) ||
                ($this->input->post('novoPrazo', true) == null)
        ) {
            //dados obrigat�rios
            $this->inicore->setMensagem('error', 'Dados insuficientes para realizar a opera��o', true);
            redirect(base_url() . 'divida/ficha/cod:' . $this->input->post('InadCod', true));
        } else {
            $data = array(
                'cob_prazo' => $this->input->post('novoPrazo', true),
                'cob_setor' => 'ADM'
            );

            $this->db->where('cob_cod', str_replace('-', '', $this->input->post('cobCod', true)));
            if ($this->db->update('cobrancas', $data)) {
                $this->inicore->setMensagem('success', 'Opera��o realizada com sucesso: Cobran�a c�digo ' . $this->input->post('cobCod', true) . ' retornou ao administrativo', true);
                redirect(base_url() . 'divida/ficha/cod:' . $this->input->post('InadCod', true));
            }
        }
    }

    function _retornaRecemEnviadoaoAdm() {
        $cods = get('c');
        $cods = explode('-', $cods);

        $data = array(
            'cob_prazo' => '30',
            'cob_remocao' => '0',
            'cob_setor' => 'ADM'
        );

        $this->db->where('cob_cod', $cods[1]);
        $this->db->update('cobrancas', $data);

        $data = array(
            'cob_remocao' => '1',
            'cob_recem_enviada' => '0',
        );

        $this->db->where('cob_cod', $cods[0]);
        if ($this->db->update('cobrancas', $data)) {
            
            $dados = array(
                    'cobranca_cob_cod' => $cods[1],
                    'operacoes_ope_cod' => '80',
                    'usuarios_usu_cod' => $this->session->userdata('usucod'),
                    'ros_data' => date('Y-m-d'),
                    'ros_hora' => date('H:i:s'),
                    'ros_detalhe' => 'ACORDO RETORNOU AO ADMNISTRATIVO'
                );
                $this->db->insert('ros', $dados);
            
            $this->inicore->setMensagem('success', 'Opera��o realizada com sucesso: Cobran�a c�digo ' . $cods[0] . ' retornou ao administrativo', true);
            redirect(base_url() . 'divida/ficha/cod:' . $cods[2]);
        }
        die("$cods");
    }

    function _enviaJud() {

        $supervisores = $this->md->getSupervisores();

        if (
                ($this->input->post('cobCodJud', true) == '0') ||
                ($this->input->post('usuario', true) == null)
        ) {
            //dados obrigat�rios
            $this->inicore->setMensagem('error', 'Dados insuficientes para realizar a opera��o', true);
            redirect(base_url() . 'divida/ficha/cod:' . $this->input->post('InadCod', true));
        } else {
            $respCod = $this->input->post('usuario', true);    
            $cobCod = $this->input->post('cobCodJud', true);
            $cobrancaDados = $this->md->getCobrancasDadosEnvJudDivida($cobCod);
            $usuRespDados = $this->md->getInfoUsuario($respCod);

//            echo '<pre>';
//            print_r();
//            echo $cobrancaDados->div_documento;
////            
////            
//            die('Para ai oo');
//============================================================================

            $sync = microtime();
            //MONTA UMA ARRAY COM OS DADOS REFERENTES A COBRAN�AS
            $dados = array(
                'repasses_rep_cod' => $cobrancaDados->repasses_rep_cod,
                'inadimplentes_ina_cod' => $cobrancaDados->inadimplentes_ina_cod,
                'credor_cre_cod' => $cobrancaDados->credor_cre_cod,
                'usuarios_usu_cod' => $cobrancaDados->usuarios_usu_cod,
                'cob_setor' => 'JUD',
                'cob_status' => '1',
                'cob_recem_enviada' => '1',
                'cob_prazo' => '0',
                'cob_sync' => $sync
            );
            //INSERE OS DADOS NO BANCO
            $this->db->insert('cobrancas', $dados);
            //USA A VARIAVEL $sync PARA BUSCAR O REGISTRO INSERIDO NO BANCO
            $cob_cod = $this->md->getCobCad($sync);
            //AGORA COME�A A PREPARAR OS DADOS PARA A TABELA D�VIDA
            //MONTA UMA ARRAY COM OS DADOS DA TABELA D�VIDA
            $dadosAco = array(
                'cobranca_cob_cod' => $cob_cod->cob_cod,
                'aco_tipo' => '3',
                'aco_procedencia' => 'ADM',
                'aco_emissao' => date('Y-m-d'),
                'aco_valor_original' => $cobrancaDados->div_total,
                'aco_valor_atualizado' => $cobrancaDados->div_total,
                'aco_val_juros' => '0.0',
                'aco_val_multa' => '0.0',
                'aco_val_honorario' => '0.0',
                'aco_qtd_parc' => '1',
                'aco_taxa_juros' => '0.0',
                'aco_taxa_multa' => '0.0',
                'aco_taxa_honorario' => '0.0',
                'aco_total_juros' => '0.0',
                'aco_total_parcelado' => $cobrancaDados->div_total,
                'aco_cob_cod_geradora' => $cobrancaDados->cob_cod
            );
            //INSERTA NO BANCO OS DADOS DA D�VIDA
            $this->db->insert('acordos', $dadosAco);
            //RECUPERA O CODIGO DA DIVIDA INSERIDA POR MEIO DO CODIGO DA COBRAN�A
            $aco_cod = $this->md->getAcoCad($cob_cod->cob_cod);

            // AGORA PARA CADA PARCELA QUE FOI MONTADA EXECUTA-SE UM INSERT NO
            //BANCO DE DADOS COM OS DADOS DA MESMA
            $dadosPar = array(
                'acordos_aco_cod' => $aco_cod->aco_cod,
                'paa_parcela' => '1',
                'paa_vencimento' => date('Y-m-d'),
                'paa_valor' => $cobrancaDados->div_total,
                'paa_saldo' => $cobrancaDados->div_total,
                'paa_juro_diario' => '0.0',
                'paa_saldo_atual' => $cobrancaDados->div_total,
                'paa_situacao' => '0'
            );

            $this->db->insert('par_acordos', $dadosPar);
//============================================================================

            $data = array(
                'cob_remocao' => '1'
            );
            $this->db->where('cob_cod', $cobrancaDados->cob_cod);
            if ($this->db->update('cobrancas', $data)) {
                foreach ($supervisores as $supervisor) {
                    $data = array(
                        'usuarios_usu_cod' => $supervisor->usu_cod,
                        'men_titulo' => removeCE_Upper('D�vida enviada ao Judicial'),
                        'men_remetente' => 'SISTEMA',
                        'men_tipo' => '1',
                        'men_texto' => removeCE_Upper('D�vida c�digo: <b>' . $this->input->post('cobCodJud', true) . '</b> foi enviada para o setor Jur�dico no dia: <b>' . date('d/m/Y') . '</b> pelo usu�rio: <b>' . $this->input->post('usuResp', true) . '</b>; Favor averiguar.'),
                        'men_lida' => '0'
                    );
                    $this->db->insert('mensagens', $data);
                }

//                INSERIR RO AUTOMATICO
                $dados = array(
                    'cobranca_cob_cod' => $cobCod,
                    'operacoes_ope_cod' => '79',
                    'usuarios_usu_cod' => $this->session->userdata('usucod'),
                    'ros_data' => date('Y-m-d'),
                    'ros_hora' => date('H:i:s'),
                    'ros_detalhe' => 'ACORDO ENVIADO AO JUDICIAL PARA O USUARIO: ' . $usuRespDados->usu_nome
                );
                $this->db->insert('ros', $dados);

//                TUDO INSERIDO

                $this->inicore->setMensagem('success', 'Opera��o realizada com sucesso: Cobran�a c�digo ' . $this->input->post('cobCod', true) . ' enviada ao jur�dico', true);
                redirect(base_url() . 'divida/ficha/cod:' . $this->input->post('InadCod', true));
            } else {
                $this->inicore->setMensagem('error', 'Ocorreu algum erro na opera��o, tente novamente, caso o problema persistir procure o administrador do sistema', true);
                redirect(base_url() . 'divida/ficha/cod:' . $this->input->post('InadCod', true));
            }
        }
    }

    function _extAcordo() {
//        die('YOU SHALL NOT PASS!');
        $supervisores = $this->md->getSupervisores();
        if (
                ($this->input->post('devdoc_mot', true) == null) ||
                ($this->input->post('renegociacao_mot', true) == null) ||
                ($this->input->post('reabilitacao_mot', true) == null) ||
                ($this->input->post('ext_jud_mot', true) == null) ||
                ($this->input->post('cobCodAco', true) == '0')
        ) {
            //dados obrigat�rios
            $this->inicore->setMensagem('error', 'Dados insuficientes para realizar a opera��o', true);
            redirect(base_url() . 'divida/ficha/cod:' . $this->input->post('InadCod', true));
        } else {
            $data = array(
                'cob_remocao' => '1'
            );

            $this->db->where('cob_cod', $this->input->post('cobCodAco', true));
            if ($this->db->update('cobrancas', $data)) {
                foreach ($supervisores as $supervisor) {
                    $data = array(
                        'usuarios_usu_cod' => $supervisor->usu_cod,
                        'men_titulo' => removeCE_Upper('Acordo extinto'),
                        'men_remetente' => 'SISTEMA',
                        'men_tipo' => '1',
                        'men_texto' => removeCE_Upper('Acordo c�digo: <b>' . $this->input->post('cobCodAco', true) . '</b> foi extinto no dia: <b>' . date('d/m/Y') . '</b> pelo usu�rio: <b>' . $this->input->post('usuResp', true) . '</b>; Favor averiguar.'),
                        'men_lida' => '0'
                    );
                    $this->db->insert('mensagens', $data);
                }

                $acordo_cod = $this->md->getAcoCodigo($this->input->post('cobCodAco', true));

                $data = array(
                    'aco_motivo_ext' => $this->input->post('motivo_ext', true),
                    'aco_devdoc' => $this->input->post('devdoc', true),
                    'aco_renegociacao' => $this->input->post('renegociacao', true),
                    'aco_reabilitacao' => $this->input->post('reabilitacao', true),
                    'aco_ext_jud' => $this->input->post('ext_jud', true),
                    'aco_devdoc_mot' => removeCE_Upper($this->input->post('devdoc_mot', true)),
                    'aco_renegociacao_mot' => removeCE_Upper($this->input->post('renegociacao_mot', true)),
                    'aco_reabilitacao_mot' => removeCE_Upper($this->input->post('reabilitacao_mot', true)),
                    'aco_ext_jud_mot' => removeCE_Upper($this->input->post('ext_jud_mot', true)),
                    'aco_ext_data' => date('Y-m-d')
                );

                $this->db->where('aco_cod', $acordo_cod->aco_cod);
                if ($this->db->update('acordos', $data)) {

                    $data = array(
                        'cobranca_cob_cod' => $this->input->post('cobCodAco', true),
                        'operacoes_ope_cod' => '51',
                        'usuarios_usu_cod' => $this->session->userdata('usucod'),
                        'ros_data' => date('Y-m-d'),
                        'ros_hora' => date('H:i:s'),
                        'ros_detalhe' => utf8_encode('ACORDO EXTINTO'),
                    );

                    $this->db->insert('ros', $data);

                    $this->inicore->setMensagem('success', 'Opera��o realizada com sucesso: Cobran�a c�digo ' . $this->input->post('cobCod', true) . ' foi extinta', true);
                    redirect(base_url() . 'divida/ficha/cod:' . $this->input->post('InadCod', true));
                }
            } else {
                $this->inicore->setMensagem('error', 'Ocorreu algum erro na opera��o, tente novamente, caso o problema persistir procure o administrador do sistema', true);
                redirect(base_url() . 'divida/ficha/cod:' . $this->input->post('InadCod', true));
            }
        }
    }

    function _receber() {
//die('OPA OPA OPA');
        ob_start(); //nunca retire esta linha para que voc� n�o sofra um mal terrivel

        $cobDados = $this->md->getCobDadosRecebimento($this->input->post('cobCodRec', true));

        $dia = date('Y-m-d');
        $hora = date('H:i:s');
        $recebido = $this->input->post('valorRecebido', true);
        $desconto = $this->input->post('desconto', true);
        $dadosCh = $this->input->post('dados_ch', true);
        
        if($dadosCh != ''){
            $dadosCh = ' - (DADOS DO CHEQUE: '.$dadosCh.' )';
        }

//        ----------------Pegando as parcelas recebidas-------------------
        $parRecBanco = '';
        $parcelasRec = $this->input->post('parcelas_recebidas', true);

        $parcelasRec = explode(';', $parcelasRec);

        $i = 0;

        foreach ($parcelasRec as $parcelaRec) {
            if ($parcelasRec[$i] != '') {
                $parcelasAuxRec[$i] = explode('|', $parcelaRec);
            }
            $i++;
        }
        $parcelasRec = $parcelasAuxRec;

        foreach ($parcelasRec as $parcelaRec) {
            $parRecBanco .= $parcelaRec[0] . '-';
        }

//------------------------ fim da pegada das parcelas recebidas ----------------

        $recebido = str_replace(".", "", $recebido);
        $desconto = str_replace(".", "", $desconto);

        $recebido = str_replace(",", ".", $recebido);
        $desconto = str_replace(",", ".", $desconto);

        if ($desconto == '') {
            $desconto = 0.0;
        }

        $data = array(
            'cobranca_cob_cod' => $this->input->post('cobCodRec', true),
            'operacoes_ope_cod' => '8',
            'usuarios_usu_cod' => $this->session->userdata('usucod'),
            'ros_data' => $dia,
            'ros_hora' => $hora,
            'ros_detalhe' => removeCE_Upper('Pagamento realizado para o acordo c�digo: ' . $this->input->post('cobCodRec', true)
                    . ' no valor de R$' . $this->input->post('valorRecebido', true) . '. Recebido pelo usuario: ' . $this->session->userdata('usuwho')
                    . ' no dia: ' . date('d/m/Y') . ' (com um desconto de ' . $this->input->post('desconto', true) . ')'.$dadosCh)
        );

        $this->db->insert('ros', $data);

//        --------------- criar registro do recebimento ------------------------

        $data = array(
//            'usuarios_usu_cod' => $this->session->userdata('usucod'),
            'usuarios_usu_cod' => $cobDados->usu_cod,
            'credor_cre_cod' => $cobDados->credor_cre_cod,
            'inadimplentes_ina_cod' => $cobDados->inadimplentes_ina_cod,
            'acordos_aco_cod' => $cobDados->aco_cod,
            'reb_data' => $dia,
            'reb_valor' => $recebido,
            'reb_tipo' => $this->input->post('tipo_doc', true),
            'reb_desconto' => $desconto,
//            'reb_baixador' => $cobDados->usu_usuario_sis,
            'reb_baixador' => $this->session->userdata('usunome'),
            'reb_parcelas' => $parRecBanco
        );

        $this->db->insert('recebimentos', $data);

        $recebimento_dados = $this->md->getCobDadosUltimoRecebimento($cobDados->aco_cod, $cobDados->credor_cre_cod, $cobDados->inadimplentes_ina_cod, $dia, $recebido, $desconto);

//        -------------------- criar registro do Recibo ------------------------

        $data = array(
            'inadimplentes_ina_cod' => $cobDados->inadimplentes_ina_cod,
            'usuarios_usu_cod' => $this->session->userdata('usucod'),
            'recebimentos_reb_cod' => $recebimento_dados->reb_cod,
            'rec_valor' => $recebido,
            'rec_data' => $dia,
            'rec_obs' => 'RECEBIMENTO REFERENTE AS PARCELAS: ' . substr($parRecBanco, 0, -1).$dadosCh
        );
        print_r($data);

        $this->db->insert('recibos', $data);

//        ------------- atualizar o valor das parcelas e -----------------------    

        $parcelas = $this->input->post('parcelas_recebidas', true);

        $parcelas = explode(';', $parcelas);

        $i = 0;

        foreach ($parcelas as $parcela) {
            if ($parcelas[$i] != '') {
                $parcelasAux[$i] = explode('|', $parcela);
            }
            $i++;
        }
        $parcelas = $parcelasAux;

        foreach ($parcelas as $parcela) {

            $dataDif = diffDate($parcela[7], date('Y-m-d'), 'D');

            if ($parcela[3] == '_semJuros') {

                if ($parcela[1] <= 0) {
                    $valorParc = 0.0;
                    $valorParcJuros = 0.0;
                } else {
                    $valorParc = $parcela[1];
                    if ($dataDif > 0) {
                        $jurosTotal = $dataDif * $parcela[4];
                        $valorParcJuros = round(($parcela[1] + ($parcela[1] * $jurosTotal)), '2');
                    } else {
                        $valorParcJuros = $parcela[1];
                    }
                }

                if ($parcela[2] == 'quitada') {
                    $situacao = '1';
                    $dataRec = date('Y-m-d');
                } else {
                    $situacao = '0';
                    $dataRec = '';
                }

                if ($dataRec != '') {
                    $data = array(
                        'paa_saldo' => $valorParc,
                        'paa_saldo_atual' => $valorParcJuros,
                        'paa_situacao' => $situacao,
                        'paa_recebimento' => $dataRec
                    );
                } else {
                    $data = array(
                        'paa_saldo' => $valorParc,
                        'paa_saldo_atual' => $valorParcJuros,
                        'paa_situacao' => $situacao
                    );
                }
            } else {
                // ------ COM JUROS ------

                if ($parcela[1] <= 0) {
                    $valorParc = 0.0;
                    $valorParcJuros = 0.0;
                } else {

                    $valorParcJuros = $parcela[1];

                    $difValor = $parcela[6] - $parcela[5];

                    if ($dataDif <= 0) {

                        $valorParc = $parcela[1];
                    } else {

                        $fatiaJuros = (100 - (($difValor * 100) / $parcela[6]));

                        $fatiaSaldo = round(($recebido * ($fatiaJuros / 100)), '2');

                        $valorParc = $parcela[5] - $fatiaSaldo;
                    }
                }

                if ($parcela[2] == 'quitada') {
                    $situacao = '1';
                    $dataRec = date('Y-m-d');
                } else {
                    $situacao = '0';
                    $dataRec = '';
                }

                if ($dataRec != '') {
                    $data = array(
                        'paa_saldo' => $valorParc,
                        'paa_saldo_atual' => $valorParcJuros,
                        'paa_situacao' => $situacao,
                        'paa_recebimento' => $dataRec
                    );
                } else {
                    $data = array(
                        'paa_saldo' => $valorParc,
                        'paa_saldo_atual' => $valorParcJuros,
                        'paa_situacao' => $situacao
                    );
                }
            }

            $this->db->where('paa_cod', $parcela[0]);
            $this->db->update('par_acordos', $data);
        }
//        ------------- Vendo se a D�vida ja foi removida --------

        if ((!isset($this->md->getParcelasDividaTeste($this->input->post('cobCodRec', true))->cob_cod)) && (!isset($this->md->getParcelasAcordoTeste($this->input->post('cobCodRec', true))->cob_cod))) {
            $data = array(
                'cob_remocao' => '1'
            );

            $this->db->where('cob_cod', $this->input->post('cobCodRec', true));
            $this->db->update('cobrancas', $data);
        }

//        ------------------------- Feedback ---------------------
        $recibo = $this->md->getRecibo($recebimento_dados->reb_cod);
        $this->inicore->setMensagem('success', 'Recebimento realizado com sucesso!<br/><a href="' . base_url() . 'impressao/recibo/cod:' . $recibo->rec_cod . '">CLIQUE AQUI PARA IMPRIMIR O RECIBO!</a>', true);
        redirect(base_url() . 'divida/ficha/cod:' . $cobDados->inadimplentes_ina_cod);
    }

    function _detalhesCobranca() {
        $cod = get('c');
        $codDetalhes = get('cod');

        if (!is_numeric($cod)) {
            $this->inicore->setMensagem('error', 'Erro ao acessar detalhes da cobran�a', true);
            redirect(base_url() . 'home');
        }

        if ($codDetalhes != '') {
            if (!is_numeric($codDetalhes)) {
                $this->inicore->setMensagem('error', 'Erro ao acessar detalhes da cobran�a', true);
                redirect(base_url() . 'home');
            }
            $codGerador = $codDetalhes;
        } else {
            $codGerador = $this->md->getCodGerador($cod)->aco_cob_cod_geradora;
        }

        if (isset($this->md->getCodAcordoGerador($codGerador)->aco_cod)) {

            $acoCod = $this->md->getCodAcordoGerador($codGerador)->aco_cod;

            $this->data['cobDados'] = $this->md->getCobrancaDadosAco($acoCod);

            if (sizeof(($this->data['cobDados'])) == 0) {
                $this->inicore->setMensagem('error', 'Erro ao acessar detalhes da cobran�a', true);
                redirect(base_url() . 'home');
            }

            $this->data['parcelas'] = $this->md->getCobrancaParcelasAco($acoCod);

            $this->inicore->loadSidebar();
            $this->inicore->loadview('cob_detalhes', $this->data);
        } else {
//            echo '<pre>';
//            print_r($codGerador);
//            die('');
            $divCod = $this->md->getCodDividaGerador($codGerador)->div_cod;

            $this->data['cobDados'] = $this->md->getCobrancaDadosDiv($divCod);

            if (sizeof(($this->data['cobDados'])) == 0) {
                die('here');
                $this->inicore->setMensagem('error', 'Erro ao acessar detalhes da cobran�a, Nenhum Ro Cadastrado', true);
                redirect(base_url() . 'home');
            }

            $this->data['parcelas'] = $this->md->getCobrancaParcelasDiv($divCod);
            $this->inicore->loadSidebar();
            $this->inicore->loadview('cob_detalhes', $this->data);
        }

//        die('oi Amigos - ' . $divCod);
    }

    function _enviaAcoJud() {

        $supervisores = $this->md->getSupervisores();

        if (
                ($this->input->post('cobCodJud', true) == '0') ||
                ($this->input->post('usuario', true) == null)
        ) {
            //dados obrigat�rios
            $this->inicore->setMensagem('error', 'Dados insuficientes para realizar a opera��o', true);
            redirect(base_url() . 'divida/ficha/cod:' . $this->input->post('InadCod', true));
        } else {
            $respCod = $this->input->post('usuario', true);
            $cobCod = $this->input->post('cobCodJud', true);
            $cobrancaDados = $this->md->getCobrancasDadosEnvJudAcordos($cobCod);
            $usuRespDados = $this->md->getInfoUsuario($respCod);

            $usuRespNome = $usuRespDados->usu_nome;
            $usuAtuaNome = $cobrancaDados->usu_nome;


//            echo '<pre>';
//            print_r($cobrancaDados->usu_nome);
//            
//            die('Para ai oo');
//============================================================================

            $sync = microtime();
            //MONTA UMA ARRAY COM OS DADOS REFERENTES A COBRAN�AS
            $dados = array(
                'repasses_rep_cod' => $cobrancaDados->repasses_rep_cod,
                'inadimplentes_ina_cod' => $cobrancaDados->inadimplentes_ina_cod,
                'credor_cre_cod' => $cobrancaDados->credor_cre_cod,
                'usuarios_usu_cod' => $this->input->post('usuario', true),
                'cob_setor' => 'JUD',
                'cob_status' => '1',
                'cob_recem_enviada' => '1',
                'cob_prazo' => '0',
                'cob_sync' => $sync
            );

            //INSERE OS DADOS NO BANCO
            $this->db->insert('cobrancas', $dados);
            //USA A VARIAVEL $sync PARA BUSCAR O REGISTRO INSERIDO NO BANCO
            $cob_cod = $this->md->getCobCad($sync);
            //AGORA COME�A A PREPARAR OS DADOS PARA A TABELA D�VIDA
            //MONTA UMA ARRAY COM OS DADOS DA TABELA D�VIDA
            $dadosAco = array(
                'cobranca_cob_cod' => $cob_cod->cob_cod,
                'aco_tipo' => '3',
                'aco_procedencia' => 'ADM',
                'aco_emissao' => date('Y-m-d'),
                'aco_valor_original' => $cobrancaDados->aco_valor_original,
                'aco_valor_atualizado' => $cobrancaDados->total_remanescente,
                'aco_val_juros' => '0.0',
                'aco_val_multa' => '0.0',
                'aco_val_honorario' => '0.0',
                'aco_qtd_parc' => '1',
                'aco_taxa_juros' => '0.0',
                'aco_taxa_multa' => '0.0',
                'aco_taxa_honorario' => '0.0',
                'aco_total_juros' => '0.0',
                'aco_total_parcelado' => $cobrancaDados->total_remanescente,
                'aco_cob_cod_geradora' => $cobrancaDados->cob_cod.'-'.$cobrancaDados->aco_cob_cod_geradora
            );
            //INSERTA NO BANCO OS DADOS DA D�VIDA
            $this->db->insert('acordos', $dadosAco);
            //RECUPERA O CODIGO DA DIVIDA INSERIDA POR MEIO DO CODIGO DA COBRAN�A
            $aco_cod = $this->md->getAcoCad($cob_cod->cob_cod);

            // AGORA PARA CADA PARCELA QUE FOI MONTADA EXECUTA-SE UM INSERT NO
            //BANCO DE DADOS COM OS DADOS DA MESMA
            $dadosPar = array(
                'acordos_aco_cod' => $aco_cod->aco_cod,
                'paa_parcela' => '1',
                'paa_vencimento' => date('Y-m-d'),
                'paa_valor' => $cobrancaDados->total_remanescente,
                'paa_saldo' => $cobrancaDados->total_remanescente,
                'paa_juro_diario' => '0.0',
                'paa_saldo_atual' => $cobrancaDados->total_remanescente,
                'paa_situacao' => '0'
            );

            $this->db->insert('par_acordos', $dadosPar);
//============================================================================

            $data = array(
                'cob_remocao' => '1'
            );
            $this->db->where('cob_cod', $cobrancaDados->cob_cod);
            if ($this->db->update('cobrancas', $data)) {
                foreach ($supervisores as $supervisor) {
                    $data = array(
                        'usuarios_usu_cod' => $supervisor->usu_cod,
                        'men_titulo' => removeCE_Upper('D�vida enviada ao Judicial'),
                        'men_remetente' => 'SISTEMA',
                        'men_tipo' => '1',
                        'men_texto' => removeCE_Upper('Acordo c�digo: <b>' . $this->input->post('cobCodJud', true) . '</b> foi enviada para o setor Jur�dico no dia: <b>' . date('d/m/Y') . '</b> pelo usu�rio: <b>' . $this->input->post('usuResp', true) . '</b>; Favor averiguar.'),
                        'men_lida' => '0'
                    );
                    $this->db->insert('mensagens', $data);
                }

//                INSERIR RO AUTOMATICO
                $dados = array(
                    'cobranca_cob_cod' => $cobrancaDados->cob_cod,
                    'operacoes_ope_cod' => '79',
                    'usuarios_usu_cod' => $this->session->userdata('usucod'),
                    'ros_data' => date('Y-m-d'),
                    'ros_hora' => date('H:i:s'),
                    'ros_detalhe' => 'ACORDO ENVIADO AO JUDICIAL PARA O USUARIO: ' . $usuRespNome
                );
                $this->db->insert('ros', $dados);

//                TUDO INSERIDO

                $this->inicore->setMensagem('success', 'Opera��o realizada com sucesso: Cobran�a c�digo ' . $this->input->post('cobCod', true) . ' enviada ao jur�dico', true);
                redirect(base_url() . 'divida/ficha/cod:' . $this->input->post('InadCod', true));
            } else {
                $this->inicore->setMensagem('error', 'Ocorreu algum erro na opera��o, tente novamente, caso o problema persistir procure o administrador do sistema', true);
                redirect(base_url() . 'divida/ficha/cod:' . $this->input->post('InadCod', true));
            }
        }
    }

}