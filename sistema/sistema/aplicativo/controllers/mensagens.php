<?php

class Mensagens extends Controller {

    function Mensagens() {
        parent::Controller();
    }

    function _remap($link) {

        $this->data['title'] = "EZ Cobran�a :: Mensagens";

        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME

        $this->load->helper("funcoes_helper");

        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'jquery.maxlength.js','plugin/jquery.maskedinput'));

        $this->load->model('mensagens_model', 'md');
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        //IR PARA O RESPECTIVO FLUXO DA URL
        if ($link == "recebidas") {
            $this->_recebidas();
        } else if ($link == "credores") {
            $this->_credores();
        } else if ($link == "saida") {
            $this->_saida();
        } else if ($link == "nova") {
            $this->_nova();
        } else if ($link == "novaMensagem") {
            $this->_novaMensagem();
        } else if ($link == "excluirMensagem") {
            $this->_excluirMensagem();
        }else if ($link == "excluirSelecionadas") {
            $this->_excluirSelecionadas();
        } else if ($link == "excluirMensagemCredor") {
            $this->_excluirMensagemCredor();
        } else if ($link == "marcarComoLida") {
            $this->_marcarComoLida();
        } else if ($link == "marcarComoLidaCredor") {
            $this->_marcarComoLidaCredor();
        } else if ($link == "responder") {
            $this->_responder();
        } else if($link == "excluirTodasMsgs"){
            $this->_apagarTodasMinhasMsgs();
        }
    }

    function _recebidas() {
    	
        // CARREGA P�GINA��O
        $this->load->library('pagination');
		
        /* Pega todas as mensagens do usu�rio da sess�o (caixa de entrada) */
        $codUsuariDaSessao = $this->session->userdata('usucod'); //pega o c�digo do usu�rio da sess�o
        
        /* -------------- PAGINACAO -------------- */
        $this->load->library('pagination'); //PAGINACAO
        $config['base_url'] = base_url() . 'mensagens/recebidas';
        $config['total_rows'] = $this->md->getMinhasMensagensRecebidasRows($codUsuariDaSessao);
        $config['per_page'] = 15; //qtd que ser� exib�do por p�gina
        $config['uri_segment'] = 3;
        $config['last_link'] = '&gt;&gt;';
        $config['first_link'] = '&lt; &lt;';

        $this->pagination->initialize($config);
        $this->data['paginacao'] = $this->pagination->create_links();

        $pag = $this->uri->segment($config['uri_segment']);
        if (!isset($pag) || $pag == '' || !is_numeric($pag))  {
        	$pag = 0;
		}

        if ($config['total_rows'] == 0) {
            //$this->inicore->setMensagem('notice', "Nenhum inadimplente encontrado!", true);
            //redirect(base_url() . 'home');
        }
        /* -------------- PAGINACAO -------------- */
        
        $this->data['totalMensagensRecebidas'] = $config['total_rows'];
        $this->data['minhasMensagensRecebidas'] = $this->md->getMinhasMensagensRecebidas($codUsuariDaSessao, $pag, $config['per_page']);
		        
        $this->inicore->loadSidebar();
        $this->inicore->loadview('men_recebidas', $this->data);
    }

    function _credores() {
    	
        // CARREGA P�GINA��O
        $this->load->library('pagination');
		
        /* Pega todas as mensagens do usu�rio da sess�o (caixa de entrada) */
        $codUsuariDaSessao = $this->session->userdata('usucod'); //pega o c�digo do usu�rio da sess�o
        
        /* -------------- PAGINACAO -------------- */
        $this->load->library('pagination'); //PAGINACAO
        $config['base_url'] = base_url() . 'mensagens/credores';
        $config['total_rows'] = $this->md->getMinhasMensagensCredorRows($codUsuariDaSessao);
        $config['per_page'] = 15; //qtd que ser� exib�do por p�gina
        $config['uri_segment'] = 3;
        $config['last_link'] = '&gt;&gt;';
        $config['first_link'] = '&lt; &lt;';

        $this->pagination->initialize($config);
        $this->data['paginacao'] = $this->pagination->create_links();

        $pag = $this->uri->segment($config['uri_segment']);
        if (!isset($pag) || $pag == '' || !is_numeric($pag))  {
        	$pag = 0;
		}

        if ($config['total_rows'] == 0) {
            //$this->inicore->setMensagem('notice', "Nenhum inadimplente encontrado!", true);
            //redirect(base_url() . 'home');
        }
        /* -------------- PAGINACAO -------------- */
        
        $this->data['totalMensagensCredor'] = $config['total_rows'];
        $this->data['minhasMensagensCredor'] = $this->md->getMinhasMensagensCredor($codUsuariDaSessao, $pag, $config['per_page']);
		
        $this->inicore->loadSidebar();
        $this->inicore->loadview('men_credores', $this->data);
    }

    function _saida() {
        /* Pega todas as mensagens que o usu�rio da sess�o enviou (caixa de sa�da) */
        $usuarioDaSessao = $this->session->userdata('usunome'); //pega nome do usu�rio da sess�o.(usu_nome)
        //die("$usuarioDaSessao");
        $this->data['minhasMensagensEnviadas'] = $this->md->getMinhasMensagensEnviadas($usuarioDaSessao);
        $this->inicore->loadSidebar();
        $this->inicore->loadview('men_saida', $this->data);
    }

    function _nova() {
        /* Pega todos os usu�rios do sistema e imprime seus nomes no combo box DESTINAT�RIO (Formul�rio para enviar nova mensagem). */
        $this->data['usuUsuarioSis'] = $this->md->getUsuarios(); //chama o m�todo do model
        $this->inicore->loadSidebar();
        $this->inicore->loadview('men_nova', $this->data);
    }

    function _novaMensagem() {
        /* Recebe os dados digitados no formul�rio de nova mensagem */

        $this->load->library('form_validation');
        /* array que armazena os dados digitados nos campos do formul�rio */
        $dados = array(
            array(
                'field' => 'titulo',
                'label' => 'T�tulo',
                'rules' => 'required' //indica que o campo � necess�rio
            ),
            array(
                'field' => 'destinatario',
                'label' => 'Destinat�rio',
                'rules' => 'required'
            ),
            array(
                'field' => 'tipo_msg',
                'label' => 'Tipo da Mensagem',
                'rules' => 'required'
            ),
            array(
                'field' => 'mensagem',
                'label' => 'Mensagem',
                'rules' => 'required'
                ));

        $this->form_validation->set_message('required', "O campo %s � obrigat�rio"); //mensagem retornada se o campo n�o for preenchido. ("%s" imprime o nome do campo que n�o foi preenchido).
        $this->form_validation->set_rules($dados); //valida os campos que foi armazenado no array acima.

        if ($this->form_validation->run() == FALSE) {
            /* se algum campo obrigat�rio n�o for preenchido d� uma mensagem de erro e v� para o formul�rio de nova mensagem */
            $mensagem = validation_errors();
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'mensagens/nova');
        } else if
        /* Sen�o, se o campo destinat�rio for = 'todos', v� at� a tabela usu�rios, selecione todos os usu�rios e envie
         * a mensagem para todos os usu�rios do sistema.
         */
        ($this->input->post("destinatario") == 'todos') {//condi��o
            $destinatarios = $this->md->getUsuarios(); //pega todos usu�rios do sistema
            foreach ($destinatarios as $destinatario) {//foreach para enviar a mensagem para todos os usu�rios do sistema
                $dados = array(
                    'usuarios_usu_cod' => $destinatario->usu_cod, //recebe o cod do destinat�rio
                    'men_titulo' => utf8_encode(removeCE_Upper($this->input->post('titulo'))),
                    'men_remetente' => $this->session->userdata('usunome'), //pega o nome do usu�rio da sess�o
                    'men_tipo' => $this->input->post('tipo_msg'), //recebe o c�digo do tipo da tarefa
                    'men_texto' => utf8_encode(removeCE_Upper($this->input->post('mensagem'))), //campo mensagem
                );
                $this->md->setNovaMensagem($dados); //m�todo para gravar a mensagem no banco.(model)
            }
            $this->inicore->setMensagem('success', 'Mensagem enviada com sucesso para todos os usu�rios');
            redirect(base_url() . 'mensagens/nova');
        } else
        /* sen�o, enviar mensagem apenas para o usu�rio selecionado no combo box destinat�rio */ {
            $dados = array(
                'usuarios_usu_cod' => $this->input->post('destinatario'), //recebe o cod do destinat�rio
                'men_titulo' => utf8_encode(removeCE_Upper($this->input->post('titulo'))),
                'men_remetente' => $this->session->userdata('usunome'), //pega o nome do usu�rio da sess�o
                'men_tipo' => $this->input->post('tipo_msg'), //recebe o c�digo do tipo da tarefa
                'men_texto' => utf8_encode(removeCE_Upper($this->input->post('mensagem'))), //campo mensagem
            );
            $this->md->setNovaMensagem($dados); //chama o m�todo setNovaMensagem() do model
            $this->inicore->setMensagem('success', 'Mensagem enviada com sucesso');
            redirect(base_url() . 'mensagens/nova');
        }
    }

    function _marcarComoLida() {
        /* Marca uma mensagem como lida */
        $codMensagem = get('codMensagem'); //pega o c�digo da mensagem.(por get)
        if (is_numeric($codMensagem)) {//se o c�d da mensagem for um n�mero
            $qtdLinhasAfetadas = $this->md->setMarcarComoLida($codMensagem); //chame o m�todo do model
            if ($qtdLinhasAfetadas < 1) {//se, n�o afetar nenhuma linha no banco d� uma mensagem de erro
                $this->inicore->setMensagem('error', 'Erro ao realizar a opera��o!', true);
                redirect(base_url() . 'mensagens/recebidas');
            } else {
                $this->inicore->setMensagem('success', 'Opera��o realizada  com sucesso!', true);
                redirect(base_url() . 'mensagens/recebidas');
            }
            //$this->inicore->setMensagem('success', 'Opera��o realizada com sucesso!', true);
            // redirect(base_url() . 'mensagens/recebidas');
        } else {//sen�o d� uma mensagem de erro
            $this->inicore->setMensagem('error', 'Opera��o n�o autorizada!', true);
            redirect(base_url() . 'mensagens/recebidas');
        }
    }

    function _marcarComoLidaCredor() {
        /* Marca uma mensagem como lida */
        $codMensagem = get('codMensagem'); //pega o c�digo da mensagem.(por get)
        if (is_numeric($codMensagem)) {//se o c�d da mensagem for um n�mero
            $qtdLinhasAfetadas = $this->md->setMarcarComoLidaCredor($codMensagem); //chame o m�todo do model
            if ($qtdLinhasAfetadas < 1) {//se, n�o afetar nenhuma linha no banco d� uma mensagem de erro
                $this->inicore->setMensagem('error', 'Erro ao realizar a opera��o!', true);
                redirect(base_url() . 'mensagens/credores');
            } else {
                $this->inicore->setMensagem('success', 'Opera��o realizada  com sucesso!', true);
                redirect(base_url() . 'mensagens/credores');
            }
            //$this->inicore->setMensagem('success', 'Opera��o realizada com sucesso!', true);
            // redirect(base_url() . 'mensagens/recebidas');
        } else {//sen�o d� uma mensagem de erro
            $this->inicore->setMensagem('error', 'Opera��o n�o autorizada!', true);
            redirect(base_url() . 'mensagens/credores');
        }
    }
    
    function _responder() {
        //pega o codigo do usuario para quem vai a resposta
        $cod = get('cod');
        //pega o usuario do banco
        $this->data['resposta'] = $this->md->getUsuarioResposta($cod);
        $this->inicore->loadSidebar();
        $this->inicore->loadview('men_nova', $this->data);
    }

    function _excluirMensagem() {
        /* Verifica se o c�digo da mensagem que ser� exclu�da vem da caixa de entrada ou da caixa de sa�da */
        $codMensagem = get('codMensagem'); // pega o c�digo da mensagem (por get).
        $origem = get('origem'); //pega a origem do link. (pode ser caixa de entrada ou caixa de sa�da)
        if ($origem == 'entrada' && is_numeric($codMensagem)) {
            $qtdLinhasAfetadas = $this->md->excluirMensagem($codMensagem);

            if ($qtdLinhasAfetadas < 1) {//se, n�o afetar nenhuma linha no banco d� uma mensagem de erro
                $this->inicore->setMensagem('error', 'Erro ao excluir mensagem', true);
                redirect(base_url() . 'mensagens/recebidas');
            } else {
                $this->inicore->setMensagem('success', 'Mensagem apagada com sucesso', true);
                redirect(base_url() . 'mensagens/recebidas');
            }
        } else if ($origem == 'saida' && is_numeric($codMensagem)) {// a mesma coisa do la�o anterior, s� que o link de origem � da caixa de sa�da
            $qtdLinhasAfetadas = $this->md->excluirMensagem($codMensagem);
            if ($qtdLinhasAfetadas == 1) {
                $this->inicore->setMensagem('success', 'Mensagem apagada com sucesso', true);
                redirect(base_url() . 'mensagens/saida');
            } else {
                $this->inicore->setMensagem('error', 'Erro ao processar exclus�o', true);
                redirect(base_url() . 'mensagens/saida');
            }
        } else {
            if ($origem == 'entrada') {
                $this->inicore->setMensagem('error', 'Erro ao processar exclus�o', true);
                redirect(base_url() . 'mensagens/recebidas');
            } else if ($origem == 'saida'){
                $this->inicore->setMensagem('error', 'Erro ao processar exclus�o', true);
                redirect(base_url() . 'mensagens/saida');
            }else{
                $this->inicore->setMensagem('error', 'Erro ao processar exclus�o, voc� foi redirecionado para a home', true);
                redirect(base_url() . 'home');
            }
        }
    }
    
    function _excluirSelecionadas(){
        
        $origem = $this->input->post('origem');
        
        $msgs = substr($this->input->post('msgs_selecionadas'), 0, -1);
        
        $msgs_array = explode(',',$msgs);
        
        $qtd = 0;
        
        foreach($msgs_array as $msg){
            $this->md->excluirMensagem($msg);
            $qtd = $qtd + 1;
        }
        
        if($origem == 'entrada'){
            $this->inicore->setMensagem('success', $qtd. ' mensagens apagadas com sucesso', true);
            redirect(base_url() . 'mensagens/recebidas');
        }
        else if($origem == 'saida'){
            $this->inicore->setMensagem('success', $qtd. ' mensagens apagadas com sucesso', true);
            redirect(base_url() . 'mensagens/saida');
        }
        
        
    }
    
    function _excluirMensagemCredor() {
        /* Verifica se o c�digo da mensagem que ser� exclu�da vem da caixa de entrada ou da caixa de sa�da */
        $codMensagem = get('codMensagem'); // pega o c�digo da mensagem (por get).
        $origem = get('origem'); //pega a origem do link. (pode ser caixa de entrada ou caixa de sa�da)

        $qtdLinhasAfetadas = $this->md->excluirMensagemCredor($codMensagem);

            if ($qtdLinhasAfetadas < 1) {//se, n�o afetar nenhuma linha no banco d� uma mensagem de erro
                $this->inicore->setMensagem('error', 'Erro ao excluir mensagem', true);
                redirect(base_url() . 'mensagens/credores');
            } else {
                $this->inicore->setMensagem('success', 'Mensagem apagada com sucesso', true);
                redirect(base_url() . 'mensagens/credores');
            }
    }

}