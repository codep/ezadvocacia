<?php

class Cadrepasse extends Controller {

	function Cadrepasse()
	{
            parent::Controller();
	}
	
	function _remap($link)
	{
            
            $this->data['title']="Recupera :: Cadastro de repasse";

            $this->inicore->addcss(array('reset','style','style_fixed','colors/blue')); // CSS HOME

            $this->load->helper("funcoes_helper");

            $this->load->model('repasse_model','md'); //carregando o model

            $this->inicore->addjs(array('jquery-1.4.2.min.js','jquery-ui-1.8.custom.min.js','jquery.ui.selectmenu.js','jquery.flot.min.js','tiny_mce/jquery.tinymce.js','smooth.js','smooth.menu.js','smooth.table.js','smooth.form.js','smooth.dialog.js','smooth.autocomplete.js','plugin/jquery.maskedinput'));
            //monta a array com as permiss�es do usuario para testar se tem  permiss�o de acesso aos recursos
            $menus1 = explode(",", $this->session->userdata('menu1'));
            $menus2 = explode(",", $this->session->userdata('menu2'));
            $menus3 = explode(",", $this->session->userdata('menu3'));

//------------------------------------------------------------------------------
            include 'testar_conexao.php';
//------------------------------------------------------------------------------
            //IR PARA O RESPECTIVO FLUXO DA URL
            if($link == "novo") {
                if (array_search('10',$menus2,TRUE)!=''){
                    //fun��o responsavel por abrir a tela de novo usuario
                    $this->_novo();
                }else{
                    $this->inicore->setMensagem('error','Erro! Voc� n�o tem permiss�o para acessar este recurso');
                    redirect(base_url().'home');
                }
            }else if($link == "incluir") {
                if (array_search('10',$menus2,TRUE)!=''){
                    //fun��o responsavel por incluir os repasses no banco
                    $this->_incluir();
                }else{
                    $this->inicore->setMensagem('error','Erro! Voc� n�o tem permiss�o para acessar este recurso');
                    redirect(base_url().'home');
                }
            }
	}

        function _novo()
        {
            $this->session->set_userdata('menusel', '9');
            $this->inicore->loadSidebar();
            //seleciono todos os repasses e o ultimo repasse cadastrado para
            //exibi-los na p�gina de cadastro
            $this->data['repasses']=$this->md->getRepasses();
            $this->data['ultimoRepasse']=$this->md->getUltimoRepasse();
            $this->inicore->loadview('ger_repasse',$this->data);
        }

        function _incluir()
        {
            if($this->input->post('nome',true)==null){
                //dados obrigat�rios
                $this->inicore->setMensagem('error','Dados insuficientes para o cadastro',true);
                redirect(base_url().'cadrepasse/novo');
            }
//            monta a array com os dados que ser�o incluidos
            $dados = array(
                'rep_nome' => removeCE_Upper(trim($this->input->post('nome',true))),
                'rep_valor' => $this->input->post('valor',true)
            );
            //tenta inseri-los no banco de dados e retorna uma mensagem para o
            //usuario informando o resutlado da opera��o
            if($this->db->insert('repasses', $dados)){
                $this->inicore->setMensagem('success','Repasse cadastrado com sucesso',true);
                redirect(base_url().'cadrepasse/novo');
            }else{
                $this->inicore->setMensagem('error','Erro ao cadastrar o repasse, tente novamente; Se o erro persistir entre em contato com a administrador do sistema',true);
                redirect(base_url().'cadrepasse/novo');
            }
        }
}
