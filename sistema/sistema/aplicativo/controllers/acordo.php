<?php

class Acordo extends Controller {

    function Acordo() {
        parent::Controller();
    }

    function _remap($link) {

        $this->data['title'] = "Recupera :: Acordo";

        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME

        $this->load->helper("funcoes_helper");

        $this->load->model('Acordomodel', 'md');

        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput', 'jquery.moeda.js'));
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        //IR PARA O RESPECTIVO FLUXO DA URL
        if ($link == "novo") {
            $this->_novo();
        } else if ($link == "incluir") {
            $this->_incluir();
        } else if ($link == "calculoParcelas") {
            $this->_calculoParcelas();
        } else if ($link == "salvarAcordo") {
            $this->_salvarAcordo();
        } else if ($link == "reacordo") {
            $this->_reacordo();
        } else if ($link == "acordoRecemEnviado") {
            $this->_acordoRecemEnviado();
        }
    }

    function _novo() {
//die('You shall not pass!!');
        if ((!$this->input->post('dados_divida', true)) || ($this->input->post('dados_divida', true) == 'undefined|')) {
            $this->inicore->setMensagem('error', 'Erro ao acessar o acordo, tente novamente. Caso o erro persista contate o admiistrador do sistema', true);
            redirect(getBackUrl());
        }

        $dados = explode('|', $this->input->post('dados_divida', true));
        $cobranca = $dados[0];
        $this->data['parcelasCod'] = $dados[1];
        $parcelas = explode('-', $dados[1]);
        $filtro_parcelas = 'WHERE ';
        foreach ($parcelas as $parcela) {
            if ($parcela != '') {
                $filtro_parcelas .= "PD.pad_codigo = '$parcela' OR ";
            }
        }
        $filtro_parcelas = substr($filtro_parcelas, 0, -4);

        $parcelas_dados = $this->md->getParcDados($filtro_parcelas);

        $valor_total = 0.0;
        $prim_venc = $parcelas_dados[0]->pad_vencimento;
        foreach ($parcelas_dados as $dados) {
            $valor_total += $dados->pad_valor;
        }

        $cobCod = $this->md->getCobCod($filtro_parcelas);
        $cobCod = $cobCod->cob_cod;

        $cobrancasDados = $this->md->getCobDados($cobCod);

        $this->data['cobDados'] = $cobrancasDados;
        $this->data['valTotal'] = $valor_total;
        $this->data['valTotalDivida'] = $valor_total;
        $this->data['vencInicial'] = $prim_venc;
        $this->data['procedencia'] = 'ADM';
        $this->data['setorAcordo'] = 'ADM';
		$this->data['subsetorAcordo'] = $cobrancasDados->cob_subsetor;

        $this->inicore->loadSidebar();
        // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
        $this->inicore->loadview('ac_principal', $this->data);
    }

    function _reacordo() {

        header('Content-Type: text/html; charset=ISO-8859-15');

        if (!$this->input->post('dados_divida', true)) {
            $this->inicore->setMensagem('error', 'Erro ao acessar o acordo, tente novamente. Caso o erro persista contate o admiistrador do sistema', true);
            redirect(getBackUrl());
        }

        $dados = explode('|', $this->input->post('dados_divida', true));
        $cobranca = $dados[0];
        $this->data['parcelasCod'] = $dados[1];
        $parcelas = explode('-', $dados[1]);
        $filtro_parcelas = 'WHERE ';
        foreach ($parcelas as $parcela) {
            if ($parcela != '') {
                $filtro_parcelas .= "PD.paa_cod = '$parcela' OR ";
            }
        }
        $filtro_parcelas = substr($filtro_parcelas, 0, -4);

        $parcelas_dados = $this->md->getParcDadosAC($filtro_parcelas);

        $valor_total = 0.0;
        $prim_venc = $parcelas_dados[0]->paa_vencimento;
        foreach ($parcelas_dados as $dados) {
            $valor_total += $dados->paa_saldo_atual;
        }

        $cobCod = $this->md->getCobCodAC($filtro_parcelas);
        $cobCod = $cobCod->cob_cod;

        $cobrancasDados = $this->md->getCobDadosAcoJud($cobCod);

        $this->data['cobDados'] = $cobrancasDados;
        $this->data['valTotal'] = $valor_total;
        $this->data['valTotalDivida'] = $cobrancasDados->aco_valor_original;
        $this->data['vencInicial'] = $prim_venc;
        $this->data['procedencia'] = 'REA';
        $this->data['setorAcordo'] = $this->input->post('setor_reacordo', true);
		$this->data['subsetorAcordo'] = $cobrancasDados->cob_subsetor;

        $this->inicore->loadSidebar();
        // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
        $this->inicore->loadview('ac_principal', $this->data);
    }

    function _acordoRecemEnviado() {

        $cobCod = get('cod');

        header('Content-Type: text/html; charset=ISO-8859-15');

        if (!$cobCod) {
            $this->inicore->setMensagem('error', 'Erro ao acessar o acordo, tente novamente. Caso o erro persista contate o admiistrador do sistema', true);
            redirect(getBackUrl());
        }

        $cobranca = $cobCod;

        $parcela = $this->md->getParcelasAcoRecemEnviados($cobCod)->paa_cod;

        $this->data['parcelasCod'] = $parcela;
        $filtro_parcelas = "WHERE PD.paa_cod = '$parcela'";

        $parcelas_dados = $this->md->getParcDadosAC($filtro_parcelas);

        $valor_total = 0.0;
        $prim_venc = $parcelas_dados[0]->paa_vencimento;
        foreach ($parcelas_dados as $dados) {
            $valor_total += $dados->paa_saldo_atual;
        }

        $cobCod = $this->md->getCobCodAC($filtro_parcelas);
        $cobCod = $cobCod->cob_cod;

        $cobrancasDados = $this->md->getCobDadosAcoJud($cobCod);

		/*
        echo '<pre>';
        print_r($cobrancasDados);
        die('');
		 * */

        $this->data['cobDados'] = $cobrancasDados;
        $this->data['valTotal'] = $valor_total;
        $this->data['valTotalDivida'] = $cobrancasDados->aco_valor_original;
        $this->data['vencInicial'] = $prim_venc;
        $this->data['procedencia'] = 'JUD';
        $this->data['setorAcordo'] = 'JUD';
		$this->data['subsetorAcordo'] = $cobrancasDados->cob_subsetor;
        $this->data['cobJudCodAco'] = $cobCod;

        $this->inicore->loadSidebar();
        // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
        $this->inicore->loadview('ac_principal', $this->data);
    }

    function _incluir() {
        $this->inicore->loadSidebar();
        // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
        $this->inicore->loadview('ag_novatarefa', $this->data);
    }

    function _calculoParcelas() {

        $n = 0.0;
//        taxa de juros
        $i = floatval($_POST["juros"]);
//        total da divida
        $pv = floatval($_POST["valorTotal"]);
//        valor da parcela desejada
        $pmt = floatval($_POST["parcela"]);
//        variavel auxiliar com o valor do juros
        $x = 1 + $i;

        $x = floatval($x);

        $aux = (1 / (1 - (($pv * $i) / $pmt)));

        $n = log($aux, $x);

//        Retorno
        die('{"status":' . $n . '}');
    }

    function _salvarAcordo() {

//        echo 'repasse: '.$this->input->post('repasse', true)
//        .'<br/> InadCod:'.$this->input->post('inadimplente_cod', true)
//        .'<br/>CredorDoc:'.$this->input->post('credor_cod', true)
//        .'<br/>Usucod: '.$this->input->post('usuario_cod', true) 
//        .'<br/>SETOR '.$this->input->post('cob_setor', true)
//        .'<br/>PRAZO '.$this->input->post('cob_prazo', true)
//        .'<br/> parcela Geradora'.$this->input->post('parcelas_geradoras_cod', true)
//        .'<br/> Cob Geradora '.$this->input->post('cobranca_geradora_cod', true)
//        .'<br/> AcoTipo: '.$this->input->post('acordo_tipo', true)
//        .'<br/> Valor ORigi'.$this->input->post('valor_original', true)
//        .'<br/> Total Div Atualizada '.$this->input->post('total_divida_atualizada', true)
//        .'<br/>Juros Total'.$this->input->post('juros_total', true)
//        .'<br/>Multa total'.$this->input->post('multa_total', true)
//        .'<br/>Honorario totral '.$this->input->post('honorario_total', true)
//        .'<br/>Juros taxa'.$this->input->post('juros_taxa_escolhido', true)
//        .'<br/>Honor�rio taxa'.$this->input->post('honorario_taxa_escolhido', true)
//        .'<br/>Custas'.$this->input->post('custas', true)
//        .'<br/>Desconto '.$this->input->post('desconto', true)
//        .'<br/>NumPAr'.$this->input->post('numero_total_parcelas', true)
//        .'<br/>JurosPar'.$this->input->post('total_juros_parcela', true)
//        .'<br/> VAlorpar'.$this->input->post('total_valor_parcela', true)
//        .'<br/> Par num '.$this->input->post('par_num', true)
//        .'<br/> ValPar'.$this->input->post('val_par', true)
//        .'<br/>Venc PAr '.$this->input->post('venc_par', true)
//        .'<br/>'
//        .'<br/>';
//                
//                
//                
//        die('Helder');

        if ($this->input->post('juros_taxa_escolhido', true) == null) {
            $jurosEscolhido = '0.0';
        }
        if ($this->input->post('honorario_taxa_escolhido', true) == null) {
            $honorarioTaxa = '0.0';
        }

//        die('Juros = '.$jurosEscolhido);
        if
        (
        /*         * *** DADOS OBRIGAT�RIOS ***** */
                ($this->input->post('repasse', true) != null) &&
                ($this->input->post('inadimplente_cod', true) != null) &&
                ($this->input->post('credor_cod', true) != '') &&
                ($this->input->post('usuario_cod', true) != null) &&
                ($this->input->post('cob_setor', true) != null) &&
                ($this->input->post('cob_prazo', true) != null) &&
                ($this->input->post('parcelas_geradoras_cod', true) != null) &&
                ($this->input->post('cobranca_geradora_cod', true) != null) &&
                ($this->input->post('acordo_tipo', true) != null) &&
                ($this->input->post('valor_original', true) != null) &&
                ($this->input->post('total_divida_atualizada', true) != null) &&
                ($this->input->post('juros_total', true) != null) &&
                ($this->input->post('multa_total', true) != null) &&
                ($this->input->post('honorario_total', true) != null) &&
                ($this->input->post('custas', true) != null) &&
                ($this->input->post('desconto', true) != null) &&
                ($this->input->post('numero_total_parcelas', true) != null) &&
                ($this->input->post('total_juros_parcela', true) != null) &&
                ($this->input->post('total_valor_parcela', true) != null) &&
                ($this->input->post('par_num', true) != null) &&
                ($this->input->post('val_par', true) != null) &&
                ($this->input->post('venc_par', true) != null)
        ) {

//        =====================================================================
//        ======================== DADOS DA COBRAN�A ==========================
//        =====================================================================

            $repasse = $this->input->post('repasse', true);
            $inadimplente = $this->input->post('inadimplente_cod', true);
            $credor = $this->input->post('credor_cod', true);
            $usuario_cod = $this->input->post('usuario_cod', true);
            $setor = $this->input->post('cob_setor', true);
			$subsetor = $this->input->post('cob_subsetor', true);
            $prazo = $this->input->post('cob_prazo', true);
            $parcelasGeradoras = explode('-', $this->input->post('parcelas_geradoras_cod', true));
            $sync = microtime();

//        echo('Repasse: ' . $repasse . '<br/> Inadimplente: ' . $inadimplente
//        . '<br/> Credor: ' . $credor . '<br/> Usuario: ' . $usuario_cod
//        . '<br/> Setor: ' . $setor . '<br/> Prazo: ' . $prazo
//        . '<br/> Sync: ' . $sync . '<br/><br/><br/><br/>');
//        print_r($parcelasGeradoras);
//
//        echo '<br/><br/><br/><br/>';
            $dados = array(
                'repasses_rep_cod' => $repasse,
                'inadimplentes_ina_cod' => $inadimplente,
                'credor_cre_cod' => $credor,
                'usuarios_usu_cod' => $usuario_cod,
                'cob_setor' => $setor,
                'cob_subsetor' => $subsetor,
                'cob_prazo' => $prazo,
                'cob_sync' => $sync
            );
            //INSERE OS DADOS NO BANCO
            $this->db->insert('cobrancas', $dados);
            //USA A VARIAVEL $sync PARA BUSCAR O REGISTRO INSERIDO NO BANCO
            $cob_cod = $this->md->getCobCad($sync);

//        =====================================================================
//        ========================= DADOS DO ACORDO ===========================
//        =====================================================================

            $cobranca_geradora = $this->input->post('cobranca_geradora_cod', true);
            $aco_tipo = $this->input->post('acordo_tipo', true);
            $aco_procedencia = $this->input->post('aco_procedencia', true);
            $aco_emissao = date('Y-m-d');

            $aco_valor_original = $this->input->post('valor_original', true);
            $aco_valor_original = str_replace('.', '', $aco_valor_original);
            $aco_valor_original = str_replace(',', '.', $aco_valor_original);

            $valor_original_divida = $this->input->post('valor_original_divida', true);
            $valor_original_divida = str_replace('.', '', $valor_original_divida);
            $valor_original_divida = str_replace(',', '.', $valor_original_divida);

            $aco_valor_atualizado = $this->input->post('total_divida_atualizada', true);
            $aco_valor_atualizado = str_replace('.', '', $aco_valor_atualizado);
            $aco_valor_atualizado = str_replace(',', '.', $aco_valor_atualizado);

            $aco_val_juros = $this->input->post('juros_total', true);
            $aco_val_juros = str_replace('.', '', $aco_val_juros);
            $aco_val_juros = str_replace(',', '.', $aco_val_juros);

            $aco_val_multa = $this->input->post('multa_total', true);
            $aco_val_multa = str_replace('.', '', $aco_val_multa);
            $aco_val_multa = str_replace(',', '.', $aco_val_multa);

            $aco_val_honorario = $this->input->post('honorario_total', true);
            $aco_val_honorario = str_replace('.', '', $aco_val_honorario);
            $aco_val_honorario = str_replace(',', '.', $aco_val_honorario);

            $aco_taxa_juros = isset($jurosEscolhido) ? $jurosEscolhido : '0.0';

            $aco_taxa_multa = $this->input->post('multa_taxa_escolhido', true) == null ? 0.0 : $this->input->post('multa_taxa_escolhido', true);

            $aco_taxa_honorario = isset($honorarioTaxa) ? $honorarioTaxa : '0.0';

            $aco_custas = $this->input->post('custas', true);
            $aco_custas = str_replace('.', '', $aco_custas);
            $aco_custas = str_replace(',', '.', $aco_custas);

            $aco_desconto = $this->input->post('desconto', true);
            $aco_desconto = str_replace('.', '', $aco_desconto);
            $aco_desconto = str_replace(',', '.', $aco_desconto);

            $aco_qtde_parcelas = $this->input->post('numero_total_parcelas', true);

            $aco_total_juros_parcelado = $this->input->post('total_juros_parcela', true);
            $aco_total_juros_parcelado = str_replace('.', '', $aco_total_juros_parcelado);
            $aco_total_juros_parcelado = str_replace(',', '.', $aco_total_juros_parcelado);

            $aco_total_parcelado = $this->input->post('total_valor_parcela', true);
            $aco_total_parcelado = str_replace('.', '', $aco_total_parcelado);
            $aco_total_parcelado = str_replace(',', '.', $aco_total_parcelado);

//        echo('Tipo: ' . $aco_tipo . '<br/> Procedencia: ' . $aco_procedencia
//        . '<br/> Emissao: ' . $aco_emissao . '<br/> Valor Original: ' . $aco_valor_original
//        . '<br/> Valor Atualizado: ' . $aco_valor_atualizado . '<br/> Valor Juros: ' . $aco_val_juros
//        . '<br/> Multa: ' . $aco_val_multa . '<br/> Honorario: ' . $aco_val_honorario
//        . '<br/> Custas: ' . $aco_custas . '<br/> Descontos: ' . $aco_desconto
//        . '<br/> Parcelas: ' . $aco_qtde_parcelas . '<br/> Taxa Juros: ' . $aco_taxa_juros
//        . '<br/> Taxa Multa: ' . $aco_taxa_multa . '<br/> Taxa Honorario: ' . $aco_taxa_honorario
//        . '<br/> Total de Juros das Parcelas: ' . $aco_total_juros_parcelado
//        . '<br/> Total parcelado: ' . $aco_total_parcelado . '<br/> Cobranca Geradora: ' . $cobranca_geradora);

            $dados = array(
                'cobranca_cob_cod' => $cob_cod->cob_cod,
                'aco_tipo' => $aco_tipo,
                'aco_procedencia' => $aco_procedencia,
                'aco_emissao' => $aco_emissao,
                'aco_valor_original' => $valor_original_divida,
                'aco_valor_atualizado' => $aco_valor_atualizado,
                'aco_val_juros' => $aco_val_juros,
                'aco_val_multa' => $aco_val_multa,
                'aco_val_honorario' => $aco_val_honorario,
                'aco_custas' => $aco_custas,
                'aco_desconto' => $aco_desconto,
                'aco_qtd_parc' => $aco_qtde_parcelas,
                'aco_taxa_juros' => $aco_taxa_juros,
                'aco_taxa_multa' => $aco_val_multa,
                'aco_taxa_honorario' => $aco_val_honorario,
                'aco_total_juros' => $aco_total_juros_parcelado,
                'aco_total_parcelado' => $aco_total_parcelado,
                'aco_cob_cod_geradora' => $cobranca_geradora
            );
//        INSERE OS DADOS NO BANCOs
            $this->db->insert('acordos', $dados);
            $aco_cod = $this->md->getAcoCad($cob_cod->cob_cod);
//        =====================================================================
//        ======================= DADOS DAS PARCELAS ==========================
//        =====================================================================
//        Juro do acordo / 30
            $par_juro_diario = ($aco_taxa_juros / 30);
            $par_juro_diario = number_format($par_juro_diario, 7);
            $i = 0;

            foreach ($this->input->post('par_num', true) as $num_par):
                $parcelas[$i]["num"] = $num_par;
                $parcelas[$i]["juros"] = $par_juro_diario;
                $i++;
            endforeach;

            $i = 0;
            $total_divida = 0.0;
            foreach ($this->input->post('val_par', true) as $val_par):
                $val_par = str_replace('.', '', $val_par);
                $parcelas[$i]["val"] = str_replace(',', '.', $val_par);
                $total_divida += $parcelas[$i]["val"];
                $i++;
            endforeach;

            $data = array(
                'aco_total_parcelado' => $total_divida
            );

            $this->db->where('aco_cod', $aco_cod->aco_cod);
            $this->db->update('acordos', $data);


            $i = 0;

            foreach ($this->input->post('venc_par', true) as $venc_par):
                $data = $venc_par; //recebe a data que o usu�rio digita no campo data do formul�rio de nova tarefa
                $dia = substr($data, 0, -8); //pega s� o dia da data
                $mes = substr($data, 3, -5); //pega s� o m�s da data
                $ano = substr($data, -4); //pega s� o ano data
                $venc = $ano . '-' . $mes . '-' . $dia; //atribui a uma vari�vel no formato aaaa-mm-dd
                $parcelas[$i]["venc"] = $venc;
                $i++;
            endforeach;

//        echo '<br/><br/><pre>';
//        print_r($parcelas);
//        die('');
            foreach ($parcelas as $parcela):
                $dados = array(
                    'acordos_aco_cod' => $aco_cod->aco_cod,
                    'paa_parcela' => $parcela['num'],
                    'paa_vencimento' => $parcela['venc'],
                    'paa_valor' => $parcela['val'],
                    'paa_saldo' => $parcela['val'],
                    'paa_juro_diario' => $parcela['juros'],
                    'paa_saldo_atual' => $parcela['val']
                );
                //INSERE OS DADOS NO BANCO
                $this->db->insert('par_acordos', $dados);
            endforeach;

            if ($aco_procedencia == 'ADM') {
                foreach ($parcelasGeradoras as $par):
                    if ($par != '') {
                        $data = array(
                            'pad_acordada' => '1'
                        );

                        $this->db->where('pad_codigo', $par);
                        $this->db->update('par_dividas', $data);
                    }
                endforeach;
                if ((!isset($this->md->getParcelasDividaTeste($cobranca_geradora)->cob_cod)) && (!isset($this->md->getParcelasAcordoTeste($cobranca_geradora)->cob_cod))) {
                    $data = array(
                        'cob_remocao' => '1'
                    );

                    $this->db->where('cob_cod', $cobranca_geradora);
                    $this->db->update('cobrancas', $data);
                }
            } else if ($aco_procedencia == 'REA') {

                $cobranca_geradoras = explode('-', $cobranca_geradora);

                foreach ($parcelasGeradoras as $par):
                    if ($par != '') {
                        $data = array(
                            'paa_situacao' => '2'
                        );

                        $this->db->where('paa_cod', $par);
                        $this->db->update('par_acordos', $data);
                    }
                endforeach;

                foreach ($cobranca_geradoras as $cobranca_geradora) {
                    if (!isset($this->md->getParcelasAcordoTeste($cobranca_geradora)->cob_cod)) {
                        $data = array(
                            'cob_remocao' => '4'
                        );

                        $this->db->where('cob_cod', $cobranca_geradora);
                        $this->db->update('cobrancas', $data);
                    }
                }
            }

            if ($aco_tipo == 1) {
                $data = array(
                    'cobranca_cob_cod' => $cob_cod->cob_cod,
                    'operacoes_ope_cod' => '54',
                    'usuarios_usu_cod' => $this->session->userdata('usucod'),
                    'ros_data' => date('Y-m-d'),
                    'ros_hora' => date('H:i:s'),
                    'ros_detalhe' => utf8_encode('ACORDO CADASTRADO NO SISTEMA'),
                );

                $this->db->insert('ros', $data);
            } else if ($aco_tipo == 2) {
                $data = array(
                    'cobranca_cob_cod' => $cob_cod->cob_cod,
                    'operacoes_ope_cod' => '55',
                    'usuarios_usu_cod' => $this->session->userdata('usucod'),
                    'ros_data' => date('Y-m-d'),
                    'ros_hora' => date('H:i:s'),
                    'ros_detalhe' => utf8_encode('PROMESSA DE ACORDO CADASTRADA NO SISTEMA'),
                );

                $this->db->insert('ros', $data);
            } else {
                $data = array(
                    'cobranca_cob_cod' => $cob_cod->cob_cod,
                    'operacoes_ope_cod' => '54',
                    'usuarios_usu_cod' => $this->session->userdata('usucod'),
                    'ros_data' => date('Y-m-d'),
                    'ros_hora' => date('H:i:s'),
                    'ros_detalhe' => utf8_encode('RENEGOCIA��O CADASTRADA NO SISTEMA'),
                );

                $this->db->insert('ros', $data);
            }

            if ($this->input->post('cobJudCodAco', true) != '') {
                $data = array(
                    'cob_remocao' => '1',
                    'cob_recem_enviada' => '0'
                );

                $this->db->where('cob_cod', $this->input->post('cobJudCodAco', true));
                $this->db->update('cobrancas', $data);
            }

            $this->inicore->setMensagem('success', 'Acordo cadastrado com sucesso', true);
            redirect(base_url() . 'divida/ficha/cod:' . $inadimplente);
        } else {
            die('Erro ao gerar acordo');
        }
    }

}

