<?php

class Acordoatualizaparcela extends Controller {

    function Acordoatualizaparcela() {
        parent::Controller();
    }

    function _remap() {

        $this->db->query("CALL SP_ATUALIZA_ACORDO('ok');");
        echo 'PARCELAS DE ACORDOS ATUALIZADAS COM SUCESSO EM: ' . date('d-m-Y, H:m:s') . '<br/>';
        $this->db->query("CALL SP_ATUALIZA_PRAZO_JUR();");
        echo 'PRAZO JURIDICO ATUALIZADO COM SUCESSO EM: ' . date('d-m-Y, H:m:s') . '<br/>';
        if (date('w') == '1') {
            $this->db->query("CALL SP_MENSAGEM_PRAZO_JUR();");
            die('MENSAGENS SOBRE PRAZO JUR�DICO ENVIADAS COM SUCESSO EM: ' . date('d-m-Y, H:m:s'));
        }
    }

}