<?php

class Imagem extends Controller {

	function __construct()
	{
            parent::Controller();
            // CARREGANDO HELPER FUNCOES
            $this->load->helper('funcoes');
	}

	function _remap()
	{
        
        //VERIFICA SE O ARQUIVO EXISTE
        $this->imagem = get('img')?get('img'):'';
        $this->pasta = get('pasta')?get('pasta').'/':'';
        
        if ( !is_file(APPPATH.'/imgdb/'.$this->pasta.$this->imagem) )
        $this->imagem = 'semimg.jpg';

        //imagem/caminho_do_arquivo/largura/altura/qualidade/tom
        if (substr($this->imagem, -4,1)=='_') {
            $ext = substr($this->imagem, -3,1);
            $this->imagem = str_replace('_'.$ext, '.'.$ext, $this->imagem);
        }
        if ($this->imagem=='') {
            die();
        } else {
            $imgdb = APPPATH.'imgdb/'.$this->pasta;
            $parametros = array('fullpath'=>$imgdb.$this->imagem);
            $this->load->library('Thumb', $parametros);
            $this->largura = get('l')?get('l'):0;
            $this->altura = get('a')?get('a'):0;
            $this->resizec = get('rc')?get('rc'):0;
            $this->alturac = get('ac')?get('ac'):0;
            $this->qualidade = get('q')?get('q'):70;
            $this->resizeperc = get('rp')?get('rp'):0;
            $this->marcadagua = get('ma')?$imgdb.get('ma'):'';
            $this->tom = get('t')?get('t'):0;
            $this->cropcenter = get('cc')?get('cc'):0;
            $this->cropx = get('cx')?get('cx'):0;
            $this->cropy = get('cy')?get('cy'):0;
            $this->cropw = get('cl')?get('cl'):0;
            $this->croph = get('ca')?get('ca'):0;
            //corta Npx em todos os lados a partir do centro
            if ($this->cropcenter>0)
            $this->thumb->cropFromCenter($this->cropcenter);
            //redimensiona a imagem de acordo com a largura e altura
            if ($this->largura>0 || $this->altura>0)
            $this->thumb->resize($this->largura, $this->altura);
            //verifica se deve ajustar pela altura e cortar a largura
            if ($this->resizec>0) {
                if ($this->thumb->currentDimensions['height']>$this->thumb->currentDimensions['width']) {
                    $this->thumb->resize($this->resizec, 0);
                } else {
                    $this->thumb->resize(0, $this->resizec);
                }
                $this->thumb->cropFromCenter($this->resizec);
            }
            //redimensiona a imagem de acordo com a porcentagem
            if ($this->resizeperc>0)
            $this->thumb->resizePercent($this->resizeperc);
            //corta a imagem de acordo com as coordenadas informadas
            if (($this->cropx>0 && $this->cropy>0) || ($this->cropw>0 && $this->croph>0))
            $this->thumb->crop($this->cropx,$this->cropy,$this->cropw,$this->croph);
            //aplica Marca dagua na imagem
            if ($this->marcadagua!='')
            $this->thumb->marcadagua($this->marcadagua);
            //modifica o tom da imagem para tom de cinza
            if ($this->tom==1)
            $this->thumb->tomcinza();
            //exibe a imagem
            $this->thumb->show($this->qualidade);
        }
	}
                
	function __destruct()
	{
            //vazia
	}

}

 ?>