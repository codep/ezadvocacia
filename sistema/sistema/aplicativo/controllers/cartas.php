<?php

class Cartas extends Controller {

    function Importacao() {
        parent::Controller();
    }

    function _remap($link) {

        $this->data['title'] = "Recupera :: Cartas";

        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME

        $this->load->helper("funcoes_helper");

        $this->load->model('Importmodel', 'md');

        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput', 'jquery.moeda.js'));
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        //IR PARA O RESPECTIVO FLUXO DA URL
        if ($link == "listar") {
            $this->_listar();
        } else {
            $this->inicore->setMensagem('error', 'Comportamento inesperado.', true);
            redirect(getBackUrl());
        }
    }

    function _listar() {
        $this->session->set_userdata('menusel', '38'); //DETERMINA O MENU QUE FICARA ABERTO
        $this->inicore->loadSidebar(); //barra lateral
        $this->inicore->loadview('cartas_lista', $this->data);
    }
}

