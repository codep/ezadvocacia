<?php

class Importacao extends Controller {

    function Importacao() {
        parent::Controller();
    }

    function _remap($link) {

        $this->data['title'] = "Recupera :: Importa��o";

        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME

        $this->load->helper("funcoes_helper");

        $this->load->model('Importmodel', 'md');

        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput', 'jquery.moeda.js'));
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        //IR PARA O RESPECTIVO FLUXO DA URL
        if ($link == "importar") {
            $this->_importar();
        } else if ($link == "principal") {
            $this->_importacao();
        } else if ($link == "exibirDados") {
            $this->_exibirDados();
        } else if ($link == "efetivar") {
            $this->_efetivar();
        } else if ($link == "efetivarTodas") {
            $this->_efetivarTodas();
        } else if ($link == "removerDuplicadas") {
            $this->_removerDuplicadas();
        } else if ($link == "removerDivida") {
            $this->_removerDivida();			
        } else if ($link == "listar") {
            $this->_listar();
        } else {
            $this->inicore->setMensagem('error', 'Comportamento inesperado.', true);
            redirect(getBackUrl());
        }
    }

    function _importar() {
        $dividasIncluidas = '';
//        die('Ainda nao ta pronto, n�o fu�e');
        // Pasta onde o arquivo vai ser salvo
        $_UP['pasta'] = APPPATH . 'upload/';
        // Tamanho m�ximo do arquivo (em Bytes)
        $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
        // Array com as extens�es permitidas
        $_UP['extensoes'] = array('txt');

        $_UP['renomeia'] = true;

        // Array com os tipos de erros de upload do PHP
        $_UP['erros'][0] = 'N�o houve erro';
        $_UP['erros'][1] = 'O arquivo no upload � maior do que o limite do PHP';
        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
        $_UP['erros'][4] = 'N�o foi feito o upload do arquivo';

        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
        if ($_FILES['arquivo']['error'] != 0) {
            die("N�o foi poss�vel fazer o upload, erro:<br />" . $_UP['erros'][$_FILES['arquivo']['error']]);
            exit; // Para a execu��o do script
        }

        // Caso script chegue a esse ponto, n�o houve erro com o upload e o PHP pode continuar
        // Faz a verifica��o da extens�o do arquivo
        $extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));

        if (array_search($extensao, $_UP['extensoes']) === false) {
            die("Por favor, envie arquivos com as seguintes extens�es: txt");
        }
        // Faz a verifica��o do tamanho do arquivo
        else if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
            die("O arquivo enviado � muito grande, envie arquivos de at� 2Mb.");
        }

        // O arquivo passou em todas as verifica��es, hora de tentar mov�-lo para a pasta
        else {

            $_FILES['arquivo']['name'] = removeCE_Upper($_FILES['arquivo']['name']); //remove caracteres especiais
            // Primeiro verifica se deve trocar o nome do arquivo

            if ($_UP['renomeia'] == true) {
                // Cria um nome baseado no UNIX TIMESTAMP atual e com extens�o .jpg
                $nome_final = time() . $_FILES['arquivo']['name'];
            } else {
                // Mant�m o nome original do arquivo
                $nome_final = $_FILES['arquivo']['name'];
            }

            // Depois verifica se � poss�vel mover o arquivo para a pasta escolhida
            if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $nome_final)) {
                // Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
            } else {
                // N�o foi poss�vel fazer o upload, provavelmente a pasta est� incorreta
                die("N�o foi poss�vel enviar o arquivo, tente novamente");
            }
        }

        // ponteiro do caminho para importa��o local
        $ponteiro = fopen("http://webhost/cobranca/sistema/aplicativo/upload/$nome_final", "r");
        $dadosTxt = '';
        while (!feof($ponteiro)) {
            $dadosTxt .= fgets($ponteiro, 4096);
        }
        fclose($ponteiro);

        $registros = explode('#', $dadosTxt);

        foreach ($registros as $registro) {
            $dados = explode('|', $registro);

            if (sizeof($dados) == '13') {

                $docCredor = $dados[0];
                $docInadimplente = $dados[1];
                $emissao = $dados[2];
                $qtdeParcelas = $dados[3];
                $vencInicial = $dados[4];
                $valorTotal = $dados[5];
                $tipoDoc = $dados[6];
                $banco = $dados[7];
                $agencia = $dados[8];
                $alinea = $dados[9];
                $informacoes = $dados[10];

                $inaCod = isset($this->md->getInaCod($docInadimplente)->ina_cod) ? $this->md->getInaCod($docInadimplente)->ina_cod : '';

                if ($inaCod == '') {
                    $inadDados = $dados[12];
                    $inadDados = explode('%', $inadDados);

                    if (sizeof($inadDados) == '33') {
                        $this->md->cadInad($inadDados[0], $inadDados[1], $inadDados[2], $inadDados[3], $docInadimplente, $inadDados[4], $inadDados[5], $inadDados[6], $inadDados[7], $inadDados[8], $inadDados[9], $inadDados[10], $inadDados[11], $inadDados[12], $inadDados[13], $inadDados[14], $inadDados[15], $inadDados[16], $inadDados[17], $inadDados[18], $inadDados[19], $inadDados[20], $inadDados[21], $inadDados[22], $inadDados[23], $inadDados[24], $inadDados[25], $inadDados[26], $inadDados[27], $inadDados[28], $inadDados[29], $inadDados[30], $inadDados[31], $inadDados[32]);
                        $inaCod = $this->md->getInaCod($docInadimplente)->ina_cod;
                        $duplicidade = '0';
                        $this->md->cadInadTemp($inadDados[0], $inadDados[1], $inadDados[2], $inadDados[3], $inadDados[4], $docInadimplente, $inadDados[5], $inadDados[6], $inadDados[7], $inadDados[8], $inadDados[9], $inadDados[10], $inadDados[11], $inadDados[12], $inadDados[13], $inadDados[14], $inadDados[15], $inadDados[16], $inadDados[17], $inadDados[18], $inadDados[19], $inadDados[20], $inadDados[21], $inadDados[22], $inadDados[23], $inadDados[24], $inadDados[25], $inadDados[26], $inadDados[27], $inadDados[28], $inadDados[29], $inadDados[30], $inadDados[31], $inadDados[32], $inaCod, $duplicidade);
                    } else {
                        die('erro72');
                    }
                } else {
                    $inadDados = $dados[12];
                    $inadDados = explode('%', $inadDados);
                    if (sizeof($inadDados) == '33') {
                        $duplicidade = '1';
                        $this->md->cadInadTemp($inadDados[0], $inadDados[1], $inadDados[2], $inadDados[3], $inadDados[4], $docInadimplente, $inadDados[5], $inadDados[6], $inadDados[7], $inadDados[8], $inadDados[9], $inadDados[10], $inadDados[11], $inadDados[12], $inadDados[13], $inadDados[14], $inadDados[15], $inadDados[16], $inadDados[17], $inadDados[18], $inadDados[19], $inadDados[20], $inadDados[21], $inadDados[22], $inadDados[23], $inadDados[24], $inadDados[25], $inadDados[26], $inadDados[27], $inadDados[28], $inadDados[29], $inadDados[30], $inadDados[31], $inadDados[32], $inaCod, $duplicidade);
                    } else {
                        die('erro85');
                    }
                }

                $creCod = $this->md->getCreCod($docCredor)->cre_cod;

                if (!isset($creCod)) {
//                    nao tem crador
                    die('erro 98');
                }

                $imp_erro = '0';

                $divida_cod = $this->md->cadastraDividaTemporaria($tipoDoc, $banco, $agencia, $alinea, $emissao, $qtdeParcelas, $vencInicial, $informacoes, $valorTotal, $creCod);
                $dividasIncluidas .= $divida_cod . '-';
//die('incluir'.$dividasIncluidas);
                $this->md->cadastraImportacao($inaCod, $duplicidade, $imp_erro, $divida_cod);

                $parcelas = $dados[11];

                $parcelasDados = explode('&', $parcelas);

                foreach ($parcelasDados as $parcela) {
                    $parcela = explode('%', $parcela);

                    $parnum = $parcela[0];
                    $parvenc = $parcela[1];
                    $parvalor = $parcela[2];

                    $this->md->cadastraParcela($divida_cod, $parnum, $parvenc, $parvalor);
                }
            } else {
                ////////////////////********************************* EEEEEEEEEEERRRRRRRRRRROOOOOOOO
                die('Nao passou');
            }
        }
        redirect(base_url() . 'importacao/exibirDados/cod:' . $dividasIncluidas);
//
//
//
//
//        $this->data['title'] = "Recupera :: Importa��o";
//        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
//        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput'));
//        $this->session->set_userdata('menusel', '38'); //DETERMINA O MENU QUE FICARA ABERTO
//        $this->inicore->loadSidebar(); //barra lateral
//        $this->inicore->loadview('importar', $this->data);
    }

    function _importacao() {
        $this->session->set_userdata('menusel', '38'); //DETERMINA O MENU QUE FICARA ABERTO
        $this->inicore->loadSidebar(); //barra lateral
        $this->inicore->loadview('importar_principal', $this->data);
    }

    function _exibirDados() {
        $cods = get('cod');
        $cods = str_replace('-', ',', $cods);
        $cods = substr($cods, 0, (strlen($cods) - 1));
        if ($this->data['dividas'] = $this->md->getDividasDados($cods)) {
            $cods = explode(',', $cods);

            $credor = $this->data['credorDados'] = $this->md->getCredDados($cods[0]);


            $this->session->set_userdata('menusel', '38'); //DETERMINA O MENU QUE FICARA ABERTO
            $this->inicore->loadSidebar(); //barra lateral
            $this->inicore->loadview('importar_resultado', $this->data);
        }else{
             $this->inicore->setMensagem('error', "Nenhuma d�vida importada at� o momento!", true);
            redirect(base_url());
        }
    }

    function _listar() {

        $credores = $this->data['credoresImportPendentes'] = $this->md->getCredoresImportPendentes();
//        echo '<pre>';
//    print_r($credores);
//    die($credores);

        foreach ($credores as $credor) {
            $importacoesPendentes[$credor->div_cre_cod] = $this->md->getImportacoesPendentes($credor->div_cre_cod);
            $repasses[$credor->div_cre_cod] = $this->md->getRepassesCredor($credor->div_cre_cod);
            $repasses[$credor->div_cre_cod] = explode(',', $repasses[$credor->div_cre_cod]->cre_repasses);
                    
        }
        
           
        // se houver d�vidas pendentes, essas vari�veis ser�o utilizadas, se n�o, temos que deix�-las sem uso.
          if(!empty($credores))
          {
            // destruindo o primeiro item do array. � uma v�rgula que a fun��o explode atribui vazio
            unset($repasses[$credor->div_cre_cod][0]);

            $aux = 0;
            foreach ($repasses[$credor->div_cre_cod] as $rep){
                $repasses['nomes'][$aux] = $this->md->getRepasseInfo($rep);
                $aux++;
            }

            $this->data['repasses'] = $repasses;
            $this->data['importacoesPendentes'] = $importacoesPendentes;
            $this->data['mensagemTitulo'] = "Importa��o efetuada com sucesso:";
          }
          else
            $this->data['mensagemTitulo'] = "Nenhuma d�vida foi importada!";

        $this->session->set_userdata('menusel', '38'); //DETERMINA O MENU QUE FICARA ABERTO
        $this->inicore->loadSidebar(); //barra lateral
        $this->inicore->loadview('importacao_lista', $this->data);
    }

    function _efetivar() {

        $aux = get('cod');

        if ($aux == '') {
            $this->inicore->setMensagem('error', 'N�o foi possivel cadastrar a d�vida', true);
            redirect(base_url() . 'importacao/listar');
        } else {

            $aux = explode('-', $aux);
            $codDivida = $aux[0];
            $codRepasse = $aux[1];

            $dadosDivida = $this->md->getDadosDividaEfetivar($codDivida);
            $parcelas = $this->md->getDadosParcelasEfetivar($codDivida);
            $repasse = $codRepasse;

//        echo '<pre>';
//        print_r($dadosDivida);
//        die('Efetiva');
//        die('oi '.$dadosDivida->imp_inad_cod);
            $sync = microtime();
            //MONTA UMA ARRAY COM OS DADOS REFERENTES A COBRAN�AS
            $dados = array(
                'repasses_rep_cod' => $repasse,
                'inadimplentes_ina_cod' => $dadosDivida->imp_inad_cod,
                'credor_cre_cod' => $dadosDivida->div_cre_cod,
                'usuarios_usu_cod' => $dadosDivida->usuarios_responsavel_cod,
                'cob_setor' => 'ADM',
                'cob_prazo' => $dadosDivida->cre_prazo,
                'cob_sync' => $sync
            );
            //INSERE OS DADOS NO BANCO
            $this->db->insert('cobrancas', $dados);
            //USA A VARIAVEL $sync PARA BUSCAR O REGISTRO INSERIDO NO BANCO
            $cob_cod = $this->md->getCobCad($sync);
			
			//ATIVA O INADIMPLENTE
			$this->db->update('inadimplentes', array('ina_ativo'=>1), array('ina_cod'=>$dadosDivida->imp_inad_cod));

            //COME�A A MONTAR A ARRAY DE PARCELAS... FUNCIONA DA SEGUINTE MANEIRA:
            /* PRIMEIRAMENTE EU TENHO UMA VARIAVEL i Q SERVIRA DE INDICE DA MINHA ARRAY
             * DEPOIS PARA CADA TIPO DE DADOS REFERENTES A PARCELA EU CRIO UM FOREACH
             * QUE IRA BUSCAR TODOS OS CAMPOS DE CADA PARCELA INSERIDA NA VIEW DE NOVA
             * DIVIDA, E INSERI-LOS COM UMA NOVA ORDEM EM UMA ARRAY DE DADOS
             */

            //MONTA UMA ARRAY COM OS DADOS DA TABELA D�VIDA
            $dadosDiv = array(
                'cobranca_cob_cod' => $cob_cod->cob_cod,
                'div_documento' => $dadosDivida->div_documento,
                'div_banco' => $dadosDivida->div_banco,
                'div_agencia' => $dadosDivida->div_agencia,
                'div_alinea' => $dadosDivida->div_alinea,
                'div_emissao' => $dadosDivida->div_emissao,
                'div_cadastro' => date('Y-m-d'),
                'div_qtd_parc' => $dadosDivida->div_qtd_parc,
                'div_venc_inicial' => $dadosDivida->div_venc_inicial,
                'div_info' => removeCE_Upper($dadosDivida->div_info),
                'div_total' => $dadosDivida->div_total,
                'div_prazo' => '40'
            );
            //INSERTA NO BANCO OS DADOS DA D�VIDA
            $this->db->insert('dividas', $dadosDiv);
//         die('Opa');
            //RECUPERA O CODIGO DA DIVIDA INSERIDA POR MEIO DO CODIGO DA COBRAN�A
            $div_cod = $this->md->getDivCad($cob_cod->cob_cod);

            // AGORA PARA CADA PARCELA QUE FOI MONTADA EXECUTA-SE UM INSERT NO
            //BANCO DE DADOS COM OS DADOS DA MESMA
            foreach ($parcelas as $par) {
                $dadosPar = array(
                    'dividas_div_cod' => $div_cod->div_cod,
                    'pad_doc_num' => empty($par->impp_doc_num)?('P' . $par->impp_num):$par->impp_doc_num,
                    'pad_par_num' => $par->impp_num,
                    'pad_vencimento' => $par->impp_venc,
                    'pad_valor' => $par->imp_valor
                );

                $this->db->insert('par_dividas', $dadosPar);
            }

            $data = array(
                'imp_status' => '1',
                'imp_divida_cod' => $div_cod->div_cod,
                'imp_data' => date('Y-m-d h:i:s')
            );

            $this->db->where('imp_cod', $dadosDivida->imp_cod);
            $this->db->update('importacao', $data);

	        $data = array(
	            'cobranca_cob_cod' => $cob_cod->cob_cod,
	            'operacoes_ope_cod' => '53',
	            'usuarios_usu_cod' => $this->session->userdata('usucod'),
	            'ros_data' => date('Y-m-d'),
	            'ros_hora' => date('H:i:s'),
	            'ros_detalhe' => utf8_encode('EFETIVA��O DE COBRAN�A NO SISTEMA VIA IMPORTA��O')
	        );

	        $this->db->insert('ros', $data);
			
            //SE TUDO OCORRER BEM, O SISTEMA RETORNA PARA O USU�RIO UMA MENSAGEM DE
            //SUCESSO E VOLTA PARA A TELA DE NOV DIVIDA(POR ENQUANTO)
            $this->inicore->setMensagem('success', 'D�vida importada com sucesso', true);
            redirect(base_url() . 'importacao/listar');
        }
    }

    function _efetivarTodas() {

        $arrayAux = array();
        $i = 0;
        $tamanho = sizeof($_POST);
        foreach (array_keys($_POST) as $valor) {
            if (($i + 1) != $tamanho) {
                $arrayAux[$i]['key'] = $valor;
                $i++;
            }
        }
        $i = 0;
        foreach ($_POST as $valor) {
            if (($i + 1) != $tamanho) {
                $arrayAux[$i]['repasse'] = $valor;
                $i++;
            }
        }

        foreach ($arrayAux as $valores) {

            $codDivida = explode('_', $valores['key']);
            $codDivida = $codDivida[2];

            $repasse = $valores['repasse'];

            $codRepasse = $repasse;

            $dadosDivida = $this->md->getDadosDividaEfetivar($codDivida);
            $parcelas = $this->md->getDadosParcelasEfetivar($codDivida);
            $repasse = $codRepasse;

            $sync = microtime();
            //MONTA UMA ARRAY COM OS DADOS REFERENTES A COBRAN�AS
            $dados = array(
                'repasses_rep_cod' => $repasse,
                'inadimplentes_ina_cod' => $dadosDivida->imp_inad_cod,
                'credor_cre_cod' => $dadosDivida->div_cre_cod,
                'usuarios_usu_cod' => $dadosDivida->usuarios_responsavel_cod,
                'cob_setor' => 'ADM',
                'cob_prazo' => $dadosDivida->cre_prazo,
                'cob_sync' => $sync
            );
            //INSERE OS DADOS NO BANCO
            $this->db->insert('cobrancas', $dados);
            //USA A VARIAVEL $sync PARA BUSCAR O REGISTRO INSERIDO NO BANCO
            $cob_cod = $this->md->getCobCad($sync);
			
			//ATIVA O INADIMPLENTE
			$this->db->update('inadimplentes', array('ina_ativo'=>1), array('ina_cod'=>$dadosDivida->imp_inad_cod));

            //COME�A A MONTAR A ARRAY DE PARCELAS... FUNCIONA DA SEGUINTE MANEIRA:
            /* PRIMEIRAMENTE EU TENHO UMA VARIAVEL i Q SERVIRA DE INDICE DA MINHA ARRAY
             * DEPOIS PARA CADA TIPO DE DADOS REFERENTES A PARCELA EU CRIO UM FOREACH
             * QUE IRA BUSCAR TODOS OS CAMPOS DE CADA PARCELA INSERIDA NA VIEW DE NOVA
             * DIVIDA, E INSERI-LOS COM UMA NOVA ORDEM EM UMA ARRAY DE DADOS
             */

            //MONTA UMA ARRAY COM OS DADOS DA TABELA D�VIDA
            $dadosDiv = array(
                'cobranca_cob_cod' => $cob_cod->cob_cod,
                'div_documento' => $dadosDivida->div_documento,
                'div_banco' => $dadosDivida->div_banco,
                'div_agencia' => $dadosDivida->div_agencia,
                'div_alinea' => $dadosDivida->div_alinea,
                'div_emissao' => $dadosDivida->div_emissao,
                'div_cadastro' => date('Y-m-d'),
                'div_qtd_parc' => $dadosDivida->div_qtd_parc,
                'div_venc_inicial' => $dadosDivida->div_venc_inicial,
                'div_info' => removeCE_Upper($dadosDivida->div_info),
                'div_total' => $dadosDivida->div_total,
                'div_prazo' => '40'
            );
            //INSERTA NO BANCO OS DADOS DA D�VIDA
            $this->db->insert('dividas', $dadosDiv);
//         die('Opa');
            //RECUPERA O CODIGO DA DIVIDA INSERIDA POR MEIO DO CODIGO DA COBRAN�A
            $div_cod = $this->md->getDivCad($cob_cod->cob_cod);

            // AGORA PARA CADA PARCELA QUE FOI MONTADA EXECUTA-SE UM INSERT NO
            //BANCO DE DADOS COM OS DADOS DA MESMA
            foreach ($parcelas as $par) {
                $dadosPar = array(
                    'dividas_div_cod' => $div_cod->div_cod,
                    'pad_doc_num' => empty($par->impp_doc_num)?('P' . $par->impp_num):$par->impp_doc_num,
                    'pad_par_num' => $par->impp_num,
                    'pad_vencimento' => $par->impp_venc,
                    'pad_valor' => $par->imp_valor
                );

                $this->db->insert('par_dividas', $dadosPar);
            }

            $data = array(
                'imp_status' => '1',
                'imp_divida_cod' => $div_cod->div_cod,
                'imp_data' => date('Y-m-d h:i:s')
            );

            $this->db->where('imp_cod', $dadosDivida->imp_cod);
            $this->db->update('importacao', $data);
			
	        $data = array(
	            'cobranca_cob_cod' => $cob_cod->cob_cod,
	            'operacoes_ope_cod' => '53',
	            'usuarios_usu_cod' => $this->session->userdata('usucod'),
	            'ros_data' => date('Y-m-d'),
	            'ros_hora' => date('H:i:s'),
	            'ros_detalhe' => utf8_encode('EFETIVA��O DE COBRAN�A NO SISTEMA VIA IMPORTA��O')
	        );
	        $this->db->insert('ros', $data);
        }

        $this->inicore->setMensagem('success', 'Todas as d�vidas foram importadas com sucesso', true);
        redirect(base_url() . 'importacao/listar');
    }

	function _removerDuplicadas() {
		
		$cre_cod = $this->uri->segment(3);
		
		if (empty($cre_cod) || !is_numeric($cre_cod)) {
	        $this->inicore->setMensagem('error', 'O credor selecionado n�o foi encontrado nas importa��es.', true);
		} else {
			
			$aff_rows = $this->md->removeDivDupCredor($cre_cod);
			if ($aff_rows>0) {
				$this->inicore->setMensagem('success', 'As d�vidas duplicadas foram removidas com sucesso da importa��o. Total: '.$aff_rows, true);	
			} else {
				$this->inicore->setMensagem('success', 'Nenhum d�vida duplicada foi encontrada para esse credor nas importa��es.', true);
			}		
				
		}
		
		redirect(base_url() . 'importacao/listar');
	}
	
	function _removerDivida() {
		
		$div_cod = $this->uri->segment(3);
		
		if (empty($div_cod) || !is_numeric($div_cod)) {
	        $this->inicore->setMensagem('error', 'Nenhuma d�vida importada foi encontrada.', true);
		} else {
			$aff_rows = $this->md->removeDiv($div_cod);
			if ($aff_rows>0) {
				$this->inicore->setMensagem('success', 'A d�vida selecionada foi removida com sucesso da importa��o.', true);	
			} else {
				$this->inicore->setMensagem('success', 'Nenhum d�vida foi encontrada para remo��o na importa��o.', true);
			}		
				
		}
		
		redirect(base_url() . 'importacao/listar');
	}

}

