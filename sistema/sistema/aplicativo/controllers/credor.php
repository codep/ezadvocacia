<?php

class Credor extends Controller {

    function Credor() {
        parent::Controller();
    }

    function _remap($link) {

        $this->data['title'] = "Recupera :: Credor";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->load->helper("funcoes_helper");
        $this->load->model('Credormodel', 'md');
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js','jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput'));

        //monta a array com as permiss�es do usuario para testar se tem  permiss�o de acesso aos recursos
        //escolhendo o menu que ficara selecionado;
        $menus1 = explode(",", $this->session->userdata('menu1'));
        $menus2 = explode(",", $this->session->userdata('menu2'));
        $menus3 = explode(",", $this->session->userdata('menu3'));

//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        //IR PARA O RESPECTIVO FLUXO DA URL
        if ($link == "novo") {
            if (array_search('4', $menus2, TRUE) != '') {
                //FUN��O RESPONSAVEL POR ABRIR A TELA DE NOVO CREDOR
                $this->_novo();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "incluir") {
            if (array_search('4', $menus2, TRUE) != '') {
                // FUN��O RESPONSAVEL POR INSERIR OS DADOS DO CREDOR NO BANCO DE DADOS
                $this->_incluir();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "listar") {
            if (array_search('16', $menus3, TRUE) != '') {
                // FUN��O RESPONS�VEL POR LISTAR OS CREDORES
                $this->_listar();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "excluir") {
            if (array_search('4', $menus2, TRUE) != '') {
                // FUN��O RESPONSAVEL POR ABRIR A TELA DE EXCLUS�O DE CREDORES
                $this->_excluir();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "excluir_representante") {
            if (array_search('4', $menus2, TRUE) != '') {
                // FUN��O RESPONSAVEL POR ABRIR A TELA DE EXCLUS�O DE CREDORES
                $this->_excluir_representantes();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "editar") {
            if (array_search('4', $menus2, TRUE) != '') {
                //FUN��O RESPONSAVEL POR ABRIR A TELA DE EDI��O DE CREDORES
                $this->_editar();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "cadRep") {
            if (array_search('4', $menus2, TRUE) != '') {
                // FUN��O RESPONSAVEL POR CADASTRAR UM REPRESENTANTE PARA UM CREDOR
                $this->_cadRep();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "cadRel") {
            if (array_search('4', $menus2, TRUE) != '') {
                // FUN��O RESPONSAVEL POR CADASTRAR UM REPRESENTANTE PARA UM CREDOR
                $this->_cadRel();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "update") {
            if (array_search('4', $menus2, TRUE) != '') {
                // FUN��O RESPONSAVEL POR ATUALIZAR UM CREDOR
                $this->_update();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "delete") {
            if (array_search('4', $menus2, TRUE) != '') {
                // FUN��O RESPONSAVEL POR EXECUTAR O DELETE NO BANCO
                $this->_delete();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "delete_representante") {
            if (array_search('4', $menus2, TRUE) != '') {
                // FUN��O RESPONSAVEL POR EXECUTAR O DELETE NO BANCO
                $this->_delete_representante();
            } else {
                $this->inicore->setMensagem('error', 'Erro! Voc� n�o tem permiss�o para acessar este recurso');
                redirect(base_url() . 'home');
            }
        } else if ($link == "listar_cidades") {
            //FUN��O UTILIZADA NO AJAZ PARA LISTAR AS CIDADES DA LISTA DE CIDADES
            $this->_listar_cidades();
        } else if ($link == "listar_pesquisa") {
            // FUN��O RESPONSAVEL POR LISTAR A PESQUISA FEITA PELO USU�RIO
            $this->_listar_pesquisa();
        } else if ($link == "listar_repasses") {
            // FUN��O RESPONSAVEL POR LISTAR OS REPASSES DO CREDOR
            $this->_listar_repasses();
        } else {
            $this->inicore->setMensagem('warning', 'Recurso ainda n�o implementado, entre em contato com o administrador do sistema');
            redirect(base_url() . 'home');
        }
    }

    //FUN��O PARA CARREGAR cad_credor COM OS CAMPOS NECESS�RIOS PARA UM NOVO CADASTRO
    function _novo() {
        $this->session->set_userdata('menusel', '1');
        /* CARREGANDO OS DADOS PARA POPULAR OS SELECTS E CHECKBOXES DA VIEW
         * usuarios ------------- cont�m todos os usu�rios ativos no banco de dados
         * bancos --------------- cont�m todos os bancos cadastrados no banco de dados
         * repasses ------------- cont�m todos os repasses cadastrados no banco
         */
        $this->data['usuarios'] = $this->md->getUsuariosNomeCod();
        $this->data['bancos'] = $this->md->getBancos();
        $this->data['repasses'] = $this->md->getRepasses();
        $this->data['estados'] = $this->md->getEstados();


        //TESTANDO FUN��O DE PESQUISA
        //$this->data['pesquisateste']=pesquisaIC('c', 'j', 'c', '10.215.147/0001-52');

        $this->inicore->loadSidebar();
        // CARREGANDO A VIEW DE CREDOR
        $this->inicore->loadview('cad_credor', $this->data);
    }

    /* FUN��O AJAX PARA PESQUISAR POR NOME OU CPF_CNPJ
     * Esta fun��o recebe os dados por post (entidade, pessoa, tipodado, valor) e os repassa para
     * uma outra fun��o do arquivo funcoes_helper.php que, por sua vez, executa as queries necess�rias
     * para retornar os dados de acordo com os par�metros enviados
     * SIGNIFICADO DOS DADOS RECEBIDOS
     * $entidade -------- i=inadimplentes, c=credores
     * $pessoa ---------- j=jur�dica, f=f�sica
     * $tipoDado -------- n=nome, c=cpf_cnpj
     * $valor ----------- valor do nome ou cpf_cnpj
     */

    function _listar_pesquisa() {
        //RECEBENDO OS DADOS POST
        $entidade = $this->input->post('entidade', true);
        $pessoa = $this->input->post('pessoa', true);
        $tipoDado = $this->input->post('tipodado', true);
        $valor = removeCE_Upper(utf8_decode($this->input->post('valor', true)));

        //ATRIBUINDO A VARI�VEL $retorno OS DADOS RECEBIDOS DA FUN��O pesquisaIC
        $retorno = pesquisaIC($entidade, $pessoa, $tipoDado, $valor);

        //CASO O RETORNO CONTENHA ALGO FA�A
        if ($retorno != false) {
            //POPULANDO O ARRAY $dados COM OS DADOS RECEBIDOS NA VARI�VEL $retorno
            foreach ($retorno as $dadoatual) {
                $dados[] = array(
                    'codigo' => rawurlencode(utf8_decode($dadoatual->codigo)),
                    'recuperador_cod' => isset($dadoatual->recuperador_cod) ? rawurlencode(utf8_decode($dadoatual->recuperador_cod)) : '',
                    'recuperador_nome' => isset($dadoatual->recuperador_nome) ? rawurlencode(utf8_decode($dadoatual->recuperador_nome)) : '',
                    'nome' => rawurlencode(utf8_decode($dadoatual->nome)),
                    'cpf_cnpj' => rawurlencode(utf8_decode($dadoatual->cpf_cnpj)),
                    'endereco' => rawurlencode(utf8_decode($dadoatual->endereco)),
                    'cidade' => rawurlencode(utf8_decode($dadoatual->cidade)),
                    'prazo' => isset($dadoatual->prazo) ? rawurlencode(utf8_decode($dadoatual->prazo)) : ''
                );
            }
            //MATA O FLUXO RETORNANDO UM OBJETO DO TIPO JSON de nome "resultado" CONTENDO O ARRAY $dados
            die(
                    json_encode(array(
                        "resultado" => $dados
                    ))
            );
        }
        //CASO A VARI�VEL $retorno SEJA DO TIPO false, MATA O FLUXO E RETORNA UM OBJETO DO TIPO JSON VAZIO
        else {
            $dados[] = array('dadoatual' => '');

            die(
                    json_encode(array(
                        "resultado" => $dados
                    ))
            );
        }
    }

    /* FUN��O AJAX PARA LISTAR CIDADES BRASILEIRAS POR UF
     * v1 ------- dado com a UF enviada da view
     */

    function _listar_cidades() {
        //VALOR DO POST ENVIADO PELA VIEW
        $uf = $this->input->post('v1', true);
        if ($uf != 'UF') {
            //VARI�VEL CONTENDO TODAS AS CIDADES DA UF
            $retorno = $this->md->getCidadesBrasil($uf);

            //POPULANDO O ARRAY $cidades COM OS DADOS RECEBIDOS NA VARI�VEL $retorno
            foreach ($retorno as $cidadeatual) {
                $cidades[] = array(
                    'cidadeatual' => rawurlencode(utf8_decode($cidadeatual->cid_nome))
                );
            }
            //MATA O FLUXO RETORNANDO UM OBJETO DO TIPO JSON de nome "cidadesbrasil" CONTENDO O ARRAY $cidades
            die(
                    json_encode(array(
                        "cidadesbrasil" => $cidades
                    ))
            );
        } else {
            $cidades[] = array('cidadeatual' => '');

            die(
                    json_encode(array(
                        "cidadesbrasil" => $cidades
                    ))
            );
        }
    }

    /* FUN��O PARA PERSISTIR OS DADOS DE UM NOVO CREDOR NO BANCO DE DADOS
     * UTILIZA��O DE STORED PROCEDURE PARA A PERSIST�NCIA DOS DADOS
     *
     */

    function _incluir() {
        $repasses = '';
        //$responsavel = '';
        $query = '';

        $arquivo = $this->input->post('logo', true);
        $this->load->helper(array('form', 'url'));

        if ($_FILES['logo']['name'] != null) {
            // Pasta onde o arquivo vai ser salvo
            $_UP['pasta'] = APPPATH . 'upload/';
            // Tamanho m�ximo do arquivo (em Bytes)
            $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
            // Array com as extens�es permitidas
            $_UP['extensoes'] = array('jpg', 'png', 'gif');

            // Pasta onde o arquivo vai ser salvo
            $_UP['pasta'] = APPPATH . 'upload/';
            // Tamanho m�ximo do arquivo (em Bytes)
            $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
            // Array com as extens�es permitidas
            $_UP['extensoes'] = array('jpg', 'png', 'gif');

            // Renomeia o arquivo? (Se true, o arquivo ser� salvo como .jpg e um nome �nico)
            $_UP['renomeia'] = false;

            // Array com os tipos de erros de upload do PHP
            $_UP['erros'][0] = 'N�o houve erro';
            $_UP['erros'][1] = 'O arquivo no upload � maior do que o limite do PHP';
            $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
            $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
            $_UP['erros'][4] = 'N�o foi feito o upload do arquivo';

            // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
            if ($_FILES['logo']['error'] != 0) {
                die("N�o foi poss�vel fazer o upload, erro:<br />" . $_UP['erros'][$_FILES['logo']['error']]);
                exit; // Para a execu��o do script
            }

            // Caso script chegue a esse ponto, n�o houve erro com o upload e o PHP pode continuar
            // Faz a verifica��o da extens�o do arquivo
            $extensao = strtolower(end(explode('.', $_FILES['logo']['name'])));
            if (array_search($extensao, $_UP['extensoes']) === false) {
                echo "Por favor, envie arquivos com as seguintes extens�es: jpg, png ou gif";
            }

            // Faz a verifica��o do tamanho do arquivo
            else if ($_UP['tamanho'] < $_FILES['logo']['size']) {
                echo "O arquivo enviado � muito grande, envie arquivos de at� 2Mb.";
            }

            // O arquivo passou em todas as verifica��es, hora de tentar mov�-lo para a pasta
            else {
                // Primeiro verifica se deve trocar o nome do arquivo
                if ($_UP['renomeia'] == true) {
                    // Cria um nome baseado no UNIX TIMESTAMP atual e com extens�o .jpg
                    $nome_final = time() . '.jpg';
                } else {
                    // Mant�m o nome original do arquivo
                    $nome_final = $_FILES['logo']['name'];
                }

                // Depois verifica se � poss�vel mover o arquivo para a pasta escolhida
                if (move_uploaded_file($_FILES['logo']['tmp_name'], $_UP['pasta'] . $nome_final)) {
                    // Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
                } else {
                    // N�o foi poss�vel fazer o upload, provavelmente a pasta est� incorreta
                }
            }
        }

        if (isset($_FILES['logo']['name'])) {
            $arquivoBanco = $_FILES['logo']['name'];
        } else {
            $arquivoBanco = '';
        }

//        
//        echo '<pre>';
//        print_r($datafile);
//
//die(' you shall not pass - '. $datafile['file_name']);
        if
        (
        /*         * *** DADOS OBRIGAT�RIOS ***** */
                ($this->input->post('responsavel', true) != null) &&
                ($this->input->post('cidade', true) != null) &&
                ($this->input->post('uf', true) != null) &&
                ($this->input->post('repasse', true) != null || $this->input->post('repasse', true) != '') &&
                ($this->input->post('nome_fantasia', true) != null) &&
                ($this->input->post('honorario', true) != null) &&
                ($this->input->post('juros', true) != null) &&
                ($this->input->post('prazo', true) != null)
        ) {
            //die('['.$this->input->post('cpf_cnpj',true).']');
            //CONCATENANDO REPASSES
            if ($this->input->post('repasse', true)) {
                foreach ($this->input->post('repasse', true) as $rep):
                    $repasses .= ',' . $rep;
                endforeach;
            }

            //CRIPTOGRAFANDO A SENHA SE DIFERENTE DE VAZIA
            if ($this->input->post('senha', true)) {
                $senha = sha1(strtolower($this->input->post('usuario', true)) . $this->input->post('senha', true));
            }
            else
                $senha = '';

            $query .= '\'' . $this->session->userdata('usucod') . '\',';
            $query .= $this->input->post('responsavel', true) . ',';
            $query .= '\'' . $this->input->post('pessoa', true) . '\',';
            $query .= '\'' . $this->input->post('sexo', true) . '\',';
            $query .= $this->input->post('estado_civil', true) . ',';
            $query .= '\'' . removeCE_Upper($this->input->post('razao_social', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('nome_fantasia', true)) . '\',';
            $query .= '\'' . $this->input->post('cpf_cnpj', true) . '\',';
            $query .= '\'' . $this->input->post('rg_ie', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('endereco', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('bairro', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('complemento', true)) . '\',';
            $query .= '\'' . $this->input->post('cep', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('cidade', true)) . '\',';
            $query .= '\'' . $this->input->post('uf', true) . '\',';
            $query .= '\'' . $this->input->post('fone1', true) . '\',';
            $query .= '\'' . $this->input->post('fone2', true) . '\',';
            $query .= '\'' . $this->input->post('fax', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('site', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('email', true)) . '\',';
            $query .= '\'' . $arquivoBanco . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('info', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('cont1_cargo', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('cont1_nome', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('cont1_email', true)) . '\',';
            $query .= '\'' . $this->input->post('cont1_fone', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('cont2_cargo', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('cont2_nome', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('cont2_email', true)) . '\',';
            $query .= '\'' . $this->input->post('cont2_fone', true) . '\',';
            $query .= '\'' . $this->input->post('banco', true) . '\',';
            $query .= '\'' . $this->input->post('agencia', true) . '\',';
            $query .= '\'' . $this->input->post('conta', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('sacado', true)) . '\',';
            $query .= $this->input->post('honorario', true) . ',';
            $query .= $this->input->post('juros', true) . ',';
            $query .= $this->input->post('prazo', true) . ',';
            $query .= '\'' . $repasses . '\',';
            $query .= '\'' . strtolower($this->input->post('usuario', true)) . '\',';
            $query .= '\'' . $senha . '\'';

            /* INSERINDO NO BANCO ATRAV�S DE STORED PROCEDURE (SP)
             * FUNCIONA APENAS COM O DRIVER MYSQLI
             * � NECESS�RIO ALTERAR O ARQUIVO database.php //modificar para// $db['default']['dbdriver'] = "mysqli";
             */
//                die("CALL SP_CADASTRA_CRED(".$query.");");
            //VARI�VEL RESPONS�VEL POR ARMAZENARO O SQL RETORNADO PELA SP
            //A SP RETORNA UM ROW COM ALIAS "msg" CONTENDO UMA STRING DE VALOR "CADASTRADO" OU COM O VALOR DA MENSAGEM DE ERRO
            $resultado = $this->db->query("CALL SP_CADASTRA_CRED(" . $query . ");");

            //CONTENDO O VALOR CADASTRADO SETA A MENSAGEM DE SUCESSO E RETORNA A VIEW
            if (utf8_decode($resultado->row()->msg) == 'CADASTRADO') {
                $this->inicore->setMensagem('success', 'Credor cadastrado com sucesso');
                redirect(base_url() . 'credor/novo');
            }
            //SE A SP RETORNAR OUTRO VALOR MATA O FLUXO COM O VALOR RETORNADO
            else {
                die(utf8_decode($resultado->row()->msg));
//                    $this->session->set_userdata(utf8_decode($resultado->row()->msg));
//                    redirect(base_url().'credor/novo');
            }
        } else {
            //SE N�O ATENDER AOS DADOS OBRIGAT�RIOS (NOT NULL NO BANCO DE DADOS)
            //SETA A MENSAGEM ABAIXO E RETORNA PARA A VIEW
            $this->session->setMensagem('error', 'Verifique os dados obrigat�rios.');
            redirect(base_url() . 'credor/novo');
        }
    }

    /* FUN��O RESPONS�VEL POR LISTAR OS CREDORES NA VIEW "credor_listar"
     * � NECESS�RIO O CARREGAMENTO DAS CIDADES DOS CREDORES CADASTRADOS PARA O FILTRO NA VIEW
     * DE ACORDO COM O FILTRO DA CIDADE � QUE A EXIBI��O E REALIZADA
     */

    function _listar() {
        $this->session->set_userdata('menusel', '9');
        //VARI�VEL CONTENDO O VALOR RECEBIDO POR POST SETADO COMO "filtro"
        $cidade = $this->input->post('filtro', true);
        
        
        //INICIA UMA VARIAVEL QUE SERA PASSADA PRA A VIEW E SERVIRA PARA DEIXAR
        //A OP��O SELECIONADA DO FILTRO
        $this->data['filtro'] = '';
        //TESTE PARA VER SE VEIO ALGUMA COISA POR POST NO FILTRO
        if ($this->input->post('filtro', true) != '') {
            //SE VIR UMA CIDADE MONTA O FILTRO COM O NOME DA MESMA
            $filtro = 'WHERE cre_ativo=\'1\' AND cre_cidade=\'' . utf8_encode($this->input->post('filtro', true)) . '\'';
            //PASSA O FILTRO PARA A SESS�O PARA QUANDO O USUARIO CLICAR NA PAGINA��O
            //O FILTRO QUE VEM POR POST N�O SE PERDER
            $this->session->set_userdata('filtro', utf8_encode($this->input->post('filtro', true)));
            //PASSA O FILTRO PARA DEIXAR A CIDADE SELECIONADA NA VIEW
            $this->data['filtro'] = $this->input->post('filtro', true);
        } else {
            //CASO N�O VENHA NADA POR POST NO FILTRO SETA O FILTRO PADR�O
            $filtro = 'WHERE cre_ativo=\'1\'';
        }
        //SE N�O VEIO NADA POR POST TESTA PARA SABER SE TEM ALGUM FILTRO NA SESS�O
        if ($this->session->userdata('filtro') != '') {
            //SE TIVER FILTRO NA SESS�O ATRIBUI A VARIAVEL PARA PASSA-LO PARA O
            //MODEL E SETA A VARIAVEL QUE VAI PARA A VIEW
            
            $filtro = 'WHERE cre_ativo=\'1\' AND cre_cidade=\'' . $this->session->userdata('filtro') . '\'';
            //die($filtro);
            $this->data['filtro'] = $this->session->userdata('filtro');
            $cidade = $this->session->userdata('filtro');
        }
        
        //die($filtro);
        //SE O FILTRO QUE VIR POR POST FOR ZERAR, ZERA O FILTRO E SETA O FILTRO PADR�O
        if ($this->input->post('filtro', true) == 'TODOS') {
            $filtro = 'WHERE cre_ativo=\'1\'';
            $this->session->unset_userdata('filtro');
        }
        
        
        // CARREGA P�GINA��O
        $this->load->library('pagination');
        //CONFIGURA��ES DE PAGINA��O PODEM SER ENCONTRADAS EM http://codeigniter.com/user_guide/libraries/pagination.html
        $config['base_url'] = base_url() . 'credor/listar';
		//var_dump($cidade);
        $config['total_rows'] = $this->md->getRows($cidade);
        $config['per_page'] = '15';
        $config['uri_segment'] = 3;
        $config['last_link'] = '&gt;&gt;';
        $config['first_link'] = '&lt; &lt;';
        
        $this->pagination->initialize($config);
        $this->data['paginacao'] = $this->pagination->create_links();
        $pag = $this->uri->segment($config['uri_segment']);
        if (!isset($pag) || $pag == '' || !is_numeric($pag))
            $pag = 0;
        
        

        //RECEBENDO AS CIDADES DOS CREDORES CADASTRADOS NO BANCO DE DADOS
        $this->data['cidades'] = $this->md->getCidades();

        //SE A VARI�VEL QUE CONT�M A CIDADE A SER FILTRADA FOR VAZIA OU CONTIVER O VALOR 'TODOS'
        //ATRIBUI NO ARRAY DE �NDICE 'credores' OS CREDORES RETORNADOS PELA FUN��O DO MODEL getCredores()
        if ($cidade == '' || $cidade == 'TODOS') {
            $this->data['credores'] = $this->md->getCredores($pag, $config['per_page'],'TODOS');
            $this->data['cidadeatual'] = '';
            //die("dsaf");
            $this->data['total'] = $this->md->getRows('TODOS');
        }
        //SEN�O EXECUTA A FUN��O DO MODEL PASSANDO A VARI�VEL $cidade COMO PAR�METRO
        //PARA POSTERIORMENTE FAZER A FILTRAGEM NA VIEW
        else {
            //die($filtro);
            $this->data['credores'] = $this->md->getCredores($pag, $config['per_page'],$filtro);
//            echo "<pre>";
//            print_r($this->data['credores']);
//            die("passou");
            $this->data['cidadeatual'] = $cidade;
            $this->data['total'] = $this->md->getRows($cidade);
            //die($filtro."-b");die("B");
        }
//die($filtro."-fim");die("FIM");
        //LA�O NECESS�RIO PARA CALCULAR A QUANTIDADE DE ATIVOS DE CADA CREDOR
        foreach ($this->data['credores'] as $cred):
            //EXECUTA A FUN��O DO MODEL getAtivos COM O C�DIGO DO CREDOR EM QUEST�O
            $aux = $this->md->getAtivos($cred->cre_cod);
            //VARI�VEL CONTENDO A QUANTIDADE DE ATIVOS RETORNADA DO SELECT COM O ALIAS ativos
            $ativos = $aux->ativos;
            //ATRIBUI A QUANTIDADE DE ATIVOS AO ARRAY COM �NDICE 'ativos', SENDO O �NDICE 'C�DIGO' O DO CREDOR EM QUEST�O
            $this->data['ativos'][$cred->cre_cod] = $ativos;
        endforeach;
        
        $this->inicore->loadSidebar();
        // CARREGANDO A VIEW DE PESQUISA DE INADIMPLENTE
        $this->inicore->loadview('credor_listar', $this->data);
    }

    /* A fun��o excluir possui uma tela de confirma��o de exclus�o que
     * serve para qualque exclus�o no sistema, para que esta tela funcione
     * corretamente deve ser passados algumas vari�veis que ser�o utilizadas
     * na constru��o da p�gina, as vari�veis necess�rias s�o:
     * $deletado-----------------------O que est� sendo deletado
     * $codigo-------------------------c�digo do que est� sendo deletado
     * $tabela-------------------------Tabela que ter� o item excluido
     * $chave--------------------------Chave primaria da tabela para o delete
     * $acaocancel---------------------A��o executada quando clicar em cancelar
     * $acaook-------------------------A��o executada quando clicar em Apagar
     */

    function _excluir() {
        $cod = get('cod');

        $this->data['title'] = "Recupera :: Confirmar exclus�o";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->inicore->addjs(array('smooth.form.js'));
        $this->data['deletado'] = 'CREDOR';
        $this->data['codigo'] = $cod;
        $this->data['tabela'] = 'credores';
        $this->data['chave'] = 'cre_cod';
        $this->data['acaocancel'] = 'credor/listar';
        $this->data['acaook'] = 'credor/delete';

        $this->inicore->loadview('conf_exclusao', $this->data);
    }

    //similar a _excluir()
    function _excluir_representantes() {
        $cod = get('cod');
        $credor = get('cre');

        $this->data['title'] = "Recupera :: Confirmar exclus�o";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        $this->inicore->addjs(array('smooth.form.js'));
        $this->data['deletado'] = 'REPRESENTANTE';
        $this->data['codigo'] = $cod;
        $this->data['tabela'] = 'rep_legais';
        $this->data['chave'] = 'rpl_cod';
        $this->data['acaocancel'] = 'credor/editar/cod:' . $credor . '#representantes';
        $this->data['acaook'] = 'credor/delete_representante';

        $this->inicore->loadview('conf_exclusao', $this->data);
    }

    //similar a _delete()
    function _delete_representante() {
        //PEGA A SENHA QUE O USUARIO PASSOU NA TELA DE EXCLUS�O E TESTA ELA
        //COM A SENHA DO BANCO DE DADOS
        $senha = sha1('senhamaster+' . $this->input->post('senha', true));
        $sql = "SELECT cod, senha FROM senha_master";
        $query = $this->db->query($sql);
        if ($query->row()->senha == $senha) {
            $cod_rep = $this->input->post('codigo', true);
            $representante_nome = $this->db->query("select * from rep_legais where rpl_cod = $cod_rep");
            $this->db->where($this->input->post('chave', true), $this->input->post('codigo', true));
            //SE FOREM IGUAIS AS SENHA ENT�O APAGA O REPRESENTANTE
            if ($this->db->delete($this->input->post('tabela', true))) {
                $this->inicore->setMensagem('notice', 'Representante apagado com sucesso', true);
                auditar('e', $this->session->userdata('usucod'), 'rep_legais', 'Representante legal: ' . $representante_nome->row()->rpl_nome);
            } else {
                $this->inicore->setMensagem('error', 'Erro ao apagar o representante, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
            }
            // VOLTA PARA ONDE A TELA DE EXCLUS�O APONTA
            redirect($this->input->post('voltar', true));
        } else {
            // SE A SENHA ESTIVER ERRADA EXIBE UMA MESAGEM DE ERRO E RETORNA A TELA ANTERIOR
            $this->inicore->setMensagem('error', 'SENHA incorreta, favor verificar e digitar novamente', true);
            redirect(base_url() . 'credor/excluir/cod:' . $this->input->post('codigo', true));
        }
    }

    /* FUN��O RESPONS�VEL POR PROCESSAR O QUE DEVE SER DELETADO NO BANCO DE DADOS
     * SOMENTE � POSS�VEL COM A DIGITA��O DA SENHA MASTER
     */

    function _delete() {
        //PEGA A SENHA QUE O USUARIO PASSOU NA TELA DE EXCLUS�O E TESTA ELA
        //COM A SENHA DO BANCO DE DADOS
        $senha = sha1('senhamaster+' . $this->input->post('senha', true));
        $sql = "SELECT cod, senha FROM senha_master";
        $query = $this->db->query($sql);
        if ($query->row()->senha == $senha) {
            $this->db->where($this->input->post('chave', true), $this->input->post('codigo', true));
            //se as senhas estiverem iguais apaga o usu�rio e exibe mensagem de acordo com o ocorrifo
            $dados = array(
                'cre_ativo' => '0'
            );
            if ($this->db->update('credores', $dados)) {
                $this->inicore->setMensagem('notice', 'Credor apagado com sucesso', true);
            } else {
                $this->inicore->setMensagem('error', 'Erro ao apagar o credor, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
            }

            auditar('e', $this->session->userdata('usucod'), 'credores', 'EXCLUIDO O CREDOR COD: ' . $this->input->post('codigo', true));

            //redireciona para onde deve ser redirecionado :)
            redirect($this->input->post('voltar', true));
            // caso a senha estiver errada monta a tela de exclus�o novamente  e
            // exibe mensagem de erro para o usu�rio
        }
//            if ($query->row()->senha==$senha) {
//                $this->db->where($this->input->post('chave',true), $this->input->post('codigo',true));
//                //SE FOREM IGUAIS AS SENHA ENT�O APAGA O CREDOR
//                if($this->db->delete($this->input->post('tabela',true))){
//                    $this->inicore->setMensagem('notice','Credor apagado com sucesso',true);
//                    auditar('e','rep_legais',$this->session->userdata('usucod'),'EXCLUIDO REPRESENTANTE LEGAL: '.$representante_nome->row()->rpl_nome);
//                }else{
//                    $this->inicore->setMensagem('error','Erro ao apagar o usu�rio, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema',true);
//                }
//                // VOLTA PARA ONDE A TELA DE EXCLUS�O APONTA
//                redirect($this->input->post('voltar',true));
//            }
        else {
            // SE A SENHA ESTIVER ERRADA EXIBE UMA MESAGEM DE ERRO E RETORNA A TELA ANTERIOR
            $this->inicore->setMensagem('error', 'SENHA incorreta, favor verificar e digitar novamente', true);
            redirect(base_url() . 'credor/excluir/cod:' . $this->input->post('codigo', true));
        }
    }

    /*
     * FUN��O RESPONS�VEL POR CARREGAR OS DADOS DO CREDOR E PERMITIR A EDI��O DOS MESMOS
     * � EXECUTADA COM BASE NO C�DIGO DO CREDOR RECEBIDO POR GET
     */

    function _editar() {
        $this->session->set_userdata('menusel', '1');
        $this->inicore->loadSidebar();

        //C�DIGO DO CREDOR RECEBIDO PELA URL
        $cod = get('cod');

        /* CARREGANDO OS DADOS PARA POPULAR OS SELECTS E CHECKBOXES DA VIEW
         * usuarios ------------- cont�m todos os usu�rios ativos no banco de dados
         * bancos --------------- cont�m todos os bancos cadastrados no banco de dados
         * repasses ------------- cont�m todos os repasses cadastrados no banco de dados
         * estados -------------- cont�m todos os estados cadastrados no banco de dados
         */
        $this->data['representatnes'] = $this->md->getRepresentantes($cod);
        $this->data['usuarios'] = $this->md->getUsuariosNomeCod();
        $this->data['bancos'] = $this->md->getBancos();
        $this->data['repasses'] = $this->md->getRepasses();
        $this->data['estados'] = $this->md->getEstados();
		$this->data['relatorios'] = $this->md->getRelatorios();

        $this->data['dados'] = $this->md->getCredor($cod);

        /* VERIFICANDO REPASSES SELECIONADOS PARA O CREDOR
         * O trecho de c�digo a seguir visa computar os repasses do credor em queset�o
         *
         * !!!!!!!!!!! C�DIGO PRECISA SER MELHORADO !!!!!!!!!!!!!!
         * SE FOR REMOVIDO ALGUM REPASSE DA SEQU�NCIA CADASTRADA NO BANCO IR� DAR PROBLEMA
         */

        //RECEBE OS REPASSES EM UM ARRAY DO DE TAMANHO = A QTD
        $repassescredor = explode(',', $this->data['dados']->cre_repasses);
        //UTILIZADA PARA CRIAR UM ARRAY COM TODOS OS REPASSES CONTIDOS NO BANCO
        $repassesbanco[] = '';
        //RECEBER� OS REPASSES EM UM ARRAY DO TAMANHO DA QUANTIDADE DE REPASSES TOTAIS PARA O CREDOR EM QUEST�O
        $repassechk[] = '';

        //VARI�VEIS AUXILIARES
        $aux = $this->data['repasses'][0]->rep_cod;
        $aux2 = $this->data['repasses'][0]->rep_cod;
        $ativo = true;

        //LA�O RESPONS�VEL POR CRIAR O ARRAY $repassesbanco E POPUL�-LO COM OS REPASSES CADASTRADOS NO BANCO DE DADOS
        foreach ($this->data['repasses'] as $rep1):
            //PERMITE QUE A POSI��O DO ARRAY RECEBA COMO VALOR A PR�PRIA POSI��O
            $repassesbanco[$aux2] = $aux2;
            $aux2++;
        endforeach;

        $i = 1;

        //ENQUANTO $i FOR MENOR QUE O TAMANHO DO ARRAY $repassescredor
        while ($i < count($repassescredor)) {
            //SE A VARI�VEL $aux FOR IGUAL AO VALOR DA POSI��O $i DO ARRAY $repassescredor
            if (($aux == $repassescredor[$i])) {
                $repassechk[$aux] = $aux;
                $aux++;
                $i++;
            } else {
                $repassechk[$aux] = 0;
                $aux++;
            }
        }

        //VARI�VEL QUE RECEBE O TAMANHO DO ARRAY $repassechk
        $aux2 = count($repassechk);

        //ENQUANTO $aux2 FOR MENOR QUE O TAMANHO DO ARRAY $repassesbanco
        while ($aux2 < count($repassesbanco)) {
            $repassechk[$aux2] = 0;
            $aux2++;
        }

        //RECEBE O ARRAY COM TODOS OS REPASSES DO CREDOR EM QUEST�O
        $this->data['repassescredor'] = $repassechk;

        $this->inicore->loadview('credor_editar', $this->data);
    }

    /*
     * FUN��O RESPONS�VEL POR ATUALIZAR OS DADOS RECEBIDOS DA FUN��O EDITAR, PROVENIENTES DA VIEW "credor_listar"
     */

    function _update() {
        //C�DIGO DO CREDOR ENVIADO POR POST
        $cod = $this->input->post('cod', true);
        //INSTANCIAMENTO DE VARI�VEIS A SEREM UTILIZADAS
        $repasses = '';
        $query = '';
        $logotipo = '';
        if ($_FILES['logo']['name'] == '') {
            $logotipo = $this->input->post('valida_logo', true);
        } else {
            $logotipo = $_FILES['logo']['name'];

            $_UP['pasta'] = APPPATH . 'upload/';
            // Tamanho m�ximo do arquivo (em Bytes)
            $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
            // Array com as extens�es permitidas
            $_UP['extensoes'] = array('jpg', 'png', 'gif');

            // Pasta onde o arquivo vai ser salvo
            $_UP['pasta'] = APPPATH . 'upload/';
            // Tamanho m�ximo do arquivo (em Bytes)
            $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
            // Array com as extens�es permitidas
            $_UP['extensoes'] = array('jpg', 'png', 'gif');

            // Renomeia o arquivo? (Se true, o arquivo ser� salvo como .jpg e um nome �nico)
            $_UP['renomeia'] = false;

            // Array com os tipos de erros de upload do PHP
            $_UP['erros'][0] = 'N�o houve erro';
            $_UP['erros'][1] = 'O arquivo no upload � maior do que o limite do PHP';
            $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
            $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
            $_UP['erros'][4] = 'N�o foi feito o upload do arquivo';

            // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
            if ($_FILES['logo']['error'] != 0) {
                die("N�o foi poss�vel fazer o upload, erro:<br />" . $_UP['erros'][$_FILES['logo']['error']]);
                exit; // Para a execu��o do script
            }

            // Caso script chegue a esse ponto, n�o houve erro com o upload e o PHP pode continuar
            // Faz a verifica��o da extens�o do arquivo
            $extensao = strtolower(end(explode('.', $_FILES['logo']['name'])));
            if (array_search($extensao, $_UP['extensoes']) === false) {
                echo "Por favor, envie arquivos com as seguintes extens�es: jpg, png ou gif";
            }

            // Faz a verifica��o do tamanho do arquivo
            else if ($_UP['tamanho'] < $_FILES['logo']['size']) {
                echo "O arquivo enviado � muito grande, envie arquivos de at� 2Mb.";
            }

            // O arquivo passou em todas as verifica��es, hora de tentar mov�-lo para a pasta
            else {
                // Primeiro verifica se deve trocar o nome do arquivo
                if ($_UP['renomeia'] == true) {
                    // Cria um nome baseado no UNIX TIMESTAMP atual e com extens�o .jpg
                    $nome_final = time() . '.jpg';
                } else {
                    // Mant�m o nome original do arquivo
                    $nome_final = $_FILES['logo']['name'];
                }

                // Depois verifica se � poss�vel mover o arquivo para a pasta escolhida
                if (move_uploaded_file($_FILES['logo']['tmp_name'], $_UP['pasta'] . $nome_final)) {
                    // Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
                } else {
                    // N�o foi poss�vel fazer o upload, provavelmente a pasta est� incorreta
                }
            }
        }

        //SE TODOS OS DADOS OBRIGAT�RIOS N�O FOREM NULOS
        if
        (
        /*         * *** DADOS OBRIGAT�RIOS ***** */
                ($this->input->post('responsavel', true) != null) &&
                ($this->input->post('cidade', true) != null) &&
                ($this->input->post('uf', true) != null) &&
                ($this->input->post('repasse', true) != null || $this->input->post('repasse', true) != '') &&
                ($this->input->post('nome_fantasia', true) != null) &&
                ($this->input->post('honorario', true) != null) &&
                ($this->input->post('juros', true) != null) &&
                ($this->input->post('prazo', true) != null)
        ) {

            //CONCATENANDO REPASSES E INSERINDO V�RGULA ENTRE ELES
            if ($this->input->post('repasse', true)) {
                foreach ($this->input->post('repasse', true) as $rep):
                    $repasses .= ',' . $rep;
                endforeach;
            }

            //SE A SENHA FOR DIFERENTE DE VAZIA PERSISTE OS DADOS NO BANCO COM A NOVA SENHA
            if ($this->input->post('senha', true) != '') {
                //CRIPTOGRAFANDO A SENHA SE DIFERENTE DE VAZIA
                $senha = $this->input->post('senha', true);
                $usuario = strtolower($this->input->post('usuario', true));
                $senha = sha1($usuario . $senha);

                $dados = array
                    (
                    'usuarios_responsavel_cod' => $this->input->post('responsavel', true),
                    'cre_pessoa' => $this->input->post('pessoa', true),
                    'cre_sexo' => $this->input->post('sexo', true),
                    'cre_estado_civil' => $this->input->post('estado_civil', true),
                    'cre_razao_social' => removeCE_Upper($this->input->post('razao_social', true)),
                    'cre_nome_fantasia' => removeCE_Upper($this->input->post('nome_fantasia', true)),
                    'cre_cpf_cnpj' => $this->input->post('cpf_cnpj', true),
                    'cre_rg_ie' => $this->input->post('rg_ie', true),
                    'cre_endereco' => removeCE_Upper($this->input->post('endereco', true)),
                    'cre_bairro' => removeCE_Upper($this->input->post('bairro', true)),
                    'cre_complemento' => removeCE_Upper($this->input->post('complemento', true)),
                    'cre_cep' => $this->input->post('cep', true),
                    'cre_cidade' => removeCE_Upper($this->input->post('cidade', true)),
                    'cre_uf' => $this->input->post('uf', true),
                    'cre_fone1' => $this->input->post('fone1', true),
                    'cre_fone2' => $this->input->post('fone2', true),
                    'cre_fax' => $this->input->post('fax', true),
                    'cre_site' => removeCE_Upper($this->input->post('site', true)),
                    'cre_email' => removeCE_Upper($this->input->post('email', true)),
                    'cre_logo' => $logotipo,
                    'cre_info' => removeCE_Upper($this->input->post('info', true)),
                    'cre_cont1_cargo' => removeCE_Upper($this->input->post('cont1_cargo', true)),
                    'cre_cont1_nome' => removeCE_Upper($this->input->post('cont1_nome', true)),
                    'cre_cont1_email' => removeCE_Upper($this->input->post('cont1_email', true)),
                    'cre_cont1_fone' => $this->input->post('cont1_fone', true),
                    'cre_cont2_cargo' => removeCE_Upper($this->input->post('cont2_cargo', true)),
                    'cre_cont2_nome' => removeCE_Upper($this->input->post('cont2_nome', true)),
                    'cre_cont2_email' => removeCE_Upper($this->input->post('cont2_email', true)),
                    'cre_cont2_fone' => $this->input->post('cont2_fone', true),
                    'cre_banco' => $this->input->post('banco', true),
                    'cre_agencia' => $this->input->post('agencia', true),
                    'cre_conta' => $this->input->post('conta', true),
                    'cre_sacado' => removeCE_Upper($this->input->post('sacado', true)),
                    'cre_honorario' => $this->input->post('honorario', true),
                    'cre_juros' => $this->input->post('juros', true),
                    'cre_prazo' => $this->input->post('prazo', true),
                    'cre_repasses' => $repasses,
                    'cre_usuario' => strtolower($this->input->post('usuario', true)),
                    'cre_senha' => $senha
                );
            }
            //SE A SENHA FOR VAZIA PERSISTE OS DADOS NO BANCO E N�O ALTERA A SENHA
            else {
                $dados = array(
                    'usuarios_responsavel_cod' => $this->input->post('responsavel', true),
                    'cre_pessoa' => $this->input->post('pessoa', true),
                    'cre_sexo' => $this->input->post('sexo', true),
                    'cre_estado_civil' => $this->input->post('estado_civil', true),
                    'cre_razao_social' => removeCE_Upper($this->input->post('razao_social', true)),
                    'cre_nome_fantasia' => removeCE_Upper($this->input->post('nome_fantasia', true)),
                    'cre_cpf_cnpj' => $this->input->post('cpf_cnpj', true),
                    'cre_rg_ie' => $this->input->post('rg_ie', true),
                    'cre_endereco' => removeCE_Upper($this->input->post('endereco', true)),
                    'cre_bairro' => removeCE_Upper($this->input->post('bairro', true)),
                    'cre_complemento' => removeCE_Upper($this->input->post('complemento', true)),
                    'cre_cep' => $this->input->post('cep', true),
                    'cre_cidade' => removeCE_Upper($this->input->post('cidade', true)),
                    'cre_uf' => $this->input->post('uf', true),
                    'cre_fone1' => $this->input->post('fone1', true),
                    'cre_fone2' => $this->input->post('fone2', true),
                    'cre_fax' => $this->input->post('fax', true),
                    'cre_site' => removeCE_Upper($this->input->post('site', true)),
                    'cre_email' => removeCE_Upper($this->input->post('email', true)),
                    'cre_logo' => $logotipo,
                    'cre_info' => removeCE_Upper($this->input->post('info', true)),
                    'cre_cont1_cargo' => removeCE_Upper($this->input->post('cont1_cargo', true)),
                    'cre_cont1_nome' => removeCE_Upper($this->input->post('cont1_nome', true)),
                    'cre_cont1_email' => removeCE_Upper($this->input->post('cont1_email', true)),
                    'cre_cont1_fone' => $this->input->post('cont1_fone', true),
                    'cre_cont2_cargo' => removeCE_Upper($this->input->post('cont2_cargo', true)),
                    'cre_cont2_nome' => removeCE_Upper($this->input->post('cont2_nome', true)),
                    'cre_cont2_email' => removeCE_Upper($this->input->post('cont2_email', true)),
                    'cre_cont2_fone' => $this->input->post('cont2_fone', true),
                    'cre_banco' => $this->input->post('banco', true),
                    'cre_agencia' => $this->input->post('agencia', true),
                    'cre_conta' => $this->input->post('conta', true),
                    'cre_sacado' => removeCE_Upper($this->input->post('sacado', true)),
                    'cre_honorario' => $this->input->post('honorario', true),
                    'cre_juros' => $this->input->post('juros', true),
                    'cre_prazo' => $this->input->post('prazo', true),
                    'cre_repasses' => $repasses,
                    'cre_usuario' => strtolower($this->input->post('usuario', true))
                );
            }
        }
        //PREPARA A CONDI��O DO UPDATE
        $this->db->where('cre_cod', $cod);

        //SE ATUALIZAR OS DADOS COM SUCESSO
        if ($this->db->update('credores', $dados)) {
            //SETAR A MENSAGEM DE QUE FOI ATUALIZADO COM SUCESSO
            $this->inicore->setMensagem('notice', 'Credor ' . $this->input->post('nome_fantasia', true) . ' atualizado com sucesso', true);
            auditar('a', $this->session->userdata('usucod'), 'credores', 'Credor codigo: ' . $cod);
        }
        //SE N�O OBTIVER SUCESSO NO UPDATE
        else {
            ////SETAR A MENSAGEM DE ERRO
            $this->inicore->setMensagem('error', 'Erro ao atualizar o usu�rio, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
        }

        //REDIRECIONA PARA A VIEW "credor_listar"
        redirect(base_url() . 'pesquisar/credores');
        //redirect(base_url() . 'credor/listar');
    }

    function _cadRel() {
    	
        //C�DIGO DO CREDOR ENVIADO POR POST
        $cod = $this->input->post('cod', true);
		$rels  = $this->input->post('cre_relatorio', true);
		$relatorios = ($rels) ? implode(',', $rels) : '';
    	
        //SE ATUALIZAR OS DADOS COM SUCESSO
        if ($this->db->update('credores', array('cre_relatorios'=>$relatorios), array('cre_cod'=>$cod))) {
            //SETAR A MENSAGEM DE QUE FOI ATUALIZADO COM SUCESSO
            $this->inicore->setMensagem('notice', 'Acesso aos relat�rios do credor ' . $this->input->post('nome_fantasia', true) . ' foram salvos com sucesso', true);
            auditar('a', $this->session->userdata('usucod'), 'credores', 'Credor codigo: ' . $cod);
        }
        //SE N�O OBTIVER SUCESSO NO UPDATE
        else {
            ////SETAR A MENSAGEM DE ERRO
            $this->inicore->setMensagem('error', 'Erro ao atualizar o acesso aos relat�rios do credor, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
        }
    	
        //REDIRECIONA PARA A VIEW "inad_editar"
        redirect(base_url() . 'credor/editar/cod:' . $cod);
    }

    function _cadRep() {
        if
        (
        /*         * *** DADOS OBRIGAT�RIOS ***** */
                ($this->input->post('nome', true) != null)
        ) {
            //MONTA UMA ARRAY COM OS DADOS DO PARENTE

            $query = '';

            $query .= '\'' . $this->session->userdata('usucod') . '\',';
            $query .= '\'' . $this->input->post('cod', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('nome', true)) . '\',';
            $query .= '\'' . $this->input->post('cpf', true) . '\',';
            $query .= '\'' . $this->input->post('estado_civil', true) . '\',';
            $query .= '\'' . $this->input->post('rg', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('endereco_rep', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('bairro_rep', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('complemento_rep', true)) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('cidade_rep', true)) . '\',';
            $query .= '\'' . $this->input->post('cep_rep', true) . '\',';
            $query .= '\'' . removeCE_Upper($this->input->post('estado_rep', true)) . '\',';
            $query .= '\'' . $this->input->post('fone_rep', true) . '\',';
            $query .= '\'' . $this->input->post('cel_rep', true) . '\',';
            $query .= '\'' . $this->input->post('email_rep', true) . '\'';

            $resultado = $this->db->query("CALL SP_CADASTRA_REP_LEGAL(" . $query . ");");

            //EXECUTA UM INSERT NO BANCO DE DADOS
            if (utf8_decode($resultado->row()->msg) == 'CADASTRADO') {
                //SETAR A MENSAGEM DE QUE FOI ATUALIZADO COM SUCESSO
                $this->inicore->setMensagem('notice', 'Registro adicionado com sucesso', true);
            }
            //SE N�O OBTIVER SUCESSO NO UPDATE
            else {
                ////SETAR A MENSAGEM DE ERRO
                $this->inicore->setMensagem('error', 'Erro ao cadastrar o representante, tente novamente mais tarde; Se o erro persistir entre em contato com a administrado do sistema', true);
            }
        } else {
            $this->inicore->setMensagem('error', 'Dados Incorretos', true);
        }
        //REDIRECIONA PARA A VIEW "inad_editar"
        redirect(base_url() . 'credor/editar/cod:' . $this->input->post('cod', true));
    }

    /* FUN��O AJAX PARA PESQUISAR POR NOME OU CPF_CNPJ
     * SOLICITA��O EFETUADA PELA VIEW cad_divida.php
     * Esta fun��o recebe os dados por post (cod)
     * $cod -------- c�digo do credor
     */

    function _listar_repasses() {
        //C�DIGO DO CREDOR RECEBIDO POR POST DA FUN��O AJAX
        //$cod = $this->input->post('cod',true);
        $cod = $this->input->post('cod', true);

        //RECEBENDO TODOS OS REPASSES CADASTRADOS NO BANCO
        $repasses_banco = $this->md->getRepasses();

        $aux = $this->md->getRepassesCredor($cod);

        $repassescredor = explode(',', $aux->cre_repasses);

//            print_r($repasses_banco);
//            print_r($repassescredor);
//            die('REPASSES DO CREDOR: '.$repasses_credor->cre_repasses);
        //POPULANDO O ARRAY $dados COM OS DADOS RECEBIDOS NA VARI�VEL $retorno
        foreach ($repasses_banco as $rep_banco) {
            if (array_search($rep_banco->rep_cod, $repassescredor, TRUE)) {
                $dados[] = array(
                    'repasse_cod' => rawurlencode(utf8_decode($rep_banco->rep_cod)),
                    'repasse_nome' => rawurlencode(utf8_decode($rep_banco->rep_nome))
                );
            }
        }
        //MATA O FLUXO RETORNANDO UM OBJETO DO TIPO JSON de nome "resultado" CONTENDO O ARRAY $dados
        die(
                json_encode(array(
                    "repasses" => $dados
                ))
        );
    }

}
