$(document).ready( function()
{
   EfeitoRollOver.rollover.init();
});

EfeitoRollOver = {};

EfeitoRollOver.rollover =
{
   init: function()
   {
      this.preload();
      
      $(".ro").hover(
         function () {
             $(this).attr( 'src', EfeitoRollOver.rollover.newimage($(this).attr('src')) );
             
         },
         function () { $(this).attr( 'src', EfeitoRollOver.rollover.oldimage($(this).attr('src')) ); }
      );
   },

   preload: function()
   {
      $(window).bind('load', function() {
         $('.ro').each( function( key, elm ) { $('<img>').attr( 'src', EfeitoRollOver.rollover.newimage( $(this).attr('src') ) ); });
      });
   },
   
   newimage: function( src )
   {
      var string = src.lastIndexOf(".");
      var ext = src.substr(string,(string+'3'));
      var img = src.substr(0,string);
      var fullimg = img+'_o'+ext;

      return fullimg;

   },
   
   oldimage: function( src )
   {
      return src.replace(/_o/, '');
   }
};