<?php

class tarefa_model extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getUsuarios($where='',$order='') {
        /* pega todos os usu�rio do sistema para colocar no combo box RESPONS�VEL do form de nova tarefa */
        if (!empty($where)) $where=' WHERE '.$where.' ';
		if (!empty($order)) $order=' ORDER BY '.$order.' ';
		$sql = 'SELECT usu_cod, usu_usuario_sis FROM usuarios'.$where.$order;
        $query = $this->db->query($sql);
        return $query->result();
    }

    function getMinhasTarefasDeHoje($codUsuarioSessao, $dataDeHoje) {//aba hoje da view ag_listar
        $query = $this->db->query("
                SELECT tar_cod, tar_titulo, tar_descricao, tar_criacao, tar_criador, tar_agendada, U.usu_usuario_sis
                  FROM tarefas T
                    INNER JOIN usuarios U
                      ON (U.usu_cod=T.usuarios_usu_cod)
                WHERE T.usuarios_usu_cod='$codUsuarioSessao'
                  AND tar_agendada='$dataDeHoje'
                    'ORDER BY T.tar_criacao ASC'");
        return $query->result();
    }

    function getMinhasTarefasDaSemana($codUsuarioSessao, $primeiroDiaDaSemana, $ultimoDiaDaSemana) {//aba semana da view ag_listar
        $query = $this->db->query("
                SELECT tar_cod, tar_titulo, tar_descricao, tar_criacao, tar_criador, tar_agendada, U.usu_usuario_sis
                  FROM tarefas T
                    INNER JOIN usuarios U
                      ON (U.usu_cod=T.usuarios_usu_cod)
                WHERE T.usuarios_usu_cod='$codUsuarioSessao'
                  AND T.tar_agendada>='$primeiroDiaDaSemana'
                  AND T.tar_agendada<='$ultimoDiaDaSemana'
               'ORDER BY T.tar_agendada ASC'");
        return $query->result();
    }

    function getMinhasTarefasDoMes($codUsuarioSessao, $mesAtual, $anoAtual) {//aba m�s da view ag_listar
        $query = $this->db->query("
                SELECT tar_cod, tar_titulo, tar_descricao, tar_criacao, tar_criador, tar_agendada, U.usu_usuario_sis
                  FROM tarefas T
                    INNER JOIN usuarios U
                       ON (U.usu_cod=T.usuarios_usu_cod)
                WHERE T.usuarios_usu_cod='$codUsuarioSessao'
                AND T.tar_agendada>='$anoAtual-$mesAtual-1'
                AND T.tar_agendada<='$anoAtual-$mesAtual-31'
                'ORDER BY T.tar_agendada ASC'");
        return $query->result();
    }

    function getTodasMinhasTarefas($codUsuarioSessao) {//aba todas da view ag_listar
        $query = $this->db->query("
                SELECT tar_cod, tar_titulo, tar_descricao, tar_criacao, tar_criador, tar_agendada,  U.usu_usuario_sis
                  FROM tarefas T
                    INNER JOIN usuarios U
                      ON (U.usu_cod=T.usuarios_usu_cod)
                WHERE T.usuarios_usu_cod='$codUsuarioSessao'
                ORDER BY T.tar_agendada ASC");
        return $query->result();
    }

    function getTarefasTodosUsuarios() {//aba geral da view ag_listar
        $query = $this->db->query('
            SELECT tar_cod, tar_titulo, tar_descricao, tar_criacao, tar_criador, tar_agendada, U.usu_usuario_sis
              FROM tarefas T
                INNER JOIN usuarios U
                  ON(U.usu_cod=T.usuarios_usu_cod)
            ORDER BY T.tar_agendada ASC');
        return( $query->result());
    }

    function setNovaTarefa($dados) {
        /* M�todo que recebe os dados e grava uma nova tarefa */
        $this->db->insert('tarefas', $dados);
    }

    function excluirTarefa($codTarefa) {
        /* Exclui a tarefa usando seu c�digo */
        $this->db->where("tar_cod", $codTarefa);
        $this->db->delete("tarefas");
        return $this->db->affected_rows();
    }

}

?>