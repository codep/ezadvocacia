<?php

class Pesquisa_model extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function pesquisar($filtro) {
        $query = $this->db->query("SELECT * FROM inadimplentes I $filtro AND I.ina_ativo = 1");
        return $query->result();
    }

    function pesquisarCre($filtro) {
        $query = $this->db->query("SELECT * FROM credores C $filtro AND C.cre_ativo = 1");
        return $query->result();
    }

    function pesAcordDiv($filtro) {
        
        /* pega os acordos */
        $queryAcordos = $this->db->query(
                       "(SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome,ac.aco_valor_atualizado AS valor, co.`cob_setor` AS setor, 'red' AS cor
                        FROM cobrancas co
                        INNER JOIN credores cre
                        ON (cre.cre_cod = co.credor_cre_cod)
                        INNER JOIN inadimplentes i
                        ON (i.ina_cod = co.inadimplentes_ina_cod)
                        INNER JOIN acordos ac
                        ON (ac.cobranca_cob_cod = co.cob_cod)
                        INNER JOIN ros ro
                        ON ro.`cobranca_cob_cod` = co.`cob_cod`
                        $filtro  
                        AND co.cob_remocao = '0'
                        AND ro.`operacoes_ope_cod` = 82
                        AND co.`cob_setor` = 'ADM'
                        ORDER BY setor, cre.`cre_nome_fantasia`, i.`ina_nome`) 
                UNION
                        (SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome, d.div_total AS valor, co.`cob_setor` AS setor, 'red' AS cor
                          FROM cobrancas co
                            INNER JOIN credores cre
                              ON (cre.cre_cod = co.credor_cre_cod)
                            INNER JOIN inadimplentes i
                              ON (i.ina_cod = co.inadimplentes_ina_cod)
                            INNER JOIN dividas d
                              ON (d.cobranca_cob_cod = co.cob_cod)
                            INNER JOIN ros ro
                                ON ro.`cobranca_cob_cod` = co.`cob_cod`
                        $filtro
                        AND co.cob_remocao = '0' 
                        AND co.cob_setor = 'ADM'
                        AND ro.`operacoes_ope_cod` = 82
                        ORDER BY cre.`cre_nome_fantasia`, i.`ina_nome`)
                        
                    UNION
                        (SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome,ac.aco_valor_atualizado AS valor, co.`cob_setor` AS setor, 'blue' AS cor
                        FROM cobrancas co
                        INNER JOIN credores cre
                        ON (cre.cre_cod = co.credor_cre_cod)
                        INNER JOIN inadimplentes i
                        ON (i.ina_cod = co.inadimplentes_ina_cod)
                        INNER JOIN acordos ac
                        ON (ac.cobranca_cob_cod = co.cob_cod)
                        INNER JOIN ros ro
                        ON ro.`cobranca_cob_cod` = co.`cob_cod`
                        $filtro  
                        AND co.cob_remocao = '0'
                        AND ro.`operacoes_ope_cod` = 83
                        AND co.`cob_setor` = 'ADM'
                        ORDER BY setor, cre.`cre_nome_fantasia`, i.`ina_nome`)
                    UNION
                        (SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome, d.div_total AS valor, co.`cob_setor` AS setor, 'blue' AS cor
                          FROM cobrancas co
                            INNER JOIN credores cre
                              ON (cre.cre_cod = co.credor_cre_cod)
                            INNER JOIN inadimplentes i
                              ON (i.ina_cod = co.inadimplentes_ina_cod)
                            INNER JOIN dividas d
                              ON (d.cobranca_cob_cod = co.cob_cod)
                            INNER JOIN ros ro
                                ON ro.`cobranca_cob_cod` = co.`cob_cod`
                        $filtro
                        AND co.cob_remocao = '0' 
                        AND co.cob_setor = 'ADM'
                        AND ro.`operacoes_ope_cod` = 83
                        ORDER BY cre.`cre_nome_fantasia`, i.`ina_nome`)
                    UNION
                        (SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome,ac.aco_valor_atualizado AS valor, co.`cob_setor` AS setor, 'black' AS cor
                        FROM cobrancas co
                        INNER JOIN credores cre
                        ON (cre.cre_cod = co.credor_cre_cod)
                        INNER JOIN inadimplentes i
                        ON (i.ina_cod = co.inadimplentes_ina_cod)
                        INNER JOIN acordos ac
                        ON (ac.cobranca_cob_cod = co.cob_cod)
                        $filtro  
                        AND co.cob_remocao = '0'
                        AND co.cob_setor = 'ADM'
                        ORDER BY setor, cre.`cre_nome_fantasia`, i.`ina_nome`
                        LIMIT 999999)
                        
                    UNION
                        (SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome, d.div_total AS valor, co.`cob_setor` AS setor, 'green' AS cor
                          FROM cobrancas co
                            INNER JOIN credores cre
                              ON (cre.cre_cod = co.credor_cre_cod)
                            INNER JOIN inadimplentes i
                              ON (i.ina_cod = co.inadimplentes_ina_cod)
                            INNER JOIN dividas d
                              ON (d.cobranca_cob_cod = co.cob_cod)
                        $filtro
                        AND co.cob_remocao = '0' 
                        AND co.cob_setor = 'ADM'
                        ORDER BY cre.`cre_nome_fantasia`, i.`ina_nome`)
                    UNION
                        (SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome,ac.aco_valor_atualizado AS valor, co.`cob_setor` AS setor, 'black' AS cor
                        FROM cobrancas co
                        INNER JOIN credores cre
                        ON (cre.cre_cod = co.credor_cre_cod)
                        INNER JOIN inadimplentes i
                        ON (i.ina_cod = co.inadimplentes_ina_cod)
                        INNER JOIN acordos ac
                        ON (ac.cobranca_cob_cod = co.cob_cod)
                        $filtro  
                        AND co.cob_remocao = '0'
                        AND co.cob_setor = 'JUD'
                        ORDER BY setor, cre.`cre_nome_fantasia`, i.`ina_nome`
                        LIMIT 999999)
                ");
//        $acordos = $queryAcordos->result();


        /* pega as d�vidas */
//        $queryDividas = $this->db->query(
//                        "SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome, d.div_total AS valor
//                          FROM cobrancas co
//                            INNER JOIN credores cre
//                              ON (cre.cre_cod = co.credor_cre_cod)
//                            INNER JOIN inadimplentes i
//                              ON (i.ina_cod = co.inadimplentes_ina_cod)
//                            INNER JOIN dividas d
//                              ON (d.cobranca_cob_cod = co.cob_cod)
//                        $filtro AND co.cob_remocao = '0' 
//                        ORDER BY cre.`cre_nome_fantasia`, i.`ina_nome`");
//        $dividas = $queryDividas->result();

        return $queryAcordos->result();
    }
//    function pesAcordDiv($filtro) {
//        /* pega os acordos */
//        $queryAcordos = $this->db->query(
//                       "(SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome,ac.aco_valor_atualizado AS valor, co.`cob_setor` AS setor, 'red' AS cor
//                        FROM cobrancas co
//                        INNER JOIN credores cre
//                        ON (cre.cre_cod = co.credor_cre_cod)
//                        INNER JOIN inadimplentes i
//                        ON (i.ina_cod = co.inadimplentes_ina_cod)
//                        INNER JOIN acordos ac
//                        ON (ac.cobranca_cob_cod = co.cob_cod)
//                        INNER JOIN ros ro
//                        ON ro.`cobranca_cob_cod` = co.`cob_cod`
//                        $filtro  
//                        AND co.cob_remocao = '0'
//                        AND ro.`operacoes_ope_cod` = 82
//                        AND co.`cob_setor` = 'ADM'
//                        ORDER BY setor, cre.`cre_nome_fantasia`, i.`ina_nome`) 
//                        UNION
//                        (SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome,ac.aco_valor_atualizado AS valor, co.`cob_setor` AS setor, 'blue' AS cor
//                        FROM cobrancas co
//                        INNER JOIN credores cre
//                        ON (cre.cre_cod = co.credor_cre_cod)
//                        INNER JOIN inadimplentes i
//                        ON (i.ina_cod = co.inadimplentes_ina_cod)
//                        INNER JOIN acordos ac
//                        ON (ac.cobranca_cob_cod = co.cob_cod)
//                        INNER JOIN ros ro
//                        ON ro.`cobranca_cob_cod` = co.`cob_cod`
//                        $filtro  
//                        AND co.cob_remocao = '0'
//                        AND ro.`operacoes_ope_cod` = 83
//                        AND co.`cob_setor` = 'ADM'
//                        ORDER BY setor, cre.`cre_nome_fantasia`, i.`ina_nome`)
//                        UNION
//                        (SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome,ac.aco_valor_atualizado AS valor, co.`cob_setor` AS setor, 'black' AS cor
//                        FROM cobrancas co
//                        INNER JOIN credores cre
//                        ON (cre.cre_cod = co.credor_cre_cod)
//                        INNER JOIN inadimplentes i
//                        ON (i.ina_cod = co.inadimplentes_ina_cod)
//                        INNER JOIN acordos ac
//                        ON (ac.cobranca_cob_cod = co.cob_cod)
//                        $filtro  
//                        AND co.cob_remocao = '0'
//                        ORDER BY setor, cre.`cre_nome_fantasia`, i.`ina_nome`
//                        LIMIT 999999)");
//        $acordos = $queryAcordos->result();
//
//
//        /* pega as d�vidas */
//        $queryDividas = $this->db->query(
//                        "SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome, d.div_total AS valor
//                          FROM cobrancas co
//                            INNER JOIN credores cre
//                              ON (cre.cre_cod = co.credor_cre_cod)
//                            INNER JOIN inadimplentes i
//                              ON (i.ina_cod = co.inadimplentes_ina_cod)
//                            INNER JOIN dividas d
//                              ON (d.cobranca_cob_cod = co.cob_cod)
//                        $filtro AND co.cob_remocao = '0' 
//                        ORDER BY cre.`cre_nome_fantasia`, i.`ina_nome`");
//        $dividas = $queryDividas->result();
//
//        return array($acordos, $dividas); //retorna um array com os acordos e com as d�vidas
//    }

    function buscInaFone($valorDigitado) {
        $query = $this->db->query("SELECT I.ina_cod,I.ina_nome,I.ina_endereco,I.ina_cidade FROM inadimplentes I LEFT JOIN parentes P ON (P.inadimplentes_ina_cod=I.ina_cod)
                                    WHERE I.ina_fonecom = '$valorDigitado' 
                                    OR I.ina_fonerec = '$valorDigitado' 
                                    OR I.ina_foneres = '$valorDigitado'
                                    OR I.ina_cel1 = '$valorDigitado'
                                    OR I.ina_cel2 = '$valorDigitado'
                                    OR I.ina_cel3 = '$valorDigitado'
                                    OR I.ina_conj_fone = '$valorDigitado'
                                    OR I.ina_mae_fone = '$valorDigitado'
                                    OR I.ina_pai_fone = '$valorDigitado'
                                    OR P.par_fone1 = '$valorDigitado'
                                    OR P.par_fone2 = '$valorDigitado'
                                    OR P.par_fone3 = '$valorDigitado'
                                    OR P.par_fone4 = '$valorDigitado'
                                    GROUP BY I.ina_cod ORDER BY I.ina_nome ");
        return $query->result();
    }

    /* Busca inadimplente pelo telefone na tabela parente */

    function buscInaPar($valorDigitado) {
        /*
         * Alteracao feita dia 08-11-2011, se n�o pedirem par alterar pode apagar essa parte comentada
         * O sistema foi feito originalmente assim, n�o aparecia o nome do parente.
         * $query = $this->db->query("aSELECT I.ina_cod,I.ina_nome,I.ina_endereco,I.ina_cidade,I.ina_foneres,I.ina_cel1,P.par_nome,P.par_parentesco FROM inadimplentes I LEFT JOIN parentes P ON (P.inadimplentes_ina_cod=I.ina_cod)
                                    WHERE I.ina_conj_nome LIKE '%$valorDigitado%' 
                                    OR I.ina_mae_nome LIKE '%$valorDigitado%'
                                    OR I.ina_pai_nome LIKE '%$valorDigitado%'
                                    OR P.par_nome LIKE '%$valorDigitado%'
                                    ORDER BY I.ina_nome ASC;");*/
        $query = $this->db->query
                ("
                    SELECT
                      I.ina_cod, I.ina_nome, I.ina_endereco, I.ina_cidade, I.ina_foneres, I.ina_cel1, P.par_parentesco,
                      IF(I.ina_conj_nome LIKE '%$valorDigitado%', I.ina_conj_nome, '') AS ina_conj_nome,
                      IF(I.ina_mae_nome LIKE '%$valorDigitado%', I.ina_mae_nome, '') AS ina_mae_nome,
                      IF(I.ina_pai_nome LIKE '%$valorDigitado%', I.ina_pai_nome, '') AS ina_pai_nome,
                      IF(P.par_nome LIKE '%$valorDigitado%', P.par_nome, '') AS par_nome
                    FROM inadimplentes I
                      LEFT JOIN parentes P
                        ON (P.inadimplentes_ina_cod = I.ina_cod)
                    WHERE I.ina_conj_nome LIKE '%$valorDigitado%'
                     OR I.ina_mae_nome LIKE '%$valorDigitado%'
                     OR I.ina_pai_nome LIKE '%$valorDigitado%'
                     OR P.par_nome LIKE '%$valorDigitado%'
                    ORDER BY I.ina_nome ASC
                ");
        return $query->result();
    }

}

?>