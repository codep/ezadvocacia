<?php

class Importmodel extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getCreCod($docCredor) {
        $query = $this->db->query("SELECT C.cre_cod FROM credores C WHERE C.cre_cpf_cnpj = '$docCredor' AND C.cre_ativo=1");
        return $query->row();
    }

    function getInaCod($docInadimplente) {
        $query = $this->db->query("SELECT I.ina_cod FROM inadimplentes I WHERE I.ina_cpf_cnpj = '$docInadimplente'");
        return $query->row();
    }

    function cadastraDividaTemporaria($tipoDoc, $banco, $agencia, $alinea, $emissao, $qtdeParcelas, $vencInicial, $informacoes, $valorTotal, $creCod) {

        $sync = microtime();

        $data = array(
            'div_documento' => $tipoDoc,
            'div_banco' => $banco,
            'div_agencia' => $agencia,
            'div_alinea' => $alinea,
            'div_emissao' => $emissao,
            'div_cadastro' => date('Y-m-d'),
            'div_qtd_parc' => $qtdeParcelas,
            'div_venc_inicial' => $vencInicial,
            'div_info' => utf8_encode($informacoes),
            'div_total' => $valorTotal,
            'div_cre_cod' => $creCod,
            'div_sync' => $sync
        );

        $this->db->insert('import_dividas', $data);

        $query = $this->db->query("SELECT ID.div_cod FROM import_dividas ID WHERE ID.div_sync = '$sync'");
        return $query->row()->div_cod;
    }

    function cadastraImportacao($inaCod, $duplicidade, $imp_erro, $divida_cod) {

        $data = array(
            'imp_inad_cod' => $inaCod,
            'imp_duplicidade' => $duplicidade,
            'imp_erro' => $imp_erro,
            'imp_divida_imp_cod' => $divida_cod
        );

        $this->db->insert('importacao', $data);
    }

    function cadastraParcela($divida_cod, $parnum, $parvenc, $parvalor) {
        $data = array(
            'impp_div_cod' => $divida_cod,
            'impp_num' => $parnum,
            'impp_venc' => $parvenc,
            'imp_valor' => $parvalor
        );

        $this->db->insert('import_dividas_parcelas', $data);
    }

    function cadInadTemp($tipoPessoa, $sexoPessoa, $estadoCivil, $inaNome, $docInadimplente, $inaRgIe, $inaEnd, $inaBairro, $inaCompl, $inaCep, $inaCidade, $inaEstado, $inaFoneRes, $inaFoneRec, $inaFoneCom, $inaCel1, $inaCel2, $inaCel3, $inaInfo, $inaConjNome, $inaConjFone, $inaConjEnd, $inaConjCidade, $inaConjUF, $inaPaiNome, $inaPaiFone, $inaPaiEnd, $inaPaiCidade, $inaPaiUF, $inaMaeNome, $inaMaeFone, $inaMaeEnd, $inaMaeCidade, $inaMaeUF, $inaCod, $inaDup) {

        $data = array(
            'ina_pessoa' => $tipoPessoa,
            'ina_sexo' => $sexoPessoa,
            'ina_estado_civil' => $estadoCivil,
            'ina_nome' => removeCE_Upper($inaNome),
            'ina_cpf_cnpj' => $docInadimplente,
            'ina_rg_ie' => $inaRgIe,
            'ina_endereco' => removeCE_Upper($inaEnd),
            'ina_bairro' => removeCE_Upper($inaBairro),
            'ina_complemento' => removeCE_Upper($inaCompl),
            'ina_cep' => str_replace(array('.', '-'), '', $inaCep),
            'ina_cidade' => removeCE_Upper($inaCidade),
            'ina_uf' => removeCE_Upper($inaEstado),
            'ina_foneres' => $inaFoneRes,
            'ina_fonerec' => $inaFoneRec,
            'ina_fonecom' => $inaFoneCom,
            'ina_cel1' => $inaCel1,
            'ina_cel2' => $inaCel2,
            'ina_cel3' => $inaCel3,
            'ina_info' => removeCE_Upper($inaInfo),
            'ina_conj_nome' => removeCE_Upper($inaConjNome),
            'ina_conj_fone' => $inaConjFone,
            'ina_conj_endereco' => removeCE_Upper($inaConjEnd),
            'ina_conj_cidade' => removeCE_Upper($inaConjCidade),
            'ina_conj_uf' => removeCE_Upper($inaConjUF),
            'ina_pai_nome' => removeCE_Upper($inaPaiNome),
            'ina_pai_fone' => $inaPaiFone,
            'ina_pai_endereco' => removeCE_Upper($inaPaiEnd),
            'ina_pai_cidade' => removeCE_Upper($inaPaiCidade),
            'ina_pai_uf' => removeCE_Upper($inaPaiUF),
            'ina_mae_nome' => removeCE_Upper($inaMaeNome),
            'ina_mae_fone' => $inaMaeFone,
            'ina_mae_endereco' => removeCE_Upper($inaMaeEnd),
            'ina_mae_cidade' => removeCE_Upper($inaMaeCidade),
            'ina_mae_uf' => removeCE_Upper($inaMaeUF),
            'ina_duplicado' => $inaDup,
            'ina_inad_original_cod' => $inaCod
        );

        $this->db->insert('import_inadimplentes', $data);
    }

    function cadInad($tipoPessoa, $sexoPessoa, $estadoCivil, $inaNome, $docInadimplente, $inaRgIe, $inaEnd, $inaBairro, $inaCompl, $inaCep, $inaCidade, $inaEstado, $inaFoneRes, $inaFoneRec, $inaFoneCom, $inaCel1, $inaCel2, $inaCel3, $inaInfo, $inaConjNome, $inaConjFone, $inaConjEnd, $inaConjCidade, $inaConjUF, $inaPaiNome, $inaPaiFone, $inaPaiEnd, $inaPaiCidade, $inaPaiUF, $inaMaeNome, $inaMaeFone, $inaMaeEnd, $inaMaeCidade, $inaMaeUF) {

        $data = array(
            'ina_pessoa' => $tipoPessoa,
            'ina_sexo' => $sexoPessoa,
            'ina_estado_civil' => $estadoCivil,
            'ina_nome' => removeCE_Upper($inaNome),
            'ina_cpf_cnpj' => $docInadimplente,
            'ina_rg_ie' => $inaRgIe,
            'ina_endereco' => removeCE_Upper($inaEnd),
            'ina_bairro' => removeCE_Upper($inaBairro),
            'ina_complemento' => removeCE_Upper($inaCompl),
            'ina_cep' => str_replace(array('.', '-'), '', $inaCep),
            'ina_cidade' => removeCE_Upper($inaCidade),
            'ina_uf' => removeCE_Upper($inaEstado),
            'ina_foneres' => $inaFoneRes,
            'ina_fonerec' => $inaFoneRec,
            'ina_fonecom' => $inaFoneCom,
            'ina_cel1' => $inaCel1,
            'ina_cel2' => $inaCel2,
            'ina_cel3' => $inaCel3,
            'ina_info' => removeCE_Upper($inaInfo),
            'ina_conj_nome' => removeCE_Upper($inaConjNome),
            'ina_conj_fone' => $inaConjFone,
            'ina_conj_endereco' => removeCE_Upper($inaConjEnd),
            'ina_conj_cidade' => removeCE_Upper($inaConjCidade),
            'ina_conj_uf' => removeCE_Upper($inaConjUF),
            'ina_pai_nome' => removeCE_Upper($inaPaiNome),
            'ina_pai_fone' => $inaPaiFone,
            'ina_pai_endereco' => removeCE_Upper($inaPaiEnd),
            'ina_pai_cidade' => removeCE_Upper($inaPaiCidade),
            'ina_pai_uf' => removeCE_Upper($inaPaiUF),
            'ina_mae_nome' => removeCE_Upper($inaMaeNome),
            'ina_mae_fone' => $inaMaeFone,
            'ina_mae_endereco' => removeCE_Upper($inaMaeEnd),
            'ina_mae_cidade' => removeCE_Upper($inaMaeCidade),
            'ina_mae_uf' => removeCE_Upper($inaMaeUF),
			'ina_ativo' => 1
        );

        $this->db->insert('inadimplentes', $data);
    }

    function getDividasDados($cods) {

        if ($cods != '') {
            $query = $this->db->query("SELECT INA.ina_nome,ID.*,I.* FROM import_dividas ID INNER JOIN importacao I ON (I.imp_divida_imp_cod = ID.div_cod) INNER JOIN inadimplentes INA ON (INA.ina_cod = I.imp_inad_cod) WHERE ID.div_cod IN ($cods)");
            return $query->result();
        }else{
            return false;
        }
        
    }

    function getCredDados($divCod) {
        $query = $this->db->query("SELECT
                                  C.cre_nome_fantasia,
                                  C.cre_cpf_cnpj,
                                  C.cre_cod
                                FROM import_dividas ID
                                  INNER JOIN credores C
                                    ON (C.cre_cod = ID.div_cre_cod)
                            WHERE ID.div_cod = $divCod");
        return $query->row();
    }

    function getRepassesCredor($cre_cod) {
        $query = $this->db->query("SELECT C.cre_repasses FROM credores C WHERE C.cre_cod = $cre_cod");
        return $query->row();
    }
    
    function getRepasseInfo($repasse_cod)
    {
        $query = $this->db->query("SELECT rep_cod, rep_nome FROM repasses WHERE rep_cod = $repasse_cod");
        return $query->row();
    }
    
    function getCredoresImportPendentes() {
        $query = $this->db->query("SELECT DISTINCT ID.div_cre_cod,C.cre_nome_fantasia,C.cre_cpf_cnpj FROM importacao I INNER JOIN import_dividas ID ON (ID.div_cod = I.imp_divida_imp_cod) INNER JOIN credores C ON (C.cre_cod = ID.div_cre_cod) WHERE I.imp_status = '0'");
        return $query->result();
    }

    function getImportacoesPendentes($crecod) {
        $query = $this->db->query("SELECT *
                                    FROM importacao I
                                      INNER JOIN import_dividas ID
                                        ON (ID.div_cod = I.imp_divida_imp_cod)
                                        INNER JOIN inadimplentes INA
                                        ON (INA.ina_cod = I.imp_inad_cod)
                                    WHERE I.imp_status = '0'
                                        AND ID.div_cre_cod = '$crecod'");
        return $query->result();
    }

    function getDadosDividaEfetivar($cod) {
        $query = $this->db->query("SELECT * FROM import_dividas ID INNER JOIN importacao I ON (ID.div_cod = I.imp_divida_imp_cod) INNER JOIN credores C ON (C.cre_cod = ID.div_cre_cod) WHERE ID.div_cod = $cod");
        return $query->row();
    }

    function getDadosParcelasEfetivar($cod) {
        $query = $this->db->query("SELECT * FROM import_dividas_parcelas IDP INNER JOIN import_dividas ID ON (IDP.impp_div_cod = ID.div_cod) WHERE ID.div_cod = $cod");
        return $query->result();
    }

    function getCobCad($sync) {
        $query = $this->db->query("SELECT C.cob_cod FROM cobrancas C WHERE C.cob_sync='$sync'");
        return $query->row();
    }

    function getDivCad($cod) {
        $query = $this->db->query("SELECT D.div_cod FROM dividas D WHERE D.cobranca_cob_cod='$cod'");
        return $query->row();
    }
    
    function removeDiv($div_cod=0) {

    	$this->db->where(array('impp_div_cod' => $div_cod))->delete('import_dividas_parcelas');
    	$this->db->where(array('div_cod' => $div_cod))->delete('import_dividas');
		$this->db->where(array('imp_divida_imp_cod' => $div_cod))->delete('importacao');
        return $this->db->affected_rows();
    	
    }
    
    function removeDivDupCredor($cre_cod=0) {
    			
		$sql = 'SELECT ID.div_cod, I.imp_cod FROM import_dividas ID INNER JOIN importacao I ON I.imp_divida_imp_cod=ID.div_cod WHERE ID.div_cre_cod='.$cre_cod.' AND I.imp_status=0 AND I.imp_duplicidade=1';
        $query = $this->db->query($sql);
        
        $registros = $query->result();
        $delReg = 0;
        
        foreach($registros as $registro) {
        	$this->db->where(array('impp_div_cod' => $registro->div_cod))->delete('import_dividas_parcelas');
        	$this->db->where(array('div_cod' => $registro->div_cod))->delete('import_dividas');
			$this->db->where(array('imp_divida_imp_cod' => $registro->imp_cod))->delete('importacao');
			$delReg = $delReg + intval($this->db->affected_rows());
        }
        
        return $delReg;
    	
    }

}

?>