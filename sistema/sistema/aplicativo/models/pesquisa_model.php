<?php

class Pesquisa_model extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function pesquisar($filtro = '', $join='') {
    	$sql = " SELECT * FROM inadimplentes I ";
		if (!empty($join)) $sql.= $join ;
		if (!empty($filtro)) $filtro = $filtro . ' AND ';
		$sql .= " WHERE $filtro I.ina_ativo = 1"; 
		
        $query = $this->db->query($sql);
        return $query->result();
    }

    function pesquisarCre($filtro) {
        $query = $this->db->query("SELECT * FROM credores C $filtro AND C.cre_ativo = 1");
        return $query->result();
    } 
	
	function pesDivCredor($cre_cod=0, $tipo='', $setor='', $agrupar='') {
		
		if (is_array($setor)) {
			$setor = implode('","', $setor);
			$setor = '"'.$setor.'"';
		} else if (empty($setor)) {
			$setor = '""';
		}
		
		switch($tipo) {
			case 'virgem':
				$sql='SELECT d.div_cod, co.cob_cod, i.ina_nome, i.ina_cod, d.div_total as valor2, co.cob_setor AS setor
                        FROM cobrancas co
                        	INNER JOIN inadimplentes i ON (i.ina_cod = co.inadimplentes_ina_cod)
                        	INNER JOIN dividas d ON (d.cobranca_cob_cod = co.cob_cod)
                        	INNER JOIN credores cre ON (cre.cre_cod = co.credor_cre_cod)
                		 WHERE d.div_virgem=1 AND co.cob_remocao IN (0)
                		 AND NOT EXISTS(SELECT ac.cobranca_cob_cod FROM acordos ac WHERE ac.cobranca_cob_cod = co.cob_cod)';
                		 //AND co.cob_setor IN ('.$setor.')';
			break;
			case 'andamento':
				$sql='SELECT d.div_cod, co.cob_cod, i.ina_nome, i.ina_cod, d.div_total as valor2, co.cob_setor AS setor
					FROM cobrancas co
                        INNER JOIN credores cre ON (cre.cre_cod = co.credor_cre_cod)
                        INNER JOIN inadimplentes i ON (i.ina_cod = co.inadimplentes_ina_cod)
                        INNER JOIN dividas d ON (d.cobranca_cob_cod = co.cob_cod)
                    WHERE d.div_virgem=0
                    AND NOT EXISTS(SELECT ro.operacoes_ope_cod FROM ros ro WHERE ro.cobranca_cob_cod = co.cob_cod AND ro.operacoes_ope_cod IN (82,83))
                    AND co.cob_recem_enviada = 0 
                    AND co.cob_remocao NOT IN (1,2,4)';
					//AND co.cob_setor IN ('.$setor.')';
			break;
			case 'atraso':
				$sql='SELECT co.cob_cod, i.ina_nome, i.ina_cod, aco_total_parcelado as valor, co.cob_setor AS setor
                    FROM acordos ac
	                    INNER JOIN cobrancas co ON (ac.cobranca_cob_cod = co.cob_cod)
	                    INNER JOIN inadimplentes i ON (co.inadimplentes_ina_cod = i.ina_cod)
	                    INNER JOIN credores cr ON (co.credor_cre_cod = cr.cre_cod)
                    WHERE (SELECT PAA.paa_vencimento FROM par_acordos PAA WHERE PAA.acordos_aco_cod = aco_cod AND PAA.paa_situacao = "0" ORDER BY PAA.paa_vencimento ASC LIMIT 1) <= "'.date('Y-m-d').'"
                    AND NOT EXISTS(SELECT ro.operacoes_ope_cod FROM ros ro WHERE ro.cobranca_cob_cod = co.cob_cod AND ro.operacoes_ope_cod IN (82,83))
                    AND co.cob_remocao IN (0)';
                    //AND ac.aco_procedencia IN ('.$setor.') AND co.cob_setor IN ('.$setor.')';
			break;
			case 'acordo':
					$sql='SELECT aco_cod, co.cob_cod, i.ina_cod, i.ina_nome, cr.cre_nome_fantasia, aco_valor_atualizado as valor, co.cob_setor AS setor
                           FROM acordos ac
                             INNER JOIN cobrancas co ON(ac.cobranca_cob_cod = co.cob_cod)
                             INNER JOIN inadimplentes i ON(co.inadimplentes_ina_cod = i.ina_cod)
                             INNER JOIN credores cr ON(co.credor_cre_cod = cr.cre_cod)
						 WHERE co.cob_recem_enviada = 0
						 AND NOT EXISTS(SELECT ro.operacoes_ope_cod FROM ros ro WHERE ro.cobranca_cob_cod = co.cob_cod AND ro.operacoes_ope_cod IN (82,83))
						 AND co.cob_remocao NOT IN (1,2,4)';
                         //WHERE 1=1'; //ac.aco_procedencia IN ('.$setor.') AND co.cob_setor IN ('.$setor.')';
			break;
			case 'estornar':
				$sql='SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome, ac.aco_valor_atualizado AS valor, d.div_total as valor2, co.cob_setor AS setor
                        FROM cobrancas co
                        	INNER JOIN credores cre ON (cre.cre_cod = co.credor_cre_cod)
                        	INNER JOIN inadimplentes i ON (i.ina_cod = co.inadimplentes_ina_cod)
                        	LEFT JOIN acordos ac ON (ac.cobranca_cob_cod = co.cob_cod)
                        	LEFT JOIN dividas d ON (d.cobranca_cob_cod = co.cob_cod)
                        	INNER JOIN ros ro ON ro.`cobranca_cob_cod` = co.`cob_cod`
                        WHERE ro.operacoes_ope_cod = 82 AND co.cob_remocao IN (0,4)';
                        //AND ac.aco_procedencia IN ('.$setor.') AND co.cob_setor IN ('.$setor.')';
			break;
			case 'analisar':
					$sql='SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome,ac.aco_valor_atualizado AS valor, d.div_total as valor2, co.cob_setor AS setor
	                    FROM cobrancas co
	                    	INNER JOIN credores cre ON (cre.cre_cod = co.credor_cre_cod)
	                    	INNER JOIN inadimplentes i ON (i.ina_cod = co.inadimplentes_ina_cod)
	                    	LEFT JOIN acordos ac ON (ac.cobranca_cob_cod = co.cob_cod)
	                        LEFT JOIN dividas d ON (d.cobranca_cob_cod = co.cob_cod)
	                    	INNER JOIN ros ro ON ro.`cobranca_cob_cod` = co.`cob_cod`
						WHERE ro.operacoes_ope_cod = 83 AND co.cob_remocao IN (0,4)';
					//AND ac.aco_procedencia IN ('.$setor.') AND co.cob_setor IN ('.$setor.')';
			break;
		}

		$sql.=' AND co.cob_setor IN ('.$setor.') AND i.ina_ativo = 1 AND co.credor_cre_cod='.$cre_cod;
		
		//N�O PODE VER 2=DOCUMENTO COMPLETO,8=Gerado Border� de Devolu��o
		$sql.=' AND co.fiscal_status NOT IN(2,8) ';
		
		if (empty($agrupar)) $sql.=' GROUP BY i.ina_cod ';
		
		$sql.=' ORDER BY i.ina_nome ';
		
		//echo $sql;
		
		//RETORNA OS REGISTROS
        $q = $this->db->query($sql);
        return $q->result();
	}

	function getMultaFiltro($multa='') {
		if ($multa=='AP') {
			$multa_filtro = ' (A.aco_multa_aplicada>0 AND A.aco_multa_protocolada=1) ';
		} elseif ($multa=='ANP') {
			$multa_filtro = ' (A.aco_multa_aplicada>0 AND A.aco_multa_protocolada=0) ';
		} elseif ($multa=='NANP') {
			$multa_filtro = ' (A.aco_multa_aplicada=0 AND A.aco_multa_protocolada=0) ';
		} else {
			$multa_filtro='';
		}
		return $multa_filtro;
	}
	
	function pesAcoJuridicoVencimentoTodas($cre_cod=0,$multa='') {
		
		$sql = 'SELECT PA.*, A.*, C.*, I.ina_nome, Cre.cre_nome_fantasia FROM par_acordos PA 	
		INNER JOIN acordos A ON A.aco_cod=PA.acordos_aco_cod 
		INNER JOIN cobrancas C ON C.cob_cod=A.cobranca_cob_cod
		INNER JOIN inadimplentes I ON I.ina_cod = C.inadimplentes_ina_cod
		INNER JOIN credores Cre ON Cre.cre_cod = C.credor_cre_cod
		WHERE PA.paa_situacao=0 AND C.cob_setor="JUD" AND C.cob_recem_enviada=0';
		$sql.= ' AND C.cob_remocao NOT IN (1,2,4) '; //EM ANDAMENTO
		$sql.= ' AND PA.paa_vencimento <= NOW() '; //VENCIDAS
		
		$multaFiltro = $this->getMultaFiltro($multa);
		if (!empty($multaFiltro)) $sql.=' AND '.$multaFiltro;
			
		if (!empty($cre_cod)) {
			$sql.=' AND C.credor_cre_cod='.$cre_cod;	
		}

		//JURIDICO N�O PODE VER 8=Gerado Border� de Devolu��o
		$sql.=' AND C.fiscal_status NOT IN(8) ';

		//RETORNAR UM REGISTRO POR COBRAN�A
		$sql.=' GROUP BY C.cob_cod ORDER BY PA.paa_vencimento';
		//echo $sql; die();
	
        $query = $this->db->query($sql);
		//echo '<pre>'; var_dump($query->result()); die();
		
		//INCLUIR DADOS SOBRE A ULTIMA PARCELA E QUANTOS DIAS AINDA FALTA
		$result = $query->result();
		foreach($result as $i=>$res) {
			//echo '<pre>'; var_dump($res); die();		
			$sql = 'SELECT DATEDIFF(PA.paa_vencimento,NOW()) as dias, PA.paa_vencimento as vencimento FROM par_acordos PA WHERE PA.acordos_aco_cod = '.$res->acordos_aco_cod.' ORDER BY PA.paa_vencimento DESC LIMIT 1';
			$PA_query = $this->db->query($sql);
			$PA_result = $PA_query->result();
			$result[$i]->ultima_parcela = reset($PA_result);
			//echo '<pre>'; var_dump($PA_result); die();
		}
		//echo '<pre>'; var_dump($result); die();
        return $result;

	}

	function pesAcoJuridicoVencimento($data_de='',$data_ate='',$cre_cod=0,$multa='') {
		
		$sql = 'SELECT PA.*, A.*, C.*, I.ina_nome, Cre.cre_nome_fantasia FROM par_acordos PA 	
		INNER JOIN acordos A ON A.aco_cod=PA.acordos_aco_cod 
		INNER JOIN cobrancas C ON C.cob_cod=A.cobranca_cob_cod
		INNER JOIN inadimplentes I ON I.ina_cod = C.inadimplentes_ina_cod
		INNER JOIN credores Cre ON Cre.cre_cod = C.credor_cre_cod
		WHERE PA.paa_situacao=0 AND C.cob_setor="JUD" AND C.cob_recem_enviada=0';
		$sql.= ' AND C.cob_remocao NOT IN (1,2,4) '; //EM ANDAMENTO
		
		$multaFiltro = $this->getMultaFiltro($multa);
		if (!empty($multaFiltro)) $sql.=' AND '.$multaFiltro;
			
		if (empty($data_de) && !empty($data_ate)) {
			$sql.=' AND PA.paa_vencimento <= "'.$data_ate.'"';
		} elseif (!empty($data_de) && empty($data_ate)) {
			$sql.=' AND PA.paa_vencimento >= "'.$data_de.'"';
		} elseif (!empty($data_de) && !empty($data_ate)) {
			$sql.=' AND PA.paa_vencimento BETWEEN "'.$data_de.'" AND "'.$data_ate.'"';			
		}

		if (!empty($cre_cod)) {
			$sql.=' AND C.credor_cre_cod='.$cre_cod;	
		} 

		//JURIDICO N�O PODE VER 8=Gerado Border� de Devolu��o
		$sql.=' AND C.fiscal_status NOT IN(8) ';

		//RETORNAR UM REGISTRO POR COBRAN�A
		$sql.=' GROUP BY C.cob_cod ORDER BY PA.paa_vencimento';
	
        $query = $this->db->query($sql);
        return $query->result();

	}

	function pesAcoJuridicoOciosidade($data_dias=0,$cre_cod=0,$multa='') {
		
		$sql = 'SELECT PA.*, A.*, C.*, RO.*, I.ina_nome, Cre.cre_nome_fantasia, OP.ope_nome, UR.usu_nome AS ros_usu_nome FROM par_acordos PA 	
		INNER JOIN acordos A ON A.aco_cod=PA.acordos_aco_cod 
		INNER JOIN cobrancas C ON C.cob_cod=A.cobranca_cob_cod
		INNER JOIN inadimplentes I ON I.ina_cod = C.inadimplentes_ina_cod
		INNER JOIN credores Cre ON Cre.cre_cod = C.credor_cre_cod
		INNER JOIN ros RO ON RO.ros_cod = C.last_ros_cod
		INNER JOIN operacoes OP ON OP.ope_cod = RO.operacoes_ope_cod
		INNER JOIN usuarios UR ON UR.usu_cod = RO.usuarios_usu_cod
		WHERE PA.paa_situacao=0 AND C.cob_setor="JUD"';
		$sql.= ' AND C.cob_remocao NOT IN (1,2,4) '; //EM ANDAMENTO
		
		$multaFiltro = $this->getMultaFiltro($multa);
		if (!empty($multaFiltro)) $sql.=' AND '.$multaFiltro;
			
		if (empty($data_dias)) $data_dias = 7; //PADR�O 1 SEMANA
		$sql.=' AND DATE(C.last_ros_update)>=DATE(DATE_SUB(NOW(), INTERVAL '.$data_dias.' DAY))';

		if (!empty($cre_cod)) {
			$sql.=' AND C.credor_cre_cod='.$cre_cod;	
		} 

		//JURIDICO N�O PODE VER 8=Gerado Border� de Devolu��o
		$sql.=' AND C.fiscal_status NOT IN(8) ';

		//RETORNAR UM REGISTRO POR COBRAN�A
		$sql.=' GROUP BY C.cob_cod ORDER BY C.last_ros_update ASC';
	
		//var_dump($sql);
	
        $query = $this->db->query($sql);
        return $query->result();

	}

	function pesPorProcesso($processo_num='',$processo_ano='',$processo_forum='',$cre_cod=0) {
		
		$sql = 'SELECT C.*, I.ina_nome, Cre.cre_nome_fantasia FROM cobrancas C 
		INNER JOIN inadimplentes I ON I.ina_cod = C.inadimplentes_ina_cod
		INNER JOIN credores Cre ON Cre.cre_cod = C.credor_cre_cod
		WHERE C.cob_remocao NOT IN (1,2,4) ';
		//$sql.= ' AND C.cob_remocao NOT IN (1,2,4) '; //EM ANDAMENTO
		if (!empty($processo_num)) $sql.=' AND C.processo_num LIKE "'.$processo_num.'%" ';
		if (!empty($processo_ano)) $sql.=' AND C.processo_ano="'.$processo_ano.'" ';		
		if (!empty($processo_forum)) $sql.=' AND C.processo_forum="'.$processo_forum.'" ';
		
		if (empty($processo_num) && empty($processo_ano) && empty($processo_forum)) {
			$sql.=' AND (C.processo_num!="" OR C.processo_ano!="" OR C.processo_forum!="")';
		}
		
		if (!empty($cre_cod)) {
			$sql.=' AND C.credor_cre_cod='.$cre_cod;	
		}

		//JURIDICO N�O PODE VER 8=Gerado Border� de Devolu��o
		$sql.=' AND C.fiscal_status NOT IN(8) ';
		
		//RETORNAR UM REGISTRO POR COBRAN�A
		$sql.=' GROUP BY C.cob_cod ORDER BY C.last_ros_update ASC';
	
		//var_dump($sql);
	
        $query = $this->db->query($sql);
        return $query->result();

	}
	
	function pesProcArquivamento($processo_num='',$processo_ano='',$processo_forum='',$cre_cod=0) {
		
		$sql = 'SELECT C.*, I.ina_nome, Cre.cre_nome_fantasia, 
		C.processo_arq_data, DATEDIFF(C.processo_arq_data,NOW()) as processo_arq_data_dias FROM cobrancas C 
		INNER JOIN inadimplentes I ON I.ina_cod = C.inadimplentes_ina_cod
		INNER JOIN credores Cre ON Cre.cre_cod = C.credor_cre_cod
		WHERE C.cob_remocao NOT IN (1,2,4) '; //EM ANDAMENTO 
		$sql.=' AND C.processo_arq_data!=""';
		
		if (!empty($processo_num)) $sql.=' AND C.processo_num LIKE "'.$processo_num.'%" ';
		if (!empty($processo_ano)) $sql.=' AND C.processo_ano="'.$processo_ano.'" ';		
		if (!empty($processo_forum)) $sql.=' AND C.processo_forum="'.$processo_forum.'" ';

		if (!empty($cre_cod)) {
			$sql.=' AND C.credor_cre_cod='.$cre_cod;
		} 

		//JURIDICO N�O PODE VER 8=Gerado Border� de Devolu��o
		$sql.=' AND C.fiscal_status NOT IN(8) ';

		//RETORNAR UM REGISTRO POR COBRAN�A
		$sql.=' GROUP BY C.cob_cod ORDER BY C.processo_arq_data';
	
        $query = $this->db->query($sql);
        return $query->result();
		
	}
	
	function pesProcDesentranhamento($processo_num='',$processo_ano='',$processo_forum='',$cre_cod=0) {
		
		$sql = 'SELECT C.*, I.ina_nome, Cre.cre_nome_fantasia, 
		C.processo_des_data, DATEDIFF(C.processo_des_data,NOW()) as processo_des_data_dias FROM cobrancas C 
		INNER JOIN inadimplentes I ON I.ina_cod = C.inadimplentes_ina_cod
		INNER JOIN credores Cre ON Cre.cre_cod = C.credor_cre_cod
		WHERE C.cob_remocao NOT IN (1,2,4) '; //EM ANDAMENTO
		
		$sql.=' AND C.processo_des_data!=""';
		
		if (!empty($processo_num)) $sql.=' AND C.processo_num LIKE "'.$processo_num.'%" ';
		if (!empty($processo_ano)) $sql.=' AND C.processo_ano="'.$processo_ano.'" ';		
		if (!empty($processo_forum)) $sql.=' AND C.processo_forum="'.$processo_forum.'" ';

		if (!empty($cre_cod)) {
			$sql.=' AND C.credor_cre_cod='.$cre_cod;
		} 
		
		//JURIDICO N�O PODE VER 8=Gerado Border� de Devolu��o
		$sql.=' AND C.fiscal_status NOT IN(8) ';

		//RETORNAR UM REGISTRO POR COBRAN�A
		$sql.=' GROUP BY C.cob_cod ORDER BY C.processo_des_data';
	
        $query = $this->db->query($sql);
        return $query->result();
		
	}
	
	/*
	 * ALL=0 ADO=1 DCO=2 DIN=3 DCA=4 DIA=5 PES=6 ADE=7 GBD=8 
	*/
	function pesFiscal($fiscal_status='',$fiscal_data_de='',$fiscal_data_ate='',$cre_cod=0,$setor='') {
		
		$sql = 'SELECT CO.cob_cod, CO.fiscal_status, CO.fiscal_data, CR.cre_cod, CR.cre_nome_fantasia, INA.ina_cod, INA.ina_nome, AC.aco_valor_atualizado AS valor, DI.div_total as valor2, CO.cob_setor AS setor
		FROM cobrancas CO 
		INNER JOIN inadimplentes INA ON INA.ina_cod = CO.inadimplentes_ina_cod
		INNER JOIN credores CR ON CR.cre_cod = CO.credor_cre_cod
        	LEFT JOIN acordos AC ON (AC.cobranca_cob_cod = CO.cob_cod)
            LEFT JOIN dividas DI ON (DI.cobranca_cob_cod = CO.cob_cod)
		WHERE INA.ina_ativo=1 AND CO.cob_remocao NOT IN (1,2,4)'; //CO.cob_status IN (1,3,4) AND CO.cob_remocao NOT IN (1,2,4) 

		//ADICIONA FILTRO DO CREDOR
		if (!empty($cre_cod)) $sql.=' AND CO.credor_cre_cod='.$cre_cod;
		
		//DATA
		if (!empty($fiscal_data_de) && !empty($fiscal_data_ate)) {
			$sql.=' AND (CO.fiscal_data BETWEEN "'.$fiscal_data_de.'" AND "'.$fiscal_data_ate.'")';			
		}
		
		//SETOR
		if (!empty($setor)) $sql.=' AND CO.cob_setor="'.$setor.'" ';
		
		//FISCAL STATUS
		if (is_numeric($fiscal_status)) $sql.=' AND CO.fiscal_status='.$fiscal_status.' ';
		
		$sql.=' ORDER BY INA.ina_nome ASC ';
	
        $query = $this->db->query($sql);
        return $query->result();		
		
	} 

	function setFiscalData($cob=0,$data='') {
		if (empty($cob)) return false;
		return $this->db->update('cobrancas',array('fiscal_data'=>$data),array('cob_cod'=>$cob));
	}

	function setFiscalStatus($cob=0,$status='') {
		if (empty($cob)) return false;
		if (empty($status)) $status=0;
		return $this->db->update('cobrancas',array('fiscal_status'=>$status),array('cob_cod'=>$cob));
	}

	function getFiscalStatus($cob=0) {
		if (empty($cob)) return false;
        $query = $this->db->query('SELECT fiscal_status FROM cobrancas WHERE cob_cod=' . $cob);
        return $query->row()!=false? $query->row()->fiscal_status : 0;
	}

	/*
    function pesAcordDiv($filtro, $tipo='black') {
		switch($tipo) {
			case 'red':
				$sql="SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome, ac.aco_valor_atualizado AS valor, d.div_total as valor2, co.`cob_setor` AS setor, 'red' AS cor
                        FROM cobrancas co
                        	INNER JOIN credores cre
                        		ON (cre.cre_cod = co.credor_cre_cod)
                        	INNER JOIN inadimplentes i
                        		ON (i.ina_cod = co.inadimplentes_ina_cod)
                        	LEFT JOIN acordos ac
                        		ON (ac.cobranca_cob_cod = co.cob_cod)
                        	LEFT JOIN dividas d
								ON (d.cobranca_cob_cod = co.cob_cod)
                        	INNER JOIN ros ro
                        		ON ro.`cobranca_cob_cod` = co.`cob_cod`
                        $filtro  
                        AND co.cob_remocao = '0'
                        AND ro.`operacoes_ope_cod` = 82
                        ORDER BY setor, cre.`cre_nome_fantasia`, i.`ina_nome`";
				break;
			case 'blue':
				$sql="SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome,ac.aco_valor_atualizado AS valor, d.div_total as valor2, co.`cob_setor` AS setor, 'blue' AS cor
                        FROM cobrancas co
                        	INNER JOIN credores cre
                        		ON (cre.cre_cod = co.credor_cre_cod)
                        	INNER JOIN inadimplentes i
                        		ON (i.ina_cod = co.inadimplentes_ina_cod)
                        	LEFT JOIN acordos ac
                        		ON (ac.cobranca_cob_cod = co.cob_cod)
                            LEFT JOIN dividas d
                              ON (d.cobranca_cob_cod = co.cob_cod)
                        	INNER JOIN ros ro
                        		ON ro.`cobranca_cob_cod` = co.`cob_cod`
                        $filtro  
                        AND co.cob_remocao = '0'
                        AND ro.`operacoes_ope_cod` = 83
                        ORDER BY setor, cre.`cre_nome_fantasia`, i.`ina_nome`";
                break;
			case 'black':
				$sql = "SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome,ac.aco_valor_atualizado AS valor, co.`cob_setor` AS setor, 'black' AS cor
                        FROM cobrancas co
                        INNER JOIN credores cre
                        	ON (cre.cre_cod = co.credor_cre_cod)
                        INNER JOIN inadimplentes i
                        	ON (i.ina_cod = co.inadimplentes_ina_cod)
                        INNER JOIN acordos ac
                        	ON (ac.cobranca_cob_cod = co.cob_cod)
                        $filtro  
                        AND co.cob_remocao = '0'
                        ORDER BY setor, cre.`cre_nome_fantasia`, i.`ina_nome`";   
                break;                     
			case 'green':
				$sql = "SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome, d.div_total AS valor, co.`cob_setor` AS setor, 'green' AS cor
                          FROM cobrancas co
                            INNER JOIN credores cre
                              ON (cre.cre_cod = co.credor_cre_cod)
                            INNER JOIN inadimplentes i
                              ON (i.ina_cod = co.inadimplentes_ina_cod)
                            INNER JOIN dividas d
                              ON (d.cobranca_cob_cod = co.cob_cod)
                        $filtro
                        AND co.cob_remocao = '0' 
                        ORDER BY cre.`cre_nome_fantasia`, i.`ina_nome`";
                break;
		}

        //pega os acordos
        $queryAcordos = $this->db->query($sql);

//        $acordos = $queryAcordos->result();


        //pega as d�vidas
//        $queryDividas = $this->db->query(
//                        "SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome, d.div_total AS valor
//                          FROM cobrancas co
//                            INNER JOIN credores cre
//                              ON (cre.cre_cod = co.credor_cre_cod)
//                            INNER JOIN inadimplentes i
//                              ON (i.ina_cod = co.inadimplentes_ina_cod)
//                            INNER JOIN dividas d
//                              ON (d.cobranca_cob_cod = co.cob_cod)
//                        $filtro AND co.cob_remocao = '0' 
//                        ORDER BY cre.`cre_nome_fantasia`, i.`ina_nome`");
//        $dividas = $queryDividas->result();

        return $queryAcordos->result();
    }
	*/
//    function pesAcordDiv($filtro) {
//        /* pega os acordos */
//        $queryAcordos = $this->db->query(
//                       "(SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome,ac.aco_valor_atualizado AS valor, co.`cob_setor` AS setor, 'red' AS cor
//                        FROM cobrancas co
//                        INNER JOIN credores cre
//                        ON (cre.cre_cod = co.credor_cre_cod)
//                        INNER JOIN inadimplentes i
//                        ON (i.ina_cod = co.inadimplentes_ina_cod)
//                        INNER JOIN acordos ac
//                        ON (ac.cobranca_cob_cod = co.cob_cod)
//                        INNER JOIN ros ro
//                        ON ro.`cobranca_cob_cod` = co.`cob_cod`
//                        $filtro  
//                        AND co.cob_remocao = '0'
//                        AND ro.`operacoes_ope_cod` = 82
//                        AND co.`cob_setor` = 'ADM'
//                        ORDER BY setor, cre.`cre_nome_fantasia`, i.`ina_nome`) 
//                        UNION
//                        (SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome,ac.aco_valor_atualizado AS valor, co.`cob_setor` AS setor, 'blue' AS cor
//                        FROM cobrancas co
//                        INNER JOIN credores cre
//                        ON (cre.cre_cod = co.credor_cre_cod)
//                        INNER JOIN inadimplentes i
//                        ON (i.ina_cod = co.inadimplentes_ina_cod)
//                        INNER JOIN acordos ac
//                        ON (ac.cobranca_cob_cod = co.cob_cod)
//                        INNER JOIN ros ro
//                        ON ro.`cobranca_cob_cod` = co.`cob_cod`
//                        $filtro  
//                        AND co.cob_remocao = '0'
//                        AND ro.`operacoes_ope_cod` = 83
//                        AND co.`cob_setor` = 'ADM'
//                        ORDER BY setor, cre.`cre_nome_fantasia`, i.`ina_nome`)
//                        UNION
//                        (SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome,ac.aco_valor_atualizado AS valor, co.`cob_setor` AS setor, 'black' AS cor
//                        FROM cobrancas co
//                        INNER JOIN credores cre
//                        ON (cre.cre_cod = co.credor_cre_cod)
//                        INNER JOIN inadimplentes i
//                        ON (i.ina_cod = co.inadimplentes_ina_cod)
//                        INNER JOIN acordos ac
//                        ON (ac.cobranca_cob_cod = co.cob_cod)
//                        $filtro  
//                        AND co.cob_remocao = '0'
//                        ORDER BY setor, cre.`cre_nome_fantasia`, i.`ina_nome`
//                        LIMIT 999999)");
//        $acordos = $queryAcordos->result();
//
//
//        /* pega as d�vidas */
//        $queryDividas = $this->db->query(
//                        "SELECT co.cob_cod, cre.cre_nome_fantasia, i.ina_cod, i.ina_nome, d.div_total AS valor
//                          FROM cobrancas co
//                            INNER JOIN credores cre
//                              ON (cre.cre_cod = co.credor_cre_cod)
//                            INNER JOIN inadimplentes i
//                              ON (i.ina_cod = co.inadimplentes_ina_cod)
//                            INNER JOIN dividas d
//                              ON (d.cobranca_cob_cod = co.cob_cod)
//                        $filtro AND co.cob_remocao = '0' 
//                        ORDER BY cre.`cre_nome_fantasia`, i.`ina_nome`");
//        $dividas = $queryDividas->result();
//
//        return array($acordos, $dividas); //retorna um array com os acordos e com as d�vidas
//    }

    function buscInaFone($valorDigitado) {
        $query = $this->db->query("SELECT I.ina_cod,I.ina_nome,I.ina_endereco,I.ina_cidade FROM inadimplentes I LEFT JOIN parentes P ON (P.inadimplentes_ina_cod=I.ina_cod)
                                    WHERE I.ina_fonecom = '$valorDigitado' 
                                    OR I.ina_fonerec = '$valorDigitado' 
                                    OR I.ina_foneres = '$valorDigitado'
                                    OR I.ina_cel1 = '$valorDigitado'
                                    OR I.ina_cel2 = '$valorDigitado'
                                    OR I.ina_cel3 = '$valorDigitado'
                                    OR I.ina_conj_fone = '$valorDigitado'
                                    OR I.ina_mae_fone = '$valorDigitado'
                                    OR I.ina_pai_fone = '$valorDigitado'
                                    OR P.par_fone1 = '$valorDigitado'
                                    OR P.par_fone2 = '$valorDigitado'
                                    OR P.par_fone3 = '$valorDigitado'
                                    OR P.par_fone4 = '$valorDigitado'
                                    GROUP BY I.ina_cod ORDER BY I.ina_nome ");
        return $query->result();
    }

    /* Busca inadimplente pelo telefone na tabela parente */

    function buscInaPar($valorDigitado) {
        /*
         * Alteracao feita dia 08-11-2011, se n�o pedirem par alterar pode apagar essa parte comentada
         * O sistema foi feito originalmente assim, n�o aparecia o nome do parente.
         * $query = $this->db->query("aSELECT I.ina_cod,I.ina_nome,I.ina_endereco,I.ina_cidade,I.ina_foneres,I.ina_cel1,P.par_nome,P.par_parentesco FROM inadimplentes I LEFT JOIN parentes P ON (P.inadimplentes_ina_cod=I.ina_cod)
                                    WHERE I.ina_conj_nome LIKE '%$valorDigitado%' 
                                    OR I.ina_mae_nome LIKE '%$valorDigitado%'
                                    OR I.ina_pai_nome LIKE '%$valorDigitado%'
                                    OR P.par_nome LIKE '%$valorDigitado%'
                                    ORDER BY I.ina_nome ASC;");*/
        $query = $this->db->query
                ("
                    SELECT
                      I.ina_cod, I.ina_nome, I.ina_endereco, I.ina_cidade, I.ina_foneres, I.ina_cel1, P.par_parentesco,
                      IF(I.ina_conj_nome LIKE '%$valorDigitado%', I.ina_conj_nome, '') AS ina_conj_nome,
                      IF(I.ina_mae_nome LIKE '%$valorDigitado%', I.ina_mae_nome, '') AS ina_mae_nome,
                      IF(I.ina_pai_nome LIKE '%$valorDigitado%', I.ina_pai_nome, '') AS ina_pai_nome,
                      IF(P.par_nome LIKE '%$valorDigitado%', P.par_nome, '') AS par_nome
                    FROM inadimplentes I
                      LEFT JOIN parentes P
                        ON (P.inadimplentes_ina_cod = I.ina_cod)
                    WHERE I.ina_conj_nome LIKE '%$valorDigitado%'
                     OR I.ina_mae_nome LIKE '%$valorDigitado%'
                     OR I.ina_pai_nome LIKE '%$valorDigitado%'
                     OR P.par_nome LIKE '%$valorDigitado%'
                    ORDER BY I.ina_nome ASC
                ");
        return $query->result();
    }

}

?>