<?php
class Judicial_model extends Model {
    function  __construct()
    {
        // Call the Model constructor
        parent::Model();
    }

    function getAcPagInt($filtro){
        $query = $this->db->query('SELECT A.*,CR.cre_nome_fantasia,I.ina_nome,I.ina_cod, C.cob_cod FROM acordos A
                                    INNER JOIN cobrancas C ON(C.cob_cod=A.cobranca_cob_cod AND (C.cob_remocao = 1))
                                    INNER JOIN inadimplentes I ON(I.ina_cod=C.inadimplentes_ina_cod)
                                    INNER JOIN credores CR ON(CR.cre_cod=C.credor_cre_cod)
                                    WHERE A.aco_motivo_ext=2 '.$filtro);
        return $query->result();
    }

    function getAcSolCred($filtro){
        $query = $this->db->query('SELECT A.*,CR.cre_nome_fantasia,I.ina_nome,I.ina_cod, C.cob_cod  FROM acordos A
                                    INNER JOIN cobrancas C ON(C.cob_cod=A.cobranca_cob_cod AND (C.cob_remocao = 1))
                                    INNER JOIN inadimplentes I ON(I.ina_cod=C.inadimplentes_ina_cod)
                                    INNER JOIN credores CR ON(CR.cre_cod=C.credor_cre_cod)
                                    WHERE A.aco_motivo_ext=3 '.$filtro);
        return $query->result();
    }

    function getAcOutros($filtro){
        $query = $this->db->query('SELECT A.*,CR.cre_nome_fantasia,I.ina_nome,I.ina_cod, C.cob_cod FROM acordos A
                                    INNER JOIN cobrancas C ON(C.cob_cod=A.cobranca_cob_cod AND (C.cob_remocao = 1))
                                    INNER JOIN inadimplentes I ON(I.ina_cod=C.inadimplentes_ina_cod)
                                    INNER JOIN credores CR ON(CR.cre_cod=C.credor_cre_cod)
                                    WHERE A.aco_motivo_ext!=3 AND A.aco_motivo_ext!=2 '.$filtro);
        return $query->result();
    }

    function getJudSolDoc($filtro){
        $query = $this->db->query("SELECT DISTINCT C.cob_cod,I.ina_nome,CR.cre_nome_fantasia,D.aco_total_parcelado AS div_total,I.ina_cod,
                                    (SELECT R.ros_data FROM ros R WHERE R.cobranca_cob_cod=C.cob_cod ORDER BY R.ros_data DESC LIMIT 1) AS ro_data,
                                    (SELECT O.ope_nome FROM ros R INNER JOIN operacoes O ON (O.ope_cod=R.operacoes_ope_cod) WHERE R.cobranca_cob_cod=C.cob_cod ORDER BY R.ros_data DESC LIMIT 1) AS ro_nome
                                    FROM cobrancas C
                                    INNER JOIN inadimplentes I ON (I.ina_cod=C.inadimplentes_ina_cod)
                                    INNER JOIN credores CR ON (CR.cre_cod = C.credor_cre_cod)
                                    INNER JOIN acordos D ON(D.cobranca_cob_cod=C.cob_cod)
                                    WHERE (C.cob_setor='JUD' AND C.cob_status=1) AND (C.cob_remocao = 0) $filtro");
        return $query->result();
    }

    function getJudAguardando($filtro){
        $query = $this->db->query("SELECT DISTINCT C.cob_cod,I.ina_nome,CR.cre_nome_fantasia,D.aco_total_parcelado AS div_total,I.ina_cod,
                                    (SELECT R.ros_data FROM ros R WHERE R.cobranca_cob_cod=C.cob_cod ORDER BY R.ros_data DESC LIMIT 1) AS ro_data,
                                    (SELECT O.ope_nome FROM ros R INNER JOIN operacoes O ON (O.ope_cod=R.operacoes_ope_cod) WHERE R.cobranca_cob_cod=C.cob_cod ORDER BY R.ros_data DESC LIMIT 1) AS ro_nome
                                    FROM cobrancas C
                                    INNER JOIN inadimplentes I ON (I.ina_cod=C.inadimplentes_ina_cod)
                                    INNER JOIN credores CR ON (CR.cre_cod = C.credor_cre_cod)
                                    INNER JOIN acordos D ON(D.cobranca_cob_cod=C.cob_cod)
                                    WHERE (C.cob_setor='JUD' AND C.cob_status=2) AND (C.cob_remocao = 0) $filtro");
        return $query->result();
    }

    function getJudAjuizado($filtro){
        $query = $this->db->query("SELECT DISTINCT C.cob_cod,I.ina_nome,CR.cre_nome_fantasia,D.aco_total_parcelado AS div_total,I.ina_cod,
                                    (SELECT R.ros_data FROM ros R WHERE R.cobranca_cob_cod=C.cob_cod ORDER BY R.ros_data DESC LIMIT 1) AS ro_data,
                                    (SELECT O.ope_nome FROM ros R INNER JOIN operacoes O ON (O.ope_cod=R.operacoes_ope_cod) WHERE R.cobranca_cob_cod=C.cob_cod ORDER BY R.ros_data DESC LIMIT 1) AS ro_nome
                                    FROM cobrancas C
                                    INNER JOIN inadimplentes I ON (I.ina_cod=C.inadimplentes_ina_cod)
                                    INNER JOIN credores CR ON (CR.cre_cod = C.credor_cre_cod)
                                    INNER JOIN acordos D ON(D.cobranca_cob_cod=C.cob_cod)
                                    WHERE (C.cob_setor='JUD' AND C.cob_status=3) AND (C.cob_remocao = 0) $filtro");
        return $query->result();
    }

    function getJudAndamento($filtro){
        $query = $this->db->query("SELECT DISTINCT C.cob_cod,I.ina_nome,CR.cre_nome_fantasia,D.aco_total_parcelado AS div_total,I.ina_cod,
                                    (SELECT R.ros_data FROM ros R WHERE R.cobranca_cob_cod=C.cob_cod ORDER BY R.ros_data DESC LIMIT 1) AS ro_data,
                                    (SELECT O.ope_nome FROM ros R INNER JOIN operacoes O ON (O.ope_cod=R.operacoes_ope_cod) WHERE R.cobranca_cob_cod=C.cob_cod ORDER BY R.ros_data DESC LIMIT 1) AS ro_nome
                                    FROM cobrancas C
                                    INNER JOIN inadimplentes I ON (I.ina_cod=C.inadimplentes_ina_cod)
                                    INNER JOIN credores CR ON (CR.cre_cod = C.credor_cre_cod)
                                    INNER JOIN acordos D ON(D.cobranca_cob_cod=C.cob_cod)
                                    WHERE (C.cob_setor='JUD' AND C.cob_status=4) AND (C.cob_remocao = 0)  $filtro");
        return $query->result();
    }

    function getJudAcordos($filtro){
        $query = $this->db->query("SELECT DISTINCT C.cob_cod,I.ina_nome,CR.cre_nome_fantasia,A.aco_valor_atualizado,I.ina_cod,
                                    (SELECT R.ros_data FROM ros R WHERE R.cobranca_cob_cod=C.cob_cod ORDER BY R.ros_data DESC LIMIT 1) AS ro_data
                                    FROM cobrancas C
                                    INNER JOIN inadimplentes I ON (I.ina_cod=C.inadimplentes_ina_cod)
                                    INNER JOIN credores CR ON (CR.cre_cod = C.credor_cre_cod)
                                    INNER JOIN acordos A ON(A.cobranca_cob_cod=C.cob_cod)
                                    WHERE (C.cob_setor='JUD' AND A.aco_procedencia='JUD') AND (C.cob_remocao = 0) $filtro");
        return $query->result();
    }

    function getAcAtrasados($data,$filtro){
        $query = $this->db->query("SELECT DISTINCT C.cob_cod,I.ina_nome,I.ina_cod,CR.cre_nome_fantasia,A.aco_valor_atualizado FROM par_acordos PA
                                    INNER JOIN acordos A ON(A.aco_cod=PA.acordos_aco_cod)
                                    INNER JOIN cobrancas C ON(C.cob_cod=A.cobranca_cob_cod AND (C.cob_remocao = 0))
                                    INNER JOIN inadimplentes I ON(I.ina_cod=C.inadimplentes_ina_cod)
                                    INNER JOIN credores CR ON (CR.cre_cod=C.credor_cre_cod)
                                    WHERE (PA.paa_vencimento<'$data' AND PA.paa_situacao=0) and C.cob_setor = 'JUD' AND C.cob_recem_enviada = '0' $filtro");
        return $query->result();
    }

    function getParAtrasada($data,$cod){
        $query = $this->db->query("SELECT PA.paa_vencimento,PA.paa_parcela FROM par_acordos PA
                                    INNER JOIN acordos A ON(A.aco_cod=PA.acordos_aco_cod)
                                    INNER JOIN cobrancas C ON(C.cob_cod=A.cobranca_cob_cod AND (C.cob_remocao = 0))
                                    WHERE PA.paa_vencimento<'$data' AND PA.paa_situacao='0' AND C.cob_cod = '$cod'
                                    ORDER BY PA.paa_vencimento ASC LIMIT 1");
        return $query->result();
    }
    
    function getCobPorProcesso($processo_num='') {
    	$query = $this->db->query('SELECT C.*, CR.cre_nome_fantasia, I.ina_nome FROM cobrancas C
    	INNER JOIN inadimplentes I ON(I.ina_cod=C.inadimplentes_ina_cod)
    	INNER JOIN credores CR ON (CR.cre_cod=C.credor_cre_cod)
    	WHERE C.processo_num="'.$processo_num.'"
    	LIMIT 1');
		return $query->row();
    }
}

?>