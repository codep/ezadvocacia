<?php
class Inadimplente_model extends Model {
    function  __construct()
    {
        // Call the Model constructor
        parent::Model();
    }

    function getRecuperadores(){//pega os usu�rios do sistema
        $query = $this->db->query("SELECT usu.usu_cod, usu.usu_usuario_sis FROM usuarios usu WHERE usu.usu_ativo = 1 ORDER BY usu.usu_usuario_sis");
        return $query->result();
    }
    function getRecuperadoresRemanejar(){//pega os usu�rios do sistema
        $query = $this->db->query("SELECT usu.usu_cod, usu.usu_usuario_sis,usu.usu_ativo FROM usuarios usu ORDER BY usu.usu_usuario_sis");
        return $query->result();
    }
    function getInadLista($ini, $fim,$filtro){
        $query = $this->db->query("SELECT I.ina_cod,I.ina_nome,I.ina_endereco,I.ina_cidade,I.ina_foneres,I.ina_cel1 FROM inadimplentes I $filtro ORDER BY I.ina_nome ASC LIMIT $ini, $fim");
        return $query->result();
    }
    function getRows($filtro=''){
        $query = $this->db->query("SELECT COUNT(ina_cod) AS trow FROM inadimplentes I $filtro");
        return $query->row()->trow;
    }
    function getCidades(){
        $query = $this->db->query('SELECT DISTINCT I.ina_cidade FROM inadimplentes I ORDER BY I.ina_cidade ASC');
        return $query->result();
    }
    function getCidadesBrasil($uf){
        $query = $this->db->query('SELECT DISTINCT C.cid_nome FROM cidades C WHERE C.cid_estado="'.$uf.'" ORDER BY C.cid_nome ASC');
        return $query->result();
    }
    function getEstados(){
        $query = $this->db->query('SELECT DISTINCT C.cid_estado FROM cidades C ORDER BY C.cid_estado ASC');
        return $query->result();
    }
    function getInadDados($cod){
        $query = $this->db->query('SELECT * FROM inadimplentes I WHERE I.ina_cod='.$cod);
        return $query->row();
    }
    function getParentes($cod){
        $query = $this->db->query("SELECT P.par_cod, P.par_parentesco, P.par_nome, P.par_fone1, P.par_fone2, P.par_fone3, P.par_fone4, P.par_inad_cod FROM parentes P WHERE P.inadimplentes_ina_cod=$cod ORDER BY P.par_nome ASC");
        return $query->result();
    }
    function getPesquisas($cod){
        $query = $this->db->query(" SELECT R.ros_data,O.ope_nome,U.usu_nome FROM ros R
            INNER JOIN operacoes O ON (O.ope_cod=R.operacoes_ope_cod)
            INNER JOIN usuarios U ON(U.usu_cod=R.usuarios_usu_cod)
            INNER JOIN cobrancas C ON(C.cob_cod=R.cobranca_cob_cod)
            WHERE C.inadimplentes_ina_cod=$cod AND (O.ope_nome LIKE '%PESQUISA%' OR R.ros_detalhe LIKE '%PESQUISA%')");
        return $query->result();
    }
    function getBem($cod){
        $query = $this->db->query("SELECT B.ben_cod,B.ben_tipo,B.ben_titulo,B.ben_detalhes FROM bens B WHERE B.inadimplentes_ina_cod=$cod");
        return $query->row();
    }

    function getParDados($parCod){
        $query = $this->db->query("SELECT * FROM parentes P WHERE P.par_cod='$parCod'");
        return $query->row();
    }

    function setRemanejarInad($inad_cod, $novo_recuperador){//REMANEJAR INADIMPLENTE
        $query = $this->db->query("UPDATE cobrancas co SET co.usuarios_usu_cod = $novo_recuperador WHERE co.inadimplentes_ina_cod = $inad_cod AND co.cob_remocao = 0");/*cob_remocao = 0 � cobran�a ativa*/
        return $this->db->affected_rows();
    }
    
    function getDadosImportInade($inaCod){
        
        $dadosImportados = "";
        
        $query = $this->db->query
                ("
                    SELECT
                    iina.*,
                    CASE iina.ina_pessoa WHEN 'f' THEN 'Fisica' WHEN 'j' THEN 'Juridica' ELSE'N/A' END AS pessoa,
                    CASE iina.ina_sexo WHEN 'f' THEN 'Feminino' WHEN 'm' THEN 'Masculino' ELSE 'N/A' END AS sexo,
                    CASE iina.ina_estado_civil
                        WHEN '1' THEN 'Solteiro'
                        WHEN '2' THEN 'Casado'
                        WHEN '3' THEN 'Separado'
                        WHEN '4' THEN 'Divorciado'
                        WHEN '5' THEN 'Viuvo'
                        WHEN '5' THEN 'Amasiado'
                        ELSE 'N/A' END AS estado_civil
                    FROM import_inadimplentes iina
                    WHERE iina.ina_inad_original_cod = $inaCod ORDER BY iina.ina_cod DESC LIMIT 1;
                    ;
                ");
        $dadosImportados = $query->result();
        return $dadosImportados;
    }
    
    function existeImportacao($cod){
        $existeImport = "";
        
        $query = $this->db->query
                ("
                    SELECT
                    iina.ina_inad_original_cod
                    FROM import_inadimplentes iina
                    WHERE iina.ina_inad_original_cod = $cod LIMIT 1;
                ");
        $existeImport = $query->row();
        
        if(sizeof($existeImport) > 0){
            return 1;
        }else{
            return 0;
        }
        die("Inesperado");
    }
    
}

?>