<?php

class Dividamodel extends Model {

    var $table = ''; // TABELA PRINCIPAL

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getInadDados($cod) {
        $query = $this->db->query("SELECT * FROM inadimplentes I WHERE I.ina_cod='$cod'");
        return $query->row();
    }

    function getInadParentes($cod) {
        $query = $this->db->query("SELECT * FROM parentes P WHERE P.inadimplentes_ina_cod='$cod'");
        return $query->result();
    }

    function getCredoresComDividas($cod) {
        $query = $this->db->query("SELECT DISTINCT C.credor_cre_cod,CR.cre_nome_fantasia FROM cobrancas C 
        RIGHT JOIN dividas D ON(D.cobranca_cob_cod=C.cob_cod)
        INNER JOIN credores CR ON (CR.cre_cod=C.credor_cre_cod) 
        WHERE C.inadimplentes_ina_cod=$cod AND C.cob_setor='ADM' 
        AND C.cob_recem_enviada = 0 AND C.cob_remocao != '1' AND C.cob_remocao != '4' AND C.cob_remocao != '2'");
        return $query->result();
    }

    function getCredoresComAcordos($cod) {
        $query = $this->db->query("SELECT DISTINCT C.credor_cre_cod,CR.cre_nome_fantasia FROM cobrancas C RIGHT JOIN acordos A ON(A.cobranca_cob_cod=C.cob_cod) INNER JOIN credores CR ON (CR.cre_cod=C.credor_cre_cod) WHERE C.inadimplentes_ina_cod=$cod AND C.cob_setor='ADM' AND C.cob_recem_enviada = 0 AND C.cob_remocao != '1' AND C.cob_remocao != '4' AND C.cob_remocao != '2'");
        return $query->result();
    }

    function getCredoresComJudicial($cod) {
        $query = $this->db->query("SELECT DISTINCT C.credor_cre_cod,CR.cre_nome_fantasia FROM cobrancas C INNER JOIN credores CR ON (CR.cre_cod=C.credor_cre_cod) WHERE C.inadimplentes_ina_cod=$cod AND C.cob_setor='JUD' AND C.cob_recem_enviada = 0 AND C.cob_remocao != '1' AND C.cob_remocao != '4' AND C.cob_remocao != '2'");
        return $query->result();
    }

    function getDividasCredInad($inaCod, $creCod) {
        $query = $this->db->query("SELECT D.*,C.cob_cod, C.cob_avaliacao, C.processo_arq_data, C.processo_des_data, C.processo_num, C.processo_ano, C.processo_forum FROM cobrancas C RIGHT JOIN dividas D ON(D.cobranca_cob_cod=C.cob_cod) WHERE C.inadimplentes_ina_cod=$inaCod AND C.credor_cre_cod=$creCod  AND C.cob_setor='ADM' AND C.cob_recem_enviada = 0 AND C.cob_remocao != '1' AND C.cob_remocao != '4' AND C.cob_remocao != '2'");
        return $query->result();
    }
    
    /**************** KONRRADO *****************/
    function getDividasCredInadGeradas($cod)
    {
        $query = $this->db->query("SELECT cobranca_cob_cod 
                                   FROM acordos
                                   WHERE aco_cob_cod_geradora = $cod
                                   OR aco_cob_cod_geradora LIKE '%-$cod'");
        return $query->result();
    }
    /******************************************/
    
    function getDividasCredInadJUD($inaCod, $creCod) {
        $query = $this->db->query("SELECT C.cob_status,C.cob_cod,C.cob_subsetor,D.* FROM cobrancas C RIGHT JOIN dividas D ON(D.cobranca_cob_cod=C.cob_cod) WHERE C.inadimplentes_ina_cod=$inaCod AND C.credor_cre_cod=$creCod AND C.cob_setor='JUD' AND C.cob_recem_enviada = 0 AND C.cob_remocao != '1' AND C.cob_remocao != '4' AND C.cob_remocao != '2'");
        return $query->result();
    }
    
    function getAcordo($aco_cod=0) {
        $query = $this->db->query("SELECT ac.* FROM acordos ac WHERE ac.aco_cod='$aco_cod'");
        return $query->row();
    }

    function getAcordosCredInad($inaCod, $creCod) {
        $query = $this->db->query("SELECT A.*,C.cob_cod, C.cob_avaliacao,C.cob_subsetor, C.processo_arq_data, C.processo_des_data, C.processo_num, C.processo_ano, C.processo_forum FROM cobrancas C RIGHT JOIN acordos A ON(A.cobranca_cob_cod=C.cob_cod) WHERE C.inadimplentes_ina_cod=$inaCod AND C.credor_cre_cod=$creCod AND C.cob_setor='ADM' AND C.cob_recem_enviada = 0 AND C.cob_remocao != '1' AND C.cob_remocao != '4' AND C.cob_remocao != '2'");
        return $query->result();
    }

    function getAcordosCredInadJUD($inaCod, $creCod) {
        $query = $this->db->query("SELECT C.cob_status,C.cob_cod,C.cob_avaliacao,C.cob_subsetor, C.processo_arq_data, C.processo_des_data, C.processo_num, C.processo_ano, C.processo_forum, A.* FROM cobrancas C RIGHT JOIN acordos A ON(A.cobranca_cob_cod=C.cob_cod) WHERE C.inadimplentes_ina_cod=$inaCod AND C.credor_cre_cod=$creCod  AND C.cob_setor='JUD' AND C.cob_recem_enviada = 0 AND C.cob_remocao != '1' AND C.cob_remocao != '4' AND C.cob_remocao != '2'");
        return $query->result();
    }

    function getAcordosJudRecemCriados($inaCod) {
    	$sql="SELECT
                                      CE.cre_nome_fantasia,
                                      A.aco_valor_original,
                                      A.aco_valor_atualizado,
                                      A.aco_emissao,
                                      A.aco_cod,
                                      CO.cob_cod,
                                      CO.cob_setor,
                                      CO.cob_subsetor,
                                      CO.processo_num, CO.processo_ano, CO.processo_forum, CO.processo_arq_data, CO.processo_des_data, 
                                      A.aco_cob_cod_geradora
                                    FROM cobrancas CO
                                      INNER JOIN credores CE
                                        ON (CE.cre_cod = CO.credor_cre_cod)
                                      INNER JOIN acordos A
                                        ON (A.cobranca_cob_cod = CO.cob_cod)
                                    WHERE CO.cob_recem_enviada = 1
                                        AND CO.cob_setor = 'JUD' AND CO.cob_remocao = '0' AND CO.inadimplentes_ina_cod = $inaCod";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function getParcelasDividas($divCod) {
        $query = $this->db->query("SELECT * FROM par_dividas PA WHERE PA.dividas_div_cod=$divCod");
        return $query->result();
    }

    function getParcelasAcordos($divCod) {
        $query = $this->db->query("SELECT * FROM par_acordos PA WHERE PA.acordos_aco_cod=$divCod");
        return $query->result();
    }

    function getUsuariosNomeCod() {
//        $query = $this->db->query('SELECT U.usu_cod, U.usu_usuario_sis FROM usuarios U ORDER BY U.usu_usuario_sis');
        //CORRE��O ERRO 10 em 20/09/2013
        $query = $this->db->query('SELECT U.usu_cod, U.usu_usuario_sis FROM usuarios U where U.usu_ativo = 1 ORDER BY U.usu_usuario_sis');
        return $query->result();
    }

    function getRepasses() {
        $query = $this->db->query('select * from repasses order by rep_cod');
        return $query->result();
    }

    function getCidades() {
        $query = $this->db->query('SELECT DISTINCT c.cre_cidade from credores c ORDER BY c.cre_cidade');
        return $query->result();
    }

    function getAtivos($codigo) {
        $query = $this->db->query('SELECT FX_GET_ATIVOS(' . $codigo . ') as ativos;');
        return $query->row();
    }

    function getCredores($cidade) {
        if ($cidade == '' || $cidade == 'TODOS')
            $query = $this->db->query('SELECT * FROM credores c where c.cre_ativo = 1');
        else
            $query = $this->db->query('SELECT * FROM credores c where c.cre_ativo = 1 AND c.cre_cidade = "' . $cidade . '"');
        return $query->result();
    }

    function getCredor($cod) {
        $query = $this->db->query('SELECT * FROM credores c where c.cre_ativo=1 AND c.cre_cod=' . $cod);
        return $query->row();
    }

    function getCobDadosRecebimento($cod) {
//        $query = $this->db->query("SELECT A.aco_cod,C.credor_cre_cod,C.inadimplentes_ina_cod,U.usu_usuario_sis, FROM cobrancas C 
        $query = $this->db->query("SELECT A.aco_cod,C.credor_cre_cod,C.inadimplentes_ina_cod,U.usu_usuario_sis, U.usu_cod FROM cobrancas C 
                                    INNER JOIN usuarios U ON (U.usu_cod = C.usuarios_usu_cod) 
                                    INNER JOIN acordos A ON (A.cobranca_cob_cod=C.cob_cod)  WHERE C.cob_cod=$cod");
        return $query->row();
    }

    function getCobDadosUltimoRecebimento($acoCod, $creCod, $inaCod, $data, $valor, $desconto) {
        $query = $this->db->query("SELECT * FROM recebimentos R WHERE R.acordos_aco_cod=$acoCod AND R.credor_cre_cod=$creCod AND R.inadimplentes_ina_cod=$inaCod AND R.reb_data='$data' AND R.reb_valor='$valor' AND R.reb_desconto='$desconto'");
        return $query->row();
    }

    function getEstados() {
        $query = $this->db->query('SELECT DISTINCT C.cid_estado FROM cidades C ORDER BY C.cid_estado ASC');
        return $query->result();
    }

    function getCidadesBrasil($uf) {
        $query = $this->db->query('SELECT DISTINCT C.cid_nome FROM cidades C WHERE c.cid_estado="' . $uf . '" ORDER BY C.cid_nome ASC');
        return $query->result();
    }

    function getBancos() {
        $query = $this->db->query('SELECT * FROM bancos ORDER BY banco_nome ASC');
        return $query->result();
    }

    function getAcoCodigo($cod) {
        $query = $this->db->query("SELECT * FROM acordos A WHERE A.cobranca_cob_cod=$cod");
        return $query->row();
    }

    function insert($dados=array()) {
        if ($this->db->insert('credores', $dados)) {
            return true;
        } else {
            return false;
        }
    }

    function getCobCad($sync='',$cob_cod=0) {
    	if (!empty($cob_cod)) {
    		$filtro = "C.cob_cod='$cob_cod'";
    	} else {
    		$filtro = "C.cob_sync='$sync'";
    	}
        $query = $this->db->query("SELECT * FROM cobrancas C WHERE $filtro");
        return $query->row();
    }

    function getDivCad($cod) {
        $query = $this->db->query("SELECT * FROM dividas D WHERE D.cobranca_cob_cod='$cod'");
        return $query->row();
    }

    function getAcoCad($cod) {
        $query = $this->db->query("SELECT * FROM acordos A WHERE A.cobranca_cob_cod='$cod'");
        return $query->row();
    }
	
    function getAcoFromGeradoraCad($cod) {
        $query = $this->db->query("SELECT C.*, A.* FROM acordos A INNER JOIN cobrancas C ON C.cob_cod=A.cobranca_cob_cod WHERE A.aco_cob_cod_geradora='$cod' AND cob_remocao=0");
        return $query->result();
    }

    function getSupervisores() {
        $query = $this->db->query("SELECT U.usu_nome,U.usu_cod,GU.gru_titulo FROM grupo_usuarios GU INNER JOIN usuarios U ON (U.grupo_usuarios_gru_cod=GU.gru_cod) WHERE (GU.gru_cod = '1') OR (GU.gru_cod = '2') OR (GU.gru_cod = '5') OR (GU.gru_titulo = 'Administrador')");
        return $query->result();
    }

    /* -- LISTAR TODAS AS COBRANCAS (DIVIDAS E ACORDOS)-- */

    function getListarDividas($ini,$fim,$filtro) {
        $dividasEAcordos = array('dividas' => '', 'acordos' => ''); //vai receber as d�vidas e os acordos encontrados

        /* ------- Pega todas as divida ------- */
        $query = $this->db->query
                ("
                    SELECT cob.cob_cod, ina.ina_nome, cre.cre_nome_fantasia, di.div_emissao, di.div_cadastro, di.div_total
                    FROM cobrancas cob
                    INNER JOIN dividas di
                    ON(cob.cob_cod = di.cobranca_cob_cod)
                    INNER JOIN inadimplentes ina
                    ON(cob.inadimplentes_ina_cod = ina.ina_cod)
                    INNER JOIN credores cre
                    ON(cob.credor_cre_cod = cre.cre_cod)
                    WHERE $filtro cob.cob_remocao IN (0) ORDER BY ina.ina_nome ASC LIMIT $ini, $fim;
                ");
        $dividas = $query->result();
        /* ------- fim Pega todas as divida ------- */

        $filtro = str_ireplace('div_emissao', 'aco_emissao', $filtro); //aqui substitui para poder pesquisar na tabela acordos

        /* -------------- pega todos os acordos -------------- */
        $query = $this->db->query
                ("
                    SELECT cob.cob_cod, ina.ina_nome, cre.cre_nome_fantasia, aco.aco_emissao, aco.aco_total_parcelado
                    FROM cobrancas cob
                    INNER JOIN acordos aco
                    ON(cob.cob_cod = aco.cobranca_cob_cod)
                    INNER JOIN inadimplentes ina
                    ON(cob.inadimplentes_ina_cod = ina.ina_cod)
                    INNER JOIN credores cre
                    ON(cob.credor_cre_cod = cre.cre_cod)
                    WHERE $filtro cob.cob_remocao IN (0, 4) ORDER BY ina.ina_nome ASC LIMIT $ini, $fim;
                ");
        $acordos = $query->result();
        /* -------------- fim pega todos os acordos -------------- */

        return $dividasEAcordos = array('dividas' => $dividas, 'acordos' => $acordos);
    }
    
    /* --- Fun��o que conta quantos tem para paginar ---*/
    
    function getRows() { // do moveis columbia - ver com o anderson sobre como alterar pro recupera
        $query = $this->db->query("SELECT COUNT(cob.cob_cod) AS trow
                    FROM cobrancas cob
                    INNER JOIN dividas di
                    ON(cob.cob_cod = di.cobranca_cob_cod)
                    INNER JOIN inadimplentes ina
                    ON(cob.inadimplentes_ina_cod = ina.ina_cod)
                    INNER JOIN credores cre
                    ON(cob.credor_cre_cod = cre.cre_cod)
                    WHERE cob.cob_remocao IN (0) ORDER BY ina.ina_nome ASC;");
        $numDIVIDA = $query->row()->trow;
        
        $query = $this->db->query("SELECT COUNT(cob.cob_cod) AS trow
                    FROM cobrancas cob
                    INNER JOIN acordos aco
                    ON(cob.cob_cod = aco.cobranca_cob_cod)
                    INNER JOIN inadimplentes ina
                    ON(cob.inadimplentes_ina_cod = ina.ina_cod)
                    INNER JOIN credores cre
                    ON(cob.credor_cre_cod = cre.cre_cod)
                    WHERE cob.cob_remocao IN (0, 4) ORDER BY ina.ina_nome ASC;");
        $numACORDO = $query->row()->trow;
//        $query = $this->db->query("SELECT COUNT(prodcod) AS trow FROM produto P WHERE P.`proddest` = 1;");
        return ($numDIVIDA > $numACORDO)? $numDIVIDA: $numACORDO;
    }

    /* --- SELECTS TODAS AS MINHAS COBRAN�AS ADM OU AS COBRAN�AS ADM DE TODOS USU�RIOS --- */

    function getMinhasOuTodasCobrancasVirgensAdm($pesquisarPor, $minhaOuTodas) {
        $query = $this->db->query(
                //A SUBQUERY CAUSA LENTID�O
//                "SELECT div_cod, co.cob_cod, i.ina_nome, i.ina_cod, cre.cre_nome_fantasia, div_total,
//                        (SELECT OP.ope_nome FROM ros RO INNER JOIN operacoes OP ON(OP.ope_cod=RO.operacoes_ope_cod) WHERE RO.cobranca_cob_cod=co.cob_cod ORDER BY RO.ros_cod DESC LIMIT 1) AS ultimo_ro
                "SELECT div_cod, co.cob_cod, i.ina_nome, i.ina_cod, cre.cre_nome_fantasia, div_total,
                        ('') AS ultimo_ro
                           FROM dividas d
                             INNER JOIN cobrancas co
                               ON(d.cobranca_cob_cod = co.cob_cod AND(co.cob_remocao = 0))
                             INNER JOIN inadimplentes i
                               ON(co.inadimplentes_ina_cod = i.ina_cod)
                             INNER JOIN credores cre
                               ON(co.credor_cre_cod = cre.cre_cod)
                         WHERE (ina_nome LIKE '%$pesquisarPor%' OR cre_nome_fantasia LIKE '%$pesquisarPor%')
                           AND co.cob_setor = 'ADM'
                           AND d.div_virgem = '1'
                           $minhaOuTodas");
        return $query->result();
    }

    function getMinhasOuTodasCobrancasEmAndamentoAdm($pesquisarPor, $minhaOuTodas) {
        $query = $this->db->query(
                //A SUBQUERY CAUSA LENTID�O
//                "SELECT div_cod, co.cob_cod, i.ina_cod, i.ina_nome, cre.cre_nome_fantasia, div_total,
//                        (SELECT OP.ope_nome FROM ros RO INNER JOIN operacoes OP ON(OP.ope_cod=RO.operacoes_ope_cod) WHERE RO.cobranca_cob_cod=co.cob_cod ORDER BY RO.ros_cod DESC LIMIT 1) AS ultimo_ro
                "SELECT div_cod, co.cob_cod, i.ina_cod, i.ina_nome, cre.cre_nome_fantasia, div_total,
                        ('') AS ultimo_ro
                           FROM dividas d
                             INNER JOIN cobrancas co
                               ON(d.cobranca_cob_cod = co.cob_cod AND(co.cob_remocao = 0))
                             INNER JOIN inadimplentes i
                               ON(co.inadimplentes_ina_cod = i.ina_cod)
                             INNER JOIN credores cre
                               ON(co.credor_cre_cod = cre.cre_cod)
                         WHERE (ina_nome LIKE '%$pesquisarPor%' OR cre_nome_fantasia LIKE '%$pesquisarPor%')
                           AND co.cob_setor = 'ADM'
                           AND d.div_virgem = '0'
                           $minhaOuTodas");
        return $query->result();
    }

    function getMinhasOuTodasCobrancasEmAcordoAdm($pesquisarPor, $minhaOuTodas) {
        $query = $this->db->query(
                //A SUBQUERY CAUSA LENTID�O
//                "SELECT aco_cod, co.cob_cod, i.ina_cod, i.ina_nome, cr.cre_nome_fantasia, aco_valor_atualizado,
//                        (SELECT OP.ope_nome FROM ros RO INNER JOIN operacoes OP ON(OP.ope_cod=RO.operacoes_ope_cod) WHERE RO.cobranca_cob_cod=co.cob_cod ORDER BY RO.ros_cod DESC LIMIT 1) AS ultimo_ro
                "SELECT aco_cod, co.cob_cod, i.ina_cod, i.ina_nome, cr.cre_nome_fantasia, aco_valor_atualizado,
                        ('') AS ultimo_ro
                           FROM acordos ac
                             INNER JOIN cobrancas co
                               ON(ac.cobranca_cob_cod = co.cob_cod AND(co.cob_remocao = 0))
                             INNER JOIN inadimplentes i
                               ON(co.inadimplentes_ina_cod = i.ina_cod)
                             INNER JOIN credores cr
                               ON(co.credor_cre_cod = cr.cre_cod)
                         WHERE (ina_nome LIKE '%$pesquisarPor%' OR cre_nome_fantasia LIKE '%$pesquisarPor%')
                           AND aco_procedencia = 'ADM'
                           AND co.cob_setor = 'ADM'
                           $minhaOuTodas");
        return $query->result();
    }

    function getMinhasOuTodasCobrancasEmAtrasoAdm($pesquisarPor, $minhaOuTodas, $dataHoje) {
        $query = $this->db->query(
                "SELECT aco_cod, co.cob_cod, i.ina_cod, i.ina_nome, cr.cre_nome_fantasia, aco_total_parcelado as div_total, (SELECT PAA.paa_vencimento FROM par_acordos PAA WHERE PAA.acordos_aco_cod = aco_cod AND PAA.paa_situacao = '0' ORDER BY PAA.paa_vencimento ASC LIMIT 1) AS div_venc_inicial
                    FROM acordos A
                    INNER JOIN cobrancas co
                    ON (A.cobranca_cob_cod = co.cob_cod
                    AND (co.cob_remocao = 0))
                    INNER JOIN inadimplentes i
                    ON (co.inadimplentes_ina_cod = i.ina_cod)
                    INNER JOIN credores cr
                    ON (co.credor_cre_cod = cr.cre_cod)
                    WHERE (ina_nome LIKE '%$pesquisarPor%' OR cre_nome_fantasia LIKE '%$pesquisarPor%')
                    AND co.cob_setor = 'ADM'
                    AND (SELECT PAA.paa_vencimento FROM par_acordos PAA WHERE PAA.acordos_aco_cod = aco_cod AND PAA.paa_situacao = '0' ORDER BY PAA.paa_vencimento ASC LIMIT 1) <= '$dataHoje'
                    AND co.cob_remocao = '0' $minhaOuTodas");
        return $query->result();
    }

    /* --- FIM SELECTS TODAS AS MINHAS COBRAN�AS ADM OU AS COBRAN�AS ADM DE TODOS USU�RIOS --- */

    function getCobrancas($cre_cod) {
        $query = $this->db->query('select * from cobrancas where credor_cre_cod = ' . $cre_cod);
        return $query->result();
    }

    function remanejarPorCredor(
    $rec_novo, $cob_cod) {
        $this->db->where('cob_cod', $cob_cod);
        $dados = array(
            'usuarios_usu_cod' => $rec_novo
        );
        $this->db->update('cobrancas', $dados);
        //equivalente �
//        $query = "update cobrancas set usuarios_usu_cod = $rec_novo where cob_cod = $cob_cod";
    }
    
	function setProcesso($cob=0,$num='',$ano='',$forum='') {
		if (empty($cob)) return false;
		return $this->db->update('cobrancas', array(
			'processo_num'=>$num,
			'processo_ano'=>$ano,
			'processo_forum'=>$forum
		),array('cob_cod'=>$cob));
	}

	function setArquivamento($cob=0,$data='') {
		if (empty($cob)) return false;
		return $this->db->update('cobrancas', array('processo_arq_data'=>$data),array('cob_cod'=>$cob));
	}
	
	function setDesentranhamento($cob=0,$data='') {
		if (empty($cob)) return false;
		return $this->db->update('cobrancas', array('processo_des_data'=>$data),array('cob_cod'=>$cob));
	}	

    function getCobrancasDadosEnvJudDivida($cobCod) {
        $query = $this->db->query("SELECT *
                FROM cobrancas C
                INNER JOIN dividas D
                ON(D.cobranca_cob_cod = C.cob_cod)
                WHERE C.cob_cod = $cobCod");
        return $query->row();
    }

    function getCobrancasDadosEnvJudAcordos($cobCod) {
        $query = $this->db->query("SELECT *,(SELECT SUM(PA.paa_saldo) FROM par_acordos PA WHERE PA.acordos_aco_cod = A.aco_cod AND PA.paa_situacao = 0) AS total_remanescente
                FROM cobrancas C
                INNER JOIN acordos A
                ON(A.cobranca_cob_cod = C.cob_cod)
                INNER JOIN usuarios U ON (U.usu_cod = C.usuarios_usu_cod)
                WHERE C.cob_cod = $cobCod");
        return $query->row();
    }

    function getCodGerador($cod) {
        $query = $this->db->query("SELECT A.aco_cob_cod_geradora FROM acordos A WHERE A.aco_cod = $cod");
        return $query->row();
    }

    function getCodAcordoGerador($codGerador) {
        $query = $this->db->query("SELECT A.aco_cod FROM cobrancas C INNER JOIN acordos A ON(A.cobranca_cob_cod = C.cob_cod) WHERE C.cob_cod = $codGerador");
        return $query->row();
    }

    function getCodDividaGerador($codGerador) {
        
        $cods = explode('-', $codGerador);
        $filtro = '';
        
        foreach ($cods as $cod){
            $filtro .= $cod.' OR C.cob_cod = ';
        }
        $filtro = substr($filtro, 0, -16);
//        die('>>'.$filtro);
        
        $query = $this->db->query("SELECT D.div_cod FROM cobrancas C INNER JOIN dividas D ON(D.cobranca_cob_cod = C.cob_cod) WHERE C.cob_cod = $filtro");
        return $query->row();
    }

    function getCobrancaDadosDiv($divCod) {
        $query = $this->db->query("SELECT
                CR.cre_pessoa,
                CR.cre_nome_fantasia,
                CR.cre_cpf_cnpj,
                R.rep_nome,
                I.ina_pessoa,
                I.ina_nome,
                I.ina_cpf_cnpj,
                D.div_documento AS documento,
                D.div_banco AS banco,
                D.div_agencia AS agencia,
                D.div_alinea AS alinea,
                D.div_emissao AS emissao,
                D.div_info AS informacoes,
                U.usu_nome,
                C.cob_cod
                FROM dividas D
                INNER JOIN cobrancas C
                ON(C.cob_cod = D.cobranca_cob_cod)
                INNER JOIN credores CR
                ON(CR.cre_cod = C.credor_cre_cod)
                INNER JOIN inadimplentes I
                ON(I.ina_cod = C.inadimplentes_ina_cod)
                INNER JOIN repasses R
                ON(R.rep_cod = C.repasses_rep_cod)
                INNER JOIN usuarios U
                ON(U.usu_cod = C.usuarios_usu_cod)
                WHERE D.div_cod = '$divCod'");
        return $query->row();
    }

    function getCobrancaDadosAco($acoCod) {
        $query = $this->db->query("SELECT
                CR.cre_pessoa,
                CR.cre_nome_fantasia,
                CR.cre_cpf_cnpj,
                R.rep_nome,
                I.ina_pessoa,
                I.ina_nome,
                I.ina_cpf_cnpj,
                ('') AS documento,
                ('') AS banco,
                ('') AS agencia,
                ('') AS alinea,
                A.aco_emissao AS emissao,
                ('') AS informacoes,
                U.usu_nome,
                C.cob_cod
                FROM acordos A
                INNER JOIN cobrancas C
                ON(C.cob_cod = A.cobranca_cob_cod)
                INNER JOIN credores CR
                ON(CR.cre_cod = C.credor_cre_cod)
                INNER JOIN inadimplentes I
                ON(I.ina_cod = C.inadimplentes_ina_cod)
                INNER JOIN repasses R
                ON(R.rep_cod = C.repasses_rep_cod)
                INNER JOIN usuarios U
                ON(U.usu_cod = C.usuarios_usu_cod)
                WHERE A.aco_cod = '$acoCod'");
        return $query->row();
    }

    function getCobrancaParcelasDiv($divCod) {
        $query = $this->db->query("SELECT
                PD.pad_doc_num AS docnum,
                PD.pad_par_num AS parnum,
                PD.pad_valor AS parvalor,
                PD.pad_vencimento AS parvenc
                FROM par_dividas PD
                WHERE PD.dividas_div_cod = $divCod");
        return $query->result();
    }

    function getCobrancaParcelasAco($acoCod) {
        $query = $this->db->query("SELECT
                PA.paa_cod AS docnum,
                PA.paa_parcela AS parnum,
                PA.paa_saldo AS parvalor,
                PA.paa_vencimento AS parvenc
                FROM par_acordos PA
                WHERE PA.acordos_aco_cod = $acoCod");
        return $query->result();
    }

    function getParcelasDividaTeste($cobCod) {
        $query = $this->db->query("SELECT C.* FROM cobrancas C
                RIGHT JOIN dividas D ON(D.cobranca_cob_cod = C.cob_cod)
                RIGHT JOIN par_dividas PD ON(PD.dividas_div_cod = D.div_cod)
                WHERE C.cob_cod = '$cobCod' AND PD.pad_acordada = '0' GROUP BY C.cob_cod;
                ");
        return $query->row();
    }

    function getParcelasAcordoTeste($cobCod) {
        $query = $this->db->query("SELECT C.* FROM cobrancas C
                RIGHT JOIN acordos A ON(A.cobranca_cob_cod = C.cob_cod)
                RIGHT JOIN par_acordos PA ON(PA.acordos_aco_cod = A.aco_cod)
                WHERE C.cob_cod = '$cobCod' AND PA.paa_situacao = '0' GROUP BY C.cob_cod;
                ");
        return $query->row();
    }

    function getInfoUsuario($aux) {
        $query = $this->db->query("SELECT * FROM usuarios WHERE usu_cod = $aux");
        return $query->row();
    }

    function getRecibo($cod) {
        $query = $this->db->query("SELECT R.rec_cod FROM recibos R WHERE R.recebimentos_reb_cod = $cod");
        return $query->row();
    }

    function getDetalhesDividaExclusao($cod) {
        $query = $this->db->query("SELECT
              I.ina_nome,
              CR.cre_nome_fantasia
            FROM cobrancas C
              INNER JOIN inadimplentes I
                ON (I.ina_cod = C.inadimplentes_ina_cod)
              INNER JOIN credores CR
                ON (CR.cre_cod = C.credor_cre_cod)
            WHERE C.cob_cod = $cod");
        return $query->row();
    }
    
    function updAcordoMulta($aco_cod=0,$multa=0) {
    	if (empty($aco_cod)) return false;
        return $this->db->update('acordos', array('aco_multa_aplicada' => $multa), array('aco_cod' => $aco_cod));
    }
    function updAcordoProtocolo($aco_cod=0,$proto=0) {
    	if (empty($aco_cod)) return false;
        return $this->db->update('acordos', array('aco_multa_protocolada' => $proto), array('aco_cod' => $aco_cod));
    }

}

?>