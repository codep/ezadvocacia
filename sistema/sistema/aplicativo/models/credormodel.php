<?php
class Credormodel extends Model {

    var $table = ''; // TABELA PRINCIPAL
    
    function  __construct()
    {
        // Call the Model constructor
        parent::Model();
    }

    function getUsuariosNomeCod(){
        $query = $this->db->query('SELECT U.usu_cod, U.usu_usuario_sis FROM usuarios U WHERE U.usu_ativo = 1 ORDER BY U.usu_usuario_sis');
        return $query->result();
    }
    
    function getUsuariosNomeCodAtivos(){
        $query = $this->db->query('SELECT U.usu_cod, U.usu_usuario_sis FROM usuarios U WHERE U.usu_ativo = 1 ORDER BY U.usu_usuario_sis');
        return $query->result();
    }
    
    function getUsuariosNomeCodAtivosRemanejar(){
        $query = $this->db->query('SELECT U.usu_cod, U.usu_usuario_sis,U.usu_ativo FROM usuarios U WHERE usu_ativo = 1 ORDER BY U.usu_usuario_sis');
        return $query->result();
    }

    function getRepasses(){
        $query = $this->db->query('select * from repasses order by rep_cod');
        return $query->result();
    }

    function getCidades(){
        $query = $this->db->query('SELECT DISTINCT c.cre_cidade from credores c ORDER BY c.cre_cidade');
        return $query->result();
    }

    function getAtivos($codigo){
        $query = $this->db->query('SELECT FX_GET_ATIVOS('.$codigo.') as ativos;');
        return $query->row();
    }
    function getCredores($ini=0, $fim=0,$filtro=''){
//        echo "<pre>";
//        print_r('FILTRO: '.$filtro.' | INI: '.$ini.' | FIM: '.$fim);
//        die();   
        if($filtro == '' || $filtro == 'TODOS'){
        	if (empty($ini)) {
        		$query = $this->db->query('SELECT * FROM credores c where (c.cre_ativo=1 OR c.cre_ativo=2) ORDER BY c.cre_nome_fantasia');	
        	} else {
            	$query = $this->db->query('SELECT * FROM credores c where (c.cre_ativo=1 OR c.cre_ativo=2) ORDER BY c.cre_nome_fantasia LIMIT '.$ini.', '.$fim);
            }
        }else{
            $query = $this->db->query('SELECT * FROM credores c '.$filtro.' ORDER BY c.cre_nome_fantasia  LIMIT '.$ini.', '.$fim);
        }
        return $query->result();
    }	
	/*
    function getCredores($cidade){
        if($cidade == '' || $cidade == 'TODOS')
            $query = $this->db->query('SELECT * FROM credores c where c.cre_ativo = 1 ORDER BY c.cre_nome_fantasia');
        else
            $query = $this->db->query('SELECT * FROM credores c where c.cre_ativo = 1 AND c.cre_cidade = "'.$cidade.'" ORDER BY c.cre_nome_fantasia');
        return $query->result();
    }
	 */

    function getCredor($cod){
        $query = $this->db->query('SELECT * FROM credores c where c.cre_cod='.$cod);
        return $query->row();
    }

    function getCredorPorRec($reccod){
        $query = $this->db->query('SELECT c.cre_cod, c.cre_nome_fantasia FROM credores c where (c.cre_ativo=1 OR c.cre_ativo=2) AND c.usuarios_responsavel_cod='.$reccod);
        return $query->result();
    }

    function getEstados(){
        $query = $this->db->query('SELECT DISTINCT C.cid_estado FROM cidades C ORDER BY C.cid_estado ASC');
        return $query->result();
    }

    function getCidadesBrasil($uf){
        $query = $this->db->query('SELECT DISTINCT C.cid_nome FROM cidades C WHERE C.cid_estado="'.$uf.'" ORDER BY C.cid_nome ASC');
        return $query->result();
    }

    function getBancos(){
        $query = $this->db->query('SELECT * FROM bancos ORDER BY banco_nome ASC');
        return $query->result();
    }

    function insert($dados=array()){
        if($this->db->insert('credores', $dados)){
            return true;
        } else {

            return false;

        }
    }
    function getRepresentantes($cod){
        $query = $this->db->query('SELECT * FROM rep_legais RP WHERE RP.credor_cre_cod='.$cod);
        return $query->result();
    }

    function getRepassesCredor($cod){
        $query = $this->db->query('SELECT cre_repasses FROM credores c WHERE c.cre_cod='.$cod);
        return $query->row();
    }

    function remanejar($query_bd)
    {
        if($this->db->query($query_bd)) return true;
        else return false;
    }
	
    function getRows($cidade){
       
        if($cidade == '' || $cidade == 'TODOS'){
            $query = $this->db->query("SELECT COUNT(cre_cod) AS trow FROM credores");
        }else{
            $query = $this->db->query('SELECT COUNT(cre_cod) AS trow FROM credores c where c.cre_cidade LIKE "%'.$cidade.'%"');
        }
        return $query->row()->trow;
    }
	
	
    function getRelatorios(){
        $query = $this->db->query('SELECT CR.crerel_cod, CR.crerel_nome FROM credores_relatorios CR ORDER BY CR.crerel_nome ASC');
        return $query->result();
    }
	
}

?>