<?php
class Contatomodel extends Model {

    var $table = 'contatos'; // TABELA PRINCIPAL

    function  __construct()
    {
        // Call the Model constructor
        parent::Model();
    }

    function insert($dados=array()){
        
        if($this->db->insert($this->table, $dados)){

            return true;
            
        } else {

            return false;
            
        }

        
    }

    function getemail(){

        $query = $this->db->query('SELECT configemail FROM configuracao LIMIT 1');
        return $query->row()->configemail;
        
    }

}

?>