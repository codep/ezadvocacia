<?php

class Devolucaomodel extends Model {

    var $table = ''; // TABELA PRINCIPAL

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getListarDividas($filtro) {
        $dividasEAcordos = array('dividas' => ''); //vai receber as d�vidas e os acordos encontrados

        /* ------- Pega todas as divida ------- */
        $query = $this->db->query
                ("
                    SELECT cob.cob_cod, ina.ina_nome, cre.cre_nome_fantasia, di.div_emissao, di.div_cadastro, di.div_total
                    FROM cobrancas cob
                    INNER JOIN dividas di
                    ON(cob.cob_cod = di.cobranca_cob_cod)
                    INNER JOIN inadimplentes ina
                    ON(cob.inadimplentes_ina_cod = ina.ina_cod)
                    INNER JOIN credores cre
                    ON(cob.credor_cre_cod = cre.cre_cod)
                    WHERE $filtro cob.cob_remocao IN (0) ORDER BY ina.ina_nome ASC, cre.cre_nome_fantasia ASC;
                ");
        $dividas = $query->result();
        /* ------- fim Pega todas as divida ------- */
        return $dividasEAcordos = array('dividas' => $dividas);
    }

}

?>