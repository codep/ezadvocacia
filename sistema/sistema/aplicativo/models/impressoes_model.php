<?php

class Impressoes_model extends Model {

    var $table = ''; // TABELA PRINCIPAL

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getRecDados($cod){
//        $query = $this->db->query("SELECT R.rec_cod,RB.reb_tipo,R.rec_valor,R.rec_data,I.ina_nome, I.ina_cpf_cnpj,C.cre_razao_social,R.rec_obs,RB.reb_parcelas,U.usu_nome AS recebedor,
//                                    (SELECT U2.usu_nome FROM recibos R2 INNER JOIN recebimentos RB2 ON(RB2.reb_cod=R2.recebimentos_reb_cod) INNER JOIN usuarios U2 ON(U2.usu_usuario_sis=RB2.reb_baixador) WHERE R2.rec_cod='$cod') AS baixador
//                                    FROM recibos R
//                                    INNER JOIN inadimplentes I ON (I.ina_cod=R.inadimplentes_ina_cod)
//                                    INNER JOIN recebimentos RB ON(RB.reb_cod=R.recebimentos_reb_cod)
//                                    INNER JOIN credores C ON(C.cre_cod=RB.credor_cre_cod)
//                                    INNER JOIN usuarios U ON(U.usu_cod=RB.usuarios_usu_cod)
//                                    WHERE R.rec_cod='$cod'");
        $query = $this->db->query("SELECT R.rec_cod,RB.reb_tipo, R.rec_valor,R.rec_data, I.ina_nome, I.ina_cpf_cnpj, C.cre_razao_social, C.cre_nome_fantasia, R.rec_obs, RB.reb_parcelas, RB.reb_baixador AS baixador, AC.aco_procedencia as procedencia
                                    FROM recibos R
                                    INNER JOIN inadimplentes I ON (I.ina_cod=R.inadimplentes_ina_cod)
                                    INNER JOIN recebimentos RB ON(RB.reb_cod=R.recebimentos_reb_cod)
									INNER JOIN acordos AC ON (AC.aco_cod=RB.acordos_aco_cod)
                                    INNER JOIN credores C ON(C.cre_cod=RB.credor_cre_cod)
                                    INNER JOIN usuarios U ON(U.usu_cod=RB.usuarios_usu_cod)
                                    WHERE R.rec_cod='$cod'");
        return $query->row();
    }
    
    function getParcelasInfo($filtroParcelas){
        $query = $this->db->query("SELECT *
                                    FROM par_acordos PA
                                    $filtroParcelas");
        return $query->result();
    }

}

?>