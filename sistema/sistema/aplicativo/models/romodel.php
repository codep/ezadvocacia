<?php

class Romodel extends Model {

    var $table = ''; // TABELA PRINCIPAL

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getInfoCredor($cod) {
        $query = $this->db->query("SELECT CR.* FROM cobrancas C INNER JOIN credores CR ON (C.credor_cre_cod = CR.cre_cod) WHERE C.cob_cod=$cod");
        return $query->row();
    }

    function getInfoInadimplente($cod) {
        $query = $this->db->query("SELECT I.* FROM cobrancas C INNER JOIN inadimplentes I ON (I.ina_cod = C.inadimplentes_ina_cod) WHERE C.cob_cod=$cod");
        return $query->row();
    }

    function getRos($filtro='') {
    	$sql = "SELECT R.*,U.usu_nome,OP.* FROM cobrancas C 
                                    INNER JOIN ros R ON (R.cobranca_cob_cod=C.cob_cod) 
                                    INNER JOIN usuarios U ON (U.usu_cod = R.usuarios_usu_cod) 
                                    LEFT JOIN operacoes OP ON (OP.ope_cod = R.operacoes_ope_cod)
                                    $filtro GROUP BY R.ros_detalhe,R.ros_data,R.ros_hora ORDER BY R.ros_data DESC, R.ros_hora DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }
            
    function getAcordos($filtro) {
        $query = $this->db->query("SELECT
                                  C.cob_cod,
                                  A.aco_cob_cod_geradora
                                FROM cobrancas C
                                  INNER JOIN acordos A
                                    ON (C.cob_cod = A.cobranca_cob_cod) $filtro;");
        return $query->result();
    }

    function getOperacoes() {
        $query = $this->db->query("SELECT * FROM operacoes WHERE ope_automatica = 0");
        return $query->result();
    }

}

?>