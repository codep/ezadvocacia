<?php

class Recebimentos_model extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getCredores() {
        $query = $this->db->query(
                'SELECT cre_cod, cre_nome_fantasia
                           FROM credores ORDER BY cre_nome_fantasia ASC');
        return $query->result();
    }

    /* cada fun��o � usada nas abas (hoje, semana, mes, pr�ximo m�s) nas views rec_listar, rec_listar_todos e rec_listar_previs�es */

    /* -----IN�CIO SELECTS MEUS RECEBIMENTOS----- */

    function getMeusRecebimentosHoje($filtroCredor, $dataDeHoje) {
        $query = $this->db->query(
                "SELECT A.cobranca_cob_cod, I.ina_nome, C.cre_nome_fantasia, R.reb_data,R.reb_valor, R.reb_desconto,RB.rec_cod
                           FROM recebimentos R
                             INNER JOIN inadimplentes I
                               ON(I.ina_cod=R.inadimplentes_ina_cod)
                             INNER JOIN credores C
                               ON(C.cre_cod=R.credor_cre_cod) 
                             INNER JOIN acordos A
                               ON(A.aco_cod=R.acordos_aco_cod)
                               RIGHT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=R.reb_cod)
                             $filtroCredor
                                 AND R.reb_data = '$dataDeHoje'");
        return $query->result();
    }

    function getMeusRecebimentosSemana($filtroCredor, $primeiroDiaDaSemana, $ultimoDiaDaSemana) {
        $query = $this->db->query(
                "SELECT A.cobranca_cob_cod, I.ina_nome, C.cre_nome_fantasia, R.reb_data,R.reb_valor, R.reb_desconto,RB.rec_cod
                           FROM recebimentos R
                             INNER JOIN inadimplentes I
                               ON(I.ina_cod=R.inadimplentes_ina_cod)
                             INNER JOIN credores C
                               ON(C.cre_cod=R.credor_cre_cod) 
                             INNER JOIN acordos A
                               ON(A.aco_cod=R.acordos_aco_cod)
                               RIGHT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=R.reb_cod)
                                $filtroCredor
                                 AND R.reb_data >= '$primeiroDiaDaSemana'
                                 AND R.reb_data<= '$ultimoDiaDaSemana' ");
        return $query->result();
    }

    function getMeusRecebimentosMes($filtroCredor, $mesAtual, $anoAtual) {
        $query = $this->db->query(
                "SELECT A.cobranca_cob_cod, I.ina_nome, C.cre_nome_fantasia, R.reb_data,R.reb_valor, R.reb_desconto,RB.rec_cod
                           FROM recebimentos R
                             INNER JOIN inadimplentes I
                               ON(I.ina_cod=R.inadimplentes_ina_cod)
                             INNER JOIN credores C
                               ON(C.cre_cod=R.credor_cre_cod)
                             INNER JOIN acordos A
                               ON(A.aco_cod=R.acordos_aco_cod)
                                RIGHT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=R.reb_cod) $filtroCredor
                                 AND R.reb_data >= '$anoAtual-$mesAtual-1'
                                 AND R.reb_data<= '$anoAtual-$mesAtual-31'");
        return $query->result();
    }

    function getMeusRecebimentosProximoMes($filtroCredor, $proximoMes, $anoAtual) {
        $query = $this->db->query(
                "SELECT A.cobranca_cob_cod, I.ina_nome, C.cre_nome_fantasia, R.reb_data,R.reb_valor, R.reb_desconto,RB.rec_cod
                           FROM recebimentos R
                             INNER JOIN inadimplentes I
                               ON(I.ina_cod=R.inadimplentes_ina_cod)
                             INNER JOIN credores C
                               ON(C.cre_cod=R.credor_cre_cod)
                             INNER JOIN acordos A
                               ON(A.aco_cod=R.acordos_aco_cod)
                                RIGHT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=R.reb_cod)
                                $filtroCredor
                                 AND R.reb_data >= '$anoAtual-$proximoMes-1'
                                 AND R.reb_data<= '$anoAtual-$proximoMes-31'");
        return $query->result();
    }

    /* ----- FIM MEUS RECEBIMENTOS ----- */


    /* ----- INICIO TODOS OS RECEBIMENTOS ----- */

    function getTodosRecebimentosHoje($filtroCredor, $dataDeHoje) {
        $query = $this->db->query(
                "SELECT A.cobranca_cob_cod, I.ina_nome, C.cre_nome_fantasia, R.reb_data, R.usuarios_usu_cod, R.reb_valor, R.reb_desconto, U.usu_nome AS recuperador, RB.rec_cod , R.reb_baixador
                         FROM recebimentos R
                           INNER JOIN inadimplentes I
                             ON(I.ina_cod=R.inadimplentes_ina_cod)
                           INNER JOIN credores C
                             ON(C.cre_cod=R.credor_cre_cod)
                           INNER JOIN usuarios U
                             ON(U.usu_cod=R.usuarios_usu_cod)
                             RIGHT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=R.reb_cod)
                           INNER JOIN acordos A
                               ON(A.aco_cod = R.acordos_aco_cod) 
                        $filtroCredor = '$dataDeHoje'
                          ORDER BY C.cre_nome_fantasia ASC");
        return $query->result();
    }

    function getTodosRecebimentosSemana($filtroCredor, $primeiroDiaDaSemana, $ultimoDiaDaSemana) {
        $query = $this->db->query(
                "SELECT A.cobranca_cob_cod, I.ina_nome, C.cre_nome_fantasia, R.reb_data, R.usuarios_usu_cod, R.reb_valor, R.reb_desconto, U.usu_nome AS recuperador, R.reb_baixador,RB.rec_cod
                           FROM recebimentos R
                             INNER JOIN inadimplentes I
                               ON(I.ina_cod=R.inadimplentes_ina_cod)
                             INNER JOIN credores C
                               ON(C.cre_cod=R.credor_cre_cod)
                             INNER JOIN usuarios U
                               ON(U.usu_cod=R.usuarios_usu_cod)
                               RIGHT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=R.reb_cod)
                             INNER JOIN acordos A
                               ON(A.aco_cod = R.acordos_aco_cod)
                         $filtroCredor >= '$primeiroDiaDaSemana'
                           AND R.reb_data <= '$ultimoDiaDaSemana'
                             ORDER BY R.reb_data ASC, C.cre_nome_fantasia ASC");
        return $query->result();
    }

    function getTodosRecebimentosMes($filtroCredor, $mesAtual, $anoAtual) {
        $query = $this->db->query(
                "SELECT A.cobranca_cob_cod, I.ina_nome, C.cre_nome_fantasia, R.reb_data, R.usuarios_usu_cod, R.reb_valor, R.reb_desconto,U.usu_nome AS recuperador, R.reb_baixador,RB.rec_cod
                           FROM recebimentos R
                             INNER JOIN inadimplentes I
                               ON(I.ina_cod=R.inadimplentes_ina_cod)
                             INNER JOIN credores C
                               ON(C.cre_cod=R.credor_cre_cod)
                             INNER JOIN usuarios U
                               ON(U.usu_cod=R.usuarios_usu_cod)
                               RIGHT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=R.reb_cod)
                             INNER JOIN acordos A
                               ON(A.aco_cod = R.acordos_aco_cod)
                         $filtroCredor >= '$anoAtual-$mesAtual-01'
                        AND R.reb_data <= '$anoAtual-$mesAtual-31'
                          ORDER BY R.reb_data, C.cre_nome_fantasia ASC");
        return $query->result();
    }

    function getTodosRecebimentosProxMes($filtroCredor, $proximoMes, $anoAtual) {
        $query = $this->db->query(
                "SELECT A.cobranca_cob_cod, I.ina_nome, C.cre_nome_fantasia, R.reb_data, R.usuarios_usu_cod, R.reb_valor, R.reb_desconto,U.usu_nome AS recuperador, R.reb_baixador,RB.rec_cod
                           FROM recebimentos R
                             INNER JOIN inadimplentes I
                               ON(I.ina_cod=R.inadimplentes_ina_cod)
                             INNER JOIN credores C
                               ON(C.cre_cod=R.credor_cre_cod)
                             INNER JOIN usuarios U
                               ON(U.usu_cod=R.usuarios_usu_cod)
                               RIGHT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=R.reb_cod)
                             INNER JOIN acordos A
                               ON(A.aco_cod = R.acordos_aco_cod)
                         $filtroCredor >= '$anoAtual-$proximoMes-01'
                        AND R.reb_data <= '$anoAtual-$proximoMes-31'
                          ORDER BY R.reb_data, C.cre_nome_fantasia");
        return $query->result();
    }

    /* |^ -----FIM TODOS RECEBIMENTOS----- |^ */


    /*  ----- IN�CIO SELECTS PREVIS�ES ----- */

    function getPrevisoesHoje($filtroCredor, $dataDeHoje) {
        $query = $this->db->query(
                "SELECT
                          A.cobranca_cob_cod,
                          I.ina_nome,
                          cr.cre_nome_fantasia,
                          PA.paa_vencimento,
                          U.usu_nome           AS recuperador,
                          PA.paa_valor
                        FROM par_acordos PA
                          INNER JOIN acordos A
                            ON (A.aco_cod = PA.acordos_aco_cod)
                          INNER JOIN cobrancas C
                            ON (C.cob_cod = A.cobranca_cob_cod)
                          INNER JOIN inadimplentes I
                            ON (I.ina_cod = C.inadimplentes_ina_cod)
                          INNER JOIN credores cr
                            ON (cr.cre_cod = C.credor_cre_cod)
                          INNER JOIN usuarios U
                            ON (U.usu_cod = C.usuarios_usu_cod)
                        WHERE PA.paa_vencimento = '$dataDeHoje'
                        AND PA.paa_situacao = 0
                        $filtroCredor
                        ORDER BY PA.paa_vencimento ASC, C.credor_cre_cod ASC;");
        return $query->result();
    }

    function getPrevisoesSemana($filtroCredor, $primeiroDiaDaSemana, $ultimoDiaDaSemana) {
        $query = $this->db->query(
                "SELECT
                          A.cobranca_cob_cod,
                          I.ina_nome,
                          cr.cre_nome_fantasia,
                          PA.paa_vencimento,
                          U.usu_nome           AS recuperador,
                          PA.paa_valor
                        FROM par_acordos PA
                          INNER JOIN acordos A
                            ON (A.aco_cod = PA.acordos_aco_cod)
                          INNER JOIN cobrancas C
                            ON (C.cob_cod = A.cobranca_cob_cod)
                          INNER JOIN inadimplentes I
                            ON (I.ina_cod = C.inadimplentes_ina_cod)
                          INNER JOIN credores cr
                            ON (cr.cre_cod = C.credor_cre_cod)
                          INNER JOIN usuarios U
                            ON (U.usu_cod = C.usuarios_usu_cod)
                        WHERE PA.paa_vencimento >= '$primeiroDiaDaSemana'
                           AND PA.paa_vencimento <= '$ultimoDiaDaSemana'
                           AND PA.paa_situacao = 0
                        $filtroCredor
                        ORDER BY PA.paa_vencimento ASC, C.credor_cre_cod ASC;
                ");
        return $query->result();
    }

    function getPrevisoesMes($filtroCredor, $mesAtual, $anoAtual) {
        $query = $this->db->query(
                "SELECT
                          A.cobranca_cob_cod,
                          I.ina_nome,
                          cr.cre_nome_fantasia,
                          PA.paa_vencimento,
                          U.usu_nome           AS recuperador,
                          PA.paa_valor
                        FROM par_acordos PA
                          INNER JOIN acordos A
                            ON (A.aco_cod = PA.acordos_aco_cod)
                          INNER JOIN cobrancas C
                            ON (C.cob_cod = A.cobranca_cob_cod)
                          INNER JOIN inadimplentes I
                            ON (I.ina_cod = C.inadimplentes_ina_cod)
                          INNER JOIN credores cr
                            ON (cr.cre_cod = C.credor_cre_cod)
                          INNER JOIN usuarios U
                            ON (U.usu_cod = C.usuarios_usu_cod)
                        WHERE PA.paa_vencimento >= '$anoAtual-$mesAtual-01'
                           AND PA.paa_vencimento <= '$anoAtual-$mesAtual-31'
                            AND PA.paa_situacao = 0
                        $filtroCredor
                        ORDER BY PA.paa_vencimento ASC, C.credor_cre_cod ASC;");
        return $query->result();
    }

    function getPrevisoesProxMes($filtroCredor, $proximoMes, $anoAtual) {
        $query = $this->db->query(
                "SELECT
                          A.cobranca_cob_cod,
                          I.ina_nome,
                          cr.cre_nome_fantasia,
                          PA.paa_vencimento,
                          U.usu_nome           AS recuperador,
                          PA.paa_valor
                        FROM par_acordos PA
                          INNER JOIN acordos A
                            ON (A.aco_cod = PA.acordos_aco_cod)
                          INNER JOIN cobrancas C
                            ON (C.cob_cod = A.cobranca_cob_cod)
                          INNER JOIN inadimplentes I
                            ON (I.ina_cod = C.inadimplentes_ina_cod)
                          INNER JOIN credores cr
                            ON (cr.cre_cod = C.credor_cre_cod)
                          INNER JOIN usuarios U
                            ON (U.usu_cod = C.usuarios_usu_cod)
                        WHERE PA.paa_vencimento >= '$anoAtual-$proximoMes-01'
                           AND PA.paa_vencimento <= '$anoAtual-$proximoMes-31'
                            AND PA.paa_situacao = 0
                        $filtroCredor
                        ORDER BY PA.paa_vencimento ASC, C.credor_cre_cod ASC;");
        return $query->result();
    }

    /* ----- FIM SELECTS PREVIS�ES ----- */
}

?>