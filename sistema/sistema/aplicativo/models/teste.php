<?php
class Credormodel extends Model {

    var $table = ''; // TABELA PRINCIPAL
    
    function  __construct()
    {
        // Call the Model constructor
        parent::Model();
    }

    function getUsuariosNomeCod(){
        $query = $this->db->query('SELECT U.usu_cod, U.usu_usuario_sis FROM usuarios U ORDER BY U.usu_usuario_sis');
        return $query->result();
    }

    function getRepasses(){
        $query = $this->db->query('select * from repasses order by rep_cod');
        return $query->result();
    }

    function getCidades(){
        $query = $this->db->query('SELECT DISTINCT c.cre_cidade from credores c ORDER BY c.cre_cidade');
        return $query->result();
    }

    function getAtivos($codigo){
        $query = $this->db->query('SELECT FX_GET_ATIVOS('.$codigo.') as ativos;');
        return $query->row();
    }
    function getCredores($cidade){
        if($cidade == '' || $cidade == 'TODOS')
            $query = $this->db->query('SELECT * FROM credores c where c.cre_ativo = 1');
        else
            $query = $this->db->query('SELECT * FROM credores c where c.cre_ativo = 1 AND c.cre_cidade = "'.$cidade.'"');
        return $query->result();
    }

    function getCredor($cod){
        $query = $this->db->query('SELECT * FROM credores c where c.cre_ativo=1 AND c.cre_cod='.$cod);
        return $query->row();
    }

    function getEstados(){
        $query = $this->db->query('SELECT DISTINCT C.cid_estado FROM cidades C ORDER BY C.cid_estado ASC');
        return $query->result();
    }

    function getCidadesBrasil($uf){
        $query = $this->db->query('SELECT DISTINCT C.cid_nome FROM cidades C WHERE c.cid_estado="'.$uf.'" ORDER BY C.cid_nome ASC');
        return $query->result();
    }

    function getBancos(){
        $query = $this->db->query('SELECT * FROM bancos ORDER BY banco_nome ASC');
        return $query->result();
    }

    function insert($dados=array()){
        if($this->db->insert('credores', $dados)){
            return true;
        } else {

            return false;

        }
    }
    function getRepresentantes($cod){
        $query = $this->db->query('SELECT * FROM rep_legais RP WHERE RP.credor_cre_cod='.$cod);
        return $query->result();
    }

    function getRepassesCredor($cod){
        $query = $this->db->query('SELECT cre_repasses FROM credores c WHERE c.cre_cod='.$cod);
        return $query->row();
    }
}

?>