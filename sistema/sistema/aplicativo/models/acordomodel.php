<?php

class Acordomodel extends Model {

    var $table = ''; // TABELA PRINCIPAL

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getCobDados($cod) {
        $query = $this->db->query("SELECT
                                  I.ina_nome,
                                  CR.cre_juros,
                                  CR.cre_honorario,
                                  C.repasses_rep_cod,
                                  I.ina_cod,
                                  CR.cre_cod,
                                  C.usuarios_usu_cod,
                                  C.cob_prazo,
                                  C.cob_cod,
                                  C.cob_setor,
                                  C.cob_subsetor
                                FROM cobrancas C
                                  INNER JOIN inadimplentes I
                                    ON (I.ina_cod = C.inadimplentes_ina_cod)
                                  INNER JOIN credores CR
                                    ON (CR.cre_cod = C.credor_cre_cod)
                                  INNER JOIN repasses R
                                    ON (R.rep_cod = C.repasses_rep_cod)
                                WHERE C.cob_cod = $cod ");
        return $query->row();
    }
    
    function getCobDadosAcoJud($cod) {
        $query = $this->db->query("SELECT 
                                    I.ina_nome,
                                    CR.cre_juros,
                                    CR.cre_honorario,
                                    C.repasses_rep_cod,
                                    I.ina_cod,
                                    CR.cre_cod,
                                    C.usuarios_usu_cod,
                                    C.cob_prazo,
                                    C.cob_cod,
                                    AC.`aco_cob_cod_geradora`,
                                    AC.`aco_valor_original`,
	                                  C.cob_setor,
	                                  C.cob_subsetor
                                    FROM
                                    cobrancas C 
                                    INNER JOIN inadimplentes I 
                                        ON (
                                        I.ina_cod = C.inadimplentes_ina_cod
                                        ) 
                                    INNER JOIN credores CR 
                                        ON (CR.cre_cod = C.credor_cre_cod) 
                                    INNER JOIN repasses R 
                                        ON (R.rep_cod = C.repasses_rep_cod) 
                                    INNER JOIN `acordos` AC 
                                        ON (
                                        AC.`cobranca_cob_cod` = C.cob_cod
                                        ) 
                                    WHERE C.cob_cod = $cod ");
        return $query->row();
    }

    function getParcDados($filtro) {
        $query = $this->db->query("SELECT *
                                    FROM par_dividas PD
                                    $filtro
                                    ORDER BY PD.pad_vencimento asc");
        return $query->result();
    }
    
    function getParcDadosAC($filtro) {
        $query = $this->db->query("SELECT *
                                    FROM par_acordos PD
                                    $filtro
                                    ORDER BY PD.paa_vencimento asc");
        return $query->result();
    }

    function getCobCod($filtro) {
        $query = $this->db->query("SELECT C.cob_cod
                                FROM par_dividas PD
                                  INNER JOIN dividas D
                                    ON (D.div_cod = PD.dividas_div_cod)
                                  INNER JOIN cobrancas C
                                    ON (C.cob_cod = D.cobranca_cob_cod)
                                $filtro
                                GROUP BY C.cob_cod");
        return $query->row();
    }
    
    function getCobCodAC($filtro) {
        $query = $this->db->query("SELECT C.cob_cod
                                FROM par_acordos PD
                                  INNER JOIN acordos A
                                    ON (A.aco_cod = PD.acordos_aco_cod)
                                  INNER JOIN cobrancas C
                                    ON (C.cob_cod = A.cobranca_cob_cod)
                                $filtro
                                GROUP BY C.cob_cod");
        return $query->row();
    }
    
    function getCobCad($sync) {
        $query = $this->db->query("SELECT C.cob_cod FROM cobrancas C WHERE C.cob_sync='$sync'");
        return $query->row();
    }
    function getAcoCad($cod) {
        $query = $this->db->query("SELECT A.aco_cod FROM acordos A WHERE A.cobranca_cob_cod='$cod'");
        return $query->row();
    }
    
    function getParcelasAcoRecemEnviados($cobCod){
        $query = $this->db->query("SELECT PA.*
                                    FROM cobrancas C
                                      INNER JOIN acordos A
                                        ON (C.cob_cod = A.cobranca_cob_cod)
                                      INNER JOIN par_acordos PA
                                        ON (PA.acordos_aco_cod = A.aco_cod)
                                        WHERE C.cob_cod = $cobCod");
        return $query->row();
    }
    
    function getParcelasDividaTeste($cobCod){
        $query = $this->db->query("SELECT C.* FROM cobrancas C 
                                    RIGHT JOIN dividas D ON (D.cobranca_cob_cod = C.cob_cod) 
                                    RIGHT JOIN par_dividas PD ON (PD.dividas_div_cod = D.div_cod)
                                    WHERE C.cob_cod = '$cobCod' AND PD.pad_acordada = '0' GROUP BY C.cob_cod;");
        return $query->row();
    }
    function getParcelasAcordoTeste($cobCod){
        $query = $this->db->query("SELECT C.* FROM cobrancas C 
                                    RIGHT JOIN acordos A ON (A.cobranca_cob_cod = C.cob_cod) 
                                    RIGHT JOIN par_acordos PA ON (PA.acordos_aco_cod = A.aco_cod)
                                    WHERE C.cob_cod = '$cobCod' AND PA.paa_situacao = '0' GROUP BY C.cob_cod;");
        return $query->row();
    }
}

?>