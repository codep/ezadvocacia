<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php echo $title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta name="robots" content="Follow" />
        <meta name="skype_toolbar" content="skype_toolbar_parser_compatible" />

        <script type="text/javascript" src="<?php echo base_url(); ?>sistema/aplicativo/jscript/jquery1.11.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>sistema/aplicativo/jscript/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>sistema/aplicativo/jscript/plugin/jquery.flash.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>sistema/aplicativo/jscript/jquery.funcoes.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>sistema/aplicativo/jscript/jquery-ui-1.8.custom.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>sistema/aplicativo/style/css/base.css" />
        

        <?php echo $csshtml; ?>
        <?php echo $jshtml; ?>

		<script type="text/javascript">
			$(document).ready(function () {
				style_path = "../style/css/colors";

				if ($.datepicker!==undefined) $("#date-picker").datepicker();

				$("#box-tabs, #box-left-tabs").tabs();
				
				if ($('.dataMascara').length >0) $('.dataMascara').mask('99/99/9999');
				if ($('.data').length >0) $('.data').mask('99/99/9999');

			});
		</script>
    </head>
       
    <body>
    	<?php /*
        <center>
            <?php echo 'TEMPO DECORRIDO: '.$this->benchmark->elapsed_time();?>    
            <br/>
            <?php echo 'MEM�RIA CONSUMIDA: '.$this->benchmark->memory_usage();?>    
        </center>
		 */ ?>
        <?php echo $header; ?>
        <?php echo $conteudo; ?>
        <?php echo $footer; ?>
        
        <script type="text/javascript" src="<?php echo base_url(); ?>sistema/aplicativo/jscript/plugin/jquery.maskedinput.1.3.1.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>sistema/aplicativo/jscript/datepicker-pt-BR.js"></script>
        
    </body>
</html>