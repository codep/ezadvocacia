<script type="text/javascript">
    $(document).ready(function(){
        //AO CLICAR EM PESSOA JURIDICA
        $('input[name="rbPesquiPor"][value="fone"]').click(function() {
            
            $('#buscaInput').html('<input style="width: 163px;" type="text" id="input-medium-br" name="inputBusRap"/>');
            
            //$('input[name="inputBusRap"]').mask('(99)9999-9999');
			$('input[name="inputBusRap"]').mask("(99)9999-9999?9").blur(function(){
			    var phone, element;
			    element = $(this);
			    element.unmask();
			    phone = element.val().replace(/\D/g, '');
			    if(phone.length > 10) {
			        element.mask("(99)99999-999?9");
			    } else {
			        element.mask("(99)9999-9999?9");
			    }
			    //element.unmask();
			});
        
        /*
			// Bind no input e propertychange para pegar ctrl-v
			// e outras formas de input
			$('input[name="inputBusRap"]').bind('input propertychange',function(){
			    // pego o valor do telefone
			    var texto = $(this).val();
			    // Tiro tudo que n�o � numero
			    texto = texto.replace(/[^\d]/g, '');
			    // Se tiver alguma coisa
			    if (texto.length > 0)
			    {
			    // Ponho o primeiro parenteses do DDD    
			    texto = "(" + texto;
			 
			        if (texto.length > 3)
			        {
			            // Fecha o parenteses do DDD
			            texto = [texto.slice(0, 3), ")", texto.slice(3)].join('');  
			        }
			        if (texto.length > 11)
			        {      
			            // Se for 13 digitos ( DDD + 9 digitos) ponhe o tra�o no quinto digito            
			            if (texto.length > 12) 
			                texto = [texto.slice(0, 9), "-", texto.slice(9)].join('');
			            else
			             // Se for 12 digitos ( DDD + 8 digitos) ponhe o tra�o no quarto digito
			                texto = [texto.slice(0, 8), "-", texto.slice(8)].join('');
			        }   
			            // N�o adianta digitar mais digitos!
			            if (texto.length > 14)                
			               texto = texto.substr(0,15);
			    }
			    // Retorna o texto
			   $(this).val(texto);     
			})
			$('input[name="inputBusRap"]').attr('value', '');
		*/            
        });
		
        $('input[name="rbPesquiPor"][value="parente"]').click(function() {

            $('#buscaInput').html('<input style="width: 163px;" type="text" id="input-medium-br" name="inputBusRap"/>');


            $('input[name="cpf_cnpj"]').attr('value', '');
        });
    });
</script>
<div id="left">
<img src="<?php echo $img.'logoez.jpg'?>" alt="EZ Advocacia" style="margin: 8px 0 0 10px; width: 190px"/>
    <?php
//    Devdo ao fato de n�o caber mais dados na sess�o toda a logica empregada na
//    gera��o da sidebar ser�o tratadas aqui
    //------------------------------------------------------------------------------
    //primeiramente pega as strings da sess�o referentes as permiss�es de usuarios
    // e apartir delas monta 3 arrays temporarias para segurar as permiss�es
    $temp = explode(",", $this->session->userdata('menu1'));
    $temp2 = explode(",", $this->session->userdata('menu2'));
    $temp3 = explode(",", $this->session->userdata('menu3'));
    //para cada indice do array de temporario nivel 1, busca-se no banco as
    //informa��es referentes ao menu
    foreach ($temp as $t) {
        if ($t != '') {
            $sql = "SELECT M.men_cod,M.men_titulo,M.men_url FROM menus M WHERE M.men_cod=$t";
            $queryaux = $this->db->query($sql);
            //cria um aarray de menus, que s�o os menus de primeiro nivel
            $menus[] = array(
                'cod' => $queryaux->row()->men_cod,
                'titulo' => utf8_decode($queryaux->row()->men_titulo),
                'url' => $queryaux->row()->men_url
            );
        }
    }
    //para cada menu, � buscao os submenus pertencentes a ele atraves do la�o abaixo
    foreach ($menus as $menu) {
        //pega a array temporaria 2 e busca pelos submenus contidos nela
        foreach ($temp2 as $t) {
            if ($t != '') {
                $sql = "SELECT M.men_cod,M.men_titulo,M.men_url
                    FROM menus M WHERE M.men_cod=$t AND M.men_menu=" . $menu['cod'];
                $queryaux = $this->db->query($sql);
                if ($queryaux->num_rows > 0) {
                    //monta uma array de submenus com o indice referente ao
                    //menu que cada um deles pertence
                    $submenus[$menu['cod']][] = array(
                        'cod' => $queryaux->row()->men_cod,
                        'titulo' => utf8_decode($queryaux->row()->men_titulo),
                        'url' => $queryaux->row()->men_url
                    );
                }
            }
        }
    }
    //agora percorre os submenus de cada menu e seleciona os menus terciarios
    //que pertencem a cada um dos submenus
    foreach ($menus as $menu) {
        if (isset($submenus[$menu['cod']])) {
            foreach ($submenus[$menu['cod']] as $submenu) {
                foreach ($temp3 as $t) {
                    if ($t != '') {
                        $sql = "SELECT M.men_cod,M.men_titulo,M.men_url
                            FROM menus M WHERE M.men_cod=$t AND M.men_menu=" . $menu['cod'] . "
                            AND M.men_submenu=" . $submenu['cod'];
                        $queryaux = $this->db->query($sql);
                        if ($queryaux->num_rows > 0) {
                            //monta uma array com os menus terciarios de daca submenu
                            $termenus[$submenu['cod']][] = array(
                                'cod' => $queryaux->row()->men_cod,
                                'titulo' => utf8_decode($queryaux->row()->men_titulo),
                                'url' => $queryaux->row()->men_url
                            );
                        }
                    }
                }
            }
        }
    }
    //------------------------------------------------------------------------------
    $menusel = $this->session->userdata('menusel');
//    com tudo montado  faz um la�o para montar o html de cada menu que o usu�rio tem permiss�o
    echo '<div id="menu">';
    foreach ($menus as $menu) {
        if ($menusel == $menu['cod']) {
            echo '<h6 class="selected" id="h-menu-' . str_replace(" ", "", $menu['titulo']) . '"><a href="#' . str_replace(" ", "", $menu['titulo']) . '"><span>' . $menu['titulo'] . '</span></a></h6>';
            echo '<ul id="menu-' . str_replace(" ", "", $menu['titulo']) . '" class="opened">';
        } else {
            echo '<h6 id="h-menu-' . str_replace(" ", "", $menu['titulo']) . '"><a href="#' . str_replace(" ", "", $menu['titulo']) . '"><span>' . $menu['titulo'] . '</span></a></h6>';
            echo '<ul id="menu-' . str_replace(" ", "", $menu['titulo']) . '" class="closed">';
        }


        if (isset($submenus[$menu['cod']])) {
            foreach ($submenus[$menu['cod']] as $submenu) {
                if (isset($termenus[$submenu['cod']])) {
                    //echo '<li><a href="' . base_url() . $submenu['url'] . '">' . $submenu['titulo'] . '</a></li>';
                    echo '<li class="collapsible">';
                    echo '<a class="plus">' . $submenu['titulo'] . '</a>';
                    foreach ($termenus[$submenu['cod']] as $termenu) {
                        echo '<ul class="collapsed">';
                        echo '<li class="last"><a href="' . base_url() . $termenu['url'] . '">' . $termenu['titulo'] . '</a></li>';
                        echo '</ul>';
                    }
                    echo '</li>';
                } else {
                    echo '<li><a href="' . base_url() . $submenu['url'] . '">' . $submenu['titulo'] . '</a></li>';
                }
            }
        }
        echo '</ul>';
    }
    echo '</div>';
    ?>
    
    <div id="date-picker"></div>
    <div>
        <div style="border-bottom: 1px solid #CDCDCD; border-top: 1px solid #CDCDCD;" class="box">
            <form id="form" action="<?php echo base_url() . 'pesquisar/buscaRapida' ?>" method="post">
                <div class="form">
                    <div class="fields">
                        <div style="border: none; padding: 0px; margin: 0px;"  class="field  field-first">
                            <div class="divleftlast" style="width: 116px; margin: 0px; padding: 0px;">
                                <div style="padding-left: 0px;" class="label">
                                    <label>Busca R�pida:</label>
                                </div>
                            </div>
                        </div>
                        <div style="padding-top: 3px; height: 70px;" class="field">
                            <div class="divleft" style="width: 71px; border: none;">
                                <div class="input">
                                    <input type="radio" name="rbPesquiPor" value="fone" style="display: block; float: left; clear: both; margin-right: 5px;" />Por Telefone<br/><br/>
                                    <input type="radio" name="rbPesquiPor" value="parente" style="display: block; float: left; clear: both; margin-bottom: 5px; margin-right: 5px;" />Por Parente<br/>
                                    <div id="buscaInput">
                                        <input style="width: 163px;" type="text" id="input-medium-br" name="inputBusRap"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <div style="margin: 0 auto;" class="highlight">
                                <input type="submit" name="submit.highlight" value="Buscar" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>