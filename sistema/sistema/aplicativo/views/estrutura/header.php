<?php
        $codUsuariDaSessao=$this->session->userdata('usucod');
        if($codUsuariDaSessao==''){
            $codUsuariDaSessao=0;
        }

        $sql = "SELECT COUNT(men_cod) AS trow FROM mensagens M WHERE M.usuarios_usu_cod = $codUsuariDaSessao AND M.men_lida='0'";
        $query = $this->db->query($sql);
        $total_mensagens_recebidas = $query->row()->trow;
		
        $sql = "SELECT COUNT(C.cre_cod) as trow FROM mensagens_credores M INNER JOIN credores C ON (C.cre_cod = M.men_remetente) WHERE M.usuarios_usu_cod = $codUsuariDaSessao AND M.men_lida='0'";
        $query = $this->db->query($sql);
        $total_mensagens_credores = $query->row()->trow;

?>
<div id="header">
            <div id="header-outer">
	            <div id="home_user">
	            	<p>Ol&aacute; <b><?php echo $this->session->userdata('usunome'); ?></b></p>
	            	<span><?php echo dataex(date('Y-m-d')); ?></span>
	            </div>
                <div id="header-inner">
                    <div id="home">
                        <a href="<?php echo base_url() ?>"></a>
                    </div>
                    <ul id="quick">
                        <li>
                            <a href="<?php echo base_url().'agenda/listar' ?>" title="Agenda"><span class="icon"><img src="<?php echo $img.'icons/calendar.png'?>" alt="Agenda" /></span><span>Agenda</span></a>
                            <ul>
                                <li><a href="<?php echo base_url().'agenda/incluir' ?>">Nova tarefa</a></li>
                                <li>
                                    <a href="<?php echo base_url().'agenda/listar' ?>">Visualizar tarefas</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'mensagens/entrada' ?>" title="Mensagens"><span class="icon"><img src="<?php echo $img.'icons/page_white_copy.png'?>" alt="Mensagens" /></span><span>Mensagens(<?php echo $total_mensagens_recebidas+$total_mensagens_credores; ?>)</span></a>
                            <ul>
                            <li><a href="<?php echo base_url().'mensagens/nova' ?>">Nova Mensagem</a></li>
                            <li><a href="<?php echo base_url().'mensagens/recebidas' ?>">Recebidas(<?php echo $total_mensagens_recebidas; ?>)</a></li>
                            <li><a href="<?php echo base_url().'mensagens/credores' ?>">Credores(<?php echo $total_mensagens_credores; ?>)</a></li>
                            <li><a href="<?php echo base_url().'mensagens/saida' ?>">Sa�da</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'usuarios/minhaconta'?>" title="Minha conta"><span class="icon"><img src="<?php echo $img.'icons/cog.png'?>" alt="Minha conta" /></span><span>Minha conta</span></a>
                        </li>
                        <li> 
                            <a href="<?php echo base_url().'usuarios/credores'?>" title="Meus Credores"><span class="icon"><img src="<?php echo $img.'icons/cog.png'?>" alt="Meus Credores" /></span><span>Meus Credores</span></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'login/sair';?>" title="Sair"><span class="icon"><img src="<?php echo $img.'icons/exit16.png'?>" alt="Sair" /></span><span>Sair</span></a>
                        </li>
                    </ul>
                    <div class="corner tl"></div>
                    <div class="corner tr"></div>
                </div>
            </div>
        </div>