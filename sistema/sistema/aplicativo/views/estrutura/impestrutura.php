<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php echo $title; ?></title>

		<meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta name="robots" content="Follow" />
        
      
        <script type="text/javascript" src="<?php echo base_url(); ?>sistema/aplicativo/jscript/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>sistema/aplicativo/jscript/plugin/jquery.flash.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>sistema/aplicativo/jscript/jquery.funcoes.js"></script>

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>sistema/aplicativo/style/css/base.css" />


        <?php echo $csshtml; ?>
        <?php echo $jshtml; ?>

    </head>
       
    <body>
        <?php echo $conteudo; ?>
    </body>
</html>