<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img.'logo_elisangela.jpg';?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br|www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo $title ;?></p>
        </div>
    </div>
    <table id="tableJur02" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="thInadimplente">Inadimplente</th>
                <th id="thCredor">Credor</th>
                <th id="th3">N� Parcela</th>
                <th id="th4">Vencimento</th>
                <th id="th5" class="last">Valor</th>
            </tr>
        </thead>
        <tbody>
            <?php $totalDeInad = 0; foreach ($acordsJudAVenc as $acordJudAVen) : ?>
            <tr>
                <td class="tdInaNome"><?php echo utf8_decode($acordJudAVen->ina_nome); ?></td>
                <td class="tdCreNomeFantasia"><?php echo utf8_decode($acordJudAVen->cre_nome_fantasia); ?></td>
                <td class="td3"><?php echo $acordJudAVen->paa_parcela; ?></td>
                <td class="td4"><?php echo convDataBanco($acordJudAVen->paa_vencimento);?></td>
                <td class="td5 last"><?php echo $acordJudAVen->paa_valor;?></td>
                <?php $totalDeInad ++; endforeach; ?>
            </tr>
            <tr>
                <td id="tdTotalA" colspan="4">TOTAL DE PARCELAS A VENCER</td>
                <td id="tdTotalB" class="last"><?php echo "$totalDeInad";?></td>
            </tr>
        </tbody>
    </table>
</div>
