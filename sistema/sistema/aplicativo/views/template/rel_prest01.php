<SCRIPT Language="Javascript">

/*
This script is written by Eric (Webcrawl@usa.net)
For full source code, installation instructions,
100's more DHTML scripts, and Terms Of
Use, visit dynamicdrive.com
*/

function printit(){  
if (window.print) {
    window.print() ;  
} else {
    var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
    WebBrowser1.ExecWB(6, 2);//Use a 1 vs. a 2 for a prompting dialog box    WebBrowser1.outerHTML = "";  
}
}
</script>

<SCRIPT Language="Javascript">  
var NS = (navigator.appName == "Netscape");
var VERSION = parseInt(navigator.appVersion);
if (VERSION > 3) {
    document.write('<form style="width:100%;text-align:center;"><input type=button value="IMPRIMIR" name="Print" onClick="printit()" class="hidebuttonPrint"></form>');        
}
</script>
<style type="text/css">
@media print {
	.hidebuttonPrint {
		display:none;
	}
}
</style>

<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img . 'logoez2.jpg'; ?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="tituloAdv">
                <h1>
                    Elis�ngela Zanur�o - OAB/SP 251.797
                </h1>
                <h2 style="display: none;">
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    <span style="display: none;">Jos� Bonif�cio  - SP  - </span>(17)3245-5028
                </h2>
                <h2 style="margin-top: 2px;">
                    elisangela@netnew.com.br<br>www.ezadvocacia.com.br
                </h2>
            </div>
<!--            ----------------------->
            <div class="logo">
                <img src="<?php echo $credor->cre_logo != ''?  base_url().'sistema/aplicativo/upload/'.$credor->cre_logo:$img.'logo_SemImg.jpg'; ?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="tituloCred">
                <h2>
                	<?php $cre_nome_fantasia = str_replace(array("(",")"),array(" - ",""),$credor->cre_nome_fantasia); ?>
                    <?php echo ucwords(strtolower($cre_nome_fantasia)) ?>
                </h2>
                <h2>
                    <?php echo $credor->cre_cpf_cnpj ?>
                </h2>
                <h2>
                    <?php echo ucwords(strtolower($credor->cre_endereco.' - '.$credor->cre_cidade)) ?>
                </h2>
                <h2>
                    <?php echo $credor->cre_fone1 ?>
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo $title; ?></p>
        </div>
    </div>
    <?php
    
        $recBalcaoDinheiro = 0;
        $recBalcaoCheque = 0;
        $recEmConta = 0;
        $recCredor = 0;
        
        $honBalcaoDinheiro = 0;
        $honBalcaoCheque = 0;
        $honEmConta = 0;
        $honCredor = 0;
        
        $repBalcaoDinheiro = 0;
        $repBalcaoCheque = 0;
        $repEmConta = 0;
        $repCredor = 0;
    
    ?>
    <?php foreach ($pesqRes as $resultado) : ?>
	    <table id="tablePrest01" cellpadding="0" cellspacing="0">
	        <thead>
				<tr>
					<td colspan="12">
		            <p class="nomeInad" ><b><?php echo $resultado->ina_nome . ' - ' . $resultado->ina_cpf_cnpj ?></b></p>
		            </td>
				</tr>
	            <tr>
	                <th>Tipo Doc.</th>
	                <th>Tipo</th>
	                <th>Origem</th>
	                <th>Pagamento</th>
	                <th>T. Atual</th>
	                <th>Honor�rios</th>
	<!--                    <th>Repasse</th>-->
	                <th>Pend�ncia</th>
	                <th>Recebido</th>
	                <th>Parcela(s)</th>
	<!--                    <th>Tipo Repasse</th>-->
	                <th>Recebimento</th>
	                <th>(%)</th>
	                <th class="last">Repasse</th>
	<!--                    <th class="last">Honor�rio</th>-->
	            </tr>
	        </thead>
	        <tbody style="text-align: center;">
            <tr>
                <td><?php echo $resultado->aco_tipo == '1' ? 'Firmado' : 'Promessa' ?></td>
                <td><?php echo $resultado->cob_setor; ?><?php if ($resultado->cob_setor=='JUD') : ?><sup><?php echo $resultado->cob_subsetor=='NAJ'?'n':'a'; ?></sup><?php endif; ?></td>
                <td>
                    <?php
                    switch ($resultado->reb_tipo) {
                        case 1:
                            echo "Dinheiro";
                            break;
                        case 2:
                            echo "Cheque";
                            break;
                        case 3:
                            echo "Credor";
                            break;
                        case 4:
                            echo "Conta";
                            break;
                    }
                    ?>
                </td>
                <td><?php echo convData($resultado->reb_data, 'd'); ?></td>
                <td><?php echo convMoney($resultado->aco_total_parcelado) ?></td>
<!--                <td>< ?php echo convMoney(($resultado->reb_valor * ((100.00 - $resultado->rep_valor) / 100))) ?></td>-->
                <td><?php echo convMoney(($resultado->reb_valor * (($resultado->rep_valor) / 100))) ?></td>
                <td><?php echo convMoney($resultado->pendencia) ?></td>
                <td><?php echo convMoney($resultado->reb_valor) ?></td>
                <td>
                <?php
                $auxT = 0;
                if ((isset($ParPagas[$resultado->reb_cod]))&&($ParPagas[$resultado->reb_cod] != null) && ($ParPagas[$resultado->reb_cod] != '') && ($ParPagas[$resultado->reb_cod] != '*')) {
                    foreach ($ParPagas[$resultado->reb_cod] as $parcelas) {
                    	//var_dump($parcelas);
                        foreach ($parcelas as $parcela) {
                            if ($auxT == 0) {
                                echo $parcela->paa_parcela;
                                $auxT = 1;
                            } else {
                                echo ',' . $parcela->paa_parcela;
                            }
                        }
                    }
                    echo ' de ' . $resultado->aco_qtd_parc;
                }else{
                    echo '<i>Parcelas n�o especificadas </i>';
                }
                
                
                ?>
                </td>
<!--                <td>< ?php echo utf8_decode($resultado->rep_nome) ?></td>-->
                <td><?php
                    $auxT = 0;
                            if ((isset($ParPagas[$resultado->reb_cod]))&&($ParPagas[$resultado->reb_cod] != null) && ($ParPagas[$resultado->reb_cod] != '') && ($ParPagas[$resultado->reb_cod] != '*')) {
                        foreach ($ParPagas[$resultado->reb_cod] as $parcelas) {
                            foreach ($parcelas as $parcela) {
                                if ($auxT == 0) {
                                    $situacao = $parcela->paa_situacao == '1' ? '(I)' : '(P)';
                                    echo $parcela->paa_parcela . $situacao;
                                    $auxT = 1;
                                } else {
                                    $situacao = $parcela->paa_situacao == '1' ? '(I)' : '(P)';
                                    echo ' - ' . $parcela->paa_parcela . $situacao;
                                }
                            }
                        }
                    } else {
                        echo '<i>Parcelas n�o especificadas </i>';
                    }
                        ?></td>
                <td><?php echo (100.00 - $resultado->rep_valor) . ' %' ?></td>
<!--                <td class="last">< ?php echo convMoney(($resultado->reb_valor * (($resultado->rep_valor) / 100))) ?></td>-->
                <td class="last"><?php echo convMoney(($resultado->reb_valor * ((100.00 - $resultado->rep_valor) / 100))) ?></td>
            </tr>

            </tbody>
        </table>        
        <?php        
	        switch ($resultado->reb_tipo) {
	            case 1:
	                $recBalcaoDinheiro += $resultado->reb_valor;
	                $honBalcaoDinheiro += ($resultado->reb_valor * ((100.00 - $resultado->rep_valor) / 100));
	                $repBalcaoDinheiro += ($resultado->reb_valor * (($resultado->rep_valor) / 100));
	                break;
	            case 2:
	                $recBalcaoCheque += $resultado->reb_valor;
	                $honBalcaoCheque += ($resultado->reb_valor * ((100.00 - $resultado->rep_valor) / 100));
	                $repBalcaoCheque += ($resultado->reb_valor * (($resultado->rep_valor) / 100));
	                break;
	            case 3:
	                $recCredor += $resultado->reb_valor;
	                $honCredor += ($resultado->reb_valor * ((100.00 - $resultado->rep_valor) / 100));
	                $repCredor += ($resultado->reb_valor * (($resultado->rep_valor) / 100));
	                break;
	            case 4:
	                $recEmConta += $resultado->reb_valor;
	                $honEmConta += ($resultado->reb_valor * ((100.00 - $resultado->rep_valor) / 100));
	                $repEmConta += ($resultado->reb_valor * (($resultado->rep_valor) / 100));
	                break;
	        }
        
            $somaAdv = ($recBalcaoDinheiro + $recBalcaoCheque + $recEmConta);
            $somaHon = ($repBalcaoDinheiro+$repBalcaoCheque+$repEmConta+$repCredor);
            if($somaHon > $somaAdv){
                $depositar = ($somaHon - $somaAdv);
                $favorecido = 'E.Z. ADVOCACIA';
            }else if($somaHon < $somaAdv){
                $depositar = ($somaAdv - $somaHon);
                $favorecido = 'CREDOR';
            }else{
                $depositar = 0.0;
                $favorecido = 'IGUAL';
            }
            
        ?>
        <?php endforeach; ?>
    <div id="visaoGeral">
        <table id="geralTable" cellpadding="0" cellspacing="0">
            <tr>
                <td class="destaque last" colspan="4">
                    VISAO GERAL DOS RECEBIMENTOS
                </td>
            </tr>
            <tr>
                <td colspan="4" class="last">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="destaque">Tipo Recebimento</td>
                <td class="destaque">Recebido</td>
                <td class="destaque">Honor�rios</td>
                <td class="destaque last">Repasse</td>
<!--                <td class="destaque">Repasse</td>
                <td class="destaque last">Honor�rio</td>-->
            </tr>
            <tr>
                <td>Balc�o Dinheiro</td>
                <td><?php echo $recBalcaoDinheiro != 0 ? convMoney($recBalcaoDinheiro):'----' ?></td>
                <td><?php echo $repBalcaoDinheiro != 0 ? convMoney($repBalcaoDinheiro):'----' ?></td>
                <td class="last" style="text-align: right"><?php echo $honBalcaoDinheiro != 0 ? convMoney($honBalcaoDinheiro):'----' ?></td>
            </tr>
            <tr>
                <td>Balc�o Cheque</td>
                <td><?php echo $recBalcaoCheque != 0 ? convMoney($recBalcaoCheque):'----' ?></td>
                <td><?php echo $repBalcaoCheque != 0 ? convMoney($repBalcaoCheque):'----' ?></td>
                <td class="last" style="text-align: right"><?php echo $honBalcaoCheque != 0 ? convMoney($honBalcaoCheque):'----' ?></td>
            </tr>
            <tr>
                <td>Em Conta</td>
                <td><?php echo $recEmConta != 0 ? convMoney($recEmConta):'----' ?></td>
                <td><?php echo $repEmConta != 0 ? convMoney($repEmConta):'----' ?></td>
                <td class="last" style="text-align: right"><?php echo $honEmConta != 0 ? convMoney($honEmConta):'----' ?></td>
            </tr>
            <tr>
                <td>Credor</td>
                <td><?php echo $recCredor != 0 ? convMoney($recCredor):'----' ?></td>
                <td><?php echo $repCredor != 0 ? convMoney($repCredor):'----' ?></td>
                <td class="last" style="text-align: right"><?php echo $honCredor != 0 ? convMoney($honCredor):'----' ?></td>
            </tr>
            <tr>
                <td class="destaque" style="text-align: left">TOTAL</td>
                <td class="destaque" style="font-weight: normal"><?php echo convMoney(($recBalcaoDinheiro+$recBalcaoCheque+$recEmConta+$recCredor)) ?></td>
                <td class="destaque" style="font-weight: normal"><?php echo convMoney(($repBalcaoDinheiro+$repBalcaoCheque+$repEmConta+$repCredor)) ?></td>
                <td class="destaque last" style="font-size: 13pt;"><?php echo convMoney(($honBalcaoDinheiro+$honBalcaoCheque+$honEmConta+$honCredor)) ?></td>
            </tr>
            <tr>
                <td colspan="4" class="last">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="destaque" style="text-align: right" colspan="3">TOTAL A SER DEPOSITADO</td>
                <td class="destaque last"><?php echo convMoney($depositar) ?></td>
            </tr>
            <tr>
                <td class="destaque" style="text-align: right" colspan="3">FAVORECIDO</td>
                <td class="destaque last"><?php echo $favorecido ?></td>
            </tr>
        </table>
    </div>
</div>
