<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <form id="form" action="<?php echo base_url() . 'cadtipousu/update'; ?>" method="post">
            <div class="box">
                <div class="title">
                    <h5>Editar grupo de usu�rio</h5>
                </div>
                <div class="form">
                    <div class="fields">
                        <div style="border: none;" class="field  field">
                            <div class="divleft" style="width: 116px;">
                                <div class="label">
                                    <label>C�digo:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dados->gru_cod; ?></p>
                            </div>
                            <div class="divleftlast" style="width: 491px;">
                                <div style="width: 119px;" class="label">
                                    <label for="input-medium">Nome do grupo:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 248px;" type="text" id="input-medium" maxlength="23" name="gruponome" value="<?php echo utf8_decode($dados->gru_titulo); ?>" />
                                    <input style="width: 248px;" type="hidden" id="input-medium" name="grupocod" value="<?php echo $dados->gru_cod; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="box-tabs" class="box">
                    <div class="title">
                        <h5>Permiss�es</h5>
                        <ul class="links">
                            <li><a href="#cad">Cadastros</a></li>
                            <li><a href="#ger">Gerenciamento</a></li>
                            <li><a href="#cob">Cobran�as</a></li>
                            <li><a href="#relat">Relat�rios</a></li>
                            <li><a href="#outros">Outros</a></li>
                        </ul>
                    </div>
                    <div id="cad">
                        <div class="form">
                            <div class="fields">
                                <div style="border: none;" class="field">
                                    <fieldset class="fieldsetsta">
                                        <legend class="forte">
                                            Cadastros:
                                        </legend>
                                        <div>
                                            <?php
                                            if (array_search('1', $menus1, TRUE) != '') {
                                                echo html_entity_decode('<input type="radio" name="cadastro" value="1" checked>Acessa
                                                      <input type="radio" name="cadastro" value="0">N�o Acessa');
                                            } else {
                                                echo html_entity_decode('<input type="radio" name="cadastro" value="1">Acessa
                                                      <input type="radio" name="cadastro" value="0" checked >N�o Acessa');
                                            }
                                            ?>
                                        </div>
                                        <!--==============-->
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Nova d�vida:
                                            </legend>
                                            <div>
                                                <?php
                                                if (array_search('2', $menus2, TRUE) != '') {
                                                    echo html_entity_decode('<input type="radio" name="cad_divida" value="1" checked>Acessa
                                                          <input type="radio" name="cad_divida" value="0">N�o Acessa');
                                                } else {
                                                    echo html_entity_decode('<input type="radio" name="cad_divida" value="1">Acessa
                                                          <input type="radio" name="cad_divida" value="0" checked >N�o Acessa');
                                                }
                                                ?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Novo inadimplente:
                                            </legend>
                                            <div>
                                                <?php
                                                if (array_search('3', $menus2, TRUE) != '') {
                                                    echo html_entity_decode('<input type="radio" name="cad_inad" value="1" checked>Acessa
                                                                                                          <input type="radio" name="cad_inad" value="0">N�o Acessa');
                                                } else {
                                                    echo html_entity_decode('<input type="radio" name="cad_inad" value="1">Acessa
                                                                                                          <input type="radio" name="cad_inad" value="0" checked >N�o Acessa');
                                                }
                                                ?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Novo credor:
                                            </legend>
                                            <div>
<?php
if (array_search('4', $menus2, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="cad_cred" value="1" checked>Acessa
                                                          <input type="radio" name="cad_cred" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="cad_cred" value="1">Acessa
                                                          <input type="radio" name="cad_cred" value="0" checked >N�o Acessa');
}
?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Novo bem:
                                            </legend>
                                            <div>
<?php
if (array_search('5', $menus2, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="cad_bem" value="1" checked>Acessa
                                                          <input type="radio" name="cad_bem" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="cad_bem" value="1">Acessa
                                                          <input type="radio" name="cad_bem" value="0" checked >N�o Acessa');
}
?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Tipo de opera��o:
                                            </legend>
                                            <div>
<?php
if (array_search('6', $menus2, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="cad_opr" value="1" checked>Acessa
                                                          <input type="radio" name="cad_opr" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="cad_opr" value="1">Acessa
                                                          <input type="radio" name="cad_opr" value="0" checked >N�o Acessa');
}
?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Usu�rios:
                                            </legend>
                                            <div>
<?php
if (array_search('7', $menus2, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="cad_usu" value="1" checked>Acessa
                                                          <input type="radio" name="cad_usu" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="cad_usu" value="1">Acessa
                                                          <input type="radio" name="cad_usu" value="0" checked >N�o Acessa');
}
?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Grupo de usu�rios:
                                            </legend>
                                            <div>
<?php
if (array_search('8', $menus2, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="cad_gru" value="1" checked>Acessa
                                                          <input type="radio" name="cad_gru" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="cad_gru" value="1">Acessa
                                                          <input type="radio" name="cad_gru" value="0" checked >N�o Acessa');
}
?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Cartas:
                                            </legend>
                                            <div>
<?php
if (array_search('43', $menus2, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="cartas" value="1" checked>Acessa
                                                          <input type="radio" name="cartas" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="cartas" value="1">Acessa
                                                          <input type="radio" name="cartas" value="0" checked >N�o Acessa');
}
?>
                                            </div>
                                        </fieldset>
                                    </fieldset>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="ger">
                        <div class="form">
                            <div class="fields">
                                <div style="border: none;" class="field">
                                    <fieldset class="fieldsetsta">
                                        <legend class="forte">
                                            Gerenciamento
                                        </legend>
                                        <div>
<?php
if (array_search('9', $menus1, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="gerenciamento" value="1" checked>Acessa
                                                      <input type="radio" name="gerenciamento" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="gerenciamento" value="1">Acessa
                                                      <input type="radio" name="gerenciamento" value="0" checked >N�o Acessa');
}
?>
                                        </div>
                                        <!--==============-->
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Repasse:
                                            </legend>
                                            <div>
                                            <?php
                                            if (array_search('10', $menus2, TRUE) != '') {
                                                echo html_entity_decode('<input type="radio" name="ger_repasse" value="1" checked>Acessa
                                                          <input type="radio" name="ger_repasse" value="0">N�o Acessa');
                                            } else {
                                                echo html_entity_decode('<input type="radio" name="ger_repasse" value="1">Acessa
                                                          <input type="radio" name="ger_repasse" value="0" checked >N�o Acessa');
                                            }
                                            ?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Remanejar credor:
                                            </legend>
                                            <div>
                                                <?php
                                                if (array_search('11', $menus2, TRUE) != '') {
                                                    echo html_entity_decode('<input type="radio" name="ger_remCred" value="1" checked>Acessa
                                                          <input type="radio" name="ger_remCred" value="0">N�o Acessa');
                                                } else {
                                                    echo html_entity_decode('<input type="radio" name="ger_remCred" value="1">Acessa
                                                          <input type="radio" name="ger_remCred" value="0" checked >N�o Acessa');
                                                }
                                                ?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Remanejar inadimplente:
                                            </legend>
                                            <div>
                                                <?php
                                                if (array_search('12', $menus2, TRUE) != '') {
                                                    echo html_entity_decode('<input type="radio" name="ger_remInad" value="1" checked>Acessa
                                                          <input type="radio" name="ger_remInad" value="0">N�o Acessa');
                                                } else {
                                                    echo html_entity_decode('<input type="radio" name="ger_remInad" value="1">Acessa
                                                          <input type="radio" name="ger_remInad" value="0" checked >N�o Acessa');
                                                }
                                                ?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                listar:
                                            </legend>
                                            <div>
                                                <?php
                                                if (array_search('13', $menus2, TRUE) != '') {
                                                    echo html_entity_decode('<input type="radio" name="ger_listar" value="1" checked>Acessa
                                                          <input type="radio" name="ger_listar" value="0">N�o Acessa');
                                                } else {
                                                    echo html_entity_decode('<input type="radio" name="ger_listar" value="1">Acessa
                                                          <input type="radio" name="ger_listar" value="0" checked >N�o Acessa');
                                                }
                                                ?>
                                            </div>
                                            <fieldset class="fieldsetstain">
                                                <legend>
                                                    Todas as cobran�as
                                                </legend>
                                                <div>
                                                <?php
                                                if (array_search('14', $menus3, TRUE) != '') {
                                                    echo html_entity_decode('<input type="radio" name="ger_list_cob" value="1" checked>Acessa
                                                          <input type="radio" name="ger_list_cob" value="0">N�o Acessa');
                                                } else {
                                                    echo html_entity_decode('<input type="radio" name="ger_list_cob" value="1">Acessa
                                                          <input type="radio" name="ger_list_cob" value="0" checked >N�o Acessa');
                                                }
                                                ?>
                                                </div>
                                            </fieldset>
                                            <fieldset class="fieldsetstain">
                                                <legend>
                                                    Todas os inadimplentes
                                                </legend>
                                                <div>
                                                    <?php
                                                    if (array_search('15', $menus3, TRUE) != '') {
                                                        echo html_entity_decode('<input type="radio" name="ger_list_inad" value="1" checked>Acessa
                                                              <input type="radio" name="ger_list_inad" value="0">N�o Acessa');
                                                    } else {
                                                        echo html_entity_decode('<input type="radio" name="ger_list_inad" value="1">Acessa
                                                              <input type="radio" name="ger_list_inad" value="0" checked >N�o Acessa');
                                                    }
                                                    ?>
                                                </div>
                                            </fieldset>
                                            <fieldset class="fieldsetstain">
                                                <legend>
                                                    Todos os credores
                                                </legend>
                                                <div>
                                                    <?php
                                                    if (array_search('16', $menus3, TRUE) != '') {
                                                        echo html_entity_decode('<input type="radio" name="ger_list_cred" value="1" checked>Acessa
                                                              <input type="radio" name="ger_list_cred" value="0">N�o Acessa');
                                                    } else {
                                                        echo html_entity_decode('<input type="radio" name="ger_list_cred" value="1">Acessa
                                                              <input type="radio" name="ger_list_cred" value="0" checked >N�o Acessa');
                                                    }
                                                    ?>
                                                </div>
                                            </fieldset>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Backup:
                                            </legend>
                                            <div>
                                            <?php
                                            if (array_search('42', $menus2, TRUE) != '') {
                                                echo html_entity_decode('<input type="radio" name="ger_backup" value="1" checked>Acessa
                                                          <input type="radio" name="ger_backup" value="0">N�o Acessa');
                                            } else {
                                                echo html_entity_decode('<input type="radio" name="ger_backup" value="1">Acessa
                                                          <input type="radio" name="ger_backup" value="0" checked >N�o Acessa');
                                            }
                                            ?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Devolu��o:
                                            </legend>
                                            <div>
                                            <?php
                                            if (array_search('44', $menus2, TRUE) != '') {
                                                echo html_entity_decode('<input type="radio" name="ger_devolucao" value="1" checked>Acessa
                                                          <input type="radio" name="ger_devolucao" value="0">N�o Acessa');
                                            } else {
                                                echo html_entity_decode('<input type="radio" name="ger_devolucao" value="1">Acessa
                                                          <input type="radio" name="ger_devolucao" value="0" checked >N�o Acessa');
                                            }
                                            ?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Listar Importa��o:
                                            </legend>
                                            <div>
                                            <?php
                                            if (array_search('38', $menus2, TRUE) != '') {
                                                echo html_entity_decode('<input type="radio" name="listar_imp" value="1" checked>Acessa
                                                          <input type="radio" name="listar_imp" value="0">N�o Acessa');
                                            } else {
                                                echo html_entity_decode('<input type="radio" name="listar_imp" value="1">Acessa
                                                          <input type="radio" name="listar_imp" value="0" checked >N�o Acessa');
                                            }
                                            ?>
                                            </div>
                                        </fieldset>
                                    </fieldset>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="cob">
                        <div class="form">
                            <div class="fields">
                                <div style="border: none;" class="field">
                                    <fieldset class="fieldsetsta">
                                        <legend class="forte">
                                            Cobran�as Administrativas:
                                        </legend>
                                        <div>
<?php
if (array_search('17', $menus1, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="cobAdm" value="1" checked>Acessa
                                                      <input type="radio" name="cobAdm" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="cobAdm" value="1">Acessa
                                                      <input type="radio" name="cobAdm" value="0" checked >N�o Acessa');
}
?>
                                        </div>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Minhas cobran�as:
                                            </legend>
                                            <div>
                                            <?php
                                            if (array_search('18', $menus2, TRUE) != '') {
                                                echo html_entity_decode('<input type="radio" name="cobAdm_minhas" value="1" checked>Acessa
                                                          <input type="radio" name="cobAdm_minhas" value="0">N�o Acessa');
                                            } else {
                                                echo html_entity_decode('<input type="radio" name="cobAdm_minhas" value="1">Acessa
                                                          <input type="radio" name="cobAdm_minhas" value="0" checked >N�o Acessa');
                                            }
                                            ?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Listar todas:
                                            </legend>
                                            <div>
                                                <?php
                                                if (array_search('19', $menus2, TRUE) != '') {
                                                    echo html_entity_decode('<input type="radio" name="cobAdm_todas" value="1" checked>Acessa
                                                          <input type="radio" name="cobAdm_todas" value="0">N�o Acessa');
                                                } else {
                                                    echo html_entity_decode('<input type="radio" name="cobAdm_todas" value="1">Acessa
                                                          <input type="radio" name="cobAdm_todas" value="0" checked >N�o Acessa');
                                                }
                                                ?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Recebimentos:
                                            </legend>
                                            <div>
<?php
if (array_search('20', $menus2, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="cobAdm_receb" value="1" checked>Acessa
                                                          <input type="radio" name="cobAdm_receb" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="cobAdm_receb" value="1">Acessa
                                                          <input type="radio" name="cobAdm_receb" value="0" checked >N�o Acessa');
}
?>
                                            </div>
                                            <fieldset class="fieldsetstain">
                                                <legend>
                                                    Meus:
                                                </legend>
                                                <div>
<?php
if (array_search('21', $menus3, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="cobAdm_receb_meus" value="1" checked>Acessa
                                                              <input type="radio" name="cobAdm_receb_meus" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="cobAdm_receb_meus" value="1">Acessa
                                                              <input type="radio" name="cobAdm_receb_meus" value="0" checked >N�o Acessa');
}
?>
                                                </div>
                                            </fieldset>
                                            <fieldset class="fieldsetstain">
                                                <legend>
                                                    Todos:
                                                </legend>
                                                <div>
<?php
if (array_search('22', $menus3, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="cobAdm_receb_todos" value="1" checked>Acessa
                                                              <input type="radio" name="cobAdm_receb_todos" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="cobAdm_receb_todos" value="1">Acessa
                                                              <input type="radio" name="cobAdm_receb_todos" value="0" checked >N�o Acessa');
}
?>
                                                </div>
                                            </fieldset>
                                            <fieldset class="fieldsetstain">
                                                <legend>
                                                    Previs�es:
                                                </legend>
                                                <div>
<?php
if (array_search('23', $menus3, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="cobAdm_receb_prev" value="1" checked>Acessa
                                                              <input type="radio" name="cobAdm_receb_prev" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="cobAdm_receb_prev" value="1">Acessa
                                                              <input type="radio" name="cobAdm_receb_prev" value="0" checked >N�o Acessa');
}
?>
                                                </div>
                                            </fieldset>
                                        </fieldset>
                                    </fieldset>
                                    <fieldset class="fieldsetsta">
                                        <legend class="forte">
                                            Cobran�as Judiciais:
                                        </legend>
                                        <div>
<?php
if (array_search('24', $menus1, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="cobJud" value="1" checked>Acessa
                                                      <input type="radio" name="cobJud" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="cobJud" value="1">Acessa
                                                      <input type="radio" name="cobJud" value="0" checked >N�o Acessa');
}
?>
                                        </div>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Listar:
                                            </legend>
                                            <div>
<?php
if (array_search('25', $menus2, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="cobJud_listar" value="1" checked>Acessa
                                                          <input type="radio" name="cobJud_listar" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="cobJud_listar" value="1">Acessa
                                                          <input type="radio" name="cobJud_listar" value="0" checked >N�o Acessa');
}
?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Acordos extintos:
                                            </legend>
                                            <div>
                                            <?php
                                            if (array_search('26', $menus2, TRUE) != '') {
                                                echo html_entity_decode('<input type="radio" name="cobJud_acordosExt" value="1" checked>Acessa
                                                          <input type="radio" name="cobJud_acordosExt" value="0">N�o Acessa');
                                            } else {
                                                echo html_entity_decode('<input type="radio" name="cobJud_acordosExt" value="1">Acessa
                                                          <input type="radio" name="cobJud_acordosExt" value="0" checked >N�o Acessa');
                                            }
                                            ?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Importar Andamento:
                                            </legend>
                                            <div>
                                            <?php
                                            if (array_search('47', $menus2, TRUE) != '') {
                                                echo html_entity_decode('<input type="radio" name="cobJud_importar" value="1" checked>Acessa
                                                          <input type="radio" name="cobJud_importar" value="0">N�o Acessa');
                                            } else {
                                                echo html_entity_decode('<input type="radio" name="cobJud_importar" value="1">Acessa
                                                          <input type="radio" name="cobJud_importar" value="0" checked >N�o Acessa');
                                            }
                                            ?>
                                            </div>
                                        </fieldset>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="relat">
                        <div class="form">
                            <div class="fields">
                                <div style="border: none;" class="field">
                                    <fieldset class="fieldsetsta">
                                        <legend class="forte">
                                            Relat�rios:
                                        </legend>
                                                <?php
                                                if (array_search('31', $menus1, TRUE) != '') {
                                                    echo html_entity_decode('<input type="radio" name="relatorios" value="1" checked>Acessa
                                                      <input type="radio" name="relatorios" value="0">N�o Acessa');
                                                } else {
                                                    echo html_entity_decode('<input type="radio" name="relatorios" value="1">Acessa
                                                      <input type="radio" name="relatorios" value="0" checked >N�o Acessa');
                                                }
                                                ?>
                                        <!--==============-->
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Relat�rios administrativos:
                                            </legend>
<?php
if (array_search('32', $menus2, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="relatAdm" value="1" checked>Acessa
                                                          <input type="radio" name="relatAdm" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="relatAdm" value="1">Acessa
                                                          <input type="radio" name="relatAdm" value="0" checked >N�o Acessa');
}
?>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Relat�rios judiciais:
                                            </legend>
                                        <?php
                                        if (array_search('33', $menus2, TRUE) != '') {
                                            echo html_entity_decode('<input type="radio" name="relatJud" value="1" checked>Acessa
                                                          <input type="radio" name="relatJud" value="0">N�o Acessa');
                                        } else {
                                            echo html_entity_decode('<input type="radio" name="relatJud" value="1">Acessa
                                                          <input type="radio" name="relatJud" value="0" checked >N�o Acessa');
                                        }
                                        ?>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Relat�rios por RO:
                                            </legend>
                                            <?php
                                            if (array_search('34', $menus2, TRUE) != '') {
                                                echo html_entity_decode('<input type="radio" name="relatRo" value="1" checked>Acessa
                                                              <input type="radio" name="relatRo" value="0">N�o Acessa');
                                            } else {
                                                echo html_entity_decode('<input type="radio" name="relatRo" value="1">Acessa
                                                              <input type="radio" name="relatRo" value="0" checked >N�o Acessa');
                                            }
                                            ?>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Relat�rios gerenciais:
                                            </legend>
                                            <?php
                                            if (array_search('35', $menus2, TRUE) != '') {
                                                echo html_entity_decode('<input type="radio" name="relatGer" value="1" checked>Acessa
                                                              <input type="radio" name="relatGer" value="0">N�o Acessa');
                                            } else {
                                                echo html_entity_decode('<input type="radio" name="relatGer" value="1">Acessa
                                                              <input type="radio" name="relatGer" value="0" checked >N�o Acessa');
                                            }
                                            ?>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Relat�rios presta��o de contas:
                                            </legend>
                                            <?php
                                            if (array_search('36', $menus2, TRUE) != '') {
                                                echo html_entity_decode('<input type="radio" name="relatPC" value="1" checked>Acessa
                                                              <input type="radio" name="relatPC" value="0">N�o Acessa');
                                            } else {
                                                echo html_entity_decode('<input type="radio" name="relatPC" value="1">Acessa
                                                              <input type="radio" name="relatPC" value="0" checked >N�o Acessa');
                                            }
                                            ?>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Relat�rios de Acesso do Credor
                                            </legend>
                                            <?php
                                            if (array_search('37', $menus2, TRUE) != '') {
                                                echo html_entity_decode('<input type="radio" name="relatAces" value="1" checked>Acessa
                                                              <input type="radio" name="relatAces" value="0">N�o Acessa');
                                            } else {
                                                echo html_entity_decode('<input type="radio" name="relatAces" value="1">Acessa
                                                              <input type="radio" name="relatAces" value="0" checked >N�o Acessa');
                                            }
                                            ?>
                                        </fieldset><fieldset class="fieldsetstain">
                                            <legend>
                                                Relat�rios de Acesso do Usu�rio
                                            </legend>
                                            <?php
                                            if (array_search('45', $menus2, TRUE) != '') {
                                                echo html_entity_decode('<input type="radio" name="relatAcesUsu" value="1" checked>Acessa
                                                              <input type="radio" name="relatAcesUsu" value="0">N�o Acessa');
                                            } else {
                                                echo html_entity_decode('<input type="radio" name="relatAcesUsu" value="1">Acessa
                                                              <input type="radio" name="relatAcesUsu" value="0" checked >N�o Acessa');
                                            }
                                            ?>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Relat�rios de Resultados de Contatos
                                            </legend>
                                            <?php
                                            if (array_search('41', $menus2, TRUE) != '') {
                                                echo html_entity_decode('<input type="radio" name="relatResultContat" value="1" checked>Acessa
                                                              <input type="radio" name="relatResultContat" value="0">N�o Acessa');
                                            } else {
                                                echo html_entity_decode('<input type="radio" name="relatResultContat" value="1">Acessa
                                                              <input type="radio" name="relatResultContat" value="0" checked >N�o Acessa');
                                            }
                                            ?>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Relat�rios de Importa��o
                                            </legend>
                                            <?php
                                            if (array_search('39', $menus2, TRUE) != '') {
                                                echo html_entity_decode('<input type="radio" name="relatImportacao" value="1" checked>Acessa
                                                              <input type="radio" name="relatImportacao" value="0">N�o Acessa');
                                            } else {
                                                echo html_entity_decode('<input type="radio" name="relatImportacao" value="1">Acessa
                                                              <input type="radio" name="relatImportacao" value="0" checked >N�o Acessa');
                                            }
                                            ?>
                                        </fieldset>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="outros">
                        <div class="form">
                            <div class="fields">
                                <div style="border: none;" class="field">
                                    <fieldset class="fieldsetsta">
                                        <legend class="forte">
                                            Pesquisar:
                                        </legend>
                                        <div>
<?php
if (array_search('27', $menus1, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="pesquisa" value="1" checked>Acessa
                                                      <input type="radio" name="pesquisa" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="pesquisa" value="1">Acessa
                                                      <input type="radio" name="pesquisa" value="0" checked >N�o Acessa');
}
?>
                                        </div>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Cobran�as:
                                            </legend>
                                            <div>
<?php
if (array_search('28', $menus2, TRUE) != '') {
    echo html_entity_decode('<input type="radio" name="pesquisa_cob" value="1" checked>Acessa
                                                          <input type="radio" name="pesquisa_cob" value="0">N�o Acessa');
} else {
    echo html_entity_decode('<input type="radio" name="pesquisa_cob" value="1">Acessa
                                                          <input type="radio" name="pesquisa_cob" value="0" checked >N�o Acessa');
}
?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Inadimplentes:
                                            </legend>
                                            <div>
                                            <?php
                                            if (array_search('29', $menus2, TRUE) != '') {
                                                echo html_entity_decode('<input type="radio" name="pesquisa_inad" value="1" checked>Acessa
                                                          <input type="radio" name="pesquisa_inad" value="0">N�o Acessa');
                                            } else {
                                                echo html_entity_decode('<input type="radio" name="pesquisa_inad" value="1">Acessa
                                                          <input type="radio" name="pesquisa_inad" value="0" checked >N�o Acessa');
                                            }
                                            ?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Credores:
                                            </legend>
                                            <div>
                                                <?php
                                                if (array_search('30', $menus2, TRUE) != '') {
                                                    echo html_entity_decode('<input type="radio" name="pesquisa_cred" value="1" checked>Acessa
                                                          <input type="radio" name="pesquisa_cred" value="0">N�o Acessa');
                                                } else {
                                                    echo html_entity_decode('<input type="radio" name="pesquisa_cred" value="1">Acessa
                                                          <input type="radio" name="pesquisa_cred" value="0" checked >N�o Acessa');
                                                }
                                                ?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Jur�dico:
                                            </legend>
                                            <div>
                                                <?php
                                                if (array_search('46', $menus2, TRUE) != '') {
                                                    echo html_entity_decode('<input type="radio" name="pesquisa_juri" value="1" checked>Acessa
                                                          <input type="radio" name="pesquisa_juri" value="0">N�o Acessa');
                                                } else {
                                                    echo html_entity_decode('<input type="radio" name="pesquisa_juri" value="1">Acessa
                                                          <input type="radio" name="pesquisa_juri" value="0" checked >N�o Acessa');
                                                }
                                                ?>
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Fiscal:
                                            </legend>
                                            <div>
                                                <?php
                                                if (array_search('48', $menus2, TRUE) != '') {
                                                    echo html_entity_decode('<input type="radio" name="pesquisa_fiscal" value="1" checked>Acessa
                                                          <input type="radio" name="pesquisa_fiscal" value="0">N�o Acessa');
                                                } else {
                                                    echo html_entity_decode('<input type="radio" name="pesquisa_fiscal" value="1">Acessa
                                                          <input type="radio" name="pesquisa_fiscal" value="0" checked >N�o Acessa');
                                                }
                                                ?>
                                            </div>
                                        </fieldset>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="title">
                        <h5>A��es</h5>
                    </div>
                    <div class="form">
                        <div class="fields">
                            <div style="text-align: center; margin-top: 10px;" class="buttons">
                                <input type="reset" name="cancelar" value="Cancelar" />
                                <div style="margin-left: 7px;" class="highlight">
                                    <input type="submit" name="alterar" value="Alterar" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>