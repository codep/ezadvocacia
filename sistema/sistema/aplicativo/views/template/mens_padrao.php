<script type="text/javascript">
    $(document).ready(function(){
        $('.voltar').click(function(e){
            e.preventDefault();
            link = $(this).attr('id');
            window.location.href=link;
        });
        $('.novo').click(function(e){
            e.preventDefault();
            link = $(this).attr('id');
            window.location.href=link;
        });
    });
</script>
<div id="login">
    <div class="title">
        <h5>Resultado da opera��o</h5>
        <div class="corner tl"></div>
        <div class="corner tr"></div>
    </div>
    <div class="inner" style="background:url(../img/login.png) no-repeat scroll left top #FFFFFF">
        <?php echo $mensagem; ?>
        <form action="" method="post">
            <div class="form">
                <div class="fields">
                    <div class="buttons">
                        <input class="voltar" id="<?php echo $voltarbtn; ?>" type="submit" value="Voltar" />
                        <input class="novo" id="<?php echo $novobtn; ?>" type="reset" value="Cadastrar outro" />
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
