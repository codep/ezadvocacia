<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>[CONTATO]</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <style type="text/css">

        /** FORMATA��O E-MAIL
        ************************/
        body{
            font-family:Arial;
            font-size:12px;
        }
        div{
            width:800px;
            margin:0 auto;
        }
        h3{
            border-bottom:1px dotted #ccc;
            color:#116DA3
        }
        b{
            color:#116DA3;
        }
        
    </style>
  </head>
  <body>
    <div>
        <h3>Dados para contato</h3>
        <b>Nome:</b> <?php echo $contatonome; ?><br />
        <b>Endere�o:</b> <?php echo $contatoendereco; ?><br />
        <b>Cidade:</b> <?php echo $contatocidade; ?><br />
        <b>Estado:</b> <?php echo $contatoestado; ?><br />
        <b>Telefone:</b> <?php echo $contatotelefone; ?><br />
        <b>E-mail:</b> <?php echo $contatoemail; ?><br />
        <p>
            <b>Mensagem:</b> <?php echo $contatomensagem; ?>
        </p>
    </div>
  </body>
</html>
