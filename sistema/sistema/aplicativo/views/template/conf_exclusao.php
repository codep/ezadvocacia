<script type="text/javascript">
    $(document).ready(function(){
        $('.cancelar').click(function(e){
            e.preventDefault();
            link = $(this).attr('id');
            window.location.href=link;
        });
    });
</script>

<div id="login">

    <div class="title">
        <h5>Corfirma��o de Exclus�o</h5>
        <div class="corner tl"></div>
        <div class="corner tr"></div>
    </div>
    <div class="inner" style="background:url(../img/login.png) no-repeat scroll left top #FFFFFF">
        <form action="<?php echo base_url() . $acaook; ?>" method="post">
            <input type="hidden" name="codigo" value="<?php echo $codigo; ?>" />
            <input type="hidden" name="tabela" value="<?php echo $tabela; ?>" />
            <input type="hidden" name="chave" value="<?php echo $chave; ?>" />
            <input type="hidden" name="voltar" value="<?php echo base_url() . $acaocancel; ?>" />
            <div class="form">
                <div class="fields">
                    <div class="field">
                        <div class="label" style="text-align: center; width: 380px; font-weight: normal">
                            <label style="font-size: 13px; color: #f00">Aten��o voc� est� prestes a excluir dados importantes do sistema!</label><br/><br/>
                            <p style="width: 90%;"><?php echo isset ($observacoes)? $observacoes : ''  ?></p><br/><br/>
                            <p style="font-weight: normal; font-size: 12px; width: 90%;">Para confirmar a exclus�o do(a) <b style="color: #f00;"><?php echo $deletado; ?></b> com o c�digo <b><?php echo $codigo; ?></b> digite a <i>Senha Master</i> do Sistema</p>
                        </div>

                    </div>
                    <div class="field">
                        <div class="label">
                            <label for="senha">Senha Master:</label>
                        </div>
                        <div class="input">
                            <input type="password" id="senha" name="senha" size="40" class="focus" />
                        </div>
                    </div>
                    <div class="buttons">
                        <dir class="righlight">
                            <input class="cancelar" id="<?php echo base_url() . $acaocancel; ?>" type="reset" value="Cancelar" />
                            <input class="aceitarBtn" name="submit.highlight" type="submit" value="Excluir" />
                        </dir>
                    </div>
                </div>
            </div>
        </form>
        <?php echo $mensagem; ?>
    </div>
</div>
