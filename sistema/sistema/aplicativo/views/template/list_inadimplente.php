<script type="text/javascript">
        $(document).ready(function(){
            $(document).ready(function(){

        var interruptor=0;

        $('.excluir').click(function(e){
            e.preventDefault()
            interruptor=1;
            link = $(this).attr('id');
            window.location.href=link;
        });

        $('.visualizar').click(function(e){
            if(interruptor==0){
                link = $(this).attr('id');
                window.location.href=link;
            }else{
                e.preventDefault();
                interruptor=0;
            }
        });
    });
        });
</script>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Lista de inadimplentes</h5>
            </div>
            <form id="form" method="post" action="<?php echo base_url().'inadimplente/listar' ?>">
                <div class="form">
                	<?php echo $mensagem ?>
                    <div class="fields" style="height: 40px;">
                        <div style="float: left; margin-top: 2px;" class="select">
                            <select style="width: 215px;" id="list_inadimplente_tipo" name="filtro1" class="chosen">
                                <option value="ina_nome">Nome</option>
                                <option value="telefone">Telefone</option>
                                <option value="ina_cidade">Cidade</option>
                                <option value="telenonePAR">Telefone (parentes)</option>
                                <option value="ina_endereco">Endere�o</option>
                                <option value="ina_cpf_cnpj">CPF</option>
                                <option value="parentes">Parentes</option>
                                <option value="ina_mae_nome">Nome da m�e</option>
                                <option value="ina_pai_nome">Nome do pai</option>
                            </select>
                        </div>
                        <div class="input" style="float: right; width: 490px">
                            <div class="buttons" style="float: right;">
	                            <div class="highlight" style="display: auto;">
	                            	<input style="width: 80px;" type="submit" name="cadastrar" value="Filtrar" />
	                            </div>
                            </div>
                            <input style="width: 400px; padding: 3px 0px;" type="text" id="list_inadimplente_desc" name="filtro1_desc"/>
                        </div>
                        
                        <div style="clear:both"></div>
                        
                    </div>
                </div>
            </form>
            
            
            <div>
                <div class="form">
                    <div class="fields">
                        <div class="table" style="padding: 0px 5px 10px; border-bottom: 1px solid #ddd;">
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th class="left" style="width: 20px;">C�d</th>
                                        <th>Inadimplente</th>
                                        <th>Endere�o</th>
                                        <th>Cidade</th>
                                        <th>Telefone</th>
                                        <th>Celular</th>
                                        <th class="last">Excluir</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($inadimplentes as $inad): ?>
                                    <tr class="visualizar" id="<?php echo base_url().'inadimplente/editar/cod:'.$inad->ina_cod ?>">
                                        <td class="title"><?php echo $inad->ina_cod ?></td>
                                        <td><?php echo utf8_decode($inad->ina_nome) ?></td>
                                        <td><?php echo utf8_decode($inad->ina_endereco) ?></td>
                                        <td><?php echo utf8_decode($inad->ina_cidade) ?></td>
                                        <td><?php echo utf8_decode($inad->ina_foneres) ?></td>
                                        <td><?php echo utf8_decode($inad->ina_cel1) ?></td>
                                        <td id="<?php echo base_url().'inadimplente/excluir/cod:'.$inad->ina_cod ?>" class="last excluir">
                                            <a href="#">
                                                <img src="<?php echo $img . 'excluir.png' ?>" alt="+"/>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="pagination pagination-left">
                            <div class="results">
                                <span>Total de inadimplentes: <i><?php echo $total?></i></span>
                            </div>
                            <div class="pager">
                                <?php echo $paginacao; ?>
                            </div>
                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
(function ($) {

	var cookieInputStatus = function(obj) {
		$.cookie($(obj).attr('id'),$(obj).val());
	};	
	$('#form input[id], #form select[id]').change(function(){
		cookieInputStatus(this);
		//console.log($(this).val());
	});
	$('#form input[id], #form select[id]').each(function(){
		var objVal = $.cookie($(this).attr('id'));
		if (objVal!=undefined) {
			$(this).val(objVal);
		}
		//console.log($(this).val());
	});
	
})(jQuery);
</script>