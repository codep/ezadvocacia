<style>
	.squarebox { display:block; padding: 2px; clear: both; }
	.squarebox input[type="radio"] { width: 15px !important; margin-right: 5px !important; }
	.square {
		width: 16px;
		height: 16px;
		display: block;
		float: left;
		border: 1px dotted #ccc;
	}
	.square.all { background-color: #ffffff; }
	.square.ado { background-color: #ffd95c; }
	.square.dco { background-color: #b4eeb4; }
	.square.din { background-color: #00bfff; }
	.square.dca { background-color: #f9821b; }
	.square.dia { background-color: #b23aee; }
	.square.pes { background-color: #800000; }
	.square.ade { background-color: #d4d4d4; }
	.square.gbd { background-color: #836476; }
</style>
<script>
	$(document).ready(function(){
		

			
	});
	
</script>

<div id="content">
        <?php echo $sidebar; ?>
    <div id="right">
        <div class="box" style="min-height: 800px;">
            <div class="title">
                <h5>Pesquisar no Fiscal</h5>
            </div>
            <form id="form_fiscal" action="<?php echo base_url().'pesquisar/pesFiscal' ?>" method="post">
                <div class="form">
                    <?php echo $mensagem; ?>
                    <div class="fields" style="height: 450px">
						<table>
							<tr class="field">
								<td width="125"><input type="checkbox" id="ck_pesquisar_fiscal_credor" name="tipo_filtro_credor" value="1" /> <b>CREDOR</b>:</td>
								<td colspan="2">
		                            <select style="width: 500px;" name="credor" id="pesquisar_fiscal_credor" class="chosen" data-placeholder="Informe o nome fantasia do credor">
		                            	<option value=""></option>
		                            	<?php foreach($credores as $credor) : ?>
											<option value="<?php echo $credor->cre_cod; ?>"><?php echo $credor->cre_nome_fantasia; ?></option>
										<?php endforeach?>
		                            </select>
	                            </td>
							</tr>
							<tr class="field">
								<td width="125"><input type="checkbox" id="ck_pesquisar_fiscal_data" name="tipo_filtro_data" value="1" /> <b>DATA FISCAL</b>:</td>
                                <td colspan="2">
	                            	<label for="de">De:</label>
	                            	<input type="text" id="pesquisar_fiscal_data_de" name="fiscal_data_de" class="dataMascara" />
                                	<label for="ate">At�:</label>
                                	<input type="text" id="pesquisar_fiscal_data_ate" name="fiscal_data_ate" class="dataMascara" />									
								</td>
							</tr>
							<tr class="field">
								<td width="125"><input type="checkbox" id="ck_pesquisar_fiscal_setor" name="tipo_filtro_setor" value="1" /> <b>SETOR</b>:</td>
                                <td colspan="2">
                                	<input type="radio" id="pesquisar_fiscal_setor_juridico" name="setor" value="ADM" /><label for="ck_pesquisar_fiscal_setor_juridico">ADM</label>                                	
	                            	<input type="radio" id="pesquisar_fiscal_setor_administrativo" name="setor" value="JUD" /><label for="ck_pesquisar_fiscal_setor_administrativo">JUD</label>
								</td>
							</tr>							
							<tr class="field">
								<td colspan="3">
									<span class="squarebox"><input type="radio" id="pesquisar_fiscal_status_blank" name="fiscal_status" value="" />Todas as Cobran�as</span>
									<span class="squarebox"><span class="square all"></span><input type="radio" id="pesquisar_fiscal_status_all" name="fiscal_status" value="0" />Cobran�a sem status fiscal </span>
									<span class="squarebox"><span class="square ado"></span><input type="radio" id="pesquisar_fiscal_status_ado" name="fiscal_status" value="1" />Aguardando Documento</span>
									<span class="squarebox"><span class="square dco"></span><input type="radio" id="pesquisar_fiscal_status_dco" name="fiscal_status" value="2" />Documento Completo</span>
									<span class="squarebox"><span class="square din"></span><input type="radio" id="pesquisar_fiscal_status_din" name="fiscal_status" value="3" />Documento Incompleto</span>
									<span class="squarebox"><span class="square dca"></span><input type="radio" id="pesquisar_fiscal_status_dca" name="fiscal_status" value="4" />Documento Completo com Acordo</span>
									<span class="squarebox"><span class="square dia"></span><input type="radio" id="pesquisar_fiscal_status_dia" name="fiscal_status" value="5" />Documento Incompleto com Acordo</span>
									<span class="squarebox"><span class="square pes"></span><input type="radio" id="pesquisar_fiscal_status_pes" name="fiscal_status" value="6" />Para Estornar</span>
									<span class="squarebox"><span class="square ade"></span><input type="radio" id="pesquisar_fiscal_status_ade" name="fiscal_status" value="7" />Aguardando Devolu��o</span>
									<span class="squarebox"><span class="square gbd"></span><input type="radio" id="pesquisar_fiscal_status_gbd" name="fiscal_status" value="8" />Gerado Border� de Devolu��o</span>
	                            </td>
							</tr>
						</table>
                        <div style="clear:both"></div>
                        
                        <div class="divlast">
                            <div class="buttons" style="margin-top: 10px;">
                                <div class="highlight">
                                	<button type="submit">Pesquisar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
(function ($) {

	var cookieInputStatus = function(obj) {
		$.cookie($(obj).attr('id'),$(obj).val());
	};
	var cookieCheckboxStatus = function(obj) {
		var status = $(obj).is(':checked')?1:0;
		$.cookie($(obj).attr('id'),status);
	};
	var cookieRadioStatus = function(obj) {
		$.cookie($(obj).attr('name'),$(obj).val());
	};	
	$(':checkbox').change(function() {
		cookieCheckboxStatus(this);
		//console.log($.cookie($(this).attr('id')));
	});
	$(':checkbox').each(function(){
		if ($.cookie($(this).attr('id'))==1) {
			$(this).attr('checked',true);
		} else {
			$(this).removeAttr('checked');
		}
	});
	$(':radio').change(function() {
		cookieRadioStatus(this);
	});
	$(':radio').each(function(){
		if ($.cookie($(this).attr('name'))==$(this).val()) {
			$(this).attr('checked',true);
		} else {
			$(this).removeAttr('checked');
		}
	});
	$('#form_fiscal input:not(:checkbox, :radio), select').change(function(){
		cookieInputStatus(this);
	});
	$('#form_fiscal input:not(:checkbox, :radio), select').each(function(){
		var objVal = $.cookie($(this).attr('id'));
		if (objVal!=undefined) $(this).val(objVal);
	});
	
	$('select.chosen').chosen();
	
	$(".dataMascara").datepicker();
	
})(jQuery);
</script>