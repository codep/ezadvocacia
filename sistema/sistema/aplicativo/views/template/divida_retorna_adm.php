<style>
	.verde { color: green; }
	.vermelho { color: red; }
	#content div.box table.inputTbl td {
		padding: 2px;
		padding-left: 5px;
	}
	#content div.box table.inputTbl tfoot td {
		padding: 10px;
		font-weight: bold;
	}
	table .input {
		width: 90% !important; 
		font-weight: bold;
		padding: 5px;
	}
</style>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Retornar Cobran�a ao ADM</h5>
            </div>
            <form action="<?php echo base_url() . 'divida/retornaRecemEnviadoaoAdm'; ?>" method="post">
            	<input type="hidden" name="cob_cod" value="<?php echo $cobranca->cob_cod; ?>" />
            	<input type="hidden" name="cob_cod_rem" value="<?php echo $cobranca_rem; ?>" />
            	
                <div class="form">
                    <div class="fields">
                        <?php if (!empty($divida)) : ?>
                    	<div style="margin-top: 12px; margin-bottom: 2px;" class="blocoTitulo">Retornar D�vida</div>
                        <div class="field">
                            <div class="label"><label>Documento:</label> <?php echo $divida->div_documento; ?></div>
                        </div>
                    	<div class="field">
                    		<div class="label"><label>Vencimento Inicial:</label> <?php echo convData($divida->div_venc_inicial); ?></div>
                    	</div>
                        <div class="field">
                        	<div class="label"><label>Total:</label> <?php echo convMoney($divida->div_total); ?></div><br>
                        </div>
                		<div class="field">
                			<div class="label"><label>Informa��es:</label> <?php echo $divida->div_info; ?></div>
                		</div>
                		<?php elseif (!empty($acordo)) : ?>
                    	<div style="margin-top: 12px; margin-bottom: 2px;" class="blocoTitulo">Retornar Acordo</div>
                        <div class="field">
                            <div class="label"><label>Proced�ncia:</label> <?php echo $acordo->aco_procedencia; ?></div>
                        </div>
                    	<div class="field">
                    		<div class="label"><label>Data Emiss�o:</label> <?php echo convData($acordo->aco_emissao); ?></div>
                    	</div>
                        <div class="field">
                        	<div class="label"><label>Valor Original:</label> <?php echo convMoney($acordo->aco_valor_original); ?></div><br>
                        </div>
                        <div class="field">
                        	<div class="label"><label>Valor Atualizado:</label> <?php echo convMoney($acordo->aco_valor_atualizado); ?></div><br>
                        </div>
                		<?php else : ?>
            			<div style="margin-top: 12px; margin-bottom: 2px;" class="blocoTitulo">Erro desconhecido...</div>
            			<?php endif; ?>
            			<?php if (count($acordos)>0) : ?>
                        <div style="margin-top: 12px; margin-bottom: 2px;" class="blocoTitulo">
                            Acordos originados dessa cobran�a:
                        </div>
                        <div class="field">
                        	<table class="inputTbl">
                        		<thead>
	                        		<tr>
											<th>Valor Original (R$)</th>                        			
	                        				<th>Valor Atualizado (R$)</th>
	                        				<th>SubSetor:</th>
	                        		</tr>
                        		</thead>
                        		<tbody>
                        			<?php foreach($acordos as $acordo) : ?>
	                        		<tr class="linhaBaseClone">
											<td>
	                                    		<?php echo convMoney($acordo->aco_valor_original); ?>											
											</td>                        			
	                        				<td>
	                        					<?php echo convMoney($acordo->aco_valor_atualizado); ?>
	                        				</td>
	                        				<td>
												<?php echo $acordo->cob_subsetor=='NAJ' ? 'N�O AJUIZADO' : 'AJUIZADO'; ?>
	                        				</td>
	                        		</tr>
	                        		<?php endforeach; ?>
                        		</tbody>
                        	</table>
                        </div>
                        <?php endif; ?>
						<b style="padding: 5px; display: block; font-size: 10px;" class="vermelho">ATEN��O: ao retornar a d�vida todos os acordos ser�o removidos.</b>
						<div style="text-align: center; margin-top: 10px;" class="buttons">
                            <div class="highlight">
                                <input type="submit" name="submit" value="CONFIRMAR RETORNO AO ADMINISTRATIVO">
                            </div>
                            <div class="highlight">
                                <a href="<?php echo base_url() . 'divida/ficha/cod:'.$cobranca->inadimplentes_ina_cod; ?>" class="ui-button ui-corner-all ui-state-default">CANCELAR</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

