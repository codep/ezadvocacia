<script type="text/javascript">
    $(document).ready(function(){

        $('.editaInad').click(function(){
                cod = $(this).attr('id');
                window.location.href="<?php echo base_url().'divida/ficha/cod:';?>"+cod;
        });
    });
</script>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Pesquisar cobranšas</h5>
            </div>
            <div class="blocoTitulo">
                Resultados da pesquisa por<br/>
                <span style="font-size: 10px"><?php echo $pesquisaPor;?></span>
            </div>
            <div class="table">
                <table id="products">
                    <thead>
                        <tr>
                            <th class="left">Credor</th>
                            <th>Inadimplente</th>
                            <th>Valor</th>
                            <th>Proc.</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php foreach ($cobrancas as $cobranca):?>
                        
                                    <tr class="editaInad" <?php echo "style='cursor: pointer;color:".($cobranca->cor == 'green'?'black':$cobranca->cor)."'"; ?> id="<?php echo $cobranca->ina_cod; ?>">
                                        <td> <?php echo utf8_decode($cobranca->cre_nome_fantasia); ?> </td>
                                        <td> <?php echo utf8_decode($cobranca->ina_nome); ?> </td>
                                        <td> <?php echo convMoney($cobranca->valor); ?> </td>
                                        <td> <?php echo ($cobranca->cor == 'green'?'-':$cobranca->setor); ?> </td>
                                    </tr>
                        <?php endforeach;?>        
                    </tbody>
                </table>
                <div class="pagination pagination-left">
                    <div class="results">
                        <span></span>
                    </div>
                </div>
                <br/>
                <br/>
            </div>
<!--            <form id="form" action="">-->
                <div class="form">
                    <div class="fields">
                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <a href="<?php echo base_url().'pesquisar/cobrancas' ?>" style="text-decoration: none;" >
                                <div style="margin-left: 7px;" class="highlight">
                                    <input type="submit" name="submit.highlight" value="Nova pesquisa" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
<!--            </form>-->
        </div>
    </div>
</div>
