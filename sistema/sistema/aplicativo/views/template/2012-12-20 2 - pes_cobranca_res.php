<script type="text/javascript">
    $(document).ready(function(){

        $('.editaInad').click(function(){
                cod = $(this).attr('id');
                window.location.href="<?php echo base_url().'divida/ficha/cod:';?>"+cod;
        });
    });
</script>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Pesquisar cobranšas</h5>
            </div>
            <div class="blocoTitulo">
                Resultados da pesquisa por<br/>
                <span style="font-size: 10px"><?php echo $pesquisaPor;?></span>
            </div>
            <div class="table">
                <table id="products">
                    <thead>
                        <tr>
                            <th class="left">Credor</th>
                            <th>Inadimplente</th>
                            <th>Valor</th>
                            <th>Proc.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for ($i=0; $i< sizeof($cobrancas); $i++)
                        {
                                for($j=0; $j< sizeof($cobrancas[$i]); $j++){
                                      
                                    $cobranca = array("chuck" => array("cre_nome" => $cobrancas[$i][$j]->cre_nome_fantasia, "ina_cod" => $cobrancas[$i][$j]->ina_cod, "ina_nome" => $cobrancas[$i][$j]->ina_nome, "valor" => $cobrancas[$i][$j]->valor));
                        ?>
                                    <tr class="editaInad" style="cursor: pointer;color:<?php echo isset($cobrancas[$i][$j]->cor) ? $cobrancas[$i][$j]->cor : 'black'; ?>" id="<?php echo $cobrancas[$i][$j]->ina_cod; ?>">
                                        <td> <?php echo utf8_decode($cobrancas[$i][$j]->cre_nome_fantasia); ?> </td>
                                        <td> <?php echo utf8_decode($cobrancas[$i][$j]->ina_nome); ?> </td>
                                        <td> <?php echo convMoney($cobrancas[$i][$j]->valor); ?> </td>
                                        <td> <?php echo isset($cobrancas[$i][$j]->setor) ? $cobrancas[$i][$j]->setor : '-'; ?> </td>
                                    </tr>
                        <?php }
                                } ?>
                    </tbody>
                </table>
                <div class="pagination pagination-left">
                    <div class="results">
                        <span></span>
                    </div>
                </div>
                <br/>
                <br/>
            </div>
<!--            <form id="form" action="">-->
                <div class="form">
                    <div class="fields">
                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <a href="<?php echo base_url().'pesquisar/cobrancas' ?>" style="text-decoration: none;" >
                                <div style="margin-left: 7px;" class="highlight">
                                    <input type="submit" name="submit.highlight" value="Nova pesquisa" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
<!--            </form>-->
        </div>
    </div>
</div>
