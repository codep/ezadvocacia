<script type="text/javascript">
    $(document).ready(function(){

        $('.editaInad').click(function(){
                cod = $(this).attr('id');
                window.location.href="<?php echo base_url().'divida/ficha/cod:';?>"+cod;
        });
        
		$('input#search').quicksearch('#products tbody tr:not(.titulo)', {});
/*
		//AUTO FOCUS
		window.setTimeout(function(){
			$('input#search').focus();	
		},300);
*/	        
        
    });
</script>
<style>
	.venc_proximo {
		background-color: #FFFF99 !important;
	}
	.venc_atrasado {
		background-color: #FF6666 !important;
	}
	.venc_normal {
		background-color: #FFFFFF !important;
	}
	span.venc_proximo,
	span.venc_atrasado,
	span.venc_normal {
		display: block;
		float: left;
		border: 1px solid #000;
		padding: 5px;
		font-weight: bold;
	}
</style>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Pesquisar Cobranšas por Vencimento</h5>
            </div>
            <?php if (!empty($credor)) : ?>
            <h3 style="text-align: center;"><?php echo $credor->cre_nome_fantasia; ?></h3>
            <?php endif; ?>
            <div class="blocoTitulo">
                <span style="font-size: 10px">Filtro: <input type="text" id="search" style="font-size: 18px; padding: 5px; width: 500px;" /></span>
            </div>
            <div class="table">
            <div>
            	<span class="venc_atrasado">ARQUIVAMENTO EM MENOS DE 3 DIAS</span>
            	<span class="venc_proximo">ARQUIVAMENTO EM MENOS DE 15 DIAS</span>
            </div>

                <table id="products">
                    <thead>
                        <tr>
                        	<?php if (empty($credor)) : ?>
                        	<th>Credor</th>
                        	<?php endif; ?>
                            <th>Inadimplente</th>
                            <th>Arquivamento</th>
                            <th width="100"></th>
                        </tr>
                    </thead>
                    <tbody>                        
                    	<?php foreach ($cobrancas as $cobranca): ?>
                    		<?php //var_dump($cobranca); die() ?>
                    		
                    		<?php
                    			if ($cobranca->processo_arq_data_dias > 3 && $cobranca->processo_arq_data_dias <= 15) {
                    				$class= 'venc_proximo';
                    			} elseif ($cobranca->processo_arq_data_dias <= 3) {
                    				$class = 'venc_atrasado';
                    			} else {
                    				$class = 'venc_normal';
                    			}
							?>
                    		
                        <tr>
                        	<?php if (empty($credor)) : ?>
                        	<td> <?php echo utf8_decode($cobranca->cre_nome_fantasia); ?> </td>
                        	<?php endif; ?>
                            <td> <?php echo utf8_decode($cobranca->ina_nome); ?> </td>
                            <td class="<?php echo $class; ?>"> <?php echo convDataBanco($cobranca->processo_arq_data).'<br><i>'. $cobranca->processo_arq_data_dias .'</i> dias'; ?> </td>
                            <td class="last">
                                <div class="buttons">
                                    <input style="width: 50px;" class="visualizar editaInad" id="<?php echo $cobranca->inadimplentes_ina_cod; ?>" type="button" value="Ficha" />
                                    <input style="width: 40px;" class="visualizar" onclick="window.location.href='<?php echo base_url().'ro/novo/cod:'.$cobranca->cob_cod.'-'; ?>'; return false;" type="button" value="RO" />
                                </div>
                            </td>
                        </tr>
                        <?php endforeach;?>                
                    </tbody>
                </table>
                <div class="pagination pagination-left">
                    <div class="results">
                        <span>Total de <?php echo count($cobrancas); ?> registros encontrados</span>
                    </div>
                </div>
                <br/>
                <br/>
            </div>
<!--            <form id="form" action="">-->
                <div class="form">
                    <div class="fields">
                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <a href="<?php echo base_url().'pesquisar/juridico' ?>" style="text-decoration: none;" >
                                <div style="margin-left: 7px;" class="highlight">
                                    <input type="submit" name="submit.highlight" value="Nova pesquisa" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
<!--            </form>-->
        </div>
    </div>
</div>
