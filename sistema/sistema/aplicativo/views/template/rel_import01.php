<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img.'logo_elisangela.jpg';?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br|www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo $title ;?></p>
        </div>
    </div>
    <!-- ************** importacoes sucesso ************** -->
    <?php if($importacoes != ""):?>
    <h3 style="text-align: center;">IMPORTA��ES SUCESSO</h3>
    <table id="tableAdm01" cellpadding="0" cellspacing="0">
        <?php foreach ($importacoes as $credor):?>
        <?php $totalCapital = 0;?>
        <thead>
            <tr ><th colspan="9" class="last" style="background-color: #CCC; padding: 5px 0;"><?php echo utf8_decode($credor->cre_nome_fantasia);?></th></tr>
            <tr>
                <th>D. Envio</th>
                <th>H. Envio</th>
                <th>D. Cadastro</th>
                <th>H. Cadastro</th>
                <th>T�tulo de Cr�dito</th>
                <th>Capital</th>
                <th>Venc</th>
                <th>Tipo de Repasse</th>
                <th>Recup.</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($credor->importacoes as $import):?>
            <?php $totalCapital += $import->capital;?>
            <tr><td colspan="9" class="last negrito"><?php echo utf8_decode($import->inadimplente);?></td></tr>
            <tr>
                <td class="alignCenter"><?php echo $import->data_envio;?></td>
                <td class="alignCenter"><?php echo $import->hora_envio;?></td>
                <td class="alignCenter"><?php echo $import->data_cadastro;?></td>
                <td class="alignCenter"><?php echo $import->hora_cadastro;?></td>
                <td class="alignCenter"><?php echo $import->titulo_credito;?></td>
                <td class="alignCenter"><?php echo convMoney($import->capital);?></td>
                <td class="alignCenter"><?php echo $import->vencimento;?></td>
                <td class="alignCenter">
                    <?php if($import->tipo_repasse != "Nao Importada"){ foreach ($import->tipo_repasse as $repasse){echo $repasse->rep_nome ."<br/>";}} else{echo $import->tipo_repasse;}?>
                </td>
                <td class="last alignCenter"><?php echo $import->recuperador;?></td>
            </tr>
            <?php endforeach;?>
            <tr>
                <td colspan="8" id="tdTotalA">TOTAL R$</td>
                <td id="tdTotalB" class="last"><?php echo convMoney($totalCapital); ?></td>
            </tr>
            <tr>
                <td colspan="8" class="tdTotalA">QTD INADIMPLENTES</td>
                <td class="alignCenter negrito last"><?php echo sizeof($credor->importacoes); ?></td>
            </tr>
            <tr><td colspan="10" class="last" style="height: 15px; border:none;"></td></tr>
        </tbody>
        <?php endforeach;?>
    </table>
    <?php endif;?>
    <!-- ************** fim importacoes sucesso ************** -->
    
    <!-- ............... IMPORTACOES ERRO ............... -->
    <?php if($importacoesErro != ""):?>
    <h3 style="text-align: center;">IMPORTA��ES ERRO</h3>
    <table id="tableAdm02" cellpadding="0" cellspacing="0">
        <?php foreach ($importacoesErro as $importErr):?>
        <thead>
            <tr><th colspan="4" class="last" style="background-color: #CCC; padding: 5px 0;"><?php echo $importErr->cre_nome_fantasia;?></th></tr>
            <tr>
                <th>Inadimplente</th>
                <th>Data</th>
                <th class="last">Hora</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($importErr->importsComErro as $impErro):?>
            <tr>
                <td><?php echo utf8_decode($impErro->ina_nome);?></td>
                <td class="alignCenter"><?php echo $impErro->data_;?></td>
                <td class="alignCenter last"><?php echo $impErro->hora;?></td>
            </tr>
            <?php endforeach;?>
            <tr>
                <td class="tdTotalA" colspan="2">QTD IMPORTA��ES</td>
                <td class="negrito last"><?php echo sizeof($importErr->importsComErro);?></td>
            </tr>
            <tr>
                <td class=" alignCenter last" colspan="3" style="height: 15px; border: none"></td>
            </tr>
        </tbody>
        <?php endforeach;?>
    </table>
    <?php endif;?>
    <!-- ............... FIM IMPORTACOES ERRO ............... -->
</div>
