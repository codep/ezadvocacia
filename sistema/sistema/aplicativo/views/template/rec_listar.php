<script type="text/javascript">
    $(document).ready(function(){
        $('.visualizar').click(function(e){
            window.location.href='http://webhost/cobranca/divida/detalhes';
        });
        $('input[value="Ficha"]').click(function(e){
            window.location.href='http://webhost/cobranca/divida/ficha';
        });

        if(<?php echo isset ($aba)?>){
            $('a[href="<?php echo $aba?>"]').trigger('click');
        }

    });
</script>
<div id="content">

    <?php echo $sidebar; ?>

    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Meus Recebimentos</h5>
                <ul class="links">
                    <li><a href="#hoje">Hoje</a></li>
                    <li><a href="#semana">Semana</a></li>
                    <li><a href="#mes">M�s</a></li>
                    <li><a href="#proxmes">M�s anterior</a></li>
                </ul>
            </div>
            <div id="hoje">
                <div class="form">
                    <div class="fields">
                        <form id="form" method="post" action="meus">
                            <div class="field  field-first">
                                <div class="divleft" style="width: 345px;">
                                    <div style="width: 129px; padding-left: 1px;" class="label">
                                        <label for="nome">Selecionar credor:</label>
                                    </div>
                                    <div class="select">
                                        <select style="width: 205px; margin-left: 5px;" id="est_civil" name="filtroCredor">
                                            <option value="todos">---Todos---</option>
                                            <?php foreach ($todosCredores as $credor): ?>
                                                <option value="<?php echo $credor->cre_cod ?>"<?php echo $credor->cre_cod == $filtroCredor ? 'selected' : ''; ?>><?php echo $credor->cre_nome_fantasia ?></option>
                                            <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 87px; margin: 0px;">
                                        <div class="buttons">
                                            <div class="highlight">
                                                <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                                <input type="hidden" name="aba" value="#hoje"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="table" style="padding: 0px 5px 10px;">
                                <table id="products">
                                    <thead>
                                        <tr>
                                            <th class="left" style="width: 50px;">D�vida</th>
                                            <th>Inadimplente</th>
                                            <th>Credor</th>
                                            <th>Recebido</th>
                                            <th style="width: 85px;">Valor</th>
                                            <th class="last" style="width: 55px;">2� Via</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php $totalRecebido = 0; $totalDescontos=0; foreach ($meusRecebimentosHoje as $recebimento): ?>
                                       <tr>
                                         <td class="title" style="width: 50px;"><?php echo $recebimento->cobranca_cob_cod; ?></td>
                                         <td><?php echo utf8_decode($recebimento->ina_nome); ?></td>
                                         <td><?php echo utf8_decode($recebimento->cre_nome_fantasia); ?></td>
                                         <td><?php echo convDataBanco($recebimento->reb_data); ?></td>
                                         <td><?php echo convMoney($recebimento->reb_valor - $recebimento->reb_desconto); ?></td>
                                         <td class="last">
                                            <a href="<?php echo base_url().'impressao/recibo2/cod:'.$recebimento->rec_cod ?>">
                                              <img src="<?php echo $img . 'recibo_imprimir.png' ?>" alt="+"/>
                                            </a>
                                         </td>
                                      </tr>
                                      <?php $totalRecebido += $recebimento->reb_valor;
                                      $totalDescontos += $recebimento->reb_desconto;
                                      endforeach; ?>
                                            <tr>
                                              <td class="title" colspan="4" style="width: 30px;"><b>Total</b></td>
                                              <td colspan="2" class="last">
                                                 <b><?php echo convMoney($totalRecebido - $totalDescontos);?>  </b>
                                              </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="semana">
                            <div class="form">
                                <div class="fields">
                                    <form id="form" method="post" action="meus">
                                        <div class="field  field-first">
                                            <div class="divleft" style="width: 345px;">
                                                <div style="width: 129px; padding-left: 1px;" class="label">
                                                    <label for="nome">Selecionar credor:</label>
                                                </div>
                                                <div class="select">
                                                    <select style="width: 205px; margin-left: 5px;" id="est_civil" name="filtroCredor">
                                                        <option value="todos">---Todos---</option>
                                                          <?php foreach ($todosCredores as $credor): ?>
                                                        <option value="<?php echo $credor->cre_cod ?>"<?php echo $credor->cre_cod == $filtroCredor ? 'selected' : ''; ?>><?php echo $credor->cre_nome_fantasia ?></option>
                                                          <?php endforeach; ?>
                                                    </select>
                                               </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="filtrar" value="Filtrar" />
                                                    <input type="hidden" name="aba" value="#semana"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 50px;">D�vida</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Recebido</th>
                                                <th style="width: 85px;">Valor</th>
                                                <th class="last" style="width: 55px;">2� Via</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                    <?php $totalRecebido = 0; $totalDescontos=0; foreach ($meusRecebimentosSemana as $recebimentosemana): ?>
                                                        <tr>
                                                            <td class="title" style="width: 50px;"><?php echo $recebimentosemana->cobranca_cob_cod; ?></td>
                                                            <td><?php echo utf8_decode($recebimentosemana->ina_nome);?></td>
                                                            <td><?php echo utf8_decode($recebimentosemana->cre_nome_fantasia);?></td>
                                                            <td><?php echo convDataBanco($recebimentosemana->reb_data); ?></td>
                                                            <td><?php echo convMoney($recebimentosemana->reb_valor);?></td>
                                                            <td class="last">
                                                                    <a href="<?php echo base_url() . 'impressao/recibo2/cod:' . $recebimentosemana->rec_cod ?>">
                                                                        <img src="<?php echo $img . 'recibo_imprimir.png' ?>" alt="+"/>
                                                                    </a>
                                                                </td>
                                                        </tr>
                                                         <?php $totalRecebido += $recebimentosemana->reb_valor;
                                                               $totalDescontos += $recebimentosemana->reb_desconto;
                                                             endforeach;?>
                                                    <tr>
                                                        <td class="title" colspan="4" style="width: 30px;"><b>Total</b></td>
                                                        <td colspan="2" class="last">
                                                            <b><?php echo convMoney($totalRecebido - $totalDescontos); ?></b>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="mes">
                                <div class="form">
                                    <div class="fields">
                                        <form id="form" method="post" action="meus">
                                            <div class="field  field-first">
                                                <div class="divleft" style="width: 345px;">
                                                    <div style="width: 129px; padding-left: 1px;" class="label">
                                                        <label for="nome">Selecionar credor:</label>
                                                    </div>
                                                    <div class="select">
                                                        <select style="width: 205px; margin-left: 5px;" id="est_civil" name="filtroCredor">
                                                            <option value="todos">---Todos---</option>
                                            <?php foreach ($todosCredores as $credor): ?>
                                                        <option value="<?php echo $credor->cre_cod ?>"<?php echo $credor->cre_cod == $filtroCredor ? 'selected' : ''; ?>><?php echo $credor->cre_nome_fantasia ?></option>
                                            <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                                <div class="buttons">
                                                    <div class="highlight">
                                                        <input style="width: 85px;" type="submit" name="filtrar" value="Filtrar" />
                                                        <input type="hidden" name="aba" value="#mes"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="table" style="padding: 0px 5px 10px;">
                                        <table id="products">
                                            <thead>
                                                <tr>
                                                    <th class="left" style="width: 50px;">D�vida</th>
                                                    <th>Inadimplente</th>
                                                    <th>Credor</th>
                                                    <th>Recebido</th>
                                                    <th style="width: 85px;">Valor</th>
                                                    <th class="last" style="width: 55px;">2� Via</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <?php $totalRecebido = 0; $totalDescontos=0; foreach ($meusRecebimentosMes as $recebimentoMes): ?>
                                                            <td class="title" style="width: 50px;"><?php echo $recebimentoMes->cobranca_cob_cod;?></td>
                                                            <td><?php echo utf8_decode($recebimentoMes->ina_nome);?></td>
                                                            <td><?php echo utf8_decode($recebimentoMes->cre_nome_fantasia);?></td>
                                                            <td><?php echo convDataBanco($recebimentoMes->reb_data); ?></td>
                                                            <td><?php echo convMoney($recebimentoMes->reb_valor); ?></td>
                                                       <td class="last">
                                                          <a href="<?php echo base_url().'impressao/recibo2/cod:'.$recebimentoMes->rec_cod ?>">
                                                            <img src="<?php echo $img . 'recibo_imprimir.png' ?>" alt="+"/>
                                                          </a>
                                                       </td>
                                                  </tr>
                                                         <?php $totalRecebido += $recebimentoMes->reb_valor;
                                                               $totalDescontos += $recebimentoMes->reb_desconto;
                                                        endforeach; ?>
                                                        <tr>
                                                            <td class="title" colspan="4" style="width: 30px;"><b>Total</b></td>
                                                            <td colspan="2" class="last">
                                                                <b><?php echo convMoney($totalRecebido - $totalDescontos); ?></b>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="proxmes">
                                    <div class="form">
                                        <div class="fields">
                                            <form id="form" method="post" action="meus">
                                                <div class="field  field-first">
                                                    <div class="divleft" style="width: 345px;">
                                                        <div style="width: 129px; padding-left: 1px;" class="label">
                                                            <label for="nome">Selecionar credor:</label>
                                                        </div>
                                                        <div class="select">
                                                            <select style="width: 205px; margin-left: 5px;" id="est_civil" name="filtroCredor">
                                                                <option value="todos">---Todos---</option>
                                            <?php foreach ($todosCredores as $credor): ?>
                                                            <option value="<?php echo $credor->cre_cod ?>"<?php echo $credor->cre_cod == $filtroCredor ? 'selected' : ''; ?>><?php echo $credor->cre_nome_fantasia ?></option>
                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="divleftlast" style="width: 87px; margin: 0px;">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="filtrar" value="Filtrar" />
                                                            <input type="hidden" name="aba" value="#proxmes"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="table" style="padding: 0px 5px 10px;">
                                            <table id="products">
                                                <thead>
                                                    <tr>
                                                        <th class="left" style="width: 50px;">D�vida</th>
                                                        <th>Inadimplente</th>
                                                        <th>Credor</th>
                                                        <th>Recebido</th>
                                                        <th style="width: 85px;">Valor</th>
                                                        <th class="last" style="width: 55px;">2� Via</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                  <?php $totalRecebido = 0; $totalDescontos=0; foreach ($meusRecebimentosProximoMes as $recebProxMes): ?>
                                   <tr>
                                     <td class="title" style="width: 30px;"><?php echo $recebProxMes->cobranca_cob_cod ?></td>
                                     <td><?php echo utf8_decode($recebProxMes->ina_nome);?></td>
                                     <td><?php echo utf8_decode($recebProxMes->cre_nome_fantasia);?></td>
                                     <td><?php echo convDataBanco($recebProxMes->reb_data); ?></td>
                                     <td><?php echo convMoney($recebProxMes->reb_valor); ?></td>
                                     <td class="last">
                                          <a href="<?php echo base_url() . 'impressao/recibo2/cod:' . $recebProxMes->rec_cod ?>">
                                              <img src="<?php echo $img . 'recibo_imprimir.png' ?>" alt="+"/>
                                          </a>
                                      </td>
                                   </tr>
                                       <?php $totalRecebido += $recebProxMes->reb_valor;
                                             $totalDescontos += $recebProxMes->reb_desconto;
                                       endforeach; ?>
                                    <tr>
                                       <td class="title" colspan="4" style="width: 30px;"><b>Total</b></td>
                                       <td colspan="2" class="last">
                                         <b><?php echo convMoney($totalRecebido - $totalDescontos); ?></b>                                                </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
