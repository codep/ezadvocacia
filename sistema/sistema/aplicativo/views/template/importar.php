<script type="text/javascript">
</script>
<style type="text/css">
    a{
        color: black;
    }
    .tabela{
        width: 702px !important;
        margin: 7px 0 7px 20px !important;
    }
    .tabela td{
        padding: 2px !important;
        text-align: center !important;
    }
    #content div.box table th{
    padding: 5px;
    vertical-align: middle;
}
.vertCenter{
    vertical-align: middle;
    text-align: center;
}
.okRed{
    color: red;
}
.okRed:hover{
    color: red !important;
}

</style>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Importa��o</h5>
            </div>
            <?php echo $mensagem ?>
            <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                    Tabela de Importa��o
            </div>
            <table class="tabela">
                <thead>
                    <tr>
                        <th colspan="5">StudioA Web (111.111.111/1111-11)</th>
                        <th colspan="4" class="last">
                            <div style="float: left;">
                                <select style="width: 140px;">
                                    <option>Aplicar Repasse</option>
                                    <option>01</option>
                                    <option>02</option>
                                    <option>03</option>
                                    <option>04</option>
                                    <option>05</option>
                                </select>
                            </div>
                            <div style="padding-top: 2px; float: left; margin-left: 2px;">
                                <input type="button" value="Todos"/>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th>Data</th>
                        <th>Inad</th>
                        <th>Cap</th>
                        <th>Venc</th>
                        <th>Repasse</th>
                        <th>Parc</th>
                        <th>Dup</th>
                        <th>Import</th>
                        <th class="last">Efetivar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>23-02<br/>2012</td>
                        <td><a hraf="#">Assaf da Silva Sauro Simpson</a></td>
                        <td class="vertCenter">R$ 300,00</td>
                        <td>23-02<br/>2012</td>
                        <td>
                            <div style="width: 45px; padding-left: 15px">
                                <select>
                                    <option>01</option>
                                    <option>02</option>
                                    <option>03</option>
                                    <option>04</option>
                                    <option>05</option>
                                </select>
                            </div>
                        </td>
                        <td class="vertCenter">12</td>
                        <td class="vertCenter">Sim</td>
                        <td class="vertCenter"><a href="#">Erro</a></td>
                        <td class="vertCenter last"><a class="okRed" href="#">[OK]</a></td>
                    </tr>
                    <tr>
                        <td>43-02<br/>2012</td>
                        <td><a hraf="#">Jo�o da Silva Sauro</a></td>
                        <td class="vertCenter">R$ 3000,00</td>
                        <td>24-02<br/>2012</td>
                        <td>
                            <div style="width: 45px; padding-left: 15px">
                                <select>
                                    <option>01</option>
                                    <option>02</option>
                                    <option>03</option>
                                    <option>04</option>
                                    <option>05</option>
                                </select>
                            </div>
                        </td>
                        <td class="vertCenter">15</td>
                        <td class="vertCenter">N�o</td>
                        <td class="vertCenter"><a href="#">Sucesso</a></td>
                        <td class="vertCenter last"><a class="okRed" href="#">[OK]</a></td>
                    </tr>
                </tbody>
            </table>
            <table class="tabela">
                <thead>
                    <tr>
                        <th colspan="5">Radioval (222.222.222/2222-22)</th>
                        <th colspan="4" class="last">
                            <div style="float: left;">
                                <select style="width: 140px;">
                                    <option>Aplicar Repasse</option>
                                    <option>01</option>
                                    <option>02</option>
                                    <option>03</option>
                                    <option>04</option>
                                    <option>05</option>
                                </select>
                            </div>
                            <div style="padding-top: 2px; float: left; margin-left: 2px;">
                                <input type="button" value="Todos"/>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th>Data</th>
                        <th>Inad</th>
                        <th>Cap</th>
                        <th>Venc</th>
                        <th>Repasse</th>
                        <th>Parc</th>
                        <th>Dup</th>
                        <th>Import</th>
                        <th class="last">Efetivar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>23-02<br/>2012</td>
                        <td><a hraf="#">Assaf da Silva Sauro Simpson</a></td>
                        <td class="vertCenter">R$ 300,00</td>
                        <td>23-02<br/>2012</td>
                        <td>
                            <div style="width: 45px; padding-left: 15px">
                                <select>
                                    <option>01</option>
                                    <option>02</option>
                                    <option>03</option>
                                    <option>04</option>
                                    <option>05</option>
                                </select>
                            </div>
                        </td>
                        <td class="vertCenter">12</td>
                        <td class="vertCenter">Sim</td>
                        <td class="vertCenter"><a href="#">Erro</a></td>
                        <td class="vertCenter last"><a class="okRed" href="#">[OK]</a></td>
                    </tr>
                    <tr>
                        <td>43-02<br/>2012</td>
                        <td><a hraf="#">Jo�o da Silva Sauro</a></td>
                        <td class="vertCenter">R$ 3000,00</td>
                        <td>24-02<br/>2012</td>
                        <td>
                            <div style="width: 45px; padding-left: 15px">
                                <select>
                                    <option>01</option>
                                    <option>02</option>
                                    <option>03</option>
                                    <option>04</option>
                                    <option>05</option>
                                </select>
                            </div>
                        </td>
                        <td class="vertCenter">15</td>
                        <td class="vertCenter">N�o</td>
                        <td class="vertCenter"><a href="#">Sucesso</a></td>
                        <td class="vertCenter last"><a class="okRed" href="#">[OK]</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>