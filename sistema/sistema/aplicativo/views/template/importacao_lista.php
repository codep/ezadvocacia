<script type="text/javascript">
    $(document).ready(function(){
        $('.comboRepassesTodos').show();
        $('.comboRepassesDivida').show();
        $('.ui-selectmenu').remove();
        
        var v1;
        
        //SETA O MESMO REPASSE PARA TODAS AS D�VIDAS DO CREDOR EM QUEST�O
        $('.comboRepassesTodos').change(function(){
            var comboAtual = $(this).attr('id');
            v1 = $('#'+comboAtual+' option:selected').val();
            $('.'+comboAtual).val(v1);
        });
        
        //VERIFICA SE EXISTE ALGUM REPASSE SEM SELE��O
        $('.forms').submit(function(){
            var aux1 = $(this).attr('id');
            var aux2 = aux1.split('_');
            var formAtual = aux2[1];
            
            var faltaRepasse = 0;
            
            $('.comboRepassesDivida.'+formAtual).each(function(){
                var repasseAtual = $(this).val();
                if(repasseAtual == 0)
                {
                    alert('Selecione repasse para todas as d�vidas');
                    faltaRepasse = 1;
                    return false;
                }  
            });
            if(faltaRepasse == 1)
                return false;
        });
        
        $('.okGreen').click(function(){
            
            var aux = '#comboRepassesDivida'+$(this).attr('id');
            var repasse = "";
            $(aux).each(function () {;
                    repasse += $(this).val() + "";
            });
            
            if(repasse == 0){
                alert('Voc� precisa selecionar um repasse');
                return false;    
            }else{
                var link = $(this).attr('rel');
                link += '/cod:'+$(this).attr('id')+'-'+repasse;
                window.location.href = link;
                return false;
            }
            
            //            //            comboRepassesDivida
        });
        
        $('.btAcao').click(function(e){
        	e.preventDefault();
        	bttxt = $(this).val();
        	url = $(this).attr('rel');
        	if (confirm("Deseja mesmo realizar essa a��o?\n\n>>> "+bttxt+"\n\n*A a��o � efetuada apenas para esse credor.")==true) {
        		window.location = url;
        	}
        });
        
    });
</script>
<style type="text/css">
    a{
        color: black;
    }
    .tabela{
        width: 702px !important;
        margin: 7px 0 7px 20px !important;
    }
    .tabela td{
        padding: 2px !important;
        text-align: center !important;
    }
    #content div.box table th{
        padding: 5px;
        vertical-align: middle;
    }
    .vertCenter{
        vertical-align: middle;
        text-align: center;
    }
    .okGreen{
        color: green;
    }
    .okGreen:hover{
        color: red !important;
    }

</style>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Importa��o</h5>
            </div>
            <?php echo $mensagem ?>
            <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
            <?php
                echo $mensagemTitulo;  
            ?>
            </div>

            <?php foreach ($credoresImportPendentes as $credor): ?>            
                <form action="<?php echo base_url() . 'importacao/efetivarTodas' ?>" method="post" id="form_<?php echo $credor->div_cre_cod ?>" class="forms">
                    <table style="margin-bottom: 10px;" class="tabela">
                        <thead>
                            <tr>
                                <th colspan="5"><?php echo $credor->cre_nome_fantasia . ' (' . $credor->cre_cpf_cnpj . ')' ?></th>
                                <th colspan="4" class="last">
                                    <select style="width: 140px;" class="comboRepassesTodos" id="<?php echo $credor->div_cre_cod ?>">
                                        <option value="0">[Aplicar Repasse]</option>
                                        <?php foreach ($repasses as $repasse): ?>
                                            <?php foreach ($repasse as $rep): ?>
                                                <?php if ($rep->rep_cod != ''): ?>
                                                    <option value="<?php echo $rep->rep_cod; ?>"><?php echo $rep->rep_nome; ?></option>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endforeach; ?>
                                    </select>                   
                                </th>
                            </tr>
                            <tr>
                                <th>Data</th>
                                <th>Inad</th>
                                <th>Cap</th>
                                <th>Venc</th>
                                <th>Repasse</th>
                                <th>Parcelas</th>
                                <th>Dup</th>
                                <th>Import</th>
                                <th class="last">Efetivar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($importacoesPendentes[$credor->div_cre_cod] as $divida): ?>
                                <tr>
                                    <td><?php echo convData($divida->div_emissao, 'd') ?></td>
                                    <td><?php echo $divida->ina_nome ?></td>
                                    <td><?php echo convMoney($divida->div_total) ?></td>
                                    <td><?php echo convData($divida->div_venc_inicial, 'd') ?></td>
                                    <td>
                                        <select id="<?php echo 'comboRepassesDivida' . $divida->div_cod ?>" style="width: 100px;" class="comboRepassesDivida <?php echo $credor->div_cre_cod ?>" name="repasse_div_<?php echo $divida->div_cod ?>">
                                            <option value="0">[Repasse]</option>
                                            <?php foreach ($repasses as $repasse): ?>
                                                <?php foreach($repasse as $rep): ?>
                                                    <?php if ($rep->rep_cod != ''): ?>
                                                        <option value="<?php echo $rep->rep_cod; ?>"><?php echo $rep->rep_nome; ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td><?php echo $divida->div_qtd_parc ?></td>
                                    <td><?php echo $divida->imp_duplicidade == '0' ? 'N�o' : 'Sim' ?></td>
                                    <td>N�o</td>
                                    <td class="vertCenter last">
                                    	<input type="button" id="<?php echo $divida->div_cod ?>" class="okGreen ui-button ui-widget ui-state-default ui-corner-all" rel="<?php echo base_url() . 'importacao/efetivar' ?>" value="Efetivar" />
                                    	<input type="button" rel="<?php echo base_url() . 'importacao/removerDivida/'.$divida->div_cod ?>" class="btAcao ui-button ui-widget ui-state-default ui-corner-all" value="Remover" />
                                	</td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div style="text-align: center;">
                    	<input type="submit" name="efetivar_todas" value="Efetivar Todas" class="btnEfetivar"/>
                    	<input type="button" rel="<?php echo base_url() . 'importacao/removerDuplicadas/'.$credor->div_cre_cod ?>" class="btAcao ui-button ui-widget ui-state-default ui-corner-all" value="Remover Duplicadas" />
                    </div>
                </form>
            <?php endforeach; ?>
            <br/>
            <a style="display: block; margin-top: 15px; text-align: center; font-size: 20px;" href="<?php echo base_url() . '' ?>">Voltar</a>
        </div>
    </div>
</div>
