<style type="text/css">
    #content p{
        font-weight: bold;
        padding-top: 5px !important;
        color: #2882D3 !important;
    }
    
    #recuperador_padrao{
        color: red;
    }
</style>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Dados da divida <?php echo $cobDados->cob_cod ?></h5>
            </div>
            <?php echo $mensagem; ?>
            <form id="formDiv" action="<?php echo base_url() . 'divida/incluir' ?>" method="post">
                <div class="form">
                    <div class="fields">
                        <div style="margin-top: 0px; margin-bottom: 13px;" class="blocoTitulo">
                            Dados do credor
                        </div>
                        <div id="hiddenCredor" style="display:none"></div>
                        <div class="field  field-first">
                            <div class="divleftleft" style="height:29px; width: 200px;">
                                <div class="label">
                                    <label>Pessoa:</label>
                                </div>
                                <p>
                                    <?php echo $cobDados->cre_pessoa == 'j' ? 'Juridica' : 'Fisica' ?>
                                </p>
                            </div>
                        </div>
                        <div class="field" id="dadosCredor">
                            <div class="divleft" style="width: 371px;">
                                <div style="width: 50px;" class="label">
                                    <label for="cre_nome">Nome:</label>
                                </div>
                                <p>
                                    <?php echo $cobDados->cre_nome_fantasia ?>
                                </p>
                            </div>

                            <div class="divleftlast" style="width: 300px;">
                                <div style="width: 76px; padding-left: 1px;" class="label">
                                    <label for="cre_cpf_cnpj">CNPJ/CPF:</label>
                                </div>
                                <p>
                                    <?php echo $cobDados->cre_cpf_cnpj ?>
                                </p>
                            </div>

                        </div>
                        <div class="field" id="repasses">
                            <div class="divleftlast" style="width: 471px; margin-left: 0px;">
                                <div style="width: 65px;" class="label">
                                    <label for="rep_cod">Repasse:</label>
                                </div>
                                <p>
                                    <?php echo utf8_decode($cobDados->rep_nome) ?>
                                </p>
                            </div>
                        </div>
                        <div style="margin-top: 12px; margin-bottom: 13px;" class="blocoTitulo">
                            Dados do inadimplente
                        </div>
                        <div id="hiddenInadimplente" style="display:none"></div>
                        <div class="field  field-first">
                            <div class="divleftleft" style="height:29px; width: 200px;">
                                <div class="label">
                                    <label>Pessoa:</label>
                                </div>
                                <p>
                                    <?php echo $cobDados->ina_pessoa == 'j' ? 'Juridica' : 'Fisica' ?>
                                </p>
                            </div>
                        </div>
                        <div class="field" id="dadosInadimplente">
                            <div class="divleft" style="width: 371px;">
                                <div style="width: 49px;" class="label">
                                    <label for="ina_nome">Nome:</label>
                                </div>
                                <p>
                                    <?php echo $cobDados->ina_nome ?>
                                </p>
                            </div>
                            <div class="divleftlast" style="width: 291px;">
                                <div style="width: 76px; padding-left: 1px;" class="label">
                                    <label for="ina_cpf_cnpj">CPF/CNPJ:</label>
                                </div>
                                <p>
                                    <?php echo $cobDados->ina_cpf_cnpj ?>
                                </p>
                            </div>
                        </div>
                        <div id="dadosDivida">
                            <div style="margin-top: 12px; margin-bottom: 9px;" class="blocoTitulo">
                                Dados da d�vida
                            </div>
                            <?php if ($cobDados->banco): ?>
                                <div style="padding-top: 5px;" class="field">
                                    <div class="divleft" style="width: 206px;">
                                        <div style="width: 86px; padding-left: 1px;" class="label">
                                            <label>Documento:</label>
                                        </div>
                                        <p>
                                            <?php echo $cobDados->documento ?>
                                        </p>
                                    </div>
                                    <div class="divleftlast" style="width: 462px;">
                                        <div style="width: 46px; padding-left: 1px;" class="label">
                                            <label for="alinea">Banco:</label>
                                        </div>
                                        <p>
                                            <?php echo $cobDados->banco ?>
                                        </p>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="field" style="margin-bottom: 0px;">
                                <div class="divleft" style="margin-left: 2px; width:140px;">
                                    <div style="width: 26px; padding-left: 1px;" class="label">
                                        <label for="agencia">AG.:</label>
                                    </div>
                                    <p>
                                        <?php echo $cobDados->agencia ?>
                                    </p>
                                </div>
                                <div class="divleft" style="margin-left: 2px; width:117px;">
                                    <div style="width: 46px; padding-left: 1px;" class="label">
                                        <label for="alinea">Al�nea:</label>
                                    </div>
                                    <p>
                                        <?php echo $cobDados->alinea ?>
                                    </p>
                                </div>
                                <div class="divleftlast" style="margin-left: 2px; width:206px;">
                                    <div style="width: 62px; padding-left: 1px;" class="label">
                                        <label for="emissao">Emiss�o:</label>
                                    </div>
                                    <p>
                                        <?php echo convData($cobDados->emissao, 'd') ?>
                                    </p>
                                </div>
                            </div>

                            <div style="margin-top: 12px; margin-bottom: 3px; padding-bottom: 12px;" class="blocoTitulo recuperador">
                                Parcelas
                            </div>

                            <?php foreach ($parcelas as $parcela): ?>
                                <div id="ParcGerada">
                                    <div class="field">
                                        <div class="divleft" style="width: 121px;"> 
                                            <div style="width: 58px; padding-left: 5px;" class="label"> 
                                                <label style="font-weight: normal;" for="doc_par[]">N�.Doc.:</label> 
                                            </div> 
                                            <p>
                                                <?php echo $parcela->docnum; ?>
                                            </p>
                                        </div> 
                                        <div class="divleft" style="width: 128px;"> 
                                            <div style="width: 80px; padding-left: 0px;" class="label"> 
                                                <label style="font-weight: normal;" for="num_par[]">Parcela N�.:</label> 
                                            </div> 
                                            <p>
                                                <?php echo $parcela->parnum; ?>
                                            </p>
                                        </div> 
                                        <div class="divleft" style="width: 222px;"> 
                                            <div style="width: 112px; padding-left: 1px;" class="label"> 
                                                <label style="font-weight: normal;" for="valor_par[]">V.Parcela (R$):</label> 
                                            </div> 
                                            <p>
                                                <?php echo convMoney($parcela->parvalor); ?>
                                            </p> 
                                        </div> 
                                        <div class="divleftlast" style="margin-left: 0px; width: 179px;"> 
                                            <div style="width: 45px; padding-left: 1px;" class="label"> 
                                                <label style="font-weight: normal;" for="venc_par[]">Venc.:</label> 
                                            </div> 
                                            <p>
                                                <?php echo convData($parcela->parvenc, 'd'); ?>
                                            </p>  
                                        </div> 
                                    </div>
                                </div>
                            <?php endforeach; ?>

                            <div style="height: 74px; margin-top: 5px;" class="field">
                                <div class="divleftlast" style="width: 701px;">
                                    <div style="width: 84px; padding-top: 0px;" class="label">
                                        <label for="info">Outras informa��es:</label>
                                    </div>
                                    <p>
                                        <?php echo $cobDados->informacoes; ?>
                                    </p>  
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 12px; margin-bottom: 3px; padding-bottom: 12px;" class="blocoTitulo recuperador">
                            Recuperador padr�o: <span id="recuperador_padrao"><?php echo $cobDados->usu_nome; ?></span>
                        </div>
                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <input type="reset" class="cancelar" name="cancelar" onclick="history.back()" value="Voltar" id="Voltar"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>