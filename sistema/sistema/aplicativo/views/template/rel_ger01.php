<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img.'logo_elisangela.jpg';?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br|www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo $title ;?></p>
        </div>
    </div>
    <table id="tableGer01" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="thInadimplente">Inadimplente</th>
                <th id="thCredor">Credor</th>
                <th id="th3">�ltimo RO</th>
                <th id="th4" class="last">Vencimento</th>
            </tr>
        </thead>
        <tbody>
            <?php $totalCobrancas = 0; foreach ($cobsAtras as $cobAtra) : ?>
            <tr>
                <td class="tdInaNome"><?php echo utf8_decode($cobAtra->ina_nome); ?></td>
                <td class="tdCreNomeFantasia"><?php echo utf8_decode($cobAtra->cre_nome_fantasia); ?></td>
                <td class="td3">
                    <strong>
                        <i>
                            <?php
                              $dataUltRo = convDataBanco($cobAtra->data_ult_ro);
                              echo ($dataUltRo == "//") ? "" : $dataUltRo; //se a data vier vazia o m�todo convDataBanco retorna s� as barra, se isso acontecer imprime nada
                              echo "&nbsp; &nbsp;";
                              echo $cobAtra->hora_ult_ro;
                            ?><br/>
                        </i>
                        <?php echo utf8_decode($cobAtra->nome_op_ult_ro); echo "<br/>";?>
                    </strong>
                    <?php echo utf8_decode($cobAtra->ult_ros_detalhe); ?>
                </td>
                <td class="td4 last">
                    <?php echo $dataUltRo = convDataBanco($cobAtra->parc_mais_atra);?>
                </td>
                <?php $totalCobrancas ++; endforeach; ?>
            </tr>
            <tr>
                <td colspan="3" id="tdTotalA">TOTAL DE COBRAN�AS EM ATRASO</td>
                <td id="tdTotalB" class="last"><?php echo "$totalCobrancas"; ?></td>
            </tr>
        </tbody>
    </table>
</div>