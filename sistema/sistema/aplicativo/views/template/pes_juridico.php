<style>
	.NAplicadaNProtocolada {
		background-color: gray;
		color: #fff;
	}
	.AplicadaNProtocolada {
		background-color: yellow;
		color: #000;
	}
	.AplicadaProtocolada {
		background-color: green;
		color: #fff;
	}
</style>
<script>
	$(document).ready(function(){
		
		$('[name=tipo_filtro_radio]').change(function(){
			$radio = $(this);
			if ($radio.val()=='P' && $radio.is(':checked')) {
				$('#ck_pesquisar_juridico_multa').removeAttr('checked').attr('disabled',true);
			} else {
				$('#ck_pesquisar_juridico_multa').removeAttr('disabled');
			}			
		}).trigger('change');	
	});
	
</script>

<div id="content">
        <?php echo $sidebar; ?>
    <div id="right">
        <div class="box" style="min-height: 800px;">
            <div class="title">
                <h5>Pesquisar no Jur�dico</h5>
            </div>
            <form id="form_juridico" action="<?php echo base_url().'pesquisar/pesJuridico' ?>" method="post">
                <div class="form">
                    <?php echo $mensagem; ?>
                    <div class="fields" style="height: 450px">
						<table>
							<tr class="field">
								<td width="125"><input type="checkbox" id="ck_pesquisar_juridico_credor" name="tipo_filtro_credor" value="1" /> <b>CREDOR</b>:</td>
								<td colspan="2">
		                            <select style="width: 500px;" name="credor" id="pesquisar_juridico_credor" class="chosen" data-placeholder="Informe o nome fantasia do credor">
		                            	<option value=""></option>
		                            	<?php foreach($credores as $credor) : ?>
											<option value="<?php echo $credor->cre_cod; ?>"><?php echo $credor->cre_nome_fantasia; ?></option>
										<?php endforeach?>
		                            </select>
	                            </td>
							</tr>
							<tr class="field">
								<td width="125"><input type="checkbox" id="ck_pesquisar_juridico_multa" name="tipo_filtro_multa" value="1" /> <b>MULTA</b>:</td>
								<td colspan="2">
									<input type="radio" id="pesquisar_juridico_multa" name="acordo_multa" value="NANP" /><span class="NAplicadaNProtocolada">&nbsp;&nbsp;&nbsp;</span> MULTA <b>N�O</b> APLICADA E <b>N�O</b> PROTOCOLADA<br />
									<input type="radio" id="pesquisar_juridico_multa" name="acordo_multa" value="ANP" /><span class="AplicadaNProtocolada">&nbsp;&nbsp;&nbsp;</span> MULTA APLICADA E <b>N�O</b> PROTOCOLADA<br />
									<input type="radio" id="pesquisar_juridico_multa" name="acordo_multa" value="AP" /><span class="AplicadaProtocolada">&nbsp;&nbsp;&nbsp;</span> MULTA APLICADA E PROTOCOLADA
	                            </td>
							</tr>
							<tr class="field">
								<td width="125" rowspan="2"><input type="radio" id="pesquisar_juridico_vencimento" name="tipo_filtro_radio" value="V" /> <b>VENCIMENTO DE PARCELAS</b>:</td>
								<td>
									<input type="radio" name="vencimento_data" id="pesquisar_juridico_vencimento_data" value="I" />Intervalo
									
	                            </td>
                                <td>
	                            	<label for="de">De:</label>
	                            	<input type="text" id="pesquisar_juridico_vencimento_data_de" name="vencimento_data_de" class="dataMascara" />
                                	<label for="ate">At�:</label>
                                	<input type="text" id="pesquisar_juridico_vencimento_data_ate" name="vencimento_data_ate" class="dataMascara" />									
								</td>
							</tr>
							<tr class="field">
								<td colspan="2">
									<input type="radio" name="vencimento_data" id="pesquisar_juridico_vencimento_final" value="T" />Todas <b>(Todas as d�vidas atrasadas)</b>
								</td>
							</tr>
							<tr class="field">
								<td width="125"><input type="radio" id="pesquisar_juridico_ociosidade" name="tipo_filtro_radio" value="O" /> <b>OCIOSIDADE</b>:</td>
								<td colspan="2">
	                            	<label for="de">De</label>
	                            	<input type="text" id="pesquisar_juridico_ociosidade_dias" name="ociosidade_data_dias" class="" style="width: 30px;" />
	                            	dias at� hoje
	                            </td>
							</tr>
							<tr class="field">
								<td width="125"rowspan="2"><input type="radio" id="pesquisar_juridico_processo" name="tipo_filtro_radio" value="P" /> <b>PROCESSO</b>:</td>
								<td colspan="2">
	                            	<label for="de">N�mero Completo:</label>
	                            	<input type="text" id="pesquisar_juridico_processo_num" name="processo_num" class="" style="width: 160px;" />
	                            	
	                            	<label for="de">Ano:</label>
	                            	<input type="text" id="pesquisar_juridico_processo_ano" name="processo_ano" class="" style="width: 60px;" />
	                            	
	                            	<label for="de">C�digo F�rum:</label>
	                            	<input type="text" id="pesquisar_juridico_processo_forum" name="processo_forum" class="" style="width: 60px;" />
	                            </td>
							</tr>
							<tr class="field">
								<td colspan="2">
									<input type="radio" name="processo_radio" id="pesquisar_juridico_processo_todos" value="T" />Todos <b>(Todas as d�vidas com n�mero, ano ou f�rum do processo)</b> <br />
									<input type="radio" name="processo_radio" id="pesquisar_juridico_processo_arquivamento" value="A" />Arquivamento <b>(Todas as d�vidas a serem arquivadas)</b> <br />
									<input type="radio" name="processo_radio" id="pesquisar_juridico_processo_desentranhamento" value="D" />Desentranhamento <b>(Todas as d�vidas a serem desentranhadas)</b>
								</td>
							</tr>
						</table>
                        <div style="clear:both"></div>
                        
                        <div class="divlast">
                            <div class="buttons" style="margin-top: 10px;">
                                <div class="highlight">
                                	<button type="submit">Pesquisar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
(function ($) {

	var cookieInputStatus = function(obj) {
		$.cookie($(obj).attr('id'),$(obj).val());
	};
	var cookieCheckboxStatus = function(obj) {
		var status = $(obj).is(':checked')?1:0;
		$.cookie($(obj).attr('id'),status);
	};
	var cookieRadioStatus = function(obj) {
		$.cookie($(obj).attr('name'),$(obj).val());
	};	
	$(':checkbox').change(function() {
		cookieCheckboxStatus(this);
		//console.log($.cookie($(this).attr('id')));
	});
	$(':checkbox').each(function(){
		if ($.cookie($(this).attr('id'))==1) {
			$(this).attr('checked',true);
		} else {
			$(this).removeAttr('checked');
		}
	});
	$(':radio').change(function() {
		cookieRadioStatus(this);
	});
	$(':radio').each(function(){
		if ($.cookie($(this).attr('name'))==$(this).val()) {
			$(this).attr('checked',true);
		} else {
			$(this).removeAttr('checked');
		}
	});
	$('#form_juridico input:not(:checkbox, :radio), select').change(function(){
		cookieInputStatus(this);
	});
	$('#form_juridico input:not(:checkbox, :radio), select').each(function(){
		var objVal = $.cookie($(this).attr('id'));
		if (objVal!=undefined) $(this).val(objVal);
	});
	
	$('select.chosen').chosen();
	
	$(".dataMascara").datepicker();
	
})(jQuery);
</script>