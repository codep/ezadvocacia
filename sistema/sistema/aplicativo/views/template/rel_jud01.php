<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img.'logo_elisangela.jpg';?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - Telefone: 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    cobranca@ezcobrancas.com.br|www.ezcobrancas.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo $title ;?></p>
        </div>
    </div>
    <table id="table01" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="th1">Inadimplente</th>
                <th id="th2">Credor</th>
                <th id="th3">Multa</th>
                <th id="th4">�ltimo RO</th>
                <th id="th5">Parcelas<br/> em atraso</th>
                <th id="th6" class="last">1� Venc.</th>
            </tr>
        </thead>
        <tbody>
            <?php $totalDeInad = 0; foreach ($acordsJudAtras as $acordJudAtra) : ?>
            <tr>
           
                <td><?php echo utf8_decode($acordJudAtra->ina_nome); ?></td>
                <td id="td2"><?php echo utf8_decode($acordJudAtra->cre_nome_fantasia); ?></td>
                <td id="td3">
                   <?php
                   /*verificando se possui o ro APLICACAO DE MULTA*/
                         if ($acordJudAtra->multa == 48)//48 = c�digo da opera��o "Multa" na tabela opera��es
                                 {
                                   echo "SIM"; //possui o ro APLICACAO DE MULTA
                                 }
                         else //2 = previs�o
                                 {
                                    echo "N�O"; //n�o possui o ro APLICACAO DE MULTA
                                 }
                   ?>
                </td>
                <td id="td4">
                    <strong>
                        <i>
                            <?php
                              $dataUltRo = convDataBanco($acordJudAtra->data_ult_ro);
                              echo ($dataUltRo == "//") ? "" : $dataUltRo; //se a data vier vazia o m�todo convDataBanco retorna s� as barra, se isso acontecer imprime nada
                              echo "&nbsp &nbsp";
                              echo $acordJudAtra->hora_ult_ro;
                            ?><br/>
                        </i>
                        <?php
                          echo utf8_decode($acordJudAtra->nome_op_ult_ro); echo "<br/>";
                        ?>
                    </strong>
                    <?php echo utf8_decode($acordJudAtra->ult_ros_detalhe); ?>
                <td id="td5"><?php echo $acordJudAtra->qtd_par_atras; ?></td>
                <td id="td6" class="last"><?php $primeVenc = convDataBanco($acordJudAtra->prime_venc); echo ($primeVenc == "//") ? "" : $primeVenc; ?></td>
                <?php $totalDeInad ++; endforeach; ?>
            </tr>
            <tr>
                <td colspan="5" id="total">TOTAL DE INADIMPLENTES EM ATRASO</td>
                <td id="td7" class="last"><?php echo "$totalDeInad"; ?></td>
            </tr>

        </tbody>
    </table>
</div>
