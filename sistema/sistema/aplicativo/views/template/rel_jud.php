<!--<script type="text/javascript">
        $(document).ready(function(){
            $('.visualizar').click(function(e){
                    window.location.href='< ?php echo base_url()?>divida/detalhes';
            });
            $('input[value="Ficha"]').click(function(e){
                    window.location.href='< ?php echo base_url()?>divida/ficha';
            });

        });
</script> -->view jud

     <div id="content">
         <?php echo $sidebar; ?>
            <div id="right">
                <div id="box-tabs" class="box" style="min-height: 798px;">
                    <div class="title">
                        <h5>Relat�rios Jur�dicos</h5>
                    </div>
                    <?php echo $mensagem ?>
                    <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                            Acordos Jur�dicos em Atraso
                    </div>
                    <form id="form01" action="<?php echo base_url().'relatorios/rel_jur_01' ?>"  method="post" target="_blank">
                        <div class="form">
                            <div class="fields">
                                <div class="field  field-first" style="margin-top:6px;">
                                    <div class="divleft" style="height:29px; width: 210px;">
                                        <div class="label">
                                            <label>Credor:</label>
                                        </div>
                                        <input style="margin-top: 7px; " type="radio" name="cre_filtro01" value="todos01" />Todos
                                        <input type="radio" name="cre_filtro01" value="credor_especifico01" />Pesquisar
                                    </div>
                                    <div class="divleftlast" style="height:29px; width: 200px;">
                                        <div class="label">
                                            <label>Pessoa:</label>
                                        </div>
                                        <input style="margin-top: 7px; " type="radio" name="cre_pessoa01" value="f" />F�sica
                                        <input type="radio" name="cre_pessoa01" value="j" />Jur�dica
                                    </div>
                                </div>
                                <div class="field" id="dadosCredor01">
                                    <div class="divleft" style="width: 371px;">
                                        <div style="width: 49px;" class="label">
                                            <label for="cre_nome">Nome:</label>
                                        </div>
                                        <div class="input" id="cre_nome01">
                                            <input style="width: 288px;" type="text" id="credor_nome01" name="cre_nome01" />
                                        </div>
                                    </div>

                                    <div class="divleft" style="width: 231px;">
                                        <div style="width: 76px; padding-left: 1px;" class="label">
                                            <label for="cre_cpf_cnpj">CNPJ/CPF:</label>
                                        </div>
                                        <div class="input" id="cre_cpf_cnpj01">
                                            <input style="width: 124px;" type="text" id="credor_cpf_cnpj01" name="cre_cpf_cnpj01"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 87px; margin: 0px;">
                                        <div class="buttons">
                                            <div class="highlight">
                                                <button style="width: 85px; padding: 4px;" name="cre01" type="button" class="dialog-form-open">Pesquisar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="margin-left: 2px; width:150px;">
                                        <div style="width: 28px; padding-left: 1px;" class="label">
                                            <label for="de">Desde:</label>
                                        </div>
                                        <div style=" width: 109px;" class="input">
                                            <input type="text" readonly name="de01" class="date"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleftlast" style="text-align: center">
                                        <div class="buttons" style="width: 700px">
                                            <div class="highlight" style=" margin: 0 auto">
                                                    <input style="width: 185px; margin: 0 auto" type="submit" name="filtrar_rel_adm01" value="Gerar relat�rio" />
                                            </div>
                                          </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                            Acordos Jur�dicos a vencer
                    </div>
                    <form id="form02" action="<?php echo base_url().'relatorios/rel_jur_02' ?>"  method="post" target="_blank">
                        <div class="form">
                            <div class="fields">
                                <div class="field  field-first" style="margin-top:6px;">
                                    <div class="divleft" style="height:29px; width: 210px;">
                                        <div class="label">
                                            <label>Credor:</label>
                                        </div>
                                        <input type="radio" name="cre_filtro02" value="todos02" style="margin-top: 7px;"/>Todos
                                        <input type="radio" name="cre_filtro02" value="credor_especifico02" />Pesquisar
                                    </div>
                                    <div class="divleftlast" style="height:29px; width: 200px;">
                                        <div class="label">
                                            <label>Pessoa:</label>
                                        </div>
                                        <input style="margin-top: 7px; " type="radio" name="cre_pessoa02" value="f" />F�sica
                                        <input type="radio" name="cre_pessoa02" value="j" />Jur�dica
                                    </div>
                                </div>
                                <div class="field" id="dadosCredor02">
                                    <div class="divleft" style="width: 371px;">
                                        <div style="width: 49px;" class="label">
                                            <label for="cre_nome">Nome:</label>
                                        </div>
                                        <div class="input" id="cre_nome02">
                                            <input style="width: 288px;" type="text" id="credor_nome02" name="cre_nome02" />
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 231px;">
                                        <div style="width: 76px; padding-left: 1px;" class="label">
                                            <label for="cre_cpf_cnpj">CNPJ/CPF:</label>
                                        </div>
                                        <div class="input" id="cre_cpf_cnpj02">
                                            <input style="width: 124px;" type="text" id="credor_cpf_cnpj02" name="cre_cpf_cnpj02"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 87px; margin: 0px;">
                                        <div class="buttons">
                                            <div class="highlight">
                                                <button style="width: 85px; padding: 4px;" name="cre02" type="button" class="dialog-form-open">Pesquisar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="margin-left: 2px; width:150px;">
                                        <div style="width: 28px; padding-left: 1px;" class="label">
                                            <label for="de">De:</label>
                                        </div>
                                        <div style=" width: 109px;" class="input">
                                            <input type="text" readonly name="data_de02" class="date"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="margin-left: 2px; width:150px;">
                                        <div style="width: 28px; padding-left: 1px;" class="label">
                                            <label for="ate">At�:</label>
                                        </div>
                                        <div style=" width: 109px;" class="input">
                                            <input type="text" readonly name="data_ate02" class="date"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleftlast" style="text-align: center">
                                        <div class="buttons" style="width: 700px">
                                            <div class="highlight" style=" margin: 0 auto">
                                                    <input style="width: 185px; margin: 0 auto" type="submit" name="filtrar_rel_adm02" value="Gerar relat�rio" />
                                            </div>
                                          </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </form>
                     <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                          Cobran�as Jur�dicas por Per�odo de Ociosidade
                    </div>
                    <form id="form03" action="<?php echo base_url().'relatorios/rel_jur_03' ?>"  method="post" target="_blank">
                        <div class="form">
                            <div class="fields">
                                <div class="field  field-first" style="margin-top:6px;">
                                    <div class="divleft" style="height:29px; width: 210px;">
                                        <div class="label">
                                            <label>Credor:</label>
                                        </div>
                                        <input type="radio" name="cre_filtro03" value="todos" style="margin-top: 7px;"/>Todos
                                        <input type="radio" name="cre_filtro03" value="credor_especifico" />Pesquisar
                                    </div>
                                    <div class="divleftlast" style="height:29px; width: 200px;">
                                        <div class="label">
                                            <label>Pessoa:</label>
                                        </div>
                                        <input style="margin-top: 7px; " type="radio" name="cre_pessoa03" value="f" />F�sica
                                        <input type="radio" name="cre_pessoa03" value="j" />Jur�dica
                                    </div>
                                </div>
                                <div class="field" id="dadosCredor03">
                                    <div class="divleft" style="width: 371px;">
                                        <div style="width: 49px;" class="label">
                                            <label for="cre_nome">Nome:</label>
                                        </div>
                                        <div class="input" id="cre_nome03">
                                            <input style="width: 288px;" type="text" id="credor_nome03" name="cre_nome03" />
                                        </div>
                                    </div>

                                    <div class="divleft" style="width: 231px;">
                                        <div style="width: 76px; padding-left: 1px;" class="label">
                                            <label for="cre_cpf_cnpj">CNPJ/CPF:</label>
                                        </div>
                                        <div class="input" id="cre_cpf_cnpj03">
                                            <input style="width: 124px;" type="text" id="credor_cpf_cnpj03" name="cre_cpf_cnpj03"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 87px; margin: 0px;">
                                        <div class="buttons">
                                            <div class="highlight">
                                                <button style="width: 85px; padding: 4px;" name="cre03" type="button" class="dialog-form-open">Pesquisar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="margin-left: 2px; width:150px;">
                                        <div style="width: 28px; padding-left: 1px;" class="label">
                                            <label for="de">De:</label>
                                        </div>
                                        <div style=" width: 109px;" class="input">
                                            <input type="text" readonly name="de03" class="date"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="margin-left: 2px; width:150px;">
                                        <div style="width: 28px; padding-left: 1px;" class="label">
                                            <label for="ate">At�:</label>
                                        </div>
                                        <div style=" width: 109px;" class="input">
                                            <input type="text" readonly name="ate03" class="date"/>
                                        </div>
                                    </div>
                                <!--    <div class="divleftlast" style="margin-left: 2px; width:250px;">
                                        <div style="width: 30px; padding-left: 1px;" class="label">
                                            <label for="tipo">Tipo:</label>
                                        </div>
                                        <input type="radio" name="tipo03" value="todos" style="margin-top: 7px;" checked />Todos
                                        <input type="radio" name="tipo03" value="acordos" />Acordos
                                        <input type="radio" name="tipo03" value="previsoes" />Previs�es
                                    </div> -->
                                </div>
                                <div class="field">
                                    <div class="divleftlast" style="text-align: center">
                                        <div class="buttons" style="width: 700px">
                                            <div class="highlight" style=" margin: 0 auto">
                                                    <input style="width: 185px; margin: 0 auto" type="submit" name="filtrar_rel_adm03" value="Gerar relat�rio" />
                                            </div>
                                          </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                            -------------------------------------
                    </div>
                    <form id="form05" action="<?php echo base_url().'relatorios/rel_05' ?>"  method="post" target="_blank">
                        <div class="form">
                            <div class="fields">
                                <div class="field  field-first" style="margin-top:6px;">
                                    <div class="divleft" style="height:29px; width: 210px;">
                                        <div class="label">
                                            <label>Credor:</label>
                                        </div>
                                        <input style="margin-top: 7px; " type="radio" name="cre_filtro05" value="0" />Todos
                                        <input type="radio" name="cre_filtro05" value="1" />Pesquisar
                                    </div>
                                    <div class="divleftlast" style="height:29px; width: 200px;">
                                        <div class="label">
                                            <label>Pessoa:</label>
                                        </div>
                                        <input style="margin-top: 7px; " type="radio" name="cre_pessoa05" value="f" />F�sica
                                        <input type="radio" name="cre_pessoa05" value="j" />Jur�dica
                                    </div>
                                </div>
                                <div class="field" id="dadosCredor05">
                                    <div class="divleft" style="width: 371px;">
                                        <div style="width: 49px;" class="label">
                                            <label for="cre_nome">Nome:</label>
                                        </div>
                                        <div class="input" id="cre_nome05">
                                            <input style="width: 288px;" type="text" id="credor_nome05" name="cre_nome05" />
                                            <input type="hidden" name="cre_cod05" value="45"/>
                                        </div>
                                    </div>

                                    <div class="divleft" style="width: 231px;">
                                        <div style="width: 76px; padding-left: 1px;" class="label">
                                            <label for="cre_cpf_cnpj">CNPJ/CPF:</label>
                                        </div>
                                        <div class="input" id="cre_cpf_cnpj05">
                                            <input style="width: 124px;" type="text" id="credor_cpf_cnpj05" name="cre_cpf_cnpj05"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 87px; margin: 0px;">
                                        <div class="buttons">
                                            <div class="highlight">
                                                <button style="width: 85px; padding: 4px;" name="cre05" type="button" class="dialog-form-open">Pesquisar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="margin-left: 2px; width:180px;">
                                        <div style="width: 58px; padding-left: 1px;" class="label">
                                            <label for="de">Desde:</label>
                                        </div>
                                        <div style=" width: 109px;" class="input">
                                            <input type="text" readonly name="de05" class="date"/>
                                        </div>
                                    </div>
                                <!--    <div class="divleftlast" style="margin-left: 2px; width:250px;">
                                        <div style="width: 30px; padding-left: 1px;" class="label">
                                            <label for="tipo">Tipo:</label>
                                        </div>
                                        <input type="radio" name="tipo05" value="todos" style="margin-top: 7px;" checked />Todos
                                        <input type="radio" name="tipo05" value="acordos" />Acordos
                                        <input type="radio" name="tipo05" value="previsoes" />Previs�es
                                    </div> -->
                                </div>
                                <div class="field">
                                    <div class="divleftlast" style="text-align: center">
                                        <div class="buttons" style="width: 700px">
                                            <div class="highlight" style=" margin: 0 auto">
                                                    <input style="width: 185px; margin: 0 auto" type="submit" name="filtrar_rel_adm05" value="Gerar relat�rio" />
                                            </div>
                                          </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    
                </div>
           </div>
      </div>