<script type="text/javascript">
        $(document).ready(function(){


//            $('.excluir').click(function(e){
//               e.preventDefault()
//               alert('CHEGOU!');
//               link = $(this).attr('id');
//               window.location.href=link;
//            });

            $('.editacredor').css('cursor','pointer');

            $('.editacredor').click(function(e){
                    link = $(this).parent().attr('id');
                    window.location.href=link;
            });
        });
</script>
    <div id="content">

        <?php echo $sidebar; ?>

            <div id="right">
                <div id="box-tabs" class="box" style="min-height: 798px;">
                    <div class="title">
                        <h5>Lista de credores</h5>
                    </div>
                    <?php echo $mensagem;?>
                    <div id="hoje">
                        <div class="form">
                            <div class="fields">
                                <form id="form" method="post" action="<?php echo base_url().'credor/listar'?>">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 395px;">
                                            <div style="width: 129px; padding-left: 1px;" class="label">
                                                <label for="nome">Selecionar cidade:</label>
                                            </div>
                                            <div class="select">
                                                <select style="width: 255px; margin-left: 5px;" id="cidade" name="filtro">
                                                    <option value="TODOS">TODOS</option>
                                                    <?php foreach ($cidades as $cid):?>
                                                        <option value="<?php echo $cid->cre_cidade;?>" <?php if($cid->cre_cidade == $cidadeatual) echo 'selected'?>><?php echo $cid->cre_cidade;?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px; border-bottom: 1px solid #ddd;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 50px;">Credor</th>
                                                <th>Cidade</th>
                                                <th>Telefone</th>
                                                <th>Ativos</th>
                                                <th class="last">Excluir</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $total_credores = 0; ?>
                                            <?php foreach ($credores as $cre):?>
                                            <tr id="<?php echo base_url().'credor/editar/cod:'.$cre->cre_cod;?>">
                                                <td class="title editacredor" style="width: 150px;"><?php echo $cre->cre_nome_fantasia;?></td>
                                                <td class="editacredor"><?php echo $cre->cre_cidade;?></td>
                                                <td class="editacredor"><?php echo $cre->cre_fone1;?></td>
                                                <td class="editacredor"><?php echo $ativos[$cre->cre_cod];?></td>
                                                <td class="last excluir" id="">
                                                    <a href="<?php echo base_url().'credor/excluir/cod:'.$cre->cre_cod;?>">
                                                        <img src="<?php echo $img.'excluir.png'?>" alt="+"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php $total_credores++; ?>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="field  field-first" style="padding-bottom: 1px;">
                                    <div class="divleftlast" style="width: 395px;">
                                        <div style="padding-left: 0px;" class="label">
                                            <label for="nome">Total de credores: <i><?php echo $total_credores?></i></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>