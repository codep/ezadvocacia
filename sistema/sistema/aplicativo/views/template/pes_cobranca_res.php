<script type="text/javascript">
    $(document).ready(function(){

        $('.editaInad').click(function(){
                cod = $(this).attr('id');
                window.location.href="<?php echo base_url().'divida/ficha/cod:';?>"+cod;
        });
        
		$('input#search').quicksearch('#products tbody tr:not(.titulo)', {});
/*
		//AUTO FOCUS
		window.setTimeout(function(){
			$('input#search').focus();	
		},300);
*/	        
        
    });
</script>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Pesquisar cobranšas</h5>
            </div>
            <h3 style="text-align: center;"><?php echo $credor->cre_nome_fantasia; ?></h3>
            <div class="blocoTitulo">
                <span style="font-size: 10px">Filtro: <input type="text" id="search" style="font-size: 18px; padding: 5px; width: 500px;" /></span>
            </div>
            <div class="table">
                <table id="products">
                    <thead>
                        <tr>
                        	<th>Setor</th>
                            <th>Inadimplente</th>
                            <th>Valor</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php $totalCobrancas=0; ?>                        
                        <?php foreach ($cobrancas as $tipo => $rows): ?>
                    	<?php $totalRows = is_array($rows) ? count($rows) : 0; ?>
                    	<?php $totalCobrancas += $totalRows; ?> 
                    	<tr class="titulo"><th colspan="5"><?php echo strtoupper($tipo); ?> (<?php echo $totalRows; ?>)</th></tr>
                    	<?php foreach ($rows as $cobranca): ?>
                        <tr>
                        	<td> <?php echo $cobranca->setor; ?> </td>
                            <td> <?php echo utf8_decode($cobranca->ina_nome); ?> </td>
                            <td> <?php echo isset($cobranca->valor2) && !empty($cobranca->valor2) ? convMoney($cobranca->valor2) : convMoney($cobranca->valor); ?> </td>
                            <td class="last">
                                <div class="buttons">
                                    <input style="width: 50px;" class="visualizar editaInad" id="<?php echo $cobranca->ina_cod; ?>" type="button" name="cadastrar" value="Ficha" />
                                </div>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        <?php endforeach;?>                
                    </tbody>
                </table>
                <div class="pagination pagination-left">
                    <div class="results">
                        <span>Total de <?php echo $totalCobrancas; ?> registros encontrados</span>
                    </div>
                </div>
                <br/>
                <br/>
            </div>
<!--            <form id="form" action="">-->
                <div class="form">
                    <div class="fields">
                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <a href="<?php echo base_url().'pesquisar/cobrancas' ?>" style="text-decoration: none;" >
                                <div style="margin-left: 7px;" class="highlight">
                                    <input type="submit" name="submit.highlight" value="Nova pesquisa" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
<!--            </form>-->
        </div>
    </div>
</div>
