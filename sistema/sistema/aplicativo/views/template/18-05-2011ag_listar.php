    <!--RECEBE OS DADOS DO CONTROLADOR agenda M�TODO listar-->
<div id="content">
<?php echo $sidebar; ?>
    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Agenda</h5>
                <ul class="links">
                    <li><a href="#dia">Hoje</a></li>
                    <li><a href="#semana">Semana</a></li>
                    <li><a href="#mes">M�s</a></li>
                    <li><a href="#todas">Todas</a></li>
                    <li><a href="#geral">Geral</a></li>
                </ul>
            </div>
            <div id="dia">
                <div class="form">
                    <div class="fields">
                        <div style="margin-bottom: 20px;" class="blocoTitulo">
                            Suas tarefas para hoje
                        </div>
                        <?php echo $mensagem;?>
                        <div class="table" style="padding: 0px 5px 10px;">
                            <?php if($tarefasDeHojeUsuSessao!=null):?>
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th class="left">T�tulo</th>
                                        <th style="width: 205px">Descri��o</th>
                                        <th>Agendado</th>
                                        <th>Executar</th>
                                        <th class="last">Excluir</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php foreach ($tarefasDeHojeUsuSessao as $tarefasDeHojeUsuario): ?>
                                    <tr>
                                        <td class="title"><?php echo utf8_decode($tarefasDeHojeUsuario->tar_titulo); ?></td>
                                        <td><?php echo utf8_decode($tarefasDeHojeUsuario->tar_descricao);?></td>
                                        <td><?php echo "Em ".convData($tarefasDeHojeUsuario->tar_criacao,'d')." por ".$tarefasDeHojeUsuario->tar_criador;?></td>
                                        <td><?php echo "Em ".convData($tarefasDeHojeUsuario->tar_agendada,'d')." por ".$tarefasDeHojeUsuario->usu_usuario_sis;?></td>
                                        <td style="text-align: center; vertical-align: middle;" class="last">
                                            <a href="<?php echo 'excluirTarefa/cod:'. $tarefasDeHojeUsuario->tar_cod ?>" class="excluir" id="">
                                                <img src="<?php echo $img.'excluir.png'?>" alt="+"/>
                                            </a>
                                        </td>
                                    </tr>
                                  <?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php endif;?>
                            <?php echo $tarefasDeHojeUsuSessao!=null?'':'<center><i>Nenhuma tarefa para hoje</i></center>'?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="semana">
                <div class="form">
                    <div class="fields">
                        <div style="margin-bottom: 20px;" class="blocoTitulo">
                            Suas tarefas para a semana
                        </div>
                        <div class="table" style="padding: 0px 5px 10px;">
                            <?php if($tarefasDaSemanaUsuSessao!=null):?>
                            <table id="products">
                                <thead>
                                    <tr>
                                    <th class="left">T�tulo</th>
                                    <th style="width: 205px">Descri��o</th>
                                    <th>Agendado</th>
                                    <th>Executar</th>
                                    <th class="last">Excluir</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($tarefasDaSemanaUsuSessao as $tarefasDaSemanaUsu): ?>
                                    <tr>
                                        <td class="title"><?php echo utf8_decode($tarefasDaSemanaUsu->tar_titulo); ?></td>
                                        <td><?php echo utf8_decode($tarefasDaSemanaUsu->tar_descricao);?></td>
                                        <td><?php echo "Em ".convData($tarefasDaSemanaUsu->tar_criacao,'d')." por ".$tarefasDaSemanaUsu->tar_criador;?></td>
                                        <td><?php echo "Em ".convData($tarefasDaSemanaUsu->tar_agendada,'d')." por ".$tarefasDaSemanaUsu->usu_usuario_sis;?></td>
                                        <td style="text-align: center; vertical-align: middle;" class="last">
                                            <a href="<?php echo 'excluirTarefa/cod:'. $tarefasDaSemanaUsu->tar_cod ?>">
                                                <img src="<?php echo $img.'excluir.png'?>" alt="+"/>
                                            </a>
                                        </td>
                                    </tr>
                                   <?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php endif;?>
                            <?php echo $tarefasDaSemanaUsuSessao!=null?'':'<center><i>Nenhuma tarefa para esta semana</i></center>'?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="mes">
                <div class="form">
                    <div class="fields">
                        <div style="margin-bottom: 20px;" class="blocoTitulo">
                            Suas tarefas para este m�s
                        </div>
                        <div class="table" style="padding: 0px 5px 10px;">
                            <?php if($tarefasDoMesUsuSessao!=null):?>
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th class="left">T�tulo</th>
                                        <th style="width: 205px">Descri��o</th>
                                        <th>Agendado</th>
                                        <th>Executar</th>
                                        <th class="last">Excluir</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($tarefasDoMesUsuSessao as $tarefaMesUsuSessao):?>
                                    <tr>
                                        <td class="title"><?php echo utf8_decode($tarefaMesUsuSessao->tar_titulo) ?></td>
                                        <td><?php echo utf8_decode($tarefaMesUsuSessao->tar_descricao) ?></td>
                                        <td><?php echo "Em ".convData($tarefaMesUsuSessao->tar_criacao,'d')." por ".$tarefaMesUsuSessao->tar_criador ?></td>
                                        <td><?php echo "Em ".convData($tarefaMesUsuSessao->tar_agendada,'d')." por ".$tarefaMesUsuSessao->usu_usuario_sis ?></td>
                                        <td style="text-align: center; vertical-align: middle;" class="last">
                                            <a href="<?php echo 'excluirTarefa/cod:'. $tarefaMesUsuSessao->tar_cod ?>">
                                                <img src="<?php echo $img.'excluir.png'?>" alt="+"/>
                                            </a>
                                        </td>
                                    </tr>
                                   <?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php endif;?>
                            <?php echo $tarefasDoMesUsuSessao!=null?'':'<center><i>Nenhuma tarefa para este m�s</i></center>'?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="todas">
                <div class="form">
                    <div class="fields">
                        <div style="margin-bottom: 20px;" class="blocoTitulo">
                            Todas as suas tarefas
                        </div>
                        <div class="table" style="padding: 0px 5px 10px;">
                            <?php if($todasTarefasUsuSessao!=null):?>
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th class="left">T�tulo</th>
                                        <th style="width: 205px">Descri��o</th>
                                        <th>Agendado</th>
                                        <th>Executar</th>
                                        <th class="last">Excluir</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($todasTarefasUsuSessao as $todaTarefaUsuSesao): ?>
                                    <tr>
                                        <td class="title"><?php echo utf8_decode($todaTarefaUsuSesao->tar_titulo) ?></td>
                                        <td><?php echo utf8_decode($todaTarefaUsuSesao->tar_descricao) ?></td>
                                        <td><?php echo "Em ". convData($todaTarefaUsuSesao->tar_criacao, 'd'). " por ". $todaTarefaUsuSesao->tar_criador;   ?></td>
                                        <td><?php echo "Em ". convData($todaTarefaUsuSesao->tar_agendada, 'd'). " por ". $todaTarefaUsuSesao->usu_usuario_sis;   ?></td>
                                        <td style="text-align: center; vertical-align: middle;" class="last">
                                            <a href="<?php echo 'excluirTarefa/cod:'. $todaTarefaUsuSesao->tar_cod ?>">
                                                <img src="<?php echo $img.'excluir.png'?>" alt="+"/>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php endif;?>
                            <?php echo $todasTarefasUsuSessao!=null?'':'<center><i>Nenhuma tarefa cadastrada</i></center>'?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="geral">
                <div class="form">
                    <div class="fields">
                        <div style="margin-bottom: 20px;" class="blocoTitulo">
                            Todas as tarefas
                        </div>
                        <div class="table" style="padding: 0px 5px 10px;">
                            <?php if($listaDeTarefas!=null):?>
                            <table id="products">
                                <thead>
                                    <tr>
                                    <th class="left">T�tulo</th>
                                    <th style="width: 205px">Descri��o</th>
                                    <th>Agendado</th>
                                    <th>Executar</th>
                                    <th class="last">Excluir</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <?php foreach ($listaDeTarefas as $listaDeTarefa): ?>
                                    <tr>
                                        <td class="title"><?php echo utf8_decode($listaDeTarefa->tar_titulo);?></td>
                                        <td><?php echo utf8_decode($listaDeTarefa->tar_descricao); ?></td>
                                        <td><?php echo "Em ". convData($listaDeTarefa->tar_criacao, 'd'). " por ". $listaDeTarefa->tar_criador;?></td>
                                        <td><?php echo "Em ". convData($listaDeTarefa->tar_agendada, 'd'). " por ". $listaDeTarefa->usu_usuario_sis; ?></td>
                                        <td style="text-align: center; vertical-align: middle;" class="last">
                                            <a href="<?php echo 'excluirTarefa/cod:'. $listaDeTarefa->tar_cod ?>">
                                                <img src="<?php echo $img.'excluir.png'?>" alt="+"/>
                                            </a>
                                        </td>
                                    </tr>
                                  <?php endforeach; ?>
                            </table>
                            <?php endif;?>
                            <?php echo $todasTarefasUsuSessao!=null?'':'<center><i>Nenhuma tarefa cadastrada</i></center>'?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
