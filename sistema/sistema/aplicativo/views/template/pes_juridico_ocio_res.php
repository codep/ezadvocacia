<script type="text/javascript">
    $(document).ready(function(){

        $('.editaInad').click(function(){
                cod = $(this).attr('id');
                window.location.href="<?php echo base_url().'divida/ficha/cod:';?>"+cod;
        });
        
		$('input#search').quicksearch('.blocoFull', {});
/*
		//AUTO FOCUS
		window.setTimeout(function(){
			$('input#search').focus();	
		},300);
*/	        
        
        $('.btHide').click(function(e){
        	e.preventDefault();
        	$blocoFull = $(this).parents('.blocoFull:first');
        	$blocoFull.slideUp('normal',function(){
        		$(this).remove();
        	})
        })
        
        
    });
</script>
<style>
	#content div.box table td {
		padding: 5px 0px;
	}
	#content div.box table tr:hover td, #content div.box .blocoFull:hover {
		background-color: #ddd;
	}
	#content div.box .blocoFull:hover .rosdesc {
		font-size: 14px; 
		background-color: #fff;
	}
	#content div.box .blocoFull:hover .rostipo {
		font-weight: bold;
	}
	#content div.box table tr { 
		/* outline: thin solid black; */
	} 
	#content div.box table tr.upper td {
		border-top: 1px solid #000;
	}
	#content div.box table tr.down td {
		border-top: 1px solid #000;
	}
</style>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Pesquisar Cobranšas por Ociosidade</h5>
            </div>
            <?php if (!empty($credor)) : ?>
            <h3 style="text-align: center;"><?php echo $credor->cre_nome_fantasia; ?></h3>
            <?php endif; ?>
            <div class="blocoTitulo">
                <span style="font-size: 10px">Filtro: <input type="text" id="search" style="font-size: 18px; padding: 5px; width: 500px;" /></span>
            </div>
            <div class="boxlegenda" style="text-align: right; vertical-align: text-top; height: 10px; margin: 0px 20px;">
            	<img src="<?php echo $img.'icons/cross.png'?>" alt="OCULTAR" style="vertical-align: middle;" /> 
            	<span>= OCULTAR APENAS PARA ESSA PESQUISA.</span>
            </div>
            <div class="table">
	            <?php 
					//ORGANIZANDO POR DATA
	            	$data_cobrancas = '';
	            	foreach ($cobrancas as $cobranca) :
	            		$data_cobrancas[$cobranca->ros_data][] = $cobranca;
					endforeach;
	            ?>
	            <?php foreach ($data_cobrancas as $ros_data => $tmp_cobrancas) : ?>
	            <h4 style="text-align: left; margin-bottom: none; padding: 0px; margin: 5px 0px;"><?php echo convDataBanco($ros_data); ?></h4>
	            <?php foreach ($tmp_cobrancas as $cobranca): ?>		
	            <div class="blocoFull" style="display: block; width: 100%; border: 1px solid #aaa; margin-bottom: 10px; padding: 3px;">
	            	<span style="float: right;"><?php echo utf8_decode($cobranca->cre_nome_fantasia); ?></span>
	            	<span style="float: left; font-size: 14px;"><?php echo utf8_decode($cobranca->ina_nome); ?></span>
	            	<div style="clear: both"></div>
	            	<div style="clear: both; margin: 5px 0px; padding: 5px; line-height: 1.4; border: 1px dashed #aaa;" class="rosdesc" >
	            		<?php echo $cobranca->ros_detalhe; ?>
	            	</div>
	            	<span style="float: right;">
	            		
                        <input style="width: 50px; padding: 0px;" class="visualizar editaInad" id="<?php echo $cobranca->inadimplentes_ina_cod; ?>" type="button" value="Ficha" />
                        <input style="width: 40px; padding: 0px;" onclick="window.location='<?php echo base_url().'ro/novo/cod:'.$cobranca->cob_cod; ?>'" class="" id="<?php echo $cobranca->inadimplentes_ina_cod; ?>" type="button" value="R.O." />
                        <a href="" style="margin-left: 5px;" class="btHide"><img src="<?php echo $img.'icons/cross.png'?>" alt="OCULTAR" /></a>
	            	</span>
	            	<span class="rostipo" style="float: left; font-size: 12px;">
	            		<?php echo utf8_decode($cobranca->ros_usu_nome.' - '.$cobranca->ope_nome); ?>
	            		
            		</span>	            	
	            	<div style="clear: both"></div>
	            </div>
	            <?php endforeach; ?>
	            <?php endforeach; ?>
<!--
                <table id="products">
                    <tbody>         
                    	<?php foreach ($cobrancas as $cobranca): ?>		
                		<?php //var_dump($cobranca); ?>
                        <tr class="upper">
                            <td> <?php echo utf8_decode($cobranca->ina_nome); ?> </td>
                            <td><?php echo utf8_decode($cobranca->ope_nome); ?></td>
                            <td style="width: 80px;"> <?php echo convDataBanco($cobranca->last_ros_update); ?></td>
                            <td class="last" style="width: 100px;">
                                <div class="buttons">
                                    <input style="width: 50px;" class="visualizar editaInad" id="<?php echo $cobranca->inadimplentes_ina_cod; ?>" type="button" value="Ficha" />
                                    <input style="width: 40px;" onclick="window.location='<?php echo base_url().'ro/novo/cod:'.$cobranca->cob_cod; ?>'" class="" id="<?php echo $cobranca->inadimplentes_ina_cod; ?>" type="button" value="R.O." />
                                </div>
                            </td>
                        </tr>
                        <tr class="middle">
                        	<td colspan="5">
		                        <div style="width: 100%">
		                        	<?php echo convDataBanco($cobranca->ros_data). ' ' . $cobranca->ros_hora; ?><br />
		                        	<?php echo $cobranca->ros_detalhe; ?><br />
		                        </div>
                        	</td>
                    	</tr>
                        <tr class="down"><td colspan="5">&nbsp;</td></tr>
                    	<?php endforeach; ?>
                    </tbody>
                </table>
-->
                <div class="pagination pagination-left">
                    <div class="results">
                        <span>Total de <?php echo count($cobrancas); ?> registros encontrados</span>
                    </div>
                </div>
                <br/>
                <br/>
            </div>
<!--            <form id="form" action="">-->
                <div class="form">
                    <div class="fields">
                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <a href="<?php echo base_url().'pesquisar/juridico' ?>" style="text-decoration: none;" >
                                <div style="margin-left: 7px;" class="highlight">
                                    <input type="submit" name="submit.highlight" value="Nova pesquisa" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
<!--            </form>-->
        </div>
    </div>
</div>
