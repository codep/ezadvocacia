<script type="text/javascript">
    $(document).ready(function(){
        var base_url = "<?php echo base_url() ?>";
        $('input[name="honorario"]').mask('99.99').click(function(){$(this).select();});
        $('input[name="juros"]').mask('99.99').click(function(){$(this).select();});
        $('input[name="prazo"]').mask('9999').click(function(){$(this).select();});
        $('input[name="cpf_cnpj"]').attr('disabled',true).click(function(){$(this).select();});
        $('.telefone').mask("(99)9999-9999?9").blur(function(){
			    var phone, element;
			    element = $(this);
			    element.unmask();
			    phone = element.val().replace(/\D/g, '');
			    if(phone.length > 10) {
			        element.mask("(99)99999-999?9");
			    } else {
			        element.mask("(99)9999-9999?9");
			    }
			    //element.unmask();
			}).click(function(){$(this).select();});
        
        var fecharAba = '0';
        //------------------------------------------------------------------------------
        //---------------------------Fun��o para validar CNPJ---------------------------
        //------------------------------------------------------------------------------
        function validaCNPJ() {
            CNPJ = $('input[name="cpf_cnpj"]').val();
            erro = new String;
            if (CNPJ.length < 18) erro += "� necessario preencher corretamente o n�mero do CNPJ! \n\n";
            if ((CNPJ.charAt(2) != ".") || (CNPJ.charAt(6) != ".") || (CNPJ.charAt(10) != "/") || (CNPJ.charAt(15) != "-")){
                if (erro.length == 0) erro += "� necess�rio preencher corretamente o n�mero do CNPJ! \n\n";
            }
            //substituir os caracteres que n�o s�o n�meros
            if(document.layers && parseInt(navigator.appVersion) == 4){
                x = CNPJ.substring(0,2);
                x += CNPJ. substring (3,6);
                x += CNPJ. substring (7,10);
                x += CNPJ. substring (11,15);
                x += CNPJ. substring (16,18);
                CNPJ = x;
            } else {
                CNPJ = CNPJ. replace (".","");
                CNPJ = CNPJ. replace (".","");
                CNPJ = CNPJ. replace ("-","");
                CNPJ = CNPJ. replace ("/","");
            }
            var nonNumbers = /\D/;
            if (nonNumbers.test(CNPJ)) erro += "A verifica��o de CNPJ suporta apenas n�meros! \n\n";
            var a = [];
            var b = new Number;
            var c = [6,5,4,3,2,9,8,7,6,5,4,3,2];
            for (i=0; i<12; i++){
                a[i] = CNPJ.charAt(i);
                b += a[i] * c[i+1];
            }
            if ((x = b % 11) < 2) { a[12] = 0 } else { a[12] = 11-x }
            b = 0;
            for (y=0; y<13; y++) {
                b += (a[y] * c[y]);
            }
            if ((x = b % 11) < 2) { a[13] = 0; } else { a[13] = 11-x; }
            if ((CNPJ.charAt(12) != a[12]) || (CNPJ.charAt(13) != a[13])){
                erro +="CNPJ INV�LIDO, Por favor verifique!";
            }
            if (erro.length > 0){
                alert(erro);
                return false;
            }
            return true;
        }
        //------------------------------------------------------------------------------
        //---------------------------Fun��o para validar CPF----------------------------
        //------------------------------------------------------------------------------
        function validaCPF() {
            cpf = $('input[name="cpf_cnpj"]').val();

            cpf = cpf.replace(".", "");
            cpf = cpf.replace(".", "");
            cpf = cpf.replace("-", "");

            erro = new String;
            if (cpf.length < 11) erro += "Sao necessarios 11 digitos para verificacao do CPF! \n\n";
            var nonNumbers = /\D/;
            if (nonNumbers.test(cpf)) erro += "A verificacao de CPF suporta apenas numeros! \n\n";
            if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999"){
                erro += "Numero de CPF invalido!"
            }
            var a = [];
            var b = new Number;
            var c = 11;
            for (i=0; i<11; i++){
                a[i] = cpf.charAt(i);
                if (i < 9) b += (a[i] * --c);
            }
            if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
            b = 0;
            c = 11;
            for (y=0; y<10; y++) b += (a[y] * c--);
            if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
            if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10])){
                erro +="Numero de CPF invalido!, Por Favor confira";
            }
            if (erro.length > 0){
                alert(erro);
                return false;
            }
            return true;
        }
        //------------------------------------------------------------------------------
        //--------------------------- Fim das Valida��es -------------------------------
        //------------------------------------------------------------------------------

        $('button[name="pesquisarIC"]').hide();
        
        //AO CLICAR EM PESSOA JURIDICA
        $('input[name="pessoa"][value="j"]').click(function() {
            pessoa='j';
            //EXIBE RAZ�O SOCIAL
            $('#razao_social_field').show();

            //EXIBE BOT�O PESQUISAR
            $('button[name="pesquisarIC"]').fadeIn(function(){
                //EXIBE BOT�O PESQUISAR
            });

            $('label[for="nome"]').text('Nome Fantasia:');
            //DESMARCAR O SEXO
            $("input[name='sexo']").each(function() {
                //desmarcar
                this.checked = false;
            });

            //DESMARCAR ESTADO CIVIL AINDA N�O FUNCIONA
            $('#est_civil option[value="0"]').attr('selected', true);

            //RESETAR CPF/CNPJ
            $('#cpf_cnpj').html('<input style="width: 124px;" type="text" name="cpf_cnpj" />');

            //HABILITAR CAMPO E APLICAR M�SCARA PARA CPF
            $('input[name="cpf_cnpj"]').attr('disabled',false).mask('99.999.999/9999-99');
            $('input[name="cpf_cnpj"]').attr('value', '');
        });

        //AO CLICAR EM PESSOA F�SICA
        $('input[name="pessoa"][value="f"]').click(function() {
            pessoa='f';
            //OCULTA RAZ�O SOCIAL
            $('#razao_social_field').fadeOut(function(){
                
            });

            //EXIBE BOT�O PESQUISAR
            $('button[name="pesquisarIC"]').fadeIn(function(){
                //EXIBE BOT�O PESQUISAR
            });

            //SETA NULO NO INPUT RAZ�O SOCIAL
            $('input[name="razao_social"]').val('');

            //ALTERA O LABEL DE NOME FANTASIA PARA NOME
            $('label[for="nome"]').text('Nome:');

            //RESETAR CPF/CNPJ
            $('#cpf_cnpj').html('<input style="width: 124px;" type="text" name="cpf_cnpj" />');

            //APLICAR M�SCARA PARA CNPJ
            $('input[name="cpf_cnpj"]').attr('disabled',false).mask('999.999.999-99');
            $('input[name="cpf_cnpj"]').attr('value', '');
        });



        //VALIDA��O DOS CAMPOS OBRIGAT�RIOS
        $('form').submit(function(){

            if(($('input[name="cpf_cnpj"]').val())!=''){
                if(pessoa=='f'){
                    ret = validaCPF();
                    if(ret==false){
                        return false;
                    }
                }else if(pessoa=='j'){
                    ret = validaCNPJ();
                    if(ret==false){
                        return false;
                    }
                }
            }

            var pessoaVazio = true;
            $('input[name="pessoa"]').each(function() {
                if ( $(this).is(':checked') ) {
                    pessoaVazio = false;
                }
            });

            if($('input[name="nome_fantasia"]').val() == ''){
                $('input[name="nome_fantasia"]').focus();
                return false;
            }
            else if(pessoaVazio){
                alert('Indique pessoa F�SICA ou JUR�DICA.');
                return false;
            }

            else if($('input[name="honorario"]').val() == ''){
                $('input[name="honorario"]').focus();
                return false;
            }

            else if($('input[name="juros"]').val() == ''){
                $('input[name="juros"]').focus();
                return false;
            }

            else if($('input[name="prazo"]').val() == ''){
                $('input[name="prazo"]').focus();
                return false;
            }

            else if($('#cidade option:selected').val() == '' || $('#uf option:selected').val() == ''){
                alert('Selecione uma CIDADE e um ESTADO');
                return false;
            }


            else if($('input[name="responsavel"]').val() == ''){
                $('input[name="responsavel"]').focus();
                return false;
            }

            else if($('input[name="senha"]').val() != $('input[name="senha_confirma"]').val()){
                if($('input[name="usuario"]').val() == '')
                    alert('Digite um USU�RIO');
                else
                    alert('Confirme a SENHA corretamente');
                return false;
            }

            else{
                var marcado = false;
                
                $(".checkpasse").each(function() {
                    if(this.checked){
                        marcado = true;
                        return false;
                    }
                    
                });
                if(!marcado){
                    alert('Selecione ao menos um tipo de REPASSE')
                    return false;
                }
                
            }
            fecharAba = '1';
        });
        //LISTANDO AS CIDADES
        $('select[id="uf"]').change(function(){
           
            v1 = '';
            //captura os options selecionados
            v1 = $('select[id="uf"] option:selected').text();
            if(v1 == '') v1='UF';
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() . 'credor/listar_cidades'; ?>",
                data: 'v1='+v1,
                dataType: 'json',
                error: function(xhr, status, er) {
                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                },
                success: function(retorno)
                {
                    $('#cidade').html('<option value="">SELECIONE UMA CIDADE</option>');//zera os valores antigos e escreve no elemento o valor selecione
                    $qtd = '';
                    $valor = '';
                    $.each(retorno.cidadesbrasil, function(i, v){//para cada �ndice retornado executa a fun��o
                        if(v.cidadeatual != ''){
                            $('#cidade-button').fadeOut('normal',function(){
                                //ESCONDE O OPTION INICIAL SEM VALOR
                            });
                            $('#cidade').append('<option value="'+unescape(v.cidadeatual)+'">'+unescape(v.cidadeatual)+'</option>');//unescape descriptografa o padr�o URL de dados retornado no PHP
                        }
                        $qtd = i;
                        $valor = v.cidadeatual;
                    });
                    if ($qtd != 0 && $valor != ''){
                        $('#cidade').fadeIn('normal', function(){
                            //CIDADES CARREGADAS COM SUCESSO
                        });
                    }
                    else
                    {
                        alert('Voc� deve selecionar um ESTADO v�lido.');
                        $('#cidade').html('<option value=""><-- SELECIONE UM ESTADO</option>');//zera os valores antigos e escreve no elemento o valor selecione
                    }
                }
            });
        });

        //LISTANDO OS RESULTADOS DA PESQUISA
        /* *************************************************************************
         * PESQUISA INADIMPLETE OU CREDOR POR NOME OU CPF/CNPJ
         * pesquisaIC($entidade, $pessoa, $tipoDado, $valor)
         * $entidade -------- i=inadimplentes, c=credores
         * $pessoa ---------- j=jur�dica, f=f�sica
         * $tipoDado -------- n=nome, c=cpf/cnpj
         * $valor ----------- valor do nome ou cpf/cnpj
         *
         * NECESS�RIO CONFIGURAR O VALOR DOS ATRIBUTOS NAME DOS INPUTS
         ************************************************************************* */
        $(".dialog-form-open").click(function () {
            //ENTIDADE DO TIPO CREDOR
            entidade = 'c';

            //RECEBENDO O TIPO DE PESSOA [f ou j]
            pessoa = '';
            $('input[name="pessoa"]').each(function() {
                if ( $(this).is(':checked') ) {
                    pessoa = $(this).attr('value');
                }
            });

            //RECEBENDO O TIPO DE DADOS [nome_fantasia ou cpf_cnpj]
            tipodado = '';

            //RECEBENDO O VALOR DIGITADO EM nome_fantasia ou cpf_cnpj
            valor = '';
            if(($('input[name="nome_fantasia"]').val() == '') && ($('input[name="cpf_cnpj"]').val() == '')){
                $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Para pesquisar � necess�rio digitar o NOME ou CPF/CNPJ</h3></td></tr>');//zera os valores antigos e escreve no elemento o valor selecione
                return false;
            }
            else {
                if ($('input[name="cpf_cnpj"]').val() != ''){
                    valor = $('input[name="cpf_cnpj"]').val();
                    tipodado = 'c';
                }

                else if ($('input[name="nome_fantasia"]').val() != ''){
                    valor = $('input[name="nome_fantasia"]').val();
                    tipodado = 'n';
                }
            }

            //FUN��O AJAX
            $.ajax({
                type: "POST",//TIPO DE DADOS
                url: base_url+'credor/listar_pesquisa',//FUN��O DO CONTROLLER PARA TRATAR A LISTAGEM
                data: 'entidade='+entidade+'&pessoa='+pessoa+'&tipodado='+tipodado+'&valor='+valor,//DADOS ENVIADOS
                dataType: 'json',//TIPO DE DADOS A SER RECEBIDO

                //CASO HAJA ERRO DE PROCESSAMENTO
                error: function(xhr, status, er) {
                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                },

                //CASO A FUN��O DO CONTROLLER SEJA EXECUTADA COM SUCESSO E OS DADOS RECEBIDOS COM SUCESSO
                success: function(retorno)
                {
                    $("#dialog-form").dialog("open");///ABRE A CAIXA DE DI�LOGO
                    $('#dadosPesquisa').html('');//zera os valores antigos e escreve no elemento o valor selecione
                    vazio = false;//VARI�VEL PARA IDENTIFICAR SE ALGUM DADO �NTEGRO TENHA SIDO RECEBIDO
                    
                    $.each(retorno.resultado, function(i, v){//para cada �ndice retornado executa a fun��o
                        if(v.nome != null){
                            //CONCATENA AS LINHAS DA TABELA COM OS VALORES RECEBIDOS
                            $('#dadosPesquisa').append(
                            '<tr class="tuplas" id="'+unescape(v.codigo)+'">'+
                                '<td>'+unescape(v.nome)+
                                '<td>'+unescape(v.cpf_cnpj)+
                                '<td>'+unescape(v.endereco)+
                                '<td class="last">'+unescape(v.codigo)+
                                '</tr>'
                        );
                        }
                        else vazio = true;//CASO OS DADOS RETORNADOS SEJAM NULOS
                    });
                    $('.tuplas').css('cursor','pointer');//SETA O TIPO DO CURSOR

                    //AO CLICAR SOBRE UMA LINHA DA TABALEA COM A CLASSE TUPLA
                    $('.tuplas').click(function(e){
                        e.preventDefault();
                        codigo = $(this).attr('id');
                        link = base_url+'credor/editar/cod:'+codigo;
                        window.open(link, 'blank');
                    });

                    //CASO OS DADOS RETORNADOS SEJAM NULOS
                    if (vazio)
                    {   //EXIBE MENSAGEM
                        $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Nenhum dado encontrado pesquisando por ['+valor+']</h3></td></tr>');
                    }
                }
            });
        });
        window.onbeforeunload = function() {
            if(fecharAba != '1'){
                return ' Se voc� fechar essa janela ir� perder tudo que voc� digitou at� agora. ';
            }
        }
    });
</script>
<div id="dialog-form" style="padding-left: 5px; padding-right: 5px; " title="Pesquisar dados">
    <p style="text-align: center">
        <strong>Dados encontrados:</strong>
    </p>
    <div id="content" style="width: 750px;">
        <div id="right" style="margin-left: 0px;">
            <div class="box">
                <div class="table" style="padding-left: 0px; font-size: 10px; width: 750px;">
                    <form action="" method="post">
                        <table id="products">
                            <thead>
                                <tr>
                                    <th class="left" style="width: 20%;">Nome</th>
                                    <th style="width: 14%;">CPF/CNPJ</th>
                                    <th>ENDERE�O</th>
                                    <th class="last">CIDADE</th>
                                </tr>
                            </thead>
                            <tbody id="dadosPesquisa">
                                <!--RESULTADO DA PESQUISA AQUI!! -->
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="content">

    <?php echo $sidebar; ?>

    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Cadastro de credor</h5>
            </div>
            <?php echo $mensagem; ?>
            <form id="form" action="<?php echo base_url() . 'credor/incluir'; ?>" method="post" enctype="multipart/form-data">
                <div class="form">

                    <div class="fields">
                        <div class="field  field-first">
                            <div class="divleft" style="width: 116px;">
                                <div class="label">
                                    <label>C�digo:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;">[novo]</p>
                            </div>
                            <div class="divleft" style="width: 200px;">
                                <div class="label">
                                    <label>Pessoa:</label>
                                </div>
                                <input style="margin-top: 7px; " type="radio" name="pessoa" value="f" id="pfisica">F�sica
                                <input type="radio" name="pessoa" value="j" id="pjuridica">Jur�dica
                            </div>
                            <div id="atrsPessoaFisica" style="width:375px; height:30px; float: right; overflow: hidden">
                                <div id="sexo_est_civil">
                                    <div class="divleft" style="width: 173px;" id="sexo" >
                                        <div class="label">
                                            <label>Sexo:</label>
                                        </div>
                                        <input type="radio" name="sexo" value="m" style="margin-top: 7px;">Masc.
                                        <input type="radio" name="sexo" value="f">Fem.
                                    </div>
                                    <div class="divleftlast" style="width: 184px;" id="estadocivil">
                                        <div class="select">
                                            <select style="width: 186px; margin-left: 5px;" id="est_civil" name="estado_civil">
                                                <option value="0" class="ec">Estado civil</option>
                                                <option value="1" class="ec">Solteiro(a)</option>
                                                <option value="2" class="ec">Casado(a)</option>
                                                <option value="3" class="ec">Separado(a)</option>
                                                <option value="4" class="ec">Divorciado(a)</option>
                                                <option value="5" class="ec">Viuvo(a)</option>
                                                <option value="6" class="ec">Amasiado(a)</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field" id="razao_social_field">
                            <div class="divleftlast" style="margin-left: 0px; width: 706px;">
                                <div style="width: 89px;" class="label">
                                    <label for="razao_social">Raz�o Social:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 591px;" type="text" id="razao_social" name="razao_social" maxlength="120"/>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 371px;">
                                <div style="width: 89px;" class="label">
                                    <label for="nome">Nome Fantasia:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 248px;" type="text" id="nome" name="nome_fantasia" maxlength="120" value=""/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 231px;">
                                <div style="width: 76px; padding-left: 1px;" class="label">
                                    <label for="cpf_cnpj">CPF/CNPJ:</label>
                                </div>
                                <div class="input" id="cpf_cnpj">
                                    <input style="width: 124px;" type="text" name="cpf_cnpj" maxlength="18"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <button style="width: 85px; padding: 4px;" name="pesquisarIC" type="button" class="dialog-form-open">Pesquisar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 211px;">
                                <div style="width: 89px;" class="label">
                                    <label for="rg_ie">RG/I.E.:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 89px;" type="text" id="rg_ie" name="rg_ie" maxlength="30"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 250px;">
                                <div style="width: 42px; padding-left: 1px;" class="label">
                                    <label for="endereco">End.:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 178px;" type="text" id="endereco" name="endereco" maxlength="130"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 233px; margin: 0px;">
                                <div style="width: 51px; padding-left: 1px;" class="label">
                                    <label for="bairro">Bairro:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 155px;" type="text" id="bairro" name="bairro" maxlength="50"/>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 211px;">
                                <div style="width: 89px;" class="label">
                                    <label for="complemento">Compl.:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 89px;" type="text" id="complemento" name="complemento" maxlength="40"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 132px;">
                                <div style="width: 34px; padding-left: 1px;" class="label">
                                    <label for="cep">CEP.:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 65px;" type="text" id="cep" name="cep" maxlength="8"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 109px;">
                                <div style="width: 27px; padding-left: 1px;" class="label">
                                    <label for="uf">UF:</label>
                                </div>
                                <div class="select">
                                    <select style="width: 65px; margin-left: 7px;" id="uf" name="uf">
                                        <option value="" selected>UF</option>
                                        <?php foreach ($estados as $uf): ?>
                                            <option value="<?php echo $uf->cid_estado; ?>"><?php echo $uf->cid_estado; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 226px; margin: 0px; float: left">
                                <div class="select">
                                    <select style="width: 230px; height: 28px; border: 1px solid #D4D0C8; padding: 5px; font-size: 11px; font-family: Verdana" id="cidade" name="cidade">
                                        <option value="" id="cidadevazia"><-- SELECIONE UM ESTADO</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 240px;">
                                <div style="width: 89px;" class="label">
                                    <label for="fone1">Fone1:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 117px;" type="text" id="fone1" name="fone1" class="telefone" maxlength="15"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 238px;">
                                <div style="width: 77px; padding-left: 1px;" class="label">
                                    <label for="fone2">Fone2:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 130px;" type="text" id="fone2" name="fone2" class="telefone" maxlength="15"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 216px; margin: 0px;">
                                <div style="width: 59px; padding-left: 1px;" class="label">
                                    <label for="fax">Fax:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 130px;" type="text" id="fax" name="fax" class="telefone" maxlength="15"/>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 371px;">
                                <div style="width: 89px;" class="label">
                                    <label for="site">Site:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 248px;" type="text" id="site" name="site" maxlength="100"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 326px; margin-left: 0px;">
                                <div style="width: 50px; padding-left: 0px;" class="label">
                                    <label for="email">E-Mail:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 256px;" type="text" id="email" name="email"/>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleftlast" style="width: 471px; margin-left: 0px;">
                                <div style="width: 89px;" class="label">
                                    <label for="site">Logotipo:</label>
                                </div>
                                <div class="input input-file">
                                    <input type="file" name="logo"/>
                                </div>
                            </div>
                        </div>
                        <div style="height: 75px;" class="field">
                            <div class="divleftlast" style="width: 701px;">
                                <div style="width: 84px;" class="label">
                                    <label for="outras_info">Outras informa��es:</label>
                                </div>
                                <div class="textarea">
                                    <textarea style="width: 586px; height: 50px;" id="outras_info" name="info" cols="500" rows="4" class="editor"></textarea>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                            Contatos na empresa
                        </div>
                        <div class="field">
                            <div class="divleft" style="border: none; width: 166px;">
                                <div style="margin-left: 6px;" class="select">
                                    <select style="width: 160px;" id="parenteses" name="cont1_cargo">
                                        <option value="">[CARGO]</option>
                                        <option>Gerente geral</option>
                                        <option>Gerente de cobran�a</option>
                                        <option>Gerente financeiro</option>
                                        <option>Gerente de vendas</option>
                                        <option>Gerente[outro]</option>
                                        <option>Operador(a) de Caixa</option>
                                        <option>Vendedor(a)</option>
                                        <option>Auxiliar administrativo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="divleft" style="width: 186px;">
                                <div class="input">
                                    <input style="width: 161px;" type="text" id="input-medium" name="cont1_nome" maxlength="50"/>
                                </div>
                            </div>
                            <div class="divleft" style="margin-right: 4px; width: 171px;">
                                <div style="width: 51px; padding-left: 1px;" class="label">
                                    <label for="input-medium">E-mail:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 90px;" type="text" id="input-medium" name="cont1_email" maxlength="100"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 162px;">
                                <div style="width: 45px; padding-left: 1px;" class="label">
                                    <label for="input-medium">Fone:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 90px;" type="text" id="input-medium" name="cont1_fone" class="telefone" maxlength="15"/>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="border: none; width: 166px;">
                                <div style="margin-left: 6px;" class="select">
                                    <select style="width: 160px;" id="parenteses" name="cont2_cargo">
                                        <option value="">[CARGO]</option>
                                        <option>Gerente geral</option>
                                        <option>Gerente de cobran�a</option>
                                        <option>Gerente financeiro</option>
                                        <option>Gerente de vendas</option>
                                        <option>Gerente[outro]</option>
                                        <option>Operador(a) de Caixa</option>
                                        <option>Vendedor(a)</option>
                                        <option>Auxiliar administrativo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="divleft" style="width: 186px;">
                                <div class="input">
                                    <input style="width: 161px;" type="text" id="input-medium" name="cont2_nome" maxlength="50"/>
                                </div>
                            </div>
                            <div class="divleft" style="margin-right: 4px; width: 171px;">
                                <div style="width: 51px; padding-left: 1px;" class="label">
                                    <label for="input-medium">E-mail:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 90px;" type="text" id="input-medium" name="cont2_email" maxlength="100"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 162px;">
                                <div style="width: 45px; padding-left: 1px;" class="label">
                                    <label for="input-medium">Fone:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 90px;" type="text" id="input-medium" name="cont2_fone" class="telefone" maxlength="15"/>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 16px;" class="blocoTitulo">
                            Dados bancarios
                        </div>
                        <div style="padding-top: 5px;" class="field">
                            <div class="divleft" style="width: 409px;">
                                <div style="margin-left: 6px;" class="select">
                                    <select style="width: 394px;" id="banco" name="banco">
                                        <option value="" selected>[SELECIONE UM BANCO]</option>
                                        <?php foreach ($bancos as $ban): ?>
                                            <option value="<?php echo $ban->banco_cod; ?>"><?php echo utf8_decode($ban->banco_nome); ?></option>
                                        <?php endforeach; ?>
                                        <option value="[OUTRO]">[OUTRO]</option>
                                    </select>
                                </div>
                            </div>
                            <div class="divleftlast" style="margin-left: 2px; width:130px;">
                                <div style="width: 60px; padding-left: 1px;" class="label">
                                    <label for="agencia">Ag�ncia:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 37px;" type="text" id="agencia" name="agencia" maxlength="10"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="margin-left: 2px; width:160px;">
                                <div style="width: 44px; padding-left: 1px;" class="label">
                                    <label for="conta">Conta:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 89px;" type="text" id="conta" name="conta" maxlength="15"/>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleftlast" style="margin-left: 0px; width: 706px;">
                                <div style="width: 89px;" class="label">
                                    <label for="sacado">Sacado:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 591px;" type="text" id="sacado" name="sacado" maxlength="50"/>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                            Cobran�a
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 240px;">
                                <div style="width: 89px;" class="label">
                                    <label for="honorario">Honor�rio:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 117px;" type="text" id="honorario" name="honorario" value="" maxlength="8"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 238px;">
                                <div style="width: 77px; padding-left: 1px;" class="label">
                                    <label for="juros">Juros:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 130px;" type="text" id="juros" name="juros" value="" maxlength="8"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 216px; margin: 0px;">
                                <div style="width: 80px; padding-left: 1px;" class="label">
                                    <label for="fax">Praz. Jurid.:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 109px;" type="text" id="fax" name="prazo" value="" maxlength="4"/>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleftlast" style="width: 89px; margin-left: 0px;">
                                <div style="width: 89px;" class="label">
                                    <label for="repasse">Repasses atribuidos:</label>
                                </div>
                            </div>

                            <div id="chekboxesRepasses" class="label" style="width: 605px; font-size: 10px;">
                                <?php foreach ($repasses as $rep): ?>
                                    <div style="float:left">
                                        <input type="checkbox" name="repasse[]" value="<?php echo $rep->rep_cod; ?>" class="checkpasse"/>
                                        <label style="font-weight: normal; padding-right: 15px;"><?php echo utf8_decode($rep->rep_nome); ?></label>
                                    </div>
                                <?php endforeach; ?>
                            </div>


                        </div>
                        <!--                        <div>
                                                    <div style="margin-top: 10px; padding-bottom: 14px; margin-left: 5px; width: 641px; float: left; margin-bottom: 3px;" class="blocoTitulo">
                                                        Representante Legal
                                                    </div>
                                                    <div style="margin-top: 10px; margin-left: 10px; width: 50px; float: left;" class="blocoTitulo">
                                                        <a href="#">
                                                            <img src="< ?php echo $img.'mais.jpg'?>" alt="+"/>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="field">
                                                    <div class="divleft" style="width: 300px;">
                                                        <div style="width: 89px;" class="label">
                                                            <label for="nome_rep">Nome:</label>
                                                        </div>
                                                        <div class="input">
                                                            <input style="width: 177px;" type="text" id="nome_rep" name="nome_rep"/>
                                                        </div>
                                                    </div>
                                                    <div class="divleft" style="width: 201px;">
                                                        <div style="width: 46px; padding-left: 1px;" class="label">
                                                            <label for="cpf_rep">CPF:</label>
                                                        </div>
                                                        <div class="input">
                                                            <input style="width: 124px;" type="text" id="cpf_rep" name="cpf_rep"/>
                                                        </div>
                                                    </div>
                                                    <div class="divleftlast" style="width: 184px;">
                                                        <div class="select">
                                                            <select style="width: 186px; margin-left: 5px;" id="est_civil" name="est_civil">
                                                                <option value="1">Estado civil</option>
                                                                <option value="2">Solteiro(a)</option>
                                                                <option value="3">Casado(a)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="field">
                                                    <div class="divleft" style="width: 211px;">
                                                        <div style="width: 89px;" class="label">
                                                            <label for="rg_rep">RG:</label>
                                                        </div>
                                                        <div class="input">
                                                            <input style="width: 89px;" type="text" id="rg_rep" name="rg_rep"/>
                                                        </div>
                                                    </div>
                                                    <div class="divleft" style="width: 250px;">
                                                        <div style="width: 42px; padding-left: 1px;" class="label">
                                                            <label for="endereco_rep">End.:</label>
                                                        </div>
                                                        <div class="input">
                                                            <input style="width: 178px;" type="text" id="endereco_rep" name="endereco_rep"/>
                                                        </div>
                                                    </div>
                                                    <div class="divleftlast" style="width: 233px; margin: 0px;">
                                                        <div style="width: 51px; padding-left: 1px;" class="label">
                                                            <label for="bairro_rep">Bairro:</label>
                                                        </div>
                                                        <div class="input">
                                                            <input style="width: 155px;" type="text" id="bairro_rep" name="bairro_rep"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="field">
                                                    <div class="divleft" style="width: 211px;">
                                                        <div style="width: 89px;" class="label">
                                                            <label for="complemento_rep">Compl.:</label>
                                                        </div>
                                                        <div class="input">
                                                            <input style="width: 89px;" type="text" id="complemento_rep" name="complemento_rep"/>
                                                        </div>
                                                    </div>
                                                    <div class="divleft" style="width: 132px;">
                                                        <div style="width: 34px; padding-left: 1px;" class="label">
                                                            <label for="cep_rep">CEP.:</label>
                                                        </div>
                                                        <div class="input">
                                                            <input style="width: 65px;" type="text" id="cep_rep" name="cep_rep"/>
                                                        </div>
                                                    </div>
                                                    <div class="divleft" style="width: 260px; margin: 0px;">
                                                        <div style="width: 59px; padding-left: 1px;" class="label">
                                                            <label for="cidade_rep">Cidade:</label>
                                                        </div>
                                                        <div class="input">
                                                            <input style="width: 170px;" type="text" id="cidade_rep" name="cidade_rep"/>
                                                        </div>
                                                    </div>
                                                    <div class="divleftlast" style="width: 84px;">
                                                        <div class="select">
                                                            <select style="width: 80px; margin-left: 7px;" id="estado_rep" name="estado_rep">
                                                                <option value="1">UF</option>
                                                                <option value="2">SP</option>
                                                                <option value="3">MG</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="field">
                                                    <div class="divleft" style="width: 240px;">
                                                        <div style="width: 89px;" class="label">
                                                            <label for="fone_rep">Fone:</label>
                                                        </div>
                                                        <div class="input">
                                                            <input style="width: 117px;" type="text" id="fone_rep" name="fone_rep"/>
                                                        </div>
                                                    </div>
                                                    <div class="divleft" style="width: 238px;">
                                                        <div style="width: 77px; padding-left: 1px;" class="label">
                                                            <label for="cel_rep">Celular:</label>
                                                        </div>
                                                        <div class="input">
                                                            <input style="width: 130px;" type="text" id="cel_rep" name="cel_rep"/>
                                                        </div>
                                                    </div>
                                                    <div class="divleftlast" style="width: 216px; margin: 0px;">
                                                        <div style="width: 59px; padding-left: 1px;" class="label">
                                                            <label for="email_rep">E-mail:</label>
                                                        </div>
                                                        <div class="input">
                                                            <input style="width: 130px;" type="text" id="email_rep" name="email_rep"/>
                                                        </div>
                                                    </div>
                                                </div>-->

                        <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                            Dados para acesso ao sistema
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 240px;">
                                <div style="width: 89px;" class="label">
                                    <label for="input-medium">Usu�rio:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 117px;" type="text" id="input-medium" name="usuario" maxlength="20"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 218px;">
                                <div style="width: 77px; padding-left: 1px;" class="label">
                                    <label for="input-medium">Senha:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 110px;" type="password" id="input-medium" name="senha" maxlength="40"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 236px; margin: 0px;">
                                <div style="width: 102px; padding-left: 1px;" class="label">
                                    <label for="input-medium">Repita a senha:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 110px;" type="password" id="input-medium" name="senha_confirma" maxlength="40"/>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 12px; margin-bottom: 3px;" class="blocoTitulo">
                            Recuperador respons�vel
                        </div>
                        <div class="field">
                            <div style="margin-left: 221px; border: none; width: 245px;">
                                <div style="margin-left: 6px;" class="select">
                                    <select style="width: 260px;" id="recuperador" name="responsavel">
                                        <?php foreach ($usuarios as $usu): ?>
                                            <option value="<?php echo $usu->usu_cod; ?>"><?php echo $usu->usu_usuario_sis; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <input type="reset" name="cancelar" value="Cancelar" />
                            <div style="margin-left: 7px;" class="highlight">
                                <input type="submit" value="Cadastrar" tititle="Cadastrar"/>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
<!--            <table border="1">
                <tr>
                    <th>NOME</th>
                    <th>CPF/CNPJ</th>
                    <th>ENDERE�O</th>
                    <th>CIDADE</th>
                </tr>
                < ?php foreach ($pesquisateste as $dados):?>
                <tr>
                    <td>< ?php echo $dados->nome;?></td>
                    <td>< ?php echo $dados->cpf_cnpj;?></td>
                    <td>< ?php echo $dados->endereco;?></td>
                    <td>< ?php echo $dados->cidade;?></td>
                </tr>
                < ?php endforeach;?>
            </table>-->
        </div>
    </div>

</div>