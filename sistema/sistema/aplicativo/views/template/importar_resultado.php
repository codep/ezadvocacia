<script type="text/javascript">
</script>
<style type="text/css">
    a{
        color: black;
    }
    .tabela{
        width: 702px !important;
        margin: 7px 0 7px 20px !important;
    }
    .tabela td{
        padding: 2px !important;
        text-align: center !important;
    }
    #content div.box table th{
    padding: 5px;
    vertical-align: middle;
}
.vertCenter{
    vertical-align: middle;
    text-align: center;
}
.okRed{
    color: red;
}
.okRed:hover{
    color: red !important;
}

</style>

<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Importa��o</h5>
            </div>
            <?php echo $mensagem ?>
            <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                     Importa��o efetuada com sucesso:
            </div>
            <table class="tabela">
                <thead>
                    <tr>
                        <th colspan="5"><?php echo $credorDados->cre_nome_fantasia.' ('.$credorDados->cre_cpf_cnpj.')' ?></th>
                        <th colspan="4" class="last">
<!--                            <div style="float: left;">-->
<!--                                <select style="width: 140px;">
                                    <option value="0">[Aplicar Repasse]</option>
                                    < ?php foreach ($repasses as $repasse):?>
                                        < ?php if ($repasse != ''):?>
                                            <option value="< ?php echo $repasse ?>">< ?php echo $repasse ?></option>
                                        < ?php endif;?>
                                    < ?php endforeach;?>
                                </select>-->
<!--                            </div>-->
<!--                            <div style="padding-top: 2px; float: left; margin-left: 2px;">-->
<!--                                <input type="button" value="Todos"/>-->
<!--                            </div>-->
                        </th>
                    </tr>
                    <tr>
                        <th>Data</th>
                        <th>Inad</th>
                        <th>Cap</th>
                        <th>Venc</th>
<!--                        <th>Repasse</th>-->
                        <th>Parc</th>
                        <th class="last">Dup</th>
<!--                        <th>Import</th>-->
<!--                        <th class="last">Efetivar</th>-->
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dividas as $divida):?>
                    <tr>
                        <td><?php echo convData($divida->div_emissao,'d') ?></td>
                        <td><a hraf="#"><?php echo $divida->ina_nome ?></a></td>
                        <td class="vertCenter"><?php echo convMoney($divida->div_total) ?></td>
                        <td><?php echo convData($divida->div_venc_inicial,'d') ?></td>
<!--                        <td>-->
<!--                            <div style="width: 45px; padding-left: 15px">-->
<!--                                <select>
                                    < ?php foreach ($repasses as $repasse):?>
                                        < ?php if ($repasse != ''):?>
                                            <option value="< ?php echo $repasse ?>">< ?php echo $repasse ?></option>
                                        < ?php endif;?>
                                    < ?php endforeach;?>
                                </select>-->
<!--                            </div>-->
<!--                        </td>-->
                        <td class="vertCenter"><?php echo $divida->div_qtd_parc ?></td>
                        <td class="vertCenter last"><?php echo $divida->imp_duplicidade == '0'?'N�o':'Sim' ?></td>
<!--                        <td class="vertCenter"><a href="#">N�o</a></td>
                        <td class="vertCenter last"><a class="okRed" href="#">[OK]</a></td>-->
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <br/>
            <a style="display: block; margin-top: 15px; text-align: center; font-size: 20px;" href="<?php echo base_url().'importacao/principal' ?>">Voltar</a>
        </div>
    </div>
</div>


           