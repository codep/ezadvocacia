<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img.'logo_elisangela.jpg';?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br|www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo $title ;?></p>
        </div>
    </div>
    <table id="tableAdm01" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="thInadimplente">Inadimplente</th>
                <th id="thCredor">Credor</th>
                <th id="th3">Tipo</th>
                <th id="th4">�ltimo RO</th>
                <th id="th5">Parcelas<br/> em atraso</th>
                <th id="th6" class="last">1� Venc.</th>
            </tr>
        </thead>
        <tbody>
            <?php $totalDeInad = 0; foreach ($acordPrevAtra as $acoPreAtra) : ?>
            <tr>
           
                <td class="tdInaNome"><?php echo utf8_decode($acoPreAtra->ina_nome); ?></td>
                <td class="tdCreNomeFantasia"><?php echo utf8_decode($acoPreAtra->cre_nome_fantasia); ?></td>
                <td class="td3">
                   <?php
                   /*verificando se � acordo ou previs�o*/
                         if ($acoPreAtra->aco_tipo == 1)//1 = acordo
                                 {
                                   echo "A"; //A = Acordo
                                 }
                         else if ($acoPreAtra->aco_tipo == 2)//2 = previs�o
                                 {
                                    echo "P"; //P = previs�o
                                 }
                         else if ($acoPreAtra->aco_tipo == 3)//3 = Rec�m envido ao Judicial
                                 {
                                    echo "Rec. env. Jud."; //P = previs�o
                                 }
                         else//se por algum motivo (que nem o Padre Quevedo possa explicar), n�o vier aco_tipo 1 ou aco_tipo 2 n�o imprime nada
                                 {
                                    echo("");
                                 }
                   ?>
                </td>
                <td class ="tdUltimoRo" class="td4">
                    <strong>
                        <i>
                            <?php
                              $dataUltRo = convDataBanco($acoPreAtra->data_ult_ro);
                              echo ($dataUltRo == "//") ? "" : $dataUltRo; //se a data vier vazia o m�todo convDataBanco retorna s� as barra, se isso acontecer imprime nada
                              echo "&nbsp &nbsp";
                              echo $acoPreAtra->hora_ult_ro;
                            ?><br/>
                        </i>
                        <?php
                          echo utf8_decode($acoPreAtra->nome_op_ult_ro); echo "<br/>";
                        ?>
                    </strong>
                    <?php echo utf8_decode($acoPreAtra->ult_ros_detalhe); ?>
                </td>
                <td class="td5"><?php echo $acoPreAtra->qtd_par_atras; ?></td>
                <td class="last" class="td6"><?php $primeVenc = convDataBanco($acoPreAtra->prime_venc); echo ($primeVenc == "//") ? "" : $primeVenc; ?></td>
                <?php $totalDeInad ++; endforeach; ?>
            </tr>
            <tr>
                <td colspan="5" id="tdTotalA">TOTAL DE INADIMPLENTES EM ATRASO</td>
                <td id="tdTotalB" class="last"><?php echo "$totalDeInad"; ?></td>
            </tr>

        </tbody>
    </table>
</div>
