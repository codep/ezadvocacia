<script type="text/javascript">
	var fonte_valor_original = <?php echo $acordo->aco_valor_original; ?>;
	var fonte_valor_atualizado = <?php echo $acordo->aco_valor_atualizado; ?>;
	
	function getUnMaskVal(obj) {
		var val = $(obj).clone().priceFormat({prefix: '',centsSeparator:'.',thousandsSeparator: ''}).val();
		return parseFloat(val);
	}
	function setMaskVal(vlr) {
		return $('<input type="text" />').val(vlr).priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        }).val();
	}
	
	function calculaTotal() {
		soma_valor_original = 0;
		$.each( $('[id^="valor_original_"]'), function(i,o) {
			var vlr = getUnMaskVal(o);
			soma_valor_original = (parseFloat(soma_valor_original) + parseFloat(vlr)).toFixed(2);
		});
		if (soma_valor_original!=fonte_valor_original) {
			$('#total_valor_original').removeClass('verde').addClass('vermelho');
		} else {
			$('#total_valor_original').addClass('verde').removeClass('vermelho');
		}
		$('#total_valor_original').html( setMaskVal(soma_valor_original) );
		
		soma_valor_atualizado = 0;
		$.each( $('[id^="valor_atualizado_"]'), function(i,o) {
			var vlr = getUnMaskVal(o);
			soma_valor_atualizado = (parseFloat(soma_valor_atualizado) + parseFloat(vlr)).toFixed(2);
		});
		if (soma_valor_atualizado!=fonte_valor_atualizado) {
			$('#total_valor_atualizado').removeClass('verde').addClass('vermelho');
		} else {
			$('#total_valor_atualizado').addClass('verde').removeClass('vermelho');
		}
		$('#total_valor_atualizado').html( setMaskVal(soma_valor_atualizado) );
		
		//BOT�O SALVAR
		if (soma_valor_original!=fonte_valor_original || soma_valor_atualizado!=fonte_valor_atualizado) {
			$('#btDividirCobranca').attr('disabled',true);
		} else {
			$('#btDividirCobranca').attr('disabled',false);
		}
	}
	
	function applyPriceFormat() {
		//SETA PLUGIN DE MOEDA
        $('input.priceF').priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });
	}

	$(document).delegate('[id^="valor_original_"]', 'blur', function(){
		calculaTotal();
	});
	$(document).delegate('[id^="valor_atualizado_"]', 'blur', function(){
		calculaTotal();
	});

    $(document).ready(function(){
    	
		applyPriceFormat(); //INICIA PLUGIN DE FORMATO
    	
		calculaTotal(); //INICIA O C�LCULO DOS TOTAIS
		
		$('#btGerarDividas').click(function(e){
			e.preventDefault();
			var nlinhas = $('#num_cobrancas').val();
			
			if (isNaN(nlinhas) || nlinhas<=0) {
				alert('O n�mero de cobran�as a gerar deve ser um valor num�rico maior que zero.');
				$('#num_cobrancas').focus();
			} else {
				
				//SETA OS PRIMEIROS COM O VALOR ORIGINAL
				$('[id^="valor_original_"]').eq(0).val( fonte_valor_original.toFixed(2)  )
				$('[id^="valor_atualizado_"]').eq(0).val( fonte_valor_atualizado.toFixed(2) );
				
				//REMOVE AS LINHAS DA TABELA EXCLUINDO A PRIMEIRA
				$('table.inputTbl tbody tr[class!="linhaBaseClone"]').remove();
				
				//ADICIONA NOVAS LINHAS
				for (i = 0; i <nlinhas; i++) {
					if (i==0) continue; //IGNORA A PRIMEIRA LINHA
					var cloneTr = $('.linhaBaseClone').clone();
					cloneTr.removeClass('linhaBaseClone'); //REMOVE A CLASE CLONEBASE
					cloneTr.find('[name="aco_valor_original[]"]').attr('id','valor_original_'+i).val('0.00');
					cloneTr.find('[name="aco_valor_atualizado[]"]').attr('id','valor_atualizado_'+i).val('0.00');
					$('table.inputTbl tbody').append(cloneTr);
				}
				
				//REAPLICA PLUGIN DE VALORES
				applyPriceFormat(); //INICIA PLUGIN DE FORMATO				
				
				//CALCULA O TOTAL NOVAMENTE
				calculaTotal();
				
			}
		});
		
		//REMOVE FORMATA��ES ANTES DE ENVIAR O FORMUL�RIO
		$('#formAcordo').submit(function(e){
			//N�O DEIXAR A PRIMEIRA COBRAN�A SER "ZERADA"
			var firstRowVO = parseFloat(getUnMaskVal($('[id^="valor_original_"]').eq(0)));
			var firstRowVA = parseFloat(getUnMaskVal($('[id^="valor_atualizado_"]').eq(0)));
			if (firstRowVO<=0 || firstRowVA<=0) {
				alert('Voc� n�o pode deixar a primeira linha com valores em branco.');
				e.preventDefault();
			} else {
				//REMOVE FORMATA��O DE VALORES
		        $('input.priceF').priceFormat({
		            prefix: "",
		            centsSeparator: ".",
		            thousandsSeparator: ""
		        });
	        }
		});
    });
</script>
<style>
	.verde { color: green; }
	.vermelho { color: red; }
	#content div.box table.inputTbl td {
		padding: 2px;
		padding-left: 5px;
	}
	#content div.box table.inputTbl tfoot td {
		padding: 10px;
		font-weight: bold;
	}
	table .input {
		width: 90% !important; 
		font-weight: bold;
		padding: 5px;
	}
</style>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Gerar acordo</h5>
            </div>
            <form id="formAcordo" action="<?php echo base_url() . 'divida/dividirCobranca'; ?>" method="post">
            	<input type="hidden" name="cob_cod" value="<?php echo $cobranca->cob_cod; ?>" />
            	
                <div class="form">
                	<h3><b>Valor Original: </b><?php echo convMoney($acordo->aco_valor_original); ?> | <b>Valor Atualizado: </b><?php echo convMoney($acordo->aco_valor_atualizado); ?></h3>
                	<h4 class="vermelho">ATEN��O:<br>Ap�s dividir uma cobran�a, se precisar devolver ao Administrativo, ser�o devolvidas todos os acordos gerados dessa divis�o.</h4>
                    <div class="fields">
                        <div style="margin-top: 0px; margin-bottom: 13px;" class="blocoTitulo">
                            Dividir Cobran�a
                        </div>
                        <div class="field  field-first">
                            <div class="divleft" style="height:29px; width: 420px;">
                                <div class="label">
                                    <label>N�mero de Cobran�as a gerar:</label>
                                </div>
                                <input style="float: left; width: 45px; padding: 5px; text-align: right; font-weight: bold" type="text" id="num_cobrancas" name="num_cobrancas" value="1" />
                                <a href="#" style="float: left; margin-top: 3px; margin-left: 10px;" id="btGerarDividas"><img src="<?php echo $img . 'atu_verde.jpg' ?>" alt="atualizar"/></a>
                            </div>
                        </div>                        
                        <div style="margin-top: 12px; margin-bottom: 2px;" class="blocoTitulo">
                            Novas Cobran�as
                        </div>
                        <div class="field">
                        	<table class="inputTbl">
                        		<thead>
	                        		<tr>
											<th>Valor Original (R$)</th>                        			
	                        				<th>Valor Atualizado (R$)</th>
	                        				<th>SubSetor:</th>
	                        		</tr>
                        		</thead>
                        		<tbody>
	                        		<tr class="linhaBaseClone">
											<td>
	                                    		<input class="input priceF" type="text" id="valor_original_0" name="aco_valor_original[]" value="<?php echo $acordo->aco_valor_original; ?>" />											
											</td>                        			
	                        				<td>
	                        					<input class="input priceF" type="text" id="valor_atualizado_0" name="aco_valor_atualizado[]" value="<?php echo $acordo->aco_valor_atualizado; ?>" />
	                        				</td>
	                        				<td>
	                        					<select class="chosen" name="cob_subsetor[]">
	                        						<option value="AJU">Ajuizado</option>
	                        						<option value="NAJ">N�o Ajuizado</option>
	                        					</select>
	                        				</td>
	                        		</tr>
                        		</tbody>
                        		<tfoot>
	                        		<tr>
											<td>= <span id="total_valor_original" class="verde"><?php echo $acordo->aco_valor_original; ?></span></td>                        			
	                        				<td>= <span id="total_valor_atualizado" class="verde"><?php echo $acordo->aco_valor_atualizado; ?></span></td>
	                        				<td></td>
	                        		</tr>
                        		</tfoot>
                        	</table>
                        	<b style="padding: 5px; display: block; font-size: 10px;" class="vermelho">ATEN��O: O Total da soma de cada coluna deve corresponder ao valor original.</b>
                        </div>
						<div style="text-align: center; margin-top: 10px;" class="buttons">
                            <div class="highlight">
                                <input type="submit" name="submit" value="DIVIDIR COBRAN�AS" id="btDividirCobranca">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

