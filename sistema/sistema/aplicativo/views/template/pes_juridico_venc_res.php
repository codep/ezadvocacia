<script type="text/javascript">
    $(document).ready(function(){

        $('.editaInad').click(function(){
                cod = $(this).attr('id');
                window.location.href="<?php echo base_url().'divida/ficha/cod:';?>"+cod;
        });
        
		$('input#search').quicksearch('#products tbody tr:not(.titulo)', {});
/*
		//AUTO FOCUS
		window.setTimeout(function(){
			$('input#search').focus();	
		},300);
*/	        
        
    });
</script>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Pesquisar Cobranšas por Vencimento</h5>
            </div>
            <?php if (!empty($credor)) : ?>
            <h3 style="text-align: center;"><?php echo $credor->cre_nome_fantasia; ?></h3>
            <?php endif; ?>
            <div class="blocoTitulo">
                <span style="font-size: 10px">Filtro: <input type="text" id="search" style="font-size: 18px; padding: 5px; width: 500px;" /></span>
            </div>
            <div class="table">
                <table id="products">
                    <thead>
                        <tr>
                        	<?php if (empty($credor)) : ?>
                        	<th>Credor</th>
                        	<?php endif; ?>
                            <th>Inadimplente</th>
                            <th>Vencimento Antigo</th>
                            <th>Valor Acordo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>                        
                    	<?php foreach ($cobrancas as $cobranca): ?>
                    		<?php //var_dump($cobranca); die() ?>
                        <tr>
                        	<?php if (empty($credor)) : ?>
                        	<td> <?php echo utf8_decode($cobranca->cre_nome_fantasia); ?> </td>
                        	<?php endif; ?>
                            <td> <?php echo utf8_decode($cobranca->ina_nome); ?> </td>
                            <td> <?php echo convDataBanco($cobranca->paa_vencimento); ?> </td>
                            <td> <?php echo convMoney($cobranca->aco_valor_atualizado); ?> </td>
                            <td class="last">
                                <div class="buttons">
                                    <input style="width: 50px;" class="visualizar editaInad" id="<?php echo $cobranca->inadimplentes_ina_cod; ?>" type="button" value="Ficha" />
                                </div>
                            </td>
                        </tr>
                        <?php endforeach;?>                
                    </tbody>
                </table>
                <div class="pagination pagination-left">
                    <div class="results">
                        <span>Total de <?php echo count($cobrancas); ?> registros encontrados</span>
                    </div>
                </div>
                <br/>
                <br/>
            </div>
<!--            <form id="form" action="">-->
                <div class="form">
                    <div class="fields">
                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <a href="<?php echo base_url().'pesquisar/juridico' ?>" style="text-decoration: none;" >
                                <div style="margin-left: 7px;" class="highlight">
                                    <input type="submit" name="submit.highlight" value="Nova pesquisa" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
<!--            </form>-->
        </div>
    </div>
</div>
