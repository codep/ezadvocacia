<script type="text/javascript">
    $(document).ready(function(){
        $('.visualizar').click(function(e){
            window.location.href='http://webhost/cobranca/divida/detalhes';
        });
        $('input[value="Ficha"]').click(function(e){
            window.location.href='http://webhost/cobranca/divida/ficha';
        });
        if(<?php echo isset($aba) ?>){
            $('a[href="<?php echo $aba ?>"]').trigger('click');
        }

    });
</script>
<html>
    <body>
        <div id="content">

            <?php echo $sidebar; ?>

            <div id="right">
                <div id="box-tabs" class="box" style="min-height: 798px;">
                    <div class="title">
                        <h5>Acordos extintos</h5>
                        <ul class="links">
                            <li><a href="#pgintegral">Pagamento integral</a></li>
                            <li><a href="#solcredor">Solicitado pelo credor</a></li>
                            <li><a href="#outros">Outros</a></li>
                        </ul>
                    </div>
                    <div id="pgintegral">
                        <div class="form">
                            <div class="fields">
                                <form id="form" method="post" action="<?php echo base_url() . 'judicial/acordoext'; ?>">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 286px;">
                                            <div style="width: 69px; padding-left: 1px;" class="label">
                                                <label for="nome">Filtro:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 188px;" type="text" name="filtro"/>
                                                <input type="hidden" name="aba" value="#pgintegral" />
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 30px;">Cod.</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Valor</th>
                                                <th>Extin��o</th>
                                                <th class="last">Visualizar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($acordosPagInt as $ac): ?>
                                                <tr>
                                                    <td class="title" style="width: 30px;"><?php echo $ac->cob_cod; ?></td>
                                                    <td><?php echo $ac->ina_nome; ?></td>
                                                    <td><?php echo $ac->cre_nome_fantasia; ?></td>
                                                    <td>R$ <?php echo $ac->aco_total_parcelado; ?></td>
                                                    <td>
                                                        <?php echo convData($ac->aco_ext_data, 'd'); ?>
                                                    </td>
                                                    <td class="last">
                                                        <div class="buttons">
                                                            <a href="<?php echo base_url() . 'divida/ficha/cod:' . $ac->ina_cod; ?>" style="text-decoration: none;">
                                                                Ficha
                                                            </a>
                                                            <a href="<?php echo base_url() . 'divida/detalhesCobranca/cod:' . $ac->cob_cod; ?>" style="text-decoration: none;">
                                                                Detalhes
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="solcredor">
                        <div class="form">
                            <div class="fields">
                                <form id="form" method="post" action="<?php echo base_url() . 'judicial/acordoext'; ?>">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 286px;">
                                            <div style="width: 69px; padding-left: 1px;" class="label">
                                                <label for="nome">Filtro:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 188px;" type="text" name="filtro"/>
                                                <input type="hidden" name="aba" value="#solcredor" />
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 30px;">Cod.</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Valor</th>
                                                <th>Extin��o</th>
                                                <th class="last">Visualizar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($acordosSolCred as $ac): ?>
                                                <tr>
                                                    <td class="title" style="width: 30px;"><?php echo $ac->cob_cod; ?></td>
                                                    <td><?php echo $ac->ina_nome; ?></td>
                                                    <td><?php echo $ac->cre_nome_fantasia; ?></td>
                                                    <td>R$ <?php echo $ac->aco_total_parcelado; ?></td>
                                                    <td>
                                                        <?php echo convData($ac->aco_ext_data, 'd'); ?>
                                                    </td>
                                                    <td class="last">
                                                        <div class="buttons">
                                                            <a href="<?php echo base_url() . 'divida/ficha/cod:' . $ac->ina_cod; ?>" style="text-decoration: none;">
                                                                Ficha
                                                            </a>
                                                            <a href="<?php echo base_url() . 'divida/detalhesCobranca/cod:' . $ac->cob_cod; ?>" style="text-decoration: none;">
                                                                Detalhes
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="outros">
                        <div class="form">
                            <div class="fields">
                                <form id="form" method="post" action="<?php echo base_url() . 'judicial/acordoext'; ?>">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 286px;">
                                            <div style="width: 69px; padding-left: 1px;" class="label">
                                                <label for="nome">Filtro:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 188px;" type="text" name="filtro"/>
                                                <input type="hidden" name="aba" value="#solcredor" />
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 30px;">Cod.</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Valor</th>
                                                <th>Extin��o</th>
                                                <th class="last">Visualizar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($acordosOutros as $ac): ?>
                                                <tr>
                                                    <td class="title" style="width: 30px;"><?php echo $ac->cob_cod; ?></td>
                                                    <td><?php echo $ac->ina_nome; ?></td>
                                                    <td><?php echo $ac->cre_nome_fantasia; ?></td>
                                                    <td>R$ <?php echo $ac->aco_total_parcelado; ?></td>
                                                    <td>
                                                        <?php echo convData($ac->aco_ext_data, 'd'); ?>
                                                    </td>
                                                    <td class="last">
                                                        <div class="buttons">
                                                            <a href="<?php echo base_url().'divida/ficha/cod:'.$ac->ina_cod;?>" style="text-decoration: none;">
                                                                Ficha
                                                            </a>
                                                            <a href="<?php echo base_url().'divida/detalhesCobranca/cod:'.$ac->cob_cod;?>" style="text-decoration: none;">
                                                                Detalhes
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>