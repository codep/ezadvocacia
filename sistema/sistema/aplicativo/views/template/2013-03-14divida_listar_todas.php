<script type="text/javascript">
        $(document).ready(function(){
            $('.dataMascara').mask('99/99/9999');
            $('.visualizar').click(function(e){
                    window.location.href='http://www.wbusca.com.br/cobranca/prototipo/divida/detalhes';
            });
            $('input[value="Ficha"]').click(function(e){
                    window.location.href='http://www.wbusca.com.br/cobranca/prototipo/divida/ficha';
            });

            $('#campoBusca2').css('display','none');

            //SE A P�GINA FOR RECARREGADA COM O FILTRO "EMISS�O" SELECIONADO
            //ADICIONA O VALOR DA DATA DE EMISS�O QUE SE APRESENTA NO TD DA TABELA
            if($('#filtro01').val() == 'div_emissao')
            {
                $('#campoBusca1').css('display','none');
                $('#campoBusca2').css('display','block');
                $('#nome2').val($('#tdEmissao').html());
            }
            else
            {
                $('#campoBusca2').css('display','none');
                $('#campoBusca1').css('display','block');
            }

            $('select[id="filtro01"]').change(function(){
                if($(this).val() == 'div_emissao')
                {
                    $('#campoBusca1').css('display','none');
                    $('#campoBusca2').css('display','block');

                }
                else
                {
                    $('#campoBusca2').css('display','none');
                    $('#campoBusca1').css('display','block');
                }
            });

            //ZERAR VALORES INCOERENTES CONFORME FILTRO SELECIONADO
            $('#formFiltro').submit(function(){
                if($('#filtro01').val() == 'div_emissao')
                {
                    $('#campoBusca1').css('display','none').html('');
                }
                else
                    $('#campoBusca2').css('display','none').html('');
            });

        });
</script>
<html>
    <body>
        <div id="content">

        <?php echo $sidebar; ?>
            <div id="right">
                <div id="box-tabs" class="box" style="min-height: 798px;">
                    <div class="title">
                        <h5>Listar d�vidas</h5>
                    </div><?php echo $mensagem?>
                    <div id="virgem">
                        
                        <div class="form">
                            <div class="fields">
                                <form id="formFiltro" method="post"  action="listar">
                                    <div class="field  field-first">

                                        <div class="divleftlast" style="width: 45px; margin-left: 0px;">
                                            <div style="width: 69px; padding-left: 1px;" class="label">
                                                <label for="nome">Filtro:</label>
                                            </div>
                                        </div>
                                        <div class="divleft" style="width: 215px;">
                                            <div style="margin-left: 6px;" class="select">
                                                <select style="width: 204px;" id="filtro01" name="filtro1">
                                                    <option value="" style="font-weight:bold"<?php echo $aux == ''  ? 'selected' : ''; ?>>[TODAS D�VIDAS]</option>
                                                    <option value="c.cob_cod" <?php echo $aux == 'div_cod' ? 'selected' : ''; ?>>C�digo</option>
                                                    <option value="ina_nome" <?php echo $aux == 'ina_nome' ? 'selected' : ''; ?>>Inadimplente</option>
                                                    <option value="div_emissao" <?php echo $aux == 'div_emissao' ? 'selected' : ''; ?>>Emiss�o</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="divleft" style="width: 213px;">
                                            <div class="input" id="campoBusca1">
                                                <input style="width: 188px;" type="text" id="nome1" name="nome"/>
                                            </div>
                                            <div class="input" id="campoBusca2">
                                                <input style="width: 188px;" type="text" id="nome2" name="nome" class="dataMascara"/>
                                            </div>
                                        </div>

                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="filtrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <?php if(sizeof($dividasEAcordos['dividas']) > 0 || sizeof($dividasEAcordos['acordos']) > 0):?><!-- if dividas ou acordos -->
                                <div class="table" style="padding: 0px 5px 10px; border-bottom: 1px solid #ddd;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 30px;">Cod.</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Emiss�o</th>
                                                <th>Cadastro</th>
                                                <th>Valor(R$)</th>
                                                <th class="last">Excluir</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(sizeof($dividasEAcordos['dividas']) > 0):?> <!-- ##### if dividas ##### -->
                                            <tr>
                                                <td class="last" colspan="7">
                                                    <div class="blocoTitulo" style="margin-top: 11px; width: 682px;">D�VIDAS</div>
                                                </td>                                                
                                            </tr>
                                            <?php foreach ($dividasEAcordos['dividas'] as $divida):?>
                                            <tr>
                                                <td class="title" style="width: 30px;"><?php echo $divida->cob_cod;?></td>
                                                <td><?php echo utf8_decode($divida->ina_nome);?></td>
                                                <td><?php echo utf8_decode($divida->cre_nome_fantasia);?></td>
                                                <td id="tdEmissao"><?php echo convDataBanco($divida->div_emissao);?></td>
                                                <td><?php echo convDataBanco($divida->div_cadastro);?></td>
                                                <td><?php echo $divida->div_total;?></td>
                                                <td class="last">
                                                    <a href="<?php echo 'excluir/codCobranca:' .$divida->cob_cod;?>">
                                                        <img src="<?php echo $img.'excluir.png'?>" alt="+"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                            <tr>
                                                <td class="last" colspan="7">
                                                    <div class="field  field-first" style="padding-bottom: 1px;">
                                                    <div class="divleftlast" style="width: 395px;">
                                                        <div style="padding-left: 0px;" class="label">
                                                            <label for="nome">Total de d�vidas encontradas: <i><?php echo sizeof($dividasEAcordos['dividas']);?></i></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                </td>                                                
                                            </tr>
                                            <?php endif;?><!-- ##### endif dividas ##### -->
                                            
                                            <!--  ACORDOS ACORDOS ACORDOS ACORDOS ACORDOS  -->
                                            <?php if(sizeof($dividasEAcordos['acordos']) > 0):?> <!-- +++++ if acordos +++++ -->
                                            <tr>
                                                <td class="last" colspan="7">
                                                    <div class="blocoTitulo" style="margin-top: 11px; width: 682px;">ACORDOS</div>
                                                </td>                                                
                                            </tr>
                                            <?php foreach ($dividasEAcordos['acordos'] as $acordo):?>
                                            <tr>
                                                <td class="title" style="width: 30px;"><?php echo $acordo->cob_cod;?></td>
                                                <td><?php echo utf8_decode($acordo->ina_nome);?></td>
                                                <td><?php echo utf8_decode($acordo->cre_nome_fantasia);?></td>
                                                <td id="tdEmissao"><?php echo convDataBanco($acordo->aco_emissao);?></td>
                                                <td><!-- < ?php echo convDataBanco($acordo->div_cadastro);?> --></td>
                                                <td><?php echo $acordo->aco_total_parcelado;?></td>
                                                <td class="last">
                                                    <a href="<?php echo 'excluir/codCobranca:' .$acordo->cob_cod;?>">
                                                        <img src="<?php echo $img.'excluir.png'?>" alt="+"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                            <tr>
                                                <td class="last" colspan="7">
                                                    <div class="field  field-first" style="padding-bottom: 1px;">
                                                    <div class="divleftlast" style="width: 395px;">
                                                        <div style="padding-left: 0px;" class="label">
                                                            <label for="nome">Total de acordos encontradas: <i><?php echo sizeof($dividasEAcordos['acordos']);?></i></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                </td>                                                
                                            </tr>
                                            <?php endif;?><!-- +++++ endif acordos +++++ -->
                                            <!-- FIM ACORDOS FIM ACORDOS FIM ACORDOS FIM ACORDOS FIM ACORDOS FIM ACORDOS FIM ACORDOS -->
                                        </tbody>
                                    </table>
                                </div>
                                <?php else:?><!-- Se nao encontrar nenhuma divida e nenhum acordo -->
                                <div class="blocoTitulo" style="margin-top: 11px; width: 682px;">NADA ENCONTRADO</div>
                                <?php endif;?><!-- endif dividas ou acordos -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>