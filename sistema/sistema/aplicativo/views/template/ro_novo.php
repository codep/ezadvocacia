<script type="text/javascript">
    $(document).ready(function(){
        $('.fechar').click(function(e){
            e.preventDefault();
            $('#dialog-form').dialog('close');
        });
        
        $(".dialog-form-open").click(function () {
            $("#dialog-form").dialog("open");
            return false;
        });
    });
</script>
<!-- Caixa de di�logo RO-->
<div id="dialog-form" class="form" style="padding-left: 5px; padding-right: 5px;" title="Novo registro">
    <form id="form" action="<?php echo base_url() . 'ro/incluir' ?>" method="post">
        <div style="width: 400px; margin: 0 auto; margin-top: 20px;">
            <select style="width: 400px;" id="operacao" name="operacao">
                <?php foreach ($operacoes as $ope): ?>
                    <option value="<?php echo $ope->ope_cod; ?>"><?php echo utf8_decode($ope->ope_nome); ?></option>
                <?php endforeach; ?>
            </select>
            <input type="hidden" value="<?php echo $cobrancas ?>" name="cobrancas"/>
            <input type="hidden" value="<?php echo $this->session->userdata('usucod'); ?>" name="usuario"/>
            <input type="hidden" value="<?php echo date("Y-m-d"); ?>" name="data"/>
            <input type="hidden" value="<?php echo date("H:i:s"); ?>" name="hora"/>
            <input type="hidden" value="<?php echo $inadimplente->ina_cod ?>" name="inadCod"/>

        </div>
        <div class="fields" style="width: 400px; margin: 0 auto; margin-top: 15px;">
            <div class="field" style="width: 400px;">
                <div style="width: 84px; padding-top: 0px;" class="label">
                    <label for="obs">Observa��o:</label>
                </div>
                <div class="textarea">
                    <textarea style="width: 386px; height: 95px;" id="obs" name="obs" cols="50" rows="10 " class="editor"></textarea>
                </div>
                <div style="margin-top: 15px; padding-top: 7px;  border-top: 1px solid #ddd;">
                    <p><i>Para agendar uma nova tarefa preencha os campos abaixo</i></p>
                    <div style="float: left;margin-right: 30px;">
                        <div style="width: 75px; padding-left: 1px;" class="label">
                            <label for="data_agendamento">Agendar:</label>
                        </div>
                        <div style=" width: 69px;" class="input">
                            <input type="text" id="data_agendamento" name="data_agendamento" class="date" />
                        </div>
                    </div>
                    <div style="float: left;">
                        <div style="width: 95px; padding-left: 1px;" class="label">
                            <label for="tarefa">Tarefa:</label>
                        </div>
                        <div style=" width: 269px;" class="input">
                            <input type="text" id="tarefa" name="tarefa" style="width: 229px;" />
                        </div>
                    </div>
                </div>
            </div>
            <div style="text-align: center; margin-top: 10px; margin-bottom: 10px;" class="buttons">
                <input type="reset" class="fechar" name="cancelar" value="Cancelar" />
                <div style="margin-left: 7px;" class="highlight">
                    <input type="submit" name="submit.highlight" value="Cadastrar" />
                </div>
            </div>
        </div>
    </form>
</div>
<!-- FIM Caixa de di�logo RO-->
<div id="content">

    <?php echo $sidebar; ?>

    <div id="right">
        <div id="box" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Registro de opera��es</h5>
                <div class="form" style="clear: none; float: right; padding-top:5px; height: 27px;">
                    <div class="fields">
                        <div class="field" style="border: none; margin: 0px; padding: 0px;">
                            <div class="highlight">
                                <a href="<?php echo base_url() . 'divida/ficha/cod:' . $inadimplente->ina_cod; ?>">
                                    <input style="width: 55px; font-size: 13px; font-weight: bold;" type="submit" name="cadastrar" value="Ficha" />
                                </a>
                            </div>
                            <div class="highlight">
                                <input style="width: 155px; font-size: 13px; font-weight: bold;" class="dialog-form-open" type="submit" name="cadastrar" value="Novo registro" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="virgem">
                <?php echo $mensagem ?>
	            <?php if ($inadimplente->ina_negativado == 2): ?>
	                <div id="box-messages">
	                    <div class="messages">
	                        <div id="message-warning" class="message message-notice">
	                            <div class="image">
	                                <img src="<?php echo $img . 'icons/warning.png' ?>" alt="Warning" height="32" />
	                            </div>
	                            <div class="text">
	                                <h6>Inadimplente REABILITADO</h6>
	                            </div>
	                            <div class="dismiss">
	                                <a href="#message-warning"></a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            <?php endif; ?>
	            <?php if ($inadimplente->ina_negativado == 1): ?>
	                <div id="box-messages">
	                    <div class="messages">
	                        <div id="message-error" class="message message-error">
	                            <div class="image">
	                                <img src="<?php echo $img . 'icons/error.png' ?>" alt="Error" height="32" />
	                            </div>
	                            <div class="text">
	                                <h6>Inadimplente NEGATIVADO</h6>
	                            </div>
	                            <div class="dismiss">
	                                <a href="#message-error"></a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            <?php endif; ?>
                <div class="form">
                    <div class="fields">
                        <div class="field  field-first" style="border: none;">
                            <div class="divleftlast" style="width: 616px;">
                                <div class="label">
                                    <label>Credor:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $credor->cre_nome_fantasia; ?> <br/> <i>Fone:</i> <?php echo $credor->cre_fone1; ?> | <i>Celular:</i> <?php echo $credor->cre_fone2; ?></p>
                            </div>
                        </div>
                        <div class="field  field-first">
                            <div class="divleftlast" style="width: 466px;">
                                <div class="label">
                                    <label>Inadimplente:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inadimplente->ina_nome; ?></p>
                            </div>
                        </div>
                        <div class="table" style="padding: 0px;">
                        	<style>
                        		#products tr.line1 td {border-top: 0.2em solid #000;}
                        		#products tr.line1 td.left,
                        		#products tr.line2 td {border-left: 0.2em solid #000 !important; }
                        		#products tr.line1 td.right,
                        		#products tr.line2 td {border-right: 0.2em solid #000 !important;}
                        		.tdHover { 
                        			background-color: #ddd !important; 
                    			}
                        	</style>
                        	<script>
                        		$(document).ready(function(){
                        			$('#products tr').hover(function(e){
                        				if ($(e.currentTarget).hasClass('line1')) {
                        					$(e.currentTarget).next('tr.line2').find('td').addClass('tdHover'); 
                        				} else {
                        					$(e.currentTarget).prev('tr.line1').find('td').addClass('tdHover');
                        				}
                        				$(e.currentTarget).find('td').addClass('tdHover');
                        			},function(e){
                        				$('tr.line1').find('td').removeClass('tdHover');
                        				$('tr.line2').find('td').removeClass('tdHover');
                        			});
                        		});
                        	</script>
                            <table id="products" style="padding: 0px;">
                                <thead>
                                    <tr>
                                        <th style=" text-align: center; width: 60px;">Data</th>
                                        <th>Hora</th>
                                        <th>Recuperador</th>
                                        <th>Opera��o</th>
                                        <th>Autom�tico</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($ros as $ro): ?>
                                        <tr class="line1">
                                           	<td class="left"><?php echo convData($ro->ros_data, 'd'); ?></td>
                                            <td><?php echo $ro->ros_hora; ?></td>
                                            <td><?php echo $ro->usu_nome; ?></td>
                                            <td><?php echo utf8_decode($ro->ope_nome); ?></td>
                                            <td class="right"><?php echo $ro->ope_automatica == '0' ? '---' : 'SIM'; ?></td>
                                        </tr>
                                        <tr class="line2">
                                            <td colspan="5">
                                            	<div style="padding: 3px; overflow: auto; line-height: 20px;"><?php echo utf8_decode($ro->ros_detalhe); ?></div>
                                        	</td>
                                        </tr>
                                    <?php endforeach; ?>
                        <!--        ----------- ROS ANTIGOS ------------->
                                    <?php if (isset($rosAntigos)): ?>
                                        <?php foreach ($rosAntigos as $ro): ?>
                                            <tr>
                                                <td class="title"><?php echo convData($ro->ros_data, 'd'); ?></td>
                                                <td><?php echo $ro->ros_hora; ?></td>
                                                <td><?php echo $ro->usu_nome; ?></td>
                                                <td><?php echo utf8_decode($ro->ope_nome); ?></td>
                                                <td style="width: 230px;">
                                                	<div style="width: 230px; padding: 3px; overflow: auto;"><?php echo utf8_decode($ro->ros_detalhe); ?></div>
                                            	</td>
                                                <td class="last"><?php echo $ro->ope_automatica == '0' ? '---' : 'SIM'; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>