<div class="conteudo">
    <div class="recibo">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img . 'logo_elisangela.jpg'; ?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br|www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="num_recibo">[<?php echo substr($recDados->procedencia,0,1); ?>] RECIBO N� <?php echo $recDados->rec_cod; ?><?php echo isset($segundavia) ? ' <i>(2� Via)</i> ' : '' ?></p>
            <p class="valor"><?php echo convMoney($recDados->rec_valor); ?></p>
        </div>
        <div class="linha3">
            <p class="termo">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Recebemos de <b><?php echo $recDados->ina_nome; ?></b>, portador(a) do CPF
                <?php echo $recDados->ina_cpf_cnpj; ?>, a import�ncia de <b><?php echo convMoney($recDados->rec_valor); ?></b><?php echo ' (' . extenso($recDados->rec_valor, "real", "reais", "centavo", "centavos") . ')' ?>, referente � quita��o dos documentos abaixo
                relacionados, emitidos em favor de <b><?php echo $recDados->cre_nome_fantasia; ?></b>,
                pelos quais damos plena quita��o da quantia ora recebida de
                acordo com sua forma de pagamento.
            </p>
            <p class="obs">
                <?php if (isset($parcelasInfo)): ?>
                    <b>OBSERVA��ES</b>: Pagamento da(s) parcela(s) <?php foreach ($parcelasInfo as $parcela): ?><?php echo ' ' . $parcela->paa_parcela . '(R$ ' . convMoney(($parcela->paa_valor - $parcela->paa_saldo)) . ')  ' ?><?php endforeach; ?>.<br/>
                <?php endif; ?>
                <?php echo $recDados->reb_tipo == '2' ? 'ESTE RECIBO TER� VALIDADE AP�S O CHEQUE SER COMPENSADO' : ''; ?>
            </p>
            <p style="font-size: 8px; margin: 0px"><?php echo $recDados->reb_tipo == '2' ? $recDados->rec_obs : '' ?></p>
        </div>
        <div class="linha4">
            <p class="baixado" style="font-size: 10px">Baixado por: <i><?php echo $recDados->baixador; ?></i></p>
            <p class="data">Jos� Bonif�cio, <?php echo dataex($recDados->rec_data) ?></p>
        </div>
        <div class="linha5">
            <p class="ass" style="margin-right: 15px;"><?php echo $recDados->ina_nome; ?></p>
            <p class="ass">Recebedor</p>
        </div>
    </div>

    <div class="recibo" style="margin-bottom: 0px;">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img . 'logo_elisangela.jpg'; ?>" width="133" height="91" alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - Telefone: 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    cobranca@ezcobrancas.com.br|www.ezcobrancas.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="num_recibo">[<?php echo substr($recDados->procedencia,0,1); ?>] RECIBO N� <?php echo $recDados->rec_cod; ?><?php echo isset($segundavia) ? ' <i>(2� Via)</i> ' : '' ?></p>
            <p class="valor"><?php echo convMoney($recDados->rec_valor); ?></p>
        </div>
        <div class="linha3">
            <p class="termo">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Recebemos de <b><?php echo $recDados->ina_nome; ?></b>, portador(a) do CPF
                <?php echo $recDados->ina_cpf_cnpj; ?>, a import�ncia de <b><?php echo convMoney($recDados->rec_valor); ?></b><?php echo ' (' . extenso($recDados->rec_valor, "real", "reais", "centavo", "centavos") . ')' ?>, referente � quita��o dos documentos abaixo
                relacionados, emitidos em favor de <b><?php echo $recDados->cre_nome_fantasia; ?></b>,
                pelos quais damos plena quita��o da quantia ora recebida de
                acordo com sua forma de pagamento.
            </p>
            <p class="obs">
                <?php if (isset($parcelasInfo)): ?>
                    <b>OBSERVA��ES</b>: Pagamento da(s) parcela(s) <?php foreach ($parcelasInfo as $parcela): ?><?php echo ' ' . $parcela->paa_parcela . '(R$ ' . convMoney(($parcela->paa_valor - $parcela->paa_saldo)) . ')  ' ?><?php endforeach; ?>.<br/>
                <?php endif; ?>
                <?php echo $recDados->reb_tipo == '2' ? 'ESTE RECIBO TER� VALIDADE AP�S O CHEQUE SER COMPENSADO' : ''; ?>
            </p>
            <p style="font-size: 8px; margin: 0px"><?php echo $recDados->reb_tipo == '2' ? $recDados->rec_obs : '' ?></p>
        </div>
        <div class="linha4">
            <p class="baixado" style="font-size: 10px">Baixado por: <i><?php echo $recDados->baixador; ?></i></p>
            <p class="data">Jos� Bonif�cio, <?php echo dataex(date('Y-m-d')) ?></p>
        </div>
        <div class="linha5">
            <p class="ass" style="margin-right: 15px;"><?php echo $recDados->ina_nome; ?></p>
            <p class="ass">Recebedor</p>
        </div>
    </div>
</div>