<script type="text/javascript">
	$(document).ready(function(){
		$('.btProcessoLancar').click(function(){
			//if (confirm("Confirma o lan�amento desse andamento judicial?")==false) return;
			
			var cob_cod = $(this).attr('data-cob_cod');
			var texto_processo = $(this).attr('data-texto_processo');
			var $bt = $(this);
			if (cob_cod!='' && texto_processo!='') {
			
	            $.ajax({
	                type: "POST",
	                url: "<?php echo base_url() . 'judicial/lancarprocesso'; ?>",
	                data: 'cob_cod='+cob_cod+'&texto='+texto_processo,
	                dataType: 'json',
	                error: function(xhr, status, er) {
	                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
	                },
	                success: function(retorno)
	                {
	                	//console.log(retorno);
	                	if (retorno.status==true || retorno.status==1) {
							$bt.parents('tr:first').next('tr.line2').remove();
							$bt.parents('tr:first').remove();
	                	} else {
	                		alert('Ocorreu um erro durante a atualiza��o dos dados, verifique e tente novamente.');
	                	}
	                }
	            });
	             
			}
		});	
		$('.btProcessoLancarTodos').click(function(){
			if (confirm("Confirma o lan�amento de todos os andamentos judiciais encontrados?")==true) {
				if ($('.btProcessoLancar').length<=0) {
					alert('N�o h� mais registros a serem lan�ados!');
				} else {
					$('.btProcessoLancar').each(function(i,o){
						$(o).trigger('click');
					});
				}
			}
		});
		
       $('.formProcessoDesconhecido').submit(function(e){
       		e.preventDefault();
        	var $form = $(this);
        	var validacao = true;
        	var campos = ['input[name=cob_cod]','input[name=processo_num]','input[name=processo_ano]','input[name=processo_forum]'];
        	$.each(campos,function(i,v){
	            if($form.find(v).val() == '')
	            {
	            	alert('Todos os campos s�o obrigat�rios');
	                $form.find(v).focus();
	                return validacao = false;
	            }
        	});
        	if (validacao==true) {
	            $.ajax({
	                type: $form.attr('method'),
	                url: $form.attr('action'),
	                data: $form.serialize(),
	                dataType: 'json',
	                error: function(xhr, status, er) {
	                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
	                },
	                success: function(retorno)
	                {
	                	//console.log(retorno);
	                	if (retorno.status==true || retorno.status==1) {
							$form.parents('tr:first').prev('tr.line1').remove();
							$form.parents('tr:first').remove();
	                	} else {
	                		alert('Ocorreu um erro durante a atualiza��o dos dados, verifique e tente novamente.');
	                	}
	                }
	            });        		
        	}
			return false;
        });
		
	});
</script>
<style type="text/css">
    a{
        color: black;
    }
    .tabela{
        width: 702px !important;
        margin: 7px 0 7px 20px !important;
    }
    .tabela td{
        padding: 2px !important;
        text-align: center !important;
    }
    #content div.box table th{
    	padding: 5px;
    	vertical-align: middle;
	}
	.vertCenter{
	    vertical-align: middle;
	    text-align: center;
	}
	.okRed{
	    color: red;
	}
	.okRed:hover{
	    color: red !important;
	}

	.legendaBox {
		display: block; width: 10px; height: 8px; float:left;
	}
	.legendaLabel {
		display: block; float:left; width: 200px; margin-left: 5px;
	}
	.bgErro {
		/*background-color: #d9534f !important;
		color: #FFF;
		*/
		border: 2px solid #d9534f !important;
	}
	.bgSucesso {
		border: 2px solid #47a447 !important;            		
	}
	tr.bgDuplicado td {
		background-color: #ed9c28 !important;
		color: #FFF !important;
	}
	i.bgDuplicado {
		border: 2px solid #ed9c28 !important;
	}

</style>
<style>
	.borderTable tr.line1 td {border-top: 0.2em solid #000;}
	.borderTable tr.line1 td.left,
	.borderTable tr.line2 td {border-left: 0.2em solid #000 !important; }
	.borderTable tr.line1 td.right,
	.borderTable tr.line2 td {border-right: 0.2em solid #000 !important;}
	.tdHover { 
		background-color: #ddd !important; 
	}
</style>
<script>
	$(document).ready(function(){
		$('.borderTable tr').hover(function(e){
			if ($(e.currentTarget).hasClass('line1')) {
				$(e.currentTarget).next('tr.line2').find('td').addClass('tdHover'); 
			} else {
				$(e.currentTarget).prev('tr.line1').find('td').addClass('tdHover');
			}
			$(e.currentTarget).find('td').addClass('tdHover');
		},function(e){
			$('tr.line1').find('td').removeClass('tdHover');
			$('tr.line2').find('td').removeClass('tdHover');
		});
	});
</script>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Importa��o de Processos em Andamento</h5>
            </div>
            <a style="display: block; margin-top: 15px; text-align: center; font-size: 20px;" href="<?php echo base_url().'judicial/importar' ?>">Voltar</a>
            
            <?php if(!empty($mensagem)): ?>
            <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
            	<?php echo $mensagem ?>
            </div>
            <?php endif; ?>
            
            <div style="padding: 10px; margin: 0px; margin-top: 20px; width: 720px; border-bottom: 2px solid #000" class="blocoTitulo">
                    <span style="float:left; font-size: 15px; margin-top: 10px;">Registros encontrados: <?php echo is_array($registros)? count($registros) : 0 ?></span>
                    <?php if (is_array($registros) && count($registros)>0) : ?>
                    <input class="btProcessoLancarTodos" style="width: 100px; height: 28px; margin-top: 5px; float: right;" type="button" value="Lan�ar Todos" />
                    <?php endif; ?>
                    <div style="clear: both"></div>
            </div>
            
			<?php if (is_array($registros) && count($registros)>0) : ?>
            <div class="table" style="padding: 0px; border-bottom: 2px solid #000;">
	            <table class="borderTable" style="padding: 0px;">
	                <thead>
	                    <tr>
	                        <th>Processo</th>
	                        <th>Credor</th>
							<th>Inadimplente</th>
	                        <th>A��o</th>
	                    </tr>
	                </thead>
	                <tbody>
	                    <?php foreach ($registros as $registro): ?>
	                        <tr class="line1">
	                           	<td class="left"><?php echo $registro['processo']; ?></td>
	                            <td><?php echo $registro['credor']; ?></td>
	                            <td><?php echo $registro['inadimplente']; ?></td>
	                            <td class="right">
	                            	<?php $texto = $registro['jornal'].' '.$registro['data'].' '.$registro['origem'].' (Arq/Pub:'.$registro['arqpub'].') '.$registro['intimacao']; ?>
	                				<input class="btProcessoLancar" data-cob_cod="<?php echo $registro['cobranca']; ?>" data-texto_processo="<?php echo $texto; ?>" style="width: 60px; height: 28px; margin-top: 5px; float: right;" type="button" value="Lan�ar" />            	
	                            </td>
	                        </tr>
	                        <tr class="line2">
	                            <td colspan="5">
	                            	<div style="padding: 3px; overflow: auto; line-height: 20px;"><?php echo $texto; ?></div>
	                        	</td>
	                        </tr>
	                    <?php endforeach; ?>
	                </tbody>
	            </table>
            </div>
			<?php endif; ?>

            <div style="padding: 10px; margin: 0px; margin-top: 20px; width: 715px; border: 2px solid #F00" class="blocoTitulo">
                    Registros n�o encontrados: <?php echo is_array($desconhecidos)? count($desconhecidos)-1 : 0 ?>
            </div>
            <?php if (is_array($desconhecidos) && count($desconhecidos)>0) : ?>
            <div class="table" style="padding: 0px; border-bottom: 2px solid #000;">
	            <table class="borderTable" style="padding: 0px;">
	                <thead>
	                    <tr>
	                    	<?php $row = reset($desconhecidos); ?>
	                    	<?php foreach($row as $col) : ?>
	                        <td><?php echo $col; ?></td>
	                        <?php endforeach; ?>
	                    </tr>
	                </thead>
	                <tbody>
	                    <?php foreach ($desconhecidos as $r=>$row): ?>
	                    	<?php if ($r==0) continue; ?>
	                        <tr class="line1">
	                        	<?php $totalCol = count($row); ?>
	                        	<?php foreach($row as $i=>$col) : ?>
	                           	<td class="<?php echo $i==1?'left':($i==$totalCol?'right':''); ?>"><?php echo $col; ?></td>
	                           	<?php endforeach; ?>
	                        </tr>
	                        <tr class="line2">
	                            <td colspan="<?php echo $totalCol; ?>">
								    <form class="formProcessoDesconhecido" action="<?php echo base_url() . 'judicial/processodesconhecido'; ?>" method="post">
	                            		<?php $texto = implode(' - ',$row); ?>
	                            		<input type="hidden" name="texto" value="<?php echo $texto; ?>" />
								        <div class="fields" style="width: 710px; margin: 0 auto;">
								        	<?php //C�DIGO PARA TENTAR ENCONTRAR UM C�DIGO DE PROCESSO AUTOMATICAMENTE
								        	foreach($row as $i=>$col) {
									        	$reg = '/(\d{7}[\-|\.]\d{2})\.(\d{4})\.(\d{1}\.\d{2})\.(\d{4})/';
									        	preg_match($reg, $col, $aProcesso);
												if (!empty($aProcesso)) break;
											}
											if (empty($aProcesso)) $aProcesso=array_fill(0, 5, '');
								        	?>
								            <div class="field" style="padding: 2px; height: 35px;">
								                <div class="divleftlast" style="width: 130px;">
								                    <div style="width: 250px; padding-left: 1px;" class="label">
								                        <label for="devdoc_mot">C�digo Cobran�a:</label>
								                    </div>
								                    <div class="input">
								                        <input style="width: 130px;" type="text" name="cob_cod" />
								                    </div>
								                </div>
								                <div class="divleftlast" style="width: 300px;">
								                    <div style="width: 250px; padding-left: 1px;" class="label">
								                        <label for="devdoc_mot">N�mero Processo Completo:</label>
								                    </div>
								                    <div class="input">
								                        <input style="width: 300px;" type="text" name="processo_num" value="<?php echo $aProcesso[0]; ?>"/>
								                    </div>
								                </div>
								                <div class="divleftlast" style="width: 110px;">
								                    <div style="width: 52px; padding-left: 1px;" class="label">
								                        <label for="renegociacao_mot">Ano:</label>
								                    </div>
								                    <div class="input">
								                        <input style="width: 110px;" type="text" name="processo_ano" value="<?php echo $aProcesso[2]; ?>"/>
								                    </div>
								                </div>
								                <div class="divleftlast" style="width: 110px;">
								                    <div style="width: 52px; padding-left: 1px;" class="label">
								                        <label for="devdoc_mot">F�rum:</label>
								                    </div>
								                    <div class="input">
								                        <input style="width: 110px;" type="text" name="processo_forum" value="<?php echo $aProcesso[4]; ?>"/>
								                    </div>
								                </div>
								                <div class="divleftlast" style="width: 20px;">
								                    <div class="buttons" style="margin-top: 10px;">
								                    	<input type="submit" name="submit.highlight" value="OK"/>
								                    </div>
								                </div>
								            </div>
								        </div>
								    </form>
	                            	
	                            	<div style="padding: 3px; overflow: auto; line-height: 20px;"><?php //echo $texto; ?></div>
	                        	</td>
	                        </tr>
	                    <?php endforeach; ?>
	                </tbody>
	            </table>
            </div>
            <?php endif; ?>
            
        </div>
    </div>
</div>