<html>
    <body>
        <div id="login">
            <div class="title">
                <h5>EZ Advocacia e Cobran�as</h5>
                <div class="corner tl"></div>
                <div class="corner tr"></div>
            </div>
            <div style="text-align: center; background-color: #fff; padding: 10px; width: 400px;">
            	<img src="<?php echo $img.'logoez.jpg'?>" alt="" width="350" />
        	</div>
            <div class="inner">
                <form action="<?php echo base_url().'login';?>" method="post">
                    <div class="form">
                        <div class="fields">
                            <div class="field">
                                <div class="label">
                                    <label for="usuario">Usu�rio:</label>
                                </div>
                                <div class="input">
                                    <input type="text" id="usuario" name="usuario" size="40" class="focus" />
                                </div>
                            </div>
                            <div class="field">
                                <div class="label">
                                    <label for="senha">Senha:</label>
                                </div>
                                <div class="input">
                                    <input type="password" id="senha" name="senha" size="40" class="focus" />
                                </div>
                            </div>
                            <div class="buttons">
                                <input type="submit" value="Entrar" />
                            </div>
                        </div>
                    </div>
                </form>
                <?php echo $mensagem; ?>
            </div>
        </div>
    </body>
</html>