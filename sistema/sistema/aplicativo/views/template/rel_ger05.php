<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img.'logo_elisangela.jpg';?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br|www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo $title ;?></p>
        </div>
    </div>
    <?php foreach ($cobsOciosas as $cobOciosa) : ?>
    <table id="tableGer05" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th id="thInadimplente">Inadimplente</th>
            <th id="thCredor">Credor</th>
            <th id="th3">Data de Cadastro</th>
            <th id="th4" class="last">�ltimo RO</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="tdInaNome"><?php echo utf8_decode($cobOciosa->ina_nome); ?></td>
            <td class="tdCreNomeFantasia"><?php echo utf8_decode($cobOciosa->cre_nome_fantasia); ?></td>
            <td class="td3">
              <?php
               if($cobOciosa->div_cadastro != "")
                   {
                   echo convDataBanco($cobOciosa->div_cadastro);
                   }
                   else if($cobOciosa->aco_emissao != "")
                       {
                       echo convDataBanco($cobOciosa->aco_emissao);
                       }
                       else
                           {
                           //se as duas dastas forem vazias
                           echo "";
                           }
              ?>
            </td>
            <td class="td4 last">
                <?php echo (sizeof($dezUltimosRos[$cobOciosa->cob_cod]) == 0 )? "Virgem" :  utf8_decode($cobOciosa->ult_ros_detalhe); ?>
            </td>
        </tr>
        <?php if (sizeof($dezUltimosRos[$cobOciosa->cob_cod]) != 0 ) :?>
        <tr>
            <td class="td5"><strong>�ltimas Opera��es</strong></td>
            <td class="last" colspan="3">
                <?php foreach ($dezUltimosRos[$cobOciosa->cob_cod] as $ros) : ?>
                <?php echo convDataBanco($ros->ros_data) ." "; echo $ros->ros_hora ." "; echo $ros->ope_nome;?><br/>
                <?php endforeach; ?>
            </td>
        </tr>
        <?php endif;?>
        <?php endforeach; ?>
    </tbody>
</table>
</div>