<div id="content">
        <?php echo $sidebar; ?>
    <div id="right">
        <div class="box" style="min-height: 800px;">
            <div class="title">
                <h5>Pesquisar Cobran�a</h5>
            </div>
            <form id="form" action="<?php echo base_url().'pesquisar/pesCobranca' ?>" method="post">
                <div class="form">
                    <?php echo $mensagem; ?>
                    <div class="fields" style="height: 400px">
                        <div class="input">
                        	<label>Credor:</label>
                            <select style="width: 600px;" name="credorFiltro" id="credorFiltro" class="chosen" data-placeholder="Informe o nome fantasia do credor">
                            	<option value=""></option>
                            	<?php foreach($credores as $credor) : ?>
									<option value="<?php echo $credor->cre_cod; ?>"><?php echo $credor->cre_nome_fantasia; ?></option>
								<?php endforeach?>
                            </select>
						</div>
						
                        <div class="input" style="padding-bottom:10px; margin-top: 10px; border-bottom: 1px solid #aaa">
                        	<label><img src="<?php echo $img.'check_all.gif'?>" rel="0" id="ckAllTipo" class="ckAll" /> Tipo: </label>
                        	<input type="checkbox" value="virgem" name="tipoFiltro[]" id="tipoVirgem" /><label for="tipoVirgem">Virgem</label>
                        	<input type="checkbox" value="andamento" name="tipoFiltro[]" id="tipoAndamento" /><label for="tipoAndamento">Em Andamento</label>
                        	<input type="checkbox" value="atraso" name="tipoFiltro[]" id="tipoAtraso" /><label for="tipoAtraso">Em Atraso</label>
                        	<input type="checkbox" value="acordo" name="tipoFiltro[]" id="tipoAcordo" /><label for="tipoAcordo">Com Acordo</label>
                        	<input type="checkbox" value="estornar" name="tipoFiltro[]" id="tipoEstornar" /><label for="tipoEstornar">Estornar</label>
                        	<input type="checkbox" value="analisar" name="tipoFiltro[]" id="tipoAnalisar" /><label for="tipoAnalisar">Analisar/Ajuizar</label>
						</div>

                        <div style="clear:both"></div>
                        
                        <div class="input" style="padding-bottom:10px; margin-top: 10px; border-bottom: 1px solid #aaa">
                        	<label><img src="<?php echo $img.'check_all.gif'?>" rel="0" id="ckAllSetor" class="ckAll" /> Setor: </label>
                        	<input type="checkbox" value="JUD" name="tipoSetor[]" id="setorJuridico" /><label for="setorJuridico">Judici�rio</label>
                        	<input type="checkbox" value="ADM" name="tipoSetor[]" id="setorAdministrativo" /><label for="setorAdministrativo">Administrativo</label>
						</div>

                        <div style="clear:both"></div>
                        
                        <div class="input" style="padding-bottom:10px; margin-top: 10px; border-bottom: 1px solid #aaa">
                        	<label>Agrupar: </label>
                        	<input type="checkbox" value="INA" name="agrupar" id="agruparInadimplentes" /><label for="agrupar_inadimplentes">Inadimplentes <em>(agrupa os resultados por nome de inadimplente)</em></label>
						</div>

                        <div style="clear:both"></div>
                        
                        <div class="divlast">
                            <div class="buttons">
                                <div class="highlight">
                                    <input style="width: 85px; margin-top: 10px;" type="submit" name="pesquisar1" value="Pesquisar" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
(function ($) {

	var cookieInputStatus = function(obj) {
		$.cookie($(obj).attr('id'),$(obj).val());
	};
	var cookieCheckboxStatus = function(obj) {
		var status = $(obj).is(':checked')?1:0;
		$.cookie($(obj).attr('id'),status);
	};
	var ckAllStatus = function(obj) {
		if (obj == undefined) {
			obj = '.ckAll';			
		}
		$(obj).attr('rel',function() {
			ckObj = $(obj).parents('.input').find(':checkbox');
			ckObjChecked = $(obj).parents('.input').find(':checkbox:checked');
			return (ckObj.length == ckObjChecked.length) ? 1 : 0;
		});
	}
	
	$(':checkbox').change(function() {
		cookieCheckboxStatus(this);
		//console.log($.cookie($(this).attr('id')));
	});
	$(':checkbox').each(function(){
		if ($.cookie($(this).attr('id'))==1) {
			$(this).attr('checked',true);
		} else {
			$(this).removeAttr('checked');
		}
	});
	
	$('#form .input input:not(:checkbox), select').change(function(){
		cookieInputStatus(this);
		//console.log($(this).val());
	});
	$('#form .input input:not(:checkbox), select').each(function(){
		var objVal = $.cookie($(this).attr('id'));
		if (objVal!=undefined) {
			$(this).val(objVal);
		}
		//console.log($(this).val());
	});
	

	$('.ckAll').click(function(){
		if ($(this).attr('rel')=='0') {
			$(this).parents('.input').find(':checkbox').attr('checked',function(){
				this.checked = true;
				cookieCheckboxStatus(this);
			});
		} else {
			$(this).parents('.input').find(':checkbox').attr('checked',function(){
				this.checked = false;
				cookieCheckboxStatus(this);
			});
		}
		ckAllStatus(this);
	});
	ckAllStatus();
	
	$('select.chosen').chosen();
	
})(jQuery);
</script>