<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img.'logo_elisangela.jpg';?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br|www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo $title ;?></p>
        </div>
    </div>
    <table id="tableGer06" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="thInadimplente">Inadimplente</th>
                <th id="thCredor">Credor</th>
                <th id="th3">Refer�ncia</th>
                <th id="th4">Recebido</th>
                <th id="th5">Recuperador</th>
                <th id="th6">Baixador</th>
                <th id="th7" class="last">Valor</th>
            </tr>
        </thead>
        <tbody>
            <?php $total = 0; foreach ($recebimentos as $recebiment) : ?>
            <tr>
                <td class="tdInaNome"><?php echo utf8_decode($recebiment->ina_nome); ?></td>
                <td class="tdCreNomeFantasia"><?php echo utf8_decode($recebiment->cre_nome_fantasia); ?></td>
                <td class="td3">
                    <?php
                    if(!isset($parcelasRecebidas[$recebiment->reb_cod])){
                    /* Se entrar aqui provavelmente a coluna reb_parcelas da tabela recebimentos esta em branco */
                        echo"Parcela indefinida";
                    }else{
                        foreach ($parcelasRecebidas[$recebiment->reb_cod] as $parcRecebida){
                            echo "$parcRecebida->recebimento ";
                        }                    
                    }?>
                </td>
                <td class="td4">
                    <?php echo $recebiment->reb_data;?>
                </td>
                <td class="td5"><?php echo utf8_decode($recebiment->usu_usuario_sis);?></td>
                <td class="td6"><?php echo utf8_decode($recebiment->reb_baixador);?></td>
                <td class="td7 last"><?php echo convMoney($recebiment->reb_valor);?></td>
                <?php $total += $recebiment->reb_valor; endforeach; ?>
            </tr>
            <tr>
                <td id="tdTotalA" colspan="6">TOTAL</td>
                <td id="tdTotalB" class="last"><?php echo convMoney($total); ?></td>
            </tr>
        </tbody>
    </table>
</div>