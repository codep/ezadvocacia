<script type="text/javascript">
        $(document).ready(function(){
            $('.visualizar').click(function(e){
                    window.location.href='http://www.wbusca.com.br/cobranca/prototipo/divida/detalhes';
            });
            $('input[value="Ficha"]').click(function(e){
                    window.location.href='http://www.wbusca.com.br/cobranca/prototipo/divida/ficha';
            });

        });
</script>
<html>
    <body>
        <div id="content">

        <?php echo $sidebar; ?>

            <div id="right">
                <div id="box-tabs" class="box" style="min-height: 798px;">
                    <div class="title">
                        <h5>D�vidas administrativas</h5>
                        <ul class="links">
                            <li><a href="#virgem">Virgem</a></li>
                            <li><a href="#andamento">Em andamento</a></li>
                            <li><a href="#acordo">Em acordo</a></li>
                            <li><a href="#atraso">Em atraso</a></li>
                        </ul>
                    </div>
                    <div id="virgem">
                        <div class="form">
                            <div class="fields">
                                <form id="form" method="post">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 286px;">
                                            <div style="width: 69px; padding-left: 1px;" class="label">
                                                <label for="nome">Filtro:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 188px;" type="text" id="nome" name="nome"/>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 30px;">Cod.</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Valor</th>
                                               <th>�ltimo RO</th>
                                                <th class="last">Visualizar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="title" style="width: 30px;">122</td>
                                                <td>Jos� da Silva</td>
                                                <td>Radioval</td>
                                                <td>R$ 1.500,00</td>
                                                <td>
                                                    xx/xx/xxxx
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" class="visualizar" type="submit" name="cadastrar" value="D�vida" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">123</td>
                                                <td>Jo�o Cresc�ncio</td>
                                                <td>Luval</td>
                                                <td>R$ 500,00</td>
                                                <td>
                                                    15/01/2011
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" class="visualizar" type="submit" name="cadastrar" value="D�vida" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">124</td>
                                                <td>Joaquim Jos� da Silva Xavier</td>
                                                <td>Loja Rosa</td>
                                                <td>R$ 300,00</td>
                                                <td>
                                                    xx/xx/xxxx
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" class="visualizar" type="submit" name="cadastrar" value="D�vida" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">125</td>
                                                <td>Pedro Lisboa</td>
                                                <td>Supermercado Dion�sio</td>
                                                <td>R$ 1.500,00</td>
                                                <td>
                                                    31/01/2011
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" class="visualizar" type="submit" name="cadastrar" value="D�vida" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="acordo">
                        <div class="form">
                            <div class="fields">
                                <form id="form" action="" method="post">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 286px;">
                                            <div style="width: 69px; padding-left: 1px;" class="label">
                                                <label for="nome">Filtro:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 188px;" type="text" id="nome" name="nome"/>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 30px;">Cod.</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Valor</th>
                                                <th>�ltimo RO</th>
                                                <th class="last">Visualizar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="title" style="width: 30px;">125</td>
                                                <td>Ant�nio Gon�alves da Silva</td>
                                                <td>Radioval</td>
                                                <td>R$ 800,00</td>
                                                <td>
                                                    25/01/2011 - 15 - Ligar para inadimplente
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">127</td>
                                                <td>Felisberto Teixeira</td>
                                                <td>Luval</td>
                                                <td>R$ 200,00</td>
                                                <td>
                                                    15/01/2011 - 13 - Mandar carta
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">128</td>
                                                <td>Joaquim Jos� da Silva Xavier</td>
                                                <td>Loja Rosa</td>
                                                <td>R$ 300,00</td>
                                                <td>
                                                    17/01/2011 - 10 - Mandar mensagem
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">129</td>
                                                <td>Pedro Lisboa</td>
                                                <td>Supermercado Dion�sio</td>
                                                <td>R$ 1.500,00</td>
                                                <td>
                                                    31/01/2011 - 15 - Ligar para inadimplente
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">125</td>
                                                <td>Ant�nio Gon�alves da Silva</td>
                                                <td>Radioval</td>
                                                <td>R$ 800,00</td>
                                                <td>
                                                    25/01/2011 - 08 - Inadimplente entrou em contato
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">127</td>
                                                <td>Felisberto Teixeira</td>
                                                <td>Luval</td>
                                                <td>R$ 200,00</td>
                                                <td>
                                                    15/01/2011 - 15 - Ligar para inadimplente
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">128</td>
                                                <td>Joaquim Jos� da Silva Xavier</td>
                                                <td>Loja Rosa</td>
                                                <td>R$ 300,00</td>
                                                <td>
                                                    17/01/2011 - 10 - Mandar mensagem
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">129</td>
                                                <td>Pedro Lisboa</td>
                                                <td>Supermercado Dion�sio</td>
                                                <td>R$ 1.500,00</td>
                                                <td>
                                                    31/01/2011 - 10 - Telefone n�o atende
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="andamento">
                        <div class="form">
                            <div class="fields">
                                <form id="form" action="" method="post">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 286px;">
                                            <div style="width: 69px; padding-left: 1px;" class="label">
                                                <label for="nome">Filtro:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 188px;" type="text" id="nome" name="nome"/>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 30px;">Cod.</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Valor</th>
                                                <th>�ltimo RO</th>
                                                <th class="last">Visualizar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="title" style="width: 30px;">130</td>
                                                <td>Fernando Sorocaba</td>
                                                <td>Supermercado Lucas</td>
                                                <td>R$ 500,00</td>
                                                
                                                <td>
                                                    26/01/2011 - 10 - Mandar mensagem
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">123</td>
                                                <td>Daniel da Silva</td>
                                                <td>Serv Festas Bot</td>
                                                <td>R$ 50,00</td>
                                                
                                                <td>
                                                    15/01/2011 - 15 - Ligar para inadimplente
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">124</td>
                                                <td>Fernando Junior</td>
                                                <td>Ponto Quente</td>
                                                <td>R$ 300,00</td>
                                                
                                                <td>
                                                    18/08/2010 - 08 - Inadimplente entrou em contato
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">125</td>
                                                <td>Egberto Pereira</td>
                                                <td>Supermercado Dion�sio</td>
                                                <td>R$ 1.500,00</td>
                                                
                                                <td>
                                                    31/01/2011 - 10 - Telefone n�o atende
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="atraso">
                        <div class="form">
                            <div class="fields">
                                <form id="form" action="" method="post">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 286px;">
                                            <div style="width: 69px; padding-left: 1px;" class="label">
                                                <label for="nome">Filtro:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 188px;" type="text" id="nome" name="nome"/>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 30px;">Cod.</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Valor</th>
                                                <th>Vencimento</th>
                                                <th class="last">Visualizar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="title" style="width: 30px;">122</td>
                                                <td>Jos� da Silva</td>
                                                <td>Radioval</td>
                                                <td>R$ 1.500,00</td>
                                                
                                                <td>
                                                    08/09/2010 - 10 - Telefone n�o atende
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">123</td>
                                                <td>Jo�o Cresc�ncio</td>
                                                <td>Luval</td>
                                                <td>R$ 500,00</td>
                                                
                                                <td>
                                                    31/12/2010 - 10 - Mandar mensagem
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">124</td>
                                                <td>Joaquim Jos� da Silva Xavier</td>
                                                <td>Loja Rosa</td>
                                                <td>R$ 300,00</td>
                                                
                                                <td>
                                                    01/01/2011 - 15 - Ligar para inadimplente
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">125</td>
                                                <td>Pedro Lisboa</td>
                                                <td>Supermercado Dion�sio</td>
                                                <td>R$ 1.500,00</td>
                                                
                                                <td>
                                                    31/01/2011 - 08 - Inadimplente entrou em contato
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">122</td>
                                                <td>Jos� da Silva</td>
                                                <td>Radioval</td>
                                                <td>R$ 1.500,00</td>

                                                <td>
                                                    08/09/2010 - 10 - Telefone n�o atende
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">123</td>
                                                <td>Jo�o Cresc�ncio</td>
                                                <td>Luval</td>
                                                <td>R$ 500,00</td>

                                                <td>
                                                    31/12/2010 - 10 - Mandar mensagem
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">124</td>
                                                <td>Joaquim Jos� da Silva Xavier</td>
                                                <td>Loja Rosa</td>
                                                <td>R$ 300,00</td>

                                                <td>
                                                    01/01/2011 - 15 - Ligar para inadimplente
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title" style="width: 30px;">125</td>
                                                <td>Pedro Lisboa</td>
                                                <td>Supermercado Dion�sio</td>
                                                <td>R$ 1.500,00</td>

                                                <td>
                                                    31/01/2011 - 10 - Telefone n�o atende
                                                </td>
                                                <td class="last">
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>