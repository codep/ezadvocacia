<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img . 'logo_elisangela.jpg'; ?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br|www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo 'Dividas ociosas'; ?></p>
        </div>
    </div>
    <table id="tableAdm04" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="th0">C�digo</th>
                <th id="th1">Inadimplente</th>
                <th id="th2">Credor</th>
                <th id="th3">Tipo</th>
                <th id="th5">Data</th>
                <th id="th6" class="last">�ltimo RO</th>
            </tr>
        </thead>
        <tbody>
            <?php $total = 0; foreach ($dividasOciosas as $divida) : ?>
                <tr>

                    <td class="td1" style="text-align: center;"><?php echo utf8_decode($divida->cob_cod); ?></td>
                    <td class="tdInaNome"><?php echo utf8_decode($divida->ina_nome); ?></td>
                    <td class="tdCreNomeFantasia"><?php echo utf8_decode($divida->cre_nome_fantasia); ?></td>

                    <td class="td4">
                        Em Andamento
                    </td>
                    <td class="td5">
                        <?php echo convData($divida->div_cadastro, 'd'); ?>
                    </td>
                    <td class="td6 last">
                        <?php echo $divida->ultimo_ro == ''?'Nenhum RO cadastrado':convData($divida->ultimo_ro, 'd'); ?>
                    </td>
                    
                    <?php $total++; endforeach; ?>
            </tr>
            <tr>
                <td id="tdTotalA" colspan="5">TOTAL DE COBRAN�AS</td>
                <td id="tdTotalB" class="last"><?php echo "$total"; ?></td>
            </tr>
        </tbody>
    </table>
    <br/>
    <div class="relatorio">
        <div class="linha1">
            <div class="linha2">
                <p class="rel_titulo"><?php echo 'Dividas virgens ociosas'; ?></p>
            </div>
        </div>
        <?php if(sizeof($dividasVirgensOciosas) != 0):?>
        <table id="tableAdm04a" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th id="th0" style="width: 95px;">C�digo</th>
                    <th id="th1">Inadimplente</th>
                    <th id="th2">Credor</th>
                    <th id="th3" style="width: 95px;" class="last">Tipo</th>
                </tr>
            </thead>
            <tbody>
                    <?php $total = 0; foreach ($dividasVirgensOciosas as $divida) : ?>
                    <tr>

                        <td class="td1" style="text-align: center;width: 95px;"><?php echo utf8_decode($divida->cob_cod); ?></td>
                        <td class="tdInaNome"><?php echo utf8_decode($divida->ina_nome); ?></td>
                        <td class="tdCreNomeFantasia"><?php echo utf8_decode($divida->cre_nome_fantasia); ?></td>

                        <td class="td4 last" style="width: 95px;">
                            Virgem
                        </td>
                    <?php $total++; endforeach; ?>
                </tr>
                <tr>
                    <td id="total" colspan="3">TOTAL DE COBRAN�AS</td>
                    <td id="td6" class="last"><?php echo "$total"; ?></td>
                </tr>
            </tbody>
        </table>
        <?php endif; ?>
        <?php echo sizeof($dividasVirgensOciosas) == 0 ?'N�o h� d�vidas virgens ociosas':''?>
    </div>
    <br/>
    <div class="relatorio">
        <div class="linha1">
            <div class="linha2">
                <p class="rel_titulo"><?php echo 'Acordos ociosos'; ?></p>
            </div>
        </div>
        <table id="tableAdm04b" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th id="th0">C�digo</th>
                    <th id="th1">Inadimplente</th>
                    <th id="th2">Credor</th>
                    <th id="th3">Tipo</th>
                    <th id="th4">Num. Parcelas</th>
                    <th id="th5">Emiss�o</th>
                    <th id="th6" class="last">�ltimo RO</th>
                </tr>
            </thead>
            <tbody>
                    <?php $total = 0; foreach ($acordosOciosos as $divida) : ?>
                    <tr>

                        <td class="td1" style="text-align: center;"><?php echo utf8_decode($divida->cob_cod); ?></td>
                        <td class="tdInaNome"><?php echo utf8_decode($divida->ina_nome); ?></td>
                        <td class="tdCreNomeFantasia"><?php echo utf8_decode($divida->cre_nome_fantasia); ?></td>

                        <td id="td3">
                            <?php echo $divida->aco_tipo=='1'?'Acordo firmado':'Promessa de acordo' ?>
                        </td>
                        <td id="td3">
                            <?php echo $divida->num_parc ?>
                        </td>
                        <td id="td3">
                            <?php echo convData($divida->aco_emissao, 'd'); ?>
                        </td>
                        <td id="td3" class="last">
                            <?php echo $divida->ultimo_ro == ''?'Nenhum RO cadastrado':convData($divida->ultimo_ro, 'd'); ?>
                        </td>
                    <?php $total++; endforeach; ?>
                </tr>
                <tr>
                    <td colspan="6" id="total">TOTAL DE COBRAN�AS</td>
                    <td id="td7" class="last"><?php echo "$total"; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

    