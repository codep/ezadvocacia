<script type="text/javascript">

	var lastParcela=0;

    //ATUALIZA COMBOS
    function refreshSelectMenu() {
		$("select:not(.chosen)").selectmenu("destroy").selectmenu({
	        style: 'dropdown',
	        icons: [
			    { find: '.locked', icon: 'ui-icon-locked' },
			    { find: '.unlocked', icon: 'ui-icon-unlocked' },
			    { find: '.folder-open', icon: 'ui-icon-folder-open' }
		    ]
	    });
    }
    
	//VALIDAR DATAS - IMPEDIR A DIGITA��O DE DATAS INEXISTENTES (31/02/5010)    
    function validaEmissao(dataInput)
    {
        var emissao = dataInput.split('/');
        emissao = emissao[2]+""+emissao[1]+""+emissao[0];
        var objData = new Date();
        var dia = objData.getDate();
        if(dia <10) dia = "0"+dia; //dias abaixo de 10 n�o apresentam 0
        var mes = objData.getMonth();
        if(mes < 9) mes = "0"+(mes+1); //m�s � de 0 a 11
        else mes+=1;
        var ano = objData.getFullYear();
        
        var auxiliar = ''+ano+''+mes+''+dia;
        
        if(parseFloat(emissao) < parseFloat(auxiliar))
            return true;
        else
            return false;
    }
        
    function validarData(data)
    {
            
        var valido = false;

        if(data == '')
        {
            alert("Digite corretamente a data.");
            return false;
        }
              
        else
        {
            var dia = data.split("/")[0];
            var mes = data.split("/")[1];
            var ano = data.split("/")[2];
            //              alert('ihuuu');
            //              alert('DIA: '+dia+ ' | MES: '+mes+ ' | ANO: '+ano);
            var MyData = new Date(ano, mes - 1, dia);
            if((MyData.getMonth() + 1 != mes)||
                (MyData.getDate() != dia)||
                (MyData.getFullYear() != ano))
                alert("Valores inv�lidos para o dia, m�s ou ano. Por favor corrija.");
            else
                valido = true;
        }
        //            if(valido == false)
        //            {
        //                alert('NUM PRESTO');
        ////              data.focus();
        ////              data.select();
        //            }
        return valido;
        //            alert("T� beleza!");
    }
    $(document).ready(function(){
         
        var ok = false;
        var dadosDiv = false;
        //OCULTANDO OS DADOS A SEREM EXIBIDOS AP�S A ESCOLHA DO TIPO DE PESSOA [f] ou [j]
        $('#dadosCredor').hide();
        $('#dadosInadimplente').hide();
        $('#repasses').hide();
        $('#dadosDivida').hide();
        
        $('#tipo_dest').change(function(e){
        	var data = $(this).find('option:selected').data();
        	if ($(this).val()=='') {
        		$('#tipo_doc, #banco, #agencia, #alinea, #emissao').val('');
        		$('#box_new_divida').show();
        	} else {
        		$('#box_new_divida').hide();
				$('#tipo_doc').val(data.div_documento);
				$('#banco').val(data.div_banco);
				$('#agencia').val(data.div_agencia);
				$('#alinea').val(data.div_alinea);
				var aData = data.div_emissao.split('-');
				var novaData = aData[2]+'/'+aData[1]+'/'+aData[0];
				$('#emissao').val(novaData);
				lastParcela = data.div_qtd_parc;
				console.log(data);
        	}
			refreshSelectMenu();
        });

        //M�SCARAS
        $('#parcelas').numeric();
        $('input[name="valor_parc"]').priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });
        $('.data').mask('99/99/9999');

        //OCULTANDO OS CAMPOS DO RECUPERADOR
        $('.recuperador').hide();
        //========================== VALIDA��O DE FORMULARIO ===========================
        $('#formDiv').submit(function(){
            
            recuperador = "";
            
            $("select[name=recuperador] option:selected").each(function () {
                recuperador += $(this).val() + "";
            });
            
            var pessoaVazio = true;
            $('input[name="cre_pessoa"]').each(function() {
                if ( $(this).is(':checked') ) {
                    pessoaVazio = false;
                }
            });

            if(pessoaVazio){
                alert('Indique pessoa F�SICA ou JUR�DICA para o CREDOR.');
                return false;
            }
            else if($('input[name="cre_nome"]').val() == ''){
                $('input[name="cre_nome"]').focus();
                return false;
            }

            var pessoaInaVazio = true;
            $('input[name="ina_pessoa"]').each(function() {
                if ( $(this).is(':checked') ) {
                    pessoaInaVazio = false;
                }
            });

            if(pessoaInaVazio){
                alert('Indique pessoa F�SICA ou JUR�DICA para o INADIMPLENTE.');
                return false;
            }
            else if($('input[name="ina_nome"]').val() == ''){
                $('input[name="ina_nome"]').focus();
                return false;
            }

            else if(($('input[name="emissao"]').val() == '') && ($('input[name="cre_cod"]').val() != '') && ($('input[name="ina_cod"]').val() != '')){
                $('input[name="emissao"]').focus();
                return false;
            }
            
            else if(($('input[name="emissao"]').val() == '') && ($('input[name="cre_cod"]').val() == '') || ($('input[name="ina_cod"]').val() == '')){
                alert('Voc� precisa selecionar um credor e um inadimplente para prosseguir.');
                return false;
            }

            else if($('input[name="qtd_parcela"]').val() == ''){
                $('input[name="qtd_parcela"]').focus();
                return false;
            }

            else if($('input[name="venc_inicial"]').val() == ''){
                $('input[name="venc_inicial"]').focus();
                return false;
            }

            else if($('input[name="valor_parc"]').val() == ''){
                $('input[name="valor_parc"]').focus();
                return false;
            }

            else if($('#repassesCredor option:selected').val() == ''){
                alert('Selecione o valor de REPASSE');
                return false;
            }


            else if($('input[name="num_par[]"]').val() == ''){
                $('input[name="num_par[]"]').focus();
                return false;
            }
            else if($('input[name="valor_par[]"]').val() == ''){
                $('input[name="valor_par[]"]').focus();
                return false;
            }
            else if($('input[name="venc_par[]"]').val() == ''){
                $('input[name="venc_par[]"]').focus();
                return false;
            }
            else if (ok==false){
                alert('Confira as parcelas da d�vida, e/ou clique no bot�o gerar');
                return false
            }
            else if(recuperador == "0"){
                alert("Selecione o recuperador respons�vel pela cobran�a");
                return false;
            }
            
            else{
                $('#btnCadastrar').hide();
            }
            
            $(".parcelas_divida").each(function() {
                
            });


        });

        $('#cancelar').click(function(){
            var resposta = confirm('Tem certeza de que deseja cancelar? \nTODOS OS DADOS DIGITADOS SER�O PERDIDOS!');

            if(resposta){
                location.reload();
            }
            else
                return resposta;
        });
        //======================= FIM DA VALIDA��O DE FORMULARIO =======================



        //CREDOR: AO CLICAR EM UM TIPO DE PESSOA
        $('input[name="cre_pessoa"]').click(function() {


            if($('#credor_nome').val()!= '' ||$('#credor_cpf_cnpj').val()!= '')
            {
                $('#repasses').fadeOut(function(){
                    //EXIBE BLOCO PAI DOS REPASSES
                });
                $('#repassesCredor').html('<select id="repassesCredor" name="repasses"></select>');
            }

            //RESETAR CAMPOS
            $('#cre_nome').html('<input style="width: 248px;" type="text" id="credor_nome" name="cre_nome" />');
            $('#cre_cpf_cnpj').html('<input style="width: 124px;" type="text" id="credor_cpf_cnpj" name="cre_cpf_cnpj"/>');
            $('#hiddenCredor').html('<input type="hidden" name="cre_cod" value=""/>');

            //OCULTAR RECUPERADOR
            $('.recuperador').fadeOut(function(){
                //OCULTA BLCO TODO
            });

            //EFEITO DE EXIBI��O
            $('#dadosCredor').fadeIn(function(){
                //EXIBE OS DADOS DO CREDOR
            });

            //SE FOR PESSOA F�SICA
            if($(this).attr('value') == 'f')
            //SETA A M�SCARA DE CPF
                $('input[name="cre_cpf_cnpj"]').mask('999.999.999-99');
            //SE FOR PESSOA JUR�DICA
            else if($(this).attr('value') == 'j')
            //SETA A M�SCARA DE CNPJ
                $('input[name="cre_cpf_cnpj"]').mask('99.999.999/9999-99');
            
        });

        //INADIMPLENTE: AO CLICAR EM UM TIPO DE PESSOA
        $('input[name="ina_pessoa"]').click(function() {

            //RESETAR CAMPOS
            $('#ina_nome').html('<input style="width: 248px;" type="text" id="inad_nome" name="ina_nome"/>');
            $('#ina_cpf_cnpj').html('<input style="width: 124px;" type="text" id="inad_cpf_cnpj" name="ina_cpf_cnpj"/>');
            $('#hiddenInadimplente').html('<input type="hidden" name="ina_cod" value=""/>');

            //EFEITO DE EXIBI��O
            $('#dadosInadimplente').fadeIn(function(){
                //EXIBE OS DADOS DO INADIMPLENTE
            });
            
            //SE FOR PESSOA F�SICA
            if($(this).attr('value') == 'f')
            //SETA A M�SCARA DE CPF
                $('input[name="ina_cpf_cnpj"]').mask('999.999.999-99');
            //SE FOR PESSOA JUR�DICA
            else if($(this).attr('value') == 'j')
            //SETA A M�SCARA DE CNPJ
                $('input[name="ina_cpf_cnpj"]').mask('99.999.999/9999-99');

        });
        //==============================================================================
        //============================ GERAR PARCELAS ==================================
        //==============================================================================

        $('#btnGerar').click(function(e){
            e.preventDefault();
            if(!validaEmissao(($('input[name="emissao"]').val()))){
                alert("A data de emiss�o n�o deve ser superior a data de hoje.")
                $('input[name="emissao"]').focus();
                return false;
            }
            else if ($('input[name="qtd_parcela"]').val()==''){
                $('input[name="qtd_parcela"]').focus();
            }else if($('input[name="venc_inicial"]').val()==''){
                $('input[name="venc_inicial"]').focus();
            }else if($('input[name="valor_parc"]').val()==''){
                $('input[name="valor_parc"]').focus();
            }
            else{
            	
            	$('#ParcGerada').html('');

                var aux = $('input[name="qtd_parcela"]').val();

                var dia = $('input[name="venc_inicial"]').val();
                dia = dia.substr(0,2);
                var mes = $('input[name="venc_inicial"]').val();;
                mes = mes.substr(3,2);
                var ano = $('input[name="venc_inicial"]').val();;
                ano = ano.substr(6,4);
                var valor = $('input[name="valor_parc"]').val();
                var parnum = (lastParcela!=undefined && lastParcela!='') ? parseInt(lastParcela)+1 : 1;

                var mesAux = '';

                for ( var i = 0 ; i < aux ; i++ )
                {
                    if (mes>12){
                        ano++;
                        mes=1;
                    }
                    if ((mes<10)&&(i>0)){
                        mes='0'+mes;
                    }
                    if((mes == '02') && (dia == '29' || dia == '30' || dia == '31'))
                    {
                        mesAux = dia;
                        dia = '28';
                    }
                    if((dia == '31') && (mes == '04' || mes == '06' || mes == '09' || mes == '11'))
                    {
                        mesAux = dia;
                        dia = '30';
                    }
                    $('#ParcGerada').append(
                    '<div class="field">'+
                        '<div class="divleft" style="width: 151px;">'+
                        '<div style="width: 64px; padding-left: 5px;" class="label">'+
                        '<label style="font-weight: normal;" for="doc_par[]">N�.Doc.:</label>'+
                        '</div>'+
                        '<div class="input">'+
                        '<input style="width: 53px;" type="text" maxlength="20" value="P'+parnum+'" name="doc_par[]"/>'+
                        '</div>'+
                        '</div>'+
                        '<div class="divleft" style="width: 128px;">'+
                        '<div style="width: 80px; padding-left: 0px;" class="label">'+
                        '<label style="font-weight: normal;" for="num_par[]">Parcela N�.:</label>'+
                        '</div>'+
                        '<div class="input">'+
                        '<input style="width: 18px;" type="text" value="'+parnum+'" name="num_par[]"/>'+
                        '</div>'+
                        '</div>'+
                        '<div class="divleft" style="width: 242px;">'+
                        '<div style="width: 112px; padding-left: 1px;" class="label">'+
                        '<label style="font-weight: normal;" for="valor_par[]">V.Parcela (R$):</label>'+
                        '</div>'+
                        '<div class="input">'+
                        '<input style="width: 99px;" type="text" value="'+valor+'" name="valor_par[]" class="parcelas_divida"/>'+
                        '</div>'+
                        '</div>'+
                        '<div class="divleftlast" style="margin-left: 0px; width: 159px;">'+
                        '<div style="width: 45px; padding-left: 1px;" class="label">'+
                        '<label style="font-weight: normal;" for="venc_par[]">Venc.:</label>'+
                        '</div>'+
                        '<div style=" width: 109px;" class="input">'+
                        '<input type="text" style="width: 97px;" name="venc_par[]" value="'+dia+'/'+mes+'/'+ano+'" class="data datasParcelas"/>'+
                        '</div>'+
                        '</div>'+
                        '</div>');
                    parnum++;
                    if(mesAux != '') dia = mesAux;
                    mes++;
                }

                //AO PERDER O FOCO NA DATA, FAZ A VALIDA�AO DA MESMA
                $('.datasParcelas').blur(function(){
                    if(validarData($(this).val()))
                    {
                        return false;
                    }
                    else
                        $(this).focus();
                });

                //$('input[name="qtd_parcela"]').attr('readonly',true);
                //$('#btnGerar').attr('disabled',true);
                $('.date').mask('99/99/9999');
                $('.parcelas_divida').priceFormat({
                    prefix: "",
                    centsSeparator: ",",
                    thousandsSeparator: "."
                });
                ok = true;
            }

        });

        //==============================================================================
        var buscaDividas = function() {
        	
        	var url_ajax = '<?php echo base_url(); ?>divida/listarAjax';
        	
        	//PESQUISA TODAS AS DIV�DAS PARA ESSE CREDOR E PARA ESSE INADIMPLENTE
        	var cre_cod = $('input[name="cre_cod"]').val();
        	var ina_cod = $('input[name="ina_cod"]').val();
        	//console.log('cre_cod: '+cre_cod);
        	//console.log('ina_cod: '+ina_cod);
        	
        	if (cre_cod=='' || cre_cod==0 || ina_cod=='' || ina_cod==0) {
        		alert('O credor e/ou o inadimplente n�o foi selecionado corretamente, por isso n�o ser� poss�vel recuperar a lista de d�vidas atuais.');
        	} else {
	            $.ajax({
	                type: "POST",//TIPO DE DADOS
	                url: url_ajax,//FUN��O DO CONTROLLER PARA TRATAR A LISTAGEM
	                data: 'c=' + cre_cod + '&i=' + ina_cod,
	                dataType: 'json', //TIPO DE DADOS A SER RECEBIDO
	                error: function(xhr, status, er) {
	                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
	                },
	                success: function(retorno) {
	                	//console.log(retorno);
	                	
	                	//LIMPANDO O SELECT
	                	$('#tipo_dest option:not(.fix)').remove();
	                	
	                	//PERCORRENDO O RETORNO CASO EXISTA
	                	if (retorno!=undefined && retorno!='' && retorno.length>0) {
	                		$.each(retorno,function(i,f) {
	                			//console.log('i: '+i);
	                			//console.log(f);
	                			$('#tipo_dest').append('<option value="'+f.cobranca_cob_cod+'">['+f.cobranca_cob_cod+'] '+f.div_documento + ' (' + f.div_total + ')</option>');
	                			$('#tipo_dest option:last').data(f);
	                			refreshSelectMenu();
	                			
	                		});
	                	}
	                	
	              	}
	          	});
          	}
        }; 
        
        //==============================================================================
        
        //LISTANDO OS RESULTADOS DA PESQUISA
        /* *************************************************************************
         * PESQUISA INADIMPLETE OU CREDOR POR NOME OU CPF/CNPJ
         * pesquisaIC($entidade, $pessoa, $tipoDado, $valor)
         * $entidade -------- i=inadimplentes, c=credores
         * $pessoa ---------- j=jur�dica, f=f�sica
         * $tipoDado -------- n=nome, c=cpf/cnpj
         * $valor ----------- valor do nome ou cpf/cnpj
         *
         * NECESS�RIO CONFIGURAR O VALOR DOS ATRIBUTOS NAME DOS INPUTS
         ************************************************************************* */
        $(".dialog-form-open").click(function () {
            //ENTIDADE DO TIPO [CRE] ou [INA]
            tipo = $(this).attr('name');

            //PRIMEIRO CARACTERE DO TIPO
            entidade = tipo.charAt(0);

            //URL DA FUN��O AJAX
            if(entidade == 'c')
                url_ajax = '<?php echo base_url(); ?>credor/listar_pesquisa';
            else if(entidade == 'i')
                url_ajax = '<?php echo base_url(); ?>inadimplente/listar_pesquisa';
            else{
                alert('Problema com a sele��o da ENTIDADE');
                return false;
            }


            //RECEBENDO O TIPO DE PESSOA [f ou j]
            pessoa = '';
            $('input[name="'+tipo+'_pessoa"]').each(function() {
                if ( $(this).is(':checked') ) {
                    pessoa = $(this).attr('value');
                }
            });

            //RECEBENDO O TIPO DE DADOS [nome_fantasia ou cpf_cnpj]
            tipodado = '';

            //RECEBENDO O VALOR DIGITADO EM nome_fantasia ou cpf_cnpj
            valor = '';
            if(($('input[name="'+tipo+'_nome"]').val() == '') && ($('input[name="'+tipo+'_cpf_cnpj"]').val() == '')){
                $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Para pesquisar � necess�rio digitar o NOME ou CPF/CNPJ</h3></td></tr>');//zera os valores antigos e escreve no elemento o valor selecione
                return false;
            }
            else {
                if ($('input[name="'+tipo+'_cpf_cnpj"]').val() != ''){
                    valor = $('input[name="'+tipo+'_cpf_cnpj"]').val();
                    tipodado = 'c';
                }

                else if ($('input[name="'+tipo+'_nome"]').val() != ''){
                    valor = $('input[name="'+tipo+'_nome"]').val();
                    tipodado = 'n';
                }
            }

            //FUN��O AJAX
            $.ajax({
                type: "POST",//TIPO DE DADOS
                url: url_ajax,//FUN��O DO CONTROLLER PARA TRATAR A LISTAGEM
                data: 'entidade='+entidade+'&pessoa='+pessoa+'&tipodado='+tipodado+'&valor='+valor,//DADOS ENVIADOS
                dataType: 'json',//TIPO DE DADOS A SER RECEBIDO
                error: function(xhr, status, er) {
                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                },

                //CASO A FUN��O DO CONTROLLER SEJA EXECUTADA COM SUCESSO E OS DADOS RECEBIDOS COM SUCESSO
                success: function(retorno)
                {
                    $("#dialog-form").dialog("open");///ABRE A CAIXA DE DI�LOGO
                    $('#dadosPesquisa').html('');//zera os valores antigos e escreve no elemento o valor selecione
                    vazio = false;//VARI�VEL PARA IDENTIFICAR SE ALGUM DADO �NTEGRO TENHA SIDO RECEBIDO

                    $.each(retorno.resultado, function(i, v){//para cada �ndice retornado executa a fun��o
                        if(v.nome != null){
                            //CONCATENA AS LINHAS DA TABELA COM OS VALORES RECEBIDOS
                            $('#dadosPesquisa').append(
                            '<tr class="tuplas" id="'+unescape(v.codigo)+'">'+
                                '<input type="hidden" id="'+unescape(v.recuperador_cod)+'" value="'+unescape(v.recuperador_nome)+'"/>'+
                                '<input type="hidden" id="prazo" value="'+unescape(v.prazo)+'"/>'+
                                '<td>'+unescape(v.nome)+
                                '<td>'+unescape(v.cpf_cnpj)+
                                '<td>'+unescape(v.endereco)+
                                '<td class="last">'+unescape(v.cidade)+
                                '</tr>'
                        );
                        }
                        else vazio = true;//CASO OS DADOS RETORNADOS SEJAM NULOS
                    });
                    $('.tuplas').css('cursor','pointer');//SETA O TIPO DO CURSOR

                    //AO CLICAR SOBRE UMA LINHA DA TABALEA COM A CLASSE TUPLA
                    $('.tuplas').click(function(e){
                        e.preventDefault();
                        
                        //VARI�VEIS QUE RECEBEM O VALOR DO C�DIGO DO RECUPERADOR PADR�O E SEU NOME
                        recuperador_cod = $(this).find(":eq(0)").attr("id");
                        recuperador_nome = $(this).find(":eq(0)").val();

                        //VARI�VEL QUE RECEBE O VALOR DO PRAZO JUR�DICO
                        prazo = $(this).find(":eq(1)").val();

                        //VARI�VEL QUE RECEBE O VALOR DO PRIMEIRO FILHO (TD) DA TR CLICADA
                        nome = $(this).find(":eq(2)").text();

                        //VARI�VEL QUE RECEBE O VALOR DO CPF_CNPJ DO SEGUNDO FILHO (TD) DA TR CLICADA
                        cpf_cnpj = $(this).find(":eq(3)").text();

                        //ATRIBUI O NOME DO CREDOR AO INPUT "NOME"
                        if(entidade == 'c'){
                            $('#hiddenCredor').html(
                            '<input type="hidden" name="cre_cod" value="'+$(this).attr('id')+'"/>'+
                                '<input type="hidden" name="usuarios_usu_cod" value="'+recuperador_cod+'"/>'+
                                '<input type="hidden" name="prazo" value="'+prazo+'"/>'
                        	);
                            $('#credor_nome').val(nome);
                            $('#credor_cpf_cnpj').val(cpf_cnpj);
                            //                            $('#cre_prazo').val(cre_prazo);
                            $('#repassesCredor').html('<select id="repassesCredor" name="repasses">');
                            $('select[name="repasses"]').append('<option value="">[SELECIONE UM REPASSE]</option>');
                            exibeRepassesAjax($(this).attr('id'));
                            $('#repassesCredor').append('</select>');
                            $('#repasses').fadeIn(function(){
                                //EXIBE BLOCO PAI DOS REPASSES
                            });
                            $('.recuperador').fadeIn(function(){
                                $('#recuperador_padrao').html(recuperador_nome).css("color","red");
                                //                                $('#select_recuperador option[value="'+recuperador_cod+'"').attr({ selected : "selected" });
                                //                                $('#select_recuperador-button').find(":eq(0)").html(recuperador_nome);
                            });
                            //CASO INADIMPLENTE E CREDOR ESTEJAM PREENCHIDOS EXIBIR O BLOCO DE DADOS DA DIVIDA
                            if($('#inad_nome').val() != '' && $('#credor_nome').val() != '')
                            {
                                $('#dadosDivida').fadeIn(function(){
									buscaDividas();                                	
                                    //EXIBE O BLOCO DE DADOS DA DIVIDA
                                });
                            }
                        }

                        //ATRIBUI O NOME DO INADIMPLENTE AO INPUT "NOME"
                        if(entidade == 'i'){
                            $('#hiddenInadimplente').html('<input type="hidden" name="ina_cod" value="'+$(this).attr('id')+'"/>');
                            $('#inad_nome').val(nome);
                            $('#inad_cpf_cnpj').val(cpf_cnpj);

                            //CASO INADIMPLENTE E CREDOR ESTEJAM PREENCHIDOS EXIBIR O BLOCO DE DADOS DA DIVIDA
                            if($('#inad_nome').val() != '' && $('#credor_nome').val() != '')
                            {
                                $('#dadosDivida').fadeIn(function(){
                                	buscaDividas();
                                    //EXIBE O BLOCO DE DADOS DA DIVIDA
                                });
                            }
                        }

                        //SIMULA UM CLIQUE NO BOT�O FECHAR DA CAIXA DE DI�LOGO
                        $('.ui-dialog-titlebar-close').trigger('click');

                    });

                    //CASO OS DADOS RETORNADOS SEJAM NULOS
                    if (vazio)
                    {   //EXIBE MENSAGEM
                        $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Nenhum dado encontrado pesquisando por ['+valor+']</h3></td></tr>');
                    }

                    
                }
            });
        });
        function exibeRepassesAjax(cod)
        {
            var resultado = '';
            //            return resultado;
            $.ajax({
                type: "POST",//TIPO DE DADOS
                url: '<?php echo base_url(); ?>credor/listar_repasses',
                data: 'cod='+cod,//DADOS ENVIADOS
                dataType: 'json',//TIPO DE DADOS A SER RECEBIDO

                //CASO HAJA ERRO DE PROCESSAMENTO
                error: function(xhr, status, er) {
                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                },

                //CASO A FUN��O DO CONTROLLER SEJA EXECUTADA COM SUCESSO E OS DADOS RECEBIDOS COM SUCESSO
                success: function(retorno)
                {
                    $.each(retorno.repasses, function(i, v){//para cada �ndice retornado executa a fun��o
                        //ACRESCENTA OS OPTIONS DE REPASSES NO SELECT
                        $('select[name="repasses"]').append('<option value="'+unescape(v.repasse_cod)+'">'+unescape(v.repasse_nome)+'</option>');
                    });
                }
            });
        }

       
    });
</script>
<div id="dialog-form" style="padding-left: 5px; padding-right: 5px; " title="Pesquisar dados">
    <p style="text-align: center">
        <strong>Dados encontrados:</strong>
    </p>
    <div id="content" style="width: 750px;">
        <div id="right" style="margin-left: 0px;">
            <div class="box">
                <div class="table" style="padding-left: 0px; font-size: 10px; width: 750px;">
                    <form action="" method="post">
                        <table id="products">
                            <thead>
                                <tr>
                                    <th class="left" style="width: 20%;">Nome</th>
                                    <th style="width: 14%;">CPF/CNPJ</th>
                                    <th>ENDERE�O</th>
                                    <th class="last">CIDADE</th>
                                </tr>
                            </thead>
                            <tbody id="dadosPesquisa">
                                <!--RESULTADO DA PESQUISA AQUI!! -->
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Nova d�vida</h5>
            </div>
            <?php echo $mensagem; ?>
            <form id="formDiv" action="<?php echo base_url() . 'divida/incluir' ?>" method="post">
                <div class="form">
                    <div class="fields">
                        <div style="margin-top: 0px; margin-bottom: 13px;" class="blocoTitulo">
                            Dados do credor
                        </div>
                        <div id="hiddenCredor" style="display:none"></div>
                        <div class="field  field-first">
                            <div class="divleftleft" style="height:29px; width: 200px;">
                                <div class="label">
                                    <label>Pessoa:</label>
                                </div>
                                <input style="margin-top: 7px; " type="radio" name="cre_pessoa" value="f" />F�sica
                                <input type="radio" name="cre_pessoa" value="j" />Jur�dica
                            </div>
                        </div>
                        <div class="field" id="dadosCredor">
                            <div class="divleft" style="width: 371px;">
                                <div style="width: 89px;" class="label">
                                    <label for="cre_nome">Nome:</label>
                                </div>
                                <div class="input" id="cre_nome">
                                    <input style="width: 248px;" type="text" id="credor_nome" name="cre_nome" />
                                </div>
                            </div>

                            <div class="divleft" style="width: 231px;">
                                <div style="width: 76px; padding-left: 1px;" class="label">
                                    <label for="cre_cpf_cnpj">CNPJ/CPF:</label>
                                </div>
                                <div class="input" id="cre_cpf_cnpj">
                                    <input style="width: 124px;" type="text" id="credor_cpf_cnpj" name="cre_cpf_cnpj"/>
                                    <input type="hidden" id="cre_prazo" name="cre_prazo" value=""/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <button style="width: 85px; padding: 4px;" name="cre" type="button" class="dialog-form-open">Pesquisar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field" id="repasses">
                            <div class="divleftlast" style="width: 471px; margin-left: 0px;">
                                <div style="width: 89px;" class="label">
                                    <label for="rep_cod">Repasse:</label>
                                </div>
                                <div style="float:left;  border: none; width: 245px;">
                                    <div style="margin-left: 0px;" class="select" id="repassesCredor">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 12px; margin-bottom: 13px;" class="blocoTitulo">
                            Dados do inadimplente
                        </div>
                        <div id="hiddenInadimplente" style="display:none"></div>
                        <div class="field  field-first">
                            <div class="divleftleft" style="height:29px; width: 200px;">
                                <div class="label">
                                    <label>Pessoa:</label>
                                </div>
                                <input style="margin-top: 7px; " type="radio" name="ina_pessoa" value="f" />F�sica
                                <input type="radio" name="ina_pessoa" value="j">Jur�dica
                            </div>
                        </div>
                        <div class="field" id="dadosInadimplente">
                            <div class="divleft" style="width: 371px;">
                                <div style="width: 89px;" class="label">
                                    <label for="ina_nome">Nome:</label>
                                </div>
                                <div class="input" id="ina_nome">
                                    <input style="width: 248px;" type="text" id="inad_nome" name="ina_nome"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 231px;">
                                <div style="width: 76px; padding-left: 1px;" class="label">
                                    <label for="ina_cpf_cnpj">CPF/CNPJ:</label>
                                </div>
                                <div class="input" id="ina_cpf_cnpj">
                                    <input style="width: 124px;" type="text" id="inad_cpf_cnpj" name="ina_cpf_cnpj"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <button style="width: 85px; padding: 4px;" name="ina" type="button" class="dialog-form-open">Pesquisar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="dadosDivida">
                            <div style="margin-top: 12px; margin-bottom: 9px;" class="blocoTitulo">
                                Dados da d�vida
                            </div>
                            <div style="padding-top: 5px;" class="field">
                               <div style="margin-left: 6px;" class="select">
                               	    <label for="" style="display: block; font-weight: bold; margin-bottom: 3px;">Destino de inser��o:</label>
                                    <select style="width: 700px;" id="tipo_dest" name="destino">
                                    	<option value="" class="fix">[NOVA D�VIDA]</option>
                                    </select>
                                </div>
                            </div>
                            <div id="box_new_divida">
	                            <div style="padding-top: 5px;" class="field">
	                                <div class="divleft" style="width: 236px;">
	                                    <div style="margin-left: 6px;" class="select">
	                                        <select style="width: 222px;" id="tipo_doc" name="documento">
	                                            <option value="">[DOCUMENTO]</option>
	                                            <option value="BOLETO">BOLETO</option>
	                                            <option value="CARNE">CARNE</option>
	                                            <option value="CHEQUE">CHEQUE</option>
	                                            <option value="DUPLICATA">DUPLICATA</option>
	                                            <option value="VALE">VALE</option>
	                                            <option value="PROMISSORIA">NOTA PROMISSORIA</option>
	                                        </select>
	                                    </div>
	                                </div>
	                                <div class="divleftlast" style="width: 462px;">
	                                    <div>
	                                        <select style="width: 452px;" id="banco" name="banco">
	                                            <option value="">[BANCO]</option>
	                                            <?php foreach ($bancos as $banco): ?>
	                                                <option value="<?php echo $banco->banco_cod ?>"><?php echo utf8_decode($banco->banco_nome) ?></option>
	                                            <?php endforeach; ?>
	                                        </select>
	                                    </div>
	                                </div>
								</div>
	                            <div class="field" style="margin-bottom: 0px;">
	                                <div class="divleft" style="margin-left: 2px; width:140px;">
	                                    <div style="width: 26px; padding-left: 1px;" class="label">
	                                        <label for="agencia">AG.:</label>
	                                    </div>
	                                    <div class="input">
	                                        <input style="width: 75px;" type="text" maxlength="10" id="agencia" name="agencia"/>
	                                    </div>
	                                </div>
	                                <div class="divleft" style="margin-left: 2px; width:117px;">
	                                    <div style="width: 46px; padding-left: 1px;" class="label">
	                                        <label for="alinea">Al�nea:</label>
	                                    </div>
	                                    <div class="input">
	                                        <input style="width: 39px;" maxlength="10" type="text" id="alinea" name="alinea"/>
	                                    </div>
	                                </div>
	                                <div class="divleftlast" style="margin-left: 2px; width:176px;">
	                                    <div style="width: 62px; padding-left: 1px;" class="label">
	                                        <label for="emissao">Emiss�o:</label>
	                                    </div>
	                                    <div style=" width: 109px;" class="input">
	                                        <input type="text" id="emissao" name="emissao" class="data" />
	                                    </div>
	                                </div>
	                            </div>
                            </div>
                            <div class="field" style="margin-bottom: 0px;">
                                <div class="divleft" style="width: 162px;">
                                    <div style="width: 110px;" class="label">
                                        <label for="parcelas">Qtd de parcelas:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 19px;" type="text" maxlength="3" id="parcelas" name="qtd_parcela"/>
                                    </div>
                                </div>
                                <div class="divleft" style="margin-left: 2px; width:180px;">
                                    <div style="width: 58px; padding-left: 1px;" class="label">
                                        <label for="venc_inicial">Venc.:</label>
                                    </div>
                                    <div style=" width: 109px;" class="input">
                                        <input type="text" name="venc_inicial" style="width: 90px;" class="data"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 226px;">
                                    <div style="width: 97px; padding-left: 1px;" class="label">
                                        <label for="total">Parcela (R$):</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 99px;" type="text" id="total" name="valor_parc"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 114px;margin: 0px; margin-left: 1px;">
                                    <div class="buttons">
                                        <div class="highlight">
                                            <input style="width: 114px;" type="submit" id="btnGerar" name="gerar" value="Gerar parcelas" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="ParcGerada">
                            </div>
                            <div style="height: 74px; margin-top: 5px;" class="field">
                                <div class="divleftlast" style="width: 701px;">
                                    <div style="width: 84px; padding-top: 0px;" class="label">
                                        <label for="info">Outras informa��es:</label>
                                    </div>
                                    <div class="textarea">
                                        <textarea style="width: 586px; height: 50px;" id="info" name="info" cols="500" rows="4" class="editor"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 12px; margin-bottom: 3px; padding-bottom: 12px;" class="blocoTitulo recuperador">
                            Recuperador padr�o: <span id="recuperador_padrao"></span>
                        </div>
                        <div class="field recuperador">
                            <div style="margin-left: 221px;  border: none; width: 245px;">
                                <div style="margin-left: 6px;" class="select">
                                    <select style="width: 260px;" id="select_recuperador" name="recuperador">
                                        <option value="0">[SELECIONE UM RECUPERADOR]</option>
                                        <?php foreach ($usuarios as $usuario): ?>
                                            <option value="<?php echo $usuario->usu_cod; ?>"><?php echo $usuario->usu_usuario_sis; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <input type="reset" class="cancelar" name="cancelar" value="Cancelar" id="cancelar"/>
                            <div style="margin-left: 7px;" class="highlight" id="btnCadastrar">
                                <input type="submit" name="submit.highlight" value="Cadastrar" />
                            </div>
                            <div style="margin-left: 7px;" class="highlight">
                               <!-- <input type="submit" name="submit.highlight" value="Incluir mais" /> -->
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>