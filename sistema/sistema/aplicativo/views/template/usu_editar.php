<script type="text/javascript">
        $(document).ready(function(){
            $('#cep').mask('99.999-999');
            $('#cpf').mask('999.999.999-99');
            $('#celular').mask("(99)9999-9999?9").blur(function(){
			    var phone, element;
			    element = $(this);
			    element.unmask();
			    phone = element.val().replace(/\D/g, '');
			    if(phone.length > 10) {
			        element.mask("(99)99999-999?9");
			    } else {
			        element.mask("(99)9999-9999?9");
			    }
			    //element.unmask();
			});
            $('#hr_entrada').mask('99:99:99');
            $('#hr_saida').mask('99:99:99');
            
            $('.cadastrar').click(function(e){
                    e.preventDefault()
                    var senha;
                    var senha_rep;
                    senha=$('#senha').attr('value');
                    senha_rep=$('#senha_rep').attr('value');
                    if((senha==senha_rep)){
                        if($('input[name="nome"]').val() == ''){
                            $('input[name="nome"]').focus();
                            return false;
                        }else if($('input[name="usuario"]').val() == ''){
                            $('input[name="usuario"]').focus();
                            return false;
                        }else if($('input[name="hr_entrada"]').val() == ''){
                            $('input[name="hr_entrada"]').focus();
                            return false;
                        }else if($('input[name="hr_saida"]').val() == ''){
                            $('input[name="hr_saida"]').focus();
                            return false;
                        }
                        else {
//------------------------------------------------------------------------------
                            var CPF = $('#cpf').val(); // Recebe o valor digitado no campo
                            CPF = CPF.replace(".", "");
                            CPF = CPF.replace(".", "");
                            CPF = CPF.replace("-", "");

                            if (CPF != '') {
                                  var POSICAO, I, SOMA, DV, DV_INFORMADO;
                                    var DIGITO = new Array(10);
                                    DV_INFORMADO = CPF.substr(9, 2); // Retira os dois �ltimos d�gitos do n�mero informado
                                    // Aqui come�a a checagem do CPF

                                    // Desemembra o n�mero do CPF na array DIGITO
                                    for (I=0; I<=8; I++) {
                                      DIGITO[I] = CPF.substr( I, 1);
                                    }

                                    // Calcula o valor do 10� d�gito da verifica��o
                                    POSICAO = 10;
                                    SOMA = 0;
                                       for (I=0; I<=8; I++) {
                                          SOMA = SOMA + DIGITO[I] * POSICAO;
                                          POSICAO = POSICAO - 1;
                                       }
                                    DIGITO[9] = SOMA % 11;
                                       if (DIGITO[9] < 2) {
                                            DIGITO[9] = 0;
                                    }
                                       else{
                                           DIGITO[9] = 11 - DIGITO[9];
                                    }

                                    // Calcula o valor do 11� d�gito da verifica��o
                                    POSICAO = 11;
                                    SOMA = 0;
                                       for (I=0; I<=9; I++) {
                                          SOMA = SOMA + DIGITO[I] * POSICAO;
                                          POSICAO = POSICAO - 1;
                                       }
                                    DIGITO[10] = SOMA % 11;
                                       if (DIGITO[10] < 2) {
                                            DIGITO[10] = 0;
                                       }
                                       else {
                                            DIGITO[10] = 11 - DIGITO[10];
                                       }

                                       // Verifica se os valores dos d�gitos verificadores conferem
                                    DV = DIGITO[9] * 10 + DIGITO[10];
                                       if (DV != DV_INFORMADO) {
                                          alert('CPF inv�lido');
                                          $('#cpf').focus();
                                          return false;
                                       }
                                    }
//------------------------------------------------------------------------------
                            $('#formCad').submit();
                        }
                    }else{
                        alert('As senhas digitadas est�o diferentes, por favor confira');
                    }

            });

            
            
        });
</script>
<div id="content">
<?php echo $sidebar; ?>

    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Cadastro de usu�rio</h5>
            </div>
            <form id="formCad" action="<?php echo base_url().'usuarios/update'?>" method="post">
                <div class="form">
                    <div class="fields">
                        <div class="field  field-first">
                            <div class="divleft" style="width: 116px;">
                                <div class="label">
                                    <label>C�digo:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dados->usu_cod ?></p>
                            </div>
                            <div class="divleft" style=" margin-left: 4px; width: 173px;">
                                <div class="label">
                                    <label>Sexo:</label>
                                </div>
                                <input style="margin-top: 7px; " type="radio" name="sexo" value="m" <?php echo $dados->usu_sexo == 'm' ? 'checked' : ''; ?>>Masc.
                                <input type="radio" name="sexo" value="f" <?php echo $dados->usu_sexo == 'f' ? 'checked' : ''; ?>>Fem.
                            </div>
                            <div class="divleftlast" style="width: 184px;">
                                <div class="select">
                                    <select style="width: 186px; margin-left: 5px;" id="tipo_usuario" name="tipo_usuario">
                                        <?php foreach ($grupos as $grupo):;?>
                                        <option value="<?php echo $grupo->gru_cod;?>" <?php echo $grupo->gru_cod == $dados->gru_cod? 'selected':''; ?>><?php echo utf8_decode($grupo->gru_titulo);?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 465px;">
                                <div style="width: 89px;" class="label">
                                    <label for="nome">Nome:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 343px;" type="text" id="nome" maxlength="70" value="<?php echo utf8_decode($dados->usu_nome); ?>" name="nome"/>
                                    <input type="hidden" value="<?php echo $dados->usu_cod; ?>" name="cod"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 228px;">
                                <div style="width: 51px; padding-left: 1px; margin-right: 0px;" class="label">
                                    <label for="cpf">CPF:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 159px;" type="text" id="cpf" value="<?php echo $dados->usu_cpf; ?>" name="cpf"/>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 211px;">
                                <div style="width: 89px;" class="label">
                                    <label for="rg">RG:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 89px;" type="text" maxlength="19" id="rg" value="<?php echo $dados->usu_rg; ?>" name="rg"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 250px;">
                                <div style="width: 42px; padding-left: 1px;" class="label">
                                    <label for="endereco">End.:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 178px;" type="text" maxlength="110" id="endereco" name="endereco" value="<?php echo $dados->usu_endereco; ?>"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 233px; margin: 0px;">
                                <div style="width: 51px; padding-left: 1px;" class="label">
                                    <label for="bairro">Bairro:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 155px;" type="text" maxlength="40" id="bairro" name="bairro" value="<?php echo $dados->usu_bairro; ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 211px;">
                                <div style="width: 89px;" class="label">
                                    <label for="complemento">Compl.:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 89px;" type="text" maxlength="18" id="complemento" name="complemento" value="<?php echo $dados->usu_complemento; ?>"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 132px;">
                                <div style="width: 34px; padding-left: 1px;" class="label">
                                    <label for="cep">CEP.:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 65px;" type="text" id="cep" name="cep" value="<?php echo $dados->usu_cep; ?>"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 260px; margin: 0px;">
                                <div style="width: 59px; padding-left: 1px;" class="label">
                                    <label for="cidade">Cidade:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 170px;" maxlength="35" type="text" id="cidade" name="cidade" value="<?php echo utf8_decode($dados->usu_cidade); ?>"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 84px;">
                                <div class="select">
                                    <select style="width: 80px; margin-left: 7px;" id="estado" name="estado">
                                        <?php foreach ($estados as $estado):?>
                                        <option value="<?php echo $estado->cid_estado;?>" <?php echo $estado->cid_estado== $dados->usu_uf?'selected':'';?> ><?php echo $estado->cid_estado;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 240px;">
                                <div style="width: 89px;" class="label">
                                    <label for="telefone">Telefone:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 117px;" type="text" id="telefone" name="telefone" value="<?php echo $dados->usu_fone; ?>"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 238px;">
                                <div style="width: 62px; padding-left: 1px;" class="label">
                                    <label for="celular">Celular:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 139px;" type="text" id="celular" name="celular" value="<?php echo $dados->usu_celular; ?>"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 216px; margin: 0px;">
                                <div style="width: 59px; padding-left: 1px;" class="label">
                                    <label for="email">E-mail:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 130px;" maxlength="45" type="text" id="email" name="email" value="<?php echo $dados->usu_email; ?>"/>
                                </div>
                            </div>
                        </div>
                        <div style="height: 75px;" class="field">
                            <div class="divleftlast" style="width: 701px;">
                                <div style="width: 84px;" class="label">
                                    <label for="funcoes">Fun��es do usu�rio:</label>
                                </div>
                                <div class="textarea">
                                    <textarea style="width: 586px; height: 50px;" id="funcoes" name="funcoes" cols="500" rows="4" class="editor" > <?php echo utf8_decode($dados->usu_funcoes); ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="blocoTitulo" style="margin-top: 15px;">
                            Dados de acesso ao sistema
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 225px;">
                                <div style="width: 89px;" class="label">
                                    <label for="usuario">Usu�rio:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 100px; background-color: #F6F6F6;" readonly maxlength="15" type="text" id="usuario" name="usuario"  value="<?php echo $dados->usu_usuario_sis; ?>"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 195px;">
                                <div style="width: 84px; padding-left: 1px;" class="label">
                                    <label for="senha">Nova Senha:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 84px;" maxlength="15" type="password" id="senha" name="senha"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 247px; margin: 0px;">
                                <div style="width: 106px; padding-left: 1px;" class="label">
                                    <label for="senha_rep">Repita a senha:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 114px;" maxlength="15" type="password" id="senha_rep" name="senha_rep"/>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 282px;">
                                <div style="width: 119px;" class="label">
                                    <label for="hr_entrada">Hora de entrada:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 127px;" type="text" id="hr_entrada" name="hr_entrada" value="<?php echo $dados->usu_hr_sis_entrada;?>"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 282px; margin-left: 0px;">
                                <div style="width: 109px;" class="label">
                                    <label for="hr_saida">Hora de sa�da:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 127px;" type="text" id="hr_saida" name="hr_saida" value="<?php echo $dados->usu_hr_sis_saida;?>"/>
                                </div>
                            </div>
                        </div>
                        <div style="text-align: center; margin-top: 18px;" class="buttons">
                            <a href="<?php echo base_url().'usuarios/listar';?>" style="text-decoration: none;">
                                <input type="reset" name="cancelar" id="btnCancelar" value="Cancelar" />
                            </a>
                            
                            <div style="margin-left: 7px;" class="highlight">
                                <input type="submit" class="cadastrar" name="cadastrar" value="Alterar" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
