<script type="text/javascript">
    $(document).ready(function(){
        $('.excluir').click(function(){
            var resposta = confirm('Tem certeza de que deseja excluir essa mensagem?');
            return resposta // retorna um bool - Se verdadeiro executa a a��o original. Se falso cancela a a��o do click.
        });
        
        //MARCAR E DESMARCAR TODOS OS CHECKBOX
        $('#sel_todas').click(function(){
            
            if(this.checked == false){
                $('.ch_msgs').each(function(){
                    $(this).attr('checked','');
                });
            }
            else{
                $('.ch_msgs').each(function(){
                    if(this.checked == false){
                        $(this).attr('checked','checked');
                    }
                });
            }
        });
        
        //EXCLUIR MENSAGENS SELECIONADAS
        $('#bt_excluir_sel').click(function(){
            
            var selecao = 0;
            
            var msgs = '';
            
            //PARA CADA CHECKBOX
            $('.ch_msgs').each(function(){
                
                //SE O CHECKBOX ESTIVER SELECIONADO, INCREMENTA A VARI�VEL SELE��O E CONCATENA A VARI�VEL msgs
                if(this.checked){
                    //alert('Selecionado: '+$(this).attr('name'));
                    selecao = selecao + 1;
                    msgs = msgs + $(this).attr('name') + ',';
                }
            });
            
            if (selecao == 0){
                alert("Selecione ao menos uma mensagem");
            }
            else{
                
                //CONFIRMA��O
                var resposta = confirm('EXCLUIR AS '+selecao+' MENSAGENS SELECIONADAS? '+ msgs);

                if(resposta){
                    //SETA OS C�DIGOS DAS MENSAGENS NO INPUT HIDDEN
                    $('#msgs_selecionadas').val(msgs);
                    
                    //EXECUTA O ACTION DO FORM excluir_msgs_sel
                    $('#excluir_msgs_sel').submit();
                }
                else{
                    alert('Opera��o cancelada');
                }
            }
        });
       
    });
</script>

<form name="excluir_msgs_sel" action="excluirSelecionadas" method="post" id="excluir_msgs_sel">
    <input type="hidden" name="origem" value="entrada"/>
    <input type="hidden" name="msgs_selecionadas" id="msgs_selecionadas" value=""/>
</form>

<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Mensagens Recebidas</h5>
            </div>
            <?php echo $mensagem ?>
            <div id="dia">
                <div class="form">
                    <div class="fields">
                        <div class="field  field-first">
                            <div class="divleft" style="width: 245px;">
                                <div style="width: 235px; padding-left: 1px;" class="label">
                                    <label for="nome"><input type="checkbox" id="sel_todas"/>Selecionar/Desmarcar TODAS</label>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <input type="submit" name="excluir_selecionadas" id="bt_excluir_sel" value="Excluir selecionadas" />
                                    </div>
                                </div>                       
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="table" style="padding: 0px 5px 10px;">
                            <?php foreach ($minhasMensagensRecebidas as $caixaEntrada): ?>

                                <table style="margin-bottom: 15px;">
                                    <thead>
                                        <tr>
                                            <th class="left" <?php echo "style=\"background-color:" . tipoDeMensagem($caixaEntrada->men_tipo) . ";\"" ?>>
                                                <input type="checkbox" class="ch_msgs" name="<?php echo $caixaEntrada->men_cod; ?>" />   
                                                <?php echo $caixaEntrada->men_titulo; ?>
                                            </th>
                                            <th class="last" <?php echo "style=\"background-color:" . tipoDeMensagem($caixaEntrada->men_tipo) . "; width: 70px;\"" ?>>A��es</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="title" <?php echo $caixaEntrada->men_lida == '0' ? "style=\"background-color: #FFFFD1;\"" : ''; ?>>
                                                <?php echo utf8_decode($caixaEntrada->men_texto); ?>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; <?php echo $caixaEntrada->men_lida == '0' ? "background-color: #FFFFD1;" : ''; ?>" class="last">
                                                <a href="<?php echo 'excluirMensagem/codMensagem:' . $caixaEntrada->men_cod; ?>/origem:entrada" class="excluir">
                                                    <img src="<?php echo $img . 'excluir.png' ?>" alt="+"/>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="title" style="text-align: center; vertical-align: middle; <?php echo $caixaEntrada->men_lida == '0' ? "background-color: #FFFFD1;" : ''; ?>">
                                                <i><b>Autor:</b> <?php echo utf8_decode($caixaEntrada->men_remetente); ?> <?php if ($caixaEntrada->men_remetente != 'SISTEMA'): ?><a href="<?php echo 'responder/cod:' . $caixaEntrada->men_cod; ?>">(Responder)</a><?php endif; ?></i> | <i><b>Escrito em:</b> <?php echo convDataBanco($caixaEntrada->men_data); ?> <b>as:</b><?php echo substr($caixaEntrada->men_data, 10, -3); ?></i>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; <?php echo $caixaEntrada->men_lida == '0' ? "background-color: #FFFFD1;" : ''; ?>" class="last">
                                                <?php if ($caixaEntrada->men_lida == '0'): ?>
                                                    <a href="<?php echo 'marcarComoLida/codMensagem:' . $caixaEntrada->men_cod; ?>" >
                                                        Marcar como lida
                                                    </a>
                                                <?php endif; ?>
                                                <?php echo $caixaEntrada->men_lida == '0' ? '' : 'Mensagem lida'; ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            <?php endforeach; ?>
                            
	                        <div class="pagination pagination-left">
	                            <div class="results">
	                                <span>Total de Mensagens: <i><?php echo (int)$totalMensagensRecebidas; ?></i></span>
	                            </div>
	                            <div class="pager">
	                                <?php echo $paginacao; ?>
	                            </div>                            
	                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>