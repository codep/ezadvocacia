<script type="text/javascript">
    $(document).ready(function(){
        $('.detalhes').hide();
        $('input[name="pesquisa_detalhada_envia"]').hide();
        

        $('input[name="pesquisa_detalhada"]').click(function(e) {
           e.preventDefault();
            //EFEITO DE EXIBI��O
            $('.detalhes').fadeIn(function(){
                //EXIBE OS DADOS DO CREDOR
            });
            $('input[name="pesquisa_detalhada_envia"]').fadeIn(function(){
                //EXIBE OS DADOS DO CREDOR
            });
            $('input[name="pesquisar1"]').hide();
            $('input[name="pesquisa_detalhada"]').hide();
        });
        
    });
</script>
<div id="content">
        <?php echo $sidebar; ?>
    <div id="right">
        <div class="box" style="min-height: 800px;">
            <div class="title">
                <h5>Pesquisar credor</h5>
            </div>
            <form id="form" action="<?php echo base_url().'pesquisar/pescredres' ?>" method="post">
                <div class="form">
                    <?php echo $mensagem; ?>
                    <div class="fields">
                        <div class="field" style="height: 400px;">
                            <div class="divleft" style="border: none; width: 210px;">
                                <div style="margin-left: 6px;" class="select">
                                    <select style="width: 204px;" id="pesquisar_credor_tipo" name="filtro1" class="chosen">
                                        <option value="cre_razao_social">Raz�o social</option>
                                        <option value="cre_nome_fantasia">Nome fantasia</option>
                                        <option value="cre_cidade">Cidade</option>
                                        <option value="telefone">Telefone</option>
                                        <option value="cre_endereco">Endere�o</option>
                                    </select>
                                </div>
                            </div>
                            <div class="divleft" style="width: 394px;">
                                <div class="input">
                                    <input style="width: 370px;" type="text" id="pesquisar_credor_desc" name="filtro1_desc"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <input style="width: 85px;" type="submit" name="pesquisar1" value="Pesquisar" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="fields">
                        <div class="field detalhes">
                            <div class="divleft" style="border: none; width: 210px;">
                                <div style="margin-left: 6px;" class="select">
                                    <select style="width: 204px;" id="filtro2" name="filtro2">
                                        <option value="cre_razao_social">Raz�o social</option>
                                        <option value="cre_nome_fantasia">Nome fantasia</option>
                                        <option value="cre_cidade">Cidade</option>
                                        <option value="telefone">Telefone</option>
                                        <option value="cre_endereco">Endere�o</option>
                                    </select>
                                </div>
                            </div>
                            <div class="divleft" style="width: 394px;">
                                <div class="input">
                                    <input style="width: 370px;" type="text" id="filtro2_desc" name="filtro2_desc"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="field detalhes">
                            <div class="divleft" style="border: none; width: 210px;">
                                <div style="margin-left: 6px;" class="select">
                                    <select style="width: 204px;" id="filtro3" name="filtro3">
                                        <option value="cre_razao_social">Raz�o social</option>
                                        <option value="cre_nome_fantasia">Nome fantasia</option>
                                        <option value="cre_cidade">Cidade</option>
                                        <option value="telefone">Telefone</option>
                                        <option value="cre_endereco">Endere�o</option>
                                    </select>
                                </div>
                            </div>
                            <div class="divleft" style="width: 394px;">
                                <div class="input">
                                    <input style="width: 370px;" type="text" id="filtro3_desc" name="filtro3_desc"/>
                                </div>
                            </div>
                        </div>
                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <div style="margin-left: 7px;" class="highlight">
                                <input type="submit" name="pesquisa_detalhada" value="Pesquisa detalhada" />
                                <input type="submit" name="pesquisa_detalhada_envia" value="Pesquisar" />
                            </div>
                        </div>
                    </div>
                    -->
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
(function ($) {

	var cookieInputStatus = function(obj) {
		$.cookie($(obj).attr('id'),$(obj).val());
	};
	var cookieCheckboxStatus = function(obj) {
		var status = $(obj).is(':checked')?1:0;
		$.cookie($(obj).attr('id'),status);
	};
	var ckAllStatus = function() {
		$('#ckAll').attr('rel',function(){
			return ($('input[name^="tipoFiltro"]').length == $('input[name^="tipoFiltro"]:checked').length) ? 1 : 0;
		});
	}
	
	$(':checkbox').change(function() {
		cookieCheckboxStatus(this);
		//console.log($.cookie($(this).attr('id')));
	});
	$(':checkbox').each(function(){
		if ($.cookie($(this).attr('id'))==1) {
			$(this).attr('checked',true);
		} else {
			$(this).removeAttr('checked');
		}
	});
	
	$('#form .input input:not(:checkbox), select').change(function(){
		cookieInputStatus(this);
		//console.log($(this).val());
	});
	$('#form .input input:not(:checkbox), select').each(function(){
		var objVal = $.cookie($(this).attr('id'));
		if (objVal!=undefined) {
			$(this).val(objVal);
		}
		//console.log($(this).val());
	});
	

	$('#ckAll').click(function(){
		
		if ($(this).attr('rel')=='0') {
			$('input[name^="tipoFiltro"]').attr('checked',function(){
				this.checked = true;
				cookieCheckboxStatus(this);
			});
			$(this).attr('rel',1);	
		} else {
			$('input[name^="tipoFiltro"]').attr('checked',function(){
				this.checked = false;
				cookieCheckboxStatus(this);
			});
		}
		ckAllStatus();
	});
	ckAllStatus();
	
	$('select.chosen').chosen();
	
})(jQuery);
</script>