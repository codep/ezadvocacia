<script type="text/javascript">
    $(document).ready(function(){
 
        var base_url = "<?php echo base_url() ?>";
        $('.dataMascara').mask('99/99/9999');

        function ocultaTudo(){
            
            $('#pessoai08').hide();
            $('#dadosIna08').hide();
            
            for(var i = 1; i<=8; i++)
            {
                $('#dadosCredor0'+i).hide();
                $('#pessoa0'+i).hide();
                $('#pessoai07').hide();
                $('#periodo0'+i).hide();
                $('#btgerar0'+i).hide();
                $('#dadosIna07').hide();
                $('input[type="text"]').val('');
            }
        }
        
        //VALIDA��ES
        $('input[name="cpf_cnpj"]').attr('disabled',true);
        ocultaTudo();
        
        var entidade = '';
        var valor = '';
        var i = '';

        $('input[type="radio"]').change(function(e){
            e.preventDefault();
            //IDENTIFICA O RADIO PARA EVITAR PROCESSAMENTO DESNECESS�RIO OU CONFLITO DE VARI�VEIS
            if(($(this).attr('name').substring(0,10) == 'cre_filtro') || ($(this).attr('name').substring(0,10) == 'ina_filtro'))
            {
                entidade = $(this).attr('name');
                valor = $(this).val();
                //SE CLICADO EM PESQUISAR
                if((valor == 'credor_especifico') || (valor == 'ina_especifico'))
                {
                    i = $(this).attr('name').substring(10,12);
                    
                    if((valor != 'ina_especifico') && (valor != 'ina_todos')){
                        ocultaTudo();    
                    }
                    
                    if((valor != 'ina_especifico')){
                        
                        $('input[type="radio"]').each(function(){
                            if($(this).attr('name') != entidade)
                                this.checked = false;
                        });
                        
                        $('#pessoa'+i).fadeIn('fast', function(){
                            //$('#pessoa'+i).change(function(){
                        });
                    }else{
                        $('#pessoai'+i).fadeIn('fast', function(){
                            //$('#pessoa'+i).change(function(){
                        });
                    }
                    //SE CLICADO NO PESSOA F�SICA
                    $('input[name="cre_pessoa"][value="f"]').click(function(){

                        //LIMPANDO OS INPUTS TEXT
                        $('input[type="text"]').val('');

                        //RESETAR CPF/CNPJ
                        $('#cre_cpf_cnpj'+i).html('<input style="width: 124px;" id="credor_cpf_cnpj'+i+'" type="text" name="cpf_cnpj" />');

                        //APLICAR M�SCARA PARA CNPJ
                        $('#credor_cpf_cnpj'+i).attr('disabled',false).mask('999.999.999-99');
                        $('#credor_cpf_cnpj'+i).attr('value', '');

                        $('#dadosCredor'+i).fadeIn('fast');
                        $('#periodo'+i).fadeIn('fast');
                        $('#btgerar'+i).fadeIn('fast');
                    });
                    
                    
                    //SE CLICADO NO PESSOA JUR�DICA
                    $('input[name="cre_pessoa"][value="j"]').click(function(){

                        //LIMPANDO OS INPUTS TEXT
                        $('input[type="text"]').val('');

                        //RESETAR CPF/CNPJ
                        $('#cre_cpf_cnpj'+i).html('<input style="width: 124px;" id="credor_cpf_cnpj'+i+'" type="text" name="cpf_cnpj" />');

                        //APLICAR M�SCARA PARA CNPJ
                        $('#credor_cpf_cnpj'+i).attr('disabled',false).mask('99.999.999/9999-99');
                        $('#credor_cpf_cnpj'+i).attr('value', '');

                        $('#dadosCredor'+i).fadeIn('fast');
                        $('#periodo'+i).fadeIn('fast');
                        $('#btgerar'+i).fadeIn('fast');
                    });
                    
                    $('input[name="ina_pessoa07"][value="juridica"]').click(function(){
                        
                        //LIMPANDO OS INPUTS TEXT
                        //                        $('input[type="text"]').val('');

                        //RESETAR CPF/CNPJ
                        $('#ina_cpf_cnpj'+i).html('<input style="width: 124px;" id="inadimplente_cpf_cnpj'+i+'" type="text" name="ina_cpf_cnpj07" />');

                        //APLICAR M�SCARA PARA CNPJ
                        $('#inadimplente_cpf_cnpj'+i).attr('disabled',false).mask('99.999.999/9999-99');
                        $('#inadimplente_cpf_cnpj'+i).attr('value', '');

                        $('#dadosIna'+i).fadeIn('fast');
                        $('#periodo'+i).fadeIn('fast');
                        $('#btgerar'+i).fadeIn('fast');
                    });
                    
                    
                    $('input[name="ina_pessoa07"][value="juridica"]').click(function(){
                        
                        //LIMPANDO OS INPUTS TEXT
                        //                        $('input[type="text"]').val('');

                        //RESETAR CPF/CNPJ
                        $('#ina_cpf_cnpj'+i).html('<input style="width: 124px;" id="inadimplente_cpf_cnpj'+i+'" type="text" name="ina_cpf_cnpj07" />');

                        //APLICAR M�SCARA PARA CNPJ
                        $('#inadimplente_cpf_cnpj'+i).attr('disabled',false).mask('99.999.999/9999-99');
                        $('#inadimplente_cpf_cnpj'+i).attr('value', '');

                        $('#dadosIna'+i).fadeIn('fast');
                        $('#periodo'+i).fadeIn('fast');
                        $('#btgerar'+i).fadeIn('fast');
                    });
                    
                    $('input[name="ina_pessoa07"][value="fisica"]').click(function(){
                        
                        //RESETAR CPF/CNPJ
                        $('#ina_cpf_cnpj'+i).html('<input style="width: 124px;" id="inadimplente_cpf_cnpj'+i+'" type="text" name="ina_cpf_cnpj07" />');
                        //RESETAR CPF/CNPJ
                        $('#ina_cpf_cnpj'+i).html('<input style="width: 124px;" id="inadimplente_cpf_cnpj'+i+'" type="text" name="ina_cpf_cnpj08" />');

                        //APLICAR M�SCARA PARA CNPJ
                        $('#inadimplente_cpf_cnpj'+i).attr('disabled',false).mask('999.999.999-99');
                        $('#inadimplente_cpf_cnpj'+i).attr('value', '');

                        $('#dadosIna'+i).fadeIn('fast');
                        $('#periodo'+i).fadeIn('fast');
                        $('#btgerar'+i).fadeIn('fast');
                    });
                    
                    
                    $('input[name="ina_pessoa08"][value="juridica"]').click(function(){
                        
                        //LIMPANDO OS INPUTS TEXT
                        //                        $('input[type="text"]').val('');

                        //RESETAR CPF/CNPJ
                        $('#ina_cpf_cnpj'+i).html('<input style="width: 124px;" id="inadimplente_cpf_cnpj'+i+'" type="text" name="ina_cpf_cnpj08" />');

                        //APLICAR M�SCARA PARA CNPJ
                        $('#inadimplente_cpf_cnpj'+i).attr('disabled',false).mask('99.999.999/9999-99');
                        $('#inadimplente_cpf_cnpj'+i).attr('value', '');

                        $('#dadosIna'+i).fadeIn('fast');
                        $('#periodo'+i).fadeIn('fast');
                        $('#btgerar'+i).fadeIn('fast');
                    });
                    
                    
                    $('input[name="ina_pessoa08"][value="fisica"]').click(function(){
                        
                        //RESETAR CPF/CNPJ
                        $('#ina_cpf_cnpj'+i).html('<input style="width: 124px;" id="inadimplente_cpf_cnpj'+i+'" type="text" name="ina_cpf_cnpj08" />');

                        //APLICAR M�SCARA PARA CNPJ
                        $('#inadimplente_cpf_cnpj'+i).attr('disabled',false).mask('999.999.999-99');
                        $('#inadimplente_cpf_cnpj'+i).attr('value', '');

                        $('#dadosIna'+i).fadeIn('fast');
                        $('#periodo'+i).fadeIn('fast');
                        $('#btgerar'+i).fadeIn('fast');
                    });
                    

                    
                }
                else if(valor == 'todos')
                {
                    ocultaTudo();
                    i = $(this).attr('name').substring(10,12);
                    //DESMARCA OS RADIOS EXCETO O MARCADO ATUAL
                    $('input[type="radio"]').each(function(){
                        if($(this).attr('name') != entidade)
                            this.checked = false;
                    });

                    $('#periodo'+i).fadeIn('fast');
                    $('#btgerar'+i).fadeIn('fast');
                }else{
                    $('#dadosIna'+i).fadeOut('fast');
                    $('#pessoai'+i).fadeOut('fast');                    
                }
            }

        });

        //LISTANDO OS RESULTADOS DA PESQUISA
        /* *************************************************************************
         * PESQUISA INADIMPLETE OU CREDOR POR NOME OU CPF/CNPJ
         * pesquisaIC($entidade, $pessoa, $tipoDado, $valor)
         * $entidade -------- i=inadimplentes, c=credores
         * $pessoa ---------- j=jur�dica, f=f�sica
         * $tipoDado -------- n=nome, c=cpf/cnpj
         * $valor ----------- valor do nome ou cpf/cnpj
         *
         * NECESS�RIO CONFIGURAR O VALOR DOS ATRIBUTOS NAME DOS INPUTS
         ************************************************************************* */
        $(".dialog-form-open").click(function () {
        
            //�NDICE DO RELAT�RIO EM
            var indice = $(this).attr('name').substring(3,5);
            
            var aux1 = $(this).attr('name').substring(0,3);
            
         
            //ENTIDADE DO TIPO CREDOR
            if(aux1 == 'cre'){
                var entidade = 'c';
                var pessoa = '';
                //RECEBENDO O TIPO DE PESSOA [f ou j]
                $('input[name="cre_pessoa"]').each(function() {
                    if ( $(this).is(':checked') ) {
                        pessoa = $(this).attr('value');
                    }
                });
                
                //RECEBENDO O TIPO DE DADOS [credor_nome ou cpf_cnpj]
                var tipodado = '';

                //RECEBENDO O VALOR DIGITADO EM credor_nome ou cpf_cnpj
                var valor = '';
                if(($('#credor_nome'+indice).val() == '') && ($('#credor_cpf_cnpj'+indice).val() == '')){
                    $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Para pesquisar � necess�rio digitar o NOME ou CPF/CNPJ</h3></td></tr>');//zera os valores antigos e escreve no elemento o valor selecione
                    alert('Digite um valor');
                    return false;
                }
                else {
                    if ($('#credor_cpf_cnpj'+indice).val() != ''){
                        valor = $('input[name="cpf_cnpj'+indice+'"]').val();
                        tipodado = 'c';
                    }

                    else if ($('#credor_nome'+indice).val() != ''){
                        valor = $('#credor_nome'+indice).val();
                        tipodado = 'n';
                    }
                }
                
            }else{ // PESQUISA POR INADIMPLENTE
                var entidade = 'i';
                //RECEBENDO O TIPO DE PESSOA [f ou j]
                $('input[name="ina_pessoa07"]').each(function() {
                    if ( $(this).is(':checked') ) {
                        pessoa = $(this).attr('value');
                        if(pessoa == 'fisica'){
                            pessoa = 'f'
                        }else{
                            pessoa = 'j'
                        }
                    }
                });
                
                $('input[name="ina_pessoa08"]').each(function() {
                    if ( $(this).is(':checked') ) {
                        pessoa = $(this).attr('value');
                        if(pessoa == 'fisica'){
                            pessoa = 'f'
                        }else{
                            pessoa = 'j'
                        }
                    }
                });
                
                //RECEBENDO O TIPO DE DADOS [ina_nome ou ina_cpf]
                var tipodado = '';

                //RECEBENDO O VALOR DIGITADO EM credor_nome ou cpf_cnpj
                var valor = '';
                if(($('#inadimplente_nome'+indice).val() == '') && ($('#inadimplente_cpf_cnpj'+indice).val() == '')){
                    $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Para pesquisar � necess�rio digitar o NOME ou CPF/CNPJ</h3></td></tr>');//zera os valores antigos e escreve no elemento o valor selecione
                    alert('Digite um valor');
                    return false;
                }
                else {
                    if ($('#inadimplente_cpf_cnpj'+indice).val() != ''){
                        valor = $('input[name="ina_cpf_cnpj07"]').val();
                        tipodado = 'c';
                    }

                    else if ($('#inadimplente_nome'+indice).val() != ''){
                        valor = $('#inadimplente_nome'+indice).val();
                        tipodado = 'n';
                    }
                }
                
            }
            //FUN��O AJAX
            $.ajax({
                type: "POST",//TIPO DE DADOS
                url: base_url+'credor/listar_pesquisa',//FUN��O DO CONTROLLER PARA TRATAR A LISTAGEM
                data: 'entidade='+entidade+'&pessoa='+pessoa+'&tipodado='+tipodado+'&valor='+valor,//DADOS ENVIADOS
                dataType: 'json',//TIPO DE DADOS A SER RECEBIDO

                //CASO HAJA ERRO DE PROCESSAMENTO
                error: function(xhr, status, er) {
                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                },

                //CASO A FUN��O DO CONTROLLER SEJA EXECUTADA COM SUCESSO E OS DADOS RECEBIDOS COM SUCESSO
                success: function(retorno)
                {
                    $("#dialog-form").dialog("open");///ABRE A CAIXA DE DI�LOGO
                    $('#dadosPesquisa').html('');//zera os valores antigos e escreve no elemento o valor selecione
                    vazio = false;//VARI�VEL PARA IDENTIFICAR SE ALGUM DADO �NTEGRO TENHA SIDO RECEBIDO

                    $.each(retorno.resultado, function(i, v){//para cada �ndice retornado executa a fun��o
                        if(v.nome != null){
                            //CONCATENA AS LINHAS DA TABELA COM OS VALORES RECEBIDOS
                            $('#dadosPesquisa').append(
                            '<tr class="tuplas" id="'+unescape(v.codigo)+'">'+
                                '<td>'+unescape(v.nome)+
                                '<td>'+unescape(v.cpf_cnpj)+
                                '<td>'+unescape(v.endereco)+
                                '<td class="last">'+unescape(v.cidade)+
                                '</tr>'
                        );
                        }
                        else vazio = true;//CASO OS DADOS RETORNADOS SEJAM NULOS
                    });
                    $('.tuplas').css('cursor','pointer');//SETA O TIPO DO CURSOR

                    //AO CLICAR SOBRE UMA LINHA DA TABALEA COM A CLASSE TUPLA
                    $('.tuplas').click(function(e){
                        e.preventDefault();
                        codigo = $(this).attr('id');
                        if(aux1 == 'cre'){
                            //                         link = base_url+'credor/editar/cod:'+codigo;
                            //                         window.open(link, 'blank');
                            $('input[name="cre_cod'+indice+'"]').val(codigo);

                            //ACRESCENTA O VALOR DO NOME DO PRIMEIRO FILHO (TD) DA TR CLICADA
                            $('#credor_nome'+indice).val($(this).find(":eq(0)").text());

                            //ACRESCENTA O VALOR DO CPF_CNPJ DO SEGUNDO FILHO (TD) DA TR CLICADA
                            $('#credor_cpf_cnpj'+indice).val($(this).find(":eq(1)").text())

                            $('.ui-dialog-titlebar-close').trigger('click');
                        }else{
                            //                         link = base_url+'credor/editar/cod:'+codigo;
                            //                         window.open(link, 'blank');
                            $('input[name="ina_cod'+indice+'"]').val(codigo);

                            //ACRESCENTA O VALOR DO NOME DO PRIMEIRO FILHO (TD) DA TR CLICADA
                            $('#inadimplente_nome'+indice).val($(this).find(":eq(0)").text());

                            //ACRESCENTA O VALOR DO CPF_CNPJ DO SEGUNDO FILHO (TD) DA TR CLICADA
                            $('#inadimplente_cpf_cnpj'+indice).val($(this).find(":eq(1)").text())

                            $('.ui-dialog-titlebar-close').trigger('click');
                        }
                    });

                    //CASO OS DADOS RETORNADOS SEJAM NULOS
                    if (vazio)
                    {   //EXIBE MENSAGEM
                        $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Nenhum dado encontrado pesquisando por ['+valor+']</h3></td></tr>');
                    }
                }
            });
        });

    });
</script>
<div id="dialog-form" style="padding-left: 5px; padding-right: 5px; " title="Pesquisar dados">
    <p style="text-align: center">
        <strong>Dados encontrados:</strong>
    </p>
    <div id="content" style="width: 750px;">
        <div id="right" style="margin-left: 0px;">
            <div class="box">
                <div class="table" style="padding-left: 0px; font-size: 10px; width: 750px;">
                    <form action="" method="post">
                        <table id="products">
                            <thead>
                                <tr>
                                    <th class="left" style="width: 20%;">Nome</th>
                                    <th style="width: 14%;">CPF/CNPJ</th>
                                    <th>ENDERE�O</th>
                                    <th class="last">CIDADE</th>
                                </tr>
                            </thead>
                            <tbody id="dadosPesquisa">
                                <!--RESULTADO DA PESQUISA AQUI!! -->
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Relat�rios de Resultados de contatos</h5>
            </div>
            <?php echo $mensagem ?>
            <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                Relat�rio de Resultados de contatos
            </div>
            <form id="form08" action="<?php echo base_url() . 'relatorios/rel_contat01';?>"  method="post" target="_blank">
                <div class="form">
                    <div class="fields">
                        <div class="field  field-first" style="margin-top:6px;">
                            <div class="divleft" style="height:29px; width: 210px;">
                                <div class="label">
                                    <label>Recuperador:</label>
                                </div>
                                <select name="recuperador08">
                                    <option value="todos" selected>Todos</option>
                                    <?php foreach ($recuperadores as $recuperador): ?>
                                        <option value="<?php echo $recuperador->usu_cod; ?>"><?php echo $recuperador->usu_usuario_sis; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="divleft" style="height:29px; width: 210px;">
                                <div class="label">
                                    <label>Credor:</label>
                                </div>
                                <input type="radio" name="cre_filtro08" value="todos" style="margin-top: 7px; "/>Todos
                                <input type="radio" name="cre_filtro08" value="credor_especifico" />Pesquisar
                            </div>
                            <div class="divleftlast" style="height:29px; width: 200px;" id="pessoa08">
                                <div class="label">
                                    <label>Pessoa:</label>
                                </div>
                                <input type="radio" name="cre_pessoa" value="f" style="margin-top: 7px;"/>F�sica
                                <input type="radio" name="cre_pessoa" value="j" />Jur�dica
                            </div>
                        </div>
                        <div class="field" id="dadosCredor08">
                            <div class="divleft" style="width: 371px;">
                                <div style="width: 49px;" class="label">
                                    <label for="cre_nome">Nome:</label>
                                </div>
                                <div class="input" id="cre_nome08">
                                    <input style="width: 288px;" type="text" id="credor_nome08" name="cre_nome08" />
                                    <input type="hidden" id="credor_cod08" name="cre_cod08" style="width: 288px;" />
                                </div>
                            </div>
                            <div class="divleft" style="width: 231px;">
                                <div style="width: 76px; padding-left: 1px;" class="label">
                                    <label for="cre_cpf_cnpj">CNPJ/CPF:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 124px;" type="text" id="credor_cpf_cnpj08" name="cpf_cnpj08"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <button style="width: 85px; padding: 4px;" name="cre08" type="button" class="dialog-form-open">Pesquisar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field  field-first" style="margin-top:6px;">
<!--                            <div class="divleft" style="height:29px; width: 210px;">
                                <div class="label">
                                    <label>Tipo Opera��o:</label>
                                </div>
                                <select name="tipoOperacao08">
                                    <option value="todos" selected>Todos</option>
                                    <?php foreach ($recuperadores as $recuperador): ?>
                                        <option value="<?php echo $recuperador->usu_cod; ?>"><?php echo $recuperador->usu_usuario_sis; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>-->
                            <div class="divleft" style="height:29px; width: 250px;">
                                <div class="label">
                                    <label>Inadimplente:</label>
                                </div>
                                <input type="radio" name="ina_filtro08" value="ina_todos" style="margin-top: 7px; "/>Todos
                                <input type="radio" name="ina_filtro08" value="ina_especifico" />Pesquisar
                            </div>
                            <div class="divleftlast" style="height:29px; width: 200px;" id="pessoai08">
                                <div class="label">
                                    <label>Pessoa:</label>
                                </div>
                                <input type="radio" name="ina_pessoa08" value="fisica" style="margin-top: 7px;"/>F�sica
                                <input type="radio" name="ina_pessoa08" value="juridica" />Jur�dica
                            </div>
                        </div>
                        <div class="field" id="dadosIna08">
                            <div class="divleft" style="width: 371px;">
                                <div style="width: 49px;" class="label">
                                    <label for="ina_nome">Nome:</label>
                                </div>
                                <div class="input" id="ina_nome08">
                                    <input style="width: 288px;" type="text" id="inadimplente_nome08" name="ina_nome08" />
                                    <input type="hidden" id="ina_cod08" name="ina_cod08" style="width: 288px;" />
                                </div>
                            </div>
                            <div class="divleft" style="width: 231px;">
                                <div style="width: 76px; padding-left: 1px;" class="label">
                                    <label for="ina_cpf_cnpj">CNPJ/CPF:</label>
                                </div>
                                <div class="input" id="ina_cpf_cnpj08">
                                    
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <button style="width: 85px; padding: 4px;" name="ina08" type="button" class="dialog-form-open">Pesquisar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field" id="periodo08">
                            <div class="divleft" style="margin-left: 2px; width:150px;">
                                <div style="width: 28px; padding-left: 1px;" class="label">
                                    <label for="de">De:</label>
                                </div>
                                <div style=" width: 109px;" class="input">
                                    <input type="text"  name="de08" class="dataMascara"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="margin-left: 2px; width:150px;">
                                <div style="width: 28px; padding-left: 1px;" class="label">
                                    <label for="ate">At�:</label>
                                </div>
                                <div style=" width: 109px;" class="input">
                                    <input type="text"  name="ate08" class="dataMascara"/>
                                </div>
                            </div>
                        </div>
                        <div class="field  field-first" style="margin-top:6px;">
                            <div class="divleftlast" style="height:29px; width: 575px;">
                                <div class="label">
                                    <label>Tipo Opera��o:</label>
                                </div>
                                <select name="tipoOperacao08" style="width: 300px;">
                                    <option value="0" selected>Todos</option>
                                    <?php foreach ($operacoes as $operacao): ?>
                                        <option value="<?php echo $operacao->ope_cod; ?>"><?php echo utf8_decode($operacao->ope_nome); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <?php if (sizeof($recuperadores) > 0): ?>
                            <div class="field" id="btgerar08">
                                <div class="divleftlast" style="text-align: center">
                                    <div class="buttons" style="width: 700px">
                                        <div class="highlight" style=" margin: 0 auto">
                                            <input style="width: 185px; margin: 0 auto" type="submit" name="filtrar_rel_adm08" value="Gerar relat�rio"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>