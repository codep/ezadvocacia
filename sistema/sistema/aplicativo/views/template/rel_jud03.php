<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img.'logo_elisangela.jpg';?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - Telefone: 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    cobranca@ezcobrancas.com.br|www.ezcobrancas.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo $title ;?></p>
        </div>
    </div>
    <table id="table03" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="th1">Inadimplente</th>
                <th id="th2">Credor</th>
                <th id="th3">Tipo</th>
                <th id="th4">Data �ltimo RO</th>
                <th id="th5" class="last">�ltimo RO</th>
            </tr>
        </thead>
        <tbody>
            <?php $totalDeInad = 0; foreach ($cobsJudOciosas as $cobJudOciosa) : ?>
            <tr>
           
                <td id="td1"><?php echo utf8_decode($cobJudOciosa->ina_nome); ?></td>
                <td id="td2"><?php echo utf8_decode($cobJudOciosa->cre_nome_fantasia); ?></td>
                <td id="td3"><?php echo utf8_decode($cobJudOciosa->tipo); ?></td>
                <td id="td4">
                            <?php
                              $dataUltRo = convDataBanco($cobJudOciosa->data_ult_ro);
                              echo ($dataUltRo == "//") ? "" : $dataUltRo; //se a data vier vazia o m�todo convDataBanco retorna s� as barra, se isso acontecer imprime nada
                              echo("<br/>");
                              echo $cobJudOciosa->hora_ult_ro;
                            ?><br/>
                </td>
                <td id="td5" class="last"><?php echo utf8_decode($cobJudOciosa->ult_ros_detalhe); ?></td>
                <?php $totalDeInad ++; endforeach; ?>
            </tr>
            <tr>
                <td colspan="4" id="total">TOTAL DE COBRAN�AS EM ATRASO</td>
                <td id="td7" class="last"><?php echo "$totalDeInad"; ?></td>
            </tr>

        </tbody>
    </table>
</div>
