<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img.'logo_elisangela.jpg';?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br|www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo $title ;?></p>
        </div>
    </div>
    <table id="tableAdm03" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="thCodCobranca">Cod Cobran�a</th>
                <th id="thInadimplente">Inadimplente</th>
                <th id="thCredor">Credor</th>
                <th id="th3">�ltimo RO</th>
                <th id="th4">Data �ltimo RO</th>
                <th id="th5" class="last">Fones</th>
            </tr>
        </thead>
        <tbody>
            <?php $totalDeCob = 0; foreach ( $cobsSemAcords as $cobSemAcord ) : ?>
            <tr>
           
                <td class="tdCodCobranca"><?php echo $cobSemAcord->cob_cod;?></td>
                <td class="tdInaNome"><?php echo utf8_decode($cobSemAcord->ina_nome); ?></td>
                <td class="tdCreNomeFantasia"><?php echo utf8_decode($cobSemAcord->cre_nome_fantasia); ?></td>

                <td class ="tdUltimoRo" id="td3">
                  <?php
                    echo"<strong>";
                      echo utf8_decode($cobSemAcord->ultimo_ro_operacao); echo "<br/>";
                    echo "</strong>";
                    echo utf8_decode($cobSemAcord->ultimo_ro_detalhe);
                  ?>
                </td>

                <td class="td4">
                    <?php
                      echo"<strong>";
                        $data = convDataBanco($cobSemAcord->ultimo_ro_data);
                      echo ($data == "//") ? "" : $data; echo "<br/>";
                      echo "</strong>";
                      echo $cobSemAcord->ultimo_ro_hora;
                    ?>
                </td>
                
                <td class="last" class="td5">
                      <?php
                        echo $cobSemAcord->ina_fonecom; echo "<br/>";
                        echo $cobSemAcord->ina_cel1;
                      ?>
                </td>
                <?php $totalDeCob ++; endforeach; ?>
            </tr>
            <tr>
                <td id="tdTotalA" colspan="5">TOTAL DE COBRAN�AS</td><!-- SERIA O TD 6 -->
                <td id="tdTotalB" class="last"><?php echo "$totalDeCob"; ?></td>
            </tr>
        </tbody>
    </table>
</div>
