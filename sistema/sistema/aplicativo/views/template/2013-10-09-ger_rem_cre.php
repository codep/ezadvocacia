<script type="text/javascript">
    $(document).ready(function(){
        $('select[id="rec_padrao_cod"]').change(function(){
            rec_padrao_nome = $('select[id="rec_padrao_cod"] option:selected').text();
            $('#rec_padrao_nome').val(rec_padrao_nome);
            //                window.location.href="";
        });
        $('select[id="rec_novo_cod"]').change(function(){
            rec_novo_nome = $('select[id="rec_novo_cod"] option:selected').text();
            $('#rec_novo_nome').val(rec_novo_nome);
            //                window.location.href="";
        });
        $('#remanejar').click(function(e){

            valida = confirm('Voc� tem certeza de que quer remanejar este credor?');
            if(!valida) return false;
        });
    });
</script>

<div id="content">

    <?php echo $sidebar; ?>

    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Remanejar credores</h5>
            </div>
            <?php echo $mensagem ?>
            <form method="post" action="<?php echo base_url() . 'carteira/listar' ?>" id="form_remanejar">
                <div class="form">
                    <div class="fields">
                        <div class="field  field-first">

                            <div class="divleft" style="width: 406px;">
                                <div style="width: 150px; padding-left: 1px;" class="label">
                                    <label for="nome">Recuperador padr�o:</label>
                                </div>
                                <div class="select">
                                    <select style="width: 245px; margin-left: 5px;" id="rec_padrao_cod" name="rec_padrao_cod">
                                        <option value="0">[selecione um recuperador]</option>
                                        <?php foreach ($usuarios as $usu): ?>
                                            <option value="<?php echo $usu->usu_cod; ?>" <?php echo $usu->usu_cod == $usu_filtro ? 'selected' : ''; ?>><?php echo $usu->usu_usuario_sis; ?><?php echo $usu->usu_ativo == '0'?' (DESATIVADO)':''; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <input type="hidden" name="rec_padrao_nome" id="rec_padrao_nome" value="<?php echo $usu_nome; ?>"/>
                                        <button style="width: 85px;" type="submit" id="filtrar" name="filtrar">Filtrar</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <?php if (sizeof($credores) == 0 && $mensagem == ''): ?>
                            <h2 style="text-align: center">Selecione um recuperador para filtrar seus credores</h2>
                        <?php endif; ?>

                        <?php if (sizeof($credores) != 0): ?>
                            <div class="table" style="padding: 0px 0px 10px;">

                                <table id="products">
                                    <thead>
                                        <tr>
                                            <th>Credor</th>
                                            <th class="last" style="width: 15px;">Selecionar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($credores as $credor): ?>
                                            <tr>
                                                <td>
                                                    <?php echo $credor->cre_nome_fantasia; ?>
                                                </td>
                                                <td class="last" style="width: 15px;">
                                                    <input type="checkbox" name="credores_selecionados[]" value="<?php echo $credor->cre_cod; ?>"/>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="field" style="border-top: 1px solid #ddd ">
                                <div class="divleft" style="width: 406px;">
                                    <div style="width: 150px; padding-left: 1px;" class="label">
                                        <label for="nome">Trocar recuperador:</label>
                                    </div>
                                    <div class="select">
                                        <select style="width: 245px; margin-left: 5px;" id="rec_novo_cod" name="rec_novo_cod">
                                            <?php foreach ($usuarios as $usu): ?>
                                                <option value="<?php echo $usu->usu_cod; ?>"><?php echo $usu->usu_usuario_sis; ?><?php echo $usu->usu_ativo == '0'?' (DESATIVADO)':''; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 87px; margin: 0px;">
                                    <div class="buttons">
                                        <div class="highlight">
                                            <input type="hidden" name="rec_novo_nome" id="rec_novo_nome" />
                                            <input style="width: 105px;" type="submit" name="remanejar" value="Remanejar" id="remanejar" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
