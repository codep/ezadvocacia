<html>
    <body>
        <div id="content">

        <?php echo $sidebar; ?>

            <div id="right">
                <div class="box">
                    <div class="title">
                        <h5>Cadastro de bens</h5>
                    </div>
                    <form id="form" action="" method="post">
                        <div class="form">
                            <div class="fields">
                                <div class="blocoTitulo" style="margin-top: 3px;">
                                    Dados do inadimplente
                                </div>
                                <div class="field  field-first">
                                    <div class="divleftlast" style="width: 400px; margin-left: 0px;">
                                        <div style="width: 85px;" class="label">
                                            <label>Pessoa:</label>
                                        </div>
                                        <input style="margin-top: 7px; " type="radio" name="pessoa" value="fisica" checked>F�sica
                                        <input type="radio" name="pessoa" value="juridica">Jur�dica
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="width: 371px;">
                                        <div style="width: 89px;" class="label">
                                            <label for="nome">Nome:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 248px;" type="text" id="nome" name="nome"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 231px;">
                                        <div style="width: 76px; padding-left: 1px;" class="label">
                                            <label for="cpf_cnpj">CPF/CNPJ:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 124px;" type="text" id="cpf_cnpj" name="cpf_cnpj"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 87px; margin: 0px;">
                                        <div class="buttons">
                                            <div class="highlight">
                                                <input style="width: 85px;" type="submit" name="pesquisar" value="Pesquisar" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="blocoTitulo" style="margin-top: 18px;">
                                    Dados do bem
                                </div>
                                <div class="field  field-first">
                                    <div class="divleftlast" style="width: 400px; margin-left: 0px;">
                                        <div style="width: 85px;" class="label">
                                            <label>Tipo:</label>
                                        </div>
                                        <input style="margin-top: 7px; " type="radio" name="tipo" value="imovel" checked>Im�vel
                                        <input type="radio" name="tipo" value="imovel">Ve�culo
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleftlast" style="width: 712px; margin-left: 0px;">
                                        <div style="width: 89px;" class="label">
                                            <label for="titulo">Titulo:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 598px;" type="text" id="titulo" name="titulo"/>
                                        </div>
                                    </div>
                                </div>
                                <div style="height: 125px;" class="field">
                                    <div class="divleftlast" style="width: 701px;">
                                        <div style="width: 84px;" class="label">
                                            <label for="detalhes">Detalhes:</label>
                                        </div>
                                        <div class="textarea">
                                            <textarea style="width: 586px; height: 100px;" id="detalhes" name="detalhes" cols="500" rows="5"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="blocoTitulo">
                                    Hist�rico de pesquisas
                                </div>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left">Data</th>
                                                <th style="width: 205px">Registro de Opera��es</th>
                                                <th class="last">Recuperador</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="title">01/01/2011</td>
                                                <td>Pesquisa Realizada no Ciretran</td>
                                                <td class="last">Anderson</td>
                                            </tr>
                                            <tr>
                                                <td class="title">05/01/2011</td>
                                                <td>Pesquisa em Cart�rio</td>
                                                <td  class="last">Diego</td>
                                            </tr>
                                            <tr>
                                                <td class="title">10/01/2011</td>
                                                <td>Pesquisa Realizada no Ciretran</td>
                                                <td class="last">Leonardo</td>
                                            </tr>
                                            <tr>
                                                <td class="title">05/02/2011</td>
                                                <td>Pesquisa em Cart�rio</td>
                                                <td class="last">Anderson</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                                <div style="text-align: center; margin-top: 10px;" class="buttons">
                                    <input type="reset" name="cancelar" value="Cancelar" />
                                    <div style="margin-left: 7px;" class="highlight">
                                        <input type="submit" name="cadastrar" value="Cadastrar" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>