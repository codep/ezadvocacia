<script type="text/javascript">
    $(document).ready(function(){
        $('#descricao').maxLength(500,'divCount');
    });
</script>
<html>
    <body>
        <div id="content">

        <?php echo $sidebar; ?>
             <div id="right">
                <div class="box" style="min-height: 798px;">
                    <div class="title">
                        <h5>Nova tarefa</h5>
                    </div>
                    <?php echo $mensagem; ?>
                    <form id="form_novatarefa" action="novaTarefa" method="post">
                        <div class="form">
                            <div class="fields">
                                <div class="field">
                                    <div class="divleft" style="width: 211px;">
                                        <div style="width: 89px;" class="label">
                                            <label for="data">Data:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 69px;" type="text" id="data" name="data" class="date" readonly/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 260px; margin: 0px;">
                                        <div style="width: 49px; padding-left: 1px;" class="label">
                                            <label for="titulo">T�tulo:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 180px;" type="text" maxlength="150" id="titulo" name="titulo"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 223px;">
                                        <div style="width: 95px; padding-left: 1px;" class="label">
                                            <label for="responsavel">Respons�vel:</label>
                                        </div>
                                        <div class="select">
                                            <select style="width: 120px; margin-left: 7px;" id="responsavel" name="responsavel">
                                              <?php  foreach ($usuUsuarioSis as $responsavel):?>
                                                <option value="<?php echo $responsavel->usu_cod; ?>" <?php echo strtoupper($responsavel->usu_usuario_sis) == strtoupper($usuSessao) ? 'selected' : ''; ?> > <?php echo utf8_decode($responsavel->usu_usuario_sis); ?></option>
                                                <?php endforeach;?>
                                            </select>
                                       </div>
                                    </div>
                                </div>
                                <div style="height: 75px;" class="field">
                                    <div class="divleftlast" style="width: 701px;">
                                        <div style="width: 84px;" class="label">
                                            <label for="descricao">Descri��o:</label>
                                        </div>
                                        <div class="textarea">
                                            <textarea style="width: 586px; height: 50px;" id="descricao" name="descricao" cols="500" rows="4"></textarea>
                                            
                                        </div>
                                        <div class="divCount" style="margin-left: 92px; font-weight: bold;"></div>
                                    </div>
                                </div>
                                <div style="text-align: center; float: right; margin-top: 10px;" class="buttons">
                                    <input type="reset" name="cancelar" value="Limpar" />
                                    <div style="margin-left: 7px;" class="highlight">
                                        <input type="submit" name="incluir" value="Incluir" />
                                     </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>