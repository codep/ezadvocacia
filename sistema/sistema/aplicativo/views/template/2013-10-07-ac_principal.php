<script type="text/javascript">
    $(document).ready(function(){
        
        $('#qtd_parcelas').bind("keyup blur focus", function(e) {
            e.preventDefault();
            var expre = /[A-Za-z\.\�\�\@\`\�\^\~\'\"\!\?\#\$\%\�\�\_\+\=\.\,\:\;\<\>\|\�\�\�\]\[\{\}\\ \)\(\*\&\-\/\\]/g;
 
            // REMOVE OS CARACTERES DA EXPRESSAO ACIMA
            if ($(this).val().match(expre))
                $(this).val($(this).val().replace(expre,''));
        });
        
      
        $('#valor_original, #pri_emissao, #dias_atraso, #juros_original, #multa_original, #juros_total, #multa_total, #honorario_original, #honorario_total').css('background-color', '#f6f6f6')
        $('#juros_atual, #multa_atual, #honorario_atual').mask('99.99');
        $('#custas, #desconto, #juros_total,#valor_desejado,#entrada').priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });
        
        var valor_juros_escolhido = 0.0;
        
        function fn_val_atual(juros, multa, honorarios){
            var valor_original = $('#valor_original').val();
            var dias_atraso = $('#dias_atraso').val();

            $('#juros_taxa_escolhido').val(juros);
            $('#multa_taxa_escolhido').val(multa);
            $('#honorario_taxa_escolhido').val(honorarios);

            valor_original = (valor_original.replace('.','')).replace(',','.');
            
            juros = (((juros/100) / 30)*dias_atraso)*valor_original;
            
            multa = (multa/100)*valor_original;
            
            honorarios = (honorarios/100)*valor_original;
            
            $('#juros_total').val(juros.toFixed(2));
            $('#multa_total').val(multa.toFixed(2));
            $('#honorario_total').val(honorarios.toFixed(2));
            
        }
        
        $('#valores_atualizados, #bloco_parcelamento, #bloco_divida_atualizada, #parcela_desejada, #qtde_parcelas, #parcelas, #botoes_acoes, #recaulcularParcelas, #recalcularQtde, #total_parcelado_bloco').hide();
        
        $('#juros_atual, #multa_atual, #honorario_atual').focus(function(){
            $('#valores_atualizados, #bloco_parcelamento, #bloco_divida_atualizada').fadeOut();
            $('#parcelas').hide();
            $('#parcelas').html('');
            $('#total_parcelado_bloco').hide();
            $('#botoes_acoes').hide();
        });
        
        $('#custas, #desconto').focus(function(){
            $('#bloco_parcelamento, #bloco_divida_atualizada').fadeOut();
            $('#parcelas').hide();
            $('#parcelas').html('');
            $('#total_parcelado_bloco').hide();
            $('#botoes_acoes').hide();
        });
        
        $('#bt_atualizar_originais').click(function(e){
            e.preventDefault();
            $('#valores_atualizados').fadeIn();
            $('#bloco_parcelamento, #bloco_divida_atualizada').fadeOut();
            fn_val_atual($('#juros_original').val(), $('#multa_original').val(), $('#honorario_original').val());
            $('#juros_total, #multa_total, #honorario_total').priceFormat({
                prefix: "",
                centsSeparator: ",",
                thousandsSeparator: "."
            });
            
            valor_juros_escolhido = $('#juros_original').val();
            
            $('#desconto').val('0.00');
            $('#custas').val('0.00');
        });
        
        $('#bt_atualizar_atualizados').click(function(e){
            e.preventDefault();
            $('#valores_atualizados').fadeIn();
            $('#bloco_parcelamento, #bloco_divida_atualizada').fadeOut();
            fn_val_atual($('#juros_atual').val(), $('#multa_atual').val(), $('#honorario_atual').val());
            $('#juros_total, #multa_total, #honorario_total').priceFormat({
                prefix: "",
                centsSeparator: ",",
                thousandsSeparator: "."
            });
            
            valor_juros_escolhido = $('#juros_atual').val();
            
            if(($('#juros_atual').val()) == ''){
                $('#juros_atual').val('0.00');
            }
            if(($('#multa_atual').val()) == ''){
                $('#multa_atual').val('0.00');
            }
            if(($('#honorario_atual').val()) == ''){
                $('#honorario_atual').val('0.00');
            }
            
            $('#desconto').val('0.00');
            $('#custas').val('0.00');
        });
        
        $('#bt_atualizar_total').click(function(e){
            e.preventDefault();
            
            var valor_original = (($('#valor_original').val()).replace('.','')).replace(',','.');
            var juros_total = (($('#juros_total').val()).replace('.','')).replace(',','.');
            var multa_total = (($('#multa_total').val()).replace('.','')).replace(',','.');
            var honorario_total = (($('#honorario_total').val()).replace('.','')).replace(',','.');
            var custas = (($('#custas').val()).replace('.','')).replace(',','.');
            var desconto = (($('#desconto').val()).replace('.','')).replace(',','.');
            
            var total_divida_atualizada = ((parseFloat(juros_total) + parseFloat(multa_total) + parseFloat(honorario_total) + parseFloat(custas)) - parseFloat(desconto)) + parseFloat(valor_original);
            
            if(parseFloat(desconto) < parseFloat(valor_original)){
                $('#total_divida_atualizada').val(total_divida_atualizada.toFixed(2));
                $('#total_divida_atualizada').priceFormat({
                    prefix: "",
                    centsSeparator: ",",
                    thousandsSeparator: "."
                });
                $('#p_total_divida_atualizada').html('R$ '+$('#total_divida_atualizada').val());
                $('#bloco_parcelamento').fadeIn();
                $('#bloco_divida_atualizada').fadeIn();
            }
            else
                alert('Desconto excede o valor original');
            
        });
        
        $('input[name="tipo_calculo"]').click(function(){
            if($(this).val() == '1'){
                $('#qtde_parcelas').fadeOut();
                $('#parcela_desejada').fadeIn();
                $('#total_parcelado_bloco').hide();
                $('#parcelas').html('');
                $('#parcelas').hide();
                $('#botoes_acoes').hide();
            }else if($(this).val() =='2'){
                $('#parcela_desejada').fadeOut();
                $('#qtde_parcelas').fadeIn();
                $('#total_parcelado_bloco').hide();
                $('#parcelas').html('');
                $('#parcelas').hide();
                $('#botoes_acoes').hide();
            }else{
                alert('Op��o incorreta!')
            }
        });
        
        $('#recaulcularParcelas').click(function(e){
            e.preventDefault();
            $('#parcelas').hide();
            $('#parcelas').html('');
            $('#recaulcularParcelas').hide();
            $('#gerarParcelasDesejadas').fadeIn('slow');
            $('#total_parcelado_bloco').hide();
            $('#botoes_acoes').hide();
        });
        
        $('#recalcularQtde').click(function(e){
            e.preventDefault();
            $('#parcelas').hide();
            $('#parcelas').html('');
            $('#recalcularQtde').hide();
            $('#gerarParcelasQtde').fadeIn('slow');
            $('#total_parcelado_bloco').hide();
            $('#botoes_acoes').hide();
        });
        var total_parcelas = 0;
        //==============================================================================        
        //=================== GERAR PARCELAS COM O VALOR DESEJADO ======================
        //==============================================================================
        $('#gerarParcelasDesejadas').click(function(e){
            
            e.preventDefault();
            
            if((($('#valor_desejado').val()) == '') || (($('#valor_desejado').val()) == '0,00')){
                alert('Por favor preencha o valor desejado!');
                $('#valor_desejado').focus();
                return false;
            }else if(($('#pri_venc_desejado').val()) == ''){
                alert('Por favor preencha a primeira data da parcela!');
                $('#pri_venc_desejado').focus();
                return false;
            }else{
                var valor_desejado = (($('#valor_desejado').val()).replace('.','')).replace(',','.');
                var pri_vencimento = $('#pri_venc_desejado').val();
                juros = ((parseFloat(valor_juros_escolhido))/100);
                valor_atual = valor_original = (($('#total_divida_atualizada').val()).replace('.','')).replace(',','.');
                valor_total_parcelado = 0.0;
                var aux = '';

                if($('#juros_mensais').is(':checked')){
                
                    $.ajax({
                        type: "POST",
                        contentType: "application/x-www-form-urlencoded;charset=ISO-8859-15",
                        url: "<?php echo base_url() . 'acordo/calculoParcelas'; ?>",
                        data: 'juros='+juros+'&valorTotal='+valor_atual+'&parcela='+valor_desejado,
                        dataType: 'json',
                        success: function(retorno)
                        {
                            aux = retorno.status+'';
                            splited = aux.split('.');
                        
                            total_parcelas = aux = splited[0];
                            auxResto = splited[1];
                        
                            var dia = pri_vencimento;
                            dia = dia.substr(0,2);
                            var mes = pri_vencimento;
                            mes = mes.substr(3,2);
                            var ano = pri_vencimento;
                            ano = ano.substr(6,4);
            
                            var parnum = 1;
                            var mesAux = '';
            
                            for ( var i = 0 ; i < aux ; i++ )
                            {
                                if (mes>12){
                                    ano++;
                                    mes=1;
                                }
                                if ((mes<10)&&(i>0)){
                                    mes='0'+mes;
                                }
                                if((mes == '02') && (dia == '29' || dia == '30' || dia == '31'))
                                {
                                    mesAux = dia;
                                    dia = '28';
                                }
                                if((dia == '31') && (mes == '04' || mes == '06' || mes == '09' || mes == '11'))
                                {
                                    mesAux = dia;
                                    dia = '30';
                                }
                
                                valor_total_parcelado = parseFloat(valor_total_parcelado);
                                $('#parcelas').append(
                                '<div class="field fieldParc">'+
                                    '<div class="divleft" style="width: 161px;">'+
                                    '<div style="width: 75px; padding-left: 5px;" class="label">'+
                                    '<label style="font-weight: normal;" for="num_par_1">N� parcela:</label>'+
                                    '</div>'+
                                    '<div class="input">'+
                                    '<input maxlength="3" style="width: 51px; text-align: center;" type="text" id="num_par_1" name="par_num[]" class="par_num" value="'+parnum+'"/>'+
                                    '</div>'+
                                    '</div>'+
                                    '<div class="divleft" style="width: 328px; margin-left: 1px;">'+
                                    '<div style="width: 143px; padding-left: 0px;" class="label">'+
                                    '<label style="font-weight: normal;" for="val_par_1">Valor da parcela (R$):</label>'+
                                    '</div>'+
                                    '<div class="input">'+
                                    '<input style="width: 155px;" class="val_par" type="text" name="val_par[]" value="'+valor_desejado+'"/>'+
                                    '</div>'+
                                    '</div>'+
                                    '<div class="divleftlast" style="width: 198px; margin-left: 2px;">'+
                                    '<div style="width: 82px; padding-left: 0px;" class="label">'+
                                    '<label style="font-weight: normal;" for="venc_par_1">Vencimento:</label>'+
                                    '</div>'+
                                    '<div style=" width: 111px;" class="input">'+
                                    '<input type="text" id="venc_par_1" name="venc_par[]" class="date data_parc" value="'+dia+'/'+mes+'/'+ano+'"/>'+
                                    '</div>'+
                                    '</div>'+
                                    '</div>'
                            );
                                parnum++;
                                if(mesAux != '') dia = mesAux;
                                mes++;
                                valor_total_parcelado += (parseFloat(valor_desejado));
                            }
            
                            //           >>>>>>>>>>>>>>>>>>>>>> PARCELAS FRACIONADAS <<<<<<<<<<<<<<<<<<<<<<
                            if(auxResto != null){
                
                                auxResto = '0.'+auxResto;
                                auxResto = parseFloat(auxResto);
                
                                if (mes>12){
                                    ano++;
                                    mes=1;
                                }
                                if ((mes<10)&&(i>0)){
                                    mes='0'+mes;
                                }
                                if((mes == '02') && (dia == '29' || dia == '30' || dia == '31'))
                                {
                                    mesAux = dia;
                                    dia = '28';
                                }
                                if((dia == '31') && (mes == '04' || mes == '06' || mes == '09' || mes == '11'))
                                {
                                    mesAux = dia;
                                    dia = '30';
                                }
                
                                valor_desejado = (valor_desejado*auxResto);
                                valor_desejado = valor_desejado.toFixed(2);
                
                                valor_total_parcelado = parseFloat(valor_total_parcelado);
                                $('#parcelas').append(
                                '<div class="field fieldParc">'+
                                    '<div class="divleft" style="width: 161px;">'+
                                    '<div style="width: 75px; padding-left: 5px;" class="label">'+
                                    '<label style="font-weight: normal;" for="num_par_1">N� parcela:</label>'+
                                    '</div>'+
                                    '<div class="input">'+
                                    '<input maxlength="3" style="width: 51px; text-align: center;" type="text" id="num_par_1" name="par_num[]" class="par_num" value="'+parnum+'"/>'+
                                    '</div>'+
                                    '</div>'+
                                    '<div class="divleft" style="width: 328px; margin-left: 1px;">'+
                                    '<div style="width: 143px; padding-left: 0px;" class="label">'+
                                    '<label style="font-weight: normal;" for="val_par_1">Valor da parcela (R$):</label>'+
                                    '</div>'+
                                    '<div class="input">'+
                                    '<input style="width: 155px;" class="val_par" type="text" name="val_par[]" value="'+valor_desejado+'"/>'+
                                    '</div>'+
                                    '</div>'+
                                    '<div class="divleftlast" style="width: 198px; margin-left: 2px;">'+
                                    '<div style="width: 82px; padding-left: 0px;" class="label">'+
                                    '<label style="font-weight: normal;" for="venc_par_1">Vencimento:</label>'+
                                    '</div>'+
                                    '<div style=" width: 111px;" class="input">'+
                                    '<input type="text" id="venc_par_1" name="venc_par[]" class="date data_parc" value="'+dia+'/'+mes+'/'+ano+'"/>'+
                                    '</div>'+
                                    '</div>'+
                                    '</div>'
                            );
                                parnum++;
                                if(mesAux != '') dia = mesAux;
                                mes++;
                                total_parcelas++;
                                valor_total_parcelado += (parseFloat(valor_desejado));
                            }
            
                            $('.date').mask('99/99/9999');
                            $('.val_par').priceFormat({
                                prefix: "",
                                centsSeparator: ",",
                                thousandsSeparator: "."
                            });
                            $('#parcelas').fadeIn();
                            $('#gerarParcelasDesejadas').hide();
                            $('#recaulcularParcelas').fadeIn();
                            $('#total_parcelado_bloco').fadeIn();
            
                            $('#total_valor_parcela').val(valor_total_parcelado.toFixed(2));
                            $('#total_valor_parcela').priceFormat({
                                prefix: "",
                                centsSeparator: ",",
                                thousandsSeparator: "."
                            });
                            $('#p_total_valor_parcela').html('R$ '+$('#total_valor_parcela').val());
            
                            valor_original = (($('#total_divida_atualizada').val()).replace('.','')).replace(',','.');
            
                            valor_total_juros = parseFloat(valor_total_parcelado) - parseFloat(valor_original);
            
                            $('#total_juros_parcela').val(valor_total_juros.toFixed(2));
                            $('#total_juros_parcela').priceFormat({
                                prefix: "",
                                centsSeparator: ",",
                                thousandsSeparator: "."
                            });
                            $('#p_total_juros_parcela').html('R$ '+$('#total_juros_parcela').val());
                            $('#numero_total_parcelas').val(total_parcelas);
                            $('#botoes_acoes').fadeIn();
                            
                            $('.par_num').bind("keyup blur focus", function(e) {
                                e.preventDefault();
                                var expre = /[A-Za-z\.\�\�\@\`\�\^\~\'\"\!\?\#\$\%\�\�\_\+\=\.\,\:\;\<\>\|\�\�\�\]\[\{\}\\ \)\(\*\&\-\/\\]/g;
 
                                // REMOVE OS CARACTERES DA EXPRESSAO ACIMA
                                if ($(this).val().match(expre))
                                    $(this).val($(this).val().replace(expre,''));
                                
                                if ($(this).val() == '')
                                    $(this).val('1');
                            });
                        }
                    });
                    
                
                }else{
                    aux = valor_original/valor_desejado;
                
                    aux = aux+'';
                
                    splited = aux.split('.');
                
                    total_parcelas = aux = splited[0];
                    auxResto = splited[1];
                
                    var dia = pri_vencimento;
                    dia = dia.substr(0,2);
                    var mes = pri_vencimento;
                    mes = mes.substr(3,2);
                    var ano = pri_vencimento;
                    ano = ano.substr(6,4);
            
                    var parnum = 1;
                    var mesAux = '';
            
                    for ( var i = 0 ; i < aux ; i++ )
                    {
                        if (mes>12){
                            ano++;
                            mes=1;
                        }
                        if ((mes<10)&&(i>0)){
                            mes='0'+mes;
                        }
                        if((mes == '02') && (dia == '29' || dia == '30' || dia == '31'))
                        {
                            mesAux = dia;
                            dia = '28';
                        }
                        if((dia == '31') && (mes == '04' || mes == '06' || mes == '09' || mes == '11'))
                        {
                            mesAux = dia;
                            dia = '30';
                        }
                
                        valor_total_parcelado = parseFloat(valor_total_parcelado);
                        $('#parcelas').append(
                        '<div class="field fieldParc">'+
                            '<div class="divleft" style="width: 161px;">'+
                            '<div style="width: 75px; padding-left: 5px;" class="label">'+
                            '<label style="font-weight: normal;" for="num_par_1">N� parcela:</label>'+
                            '</div>'+
                            '<div class="input">'+
                            '<input maxlength="3" style="width: 51px;  text-align: center;" type="text" id="num_par_1" name="par_num[]" class="par_num" value="'+parnum+'"/>'+
                            '</div>'+
                            '</div>'+
                            '<div class="divleft" style="width: 328px; margin-left: 1px;">'+
                            '<div style="width: 143px; padding-left: 0px;" class="label">'+
                            '<label style="font-weight: normal;" for="val_par_1">Valor da parcela (R$):</label>'+
                            '</div>'+
                            '<div class="input">'+
                            '<input style="width: 155px;" class="val_par" type="text" name="val_par[]" value="'+valor_desejado+'"/>'+
                            '</div>'+
                            '</div>'+
                            '<div class="divleftlast" style="width: 198px; margin-left: 2px;">'+
                            '<div style="width: 82px; padding-left: 0px;" class="label">'+
                            '<label style="font-weight: normal;" for="venc_par_1">Vencimento:</label>'+
                            '</div>'+
                            '<div style=" width: 111px;" class="input">'+
                            '<input type="text" id="venc_par_1" name="venc_par[]" class="date data_parc" value="'+dia+'/'+mes+'/'+ano+'"/>'+
                            '</div>'+
                            '</div>'+
                            '</div>'
                    );
                        parnum++;
                        if(mesAux != '') dia = mesAux;
                        mes++;
                        valor_total_parcelado += (parseFloat(valor_desejado));
                    }
            
                    //           >>>>>>>>>>>>>>>>>>>>>> PARCELAS FRACIONADAS <<<<<<<<<<<<<<<<<<<<<<
                    if(auxResto != null){
                
                        auxResto = '0.'+auxResto;
                        auxResto = parseFloat(auxResto);
                
                        if (mes>12){
                            ano++;
                            mes=1;
                        }
                        if ((mes<10)&&(i>0)){
                            mes='0'+mes;
                        }
                        if((mes == '02') && (dia == '29' || dia == '30' || dia == '31'))
                        {
                            mesAux = dia;
                            dia = '28';
                        }
                        if((dia == '31') && (mes == '04' || mes == '06' || mes == '09' || mes == '11'))
                        {
                            mesAux = dia;
                            dia = '30';
                        }
                
                        valor_desejado = (valor_desejado*auxResto);
                        valor_desejado = valor_desejado.toFixed(2);
                
                        valor_total_parcelado = parseFloat(valor_total_parcelado);
                        $('#parcelas').append(
                        '<div class="field fieldParc">'+
                            '<div class="divleft" style="width: 161px;">'+
                            '<div style="width: 75px; padding-left: 5px;" class="label">'+
                            '<label style="font-weight: normal;" for="num_par_1">N� parcela:</label>'+
                            '</div>'+
                            '<div class="input">'+
                            '<input maxlength="3" style="width: 51px; text-align: center;" type="text" id="num_par_1" name="par_num[]" class="par_num" value="'+parnum+'"/>'+
                            '</div>'+
                            '</div>'+
                            '<div class="divleft" style="width: 328px; margin-left: 1px;">'+
                            '<div style="width: 143px; padding-left: 0px;" class="label">'+
                            '<label style="font-weight: normal;" for="val_par_1">Valor da parcela (R$):</label>'+
                            '</div>'+
                            '<div class="input">'+
                            '<input style="width: 155px;" class="val_par" type="text" name="val_par[]" value="'+valor_desejado+'"/>'+
                            '</div>'+
                            '</div>'+
                            '<div class="divleftlast" style="width: 198px; margin-left: 2px;">'+
                            '<div style="width: 82px; padding-left: 0px;" class="label">'+
                            '<label style="font-weight: normal;" for="venc_par_1">Vencimento:</label>'+
                            '</div>'+
                            '<div style=" width: 111px;" class="input">'+
                            '<input type="text" id="venc_par_1" name="venc_par[]" class="date data_parc" value="'+dia+'/'+mes+'/'+ano+'"/>'+
                            '</div>'+
                            '</div>'+
                            '</div>'
                    );
                        parnum++;
                        if(mesAux != '') dia = mesAux;
                        mes++;
                        total_parcelas++;
                        valor_total_parcelado += (parseFloat(valor_desejado));
                    }
            
                    $('.date').mask('99/99/9999');
                    $('.val_par').priceFormat({
                        prefix: "",
                        centsSeparator: ",",
                        thousandsSeparator: "."
                    });
                    $('#parcelas').fadeIn();
                    $('#gerarParcelasDesejadas').hide();
                    $('#recaulcularParcelas').fadeIn();
                    $('#total_parcelado_bloco').fadeIn();
            
                    $('#total_valor_parcela').val(valor_total_parcelado.toFixed(2));
                    $('#total_valor_parcela').priceFormat({
                        prefix: "",
                        centsSeparator: ",",
                        thousandsSeparator: "."
                    });
                    $('#p_total_valor_parcela').html('R$ '+$('#total_valor_parcela').val());
            
                    valor_original = (($('#total_divida_atualizada').val()).replace('.','')).replace(',','.');
            
                    valor_total_juros = parseFloat(valor_total_parcelado) - parseFloat(valor_original);
            
                    $('#total_juros_parcela').val(valor_total_juros.toFixed(2));
                    $('#total_juros_parcela').priceFormat({
                        prefix: "",
                        centsSeparator: ",",
                        thousandsSeparator: "."
                    });
                    $('#p_total_juros_parcela').html('R$ '+$('#total_juros_parcela').val());
                    $('#numero_total_parcelas').val(total_parcelas);
                    $('#botoes_acoes').fadeIn();
                    
                    $('.par_num').bind("keyup blur focus", function(e) {
                        e.preventDefault();
                        var expre = /[A-Za-z\.\�\�\@\`\�\^\~\'\"\!\?\#\$\%\�\�\_\+\=\.\,\:\;\<\>\|\�\�\�\]\[\{\}\\ \)\(\*\&\-\/\\]/g;
 
                        // REMOVE OS CARACTERES DA EXPRESSAO ACIMA
                        if ($(this).val().match(expre))
                            $(this).val($(this).val().replace(expre,''));
                                
                        if ($(this).val() == '')
                            $(this).val('1');
                    });
                    
                }
            }
            $('.val_par').bind("blur", function(e) {
                var valor = $(this).attr('value');
                var total = parseFloat(0);
                        
                $('.val_par').each(function(){
                    var aux = $(this).attr('value').replace('.','');
                    aux = aux.replace(',','.');
                    total = parseFloat(total) + parseFloat(aux);
                });
                total = total.toFixed(2);
                $('#total_valor_parcela').val(total);
                $('#total_valor_parcela').priceFormat({
                    prefix: "",
                    centsSeparator: ",",
                    thousandsSeparator: "."
                });
                precoLegivel = "R$ "+ $('#total_valor_parcela').val();
                $('#p_total_valor_parcela').html(precoLegivel);
            });
        });
        //==============================================================================
        //==============================================================================
        //==============================================================================
        
        //==============================================================================        
        //======================= GERAR PARCELAS PELA QUANTIDADE =======================
        //==============================================================================
        $('#gerarParcelasQtde').click(function(e){
            e.preventDefault();
            
            if((($('#qtd_parcelas').val()) == '') || (($('#qtd_parcelas').val()) == 0)){
                alert('Por favor preencha aquantidade de parcelas desejadas!');
                $('#qtd_parcelas').focus();
                return false;
            }else if(($('#pri_venc_qtd').val()) == ''){
                alert('Por favor selecione a data do primeiro vencimento das parcelas');
                $('#pri_venc_qtd').focus();
                return false;
            }else{
                
                var qtdeParcelas = $('#qtd_parcelas').val();
                pri_vencimento = $('#pri_venc_qtd').val();
                valor_atual = valor_original = (($('#total_divida_atualizada').val()).replace('.','')).replace(',','.');
                valor_total_parcelado = 0.0;
                total_parcelas = qtdeParcelas;
                if($('#juros_mensais').is(':checked')){
                    juros_habilitados = 1;
                }else{
                    juros_habilitados = 0;
                }
            
            
            
                if(juros_habilitados == 1){
                    e_var = 1.0;
                    juros = ((parseFloat(valor_juros_escolhido))/100);
                    cont = 1.0;
                
                    for(k=1;k<=qtdeParcelas;k++){
                        cont    = (cont*(1+juros));
                        e_var   = e_var+cont;
                    }
                    e_var   = e_var-cont;
                    valor_atual = (parseFloat(valor_atual)*cont);
                
                    var valor_parc_atual = (valor_atual / e_var);
                }else{
                    var valor_parc_atual = (valor_atual / qtdeParcelas);    
                }
            
                valor_parc_atual = valor_parc_atual.toFixed(2);
            
                aux = qtdeParcelas;
            
                var dia = pri_vencimento;
                dia = dia.substr(0,2);
                var mes = pri_vencimento;
                mes = mes.substr(3,2);
                var ano = pri_vencimento;
                ano = ano.substr(6,4);
            
                var parnum = 1;
                var mesAux = '';
            
                for ( var i = 0 ; i < aux ; i++ )
                {
                    if (mes>12){
                        ano++;
                        mes=1;
                    }
                    if ((mes<10)&&(i>0)){
                        mes='0'+mes;
                    }
                    if((mes == '02') && (dia == '29' || dia == '30' || dia == '31'))
                    {
                        mesAux = dia;
                        dia = '28';
                    }
                    if((dia == '31') && (mes == '04' || mes == '06' || mes == '09' || mes == '11'))
                    {
                        mesAux = dia;
                        dia = '30';
                    }
                
                    $('#parcelas').append(
                    '<div class="field fieldParc">'+
                        '<div class="divleft" style="width: 161px;">'+
                        '<div style="width: 75px; padding-left: 5px;" class="label">'+
                        '<label style="font-weight: normal;" for="num_par_1">N� parcela:</label>'+
                        '</div>'+
                        '<div class="input">'+
                        '<input maxlength="3" style="width: 51px; text-align: center;" type="text" id="num_par_1" name="par_num[]" class="par_num" value="'+parnum+'"/>'+
                        '</div>'+
                        '</div>'+
                        '<div class="divleft" style="width: 328px; margin-left: 1px;">'+
                        '<div style="width: 143px; padding-left: 0px;" class="label">'+
                        '<label style="font-weight: normal;" for="val_par_1">Valor da parcela (R$):</label>'+
                        '</div>'+
                        '<div class="input">'+
                        '<input style="width: 155px;" class="val_par" type="text" name="val_par[]" value="'+valor_parc_atual+'"/>'+
                        '</div>'+
                        '</div>'+
                        '<div class="divleftlast" style="width: 198px; margin-left: 2px;">'+
                        '<div style="width: 82px; padding-left: 0px;" class="label">'+
                        '<label style="font-weight: normal;" for="venc_par_1">Vencimento:</label>'+
                        '</div>'+
                        '<div style=" width: 111px;" class="input">'+
                        '<input type="text" id="venc_par_1" name="venc_par[]" class="date data_parc" value="'+dia+'/'+mes+'/'+ano+'"/>'+
                        '</div>'+
                        '</div>'+
                        '</div>'
                );
                    parnum++;
                    if(mesAux != '') dia = mesAux;
                    mes++;
                    valor_total_parcelado += (parseFloat(valor_parc_atual));
                }
                $('.date').mask('99/99/9999');
                $('.val_par').priceFormat({
                    prefix: "",
                    centsSeparator: ",",
                    thousandsSeparator: "."
                });
                $('#parcelas').fadeIn();
                $('#gerarParcelasQtde').hide();
                $('#recalcularQtde').fadeIn();
                $('#total_parcelado_bloco').fadeIn();
            
                $('#total_valor_parcela').val(valor_total_parcelado.toFixed(2));
                $('#total_valor_parcela').priceFormat({
                    prefix: "",
                    centsSeparator: ",",
                    thousandsSeparator: "."
                });
                $('#p_total_valor_parcela').html('R$ '+$('#total_valor_parcela').val());
            
                valor_original = (($('#total_divida_atualizada').val()).replace('.','')).replace(',','.');
            
                valor_total_juros = parseFloat(valor_total_parcelado) - parseFloat(valor_original);
            
                $('#total_juros_parcela').val(valor_total_juros.toFixed(2));
                $('#total_juros_parcela').priceFormat({
                    prefix: "",
                    centsSeparator: ",",
                    thousandsSeparator: "."
                });
                $('#p_total_juros_parcela').html('R$ '+$('#total_juros_parcela').val());
                $('#numero_total_parcelas').val(total_parcelas);
                $('#botoes_acoes').fadeIn();
                
                $('.par_num').bind("keyup blur focus", function(e) {
                    e.preventDefault();
                    var expre = /[A-Za-z\.\�\�\@\`\�\^\~\'\"\!\?\#\$\%\�\�\_\+\=\.\,\:\;\<\>\|\�\�\�\]\[\{\}\\ \)\(\*\&\-\/\\]/g;
 
                    // REMOVE OS CARACTERES DA EXPRESSAO ACIMA
                    if ($(this).val().match(expre))
                        $(this).val($(this).val().replace(expre,''));
                                
                    if ($(this).val() == '')
                        $(this).val('1');
                });
                
            }
            $('.val_par').bind("blur", function(e) {
                var valor = $(this).attr('value');
                var total = parseFloat(0);
                        
                $('.val_par').each(function(){
                    var aux = $(this).attr('value').replace('.','');
                    aux = aux.replace(',','.');
                    total = parseFloat(total) + parseFloat(aux);
                });
                total = total.toFixed(2);
                $('#total_valor_parcela').val(total);
                $('#total_valor_parcela').priceFormat({
                    prefix: "",
                    centsSeparator: ",",
                    thousandsSeparator: "."
                });
                precoLegivel = "R$ "+ $('#total_valor_parcela').val();
                $('#p_total_valor_parcela').html(precoLegivel);
            });
        });
        //==============================================================================
        //==============================================================================
        //==============================================================================
        $('#btnEnviaAcordo').click(function(e){
            e.preventDefault();
            
            $('.par_num').each(function() { 
                if($(this).val() == ''){
                    $(this).focus();
                    return false;
                }
            });
            
            $('.val_par').each(function() { 
                if(($(this).val() == '0,00') || ($(this).val() == '')){
                    $(this).focus();
                    return false;
                }
            });
            
            $('.data_parc').each(function() { 
                if($(this).val() == ''){
                    $(this).focus();
                    return false;
                }
            });
            $('#btnEnviaAcordo').hide();
            $('#formAcordo').submit();
        });
        // Atualizar parcelas quando muda o valor:
    });
</script>
<!-- < ?php
$n = 0.0;
$i = 0.1223; //0.06
$pv = 996.51; //10713.12
$pmt = 130.00; //800
$x = 1 + $i;

$aux = (1 / (1 - (($pv * $i) / $pmt)));

$n = log($aux, $x);

echo 'O Valor de N � = ' . $n;
?> -->
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Gerar acordo</h5>
            </div>
            <form id="formAcordo" action="<?php echo base_url() . 'acordo/salvarAcordo'; ?>" method="post">
                <div class="form">
                    <div class="fields">
                        <div style="margin-top: 0px; margin-bottom: 13px;" class="blocoTitulo">
                            Tipo de acordo
                        </div>
                        <div class="field  field-first">
                            <div class="divleftleft" style="height:29px; width: 420px;">
                                <div class="label">
                                    <label>Tipo de acordo:</label>
                                </div>
                                <input style="margin-top: 7px; " type="radio" name="acordo_tipo" value="1" checked />Acordo Firmado
                                <input type="radio" name="acordo_tipo" value="2" />Previs�o de pagamento
                            </div>
                        </div>
                        <div style="margin-top: 12px; margin-bottom: 2px;" class="blocoTitulo">
                            C�lculo da d�vida
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 315px;">
                                <div style="width: 135px;" class="label">
                                    <label for="valor_original">Valor original (R$):</label>
                                </div>
                                <div class="input">
                                    <input style="width: 145px; font-weight: bold" type="text" id="valor_original" name="valor_original" value="<?php echo number_format($valTotal, 2, ',', '.'); ?>" readonly/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 205px; margin-right: 4px;">
                                <div style="width: 83px; padding-left: 1px;" class="label">
                                    <label for="pri_emissao">1� Emiss�o:</label>
                                </div>
                                <div style=" width: 109px;" class="input">
                                    <input type="text" id="pri_emissao" name="pri_emissao" value="<?php echo convData($vencInicial, 'd') ?>" style=" width: 93px;" readonly/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 175px; margin-left: 0px;">
                                <div style="width: 108px;" class="label">
                                    <label for="dias_atraso">Dias em atraso:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 39px;" type="text" id="dias_atraso" name="dias_atraso" value="<?php echo diffDate($vencInicial, date('Y-m-d'), 'D') ?>" readonly/>
                                </div>
                            </div>
                        </div>
                        <div class="field" style="margin-bottom: 0px;">
                            <div class="divleft" style="width: 186px;">
                                <div style="width: 165px;" class="label">
                                    <label style="color: #4B9B5C;">Modificadores originais</label>
                                </div>
                            </div>
                            <div class="divleft" style="width: 139px;">
                                <div style="width: 79px; padding-left: 1px;" class="label">
                                    <label for="juros_original">Juros(%):</label>
                                </div>
                                <div class="input">
                                    <input style="width: 36px; color: #4B9B5C; font-weight: bold;" type="text" id="juros_original" name="juros_original" value="<?php echo $cobDados->cre_juros ?>" readonly/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 139px;">
                                <div style="width: 79px; padding-left: 1px;" class="label">
                                    <label for="multa_original">Multa(%):</label>
                                </div>
                                <div class="input">
                                    <input style="width: 36px; color: #4B9B5C; font-weight: bold;" type="text" id="multa_original" name="multa_original" value="0.0" readonly/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 175px;">
                                <div style="width: 115px; padding-left: 1px;" class="label">
                                    <label for="honorario_original">Honor�rios(%):</label>
                                </div>
                                <div class="input">
                                    <input style="width: 36px; color: #4B9B5C; font-weight: bold;" type="text" id="honorario_original" name="honorario_original" value="<?php echo $cobDados->cre_honorario ?>" readonly/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 28px; margin-left: 2px;" id="bt_atualizar_originais">
                                <a href="#">
                                    <img src="<?php echo $img . 'atu_verde.jpg' ?>" alt="atualizar"/>
                                </a>
                            </div>
                        </div>
                        <div class="field" style="margin-bottom: 0px;">
                            <div class="divleft" style="width: 186px;">
                                <div style="width: 175px;" class="label">
                                    <label style="color: #3173AD;">Modificadores atualizados</label>
                                </div>
                            </div>
                            <div class="divleft" style="width: 139px;">
                                <div style="width: 79px; padding-left: 1px;" class="label">
                                    <label for="juros_atual">Juros(%):</label>
                                </div>
                                <div class="input">
                                    <input style="width: 36px; color: #3173AD; font-weight: bold;" type="text" id="juros_atual" name="juros_atual" value="<?php echo strlen($cobDados->cre_juros) <= 3 ?>"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 139px;">
                                <div style="width: 79px; padding-left: 1px;" class="label">
                                    <label for="multa_atual">Multa(%):</label>
                                </div>
                                <div class="input">
                                    <input style="width: 36px; color: #3173AD; font-weight: bold;" type="text" id="multa_atual" name="multa_atual" value=""/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 175px;">
                                <div style="width: 115px; padding-left: 1px;" class="label">
                                    <label for="honorario_atual">Honor�rios(%):</label>
                                </div>
                                <div class="input">
                                    <input style="width: 36px; color: #3173AD; font-weight: bold;" type="text" id="honorario_atual" name="honorario_atual" value=""/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 28px; margin-left: 2px;" id="bt_atualizar_atualizados">
                                <a href="#">
                                    <img src="<?php echo $img . 'atu_azul.jpg' ?>" alt="atualizar"/>
                                </a>
                            </div>
                        </div>
                        <div id="valores_atualizados">
                            <div class="field" style="margin-bottom: 0px;">
                                <div class="divleft" style="width: 186px;">
                                    <div style="width: 175px;" class="label">
                                        <label>Valores atualizados (R$)</label>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 157px;">
                                    <div style="width: 45px; padding-left: 1px;" class="label">
                                        <label for="juros_total">Juros:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 79px; font-weight: bold;" type="text" id="juros_total" name="juros_total" value="" readonly/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 159px;">
                                    <div style="width: 48px; padding-left: 1px;" class="label">
                                        <label for="multa_total">Multa:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 81px; font-weight: bold;" type="text" id="multa_total" name="multa_total" value="" readonly/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 175px;">
                                    <div style="width: 85px; padding-left: 1px;" class="label">
                                        <label for="honorario_total">Honor�rios:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 59px; font-weight: bold;" type="text" id="honorario_total" name="honorario_total" value="" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="field" style="margin-bottom: 0px;">
                                <div class="divleft" style="width: 186px;">
                                    <div style="width: 81px;" class="label">
                                        <label for="custas">Custas (R$)</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 71px; font-weight: bold;" type="text" id="custas" name="custas" value=""/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 262px;">
                                    <div style="width: 112px; padding-left: 1px;" class="label">
                                        <label for="desconto">Desconto (-R$):</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 81px; font-weight: bold;" type="text" id="desconto" name="desconto" value=""/>
                                    </div>
                                    <a href="#" style="float: left; margin-left: 10px;" id="bt_atualizar_total">
                                        <img src="<?php echo $img . 'atu_vermelho.jpg' ?>" alt="atualizar"/>
                                    </a>
                                </div>
                                <div class="divleftlast" style="width: 240px; margin-left: 0px;" id="bloco_divida_atualizada">
                                    <div style="width: 101px; text-align: right; padding-top: 3px; padding-left: 0px;" class="label">
                                        <label for="total_divida_atualizada">TOTAL D�VIDA ATUALIZADA</label>
                                    </div>
                                    <p style="font-weight: bold; color: #f00; margin: 0px; float: right; font-size: 16px;" id="p_total_divida_atualizada"></p>
                                    <input type="hidden" name="total_divida_atualizada" id="total_divida_atualizada"/>

                                </div>

                            </div>
                        </div>
                        <div id="bloco_parcelamento">
                            <div style="margin-top: 23px; margin-bottom: 12px;" class="blocoTitulo">
                                Parcelamento
                            </div>

                            <div class="field" style="margin-bottom: 0px;">
                                <div class="divleft" style="width: 243px; margin-left: 37px; margin-right: 0px; ">
                                    <div style="width: 228px;" class="label">
                                        <input type="checkbox" name="juros_mensais" id="juros_mensais" value="1"/>
                                        <label>Habilitar juros futuros mensais</label>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 383px; margin-left: 3px;">
                                    <div style="width: 368px;" class="label">
                                        <label>Tipo de c�lculo</label>
                                        <input type="radio" name="tipo_calculo" value="1"/>Parcela desejada
                                        <input type="radio" name="tipo_calculo" value="2"/>n� de parcelas
                                    </div>
                                </div>
                            </div>

                            <div id="qtde_parcelas" class="field" style="margin-bottom: 0px;">
                                <div class="divleft" style="width: 184px;">
                                    <div style="width: 118px;" class="label">
                                        <label for="qtd_parcelas">Qtd. de parcelas:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 31px; font-weight: bold;" type="text" id="qtd_parcelas" maxlength="3" name="qtd_parcelas"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 186px;">
                                    <div style="width: 65px; padding-left: 0px;" class="label">
                                        <label for="pri_venc_qtd">1� Venc.:</label>
                                    </div>
                                    <div style=" width: 109px;" class="input">
                                        <input type="text" id="pri_venc_qtd" name="pri_venc_qtd" class="date" />
                                    </div>
                                </div>

                                <div class="divleftlast" style="width: 65px; margin-left: 2px;">
                                    <div class="buttons">
                                        <div class="highlight">
                                            <input style="width: 172px;" type="submit" name="qtde" id="gerarParcelasQtde" value="Gerar parcelas" />
                                            <input style="width: 172px;" type="submit" name="qtdeh" id="recalcularQtde" value="Racalcular parcelas" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="parcela_desejada" class="field" style="margin-bottom: 0px;">
                                <div class="divleft" style="width: 338px;">
                                    <div style="width: 161px;" class="label">
                                        <label for="valor_desejado">Parcela desejada (R$):</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 142px; font-weight: bold;" type="text" id="valor_desejado" name="valor_desejado" value=""/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 186px;">
                                    <div style="width: 65px; padding-left: 0px;" class="label">
                                        <label for="pri_venc_desejado">1� Venc.:</label>
                                    </div>
                                    <div style=" width: 109px;" class="input">
                                        <input type="text" id="pri_venc_desejado" name="pri_venc_desejado" class="date" value="" />
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 165px; margin-left: 2px;">
                                    <div class="buttons">
                                        <div class="highlight">
                                            <input style="width: 162px;" type="submit" name="pesquisar2" id="gerarParcelasDesejadas" value="Gerar parcelas" />
                                            <input style="width: 162px;" type="submit" name="recalcular" id="recaulcularParcelas" value="Recalcular parcelas" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="parcelas">
                                <!-- Aqui v�o as parcelas -->
                            </div>

                            <div class="field" id="total_parcelado_bloco" style="margin-bottom: 0px;">
                                <div class="divleft" style="width: 325px; margin-left: 0px; padding-top: 7px; padding-right: 5px;">
                                    <div style="width: 200px; text-align: right; padding-top: 3px; padding-left: 0px; font-size: 15px;" class="label">
                                        <label for="">Total de juros (R$):</label>
                                    </div>
                                    <p style="font-weight: bold; color: #f00; margin: 0px; float: right; font-size: 15px;" id="p_total_juros_parcela"></p>
                                    <input type="hidden" name="total_juros_parcela" id="total_juros_parcela" value=""/>
                                </div>

                                <div class="divleftlast" style="width: 365px; margin-left: 0px; padding-top: 7px; padding-right: 5px;">
                                    <div style="width: 235px; text-align: right; padding-top: 3px; padding-left: 0px; font-size: 15px;" class="label">
                                        <label for="">TOTAL PARCELADO (R$):</label>
                                    </div>
                                    <p style="font-weight: bold; color: #f00; margin: 0px; float: right; font-size: 15px;" id="p_total_valor_parcela"></p>
                                    <input type="hidden" name="total_valor_parcela" id="total_valor_parcela" value=""/>
                                </div>
                            </div>

                            <div style="text-align: center; margin-top: 20px;" class="buttons" id="botoes_acoes">
                                <input type="reset" class="cancelar" name="cancelar" value="Cancelar" />
                                <div style="margin-left: 7px;" class="highlight">
                                    <input type="hidden" name="cobranca_geradora_cod" value="<?php echo isset($cobDados->aco_cob_cod_geradora) ? $cobDados->cob_cod . '-' . $cobDados->aco_cob_cod_geradora : $cobDados->cob_cod ?>"/>
                                    <input type="hidden" name="parcelas_geradoras_cod" value="<?php echo $parcelasCod; ?>"/>
                                    <input type="hidden" name="repasse" value="<?php echo $cobDados->repasses_rep_cod ?>"/>
                                    <input type="hidden" name="inadimplente_cod" value="<?php echo $cobDados->ina_cod ?>"/>
                                    <input type="hidden" name="credor_cod" value="<?php echo $cobDados->cre_cod ?>"/>
                                    <input type="hidden" name="usuario_cod" value="<?php echo $cobDados->usuarios_usu_cod ?>"/>
                                    <input type="hidden" name="cob_setor" value="<?php echo $setorAcordo; ?>"/>
                                    <input type="hidden" name="cob_prazo" value="<?php echo $cobDados->cob_prazo ?>"/>
                                    <input type="hidden" name="aco_procedencia" value="<?php echo $procedencia ?>"/>
                                    <input type="hidden" name="numero_total_parcelas" id="numero_total_parcelas" value=""/>
                                    <input type="hidden" name="valor_original_divida" value="<?php echo number_format($valTotalDivida, 2, ',', '.'); ?>"/>
                                    <input type="hidden" name="juros_taxa_escolhido" id="juros_taxa_escolhido" value=""/>
                                    <input type="hidden" name="multa_taxa_escolhido" id="multa_taxa_escolhido" value=""/>
                                    <input type="hidden" name="honorario_taxa_escolhido" id="honorario_taxa_escolhido" value=""/>
                                    <input type="hidden" name="cobJudCodAco" id="cobJudCodAco" value="<?php echo isset($cobJudCodAco) ? $cobJudCodAco : '' ?>"/>

                                    <input type="submit" name="submit.highlight" id="btnEnviaAcordo" value="Finalizar acordo" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

