<script type="text/javascript">
	base_url = "<?php echo base_url(); ?>"; 
    $(document).ready(function(){

        $('.editaInad').click(function(){
                cod = $(this).attr('id');
                window.location.href="<?php echo base_url().'divida/ficha/cod:';?>"+cod;
        });
        
		$('input#search').quicksearch('#products tbody tr:not(.titulo)', {});
/*
		//AUTO FOCUS
		window.setTimeout(function(){
			$('input#search').focus();	
		},300);
*/	        
        
    });
</script>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Pesquisar Cobranšas por Processo</h5>
            </div>
            <?php if (!empty($credor)) : ?>
            <h3 style="text-align: center;"><?php echo $credor->cre_nome_fantasia; ?></h3>
            <?php endif; ?>
            <div class="blocoTitulo">
                <span style="font-size: 10px">Filtro: <input type="text" id="search" style="font-size: 18px; padding: 5px; width: 500px;" /></span>
            </div>
            <div class="table">
                <table id="products">
                    <thead>
                        <tr>
                        	<?php if (empty($credor)) : ?>
                        	<th>Credor</th>
                        	<?php endif; ?>
                            <th>Inadimplente</th>
                            <th>Setor</th>
                            <th>Processo</th>
                            <th style="width: 95px;"></th>
                        </tr>
                    </thead>
                    <tbody>                        
                    	<?php foreach ($cobrancas as $cobranca): ?>
                    		<?php //var_dump($cobranca); die() ?>
                        <tr>
                        	<?php if (empty($credor)) : ?>
                        	<td> <?php echo utf8_decode($cobranca->cre_nome_fantasia); ?> </td>
                        	<?php endif; ?>
                            <td> <?php echo utf8_decode($cobranca->ina_nome); ?> </td>
                            <td> <?php echo $cobranca->cob_setor . (!empty($cobranca->cob_subsetor)? '/'.$cobranca->cob_subsetor : ''); ?> </td>
                            <td> <?php echo utf8_decode($cobranca->processo_num); ?> </td>
                            <td class="last">
                                <div class="buttons">
                                    <input style="width: 50px;" class="visualizar editaInad" id="<?php echo $cobranca->inadimplentes_ina_cod; ?>" type="button" value="Ficha" />
                                    <input style="width: 38px;" class="" onclick="window.location.href=base_url+'ro/novo/cod:<?php echo $cobranca->cob_cod; ?>-';" type="button" value="RO" />
                                </div>
                            </td>
                        </tr>
                        <?php endforeach;?>                
                    </tbody>
                </table>
                <div class="pagination pagination-left">
                    <div class="results">
                        <span>Total de <?php echo count($cobrancas); ?> registros encontrados</span>
                    </div>
                </div>
                <br/>
                <br/>
            </div>
        </div>
    </div>
</div>
