<script type="text/javascript">
    $(document).ready(function(){

        $('.editaInad').click(function(e){
                link = $(this).attr('id');
                window.location.href=link;
        });
        
		$('input#search').quicksearch('#products tbody tr:not(.titulo)', {});
/*
		//AUTO FOCUS
		window.setTimeout(function(){
			$('input#search').focus();	
		},300);
*/	
    });
</script>
<div id="content">

    <?php echo $sidebar; ?>

    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Pesquisar inadimplente</h5>
            </div>
            <div class="blocoTitulo">
				<span style="font-size: 10px">Filtro: <input type="text" id="search" style="font-size: 18px; padding: 5px; width: 500px;" /></span>
            </div>
            <div class="table">
                <table id="products">
                    <thead>
                        <tr>
                            <th class="left">Nome</th>
                            <th>Endere�o</th>
                            <th>Telefone</th>
                            <th>Cidade</th>
                            <th class="last">UF</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=0; foreach ($resultados as $resultado): ?>
                            <tr style="cursor: pointer;">
                                <td class="title"><?php echo $resultado->ina_nome; ?></td>
                                <td><?php echo $resultado->ina_endereco; ?></td>
                                <td><?php echo $resultado->ina_foneres; ?></td>
                                <td><?php echo $resultado->ina_cidade; ?></td>
                                <td><?php echo $resultado->ina_uf; ?></td>
                                <td class="last">
                                	<input type="button" value="Cadastro" class="editaInad" id="<?php echo base_url().'inadimplente/editar/cod:'.$resultado->ina_cod;?>" />
                                	<input type="button" value="Cobran�a" class="editaInad" id="<?php echo base_url().'divida/ficha/cod:'.$resultado->ina_cod;?>" /> 
                                </td>
                            </tr>
                        <?php $i++; endforeach; ?>
                    </tbody>
                </table>
                <div class="pagination pagination-left">
                    <div class="results">
                        <span><?php echo $i; ?> registros encontrados</span>
                    </div>
                </div>
                <br/>
                <br/>
                <div><strong>FILTROS DA PESQUISA </strong></div>
                <br/>
                <?php echo isset ($filtro1)?'<div><strong>'.$filtro1.': </strong>'.$filtro1_desc.'</div>':'';?>
                <?php echo isset ($filtro2)?'<div><strong>'.$filtro2.': </strong>'.$filtro2_desc.'</div>':'';?>
                <?php echo isset ($filtro3)?'<div><strong>'.$filtro3.': </strong>'.$filtro3_desc.'</div>':'';?>
            </div>
<!--            <form id="form" action="">-->
                <div class="form">
                    <div class="fields">
                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <a href="<?php echo base_url().'pesquisar/inadimplente' ?>" style="text-decoration: none;" >
                                <div style="margin-left: 7px;" class="highlight">
                                    <input type="submit" name="submit.highlight" value="Nova pesquisa" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
<!--            </form>-->
        </div>
    </div>
</div>
