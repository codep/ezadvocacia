<script type="text/javascript">
    v1 = '';
    $(document).ready(function(){
        var existeParente = "<?php echo $existeParente; ?>";
        if(existeParente != '0')
        {
            $('a[href="#parentes"]').trigger('click');
        }
        $('.telefone').mask("(99)9999-9999?9").blur(function(){
			    var phone, element;
			    element = $(this);
			    element.unmask();
			    phone = element.val().replace(/\D/g, '');
			    if(phone.length > 10) {
			        element.mask("(99)99999-999?9");
			    } else {
			        element.mask("(99)9999-9999?9");
			    }
			    //element.unmask();
			}).click(function(){$(this).select();});
        $('input[name="cpf_cnpj"]').click(function(){$(this).select();});
        //------------------------------------------------------------------------------
        //---------------------------Fun��o para validar CNPJ---------------------------
        //------------------------------------------------------------------------------
        function validaCNPJ() {
            CNPJ = $('input[name="cpf_cnpj"]').val();
            erro = new String;
            if (CNPJ.length < 18) erro += "� necessario preencher corretamente o n�mero do CNPJ! \n\n";
            if ((CNPJ.charAt(2) != ".") || (CNPJ.charAt(6) != ".") || (CNPJ.charAt(10) != "/") || (CNPJ.charAt(15) != "-")){
                if (erro.length == 0) erro += "� necess�rio preencher corretamente o n�mero do CNPJ! \n\n";
            }
            //substituir os caracteres que n�o s�o n�meros
            if(document.layers && parseInt(navigator.appVersion) == 4){
                x = CNPJ.substring(0,2);
                x += CNPJ. substring (3,6);
                x += CNPJ. substring (7,10);
                x += CNPJ. substring (11,15);
                x += CNPJ. substring (16,18);
                CNPJ = x;
            } else {
                CNPJ = CNPJ. replace (".","");
                CNPJ = CNPJ. replace (".","");
                CNPJ = CNPJ. replace ("-","");
                CNPJ = CNPJ. replace ("/","");
            }
            var nonNumbers = /\D/;
            if (nonNumbers.test(CNPJ)) erro += "A verifica��o de CNPJ suporta apenas n�meros! \n\n";
            var a = [];
            var b = new Number;
            var c = [6,5,4,3,2,9,8,7,6,5,4,3,2];
            for (i=0; i<12; i++){
                a[i] = CNPJ.charAt(i);
                b += a[i] * c[i+1];
            }
            if ((x = b % 11) < 2) { a[12] = 0 } else { a[12] = 11-x }
            b = 0;
            for (y=0; y<13; y++) {
                b += (a[y] * c[y]);
            }
            if ((x = b % 11) < 2) { a[13] = 0; } else { a[13] = 11-x; }
            if ((CNPJ.charAt(12) != a[12]) || (CNPJ.charAt(13) != a[13])){
                erro +="CNPJ INV�LIDO, Por favor verifique!";
            }
            if (erro.length > 0){
                alert(erro);
                return false;
            }
            return true;
        }
        //------------------------------------------------------------------------------
        //---------------------------Fun��o para validar CPF----------------------------
        //------------------------------------------------------------------------------
        function validaCPF() {
            cpf = $('input[name="cpf_cnpj"]').val();

            cpf = cpf.replace(".", "");
            cpf = cpf.replace(".", "");
            cpf = cpf.replace("-", "");

            erro = new String;
            if (cpf.length < 11) erro += "Sao necessarios 11 digitos para verificacao do CPF! \n\n";
            var nonNumbers = /\D/;
            if (nonNumbers.test(cpf)) erro += "A verificacao de CPF suporta apenas numeros! \n\n";
            if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999"){
                erro += "Numero de CPF invalido!"
            }
            var a = [];
            var b = new Number;
            var c = 11;
            for (i=0; i<11; i++){
                a[i] = cpf.charAt(i);
                if (i < 9) b += (a[i] * --c);
            }
            if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
            b = 0;
            c = 11;
            for (y=0; y<10; y++) b += (a[y] * c--);
            if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
            if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10])){
                erro +="Numero de CPF invalido!, Por Favor confira";
            }
            if (erro.length > 0){
                alert(erro);
                return false;
            }
            return true;
        }
        //------------------------------------------------------------------------------
        //--------------------------- Fim das Valida��es -------------------------------
        //------------------------------------------------------------------------------

        //SE O RADIO PESSOA JUR�DICA VIER MARCADO
        if($('input[name="pessoa"][value="j"]').is(':checked'))
        {
            //MUDA O LABEL DO PARA NOME FANTASIA
            $('label[for="nome"]').text('Nome Fantasia:');
            //DESMARCAR O SEXO
            $("input[name='sexo']").each(function() {
                //desmarcar
                this.checked = false;
            });
            //ATRIBUI A LETRA J A VARIAVEL PESSOA QUE SERA USADA NA VERIFICA��O DO CPF/CNPJ
            pessoa='j';
            //DESMARCAR ESTADO CIVIL AINDA N�O FUNCIONA
            $('#est_civil option[value="0"]').attr('selected', true);
            //RESETAR CPF/CNPJ
            $('#cpf_cnpj').html('<input style="width: 124px;" type="text" name="cpf_cnpj"/>');
            //HABILITAR CAMPO E APLICAR M�SCARA PARA CPF
            $('input[name="cpf_cnpj"]').attr('disabled',false);
            $('input[name="cpf_cnpj"]').mask('99.999.999/9999-99');
            $('input[name="cpf_cnpj"]').attr('value', '<?php echo $inadDados->ina_pessoa == "j" ? "$inadDados->ina_cpf_cnpj" : ""; ?>');
        }

        //SE O RADIO PESSOA F�SICA VIER MARCADO
        if($('input[name="pessoa"][value="f"]').is(':checked'))
        {
            //ATRIBUI A LETRA F A VARIAVEL PESSOA QUE SERA USADA NA VERIFICA��O DO CPF/CNPJ
            pessoa='f';
            //ALTERA O LABEL DE NOME FANTASIA PARA NOME
            $('label[for="nome"]').text('Nome:');
            $('#sexo_est_civil').css('padding-top','0px');
            $('input[name="cpf_cnpj"]').mask('999.999.999-99');
        }

        //AO CLICAR EM PESSOA JURIDICA
        $('input[name="pessoa"][value="j"]').click(function() {
            //ATRIBUI A LETRA J A VARIAVEL PESSOA QUE SERA USADA NA VERIFICA��O DO CPF/CNPJ
            pessoa='j';
            $('label[for="nome"]').text('Nome Fantasia:');
            //DESMARCAR O SEXO
            $("input[name='sexo']").each(function() {
                //desmarcar
                this.checked = false;
            });
            //DESMARCAR ESTADO CIVIL AINDA N�O FUNCIONA
            $('#est_civil option[value="0"]').attr('selected', true);
            //RESETAR CPF/CNPJ
            $('#cpf_cnpj').html('<input style="width: 124px;" type="text" name="cpf_cnpj"/>');
            //HABILITAR CAMPO E APLICAR M�SCARA PARA CPF
            $('input[name="cpf_cnpj"]').attr('disabled',false).mask('99.999.999/9999-99');
            $('input[name="cpf_cnpj"]').attr('value', '<?php echo $inadDados->ina_pessoa == "j" ? "$inadDados->ina_cpf_cnpj" : ""; ?>');
        });

        //AO CLICAR EM PESSOA F�SICA
        $('input[name="pessoa"][value="f"]').click(function() {
            //ATRIBUI A LETRA F A VARIAVEL PESSOA QUE SERA USADA NA VERIFICA��O DO CPF/CNPJ
            pessoa='f';
            //SETA NULO NO INPUT RAZ�O SOCIAL
            $('input[name="razao_social"]').val('<?php echo $inadDados->ina_pessoa == "j" ? "$inadDados->ina_nome" : ""; ?>');
            //ALTERA O LABEL DE NOME FANTASIA PARA NOME
            $('label[for="nome"]').text('Nome:');
            //RESETAR CPF/CNPJ
            $('#cpf_cnpj').html('<input style="width: 124px;" type="text" name="cpf_cnpj" />');
            //APLICAR M�SCARA PARA CNPJ
            $('input[name="cpf_cnpj"]').attr('disabled',false).mask('999.999.999-99');
            $('input[name="cpf_cnpj"]').attr('value', '<?php echo $inadDados->ina_pessoa == "f" ? "$inadDados->ina_cpf_cnpj" : ""; ?>');
        });

        //VALIDA��O DOS CAMPOS OBRIGAT�RIOS DO FURMULARIO DE INADIMPLENTE
        $('#formUP').submit(function(){
            //SE O USU�RIO DIGITOU CPF OU CNPJ A VALIDA��O DEVE SER FEITA
            if(($('input[name="cpf_cnpj"]').val())!='')
            {
                //SE A VARIAVEL PESSOA FOR F(F�SICA) ENT�O VALIDA O CPF SEN�O VALIDA O CNPJ
                if(pessoa=='f'){
                    //ATRIBUI O RETURN DA FUN��O SE FOR FALSO N�O DEIXA ENVIAR O FORM
                    ret = validaCPF();
                    if(ret==false){
                        return false;
                    }
                }
                else if(pessoa=='j')
                {
                    ret = validaCNPJ();
                    if(ret==false){
                        return false;
                    }
                }
            }
            //TESTA PARA VER SE OS CAMPOS OBRIGAT�RIOS EST�O PREENCHIDOS
            var pessoaVazio = true;
            $('input[name="pessoa"]').each(function() {
                if ( $(this).is(':checked') ) {
                    pessoaVazio = false;
                }
            });
            if($('input[name="nome_fantasia"]').val() == '')
            {
                $('input[name="nome_fantasia"]').focus();
                return false;
            }
            else if(pessoaVazio)
            {
                alert('Indique pessoa F�SICA ou JUR�DICA.');
                return false;
            }
            else if($('#cidade option:selected').val() == '' || $('#uf option:selected').val() == '')
            {
                alert('Selecione uma CIDADE e um ESTADO');
                return false;
            }
            else
            {
                var marcado = false;
            }
            $('#conj_nome').attr('disabled',false);
            $('#conj_fone').attr('disabled',false);
            $('#pai_nome').attr('disabled',false);
            $('#pai_fone').attr('disabled',false);
            $('#mae_nome').attr('disabled',false);
            $('#mae_fone').attr('disabled',false);

        });

        //VALDA��O DOS CAMPOS OBRIGATORIOS DO FORMULARIO DE PARENTES
        $('#formPAR').submit(function(){
            if($('input[name="parentesco_01_nome"]').val() == '')
            {
                $('input[name="parentesco_01_nome"]').focus();
                return false;
            }
            $('#par_nome').attr('disabled',false);
            $('#par_fone').attr('disabled',false);
        });

        //VALDA��O DOS CAMPOS OBRIGATORIOS DO FORMULARIO DE BENS
        $('#formBEM').submit(function(){
            if($('input[name="titulo"]').val() == '')
            {
                $('input[name="titulo"]').focus();
                return false;
            }
        });

        //LISTANDO AS CIDADES
        $('select[id="ina_uf"]').change(function(){
            popularCidade('ina_');
        });
        $('select[id="con_uf"]').change(function(){
//            alert('Entrou no: PAI');
            popularCidade('con_');
        });
        $('select[id="pai_uf"]').change(function(){
            popularCidade('pai_');
        });
        $('select[id="mae_uf"]').change(function(){
            popularCidade('mae_');
        });

        function popularCidade(entidade)
        {

//            alert('Entrou na Fun��o: '+entidade);
            //$('select[id="uf"]').change(function(){

            v1 = '';
            //captura os options selecionados
            v1 = $('select[id="'+entidade+'uf"] option:selected').text();
            if(v1 == '') v1='UF';
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() . 'inadimplente/listar_cidades'; ?>",
                data: 'v1='+v1,
                dataType: 'json',
                error: function(xhr, status, er) {
                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                },
                success: function(retorno)
                {
                    $('#'+entidade+'cidade').html('<option value="">SELECIONE UMA CIDADE</option>');//zera os valores antigos e escreve no elemento o valor selecione
                    $qtd = '';
                    $valor = '';
                    $.each(retorno.cidadesbrasil, function(i, v){//para cada �ndice retornado executa a fun��o
                        if(v.cidadeatual != ''){
                            $('#'+entidade+'cidade-button').fadeOut('normal',function(){
                                //ESCONDE O OPTION INICIAL SEM VALOR
                            });
                            $('#'+entidade+'cidade').append('<option value="'+unescape(v.cidadeatual)+'">'+unescape(v.cidadeatual)+'</option>');//unescape descriptografa o padr�o URL de dados retornado no PHP
                        }
                        $qtd = i;
                        $valor = v.cidadeatual;
                    });
                    if ($qtd != 0 && $valor != ''){
                        $('#'+entidade+'cidade').fadeIn('normal', function(){
                            //CIDADES CARREGADAS COM SUCESSO
                        });
                    }
                    else
                    {
                        alert('Voc� deve selecionar um ESTADO v�lido.');
                        $('#'+entidade+'cidade').html('<option value=""><-- SELECIONE UM ESTADO</option>');//zera os valores antigos e escreve no elemento o valor selecione
                    }
                }
            });
        }
        base_url = "<?php echo base_url();?>";

        //OCULTANDO BOT�ES DE CADASTRO
        $("#cadastroCONJ").css('display','none');
        $("#cadastroPAI").css('display','none');
        $("#cadastroMAE").css('display','none');
        $("#cadastroPAR").css('display','none');

        //CASO O PARENTE SEJA INADIMLPENTE
        if($("#conj_cod").val() != '0') verificaParInad('conj');
        if($("#pai_cod").val() != '0') verificaParInad('pai');
        if($("#mae_cod").val() != '0') verificaParInad('mae');

        function verificaParInad(entidade)
        {
                //OCULTAR ELEMENTOS E TRAVAR DIGITA��O
                $("#"+entidade).hide();
                $("#"+entidade+"_nome").attr('disabled',true);
                $("#"+entidade+"_fone").attr('disabled',true);
                $("#pesquisar"+entidade.toUpperCase()).css('display','none');

                //EXIBIR ELEMENTOS
                $("#cadastro"+entidade.toUpperCase()).css('display','block');
                $("#"+entidade+"_novaPesquisa").css('display','block');

                //AO CLICAR EM NOVA PESQUISA
                $('#'+entidade+'_novaPesquisa').click(function(e){
                    e.preventDefault();
                    $('#pesquisar'+entidade.toUpperCase()).css('display','block');
                    $('#'+entidade).css('display','block');
                    $('#cadastro'+entidade.toUpperCase()).css('display','none');
                    $('#'+entidade+'_nome').val("").attr('disabled',false);
                    $('#'+entidade+'_fone').val("").attr('disabled',false);
                    $('#'+entidade+'_cod').val("0");
                    $(this).css('display','none');
                });

                //DIRECIONANDO USUARIO PARA O CADASTRO DO INADIMPLENTE
                $('#cadastro'+entidade.toUpperCase()).click(function(){
                    link = base_url+'inadimplente/editar/cod:'+$("#"+entidade+"_cod").val();
                    window.open(link, 'blank');
                });
        }

        //PESQUISA DE PARENTES
        $("#pesquisarCONJ").click(function () {
            pesquisarParentes("conj_")
        });
        $("#pesquisarPAI").click(function () {
            pesquisarParentes("pai_")
        });
        $("#pesquisarMAE").click(function () {
            pesquisarParentes("mae_")
        });
        $("#pesquisarPAR").click(function () {
            pesquisarParentes("par_")
        });

        function pesquisarParentes(origem)
        {
            //ENTIDADE DO TIPO INADIMPLENTE
            entidade = 'i';

            //RECEBENDO O TIPO DE PESSOA [f]
            pessoa = 'f';

            //RECEBENDO O TIPO DE DADOS [nome_fantasia ou cpf_cnpj]
            tipodado = 'n';

            //RECEBENDO O VALOR DIGITADO EM nome
            valor = $('input[name="'+origem+'nome"]').val();

            if(origem == 'par_') valor = $('#'+origem+'nome').val();

            //FUN��O AJAX
            $.ajax({
                type: "POST",//TIPO DE DADOS
                url: base_url+'inadimplente/listar_pesquisa',//FUN��O DO CONTROLLER PARA TRATAR A LISTAGEM
                data: 'entidade='+entidade+'&pessoa='+pessoa+'&tipodado='+tipodado+'&valor='+valor,//DADOS ENVIADOS
                dataType: 'json',//TIPO DE DADOS A SER RECEBIDO

                //CASO HAJA ERRO DE PROCESSAMENTO
                error: function(xhr, status, er) {
                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                },

                //CASO A FUN��O DO CONTROLLER SEJA EXECUTADA COM SUCESSO E OS DADOS RECEBIDOS COM SUCESSO
                success: function(retorno)
                {
                    $("#dialog-form").dialog("open");///ABRE A CAIXA DE DI�LOGO
                    $("#products").find(":eq(3)").html('FONE');//Substituindo a coluna CPF_CNPJ por FONE
                    $('#dadosPesquisa').html('');//zera os valores antigos e escreve no elemento o valor selecione
                    vazio = false;//VARI�VEL PARA IDENTIFICAR SE ALGUM DADO �NTEGRO TENHA SIDO RECEBIDO

                    $.each(retorno.resultado, function(i, v){//para cada �ndice retornado executa a fun��o
                        if(v.nome != null)
                        {
                            //CONCATENA AS LINHAS DA TABELA COM OS VALORES RECEBIDOS
                            $('#dadosPesquisa').append(
                                '<tr class="tuplas" id="'+unescape(v.codigo)+'">'+
                                    '<td>'+unescape(v.nome)+'</td>'+
                                    '<td>'+unescape(v.fone)+'</td>'+
                                    '<td>'+unescape(v.endereco)+'</td>'+
                                    '<td class="last">'+unescape(v.cidade)+'</td>'+
                                '</tr>'
                            );
                        }
                        else vazio = true;//CASO OS DADOS RETORNADOS SEJAM NULOS
                    });
                    $('.tuplas').css('cursor','pointer');//SETA O TIPO DO CURSOR

                    //AO CLICAR SOBRE UMA LINHA DA TABALA COM A CLASSE TUPLA
                    $('.tuplas').click(function(e){
                        e.preventDefault();
                        alert('deu certo');

                        //CAPTURANDO O C�DIGO, O NOME E O TELEFONE
                        codigo = $(this).attr('id');
                        nome = $(this).find(":eq(0)").text();
                        fone = $(this).find(":eq(1)").text();

                        //ATRIBUINDO VALORS CAMPOS
                        $('#'+origem+'nome').val(nome).attr('disabled',true);
                        $('#'+origem+'fone').val(fone).attr('disabled',true);
                        $('#'+origem+'cod').val(codigo);

                        //ZERANDO O ENDERE�O, A UF E A CIDADE
                        $('#'+origem+'endereco').val("");
                        $('select[name="'+origem+'uf"]').val("");
                        resetauf = origem;//VARI�VEL AUXILIAR PARA REMOVER O J DO CONJ
                        if(origem == "conj_") resetauf = origem.replace("j","")
                        $('#'+resetauf+'uf-button').find(":eq(0)").html('UF');
                        $('select[name="'+origem+'cidade"]').val("");

                        //OCULTANDO E EXIBINDO OS BOT�ES
                        ocultarExibir = origem.replace("_","");
                        $('#'+ocultarExibir).css('display','none');
                        $('#pesquisar'+ocultarExibir.toUpperCase()).css('display','none');
                        $('#cadastro'+ocultarExibir.toUpperCase()).css('display','block');

                        //AO CLICAR EM NOVA PESQUISA
                        $('#'+origem+'novaPesquisa').css('display','block').click(function(e){
                            e.preventDefault();
                            $('#pesquisar'+ocultarExibir.toUpperCase()).css('display','block');
                            $('#'+ocultarExibir).css('display','block');
                            $('#cadastro'+ocultarExibir.toUpperCase()).css('display','none');
                            $('#'+origem+'nome').val("").attr('disabled',false);
                            $('#'+origem+'fone').val("").attr('disabled',false);
                            $('#'+origem+'cod').val("0");
                            $(this).css('display','none');
                        });

                        //DIRECIONANDO USUARIO PARA O CADASTRO DO INADIMPLENTE
                        $('#cadastro'+ocultarExibir.toUpperCase()).click(function(){
                            link = base_url+'inadimplente/editar/cod:'+codigo;
                            window.open(link, 'blank');
                        });

                        //FECHA A CAIXA DE DIALOGO
                        $('.ui-dialog-titlebar-close').trigger('click');
                    });

                    //CASO OS DADOS RETORNADOS SEJAM NULOS
                    if (vazio)
                    {   //EXIBE MENSAGEM
                        $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Nenhum dado encontrado pesquisando por ['+valor+']</h3></td></tr>');
                    }
                }
            });
        }
    });
</script>
<div id="dialog-form" style="padding-left: 5px; padding-right: 5px; " title="Pesquisar dados">
    <p style="text-align: center">
        <strong>Dados encontrados:</strong>
    </p>
    <div id="content" style="width: 750px;">
        <div id="right" style="margin-left: 0px;">
            <div class="box">
                <div class="table" style="padding-left: 0px; font-size: 10px; width: 750px;">
                    <form action="" method="post">
                        <table id="products">
                            <thead>
                                <tr>
                                    <th class="left" style="width: 20%;">NOME</th>
                                    <th style="width: 14%;">CPF/CNPJ</th>
                                    <th>ENDERE�O</th>
                                    <th class="last">CIDADE</th>
                                </tr>
                            </thead>
                            <tbody id="dadosPesquisa">
                                <!--RESULTADO DA PESQUISA AQUI!! -->
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right" style="min-height: 798px;">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Cadastro (<?php echo utf8_decode($inadDados->ina_nome); ?>)</h5>
                <ul class="links">
                    <li><a href="#cadastro">Cadastro</a></li>
                    <li><a href="#parentes">Parentes</a></li>
                    <li><a href="#bens">Bens</a></li>
                </ul>
            </div>
            <div id="cadastro">
                <form id="formUP" action="<?php echo base_url() . 'inadimplente/update'; ?>" method="post">
                    <?php echo $mensagem ?>
                    <div class="form">
                        <?php if($existeImportacao == 1):?>
                        <div class="fields">
                            <div class="field  field-first">
                                <div class="divleftlast" style="width: 493px; margin: 0px;">
                                    <div class="buttons" style="margin: 10px 0 0 283px;">
                                        <div class="highlight">
                                            <a href="<?php echo base_url().'inadimplente/inadDadosImportados/inaCod:'.$inadDados->ina_cod;?>" target="_blank" class="dialog-form-open ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">Visualiar dados Importados</a>
                                        </div>
                                    </div>                       
                                </div>
                            </div>
                        </div>
                        <?php endif;?>
                        <div class="fields">
                            <div class="field  field-first">
                                <div class="divleft" style="width: 116px;">
                                    <div class="label">
                                        <label>C�digo:</label>
                                    </div>
                                    <input type="hidden" name="cod"  value="<?php echo $inadDados->ina_cod; ?>"/>
                                    <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inadDados->ina_cod; ?></p>
                                </div>
                                <div class="divleft" style="width: 200px;">
                                    <div class="label">
                                        <label>Pessoa:</label>
                                    </div>
                                    <input style="margin-top: 7px; " type="radio" name="pessoa" value="f" id="pfisica" <?php echo $inadDados->ina_pessoa == 'f' ? 'checked' : ''; ?>>F�sica
                                    <input type="radio" name="pessoa" value="j" id="pjuridica" <?php echo $inadDados->ina_pessoa == 'j' ? 'checked' : ''; ?>>Jur�dica
                                </div>
                                <div id="atrsPessoaFisica" style="width:375px; height:30px; float: right; overflow: hidden">
                                    <div id="sexo_est_civil">
                                        <div class="divleft" style="width: 173px;" id="sexo" >
                                            <div class="label">
                                                <label>Sexo:</label>
                                            </div>
                                            <input type="radio" name="sexo" value="m" style="margin-top: 7px;" <?php echo $inadDados->ina_sexo == 'm' ? 'checked' : ''; ?>>Masc.
                                            <input type="radio" name="sexo" value="f" <?php echo $inadDados->ina_sexo == 'f' ? 'checked' : ''; ?> >Fem.
                                        </div>
                                        <div class="divleftlast" style="width: 184px;" id="estadocivil">
                                            <div class="select">
                                                <select style="width: 186px; margin-left: 5px;" id="est_civil" name="estado_civil">
                                                    <option value="0" class="ec">Estado civil</option>
                                                    <option value="1" class="ec" <?php echo $inadDados->ina_estado_civil == 1 ? 'selected' : ''; ?>>Solteiro(a)</option>
                                                    <option value="2" class="ec" <?php echo $inadDados->ina_estado_civil == 2 ? 'selected' : ''; ?>>Casado(a)</option>
                                                    <option value="3" class="ec" <?php echo $inadDados->ina_estado_civil == 3 ? 'selected' : ''; ?>>Separado(a)</option>
                                                    <option value="4" class="ec" <?php echo $inadDados->ina_estado_civil == 4 ? 'selected' : ''; ?>>Divorciado(a)</option>
                                                    <option value="5" class="ec" <?php echo $inadDados->ina_estado_civil == 5 ? 'selected' : ''; ?>>Viuvo(a)</option>
                                                    <option value="6" class="ec" <?php echo $inadDados->ina_estado_civil == 6 ? 'selected' : ''; ?>>Amasiado(a)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 371px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="nome">Nome:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 248px;" type="text" id="nome" name="nome_fantasia" maxlength="120" value="<?php echo utf8_decode($inadDados->ina_nome); ?>"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 231px;">
                                    <div style="width: 76px; padding-left: 1px;" class="label">
                                        <label for="cpf_cnpj">CPF/CNPJ:</label>
                                    </div>
<!--                                    <div class="input" id="cpf_cnpj_div">
                                        
                                        <input style="width: 124px;" type="text" id="cpf_cnpj" name="cpf_cnpj" maxlength="18" value="< ?php echo $inadDados->ina_cpf_cnpj; ?>" />
                                    </div>-->
                                    <div class="input" id="cpf_cnpj_div">
                                        <?php
                                        if(($inadDados->ina_cpf_cnpj == '') || ($usuarioDados->grupo_usuarios_gru_cod == '1') || ($usuarioDados->grupo_usuarios_gru_cod == '2')){
                                            echo '<input style="width: 124px; background-color:#F6F6F6;" type="text" id="cpf_cnpj" name="cpf_cnpj" maxlength="18" value="'.$inadDados->ina_cpf_cnpj.'"/>';
                                        }else{
                                            echo '<input disabled style="" type="text" id="cpf_cnpj_falso" name="cpf_cnpj_falso" maxlength="18" value="'.$inadDados->ina_cpf_cnpj.'"/>';
                                            echo '<input type="hidden" name="cpf_cnpj" value="'.$inadDados->ina_cpf_cnpj.'"/>';
                                        }
                                        ?>

                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 211px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="rg_ie">RG/I.E.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 89px;" type="text" id="rg_ie" maxlength="30" name="rg_ie" value="<?php echo $inadDados->ina_rg_ie; ?>"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 250px;">
                                    <div style="width: 42px; padding-left: 1px;" class="label">
                                        <label for="endereco">End.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 178px;" type="text" id="endereco" maxlength="130" name="endereco" value="<?php echo utf8_decode($inadDados->ina_endereco); ?>"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 233px; margin: 0px;">
                                    <div style="width: 51px; padding-left: 1px;" class="label">
                                        <label for="bairro">Bairro:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 155px;" type="text" id="bairro" maxlength="50" name="bairro" value="<?php echo utf8_decode($inadDados->ina_bairro); ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 211px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="complemento">Compl.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 89px;" type="text" id="complemento" name="complemento" maxlength="40" value="<?php echo utf8_decode($inadDados->ina_complemento); ?>"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 132px;">
                                    <div style="width: 34px; padding-left: 1px;" class="label">
                                        <label for="cep">CEP.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 65px;" type="text" id="cep" name="cep" maxlength="8" value="<?php echo $inadDados->ina_cep; ?>"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 109px;">
                                    <div style="width: 27px; padding-left: 1px;" class="label">
                                        <label for="uf">UF:</label>
                                    </div>
                                    <div class="select">
                                        <select style="width: 65px; margin-left: 7px;" id="ina_uf" name="uf">
                                            <option>UF</option>
                                            <?php foreach ($estados as $uf): ?>
                                                <option value="<?php echo $uf->cid_estado; ?>" <?php echo $inadDados->ina_uf == $uf->cid_estado ? 'selected' : ''; ?>><?php echo $uf->cid_estado; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 230px; margin: 0px; float: left">
                                    <div class="select">
                                        <select style="width: 230px; height: 28px; border: 1px solid #D4D0C8; padding: 5px; font-size: 11px; font-family: Verdana" id="ina_cidade" name="cidade">
                                            <option style="border: 1px solid;"><?php echo utf8_decode($inadDados->ina_cidade); ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                                <div class="field">
                                    <div class="divleft" style="width: 240px;">
                                        <div style="width: 89px;" class="label">
                                            <label for="foneres">Fone Res.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 117px;" type="text" class="telefone" id="foneres" name="foneres" value="<?php echo $inadDados->ina_foneres; ?>"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 238px;">
                                        <div style="width: 77px; padding-left: 1px;" class="label">
                                            <label for="fonerec">Fone Rec.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 130px;" type="text" class="telefone" id="fonerec" name="fonerec" value="<?php echo $inadDados->ina_fonerec; ?>"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 216px; margin: 0px;">
                                        <div style="width: 74px; padding-left: 1px;" class="label">
                                            <label for="fonecom">Fone Com.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 115px;" type="text" class="telefone" id="fonecom" name="fonecom" value="<?php echo $inadDados->ina_fonecom; ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="width: 240px;">
                                        <div style="width: 89px;" class="label">
                                            <label for="cel1">Celular(1):</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 117px;" type="text" class="telefone" id="cel1" name="cel1" value="<?php echo $inadDados->ina_cel1; ?>"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 238px;">
                                        <div style="width: 77px; padding-left: 1px;" class="label">
                                            <label for="cel2">Celular(2):</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 130px;" type="text" class="telefone" id="cel2" name="cel2" value="<?php echo $inadDados->ina_cel2; ?>"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 216px; margin: 0px;">
                                        <div style="width: 74px; padding-left: 1px;" class="label">
                                            <label for="cel3">Celular(3):</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 115px;" type="text" class="telefone" id="cel3" name="cel3" value="<?php echo $inadDados->ina_cel3; ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div style="height: 205px;" class="field">
                                    <div class="divleftlast" style="width: 701px;">
                                        <div style="width: 84px;" class="label">
                                            <label for="info">Informa��es relevantes:</label>
                                        </div>
                                        <div class="textarea">
                                            <textarea style="width: 586px; height: 180px;" id="info" name="info" cols="500" rows="4"><?php echo utf8_decode($inadDados->ina_info); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="blocoTitulo">
                                    Parentes de 1� grau
                                </div>
                                <div class="field" style="border-bottom: none;">
                                    <div class="divleft" style="width: 406px;">
                                        <div style="width: 89px;" class="label">
                                            <label for="conj_nome">Conjuge:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 283px;" type="text" id="conj_nome" maxlength="50" name="conj_nome" value="<?php echo utf8_decode($inadDados->ina_conj_nome); ?>"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 196px;">
                                        <div style="width: 41px; padding-left: 1px;" class="label">
                                            <label for="conj_fone">Fone:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 124px;" type="text" class="telefone" id="conj_fone" name="conj_fone" value="<?php echo $inadDados->ina_conj_fone; ?>" />
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 87px; margin: 0px;">
                                        <div class="buttons">
                                            <div class="highlight">
                                                <button style="width: 85px; padding: 4px;" id="pesquisarCONJ" name="pesquisarCONJ" type="button" class="dialog-form-open">Pesquisar</button>
                                                <button style="width: 85px; padding: 4px;" id="cadastroCONJ" name="cadastroCONJ" type="button" class="dialog-form-open">Cadastro</button>
                                                <input type="hidden" id="conj_cod" name="conj_cod" value="<?php echo utf8_decode($inadDados->ina_conj_cod); ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="conj_novaPesquisa" style="text-align: center; display:none"><a href="#">Nova Pesquisa</a></div>
                                <div class="field">
                                    <div id="conj">
                                        <div class="divleft" style="width: 351px;">
                                            <div style="width: 89px;" class="label">
                                                <label for="conj_endereco">Endere�o:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 223px;" type="text" id="conj_endereco" maxlength="130" name="conj_endereco" value="<?php echo utf8_decode($inadDados->ina_conj_endereco); ?>" />
                                            </div>
                                        </div>

                                        <div class="divleftlast" style="width: 109px;">
                                            <div style="width: 27px; padding-left: 1px;" class="label">
                                                <label for="uf">UF:</label>
                                            </div>
                                            <div class="select">
                                                <select style="width: 65px; margin-left: 7px;" id="con_uf" name="conj_uf">
                                                    <option value="" selected>UF</option>
                                                    <?php foreach ($estados as $uf): ?>
                                                    <option value="<?php echo $uf->cid_estado; ?>" <?php echo $inadDados->ina_conj_uf == $uf->cid_estado ? 'selected' : ''; ?>><?php echo $uf->cid_estado; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 226px; margin: 0px; float: left">
                                            <div class="select">
                                                <select style="width: 230px; height: 28px; border: 1px solid #D4D0C8; padding: 5px; font-size: 11px; font-family: Verdana" id="con_cidade" name="conj_cidade">
                                                    <option value="<?php echo utf8_decode($inadDados->ina_conj_cidade); ?>"><?php echo $inadDados->ina_conj_cidade != '' ? $inadDados->ina_conj_cidade : '<-- SELECIONE UM ESTADO'; ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="field" style="border-bottom: none;">
                                    <div class="divleft" style="width: 406px;">
                                        <div style="width: 89px;" class="label">
                                            <label for="pai_nome">Pai:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 283px;" type="text" id="pai_nome" maxlength="50" name="pai_nome" value="<?php echo utf8_decode($inadDados->ina_pai_nome); ?>"/>
                                            <input type="hidden" name="pai_cod" value="0"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 196px;">
                                        <div style="width: 41px; padding-left: 1px;" class="label">
                                            <label for="pai_fone">Fone:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 124px;" type="text" class="telefone" id="pai_fone" name="pai_fone" value="<?php echo $inadDados->ina_pai_fone; ?>"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 87px; margin: 0px;">
                                        <div class="buttons">
                                            <div class="highlight">
                                                <button style="width: 85px; padding: 4px;" id="pesquisarPAI" name="pesquisarPAI" type="button" class="dialog-form-open">Pesquisar</button>
                                                <button style="width: 85px; padding: 4px;" id="cadastroPAI" name="cadastroPAI" type="button" class="dialog-form-open">Cadastro</button>
                                                <input type="hidden" id="pai_cod" name="pai_cod" value="<?php echo utf8_decode($inadDados->ina_pai_cod); ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="pai_novaPesquisa" style="text-align: center; display:none"><a href="#">Nova Pesquisa</a></div>
                                <div class="field">
                                    <div id="pai">
                                        <div class="divleft" style="width: 351px;">
                                            <div style="width: 89px;" class="label">
                                                <label for="pai_endereco">Endere�o:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 223px;" type="text" id="pai_endereco" maxlength="130" name="pai_endereco" value="<?php echo utf8_decode($inadDados->ina_pai_endereco); ?>"/>
                                            </div>
                                        </div>

                                        <div class="divleftlast" style="width: 109px;">
                                            <div style="width: 27px; padding-left: 1px;" class="label">
                                                <label for="uf">UF:</label>
                                            </div>
                                            <div class="select">
                                                <select style="width: 65px; margin-left: 7px;" id="pai_uf" name="pai_uf">
                                                    <option value="" selected>UF</option>
                                                    <?php foreach ($estados as $uf): ?>
                                                    <option value="<?php echo $uf->cid_estado; ?>" <?php echo $inadDados->ina_pai_uf == $uf->cid_estado ? 'selected' : ''; ?>><?php echo $uf->cid_estado; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 226px; margin: 0px; float: left">
                                            <div class="select">
                                                <select style="width: 230px; height: 28px; border: 1px solid #D4D0C8; padding: 5px; font-size: 11px; font-family: Verdana" id="pai_cidade" name="pai_cidade">
                                                    <option value="<?php echo utf8_decode($inadDados->ina_pai_cidade); ?>"><?php echo $inadDados->ina_pai_cidade != '' ? $inadDados->ina_pai_cidade : '<-- SELECIONE UM ESTADO'; ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field" style="border-bottom: none;">
                                    <div class="divleft" style="width: 406px;">
                                        <div style="width: 89px;" class="label">
                                            <label for="mae_nome">M�e:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 283px;" type="text" id="mae_nome" maxlength="50" name="mae_nome" value="<?php echo utf8_decode($inadDados->ina_mae_nome); ?>"/>
                                            <input type="hidden" name="mae_cod" value="0"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 196px;">
                                        <div style="width: 41px; padding-left: 1px;" class="label">
                                            <label for="mae_fone">Fone:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 124px;" type="text" class="telefone" id="mae_fone" name="mae_fone" value="<?php echo $inadDados->ina_mae_fone; ?>"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 87px; margin: 0px;">
                                        <div class="buttons">
                                            <div class="highlight">
                                                <button style="width: 85px; padding: 4px;" id="pesquisarMAE" name="pesquisarMAE" type="button" class="dialog-form-open">Pesquisar</button>
                                                <button style="width: 85px; padding: 4px;" id="cadastroMAE" name="cadastroMAE" type="button" class="dialog-form-open">Cadastro</button>
                                                <input type="hidden" id="mae_cod" name="mae_cod" value="<?php echo utf8_decode($inadDados->ina_mae_cod); ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="mae_novaPesquisa" style="text-align: center; display:none"><a href="#">Nova Pesquisa</a></div>
                                <div class="field">
                                    <div id="mae">
                                        <div class="divleft" style="width: 351px;">
                                            <div style="width: 89px;" class="label">
                                                <label for="mae_endereco">Endere�o:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 223px;" type="text" id="mae_endereco" maxlength="130" name="mae_endereco" value="<?php echo utf8_decode($inadDados->ina_mae_endereco); ?>"/>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 109px;">
                                            <div style="width: 27px; padding-left: 1px;" class="label">
                                                <label for="uf">UF:</label>
                                            </div>
                                            <div class="select">
                                                <select style="width: 65px; margin-left: 7px;" id="mae_uf" name="mae_uf">
                                                    <option value="" selected>UF</option>
                                                    <?php foreach ($estados as $uf): ?>
                                                    <option value="<?php echo $uf->cid_estado; ?>" <?php echo $inadDados->ina_mae_uf == $uf->cid_estado ? 'selected' : ''; ?>><?php echo $uf->cid_estado; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 226px; margin: 0px; float: left">
                                            <div class="select">
                                                <select style="width: 230px; height: 28px; border: 1px solid #D4D0C8; padding: 5px; font-size: 11px; font-family: Verdana" id="mae_cidade" name="mae_cidade">
                                                    <option value="<?php echo utf8_decode($inadDados->ina_mae_cidade); ?>"><?php echo $inadDados->ina_mae_cidade != '' ? $inadDados->ina_mae_cidade : '<-- SELECIONE UM ESTADO'; ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            <div style="text-align: center; margin-top: 10px;" class="buttons">
                                <input type="reset" name="Cancelar" value="Cancelar" onclick="history.back(-1)"/>
                                <div style="margin-left: 7px;" class="highlight">
                                    <input type="submit" name="cadastrar" value="Atualizar" />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div id="parentes">
                <form action="<?php echo isset($parDados)==false?base_url().'inadimplente/novo_parente':base_url().'inadimplente/atualizar_parente'?>" id="formPAR" method="post">
                    <div class="form">
                        <?php if(isset($parDados)==false):?>
                            <div class="fields">
                                <div>
                                    <div class="blocoTitulo">
                                        Adicionar parentes
                                    </div>
                                </div>
                                <div class="field" style="border: none; margin-bottom: 0px;">
                                    <div class="divleft" style="border: none; width: 116px;">
                                        <div style="margin-left: 6px;" class="select">
                                            <select style="width: 110px;" id="parentesco_01" name="parentesco_01">
                                                <option value="Avo">Av�/Av�</option>
                                                <option value="Cunhado(a)">Cunhado(a)</option>
                                                <option value="Enteado(a)">Enteado(a)</option>
                                                <option value="Filho(a)">Filho(a)</option>
                                                <option value="Genro/Nora">Genro/Nora</option>
                                                <option value="Irm�o(a)">Irm�o(a)</option>
                                                <option value="Padrasto/Madrasta">Padrasto/Madrasta</option>
                                                <option value="Primo(a)">Primo(a)</option>
                                                <option value="Sobrinho(a)">Sobrinho(a)</option>
                                                <option value="Sogro(a)">Sogro(a)</option>
                                                <option value="Tio(a)">Tio(a)</option>
                                                <option value="Outros">OUTROS</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 282px;">
                                        <div class="input">
                                            <input style="width: 257px;" type="text" id="par_nome" maxlength="50" name="parentesco_01_nome"/>
                                            <input type="hidden" name="cod" value="<?php echo $inadDados->ina_cod; ?>"/>
                                            <input type="hidden" id="par_cod" name="inad_cod" value="0"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 196px;">
                                        <div style="width: 41px; padding-left: 1px;" class="label">
                                            <label for="parentesco_01_fone">Fone:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 124px;" type="text" id="par_fone" class="telefone" name="parentesco_01_fone"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 87px; margin: 0px;">
                                        <div class="buttons">
                                            <div class="highlight">
                                                <button style="width: 85px; padding: 4px;" id="pesquisarPAR" name="pesquisarPAR" type="button" class="dialog-form-open">Pesquisar</button>
                                                <button style="width: 85px; padding: 4px;" id="cadastroPAR" name="cadastroPAR" type="button" class="dialog-form-open">Cadastro</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="par_novaPesquisa" style="text-align: center; display:none"><a href="#">Nova Pesquisa</a></div>
                                <div id="par">
                                    <div class="field">
                                        <div class="divleft" style="width: 240px;">
                                            <div style="width: 89px;" class="label">
                                                <label for="parentesco_01_fone2">Fone(2):</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 117px;" type="text" id="parentesco_01_fone2" class="telefone" name="parentesco_01_fone2"/>
                                            </div>
                                        </div>
                                        <div class="divleft" style="width: 238px;">
                                            <div style="width: 77px; padding-left: 1px;" class="label">
                                                <label for="parentesco_01_fone3">Fone(3):</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 130px;" type="text" id="parentesco_01_fone3" class="telefone" name="parentesco_01_fone3"/>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 216px; margin: 0px;">
                                            <div style="width: 74px; padding-left: 1px;" class="label">
                                                <label for="parentesco_01_fone4">Fone(4):</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 115px;" type="text" id="parentesco_01_fone4" class="telefone" name="parentesco_01_fone4"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="text-align: center; margin-top: 10px;" class="buttons">
                                    <div style="margin-left: 7px;" class="highlight">
                                        <input type="submit" name="cadastrar" value="Adicionar parente" />
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if(isset($parDados)==true):?>
                            <div class="fields">
                                <div>
                                    <div class="blocoTitulo">
                                        Editar parente
                                    </div>
                                </div>
                                <div class="field" style="border: none; margin-bottom: 0px;">
                                    <div class="divleft" style="border: none; width: 116px;">
                                        <div style="margin-left: 6px;" class="select">
                                            <select style="width: 110px;" id="parentesco_01" name="parentesco_01">
                                                <option <?php echo $parDados->par_parentesco=='Avo'?'selected="selected"':'';?> value="Avo">Av�/Av�</option>
                                                <option <?php echo $parDados->par_parentesco=='Cunhado(a)'?'selected="selected"':'';?> value="Cunhado(a)">Cunhado(a)</option>
                                                <option <?php echo $parDados->par_parentesco=='Enteado(a)'?'selected="selected"':'';?> value="Enteado(a)">Enteado(a)</option>
                                                <option <?php echo $parDados->par_parentesco=='Filho(a)'?'selected="selected"':'';?> value="Filho(a)">Filho(a)</option>
                                                <option <?php echo $parDados->par_parentesco=='Genro/Nora'?'selected="selected"':'';?> value="Genro/Nora">Genro/Nora</option>
                                                <option <?php echo $parDados->par_parentesco=='Irm�o(a)'?'selected="selected"':'';?> value="Irm�o(a)">Irm�o(a)</option>
                                                <option <?php echo $parDados->par_parentesco=='Padrasto/Madrasta'?'selected="selected"':'';?> value="Padrasto/Madrasta">Padrasto/Madrasta</option>
                                                <option <?php echo $parDados->par_parentesco=='Primo(a)'?'selected="selected"':'';?> value="Primo(a)">Primo(a)</option>
                                                <option <?php echo $parDados->par_parentesco=='Sobrinho(a)'?'selected="selected"':'';?> value="Sobrinh(a)">Sobrinh(a)</option>
                                                <option <?php echo $parDados->par_parentesco=='Sogro(a)'?'selected="selected"':'';?> value="Sogro(a)">Sogro(a)</option>
                                                <option <?php echo $parDados->par_parentesco=='Tio(a)'?'selected="selected"':'';?> value="Tio(a)">Tio(a)</option>
                                                <option <?php echo $parDados->par_parentesco=='Outros'?'selected="selected"':'';?> value="Outros">OUTROS</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 282px;">
                                        <div class="input">
                                            <input style="width: 257px;" type="text" id="par_nome" maxlength="50" value="<?php echo $parDados->par_nome;?>" name="parentesco_01_nome"/>
                                            <input type="hidden" name="codIna" value="<?php echo $inadDados->ina_cod; ?>"/>
                                            <input type="hidden" name="cod" value="<?php echo $parDados->par_cod; ?>"/>
                                            <input type="hidden" name="inad_cod"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 196px;">
                                        <div style="width: 41px; padding-left: 1px;" class="label">
                                            <label for="parentesco_01_fone">Fone:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 124px;" type="text" id="par_fone" class="telefone" value="<?php echo $parDados->par_fone1;?>" name="parentesco_01_fone"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 87px; margin: 0px;">
                                        <div class="buttons">
                                            <div class="highlight">
                                                <button style="width: 85px; padding: 4px;" id="pesquisarPAR" name="pesquisarPAR" type="button" class="dialog-form-open">Pesquisar</button>
                                                <button style="width: 85px; padding: 4px;" id="cadastroPAR" name="cadastroPAR" type="button" class="dialog-form-open">Cadastro</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="par_novaPesquisa" style="text-align: center; display:none"><a href="#">Nova Pesquisa</a></div>
                                <div class="field">
                                    <div id="par">
                                        <div class="divleft" style="width: 240px;">
                                            <div style="width: 89px;" class="label">
                                                <label for="parentesco_01_fone2">Fone(2):</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 117px;" type="text" id="parentesco_01_fone2" class="telefone" value="<?php echo $parDados->par_fone2;?>" name="parentesco_01_fone2"/>
                                            </div>
                                        </div>
                                        <div class="divleft" style="width: 238px;">
                                            <div style="width: 77px; padding-left: 1px;" class="label">
                                                <label for="parentesco_01_fone3">Fone(3):</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 130px;" type="text" id="parentesco_01_fone3" class="telefone" value="<?php echo $parDados->par_fone3;?>" name="parentesco_01_fone3"/>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 216px; margin: 0px;">
                                            <div style="width: 74px; padding-left: 1px;" class="label">
                                                <label for="parentesco_01_fone4">Fone(4):</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 115px;" type="text" id="parentesco_01_fone4" class="telefone" value="<?php echo $parDados->par_fone4;?>" name="parentesco_01_fone4"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="text-align: center; margin-top: 10px;" class="buttons">
                                    <div style="margin-left: 7px;" class="highlight">
                                        <input type="submit" name="cadastrar" value="Atualizar parente" />
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>
                </form>
                <div class="fields">
                    <?php if($parentes!=null): ;?>
                    
                    <div>
                        <div class="blocoTitulo">
                            Parentes Cadastrados
                        </div>
                    </div>
                    <div class="table" style="padding: 0px 19px 10px;">
                        <table id="products">
                            <thead>
                                <tr>
                                    <th class="left">Parentesco</th>
                                    <th style="width: 205px">Nome</th>
                                    <th style="">Telefones</th>
                                    <th class="last">A��o</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($parentes as $parente): ?>
                                <tr>
                                    <td class="title"><?php echo $parente->par_parentesco ;?></td>
                                    <td><?php echo $parente->par_nome ;?></td>
                                    <td><?php echo $parente->par_fone1.' - '.$parente->par_fone2.' - '.$parente->par_fone3.' - '.$parente->par_fone4 ;?></td>
                                    <td class="last"><?php echo $parente->par_inad_cod!='0'?'<a href="'.base_url().'inadimplente/editar/cod:'.$parente->par_inad_cod.'" target="_blank">Cadastro</a>':'<a href="'.base_url().'inadimplente/editar/cod:'.$inadDados->ina_cod.'/parCod:'.$parente->par_cod.'">Editar</a>' ;?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <div id="bens">
                <form id="formBEM" action="<?php echo $bem==null?base_url().'inadimplente/cadBem':base_url().'inadimplente/atualizaBem'; ?>" method="post">
                    <div class="form">
                        <div class="fields">
                            <div class="blocoTitulo" style="margin-top: 18px;">
                                Dados do bem
                            </div>
                            <div class="field  field-first">
                                <div class="divleftlast" style="width: 400px; margin-left: 0px;">
                                    <div style="width: 85px;" class="label">
                                        <label>Tipo:</label>
                                    </div>
                                    <?php if($bem==null): ?>
                                    <input style="margin-top: 7px; " type="radio" name="tipo" value="I" checked>Im�vel
                                    <input type="radio" name="tipo" value="A">Ve�culo
                                    <?php endif;?>
                                    <?php if($bem!=null): ?>
                                    <input style="margin-top: 7px; " type="radio" name="tipo" value="I" <?php echo $bem->ben_tipo=='I'?'checked':''; ?>>Im�vel
                                    <input type="radio" name="tipo" value="A" <?php echo $bem->ben_tipo=='A'?'checked':''; ?> >Ve�culo
                                    <input type="hidden" name="bem_cod"  value="<?php echo $bem->ben_cod; ?>"/>
                                    <?php endif;?>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleftlast" style="width: 712px; margin-left: 0px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="titulo">Titulo:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 598px;" type="text" id="titulo" maxlength="50" name="titulo" <?php echo $bem==null?'':'value="'.utf8_decode($bem->ben_titulo).'"'; ?>/>
                                        <input type="hidden" name="cod"  value="<?php echo $inadDados->ina_cod; ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div style="height: 125px;" class="field">
                                <div class="divleftlast" style="width: 701px;">
                                    <div style="width: 84px;" class="label">
                                        <label for="detalhes">Detalhes:</label>
                                    </div>
                                    <div class="textarea">
                                        <textarea style="width: 586px; height: 100px;" id="detalhes" name="detalhes" cols="500" rows="5"><?php echo $bem==null?'':utf8_decode($bem->ben_detalhes); ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <?php if ($pesquisas!=null): ?>
                            <div class="blocoTitulo">
                                Hist�rico de pesquisas
                            </div>
                            <div class="table" style="padding: 0px 5px 10px;">
                                <table id="products">
                                    <thead>
                                        <tr>
                                            <th class="left">Data</th>
                                            <th style="width: 205px">Registro de Opera��es</th>
                                            <th class="last">Recuperador</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($pesquisas as $pesq):?>
                                        <tr>
                                            <td class="title"><?php echo convData($pesq->ros_data,'d') ?></td>
                                            <td><?php echo $pesq->ope_nome ?></td>
                                            <td class="last"><?php echo $pesq->usu_nome ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <?php endif; ?>
                            <div style="text-align: center; margin-top: 10px;" class="buttons">
                                <input type="reset" name="cancelar" value="Cancelar" />
                                <div style="margin-left: 7px;" class="highlight">
                                    <input type="submit" name="cadastrar" <?php echo $bem==null?'value="Cadastrar"':'value="Atualizar"'; ?> />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>