<script type="text/javascript">
        $(document).ready(function(){
            $('#valor').mask('99.99');

            $('.cadastrar').click(function(e){
                    e.preventDefault()
                        if($('input[name="nome"]').val() == ''){
                            $('input[name="nome"]').focus();
                            return false;
                        }else {
                            $('#formCad').submit();
                        }
            });
        });
</script>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Cadastro de tipo de opera��o</h5>
            </div>
            <?php echo $mensagem ?>
            <form id="formCad" action="<?php echo base_url().'cadopr/incluir'?>" method="post">
                <div class="form">
                    <div class="fields">
                        <div class="field  field-first">
                            <div class="divleft" style="width: 116px;">
                                <div class="label">
                                    <label>C�digo:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;">[NOVO]</p>
                            </div>
                            <div class="divleft" style="width: 486px;">
                                <div style="width: 157px; padding-left: 1px;" class="label">
                                    <label for="nome">Novo tipo de opera��o:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 298px;" type="text" id="nome" maxlength="45" name="nome"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <input style="width: 85px;" class="cadastrar" type="submit" name="cadastrar" value="Cadastrar" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 11px;" class="blocoTitulo">
                            Tipos de opera��o cadastrados
                        </div>
                        <div style="width: 710px; min-height: 700px; margin: 27px 0px 0px 5px;">
                            <?php $c=1; foreach ($operacoes as $ope):?>
                            <p class="opitem" style="display:block; line-height: 11px !important; height: 30px !important;<?php echo $c%3!=0?'width: 270px;':'width: 170px;' ?>"><?php echo utf8_decode($ope->ope_nome); ?></p>
                            <?php $c++; endforeach;?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
