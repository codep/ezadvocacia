<html>
    <body>
        <div id="content">

        <?php echo $sidebar; ?>

            <div id="right">
                <div class="box">
                    <div class="title">
                        <h5>Cadastro de repasse</h5>
                    </div>
                    <form id="form" action="" method="post">
                        <div class="form">
                            <div class="fields">
                                <div class="field  field-first">
                                    <div class="divleft" style="width: 428px;">
                                        <div style="width: 150px; padding-left: 1px;" class="label">
                                            <label for="nome">Novo tipo de repasse:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 246px;" type="text" id="nome" name="nome"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 156px;">
                                        <div style="width: 39px; padding-left: 1px;" class="label">
                                            <label for="nome">(%):</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 88px;" type="text" id="nome" name="nome"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 107px; margin: 0px;">
                                        <div class="buttons">
                                            <div class="highlight">
                                                <input style="width: 105px;" type="submit" name="cadastrar" value="Cadastrar" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 11px;" class="blocoTitulo">
                                    Tipos de repasse cadastrados
                                </div>
                                <div style="width: 710px; min-height: 700px; margin: 27px 0px 0px 5px;">
                                    <p class="opitem2" style="width: 700px;">Cobran�a terceirizada (90%)</p>
                                    <p class="opitem2" style="width: 700px;">Cobran�a comum (70%)</p>
                                    <p class="opitem2" style="width: 700px;">Cobran�a antiga (50%)</p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>