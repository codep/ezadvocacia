<html>
    <body>
<!-- Caixa de di�logo RO-->
        <div id="dialog-form" class="form" style="padding-left: 5px; padding-right: 5px;" title="Novo registro">
            <form id="form" action="" method="post">
                <div style="width: 400px; margin: 0 auto; margin-top: 20px;">
                    <select style="width: 400px;" id="tipo_doc" name="tipo_doc">
                        <option value="1">Tipo de opera��o</option>
                        <option value="2">Ligar para o inadimplente</option>
                        <option value="3">Enviar mensagem</option>
                        <option value="3">Enviar carta</option>
                        <option value="3">Fazer acordo</option>
                        <option value="3">Pagamento no balc�o</option>
                        <option value="3">Enviar para Jur�dico</option>
                        <option value="3">Simular Acordo</option>
                        <option value="3">Enviar notifica��o</option>
                        <option value="3">Enviar E-mail</option>
                    </select>
                </div>
                <div class="fields" style="width: 400px; margin: 0 auto; margin-top: 15px;">
                    <div class="field" style="width: 400px;">
                        <div style="width: 84px; padding-top: 0px;" class="label">
                            <label for="coisas">Observa��o:</label>
                        </div>
                        <div class="textarea">
                            <textarea style="width: 386px; height: 95px;" id="informacoes" name="coisas" cols="50" rows="10 " class="editor"></textarea>
                        </div>
                        <div style="margin-top: 15px;">
                            <div style="float: left; margin-right: 30px;">
                                <div style="width: 75px; padding-left: 1px;" class="label">
                                    <label for="data_agendamento">Agendar:</label>
                                </div>
                                <div style=" width: 69px;" class="input">
                                    <input type="text" id="data_agendamento" name="data_agendamento" class="date" />
                                </div>
                            </div>
                            <div style="float: left;">
                                <div style="width: 95px; padding-left: 1px;" class="label">
                                    <label for="tarefa">Tarefa:</label>
                                </div>
                                <div style=" width: 269px;" class="input">
                                    <input type="text" id="tarefa" name="tarefa" style="width: 229px;" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center; margin-top: 10px; margin-bottom: 10px;" class="buttons">
                        <input type="reset" name="cancelar" value="Cancelar" />
                        <div style="margin-left: 7px;" class="highlight">
                            <input type="submit" name="submit.highlight" value="Cadastrar" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
<!-- FIM Caixa de di�logo RO-->
        <div id="content">

            <?php echo $sidebar; ?>

            <div id="right">
                <div class="box">
                    <div class="title">
                        <h5>Nova d�vida (24230)</h5>
                    </div>
                    <form id="form" action="" method="post">
                        <div class="form">
                            <div class="fields">
                                <div style="margin-top: 0px; margin-bottom: 13px;" class="blocoTitulo">
                                    Dados do credor
                                </div>
                                <div class="field  field-first">
                                    <div class="divleftleft" style="height:29px; width: 200px;">
                                        <div class="label">
                                            <label>Pessoa:</label>
                                        </div>
                                        <input style="margin-top: 7px; " type="radio" name="credor_pessoa" value="fisica" checked>F�sica
                                        <input type="radio" name="credor_pessoa" value="juridica">Jur�dica
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="width: 371px;">
                                        <div style="width: 89px;" class="label">
                                            <label for="credor_nome">Nome:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 248px;" type="text" id="credor_nome" name="credor_nome"/>
                                        </div>
                                    </div>

                                    <div class="divleft" style="width: 231px;">
                                        <div style="width: 76px; padding-left: 1px;" class="label">
                                            <label for="credor_cpf_cnpj">CNPJ/CPF:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 124px;" type="text" id="credor_cpf_cnpj" name="credor_cpf_cnpj"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 87px; margin: 0px;">
                                        <div class="buttons">
                                            <div class="highlight">
                                                <input style="width: 85px;" class="dialog-form-open" type="submit" name="credor_pesquisar" value="Pesquisar" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 12px; margin-bottom: 13px;" class="blocoTitulo">
                                    Dados do inadimplente
                                </div>
                                <div class="field  field-first">
                                    <div class="divleftleft" style="height:29px; width: 200px;">
                                        <div class="label">
                                            <label>Pessoa:</label>
                                        </div>
                                        <input style="margin-top: 7px; " type="radio" name="inad_pessoa" value="fisica" checked>F�sica
                                        <input type="radio" name="inad_pessoa" value="juridica">Jur�dica
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="width: 371px;">
                                        <div style="width: 89px;" class="label">
                                            <label for="inad_nome">Nome:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 248px;" type="text" id="inad_nome" name="inad_nome"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 231px;">
                                        <div style="width: 76px; padding-left: 1px;" class="label">
                                            <label for="inad_cpf_cnpj">CPF/CNPJ:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 124px;" type="text" id="inad_cpf_cnpj" name="inad_cpf_cnpj"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 87px; margin: 0px;">
                                        <div class="buttons">
                                            <div class="highlight">
                                                <input style="width: 85px;" class="dialog-form-open" type="submit" name="inad_pesquisar" value="Pesquisar" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 12px; margin-bottom: 9px;" class="blocoTitulo">
                                    Dados da d�vida
                                </div>
                                <div style="padding-top: 5px;" class="field">
                                    <div class="divleft" style="width: 136px;">
                                        <div style="margin-left: 6px;" class="select">
                                            <select style="width: 122px;" id="tipo_doc" name="tipo_doc">
                                                <option value="1">Tipo de doc.</option>
                                                <option value="2">(002)</option>
                                                <option value="3">(003)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 92px;">
                                        <div style="margin-left: 2px;" class="select">
                                            <select style="width: 82px;" id="banco" name="banco">
                                                <option value="1">Banco</option>
                                                <option value="2">(002)</option>
                                                <option value="3">(003)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="margin-left: 2px; width:98px;">
                                        <div style="width: 26px; padding-left: 1px;" class="label">
                                            <label for="agencia">AG.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 42px;" type="text" id="agencia" name="agencia"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 46px;">
                                        <div class="input">
                                            <input style="width: 18px;" type="text" id="agencia_dig" name="agencia_dig"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="margin-left: 2px; width:117px;">
                                        <div style="width: 46px; padding-left: 1px;" class="label">
                                            <label for="alinea">Al�nea:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 39px;" type="text" id="alinea" name="alinea"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="margin-left: 2px; width:176px;">
                                        <div style="width: 62px; padding-left: 1px;" class="label">
                                            <label for="emissao">Emiss�o:</label>
                                        </div>
                                        <div style=" width: 109px;" class="input">
                                            <input type="text" id="emissao" name="emissao" class="date" />
                                        </div>
                                    </div>
                                </div>    
                                <div class="field" style="margin-bottom: 0px;">
                                    <div class="divleft" style="width: 219px;">
                                        <div style="width: 165px;" class="label">
                                            <label for="parcelas">Quantidade de parcelas:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 19px;" type="text" id="parcelas" name="parcelas"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 252px;">
                                        <div style="width: 122px; padding-left: 1px;" class="label">
                                            <label for="total_divida">Total d�vida (R$):</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 99px;" type="text" id="total_divida" name="total_divida"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 217px;margin: 0px; margin-left: 1px;">
                                        <div class="buttons">
                                            <div class="highlight">
                                                <input style="width: 214px;" type="submit" name="gerar" value="Gerar parcelas" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="width: 151px;">
                                        <div style="width: 64px; padding-left: 5px;" class="label">
                                            <label for="doc_par_1">N�.Doc.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 53px;" type="text" id="doc_par_1" name="doc_par_1"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 128px;">
                                        <div style="width: 80px; padding-left: 0px;" class="label">
                                            <label for="num_par_1">Parcela N�.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 18px;" type="text" id="num_par_1" name="num_par_1"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 242px;">
                                        <div style="width: 112px; padding-left: 1px;" class="label">
                                            <label for="valor_par_1">V.Parcela (R$):</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 99px;" type="text" id="valor_par_1" name="valor_par_1"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="margin-left: 0px; width: 159px;">
                                        <div style="width: 45px; padding-left: 1px;" class="label">
                                            <label for="venc_par_1">Venc.:</label>
                                        </div>
                                        <div style=" width: 109px;" class="input">
                                            <input type="text" id="venc_par_1" name="venc_par_1" class="date" />
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="width: 151px;">
                                        <div style="width: 64px; padding-left: 5px;" class="label">
                                            <label for="doc_par_2">N�.Doc.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 53px;" type="text" id="doc_par_2" name="doc_par_2"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 128px;">
                                        <div style="width: 80px; padding-left: 0px;" class="label">
                                            <label for="num_par_2">Parcela N�.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 18px;" type="text" id="num_par_2" name="num_par_2"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 242px;">
                                        <div style="width: 112px; padding-left: 1px;" class="label">
                                            <label for="valor_par_2">V.Parcela (R$):</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 99px;" type="text" id="valor_par_2" name="valor_par_2"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="margin-left: 0px; width: 159px;">
                                        <div style="width: 45px; padding-left: 1px;" class="label">
                                            <label for="venc_par_2">Venc.:</label>
                                        </div>
                                        <div style=" width: 109px;" class="input">
                                            <input type="text" id="venc_par_2" name="venc_par_2" class="date" />
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="width: 151px;">
                                        <div style="width: 64px; padding-left: 5px;" class="label">
                                            <label for="doc_par_3">N�.Doc.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 53px;" type="text" id="doc_par_3" name="doc_par_3"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 128px;">
                                        <div style="width: 80px; padding-left: 0px;" class="label">
                                            <label for="num_par_3">Parcela N�.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 18px;" type="text" id="num_par_3" name="num_par_3"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 242px;">
                                        <div style="width: 112px; padding-left: 1px;" class="label">
                                            <label for="valor_par_3">V.Parcela (R$):</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 99px;" type="text" id="valor_par_3" name="valor_par_3"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="margin-left: 0px; width: 159px;">
                                        <div style="width: 45px; padding-left: 1px;" class="label">
                                            <label for="venc_par_3">Venc.:</label>
                                        </div>
                                        <div style=" width: 109px;" class="input">
                                            <input type="text" id="venc_par_3" name="venc_par_3" class="date" />
                                        </div>
                                    </div>
                                </div>
                                <div style="height: 74px; margin-top: 5px;" class="field">
                                    <div class="divleftlast" style="width: 701px;">
                                        <div style="width: 84px; padding-top: 0px;" class="label">
                                            <label for="informacoes">Outras informa��es:</label>
                                        </div>
                                        <div class="textarea">
                                            <textarea style="width: 586px; height: 50px;" id="informacoes" name="informacoes" cols="500" rows="4" class="editor"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 12px; margin-bottom: 3px; padding-bottom: 2px;" class="blocoTitulo">
                                    Recuperador padr�o: FULANO
                                </div>
                                <div class="field">
                                    <div style="margin-left: 221px;  border: none; width: 245px;">
                                        <div style="margin-left: 6px;" class="select">
                                            <select style="width: 260px;" id="recuperador" name="recuperador">
                                                <option value="1">Fulano</option>
                                                <option value="2">Beltrano</option>
                                                <option value="3">Sicrano</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div style="text-align: center; margin-top: 10px;" class="buttons">
                                    <input type="reset" name="cancelar" value="Cancelar" />
                                    <div style="margin-left: 7px;" class="highlight">
                                        <input type="submit" name="submit.highlight" value="Cadastrar" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>