<script type="text/javascript">
        $(document).ready(function(){
            $('input[value="Ficha"]').click(function(e){
                    window.location.href='<?php echo base_url();?>divida/ficha/cod:'+$(this).attr('id');
            });
            validacao = "<?php if(isset($aba)) echo $aba?>";
            if(validacao != ''){
                $('a[href='+validacao+']').trigger('click');
            }

        });
</script>
<div id="content">
<?php echo $sidebar; ?>
    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Minhas cobran�as administrativas</h5>
                <ul class="links">
                    <li><a href="#virgem">Virgem</a></li>
                    <li><a href="#andamento">Em andamento</a></li>
                    <li><a href="#acordo">Em acordo</a></li>
                    <li><a href="#atraso">Em atraso</a></li>
                </ul>
            </div>
            <div id="virgem">
                <div class="form">
                    <div class="fields">
                        <form id="form" method="post" action="<?php echo base_url().'divida/listaradm' ?>">
                            <div class="field  field-first">
                                <div class="divleft" style="width: 286px;">
                                    <div style="width: 69px; padding-left: 1px;" class="label">
                                        <label for="nome">Filtro:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 188px;" type="text" id="txPesquisar" name="pesquisar"/>
                                        <input type="hidden" name="aba" value="#virgem"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 87px; margin: 0px;">
                                    <div class="buttons">
                                        <div class="highlight">
                                            <input style="width: 85px;" type="submit" name="filtrar" value="Filtrar" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="table" style="padding: 0px 5px 10px;">
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th class="left" style="width: 30px;">Cod.</th>
                                        <th>Inadimplente</th>
                                        <th>Credor</th>
                                        <th>Valor</th>
                                        <th class="last">Visualizar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($minhasCobrancasVirgensAdm as $minhaCobVirgAdm):?>
                                    <tr>
                                        <td class="title" style="width: 30px;"><?php echo $minhaCobVirgAdm->cob_cod;?></td>
                                        <td><?php echo utf8_decode($minhaCobVirgAdm->ina_nome);?></td>
                                        <td><?php echo utf8_decode($minhaCobVirgAdm->cre_nome_fantasia);?></td>
                                        <td><?php echo convMoney($minhaCobVirgAdm->div_total);?></td>
                                        <td class="last">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" class="visualizar" type="submit" name="cadastrar" value="Ficha"  id="<?php echo $minhaCobVirgAdm->ina_cod;?>"/>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="acordo">
                <div class="form">
                    <div class="fields">
                        <form id="form" action="<?php echo base_url().'divida/listaradm' ?>" method="post">
                            <div class="field  field-first">
                                <div class="divleft" style="width: 286px;">
                                    <div style="width: 69px; padding-left: 1px;" class="label">
                                        <label for="nome">Filtro:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 188px;" type="text" id="nome" name="pesquisar"/>
                                        <input type="hidden" name="aba" value="#acordo"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 87px; margin: 0px;">
                                    <div class="buttons">
                                        <div class="highlight">
                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="table" style="padding: 0px 5px 10px;">
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th class="left" style="width: 30px;">Cod.</th>
                                        <th>Inadimplente</th>
                                        <th>Credor</th>
                                        <th>Valor</th>
                                        <th>�ltimo RO</th>
                                        <th class="last">Visualizar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($minhasCobrancasEmAcordoAdm as $minhaCobAcor):?>
                                    <tr>
                                        <td class="title" style="width: 30px;"><?php echo $minhaCobAcor->cob_cod;?></td>
                                        <td><?php echo utf8_decode($minhaCobAcor->ina_nome);?></td>
                                        <td><?php echo utf8_decode($minhaCobAcor->cre_nome_fantasia);?></td>
                                        <td><?php echo convMoney($minhaCobAcor->aco_valor_atualizado);?></td>
                                        <td>
                                            <?php echo utf8_decode($minhaCobAcor->ultimo_ro);?>
                                        </td>
                                        <td class="last">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" class="visualizar" type="submit" name="cadastrar" value="Ficha" id="<?php echo $minhaCobAcor->ina_cod;?>" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="andamento">
                <div class="form">
                    <div class="fields">
                        <form id="form" action="<?php echo base_url().'divida/listaradm' ?>" method="post">
                            <div class="field  field-first">
                                <div class="divleft" style="width: 286px;">
                                    <div style="width: 69px; padding-left: 1px;" class="label">
                                        <label for="nome">Filtro:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 188px;" type="text" id="nome" name="pesquisar"/>
                                        <input type="hidden" name="aba" value="#andamento"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 87px; margin: 0px;">
                                    <div class="buttons">
                                        <div class="highlight">
                                            <input style="width: 85px;" type="submit" name="btnFiltrar" value="Filtrar" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="table" style="padding: 0px 5px 10px;">
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th class="left" style="width: 30px;">Cod.</th>
                                        <th>Inadimplente</th>
                                        <th>Credor</th>
                                        <th>Valor</th>
                                        <th>�ltimo RO</th>
                                        <th class="last">Visualizar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($minhasCobrancasEmAndamentoAdm as $minhaCobAnd):?>
                                    <tr>
                                        <td class="title" style="width: 30px;"><?php echo $minhaCobAnd->cob_cod;?></td>
                                        <td><?php echo utf8_decode($minhaCobAnd->ina_nome);?></td>
                                        <td><?php echo utf8_decode($minhaCobAnd->cre_nome_fantasia);?></td>
                                        <td><?php echo convMoney($minhaCobAnd->div_total);?></td>

                                        <td>
                                           <?php echo utf8_decode($minhaCobAnd->ultimo_ro);?>
                                        </td>
                                        <td class="last">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" class="visualizar" type="submit" name="cadastrar" value="Ficha" id="<?php echo $minhaCobAnd->ina_cod;?>"/>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="atraso">
                <div class="form">
                    <div class="fields">
                        <form id="form" action="<?php echo base_url().'divida/listaradm' ?>" method="post">
                            <div class="field  field-first">
                                <div class="divleft" style="width: 286px;">
                                    <div style="width: 69px; padding-left: 1px;" class="label">
                                        <label for="nome">Filtro:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 188px;" type="text" id="nome" name="pesquisar"/>
                                        <input type="hidden" name="aba" value="#atraso"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 87px; margin: 0px;">
                                    <div class="buttons">
                                        <div class="highlight">
                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="table" style="padding: 0px 5px 10px;">
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th class="left" style="width: 30px;">Cod.</th>
                                        <th>Inadimplente</th>
                                        <th>Credor</th>
                                        <th>Valor</th>
                                        <th>Vencimento</th>
                                        <th class="last">Visualizar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($minhasCobrancasEmAtrasoAdm as $minhaCobAtr):?>
                                    <tr>
                                        <td class="title" style="width: 30px;"><?php echo $minhaCobAtr->cob_cod;?></td>
                                        <td><?php echo utf8_decode($minhaCobAtr->ina_nome);?></td>
                                        <td><?php echo utf8_decode($minhaCobAtr->cre_nome_fantasia);?></td>
                                        <td><?php echo convMoney($minhaCobAtr->div_total);?></td>
                                        <td><?php echo convDataBanco($minhaCobAtr->div_venc_inicial);?></td>
                                        <td class="last">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" class="visualizar" type="submit" name="cadastrar" value="Ficha" id="<?php echo $minhaCobAtr->ina_cod;?>"/>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>