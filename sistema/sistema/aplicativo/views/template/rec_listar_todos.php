<script type="text/javascript">
        $(document).ready(function(){
            $('.visualizar').click(function(e){
                    window.location.href='http://webhost/cobranca/divida/detalhes';
            });
            $('input[value="Ficha"]').click(function(e){
                    window.location.href='http://webhost/cobranca/divida/ficha';
            });

        });
</script>
<html>
    <body>
        <div id="content">

        <?php echo $sidebar; ?>

            <div id="right">
                <div id="box-tabs" class="box" style="min-height: 798px;">
                    <div class="title">
                        <h5>Todos os Recebimentos</h5>
                        <ul class="links">
                            <li><a href="#hoje">Hoje</a></li>
                            <li><a href="#semana">Semana</a></li>
                            <li><a href="#mes">M�s</a></li>
                            <li><a href="#proxmes">M�s anterior</a></li>
                        </ul>
                    </div>
                    <div id="hoje">
                        <div class="form">
                            <div class="fields">
                                <form id="form" method="post" action="todos">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 345px;">
                                            <div style="width: 129px; padding-left: 1px;" class="label">
                                                <label for="nome">Selecionar credor:</label>
                                            </div>
                                            <div class="select">
                                                <select style="width: 205px; margin-left: 5px;" id="est_civil" name="filtroCredor">
                                                    <option value="todos">---Todos---</option>
                                                      <?php foreach ($todosCredores as $credor): ?>
                                                    <option value="<?php echo $credor->cre_cod ?>"<?php echo $credor->cre_cod == $filtroCredor ? 'selected' : ''; ?>><?php echo $credor->cre_nome_fantasia ?></option>
                                                      <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="filtrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 40px;">C�d</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Recebido</th>
                                                <th>Recuperador</th>
                                                <th>Baixador</th>
                                                <th style="width: 85px;">Valor</th>
                                                <th class="last" style="width: 55px;">2� Via</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $totalRecebido = 0; $totalDescontos=0; foreach ($todosRecebimentosHoje as $todosRecHoje): ?>
                                            <tr>
                                                <td class="title" style="width: 50px;"><?php echo $todosRecHoje->cobranca_cob_cod ?></td>
                                                <td><?php echo utf8_decode($todosRecHoje->ina_nome);?></td>
                                                <td><?php echo utf8_decode($todosRecHoje->cre_nome_fantasia);?></td>
                                                <td><?php echo convDataBanco($todosRecHoje->reb_data);?></td>
                                                <td><?php echo utf8_decode($todosRecHoje->recuperador);?></td>
                                                <td><?php echo utf8_decode($todosRecHoje->reb_baixador)?></td>
                                                <td><?php echo convMoney($todosRecHoje->reb_valor);?></td>
                                                <td class="last">
                                                    <a href="<?php echo base_url() . 'impressao/recibo2/cod:' . $todosRecHoje->rec_cod ?>">
                                                        <img src="<?php echo $img . 'recibo_imprimir.png' ?>" alt="+"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php $totalRecebido += $todosRecHoje->reb_valor;
                                                  $totalDescontos += $todosRecHoje->reb_desconto;
                                                  endforeach; ?>
                                            <tr>
                                            <tr>
                                                <td class="title" colspan="6" style="width: 30px;"><b>Total</b></td>
                                                <td class="last" colspan="2">
                                                    <b><?php echo convMoney($totalRecebido - $totalDescontos);?></b>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="semana">
                        <div class="form">
                            <div class="fields">
                                <form id="form" method="post" action="todos">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 345px;">
                                            <div style="width: 129px; padding-left: 1px;" class="label">
                                                <label for="nome">Selecionar credor:</label>
                                            </div>
                                            <div class="select">
                                                <select style="width: 205px; margin-left: 5px;" id="est_civil" name="filtroCredor">
                                                    <option value="todos">---Todos---</option>
                                                    <?php foreach ($todosCredores as $credor): ?>
                                                    <option value="<?php echo $credor->cre_cod ?>"<?php echo $credor->cre_cod == $filtroCredor ? 'selected' : ''; ?>><?php echo $credor->cre_nome_fantasia ?></option>
                                                  <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="filtrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 40px;">C�d</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Recebido</th>
                                                <th>Recuperador</th>
                                                <th>Baixador</th>
                                                <th style="width: 85px;">Valor</th>
                                                <th class="last" style="width: 62px;">2� Via</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php $totalRecebido = 0; $totalDescontos=0; foreach ($todosRecebimentosSemana as $todosRecSemana): ?>
                                            <tr>
                                                <td class="title" style="width: 50px;"><?php echo $todosRecSemana->cobranca_cob_cod ?></td>
                                                <td><?php echo utf8_decode($todosRecSemana->ina_nome);?></td>
                                                <td><?php echo utf8_decode($todosRecSemana->cre_nome_fantasia);?></td>
                                                <td><?php echo convDataBanco($todosRecSemana->reb_data);?></td>
                                                <td><?php echo utf8_decode($todosRecSemana->recuperador);?></td>
                                                <td><?php echo utf8_decode($todosRecSemana->reb_baixador);?></td>
                                                <td><?php echo convMoney($todosRecSemana->reb_valor);?></td>
                                                <td class="last">
                                                    <a href="<?php echo base_url() . 'impressao/recibo2/cod:' . $todosRecSemana->rec_cod ?>">
                                                        <img src="<?php echo $img . 'recibo_imprimir.png' ?>" alt="+"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php $totalRecebido += $todosRecSemana->reb_valor;
                                                   $totalDescontos += $todosRecSemana->reb_desconto;
                                            endforeach; ?>
                                            <tr>
                                                <td class="title" colspan="6" style="width: 30px;"><b>Total</b></td>
                                                <td colspan="2" class="last">
                                                    <b><?php echo convMoney($totalRecebido - $totalDescontos);?></b>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="mes">
                        <div class="form">
                            <div class="fields">
                                <form id="form" method="post" action="todos">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 345px;">
                                            <div style="width: 129px; padding-left: 1px;" class="label">
                                                <label for="nome">Selecionar credor:</label>
                                            </div>
                                            <div class="select">
                                                <select style="width: 205px; margin-left: 5px;" id="est_civil" name="filtroCredor">
                                                    <option value="todos">---Todos---</option>
                                                    <?php foreach ($todosCredores as $credor): ?>
                                                    <option value="<?php echo $credor->cre_cod ?>"<?php echo $credor->cre_cod == $filtroCredor ? 'selected' : ''; ?>><?php echo $credor->cre_nome_fantasia ?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="filtrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 40px;">C�d</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Recebido</th>
                                                <th>Recuperador</th>
                                                <th>Baixador</th>
                                                <th style="width: 85px;">Valor</th>
                                                <th class="last" style="width: 62px;">2� Via</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $totalRecebido = 0; $totalDescontos=0; foreach ($todosRecebimentosMes as $todosRecMes): ?>
                                            <tr>
                                                <td class="title" style="width: 50px;"><?php echo $todosRecMes->cobranca_cob_cod ?></td>
                                                <td><?php echo utf8_decode($todosRecMes->ina_nome);?></td>
                                                <td><?php echo utf8_decode($todosRecMes->cre_nome_fantasia);?></td>
                                                <td><?php echo convDataBanco($todosRecMes->reb_data);?></td>
                                                <td><?php echo utf8_decode($todosRecMes->recuperador);?></td>
                                                <td><?php echo utf8_decode($todosRecMes->reb_baixador);?></td>
                                                <td><?php echo convMoney($todosRecMes->reb_valor);?></td>
                                                <td class="last">
                                                    <a href="<?php echo base_url() . 'impressao/recibo2/cod:' . $todosRecMes->rec_cod ?>">
                                                        <img src="<?php echo $img . 'recibo_imprimir.png' ?>" alt="+"/>
                                                    </a>
                                                </td>
                                            </tr>
                                             <?php $totalRecebido += $todosRecMes->reb_valor;
                                                   $totalDescontos += $todosRecMes->reb_desconto;
                                                   endforeach;
                                            ?>
                                            <tr>
                                                <td class="title" colspan="6" style="width: 30px;"><b>Total</b></td>
                                                <td colspan="2" class="last">
                                                    <b><?php echo convMoney($totalRecebido - $totalDescontos);?></b>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="proxmes">
                        <div class="form">
                            <div class="fields">
                                <form id="form" method="post" action="todos">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 345px;">
                                            <div style="width: 129px; padding-left: 1px;" class="label">
                                                <label for="nome">Selecionar credor:</label>
                                            </div>
                                            <div class="select">
                                                <select style="width: 205px; margin-left: 5px;" id="est_civil" name="filtroCredor">
                                                    <option value="todos">---Todos---</option>
                                                    <?php foreach ($todosCredores as $credor): ?>
                                                    <option value="<?php echo $credor->cre_cod ?>"<?php echo $credor->cre_cod == $filtroCredor ? 'selected' : ''; ?>><?php echo $credor->cre_nome_fantasia ?></option>
                                                    <?php endforeach;?>                                                   
                                                </select>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 40px;">C�d</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Recebido</th>
                                                <th>Recuperador</th>
                                                <th>Baixador</th>
                                                <th style="width: 85px;">Valor</th>
                                                <th class="last" style="width: 62px;">2� Via</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $totalRecebido = 0; $totalDescontos=0; foreach ($todosRecebimentosProxMes as $todosRecProxMes): ?>
                                            <tr>
                                                <td class="title" style="width: 30px;"><?php echo $todosRecProxMes->cobranca_cob_cod?></td>
                                                <td><?php echo utf8_decode($todosRecProxMes->ina_nome);?></td>
                                                <td><?php echo utf8_decode($todosRecProxMes->cre_nome_fantasia);?></td>
                                                <td><?php echo convDataBanco($todosRecProxMes->reb_data);?></td>
                                                <td><?php echo utf8_decode($todosRecProxMes->recuperador);?></td>
                                                <td><?php echo utf8_decode($todosRecProxMes->reb_baixador);?></td>
                                                <td><?php echo convMoney(($todosRecProxMes->reb_valor));?></td>
                                                <td class="last">
                                                    <a href="<?php echo base_url() . 'impressao/recibo2/cod:' . $todosRecProxMes->rec_cod ?>">
                                                        <img src="<?php echo $img . 'recibo_imprimir.png' ?>" alt="+"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php $totalRecebido += $todosRecProxMes->reb_valor;
                                                   $totalDescontos += $todosRecProxMes->reb_desconto;
                                                   endforeach;
                                            ?>
                                            <tr>
                                                <td class="title" colspan="6" style="width: 30px;"><b>Total</b></td>
                                                <td colspan="2" class="last">
                                                    <b><?php echo convMoney($totalRecebido - $totalDescontos);?></b>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>