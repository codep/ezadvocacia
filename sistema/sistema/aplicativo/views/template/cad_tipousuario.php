<script type="text/javascript">
    $(document).ready(function(){
        $('.cadastrar').click(function(e){
            e.preventDefault()
            if($('input[name="gruponome"]').val() == ''){
                $('input[name="gruponome"]').focus();
                return false;
            }else {
                $('#formCad').submit();
            }
        });
    });
</script>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <form id="formCad" action="<?php echo base_url() . 'cadtipousu/incluir'; ?>" method="post">
            <div class="box">
                <div class="title">
                    <h5>Novo grupo de usu�rio</h5>
                </div>
                <div class="form">
                    <div class="fields">
                        <div style="border: none;" class="field  field">
                            <div class="divleft" style="width: 116px;">
                                <div class="label">
                                    <label>C�digo:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;">[NOVO]</p>
                            </div>
                            <div class="divleftlast" style="width: 491px;">
                                <div style="width: 119px;" class="label">
                                    <label for="input-medium">Nome do grupo:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 248px;" type="text" id="input-medium" maxlength="23" name="gruponome" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="box-tabs" class="box">
                <div class="title">
                    <h5>Permiss�es</h5>
                    <ul class="links">
                        <li><a href="#cad">Cadastros</a></li>
                        <li><a href="#ger">Gerenciamento</a></li>
                        <li><a href="#cob">Cobran�as</a></li>
                        <li><a href="#relat">Relat�rios</a></li>
                        <li><a href="#outros">Outros</a></li>
                    </ul>
                </div>
                <div id="cad">
                    <div class="form">
                        <div class="fields">
                            <div style="border: none;" class="field">
                                <fieldset class="fieldsetsta">
                                    <legend class="forte">
                                        Cadastros:
                                    </legend>
                                    <div>
                                        <input type="radio" name="cadastro" value="1">Acessa
                                        <input type="radio" name="cadastro" value="0" checked >N�o Acessa
                                    </div>
                                    <!--==============-->
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Nova d�vida:
                                        </legend>
                                        <div>
                                            <input type="radio" name="cad_divida" value="1">Acessa
                                            <input type="radio" name="cad_divida" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Novo inadimplente:
                                        </legend>
                                        <div>
                                            <input type="radio" name="cad_inad" value="1">Acessa
                                            <input type="radio" name="cad_inad" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Novo credor:
                                        </legend>
                                        <div>
                                            <input type="radio" name="cad_cred" value="1">Acessa
                                            <input type="radio" name="cad_cred" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Novo bem:
                                        </legend>
                                        <div>
                                            <input type="radio" name="cad_bem" value="1">Acessa
                                            <input type="radio" name="cad_bem" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Tipo de opera��o:
                                        </legend>
                                        <div>
                                            <input type="radio" name="cad_opr" value="1">Acessa
                                            <input type="radio" name="cad_opr" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Usu�rios:
                                        </legend>
                                        <div>
                                            <input type="radio" name="cad_usu" value="1">Acessa
                                            <input type="radio" name="cad_usu" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Grupo de usu�rios:
                                        </legend>
                                        <div>
                                            <input type="radio" name="cad_gru" value="1">Acessa
                                            <input type="radio" name="cad_gru" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Cartas:
                                        </legend>
                                        <div>
                                            <input type="radio" name="cartas" value="1">Acessa
                                            <input type="radio" name="cartas" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                </fieldset>

                            </div>
                        </div>
                    </div>
                </div>
                <div id="ger">
                    <div class="form">
                        <div class="fields">
                            <div style="border: none;" class="field">
                                <fieldset class="fieldsetsta">
                                    <legend class="forte">
                                        Ger�nciamento
                                    </legend>
                                    <div>
                                        <input type="radio" name="gerenciamento" value="1">Acessa
                                        <input type="radio" name="gerenciamento" value="0" checked >N�o Acessa
                                    </div>
                                    <!--==============-->
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Repasse:
                                        </legend>
                                        <div>
                                            <input type="radio" name="ger_repasse" value="1">Acessa
                                            <input type="radio" name="ger_repasse" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Remanejar credor:
                                        </legend>
                                        <div>
                                            <input type="radio" name="ger_remCred" value="1">Acessa
                                            <input type="radio" name="ger_remCred" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Remanejar inadimplente:
                                        </legend>
                                        <div>
                                            <input type="radio" name="ger_remInad" value="1">Acessa
                                            <input type="radio" name="ger_remInad" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            listar:
                                        </legend>
                                        <div>
                                            <input type="radio" name="ger_listar" value="1">Acessa
                                            <input type="radio" name="ger_listar" value="0" checked >N�o Acessa
                                        </div>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Todas as cobran�as
                                            </legend>
                                            <div>
                                                <input type="radio" name="ger_list_cob" value="1">Acessa
                                                <input type="radio" name="ger_list_cob" value="0" checked >N�o Acessa
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Todas os inadimplentes
                                            </legend>
                                            <div>
                                                <input type="radio" name="ger_list_inad" value="1">Acessa
                                                <input type="radio" name="ger_list_inad" value="0" checked >N�o Acessa
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Todos os credores
                                            </legend>
                                            <div>
                                                <input type="radio" name="ger_list_cred" value="1">Acessa
                                                <input type="radio" name="ger_list_cred" value="0" checked >N�o Acessa
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Devolu��o
                                            </legend>
                                            <div>
                                                <input type="radio" name="ger_devolucao" value="1">Acessa
                                                <input type="radio" name="ger_devolucao" value="0" checked >N�o Acessa
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Listar Importa��o
                                            </legend>
                                            <div>
                                                <input type="radio" name="listar_imp" value="1">Acessa
                                                <input type="radio" name="listar_imp" value="0" checked >N�o Acessa
                                            </div>
                                        </fieldset>
                                    </fieldset>
                                </fieldset>

                            </div>
                        </div>
                    </div>
                </div>
                <div id="cob">
                    <div class="form">
                        <div class="fields">
                            <div style="border: none;" class="field">
                                <fieldset class="fieldsetsta">
                                    <legend class="forte">
                                        Cobran�as Administrativas:
                                    </legend>
                                    <div>
                                        <input type="radio" name="cobAdm" value="1">Acessa
                                        <input type="radio" name="cobAdm" value="0" checked >N�o Acessa
                                    </div>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Minhas cobran�as:
                                        </legend>
                                        <div>
                                            <input type="radio" name="cobAdm_minhas" value="1">Acessa
                                            <input type="radio" name="cobAdm_minhas" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Listar todas:
                                        </legend>
                                        <div>
                                            <input type="radio" name="cobAdm_todas" value="1">Acessa
                                            <input type="radio" name="cobAdm_todas" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Recebimentos:
                                        </legend>
                                        <div>
                                            <input type="radio" name="cobAdm_receb" value="1">Acessa
                                            <input type="radio" name="cobAdm_receb" value="0" checked >N�o Acessa
                                        </div>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Meus:
                                            </legend>
                                            <div>
                                                <input type="radio" name="cobAdm_receb_meus" value="1">Acessa
                                                <input type="radio" name="cobAdm_receb_meus" value="0" checked >N�o Acessa
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Todos:
                                            </legend>
                                            <div>
                                                <input type="radio" name="cobAdm_receb_todos" value="1">Acessa
                                                <input type="radio" name="cobAdm_receb_todos" value="0" checked >N�o Acessa
                                            </div>
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Previs�es:
                                            </legend>
                                            <div>
                                                <input type="radio" name="cobAdm_receb_prev" value="1">Acessa
                                                <input type="radio" name="cobAdm_receb_prev" value="0" checked >N�o Acessa
                                            </div>
                                        </fieldset>
                                    </fieldset>
                                </fieldset>
                                <fieldset class="fieldsetsta">
                                    <legend class="forte">
                                        Cobran�as Judiciais:
                                    </legend>
                                    <div>
                                        <input type="radio" name="cobJud" value="1">Acessa
                                        <input type="radio" name="cobJud" value="0" checked >N�o Acessa
                                    </div>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Listar:
                                        </legend>
                                        <div>
                                            <input type="radio" name="cobJud_listar" value="1">Acessa
                                            <input type="radio" name="cobJud_listar" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Acordos extintos:
                                        </legend>
                                        <div>
                                            <input type="radio" name="cobJud_acordosExt" value="1">Acessa
                                            <input type="radio" name="cobJud_acordosExt" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Importa��o:
                                        </legend>
                                        <div>
                                            <input type="radio" name="cobJud_importar" value="1">Acessa
                                            <input type="radio" name="cobJud_importar" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="relat">
                    <div id="relat">
                        <div class="form">
                            <div class="fields">
                                <div style="border: none;" class="field">
                                    <fieldset class="fieldsetsta">
                                        <legend class="forte">
                                            Relat�rios:
                                        </legend>
                                            <input type="radio" name="relatorios" value="1"/>Acessa
                                            <input type="radio" name="relatorios" value="0" checked/>N�o Acessa
                                    <!--==============-->
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Relat�rios administrativos:
                                            </legend>
                                                <input type="radio" name="relatAdm" value="1">Acessa
                                                <input type="radio" name="relatAdm" value="0" checked>N�o Acessa
                                        </fieldset>
                                        <fieldset class="fieldsetstain">
                                            <legend>
                                                Relat�rios judiciais:
                                            </legend>
                                                <input type="radio" name="relatJud" value="1">Acessa
                                                <input type="radio" name="relatJud" value="0" checked>N�o Acessa
                                        </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Relat�rios por RO:
                                        </legend>
                                            <input type="radio" name="relatRo" value="1">Acessa
                                            <input type="radio" name="relatRo" value="0" checked>N�o Acessa
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Relat�rios gerenciais:
                                        </legend>
                                            <input type="radio" name="relatGer" value="1">Acessa
                                            <input type="radio" name="relatGer" value="0" checked>N�o Acessa
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Relat�rios de presta��o de contas:
                                        </legend>
                                            <input type="radio" name="relatPC" value="1"/>Acessa
                                            <input type="radio" name="relatPC" value="0" checked/>N�o Acessa
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Relat�rios de acesso do credor:
                                        </legend>
                                            <input type="radio" name="relatAces" value="1"/>Acessa
                                            <input type="radio" name="relatAces" value="0" checked/>N�o Acessa
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Relat�rios de acesso do usu�rio:
                                        </legend>
                                            <input type="radio" name="relatAcesUsu" value="1"/>Acessa
                                            <input type="radio" name="relatAcesUsu" value="0" checked/>N�o Acessa
                                    </fieldset>
                                    relatAcesUsu
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="outros">
                    <div class="form">
                        <div class="fields">
                            <div style="border: none;" class="field">
                                <fieldset class="fieldsetsta">
                                    <legend class="forte">
                                        Pesquisar:
                                    </legend>
                                    <div>
                                        <input type="radio" name="pesquisa" value="1">Acessa
                                        <input type="radio" name="pesquisa" value="0" checked >N�o Acessa
                                    </div>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Cobran�as:
                                        </legend>
                                        <div>
                                            <input type="radio" name="pesquisa_cob" value="1">Acessa
                                            <input type="radio" name="pesquisa_cob" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Inadimplentes:
                                        </legend>
                                        <div>
                                            <input type="radio" name="pesquisa_inad" value="1">Acessa
                                            <input type="radio" name="pesquisa_inad" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Credores:
                                        </legend>
                                        <div>
                                            <input type="radio" name="pesquisa_cred" value="1">Acessa
                                            <input type="radio" name="pesquisa_cred" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Jur�dico:
                                        </legend>
                                        <div>
                                            <input type="radio" name="pesquisa_juri" value="1">Acessa
                                            <input type="radio" name="pesquisa_juri" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldsetstain">
                                        <legend>
                                            Fiscal:
                                        </legend>
                                        <div>
                                            <input type="radio" name="pesquisa_fiscal" value="1">Acessa
                                            <input type="radio" name="pesquisa_fiscal" value="0" checked >N�o Acessa
                                        </div>
                                    </fieldset>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="title">
                    <h5>A��es</h5>
                </div>
                <div class="form">
                    <div class="fields">
                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <input type="reset" name="cancelar" value="Cancelar" />
                            <div style="margin-left: 7px;" class="highlight">
                                <input type="submit" class="cadastrar" name="cadastrar" value="Cadastrar" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>