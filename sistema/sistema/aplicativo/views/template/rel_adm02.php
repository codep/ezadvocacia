<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img.'logo_elisangela.jpg';?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br|www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo $title ;?></p>
        </div>
    </div>
    <table id="tableAdm02" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="thInadimplente">Inadimplente</th>
                <th id="thCredor">Credor</th>
                <th id="th3">Tipo</th>
                <th id="th4">N� Parcela</th>
                <th id="th5">Vencimento</th>
                <th id="th6" class="last">Valor</th>
            </tr>
        </thead>
        <tbody>
            <?php $totalDeParc = 0; foreach ( $acordPrevVenc as $acoPreVenc ) : ?>
            <tr>
           
                <td class="tdInaNome"><?php echo utf8_decode($acoPreVenc->ina_nome); ?></td>
                <td class="tdCreNomeFantasia"><?php echo utf8_decode($acoPreVenc->cre_nome_fantasia); ?></td>
                <td class="td3">
                   <?php
                   /*verificando se � acordo ou previs�o*/
                         if ($acoPreVenc->aco_tipo == 1)//1 = acordo
                                 {
                                   echo "A"; //A = Acordo
                                 }
                         else if ($acoPreVenc->aco_tipo == 2)//2 = previs�o
                                 {
                                    echo "P"; //P = previs�o
                                 }
                         else if ($acoPreVenc->aco_tipo == 3)//3 = Rec�m envido ao Judicial
                                 {
                                    echo "Rec. env. Jud.";
                                 }
                         else//se por algum motivo (que nem o Padre Quevedo possa explicar), n�o vier aco_tipo 1 ou aco_tipo 2 n�o imprime nada
                                 {
                                    echo("");
                                 }
                   ?>
                </td>
                <td class="td4"> <?php echo $acoPreVenc->numDaParcela; ?></td>
                <td class="td5"><?php echo convDataBanco($acoPreVenc->paa_vencimento); ?></td>
                <td class="last" class="td6"><?php echo convMoney($acoPreVenc->paa_valor); ?></td>
                <?php $totalDeParc ++; endforeach; ?>
            </tr>
            <tr>
                <td id="tdTotalA" colspan="5">TOTAL DE PARCELAS EM ATRASO</td><!-- SERIA O TD 7 -->
                <td id="tdTotalB" class="last"><?php echo "$totalDeParc"; ?></td>
            </tr>

        </tbody>
    </table>
</div>
