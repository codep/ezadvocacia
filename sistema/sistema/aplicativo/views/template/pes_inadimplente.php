<script type="text/javascript">
    $(document).ready(function(){
        $('.detalhes').hide();
        $('input[name="pesquisa_detalhada_envia"]').hide();
        

        $('input[name="pesquisa_detalhada"]').click(function(e) {
           e.preventDefault();
            //EFEITO DE EXIBI��O
            $('.detalhes').fadeIn(function(){
                //EXIBE OS DADOS DO CREDOR
            });
            $('input[name="pesquisa_detalhada_envia"]').fadeIn(function(){
                //EXIBE OS DADOS DO CREDOR
            });
            $('input[name="pesquisar1"]').hide();
            $('input[name="pesquisa_detalhada"]').hide();
        });
        
    });
</script>
<div id="content">
        <?php echo $sidebar; ?>
    <div id="right">
        <div class="box" style="min-height: 800px;">
            <div class="title">
                <h5>Pesquisar inadimplente</h5>
            </div>
            <form id="form" action="<?php echo base_url().'pesquisar/pesinadres' ?>" method="post">
                <div class="form">
                    <?php echo $mensagem; ?>
                    <div class="fields" style="height: 400px;">
                        <div style="float: left; margin-top: 2px;" class="select">
                            <select style="width: 215px;" id="pesquisar_inadimplente_tipo" name="filtro1" class="chosen">
                                <option value="ina_nome">Nome</option>
                                <option value="telefone">Telefone</option>
                                <option value="ina_cidade">Cidade</option>
                                <option value="telenonePAR">Telefone (parentes)</option>
                                <option value="ina_endereco">Endere�o</option>
                                <option value="ina_cpf_cnpj">CPF</option>
                                <option value="parentes">Parentes</option>
                                <option value="ina_mae_nome">Nome da m�e</option>
                                <option value="ina_pai_nome">Nome do pai</option>
                            </select>
                        </div>
                        <div class="input" style="float: right;">
                            <input style="width: 490px; padding: 3px 0px;" type="text" id="pesquisar_inadimplente_desc" name="filtro1_desc"/>
                        </div>
                        
                        <div style="clear:both"></div>
                        
                        <div class="input" style="padding-bottom:10px; margin-top: 10px; border-bottom: 1px solid #aaa">
                        	<label><img src="<?php echo $img.'check_all.gif'?>" rel="0" id="ckAllTipo" class="ckAll" /> Cobran�a: </label>
                        	<input type="checkbox" value="divida" name="extraFiltro[]" id="extraDivida" /><label for="tipoVirgem">Possui D�vida</label>
                        	<input type="checkbox" value="acordo" name="extraFiltro[]" id="extraAcordo" /><label for="tipoAndamento">Possui Acordo</label>
                        	<b style="display:block; font-style:italic">OBS: Caso selecione ambos, indica que o inadimplente dever� possuir d�vida <span style="color: red">E</span> acordo</b>
						</div>
						
                        <div class="input" style="padding-bottom:10px; margin-top: 10px; border-bottom: 1px solid #aaa">
                        	<label><img src="<?php echo $img.'check_all.gif'?>" rel="0" id="ckAllSetor" class="ckAll" /> Setor: </label>
                        	<input type="checkbox" value="JUD" name="tipoSetor[]" id="setorJuridico" /><label for="setorJuridico">Judici�rio</label>
                        	<input type="checkbox" value="ADM" name="tipoSetor[]" id="setorAdministrativo" /><label for="setorAdministrativo">Administrativo
							<b style="display:block; font-style:italic">OBS: Caso selecione ambos, indica que o inadimplente dever� possuir cobran�as no setor Judici�rio <span style="color: red">E</span> Administrativo</b>
						</div>
						
                       <div class="buttons">
                            <div class="highlight">
                                <input style="width: 85px;" type="submit" name="pesquisar1" value="Pesquisar" />
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="fields">
                        <div class="field detalhes">
                            <div class="divleft" style="border: none; width: 210px;">
                                <div style="margin-left: 6px;" class="select">
                                    <select style="width: 204px;" id="filtro2" name="filtro2">
                                        <option value="ina_nome">Nome</option>
                                        <option value="telefone">Telefone</option>
                                        <option value="ina_cidade">Cidade</option>
                                        <option value="telenonePAR">Telefone (parentes)</option>
                                        <option value="ina_endereco">Endere�o</option>
                                        <option value="ina_cpf_cnpj">CPF</option>
                                        <option value="parentes">Parentes</option>
                                        <option value="ina_mae_nome">Nome da m�e</option>
                                        <option value="ina_pai_nome">Nome do pai</option>
                                        <option value="ina_nome">Sobrenome</option>
                                    </select>
                                </div>
                            </div>
                            <div class="divleft" style="width: 394px;">
                                <div class="input">
                                    <input style="width: 370px;" type="text" id="filtro2_desc" name="filtro2_desc"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="field detalhes">
                            <div class="divleft" style="border: none; width: 210px;">
                                <div style="margin-left: 6px;" class="select">
                                    <select style="width: 204px;" id="filtro3" name="filtro3">
                                        <option value="ina_nome">Nome</option>
                                        <option value="telefone">Telefone</option>
                                        <option value="ina_cidade">Cidade</option>
                                        <option value="telenonePAR">Telefone (parentes)</option>
                                        <option value="ina_endereco">Endere�o</option>
                                        <option value="ina_cpf_cnpj">CPF</option>
                                        <option value="parentes">Parentes</option>
                                        <option value="ina_mae_nome">Nome da m�e</option>
                                        <option value="ina_pai_nome">Nome do pai</option>
                                        <option value="ina_nome">Sobrenome</option>
                                    </select>
                                </div>
                            </div>
                            <div class="divleft" style="width: 394px;">
                                <div class="input">
                                    <input style="width: 370px;" type="text" id="filtro3_desc" name="filtro3_desc"/>
                                </div>
                            </div>
                        </div>
                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <div style="margin-left: 7px;" class="highlight">
                                <input type="submit" name="pesquisa_detalhada" value="Pesquisa detalhada" />
                                <input type="submit" name="pesquisa_detalhada_envia" value="Pesquisar" />
                            </div>
                        </div>
                    </div>
                    -->
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
(function ($) {

	var cookieInputStatus = function(obj) {
		$.cookie($(obj).attr('id'),$(obj).val());
	};
	var cookieCheckboxStatus = function(obj) {
		var status = $(obj).is(':checked')?1:0;
		$.cookie($(obj).attr('id'),status);
	};
	var ckAllStatus = function(obj) {
		if (obj == undefined) {
			obj = '.ckAll';			
		}
		$(obj).attr('rel',function() {
			ckObj = $(obj).parents('.input').find(':checkbox');
			ckObjChecked = $(obj).parents('.input').find(':checkbox:checked');
			return (ckObj.length == ckObjChecked.length) ? 1 : 0;
		});
	}
	
	$(':checkbox').change(function() {
		cookieCheckboxStatus(this);
		//console.log($.cookie($(this).attr('id')));
	});
	$(':checkbox').each(function(){
		if ($.cookie($(this).attr('id'))==1) {
			$(this).attr('checked',true);
		} else {
			$(this).removeAttr('checked');
		}
	});
	
	$('#form .input input:not(:checkbox), select').change(function(){
		cookieInputStatus(this);
		//console.log($(this).val());
	});
	$('#form .input input:not(:checkbox), select').each(function(){
		var objVal = $.cookie($(this).attr('id'));
		if (objVal!=undefined) {
			$(this).val(objVal);
		}
		//console.log($(this).val());
	});
	

	$('.ckAll').click(function(){
		if ($(this).attr('rel')=='0') {
			$(this).parents('.input').find(':checkbox').attr('checked',function(){
				this.checked = true;
				cookieCheckboxStatus(this);
			});
		} else {
			$(this).parents('.input').find(':checkbox').attr('checked',function(){
				this.checked = false;
				cookieCheckboxStatus(this);
			});
		}
		ckAllStatus(this);
	});
	ckAllStatus();
	
	$('select.chosen').chosen();
	
})(jQuery);
</script>