<script type="text/javascript">
    $(document).ready(function(){

/*
        $('.editaInad').click(function(e){
                link = $(this).attr('id');
                window.location.href=link;
        });
*/
        
        $('input#search').quicksearch('#products tbody tr:not(.titulo)', {});
/*
		//AUTO FOCUS
		window.setTimeout(function(){
			$('input#search').focus();	
		},300);
*/
    });
</script>
<div id="content">

    <?php echo $sidebar; ?>

    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Pesquisar credor</h5>
            </div>
            <div class="blocoTitulo">
				<span style="font-size: 10px">Filtro: <input type="text" id="search" style="font-size: 18px; padding: 5px; width: 500px;" /></span>
            </div>
            <div class="table">
                <table id="products">
                    <thead>
                        <tr>
                            <th class="left">Nome fantasia</th>
                            <th>Raz�o social</th>
                            <th>Endere�o</th>
                            <th>Telefone</th>
                            <th>Cidade</th>
                            <th class="last">UF</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=0; foreach ($resultados as $resultado): ?> 
                            <tr class="" style="cursor: pointer;" id="<?php echo base_url().'credor/editar/cod:'.$resultado->cre_cod;?>">
                                <td class="title"><?php echo $resultado->cre_nome_fantasia; ?></td>
                                <td><?php echo $resultado->cre_razao_social; ?></td>
                                <td><?php echo $resultado->cre_endereco; ?></td>
                                <td><?php echo $resultado->cre_fone1; ?></td>
                                <td><?php echo $resultado->cre_cidade; ?></td>
                                <td class="last"><?php echo $resultado->cre_uf; ?></td>
                            </tr>
                        <?php $i++; endforeach; ?>
                    </tbody>
                </table>
                <div class="pagination pagination-left">
                    <div class="results">
                        <span><?php echo $i; ?> registros encontrados</span>
                    </div>
                </div>
                <br/>
            </div>
        </div>
    </div>
</div>
