<script type="text/javascript">
        $(document).ready(function(){
            $('input[value="Ficha"]').click(function(e){
                    e.preventDefault();
                    window.location.href='http://webhost/cobranca/divida/ficha';
            });

        });
        $('.fechar').click(function(e){
            e.preventDefault();
            $('#dialog-form').dialog('close');
        });
</script>
<html>
    <body>
        <!-- Caixa de di�logo RO-->
        <div id="dialog-form" class="form" style="padding-left: 5px; padding-right: 5px;" title="Motivo da devolu��o">
            <form id="form" action="" method="post">
                <div class="fields" style="width: 400px; margin: 0 auto;">
                    <div class="field" style="width: 400px;">
                        <div style="margin-top: 15px; padding-top: 7px;">
                            <div style="float: left; margin-right: 2px;">
                                <input type="radio" id="deslocalizado" name="motivo_devolucao"/>
                            </div>
                            <div style="float: left;">
                                <div style="width: 195px; padding-left: 1px; margin-top: 3px;" class="label">
                                    <label for="tarefa">Deslocalizado</label>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 15px; padding-top: 7px;">
                            <div style="float: left; margin-right: 2px;">
                                <input type="radio" checked id="falecido" name="motivo_devolucao"/>
                            </div>
                            <div style="float: left;">
                                <div style="width: 195px; padding-left: 1px; margin-top: 3px;" class="label">
                                    <label for="tarefa">Falecido</label>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 15px; padding-top: 7px;">
                            <div style="float: left; margin-right: 2px;">
                                <input type="radio" id="titulo_nao_habil" name="motivo_devolucao"/>
                            </div>
                            <div style="float: left;">
                                <div style="width: 195px; padding-left: 1px; margin-top: 3px;" class="label">
                                    <label for="tarefa">T�tulo n�o h�bil</label>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 15px; padding-top: 7px;">
                            <div style="float: left; margin-right: 2px;">
                                <input type="radio" id="outros" name="motivo_devolucao"/>
                            </div>
                            <div style="float: left;">
                                <div style="width: 195px; padding-left: 1px; margin-top: 3px;" class="label">
                                    <label for="tarefa">Outros</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field" style="width: 400px;">
                        <div style="margin-top: 0px; padding-top: 0px;">
                            <div style="width: 103px; padding-left: 1px; float: left;" class="label">
                                <label style="float: left;" for="obs">Observa��es:</label>
                            </div>
                            <div class="input" style="width: 383px; float: left !important;">
                                <input style="float: left;" type="text" id="obs" name="obs" value="Inadimplente faleceu dia 15/01/2011"/>
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center; margin-top: 10px; margin-bottom: 10px;" class="buttons">
                        <input type="reset" class="fechar" name="cancelar" value="Cancelar" />
                        <div style="margin-left: 7px;" class="highlight">
                            <input type="submit" name="submit.highlight" value="Confirmar" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
<!-- FIM Caixa de di�logo RO-->
        <div id="content">

            <?php echo $sidebar; ?>

            <div id="right">
                <div class="box">
                    <div class="title">
                        <h5>Detalhes da d�vida (24230)</h5>
                    </div>
                    <form id="form" method="post">
                        <div class="form">
                            <div class="fields">
                                <div style="margin-top: 0px; margin-bottom: 13px;" class="blocoTitulo">
                                    Dados do credor
                                </div>
                                <div class="field  field-first">
                                    <div class="divleftleft" style="height:29px; width: 200px;">
                                        <div class="label">
                                            <label>Pessoa:</label>
                                        </div>
                                        <input style="margin-top: 7px; " type="radio" name="credor_pessoa" value="fisica" checked>F�sica
                                        <input type="radio" name="credor_pessoa" value="juridica">Jur�dica
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="width: 477px;">
                                        <div style="width: 89px;" class="label">
                                            <label for="credor_nome">Nome:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 356px;" type="text" id="credor_nome" name="credor_nome" value="Radioval M�veis e Eletro" readonly/>
                                        </div>
                                    </div>

                                    <div class="divleftlast" style="width: 225px; margin-left: 0px">
                                        <div style="width: 76px; padding-left: 1px;" class="label">
                                            <label for="credor_cpf_cnpj">CNPJ/CPF:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 124px; text-align: center" type="text" id="credor_cpf_cnpj" name="credor_cpf_cnpj" value="00.000.000/0001-00" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 12px; margin-bottom: 13px;" class="blocoTitulo">
                                    Dados do inadimplente
                                </div>
                                <div class="field  field-first">
                                    <div class="divleftleft" style="height:29px; width: 200px;">
                                        <div class="label">
                                            <label>Pessoa:</label>
                                        </div>
                                        <input style="margin-top: 7px;" type="radio" name="inad_pessoa" value="fisica" checked readonly>F�sica
                                        <input type="radio" name="inad_pessoa" value="juridica" readonly>Jur�dica
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="width: 477px;">
                                        <div style="width: 89px;" class="label">
                                            <label for="inad_nome">Nome:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 356px;" type="text" id="inad_nome" name="inad_nome" value="Jo�o Pereira da Silva" readonly/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 225px; margin-left: 0px">
                                        <div style="width: 76px; padding-left: 1px;" class="label">
                                            <label for="inad_cpf_cnpj">CPF/CNPJ:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 124px; text-align: center" type="text" id="inad_cpf_cnpj" name="inad_cpf_cnpj" value="000.000.000-00" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 12px; margin-bottom: 9px;" class="blocoTitulo">
                                    Dados da d�vida
                                </div>
                                <div style="padding-top: 5px;" class="field">
                                    <div class="divleft" style="width: 136px;">
                                        <div style="margin-left: 6px;" class="select">
                                            <select style="width: 122px;" id="tipo_doc" name="tipo_doc">
                                                <option value="1">Tipo de doc.</option>
                                                <option value="2" selected>Cheque</option>
                                                <option value="3">(003)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 92px;">
                                        <div style="margin-left: 2px;" class="select">
                                            <select style="width: 82px;" id="banco" name="banco">
                                                <option value="1">Banco</option>
                                                <option value="2">(002)</option>
                                                <option value="3" selected>(003)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="margin-left: 2px; width:98px;">
                                        <div style="width: 26px; padding-left: 1px;" class="label">
                                            <label for="agencia">AG.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 42px;" type="text" id="agencia" name="agencia" value="478" readonly/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 46px;">
                                        <div class="input">
                                            <input style="width: 18px;" type="text" id="agencia_dig" name="agencia_dig" value="9" readonly/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="margin-left: 2px; width:117px;">
                                        <div style="width: 46px; padding-left: 1px;" class="label">
                                            <label for="alinea">Al�nea:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 39px;" type="text" id="alinea" name="alinea" value="12" readonly/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="margin-left: 2px; width:176px;">
                                        <div style="width: 62px; padding-left: 1px;" class="label">
                                            <label for="emissao">Emiss�o:</label>
                                        </div>
                                        <div style="width: 93px;" class="input">
                                            <input type="text" id="emissao" name="emissao" value="17/06/2010" readonly style="width: 93px; text-align: center"/>
                                        </div>
                                    </div>
                                </div>    
                                <div class="field" style="margin-bottom: 0px;">
                                    <div class="divleft" style="width: 219px;">
                                        <div style="width: 165px;" class="label">
                                            <label for="parcelas">Quantidade de parcelas:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 19px;" type="text" id="parcelas" name="parcelas" value="3" readonly/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 252px;">
                                        <div style="width: 122px; padding-left: 1px;" class="label">
                                            <label for="total_divida">Total d�vida (R$):</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 99px;" type="text" id="total_divida" name="total_divida" value="R$ 5.340,00" readonly/>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="width: 151px;">
                                        <div style="width: 64px; padding-left: 5px;" class="label">
                                            <label for="doc_par_1">N�.Doc.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 53px;" type="text" id="doc_par_1" name="doc_par_1" value="00462" readonly/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 128px;">
                                        <div style="width: 80px; padding-left: 0px;" class="label">
                                            <label for="num_par_1">Parcela N�.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 18px;" type="text" id="num_par_1" name="num_par_1" value="1" readonly/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 242px;">
                                        <div style="width: 112px; padding-left: 1px;" class="label">
                                            <label for="valor_par_1">V.Parcela (R$):</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 99px;" type="text" id="valor_par_1" name="valor_par_1" value="R$ 1.780,00" readonly/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="margin-left: 0px; width: 159px;">
                                        <div style="width: 45px; padding-left: 1px;" class="label">
                                            <label for="venc_par_1">Venc.:</label>
                                        </div>
                                        <div style="width: 93px;" class="input">
                                            <input type="text" id="venc_par_1" name="venc_par_1" value="17/07/2010" readonly style="width: 93px; text-align: center"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="width: 151px;">
                                        <div style="width: 64px; padding-left: 5px;" class="label">
                                            <label for="doc_par_1">N�.Doc.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 53px;" type="text" id="doc_par_1" name="doc_par_1" value="00463" readonly/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 128px;">
                                        <div style="width: 80px; padding-left: 0px;" class="label">
                                            <label for="num_par_1">Parcela N�.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 18px;" type="text" id="num_par_1" name="num_par_1" value="2" readonly/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 242px;">
                                        <div style="width: 112px; padding-left: 1px;" class="label">
                                            <label for="valor_par_1">V.Parcela (R$):</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 99px;" type="text" id="valor_par_1" name="valor_par_1" value="R$ 1.780,00" readonly/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="margin-left: 0px; width: 159px;">
                                        <div style="width: 45px; padding-left: 1px;" class="label">
                                            <label for="venc_par_1">Venc.:</label>
                                        </div>
                                        <div style="width: 93px;" class="input">
                                            <input type="text" id="venc_par_1" name="venc_par_1" value="17/08/2010" readonly style="width: 93px; text-align: center"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="width: 151px;">
                                        <div style="width: 64px; padding-left: 5px;" class="label">
                                            <label for="doc_par_1">N�.Doc.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 53px;" type="text" id="doc_par_1" name="doc_par_1" value="00464" readonly/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 128px;">
                                        <div style="width: 80px; padding-left: 0px;" class="label">
                                            <label for="num_par_1">Parcela N�.:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 18px;" type="text" id="num_par_1" name="num_par_1" value="3" readonly/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 242px;">
                                        <div style="width: 112px; padding-left: 1px;" class="label">
                                            <label for="valor_par_1">V.Parcela (R$):</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 99px;" type="text" id="valor_par_1" name="valor_par_1" value="R$ 1.780,00" readonly/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="margin-left: 0px; width: 159px;">
                                        <div style="width: 45px; padding-left: 1px;" class="label">
                                            <label for="venc_par_1">Venc.:</label>
                                        </div>
                                        <div style="width: 93px;" class="input">
                                            <input type="text" id="venc_par_1" name="venc_par_1" value="17/09/2010" readonly style="width: 93px; text-align: center"/>
                                        </div>
                                    </div>
                                </div>
                                <div style="height: 74px; margin-top: 5px;" class="field">
                                    <div class="divleftlast" style="width: 701px;">
                                        <div style="width: 84px; padding-top: 0px;" class="label">
                                            <label for="informacoes">Outras informa��es:</label>
                                        </div>
                                        <div class="textarea">
                                            <textarea style="width: 586px; height: 50px;" id="informacoes" name="informacoes" cols="500" rows="4" class="editor" readonly>
D�vida referente � compra de dois aparelhos de som Gradiente, um fog�o Dako, oito geladeiras Consul e vinte e cinco cadeiras de balan�o.a
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 12px; margin-bottom: 3px; padding-bottom: 12px;" class="blocoTitulo">
                                    Recuperador padr�o: FULANO
                                </div>
                                <div style="text-align: center; margin-top: 10px;" class="buttons">
                                    <div class="highlight">
                                        <input type="submit" name="submit.highlight" value="R.O." />
                                    </div>
                                    <div style="margin-left: 7px;" class="highlight">
                                        <input type="submit" name="submit.highlight" value="Ficha" />
                                    </div>
                                    <div style="margin-left: 7px;" class="highlight">

                                        <input type="submit" class="dialog-form-open" name="submit.highlight" value="Devolver" />
                                    </div>
                                    <div style="margin-left: 7px;" class="highlight">
                                        <input type="submit" name="submit.highlight" value="Encaminhar jur�dico" />
                                    </div>
                                    <input type="reset" name="cancelar" value="Voltar" style="margin-left: 7px;" onclick="window.location='http://webhost/cobranca/divida/listar'"/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>