<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img.'logo_elisangela.jpg';?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br|www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo $title ;?></p>
        </div>
    </div>
    <table id="tableR1" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="thInadimplente">Inadimplente</th>
                <th id="thCredor">Credor</th>
                <th id="th3">Data <?php echo ($agendOuCadast == 'Agendamento')? 'Agendamento': 'Cadastramento'; ?></th>
                <th id="th4" class="last">Detalhes RO</th>
            </tr>
        </thead>
        <tbody>
            <?php $totalDeInad = 0; foreach ($relPorRo as $rPorRo) : ?>
            <tr>
           
                <td class="tdInaNome"><?php echo utf8_decode($rPorRo->ina_nome); ?></td>
                <td class="tdCreNomeFantasia"><?php echo utf8_decode($rPorRo->cre_nome_fantasia); ?></td>
                <td class="td3"><?php echo ($agendOuCadast == 'Agendamento')? convDataBanco($rPorRo->ros_agendado): convDataBanco($rPorRo->ros_data); ?></td>
                <td class="td4 last"><?php echo utf8_decode($rPorRo->ros_detalhe); ?>
            </tr>
            <?php $totalDeInad ++; endforeach; ?>
            <tr>
                <td colspan="4" id="tdTotalA" class="last">TOTAL DE ROs: <?php echo "$totalDeInad"; ?></td>
            </tr>

        </tbody>
    </table>
</div>
