<script type="text/javascript">
    $(document).ready(function(){
        var base_url = "<?php echo base_url() ?>";
        $('input[name="cpf_cnpj"]').attr('disabled',true).click(function(){$(this).select();});
        $('.telefone').mask("(99)9999-9999?9").blur(function(){
			    var phone, element;
			    element = $(this);
			    element.unmask();
			    phone = element.val().replace(/\D/g, '');
			    if(phone.length > 10) {
			        element.mask("(99)99999-999?9");
			    } else {
			        element.mask("(99)9999-9999?9");
			    }
			    //element.unmask();
			}).click(function(){$(this).select();});
        $('#cep').mask('99999999').click(function(){$(this).select();});
        var pessoa;
        var fecharAba = '0';
        //------------------------------------------------------------------------------
        //---------------------------Fun��o para validar CNPJ---------------------------
        //------------------------------------------------------------------------------
        function validaCNPJ() {
            CNPJ = $('input[name="cpf_cnpj"]').val();
            erro = new String;
            if (CNPJ.length < 18) erro += "� necessario preencher corretamente o n�mero do CNPJ! \n\n";
            if ((CNPJ.charAt(2) != ".") || (CNPJ.charAt(6) != ".") || (CNPJ.charAt(10) != "/") || (CNPJ.charAt(15) != "-")){
                if (erro.length == 0) erro += "� necess�rio preencher corretamente o n�mero do CNPJ! \n\n";
            }
            //substituir os caracteres que n�o s�o n�meros
            if(document.layers && parseInt(navigator.appVersion) == 4){
                x = CNPJ.substring(0,2);
                x += CNPJ. substring (3,6);
                x += CNPJ. substring (7,10);
                x += CNPJ. substring (11,15);
                x += CNPJ. substring (16,18);
                CNPJ = x;
            } else {
                CNPJ = CNPJ. replace (".","");
                CNPJ = CNPJ. replace (".","");
                CNPJ = CNPJ. replace ("-","");
                CNPJ = CNPJ. replace ("/","");
            }
            var nonNumbers = /\D/;
            if (nonNumbers.test(CNPJ)) erro += "A verifica��o de CNPJ suporta apenas n�meros! \n\n";
            var a = [];
            var b = new Number;
            var c = [6,5,4,3,2,9,8,7,6,5,4,3,2];
            for (i=0; i<12; i++){
                a[i] = CNPJ.charAt(i);
                b += a[i] * c[i+1];
            }
            if ((x = b % 11) < 2) { a[12] = 0 } else { a[12] = 11-x }
            b = 0;
            for (y=0; y<13; y++) {
                b += (a[y] * c[y]);
            }
            if ((x = b % 11) < 2) { a[13] = 0; } else { a[13] = 11-x; }
            if ((CNPJ.charAt(12) != a[12]) || (CNPJ.charAt(13) != a[13])){
                erro +="CNPJ INV�LIDO, Por favor verifique!";
            }
            if (erro.length > 0){
                alert(erro);
                return false;
            } 
            return true;
        }
        //------------------------------------------------------------------------------
        //---------------------------Fun��o para validar CPF----------------------------
        //------------------------------------------------------------------------------
        function validaCPF() {
            cpf = $('input[name="cpf_cnpj"]').val();

            cpf = cpf.replace(".", "");
            cpf = cpf.replace(".", "");
            cpf = cpf.replace("-", "");

            erro = new String;
            if (cpf.length < 11) erro += "Sao necessarios 11 digitos para verificacao do CPF! \n\n";
            var nonNumbers = /\D/;
            if (nonNumbers.test(cpf)) erro += "A verificacao de CPF suporta apenas numeros! \n\n";
            if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999"){
                erro += "Numero de CPF invalido!"
            }
            var a = [];
            var b = new Number;
            var c = 11;
            for (i=0; i<11; i++){
                a[i] = cpf.charAt(i);
                if (i < 9) b += (a[i] * --c);
            }
            if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
            b = 0;
            c = 11;
            for (y=0; y<10; y++) b += (a[y] * c--);
            if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
            if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10])){
                erro +="Numero de CPF invalido!, Por Favor confira";
            }
            if (erro.length > 0){
                alert(erro);
                return false;
            }
            return true;
        }
        //------------------------------------------------------------------------------
        //--------------------------- Fim das Valida��es -------------------------------
        //------------------------------------------------------------------------------

        $('#pesquisarIC').hide();

        //AO CLICAR EM PESSOA JURIDICA
        $('input[name="pessoa"][value="j"]').click(function() {
            pessoa='j';
            //EXIBE BOT�O PESQUISAR
            $('#pesquisarIC').fadeIn(function(){
                //EXIBE BOT�O PESQUISAR
            });

            $('label[for="nome"]').text('Nome Fantasia:');
            //DESMARCAR O SEXO
            $("input[name='sexo']").each(function() {
                //desmarcar
                this.checked = false;
            });

            //DESMARCAR ESTADO CIVIL AINDA N�O FUNCIONA
            $('#est_civil option[value="0"]').attr('selected', true);

            //RESETAR CPF/CNPJ
            $('#cpf_cnpj').html('<input style="width: 124px;" type="text" id="cpf_cnpjInad" name="cpf_cnpj" />');
            
            $('#cpf_cnpjInad').blur(function () {
                entidade = 'i';
                pessoa = 'f';
                $('input[name="pessoa"]').each(function() {
                    if ( $(this).is(':checked') ) {
                        pessoa = $(this).attr('value');
                    }
                });
                tipodado = '';
                valor = '';
                
                if(($('input[name="nome"]').val() == '') && ($('input[name="cpf_cnpj"]').val() == '')){
                    $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Para pesquisar � necess�rio digitar o NOME ou CPF/CNPJ</h3></td></tr>');//zera os valores antigos e escreve no elemento o valor selecione
                    return false;
                }
                else {
                    if ($('input[name="cpf_cnpj"]').val() != ''){
                        valor = $('input[name="cpf_cnpj"]').val();
                        tipodado = 'c';
                    }

                    else if ($('input[name="nome_fantasia"]').val() != ''){
                        valor = $('input[name="nome_fantasia"]').val();
                        tipodado = 'n';
                    }
                }
                $.ajax({
                    type: "POST",//TIPO DE DADOS
                    url: base_url+'inadimplente/listar_pesquisa_blur',//FUN��O DO CONTROLLER PARA TRATAR A LISTAGEM
                    data: 'entidade='+entidade+'&pessoa='+pessoa+'&tipodado='+tipodado+'&valor='+valor,//DADOS ENVIADOS
                    dataType: 'json',//TIPO DE DADOS A SER RECEBIDO
                    error: function(xhr, status, er) {
                        alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                    },
                    success: function(retorno)
                    {
                        var codigo = '';
                        //                    $("#dialog-form").dialog("open");///ABRE A CAIXA DE DI�LOGO
                        //                    $('#dadosPesquisa').html('');//zera os valores antigos e escreve no elemento o valor selecione
                        vazio = false;//VARI�VEL PARA IDENTIFICAR SE ALGUM DADO �NTEGRO TENHA SIDO RECEBIDO

                        $.each(retorno.resultado, function(i, v){//para cada �ndice retornado executa a fun��o
                            if(v.nome != null){
                                codigo = v.codigo;
                                //CONCATENA AS LINHAS DA TABELA COM OS VALORES RECEBIDOS
                            }
                            else vazio = true;//CASO OS DADOS RETORNADOS SEJAM NULOS
                        });
                        if(codigo != ''){
                        	$('#cpf_cnpjInad').val('');
                            if(confirm('J� existe um INADIMPLENTE possivelmente cadastrado com este CPF, gostaria de visualizar o cadastro?')){
                                link = base_url+'divida/ficha/cod:'+codigo;
                                window.location = link;
                            }
                        }
                    }
                });
            });

            //HABILITAR CAMPO E APLICAR M�SCARA PARA CPF
            $('input[name="cpf_cnpj"]').attr('value', '');
            $('input[name="cpf_cnpj"]').attr('disabled',false).mask('99.999.999/9999-99');
        });

        //AO CLICAR EM PESSOA F�SICA
        $('input[name="pessoa"][value="f"]').click(function() {
            pessoa='f';
            //EXIBE BOT�O PESQUISAR
            $('#pesquisarIC').fadeIn(function(){
                //EXIBE BOT�O PESQUISAR
            });

            //ALTERA O LABEL DE NOME FANTASIA PARA NOME
            $('label[for="nome"]').text('Nome:');

            //RESETAR CPF/CNPJ
            $('#cpf_cnpj').html('<input style="width: 124px;" type="text" id="cpf_cnpjInad" name="cpf_cnpj" />');
            
            $('#cpf_cnpjInad').blur(function () {
                
                entidade = 'i';
                pessoa = 'f';
                $('input[name="pessoa"]').each(function() {
                    if ( $(this).is(':checked') ) {
                        pessoa = $(this).attr('value');
                    }
                });
                tipodado = '';
                valor = '';
                if((($('input[name="nome"]').val() == '') && ($('input[name="cpf_cnpj"]').val() == '')) || ($('input[name="cpf_cnpj"]').val() == '___.___.___-__')){
                    $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Para pesquisar � necess�rio digitar o NOME ou CPF/CNPJ</h3></td></tr>');//zera os valores antigos e escreve no elemento o valor selecione
                    return false;
                }
                else {
                    if ($('input[name="cpf_cnpj"]').val() != ''){
                        valor = $('input[name="cpf_cnpj"]').val();
                        tipodado = 'c';
                    }

                    else if ($('input[name="nome_fantasia"]').val() != ''){
                        valor = $('input[name="nome_fantasia"]').val();
                        tipodado = 'n';
                    }
                }
                $.ajax({
                    type: "POST",//TIPO DE DADOS
                    url: base_url+'inadimplente/listar_pesquisa_blur',//FUN��O DO CONTROLLER PARA TRATAR A LISTAGEM
                    data: 'entidade='+entidade+'&pessoa='+pessoa+'&tipodado='+tipodado+'&valor='+valor,//DADOS ENVIADOS
                    dataType: 'json',//TIPO DE DADOS A SER RECEBIDO
                    error: function(xhr, status, er) {
                        alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                    },
                    success: function(retorno)
                    {
                        var codigo = '';
                        //                    $("#dialog-form").dialog("open");///ABRE A CAIXA DE DI�LOGO
                        //                    $('#dadosPesquisa').html('');//zera os valores antigos e escreve no elemento o valor selecione
                        vazio = false;//VARI�VEL PARA IDENTIFICAR SE ALGUM DADO �NTEGRO TENHA SIDO RECEBIDO

                        $.each(retorno.resultado, function(i, v){//para cada �ndice retornado executa a fun��o
                            if(v.nome != null){
                                codigo = v.codigo;
                                //CONCATENA AS LINHAS DA TABELA COM OS VALORES RECEBIDOS
                            }
                            else vazio = true;//CASO OS DADOS RETORNADOS SEJAM NULOS
                        });
                        if(codigo != ''){
                        	$('#cpf_cnpjInad').val('');
                            if(confirm('J� existe um INADIMPLENTE possivelmente cadastrado com este CPF, gostaria de visualizar o cadastro?')){
                                link = base_url+'divida/ficha/cod:'+codigo;
                                window.location = link;
                            }
                        }
                    }
                });
            });

            //APLICAR M�SCARA PARA CNPJ
            $('input[name="cpf_cnpj"]').attr('value', '');
            $('input[name="cpf_cnpj"]').attr('disabled',false).mask('999.999.999-99');
        });



        //VALIDA��O DOS CAMPOS OBRIGAT�RIOS
        $('form').submit(function(){

            if(($('input[name="cpf_cnpj"]').val())!=''){
                if(pessoa=='f'){
                    ret = validaCPF();
                    if(ret==false){
                        return false;
                    }
                }else if(pessoa=='j'){
                    ret = validaCNPJ();
                    if(ret==false){
                        return false;
                    }
                }
            }

            var pessoaVazio = true;
            $('input[name="pessoa"]').each(function() {
                if ( $(this).is(':checked') ) {
                    pessoaVazio = false;
                }
            });

            if($('input[name="nome_fantasia"]').val() == ''){
                $('input[name="nome_fantasia"]').focus();
                return false;
            }
            else if(pessoaVazio){
                alert('Indique pessoa F�SICA ou JUR�DICA.');
                return false;
            }

            else if($('#cidade option:selected').val() == '' || $('#uf option:selected').val() == ''){
                alert('Selecione uma CIDADE e um ESTADO');
                return false;
            }
            
            fecharAba = '1';

        });
        //LISTANDO AS CIDADES
        $('select[id="ina_uf"]').change(function(){
            popularCidade('ina_');
        });
        $('select[id="con_uf"]').change(function(){
            popularCidade('con_');
        });
        $('select[id="pai_uf"]').change(function(){
            popularCidade('pai_');
        });
        $('select[id="mae_uf"]').change(function(){
            popularCidade('mae_');
        });

        function popularCidade(entidade)
        {

            //            alert('Entrou na Fun��o: '+entidade);
            //$('select[id="uf"]').change(function(){

            v1 = '';
            //captura os options selecionados
            v1 = $('select[id="'+entidade+'uf"] option:selected').text();
            if(v1 == '') v1='UF';
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() . 'inadimplente/listar_cidades'; ?>",
                data: 'v1='+v1,
                dataType: 'json',
                error: function(xhr, status, er) {
                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                },
                success: function(retorno)
                {
                    $('#'+entidade+'cidade').html('<option value="">SELECIONE UMA CIDADE</option>');//zera os valores antigos e escreve no elemento o valor selecione
                    $qtd = '';
                    $valor = '';
                    $.each(retorno.cidadesbrasil, function(i, v){//para cada �ndice retornado executa a fun��o
                        if(v.cidadeatual != ''){
                            $('#'+entidade+'cidade-button').fadeOut('normal',function(){
                                //ESCONDE O OPTION INICIAL SEM VALOR
                            });
                            $('#'+entidade+'cidade').append('<option value="'+unescape(v.cidadeatual)+'">'+unescape(v.cidadeatual)+'</option>');//unescape descriptografa o padr�o URL de dados retornado no PHP
                        }
                        $qtd = i;
                        $valor = v.cidadeatual;
                    });
                    if ($qtd != 0 && $valor != ''){
                        $('#'+entidade+'cidade').fadeIn('normal', function(){
                            //CIDADES CARREGADAS COM SUCESSO
                        });
                    }
                    else
                    {
                        alert('Voc� deve selecionar um ESTADO v�lido.');
                        $('#'+entidade+'cidade').html('<option value=""><-- SELECIONE UM ESTADO</option>');//zera os valores antigos e escreve no elemento o valor selecione
                    }
                }
            });
        }
        //});

        //LISTANDO OS RESULTADOS DA PESQUISA
        /* *************************************************************************
         * PESQUISA INADIMPLETE OU CREDOR POR NOME OU CPF/CNPJ
         * pesquisaIC($entidade, $pessoa, $tipoDado, $valor)
         * $entidade -------- i=inadimplentes, c=credores
         * $pessoa ---------- j=jur�dica, f=f�sica
         * $tipoDado -------- n=nome, c=cpf/cnpj
         * $valor ----------- valor do nome ou cpf/cnpj
         *
         * NECESS�RIO CONFIGURAR O VALOR DOS ATRIBUTOS NAME DOS INPUTS
         ************************************************************************* */
        $("#pesquisarIC").click(function () {

            //ENTIDADE DO TIPO INADIMPLENTE
            entidade = 'i';

            //RECEBENDO O TIPO DE PESSOA [f ou j]
            pessoa = '';
            $('input[name="pessoa"]').each(function() {
                if ( $(this).is(':checked') ) {
                    pessoa = $(this).attr('value');
                }
            });

            //RECEBENDO O TIPO DE DADOS [nome_fantasia ou cpf_cnpj]
            tipodado = '';

            //RECEBENDO O VALOR DIGITADO EM nome_fantasia ou cpf_cnpj
            valor = '';
            if(($('input[name="nome"]').val() == '') && ($('input[name="cpf_cnpj"]').val() == '')){
                $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Para pesquisar � necess�rio digitar o NOME ou CPF/CNPJ</h3></td></tr>');//zera os valores antigos e escreve no elemento o valor selecione
                return false;
            }
            else {
                if ($('input[name="cpf_cnpj"]').val() != ''){
                    valor = $('input[name="cpf_cnpj"]').val();
                    tipodado = 'c';
                }

                else if ($('input[name="nome_fantasia"]').val() != ''){
                    valor = $('input[name="nome_fantasia"]').val();
                    tipodado = 'n';
                }
            }

            //FUN��O AJAX
            $.ajax({
                type: "POST",//TIPO DE DADOS
                url: base_url+'inadimplente/listar_pesquisa',//FUN��O DO CONTROLLER PARA TRATAR A LISTAGEM
                data: 'entidade='+entidade+'&pessoa='+pessoa+'&tipodado='+tipodado+'&valor='+valor,//DADOS ENVIADOS
                dataType: 'json',//TIPO DE DADOS A SER RECEBIDO

                //CASO HAJA ERRO DE PROCESSAMENTO
                error: function(xhr, status, er) {
                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                },

                //CASO A FUN��O DO CONTROLLER SEJA EXECUTADA COM SUCESSO E OS DADOS RECEBIDOS COM SUCESSO
                success: function(retorno)
                {
                    $("#dialog-form").dialog("open");///ABRE A CAIXA DE DI�LOGO
                    $('#dadosPesquisa').html('');//zera os valores antigos e escreve no elemento o valor selecione
                    vazio = false;//VARI�VEL PARA IDENTIFICAR SE ALGUM DADO �NTEGRO TENHA SIDO RECEBIDO

                    $.each(retorno.resultado, function(i, v){//para cada �ndice retornado executa a fun��o
                        if(v.nome != null){
                            //CONCATENA AS LINHAS DA TABELA COM OS VALORES RECEBIDOS
                            $('#dadosPesquisa').append(
                            '<tr class="tuplas" id="'+unescape(v.codigo)+'">'+
                                '<td>'+unescape(v.nome)+'</td>'+
                                '<td>'+unescape(v.cpf_cnpj)+'</td>'+
                                '<td>'+unescape(v.endereco)+'</td>'+
                                '<td class="last">'+unescape(v.cidade)+'</td>'+
                                '</tr>'
                        );
                        }
                        else vazio = true;//CASO OS DADOS RETORNADOS SEJAM NULOS
                    });
                    $('.tuplas').css('cursor','pointer');//SETA O TIPO DO CURSOR

                    //AO CLICAR SOBRE UMA LINHA DA TABALEA COM A CLASSE TUPLA
                    $('.tuplas').click(function(e){
                        e.preventDefault();
                        codigo = $(this).attr('id');
                        link = base_url+'inadimplente/editar/cod:'+codigo;
                        window.open(link, 'blank');
                    });

                    //CASO OS DADOS RETORNADOS SEJAM NULOS
                    if (vazio)
                    {   //EXIBE MENSAGEM
                        $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Nenhum dado encontrado pesquisando por ['+valor+']</h3></td></tr>');
                    }
                }
            });
        });

        //PESQUISA DE PARENTES
        $("#pesquisarCONJ").click(function () {
            pesquisarParentes("conj_")
        });
        $("#pesquisarPAI").click(function () {
            pesquisarParentes("pai_")
        });
        $("#pesquisarMAE").click(function () {
            pesquisarParentes("mae_")
        });
        $("#cadastroCONJ").css('display','none');
        $("#cadastroPAI").css('display','none');
        $("#cadastroMAE").css('display','none');
        function pesquisarParentes(origem)
        {
            //ENTIDADE DO TIPO INADIMPLENTE
            entidade = 'i';

            //RECEBENDO O TIPO DE PESSOA [f]
            pessoa = 'f';

            //RECEBENDO O TIPO DE DADOS [nome_fantasia ou cpf_cnpj]
            tipodado = 'n';

            //RECEBENDO O VALOR DIGITADO EM nome
            valor = $('input[name="'+origem+'nome"]').val();
            

            //FUN��O AJAX
            $.ajax({
                type: "POST",//TIPO DE DADOS
                url: base_url+'inadimplente/listar_pesquisa',//FUN��O DO CONTROLLER PARA TRATAR A LISTAGEM
                data: 'entidade='+entidade+'&pessoa='+pessoa+'&tipodado='+tipodado+'&valor='+valor,//DADOS ENVIADOS
                dataType: 'json',//TIPO DE DADOS A SER RECEBIDO

                //CASO HAJA ERRO DE PROCESSAMENTO
                error: function(xhr, status, er) {
                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                },

                //CASO A FUN��O DO CONTROLLER SEJA EXECUTADA COM SUCESSO E OS DADOS RECEBIDOS COM SUCESSO
                success: function(retorno)
                {
                    $("#dialog-form").dialog("open");///ABRE A CAIXA DE DI�LOGO
                    $("#products").find(":eq(3)").html('FONE');//Substituindo a coluna CPF_CNPJ por FONE
                    $('#dadosPesquisa').html('');//zera os valores antigos e escreve no elemento o valor selecione
                    vazio = false;//VARI�VEL PARA IDENTIFICAR SE ALGUM DADO �NTEGRO TENHA SIDO RECEBIDO

                    $.each(retorno.resultado, function(i, v){//para cada �ndice retornado executa a fun��o
                        if(v.nome != null)
                        {
                            //CONCATENA AS LINHAS DA TABELA COM OS VALORES RECEBIDOS
                            $('#dadosPesquisa').append(
                            '<tr class="tuplas" id="'+unescape(v.codigo)+'">'+
                                '<td>'+unescape(v.nome)+'</td>'+
                                '<td>'+unescape(v.fone)+'</td>'+
                                '<td>'+unescape(v.endereco)+'</td>'+
                                '<td class="last">'+unescape(v.cidade)+'</td>'+
                                '</tr>'
                        );
                        }
                        else vazio = true;//CASO OS DADOS RETORNADOS SEJAM NULOS
                    });
                    $('.tuplas').css('cursor','pointer');//SETA O TIPO DO CURSOR

                    //AO CLICAR SOBRE UMA LINHA DA TABALA COM A CLASSE TUPLA
                    $('.tuplas').click(function(e){
                        e.preventDefault();
                        //                        alert('deu certo');

                        //CAPTURANDO O C�DIGO, O NOME E O TELEFONE
                        codigo = $(this).attr('id');
                        nome = $(this).find(":eq(0)").text();
                        fone = $(this).find(":eq(1)").text();

                        //ATRIBUINDO VALORS CAMPOS
                        $('#'+origem+'nome').val(nome).attr('readonly',true);
                        $('#'+origem+'fone').val(fone).attr('readonly',true);
                        $('#'+origem+'cod').val(codigo);

                        //ZERANDO O ENDERE�O, A UF E A CIDADE
                        $('#'+origem+'endereco').val("");
                        $('select[name="'+origem+'uf"]').val("");
                        resetauf = origem;//VARI�VEL AUXILIAR PARA REMOVER O J DO CONJ
                        if(origem == "conj_") resetauf = origem.replace("j","")
                        $('#'+resetauf+'uf-button').find(":eq(0)").html('UF');
                        $('select[name="'+origem+'cidade"]').val("");

                        //OCULTANDO E EXIBINDO OS BOT�ES
                        ocultarExibir = origem.replace("_","");
                        $('#'+ocultarExibir).css('display','none');
                        $('#pesquisar'+ocultarExibir.toUpperCase()).css('display','none');
                        $('#cadastro'+ocultarExibir.toUpperCase()).css('display','block');

                        //AO CLICAR EM NOVA PESQUISA
                        $('#'+origem+'novaPesquisa').css('display','block').click(function(e){
                            e.preventDefault();
                            $('#pesquisar'+ocultarExibir.toUpperCase()).css('display','block');
                            $('#'+ocultarExibir).css('display','block');
                            $('#cadastro'+ocultarExibir.toUpperCase()).css('display','none');
                            $('#'+origem+'nome').val("").attr('readonly',false);
                            $('#'+origem+'fone').val("").attr('readonly',false);
                            $('#'+origem+'cod').val("0");
                            $(this).css('display','none');
                        });

                        //DIRECIONANDO USUARIO PARA O CADASTRO DO INADIMPLENTE
                        $('#cadastro'+ocultarExibir.toUpperCase()).click(function(){
                            link = base_url+'inadimplente/editar/cod:'+codigo;
                            window.open(link, 'blank');
                        });

                        //FECHA A CAIXA DE DIALOGO
                        $('.ui-dialog-titlebar-close').trigger('click');
                    });

                    //CASO OS DADOS RETORNADOS SEJAM NULOS
                    if (vazio)
                    {   //EXIBE MENSAGEM
                        $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Nenhum dado encontrado pesquisando por ['+valor+']</h3></td></tr>');
                    }
                }
            });
        }
        
        window.onbeforeunload = function() {
            if(fecharAba != '1'){
                return ' Se voc� fechar essa janela ir� perder tudo que voc� digitou at� agora. ';
            }
        }
        
        $("#nome").blur(function () {
            //ENTIDADE DO TIPO INADIMPLENTE
            entidade = 'i';

            //RECEBENDO O TIPO DE PESSOA [f ou j]
            pessoa = 'f';
            $('input[name="pessoa"]').each(function() {
                if ( $(this).is(':checked') ) {
                    pessoa = $(this).attr('value');
                }
            });
            //RECEBENDO O TIPO DE DADOS [nome_fantasia ou cpf_cnpj]
            tipodado = '';

            //RECEBENDO O VALOR DIGITADO EM nome_fantasia ou cpf_cnpj
            valor = '';
            if((($('input[name="nome"]').val() == '') && ($('input[name="cpf_cnpj"]').val() == '')) || $('input[name="cpf_cnpj"]').val() == '___.___.___-__'){
                $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Para pesquisar � necess�rio digitar o NOME ou CPF/CNPJ</h3></td></tr>');//zera os valores antigos e escreve no elemento o valor selecione
                return false;
            }
            else {
                if ($('input[name="cpf_cnpj"]').val() != ''){
                    valor = $('input[name="cpf_cnpj"]').val();
                    tipodado = 'c';
                }

                else if ($('input[name="nome_fantasia"]').val() != ''){
                    valor = $('input[name="nome_fantasia"]').val();
                    tipodado = 'n';
                }
            }
            if (valor != ''){
                //FUN��O AJAX
                $.ajax({
                    type: "POST",//TIPO DE DADOS
                    url: base_url+'inadimplente/listar_pesquisa_blur',//FUN��O DO CONTROLLER PARA TRATAR A LISTAGEM
                    data: 'entidade='+entidade+'&pessoa='+pessoa+'&tipodado='+tipodado+'&valor='+valor,//DADOS ENVIADOS
                    dataType: 'json',//TIPO DE DADOS A SER RECEBIDO

                    //CASO HAJA ERRO DE PROCESSAMENTO
                    error: function(xhr, status, er) {
                        alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                    },

                    //CASO A FUN��O DO CONTROLLER SEJA EXECUTADA COM SUCESSO E OS DADOS RECEBIDOS COM SUCESSO
                    success: function(retorno)
                    {
                        var codigo = '';
                        //                    $("#dialog-form").dialog("open");///ABRE A CAIXA DE DI�LOGO
                        //                    $('#dadosPesquisa').html('');//zera os valores antigos e escreve no elemento o valor selecione
                        vazio = false;//VARI�VEL PARA IDENTIFICAR SE ALGUM DADO �NTEGRO TENHA SIDO RECEBIDO

                        $.each(retorno.resultado, function(i, v){//para cada �ndice retornado executa a fun��o
                            if(v.nome != null){
                                codigo = v.codigo;
                                //CONCATENA AS LINHAS DA TABELA COM OS VALORES RECEBIDOS
                            }
                            else vazio = true;//CASO OS DADOS RETORNADOS SEJAM NULOS
                        });
                        if(codigo != ''){
                            if(confirm('J� existe um INADIMPLENTE possivelmente cadastrado com este NOME, gostaria de visualizar o cadastro?')){
                                link = base_url+'divida/ficha/cod:'+codigo;
//                                var aux = '<a href="'+link+'" id="aAuxiliar" target="_blank">clica</a>'
//                                
//                                $('#divAAuxiliar').html(aux);
//                                $('#aAuxiliar').trigger('click');
                                window.open(link, '_blank');
                            }
                        }
                    }
                });
            }
            
        });
        
    });
</script>
<div id="divAAuxiliar">
    
</div>
<div id="dialog-form" style="padding-left: 5px; padding-right: 5px; " title="Pesquisar dados">
    <p style="text-align: center">
        <strong>Dados encontrados:</strong>
    </p>
    <div id="content" style="width: 750px;">
        <div id="right" style="margin-left: 0px;">
            <div class="box">
                <div class="table" style="padding-left: 0px; font-size: 10px; width: 750px;">
                    <form action="" method="post">
                        <table id="products">
                            <thead>
                                <tr>
                                    <th class="left" style="width: 20%;">NOME</th>
                                    <th style="width: 14%;">CPF/CNPJ</th>
                                    <th>ENDERE�O</th>
                                    <th class="last">CIDADE</th>
                                </tr>
                            </thead>
                            <tbody id="dadosPesquisa">
                                <!--RESULTADO DA PESQUISA AQUI!! -->
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Cadastro de inadimplente</h5>
            </div>
            <?php echo $mensagem ?>
            <form id="form" action="<?php echo base_url() . 'inadimplente/incluir'; ?>" method="post">
                <div class="form">
                    <div class="fields">
                        <div class="field  field-first">
                            <div class="divleft" style="width: 116px;">
                                <div class="label">
                                    <label>C�digo:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;">[NOVO]</p>
                            </div>
                            <div class="divleft" style="width: 200px;">
                                <div class="label">
                                    <label>Pessoa:</label>
                                </div>
                                <input style="margin-top: 7px; " type="radio" name="pessoa" value="f" id="pfisica">F�sica
                                <input type="radio" name="pessoa" value="j" id="pjuridica">Jur�dica
                            </div>
                            <div id="atrsPessoaFisica" style="width:375px; height:30px; float: right; overflow: hidden">
                                <div id="sexo_est_civil">
                                    <div class="divleft" style="width: 173px;" id="sexo" >
                                        <div class="label">
                                            <label>Sexo:</label>
                                        </div>
                                        <input type="radio" name="sexo" value="m" style="margin-top: 7px;">Masc.
                                        <input type="radio" name="sexo" value="f">Fem.
                                    </div>
                                    <div class="divleftlast" style="width: 184px;" id="estadocivil">
                                        <div class="select">
                                            <select style="width: 186px; margin-left: 5px;" id="est_civil" name="estado_civil">
                                                <option value="0" class="ec">Estado civil</option>
                                                <option value="1" class="ec">Solteiro(a)</option>
                                                <option value="2" class="ec">Casado(a)</option>
                                                <option value="3" class="ec">Separado(a)</option>
                                                <option value="4" class="ec">Divorciado(a)</option>
                                                <option value="5" class="ec">Viuvo(a)</option>
                                                <option value="6" class="ec">Amasiado(a)</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 371px;">
                                <div style="width: 89px;" class="label">
                                    <label for="nome">Nome:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 248px;" type="text" id="nome" name="nome_fantasia" maxlength="120" value=""/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 231px;">
                                <div style="width: 76px; padding-left: 1px;" class="label">
                                    <label for="cpf_cnpj">CPF/CNPJ:</label>
                                </div>
                                <div class="input" id="cpf_cnpj">
                                    <input style="width: 124px;" type="text" id="cpf_cnpjInad" name="cpf_cnpj" maxlength="18"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <button style="width: 85px; padding: 4px;" id="pesquisarIC" name="pesquisarIC" type="button" class="dialog-form-open">Pesquisar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 211px;">
                                <div style="width: 89px;" class="label">
                                    <label for="rg_ie">RG/I.E.:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 89px;" type="text" id="rg_ie" maxlength="30" name="rg_ie"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 250px;">
                                <div style="width: 42px; padding-left: 1px;" class="label">
                                    <label for="endereco">End.:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 178px;" type="text" id="endereco" maxlength="130" name="endereco"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 233px; margin: 0px;">
                                <div style="width: 51px; padding-left: 1px;" class="label">
                                    <label for="bairro">Bairro:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 155px;" type="text" id="bairro" maxlength="50" name="bairro"/>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 211px;">
                                <div style="width: 89px;" class="label">
                                    <label for="complemento">Compl.:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 89px;" type="text" id="complemento" name="complemento" maxlength="40"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 132px;">
                                <div style="width: 34px; padding-left: 1px;" class="label">
                                    <label for="cep">CEP.:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 65px;" type="text" id="cep" name="cep" maxlength="8"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 109px;">
                                <div style="width: 27px; padding-left: 1px;" class="label">
                                    <label for="uf">UF:</label>
                                </div>
                                <div class="select">
                                    <select style="width: 65px; margin-left: 7px;" id="ina_uf" name="uf">
                                        <option value="" selected>UF</option>
                                        <?php foreach ($estados as $uf): ?>
                                            <option value="<?php echo $uf->cid_estado; ?>"><?php echo $uf->cid_estado; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 226px; margin: 0px; float: left">
                                <div class="select">
                                    <select style="width: 230px; height: 28px; border: 1px solid #D4D0C8; padding: 5px; font-size: 11px; font-family: Verdana" id="ina_cidade" name="cidade">
                                        <option value="" id="cidadevazia"><-- SELECIONE UM ESTADO</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 240px;">
                                <div style="width: 89px;" class="label">
                                    <label for="foneres">Fone Res.:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 117px;" type="text" class="telefone" id="foneres" name="foneres"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 238px;">
                                <div style="width: 77px; padding-left: 1px;" class="label">
                                    <label for="fonerec">Fone Rec.:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 130px;" type="text" class="telefone" id="fonerec" name="fonerec"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 216px; margin: 0px;">
                                <div style="width: 74px; padding-left: 1px;" class="label">
                                    <label for="fonecom">Fone Com.:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 115px;" type="text" class="telefone" id="fonecom" name="fonecom"/>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 240px;">
                                <div style="width: 89px;" class="label">
                                    <label for="cel1">Celular(1):</label>
                                </div>
                                <div class="input">
                                    <input style="width: 117px;" type="text" class="telefone" id="cel1" name="cel1"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 238px;">
                                <div style="width: 77px; padding-left: 1px;" class="label">
                                    <label for="cel2">Celular(2):</label>
                                </div>
                                <div class="input">
                                    <input style="width: 130px;" type="text" class="telefone" id="cel2" name="cel2"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 216px; margin: 0px;">
                                <div style="width: 74px; padding-left: 1px;" class="label">
                                    <label for="cel3">Celular(3):</label>
                                </div>
                                <div class="input">
                                    <input style="width: 115px;" type="text" class="telefone" id="cel3" name="cel3"/>
                                </div>
                            </div>
                        </div>
                        <div style="height: 205px;" class="field">
                            <div class="divleftlast" style="width: 701px;">
                                <div style="width: 84px;" class="label">
                                    <label for="info">Informa��es relevantes:</label>
                                </div>
                                <div class="textarea">
                                    <textarea style="width: 586px; height: 180px;" id="info" name="info" cols="500" rows="4"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="blocoTitulo">
                            Parentes de 1� grau
                        </div>
                        <div class="field" style="border-bottom: none;">
                            <div class="divleft" style="width: 406px;">
                                <div style="width: 89px;" class="label">
                                    <label for="conj_nome">Conjuge:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 283px;" type="text" id="conj_nome" maxlength="50" name="conj_nome"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 196px;">
                                <div style="width: 41px; padding-left: 1px;" class="label">
                                    <label for="conj_fone">Fone:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 124px;" type="text" class="telefone" id="conj_fone" name="conj_fone"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <button style="width: 85px; padding: 4px;" id="pesquisarCONJ" name="pesquisarCONJ" type="button" class="dialog-form-open">Pesquisar</button>
                                        <button style="width: 85px; padding: 4px;" id="cadastroCONJ" name="cadastroCONJ" type="button" class="dialog-form-open">Cadastro</button>
                                        <input type="hidden" id="conj_cod" name="conj_cod" value="0"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="conj_novaPesquisa" style="text-align: center; display:none"><a href="#">Nova Pesquisa</a></div>
                        <div class="field">
                            <div id="conj">
                                <div class="divleft" style="width: 351px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="conj_endereco">Endere�o:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 223px;" type="text" id="conj_endereco" maxlength="130" name="conj_endereco"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 109px;">
                                    <div style="width: 27px; padding-left: 1px;" class="label">
                                        <label for="uf">UF:</label>
                                    </div>
                                    <div class="select">
                                        <select style="width: 65px; margin-left: 7px;" id="con_uf" name="conj_uf">
                                            <option value="" selected>UF</option>
                                            <?php foreach ($estados as $uf): ?>
                                                <option value="<?php echo $uf->cid_estado; ?>"><?php echo $uf->cid_estado; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 226px; margin: 0px; float: left">
                                    <div class="select">
                                        <select style="width: 230px; height: 28px; border: 1px solid #D4D0C8; padding: 5px; font-size: 11px; font-family: Verdana" id="con_cidade" name="conj_cidade">
                                            <option value="" id="cidadevazia"><-- SELECIONE UM ESTADO</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field" style="border-bottom: none;">
                            <div class="divleft" style="width: 406px;">
                                <div style="width: 89px;" class="label">
                                    <label for="pai_nome">Pai:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 283px;" type="text" id="pai_nome" maxlength="50" name="pai_nome"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 196px;">
                                <div style="width: 41px; padding-left: 1px;" class="label">
                                    <label for="pai_fone">Fone:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 124px;" type="text" class="telefone" id="pai_fone" name="pai_fone"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <button style="width: 85px; padding: 4px;" id="pesquisarPAI" name="pesquisarPAI" type="button" class="dialog-form-open">Pesquisar</button>
                                        <button style="width: 85px; padding: 4px;" id="cadastroPAI" name="cadastroPAI" type="button" class="dialog-form-open">Cadastro</button>
                                        <input type="hidden" id="pai_cod" name="pai_cod" value="0"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="pai_novaPesquisa" style="text-align: center; display:none"><a href="#">Nova Pesquisa</a></div>
                        <div class="field">
                            <div id="pai">

                                <div class="divleft" style="width: 351px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="pai_endereco">Endere�o:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 223px;" type="text" id="pai_endereco" maxlength="130" name="pai_endereco"/>
                                    </div>
                                </div>

                                <div class="divleftlast" style="width: 109px;">
                                    <div style="width: 27px; padding-left: 1px;" class="label">
                                        <label for="uf">UF:</label>
                                    </div>
                                    <div class="select">
                                        <select style="width: 65px; margin-left: 7px;" id="pai_uf" name="pai_uf">
                                            <option value="" selected>UF</option>
                                            <?php foreach ($estados as $uf): ?>
                                                <option value="<?php echo $uf->cid_estado; ?>"><?php echo $uf->cid_estado; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 226px; margin: 0px; float: left">
                                    <div class="select">
                                        <select style="width: 230px; height: 28px; border: 1px solid #D4D0C8; padding: 5px; font-size: 11px; font-family: Verdana" id="pai_cidade" name="pai_cidade">
                                            <option value="" id="cidadevazia"><-- SELECIONE UM ESTADO</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field" style="border-bottom: none;">
                            <div class="divleft" style="width: 406px;">
                                <div style="width: 89px;" class="label">
                                    <label for="mae_nome">M�e:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 283px;" type="text" id="mae_nome" maxlength="50" name="mae_nome"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 196px;">
                                <div style="width: 41px; padding-left: 1px;" class="label">
                                    <label for="mae_fone">Fone:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 124px;" type="text" class="telefone" id="mae_fone" name="mae_fone"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <button style="width: 85px; padding: 4px;" id="pesquisarMAE" name="pesquisarMAE" type="button" class="dialog-form-open">Pesquisar</button>
                                        <button style="width: 85px; padding: 4px;" id="cadastroMAE" name="cadastroMAE" type="button" class="dialog-form-open">Cadastro</button>
                                        <input type="hidden" id="mae_cod" name="mae_cod" value="0"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="mae_novaPesquisa" style="text-align: center; display:none"><a href="#">Nova Pesquisa</a></div>
                        <div class="field">
                            <div id="mae">
                                <div class="divleft" style="width: 351px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="mae_endereco">Endere�o:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 223px;" type="text" id="mae_endereco" maxlength="130" name="mae_endereco"/>
                                    </div>
                                </div>

                                <div class="divleftlast" style="width: 109px;">
                                    <div style="width: 27px; padding-left: 1px;" class="label">
                                        <label for="uf">UF:</label>
                                    </div>
                                    <div class="select">
                                        <select style="width: 65px; margin-left: 7px;" id="mae_uf" name="mae_uf">
                                            <option value="" selected>UF</option>
                                            <?php foreach ($estados as $uf): ?>
                                                <option value="<?php echo $uf->cid_estado; ?>"><?php echo $uf->cid_estado; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 226px; margin: 0px; float: left">
                                    <div class="select">
                                        <select style="width: 230px; height: 28px; border: 1px solid #D4D0C8; padding: 5px; font-size: 11px; font-family: Verdana" id="mae_cidade" name="mae_cidade">
                                            <option value="" id="cidadevazia"><-- SELECIONE UM ESTADO</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <input type="reset" name="cancelar" value="Cancelar" />
                            <!--                                <a href="</?php echo base_url() . 'cadbem/novo' ?>">
                                                            <div style="margin-left: 7px;" class="highlight">
                                                                <input type="submit" name="bem" value="Bens" />
                                                            </div>
                                                        </a>-->
                            <div style="margin-left: 7px;" class="highlight">
                                <input type="submit" name="cadastrar" value="Cadastrar" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>