<script type="text/javascript">
    v1 = '';
    $(document).ready(function(){
        $('input[name="honorario"]').mask('99.99').click(function(){$(this).select();});
        $('input[name="juros"]').mask('99.99').click(function(){$(this).select();});
        $('#cep_rep').mask('99999999').click(function(){$(this).select();});
        $('#cep').mask('99999999').click(function(){$(this).select();});
        $('#cpf').mask('999.999.999-99').click(function(){$(this).select();});
        //$('input[name="cpf_cnpj"]').attr('disabled',true);
        $('.telefone').mask("(99)9999-9999?9").blur(function(){
			    var phone, element;
			    element = $(this);
			    element.unmask();
			    phone = element.val().replace(/\D/g, '');
			    if(phone.length > 10) {
			        element.mask("(99)99999-999?9");
			    } else {
			        element.mask("(99)9999-9999?9");
			    }
			    //element.unmask();
			}).click(function(){$(this).select();});

        //------------------------------------------------------------------------------
        //---------------------------Fun��o para validar CNPJ---------------------------
        //------------------------------------------------------------------------------
        function validaCNPJ() {
            CNPJ = $('input[name="cpf_cnpj"]').val();
            erro = new String;
            if (CNPJ.length < 18) erro += "� necessario preencher corretamente o n�mero do CNPJ! \n\n";
            if ((CNPJ.charAt(2) != ".") || (CNPJ.charAt(6) != ".") || (CNPJ.charAt(10) != "/") || (CNPJ.charAt(15) != "-")){
                if (erro.length == 0) erro += "� necess�rio preencher corretamente o n�mero do CNPJ! \n\n";
            }
            //substituir os caracteres que n�o s�o n�meros
            if(document.layers && parseInt(navigator.appVersion) == 4){
                x = CNPJ.substring(0,2);
                x += CNPJ. substring (3,6);
                x += CNPJ. substring (7,10);
                x += CNPJ. substring (11,15);
                x += CNPJ. substring (16,18);
                CNPJ = x;
            } else {
                CNPJ = CNPJ. replace (".","");
                CNPJ = CNPJ. replace (".","");
                CNPJ = CNPJ. replace ("-","");
                CNPJ = CNPJ. replace ("/","");
            }
            var nonNumbers = /\D/;
            if (nonNumbers.test(CNPJ)) erro += "A verifica��o de CNPJ suporta apenas n�meros! \n\n";
            var a = [];
            var b = new Number;
            var c = [6,5,4,3,2,9,8,7,6,5,4,3,2];
            for (i=0; i<12; i++){
                a[i] = CNPJ.charAt(i);
                b += a[i] * c[i+1];
            }
            if ((x = b % 11) < 2) { a[12] = 0 } else { a[12] = 11-x }
            b = 0;
            for (y=0; y<13; y++) {
                b += (a[y] * c[y]);
            }
            if ((x = b % 11) < 2) { a[13] = 0; } else { a[13] = 11-x; }
            if ((CNPJ.charAt(12) != a[12]) || (CNPJ.charAt(13) != a[13])){
                erro +="CNPJ INV�LIDO, Por favor verifique!";
            }
            if (erro.length > 0){
                alert(erro);
                return false;
            }
            return true;
        }
        //------------------------------------------------------------------------------
        //---------------------------Fun��o para validar CPF----------------------------
        //------------------------------------------------------------------------------
        function validaCPF() {
            cpf = $('input[name="cpf_cnpj"]').val();

            cpf = cpf.replace(".", "");
            cpf = cpf.replace(".", "");
            cpf = cpf.replace("-", "");

            erro = new String;
            if (cpf.length < 11) erro += "Sao necessarios 11 digitos para verificacao do CPF! \n\n";
            var nonNumbers = /\D/;
            if (nonNumbers.test(cpf)) erro += "A verificacao de CPF suporta apenas numeros! \n\n";
            if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999"){
                erro += "Numero de CPF invalido!"
            }
            var a = [];
            var b = new Number;
            var c = 11;
            for (i=0; i<11; i++){
                a[i] = cpf.charAt(i);
                if (i < 9) b += (a[i] * --c);
            }
            if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
            b = 0;
            c = 11;
            for (y=0; y<10; y++) b += (a[y] * c--);
            if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
            if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10])){
                erro +="Numero de CPF invalido!, Por Favor confira";
            }
            if (erro.length > 0){
                alert(erro);
                return false;
            }
            return true;
        }
        //------------------------------------------------------------------------------
        //--------------------------- Fim das Valida��es -------------------------------
        //------------------------------------------------------------------------------
        
        //SE O RADIO PESSOA JUR�DICA VIER MARCADO
        if($('input[name="pessoa"][value="j"]').is(':checked'))
        {
            pessoa='j';
            $('#razao_social_field').show();

            $('label[for="nome"]').text('Nome Fantasia:');
            //DESMARCAR O SEXO
            $("input[name='sexo']").each(function() {
                //desmarcar
                this.checked = false;
            });

            //DESMARCAR ESTADO CIVIL AINDA N�O FUNCIONA
            $('#est_civil option[value="0"]').attr('selected', true);

            //RESETAR CPF/CNPJ
            $('#cpf_cnpj').html('<input style="width: 124px;" type="text" name="cpf_cnpj"/>');

            //HABILITAR CAMPO E APLICAR M�SCARA PARA CPF
            $('input[name="cpf_cnpj"]').attr('disabled',false).mask('99.999.999/9999-99');
            $('input[name="cpf_cnpj"]').attr('value', '<?php echo $dados->cre_pessoa == "j" ? "$dados->cre_cpf_cnpj" : ""; ?>');
        }

        //SE O RADIO PESSOA F�SICA VIER MARCADO
        if($('input[name="pessoa"][value="f"]').is(':checked'))
        {
            pessoa='f';
            $('#razao_social_field').hide();

            //SETA NULO NO INPUT RAZ�O SOCIAL
            $('input[name="razao_social"]').val('<?php echo $dados->cre_pessoa == "j" ? "$dados->cre_razao_social" : ""; ?>');

            //ALTERA O LABEL DE NOME FANTASIA PARA NOME
            $('label[for="nome"]').text('Nome:');

            $('#sexo_est_civil').css('padding-top','0px');
        }

        //AO CLICAR EM PESSOA JURIDICA
        $('input[name="pessoa"][value="j"]').click(function() {
            pessoa='j';
            $('#razao_social_field').show();

            $('label[for="nome"]').text('Nome Fantasia:');
            //DESMARCAR O SEXO
            $("input[name='sexo']").each(function() {
                //desmarcar
                this.checked = false;
            });

            //DESMARCAR ESTADO CIVIL AINDA N�O FUNCIONA
            $('#est_civil option[value="0"]').attr('selected', true);

            //RESETAR CPF/CNPJ
            $('#cpf_cnpj').html('<input style="width: 124px;" type="text" name="cpf_cnpj"/>');

            //HABILITAR CAMPO E APLICAR M�SCARA PARA CPF
            $('input[name="cpf_cnpj"]').attr('disabled',false).mask('99.999.999/9999-99');
            $('input[name="cpf_cnpj"]').attr('value', '<?php echo $dados->cre_pessoa == "j" ? "$dados->cre_cpf_cnpj" : ""; ?>');
        });

        //AO CLICAR EM PESSOA F�SICA
        $('input[name="pessoa"][value="f"]').click(function() {
            pessoa='f';
            //OCULTA RAZ�O SOCIAL
            $('#razao_social_field').hide();

            //SETA NULO NO INPUT RAZ�O SOCIAL
            $('input[name="razao_social"]').val('<?php echo $dados->cre_pessoa == "j" ? "$dados->cre_razao_social" : ""; ?>');

            //ALTERA O LABEL DE NOME FANTASIA PARA NOME
            $('label[for="nome"]').text('Nome:');

            //RESETAR CPF/CNPJ
            $('#cpf_cnpj').html('<input style="width: 124px;" type="text" name="cpf_cnpj" />');

            //APLICAR M�SCARA PARA CNPJ
            $('input[name="cpf_cnpj"]').attr('disabled',false).mask('999.999.999-99');
            $('input[name="cpf_cnpj"]').attr('value', '<?php echo $dados->cre_pessoa == "f" ? "$dados->cre_cpf_cnpj" : ""; ?>');
        });

        //VALIDA��O DOS CAMPOS OBRIGAT�RIOS
        $('#form1').submit(function(){
            if(($('input[name="cpf_cnpj"]').val())!=''){
                if(pessoa=='f'){
                    ret = validaCPF();
                    if(ret==false){
                        return false;
                    }
                }else if(pessoa=='j'){
                    ret = validaCNPJ();
                    if(ret==false){
                        return false;
                    }
                }
            }

            var pessoaVazio = true;
            $('input[name="pessoa"]').each(function() {
                if ( $(this).is(':checked') ) {
                    pessoaVazio = false;
                }
            });

            if($('input[name="nome_fantasia"]').val() == ''){
                $('input[name="nome_fantasia"]').focus();
                return false;
            }
            else if(pessoaVazio){
                alert('Indique pessoa F�SICA ou JUR�DICA.');
                return false;
            }

            else if($('input[name="honorario"]').val() == ''){
                $('input[name="honorario"]').focus();
                return false;
            }

            else if($('input[name="juros"]').val() == ''){
                $('input[name="juros"]').focus();
                return false;
            }

            else if($('input[name="prazo"]').val() == ''){
                $('input[name="prazo"]').focus();
                return false;
            }

            else if($('#cidade option:selected').val() == '' || $('#uf option:selected').val() == ''){
                alert('Selecione uma CIDADE e um ESTADO');
                return false;
            }


            else if($('input[name="responsavel"]').val() == ''){
                $('input[name="responsavel"]').focus();
                return false;
            }

            else if($('input[name="senha"]').val() != $('input[name="senha_confirma"]').val()){
                if($('input[name="usuario"]').val() == '')
                    alert('Digite um USU�RIO');
                else
                    alert('Confirme a SENHA corretamente');
                return false;
            }
            else{
                var marcado = false;
                
                $(".checkpasse").each(function() {
                    if(this.checked){
                        marcado = true;
                        return false;
                    }
                    
                });
                if(!marcado){
                    alert('Selecione ao menos um tipo de REPASSE')
                    return false;
                }
                
            }

        });

        //VALDA��O DOS CAMPOS OBRIGATORIOS DO FORMULARIO DE REPRESENTANTES
        $('#formRep').submit(function(){
            if($('input[name="nome"]').val() == ''){
                $('input[name="nome"]').focus();
                return false;
            }
        });
        $('.excluir_rep').click(function(){
            var cod = $(this).attr('id');
            var cre = "<?php echo $dados->cre_cod; ?>";
            var link = "<?php echo base_url().'credor/excluir_representante/cod:';?>"+cod+"/cre:"+cre;
            var confirmacao = confirm('Tem certeza de que deseja EXCLUIR esse representante?');
            if(confirmacao)
            {
                location.href = link;
            }
            else return confirmacao;
        });

        //LISTANDO AS CIDADES
        $('select[name="uf"]').change(function(){

            v1 = '';
            //captura os options selecionados
            v1 = $('select[name="uf"] option:selected').text();
            if(v1 == '') v1='UF';
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() . 'credor/listar_cidades'; ?>",
                data: 'v1='+v1,
                dataType: 'json',
                error: function(xhr, status, er) {
                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                },
                success: function(retorno)
                {
                    $('#cidade').html('<option value="">SELECIONE UMA CIDADE</option>');//zera os valores antigos e escreve no elemento o valor selecione
                    $qtd = '';
                    $valor = '';
                    $.each(retorno.cidadesbrasil, function(i, v){//para cada �ndice retornado executa a fun��o
                        if(v.cidadeatual != ''){
                            $('#cidade-button').fadeOut('normal',function(){
                                //ESCONDE O OPTION INICIAL SEM VALOR
                            });
                            $('#cidade').append('<option value="'+unescape(v.cidadeatual)+'">'+unescape(v.cidadeatual)+'</option>');//unescape descriptografa o padr�o URL de dados retornado no PHP
                        }
                        $qtd = i;
                        $valor = v.cidadeatual;
                    });
                    if ($qtd != 0 && $valor != ''){
                        $('#cidade').fadeIn('normal', function(){
                            //CIDADES CARREGADAS COM SUCESSO
                        });
                    }
                    else
                    {
                        alert('Voc� deve selecionar um ESTADO v�lido.');
                        $('#cidade').html('<option value=""><-- SELECIONE UM ESTADO</option>');//zera os valores antigos e escreve no elemento o valor selecione
                    }
                }
            });
        });
    });
</script>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right" style="min-height: 798px;">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Cadastro (<?php echo $dados->cre_nome_fantasia; ?>)</h5>
                <ul class="links">
                    <li><a href="#cadastro">Cadastro</a></li>
                    <li><a href="#representantes">Representantes</a></li>
                    <li><a href="#relatorios">Relat�rios</a></li>
                </ul>
            </div>
            <div id="cadastro">
                <?php echo $mensagem; ?>
                <form id="form1" action="<?php echo base_url() . 'credor/update'; ?>" method="post" enctype="multipart/form-data">
                    <div class="form">

                        <div class="fields">
                            <div class="field  field-first">
                                <div class="divleft" style="width: 116px;">
                                    <div class="label">
                                        <label>C�digo:</label>
                                    </div>
                                    <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dados->cre_cod; ?></p>
                                </div>
                                <div class="divleft" style="width: 200px;">
                                    <div class="label">
                                        <label>Pessoa:</label>
                                    </div>
                                    <input style="margin-top: 7px; " type="radio" name="pessoa" value="f" id="pfisica" <?php echo $dados->cre_pessoa == 'f' ? 'checked' : ''; ?>>F�sica
                                    <input type="radio" name="pessoa" value="j" id="pjuridica" <?php echo $dados->cre_pessoa == 'j' ? 'checked' : ''; ?>>Jur�dica
                                </div>
                                <div id="atrsPessoaFisica" style="width:375px; height:30px; float: right; overflow: hidden">
                                    <div id="sexo_est_civil">
                                        <div class="divleft" style="width: 173px;" id="sexo" >
                                            <div class="label">
                                                <label>Sexo:</label>
                                            </div>
                                            <input type="radio" name="sexo" value="m" style="margin-top: 7px;" <?php echo $dados->cre_sexo == 'm' ? 'checked' : ''; ?>/>Masc.
                                            <input type="radio" name="sexo" value="f" <?php echo $dados->cre_sexo == 'f' ? 'checked' : ''; ?>/>Fem.
                                        </div>
                                        <div class="divleftlast" style="width: 184px;" id="estadocivil">
                                            <div class="select">
                                                <select style="width: 186px; margin-left: 5px;" id="est_civil" name="estado_civil">
                                                    <option value="0" class="ec">Estado civil</option>
                                                    <option value="1" class="ec" <?php echo $dados->cre_estado_civil == 1 ? 'selected' : ''; ?>>Solteiro(a)</option>
                                                    <option value="2" class="ec" <?php echo $dados->cre_estado_civil == 2 ? 'selected' : ''; ?>>Casado(a)</option>
                                                    <option value="3" class="ec" <?php echo $dados->cre_estado_civil == 3 ? 'selected' : ''; ?>>Separado(a)</option>
                                                    <option value="4" class="ec" <?php echo $dados->cre_estado_civil == 4 ? 'selected' : ''; ?>>Divorciado(a)</option>
                                                    <option value="5" class="ec" <?php echo $dados->cre_estado_civil == 5 ? 'selected' : ''; ?>>Viuvo(a)</option>
                                                    <option value="6" class="ec" <?php echo $dados->cre_estado_civil == 6 ? 'selected' : ''; ?>>Amasiado(a)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field" id="razao_social_field">
                                <div class="divleftlast" style="margin-left: 0px; width: 706px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="razao_social">Raz�o Social:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 591px;" type="text" id="razao_social" name="razao_social" maxlength="120" value="<?php echo $dados->cre_razao_social; ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 467px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="nome">Nome Fantasia:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 344px;" type="text" id="nome" name="nome_fantasia" maxlength="120" value="<?php echo $dados->cre_nome_fantasia; ?>"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 231px;">
                                    <div style="width: 76px; padding-left: 1px;" class="label">
                                        <label for="cpf_cnpj">CPF/CNPJ:</label>
                                    </div>
                                    <div class="input" id="cpf_cnpj">
                                        <input style="width: 128px;" type="text" name="cpf_cnpj" maxlength="18" value="<?php echo $dados->cre_cpf_cnpj; ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 211px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="rg_ie">RG/I.E.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 89px;" type="text" id="rg_ie" name="rg_ie" maxlength="30" value="<?php echo $dados->cre_rg_ie; ?>"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 250px;">
                                    <div style="width: 42px; padding-left: 1px;" class="label">
                                        <label for="endereco">End.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 178px;" type="text" id="endereco" name="endereco" maxlength="130" value="<?php echo $dados->cre_endereco; ?>"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 233px; margin: 0px;">
                                    <div style="width: 51px; padding-left: 1px;" class="label">
                                        <label for="bairro">Bairro:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 155px;" type="text" id="bairro" name="bairro" maxlength="50" value="<?php echo $dados->cre_bairro; ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 211px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="complemento">Compl.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 89px;" type="text" id="complemento" name="complemento" maxlength="40" value="<?php echo $dados->cre_complemento; ?>"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 132px;">
                                    <div style="width: 34px; padding-left: 1px;" class="label">
                                        <label for="cep">CEP.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 65px;" type="text" id="cep" name="cep" maxlength="8" value="<?php echo $dados->cre_cep; ?>"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 109px;">
                                    <div style="width: 27px; padding-left: 1px;" class="label">
                                        <label for="uf">UF:</label>
                                    </div>
                                    <div class="select">
                                        <select style="width: 65px; margin-left: 7px;" id="uf" name="uf">
                                            <option>UF</option>
                                            <?php foreach ($estados as $uf): ?>
                                                <option value="<?php echo $uf->cid_estado; ?>" <?php echo $dados->cre_uf == $uf->cid_estado ? 'selected' : ''; ?>><?php echo $uf->cid_estado; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 230px; margin: 0px; float: left">
                                        <div class="select">
                                            <select style="width: 230px; height: 28px; border: 1px solid #D4D0C8; padding: 5px; font-size: 11px; font-family: Verdana" id="cidade" name="cidade">
                                                <option style="border: 1px solid;"><?php echo $dados->cre_cidade; ?></option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="field">
                                    <div class="divleft" style="width: 240px;">
                                        <div style="width: 89px;" class="label">
                                            <label for="fone1">Fone1:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 117px;" type="text" id="fone1" name="fone1" class="telefone" maxlength="15" value="<?php echo $dados->cre_fone1; ?>"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 238px;">
                                        <div style="width: 77px; padding-left: 1px;" class="label">
                                            <label for="fone2">Fone2:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 130px;" type="text" id="fone2" name="fone2" class="telefone" maxlength="15" value="<?php echo $dados->cre_fone2; ?>"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 216px; margin: 0px;">
                                        <div style="width: 59px; padding-left: 1px;" class="label">
                                            <label for="fax">Fax:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 130px;" type="text" id="fax" name="fax" class="telefone" maxlength="15" value="<?php echo $dados->cre_fax; ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="width: 371px;">
                                        <div style="width: 89px;" class="label">
                                            <label for="site">Site:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 248px;" type="text" id="site" name="site" maxlength="100" value="<?php echo $dados->cre_site; ?>"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 326px; margin-left: 0px;">
                                        <div style="width: 50px; padding-left: 0px;" class="label">
                                            <label for="email">E-Mail:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 256px;" type="text" id="email" name="email" value="<?php echo $dados->cre_email; ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="field" style="height: 50px;">
                                    <div class="divleftlast" style="width: 704px; margin-left: 0px;">
                                        <div style="width: 89px;" class="label">
                                            <label for="logotipo">Logotipo:</label>
                                        </div>
                                        <div class="input input-file">
                                            <input type="file" id="file" name="logo" size="40" maxlength="255" value=""/>
                                            <input type="hidden" name="valida_logo" value="<?php echo $dados->cre_logo; ?>"/>
                                        </div>
                                        <div class="label" style="width: 289px;">
                                            <label for="logotipoAtual">Atual: <?php echo $dados->cre_logo; ?></label>
                                        </div>
                                    </div>
                                </div>
                                <div style="height: 75px;" class="field">
                                    <div class="divleftlast" style="width: 701px;">
                                        <div style="width: 84px;" class="label">
                                            <label for="outras_info">Outras informa��es:</label>
                                        </div>
                                        <div class="textarea">
                                            <textarea style="width: 586px; height: 50px;" id="outras_info" name="info" cols="500" rows="4" class="editor"><?php echo $dados->cre_info; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                                    Contatos na empresa
                                </div>
                                <div class="field">
                                    <div class="divleft" style="border: none; width: 166px;">
                                        <div style="margin-left: 6px;" class="select">
                                            <select style="width: 160px;" id="parenteses" name="cont1_cargo">
                                                <option value="">[CARGO]</option>
                                                <option <?php echo $dados->cre_cont1_cargo == 'GERENTE GERAL' ? 'selected' : ''; ?>>Gerente geral</option>
                                                <option <?php echo $dados->cre_cont1_cargo == 'GERENTE DE COBRANCA' ? 'selected' : ''; ?>>Gerente de cobran�a</option>
                                                <option <?php echo $dados->cre_cont1_cargo == 'GERENTE FINANCEIRO' ? 'selected' : ''; ?>>Gerente financeiro</option>
                                                <option <?php echo $dados->cre_cont1_cargo == 'GERENTE DE VENDAS' ? 'selected' : ''; ?>>Gerente de vendas</option>
                                                <option <?php echo $dados->cre_cont1_cargo == 'GERENTE[OUTRO]' ? 'selected' : ''; ?>>Gerente[outro]</option>
                                                <option <?php echo $dados->cre_cont1_cargo == 'OPERADOR(A) DE CAIXA' ? 'selected' : ''; ?>>Operador(a) de Caixa</option>
                                                <option <?php echo $dados->cre_cont1_cargo == 'VENDEDOR(A)' ? 'selected' : ''; ?>>Vendedor(a)</option>
                                                <option <?php echo $dados->cre_cont1_cargo == 'AUXILIAR ADMINISTRATIVO' ? 'selected' : ''; ?>>Auxiliar administrativo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 186px;">
                                        <div class="input">
                                            <input style="width: 161px;" type="text" id="input-medium" name="cont1_nome" maxlength="50" value="<?php echo $dados->cre_cont1_nome; ?>"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="margin-right: 4px; width: 171px;">
                                        <div style="width: 51px; padding-left: 1px;" class="label">
                                            <label for="input-medium">E-mail:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 90px;" type="text" id="input-medium" name="cont1_email" maxlength="100" value="<?php echo $dados->cre_cont1_email; ?>"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 162px;">
                                        <div style="width: 45px; padding-left: 1px;" class="label">
                                            <label for="input-medium">Fone:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 90px;" type="text" id="input-medium" name="cont1_fone" class="telefone" maxlength="15" value="<?php echo $dados->cre_cont1_fone; ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="divleft" style="border: none; width: 166px;">
                                        <div style="margin-left: 6px;" class="select">
                                            <select style="width: 160px;" id="parenteses" name="cont2_cargo">
                                                <option value="">[CARGO]</option>
                                                <option <?php echo $dados->cre_cont2_cargo == 'GERENTE GERAL' ? 'selected' : ''; ?>>Gerente geral</option>
                                                <option <?php echo $dados->cre_cont2_cargo == 'GERENTE DE COBRANCA' ? 'selected' : ''; ?>>Gerente de cobran�a</option>
                                                <option <?php echo $dados->cre_cont2_cargo == 'GERENTE FINANCEIRO' ? 'selected' : ''; ?>>Gerente financeiro</option>
                                                <option <?php echo $dados->cre_cont2_cargo == 'GERENTE DE VENDAS' ? 'selected' : ''; ?>>Gerente de vendas</option>
                                                <option <?php echo $dados->cre_cont2_cargo == 'GERENTE[OUTRO]' ? 'selected' : ''; ?>>Gerente[outro]</option>
                                                <option <?php echo $dados->cre_cont2_cargo == 'OPERADOR(A) DE CAIXA' ? 'selected' : ''; ?>>Operador(a) de Caixa</option>
                                                <option <?php echo $dados->cre_cont2_cargo == 'VENDEDOR(A)' ? 'selected' : ''; ?>>Vendedor(a)</option>
                                                <option <?php echo $dados->cre_cont2_cargo == 'AUXILIAR ADMINISTRATIVO' ? 'selected' : ''; ?>>Auxiliar administrativo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 186px;">
                                        <div class="input">
                                            <input style="width: 161px;" type="text" id="input-medium" name="cont2_nome" maxlength="50" value="<?php echo $dados->cre_cont2_nome; ?>"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="margin-right: 4px; width: 171px;">
                                        <div style="width: 51px; padding-left: 1px;" class="label">
                                            <label for="input-medium">E-mail:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 90px;" type="text" id="input-medium" name="cont2_email" maxlength="100" value="<?php echo $dados->cre_cont2_email; ?>"/>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 162px;">
                                        <div style="width: 45px; padding-left: 1px;" class="label">
                                            <label for="input-medium">Fone:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 90px;" type="text" id="input-medium" name="cont2_fone" class="telefone" maxlength="15" value="<?php echo $dados->cre_cont2_fone; ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 16px;" class="blocoTitulo">
                                    Dados bancarios
                                </div>
                                <div style="padding-top: 5px;" class="field">
                                    <div class="divleft" style="width: 409px;">
                                        <div style="margin-left: 6px;" class="select">
                                            <select style="width: 394px;" id="banco" name="banco">
                                                <option value="">[SELECIONE UM BANCO]</option>
                                            <?php foreach ($bancos as $ban): ?>
                                                    <option value="<?php echo $ban->banco_cod; ?>" <?php echo $dados->cre_banco == $ban->banco_cod ? 'selected' : ''; ?>><?php echo utf8_decode($ban->banco_nome); ?></option>
                                            <?php endforeach; ?>
                                                    <option value="[OUTRO]">[OUTRO]</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="margin-left: 2px; width:130px;">
                                            <div style="width: 60px; padding-left: 1px;" class="label">
                                                <label for="agencia">Ag�ncia:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 37px;" type="text" id="agencia" name="agencia" maxlength="10" value="<?php echo $dados->cre_agencia; ?>"/>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="margin-left: 2px; width:160px;">
                                            <div style="width: 44px; padding-left: 1px;" class="label">
                                                <label for="conta">Conta:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 89px;" type="text" id="conta" name="conta" maxlength="15" value="<?php echo $dados->cre_conta; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field">
                                        <div class="divleftlast" style="margin-left: 0px; width: 706px;">
                                            <div style="width: 89px;" class="label">
                                                <label for="sacado">Sacado:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 591px;" type="text" id="sacado" name="sacado" maxlength="50" value="<?php echo $dados->cre_sacado; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                                        Cobran�a
                                    </div>
                                    <div class="field">
                                        <div class="divleft" style="width: 240px;">
                                            <div style="width: 89px;" class="label">
                                                <label for="honorario">Honor�rio:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 117px;" type="text" id="honorario" name="honorario" maxlength="8" value="<?php echo $dados->cre_honorario; ?>"/>
                                            </div>
                                        </div>
                                        <div class="divleft" style="width: 238px;">
                                            <div style="width: 77px; padding-left: 1px;" class="label">
                                                <label for="juros">Juros:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 130px;" type="text" id="juros" name="juros" maxlength="8" value="<?php echo strlen($dados->cre_juros)==5?$dados->cre_juros:'0'.$dados->cre_juros; ?>"/>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 216px; margin: 0px;">
                                            <div style="width: 80px; padding-left: 1px;" class="label">
                                                <label for="fax">Praz. Jurid.:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 109px;" type="text" id="fax" name="prazo" maxlength="4" value="<?php echo $dados->cre_prazo; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field">
                                        <div class="divleftlast" style="width: 89px; margin-left: 0px;">
                                            <div style="width: 89px;" class="label">
                                                <label for="repasse">Repasses atribuidos:</label>
                                            </div>
                                        </div>

                                        <div id="chekboxesRepasses" class="label" style="width: 605px; font-size: 10px;">
                                            <?php foreach ($repasses as $rep): ?>
                                                <?php //if(isset($repassescredor[$rep->rep_cod])):?>
                                                    <div style="float:left">
                                                        <input type="checkbox" name="repasse[]" value="<?php echo $rep->rep_cod; ?>" class="checkpasse" <?php echo isset($repassescredor[$rep->rep_cod]) && $repassescredor[$rep->rep_cod] != 0 ? 'checked' : ''; ?> />
                                                        <label style="font-weight: normal; padding-right: 15px;"><?php echo utf8_decode($rep->rep_nome); ?></label>
                                                    </div>
                                                <?php //endif; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                                        Dados para acesso ao sistema
                                    </div>
                                    <div class="field">
                                        <div class="divleft" style="width: 240px;">
                                            <div style="width: 89px;" class="label">
                                                <label for="input-medium">Usu�rio:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 117px; <?php echo $dados->cre_usuario != ''?'background-color: #F6F6F6':''; ?>" type="text" id="input-medium" name="usuario" maxlength="20" value="<?php echo $dados->cre_usuario; ?>" />
                                            </div>
                                        </div>
                                        <div class="divleft" style="width: 218px;">
                                            <div style="width: 77px; padding-left: 1px;" class="label">
                                                <label for="input-medium">Nova senha:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 110px;" type="password" id="input-medium" name="senha" maxlength="40"/>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 236px; margin: 0px;">
                                            <div style="width: 102px; padding-left: 1px;" class="label">
                                                <label for="input-medium">Repita a senha:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 110px;" type="password" id="input-medium" name="senha_confirma" maxlength="40"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="margin-top: 12px; margin-bottom: 3px;" class="blocoTitulo">
                                        Recuperador respons�vel
                                    </div>
                                    <div class="field">
                                        <div style="margin-left: 221px; border: none; width: 245px;">
                                            <div style="margin-left: 6px;" class="select">
                                                <select style="width: 260px;" id="recuperador" name="responsavel">
                                                <?php foreach ($usuarios as $usu): ?>
                                                    <option value="<?php echo $usu->usu_cod; ?>" <?php echo $usu->usu_cod == $dados->usuarios_responsavel_cod?'selected':'';?>><?php echo $usu->usu_usuario_sis; ?></option>
                                                <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="cod" value="<?php echo $dados->cre_cod; ?>" />
                                    <div style="text-align: center; margin-top: 10px;" class="buttons">
                                        <input type="reset" name="cancelar" value="Cancelar" />
                                        <div style="margin-left: 7px;" class="highlight">
                                            <input type="submit" value="Alterar" title="Cadastrar"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                <div id="representantes">
                    <?php echo $mensagem; ?>
                    <form id="formRep" action="<?php echo base_url() . 'credor/cadRep'; ?>" method="post">
                    <div class="form">
                        <div class="fields">
                            <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                                Novo representante legal
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 300px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="nome_rep">Nome:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 177px;" type="text" maxlength="50" name="nome"/>
                                        <input type="hidden" name="cod" value="<?php echo $dados->cre_cod; ?>" />
                                    </div>
                                </div>
                                <div class="divleft" style="width: 201px;">
                                    <div style="width: 46px; padding-left: 1px;" class="label">
                                        <label for="cpf">CPF:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 124px;" type="text" maxlength="14" id="cpf" name="cpf"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 184px;">
                                    <div class="select">
                                        <select style="width: 186px; margin-left: 5px;" name="estado_civil">
                                            <option value="0">[Estado civil]</option>
                                            <option value="1">Solteiro(a)</option>
                                            <option value="2">Casado(a)</option>
                                            <option value="3">Separado(a)</option>
                                            <option value="4">Divorciado(a)</option>
                                            <option value="5">Amaziado(a)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 211px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="rg">RG:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 89px;" type="text" maxlength="20" id="rg" name="rg"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 250px;">
                                    <div style="width: 42px; padding-left: 1px;" class="label">
                                        <label for="endereco_rep">End.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 178px;" type="text" id="endereco_rep" maxlength="130" name="endereco_rep"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 233px; margin: 0px;">
                                    <div style="width: 51px; padding-left: 1px;" class="label">
                                        <label for="bairro_rep">Bairro:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 155px;" type="text" id="bairro_rep" maxlength="30" name="bairro_rep"/>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 211px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="complemento_rep">Compl.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 89px;" type="text" id="complemento_rep" maxlength="25" name="complemento_rep"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 132px;">
                                    <div style="width: 34px; padding-left: 1px;" class="label">
                                        <label for="cep_rep">CEP.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 65px;" type="text" id="cep_rep" maxlength="8" name="cep_rep"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 260px; margin: 0px;">
                                    <div style="width: 59px; padding-left: 1px;" class="label">
                                        <label for="cidade_rep">Cidade:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 170px;" type="text" id="cidade_rep" maxlength="50" name="cidade_rep"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 84px;">
                                    <div class="select">
                                        <select style="width: 80px; margin-left: 7px;" id="estado_rep" name="estado_rep">
                                            <option value="">UF</option>
                                            <?php foreach ($estados as $uf): ?>
                                                <option value="<?php echo $uf->cid_estado; ?>"><?php echo $uf->cid_estado; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 240px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="fone_rep">Fone:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 117px;" type="text" id="fone_rep" class="telefone" name="fone_rep"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 238px;">
                                    <div style="width: 77px; padding-left: 1px;" class="label">
                                        <label for="cel_rep">Celular:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 130px;" type="text" id="cel_rep" class="telefone" name="cel_rep"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 216px; margin: 0px;">
                                    <div style="width: 59px; padding-left: 1px;" class="label">
                                        <label for="email_rep">E-mail:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 130px;" type="text" id="email_rep" maxlength="55" name="email_rep"/>
                                    </div>
                                </div>
                                
                            </div>
                            <div style="text-align: center; margin-top: 10px;" class="buttons">
                                <div style="margin-left: 7px;" class="highlight">
                                    <input type="submit" value="Cadastrar" title="Cadastrar"/>
                                </div>
                            </div>
                        </div>
                        <div class="fields">
                            <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                                Representantes legais cadastrados
                            </div>
                            <?php foreach ($representatnes as $repre): ?>
                            <div class="field" style="padding: 0px;">
                                <div class="divleft" style="width: 300px;">
                                    <div style="width: 40px;" class="label">
                                        <label for="nome_rep">Nome:</label>
                                    </div>
                                    <p class="forte" style="margin-left: 10px; margin-right: 0px; padding-top: 5px; color: #356BA0;float: left; font-size: 11px;"><?php echo utf8_decode($repre->rpl_nome);?></p>
                                </div>
                                <div class="divleft" style="width: 201px;">
                                    <div style="width: 46px; padding-left: 1px;" class="label">
                                        <label for="cpf_rep">CPF:</label>
                                    </div>
                                    <p class="forte" style="margin-left: 10px; margin-right: 0px; padding-top: 5px; color: #356BA0;float: left; font-size: 11px;"><?php echo $repre->rpl_cpf;?></p>
                                </div>
                                <div class="divleftlast" style="width: 184px;">
                                    <p class="forte" style="margin-left: 10px; margin-right: 0px; padding-top: 5px; color: #356BA0;float: left; font-size: 11px;">
                                    <?php
                                        switch ($repre->rpl_estado_civil) {
                                        case "1":
                                        echo "Solteiro(a)";
                                        break;
                                        case "2":
                                        echo "Casado(a)";
                                        break;
                                        case "3":
                                        echo "Separado(a)";
                                        break;
                                        case "4":
                                        echo "Divorciado(a)";
                                        break;
                                        case "5":
                                        echo "Viuvo(a)";
                                        break;
                                        case "6":
                                        echo "Amaziado(a)";
                                        break;
                                        }
                                ?></p>
                                </div>
                            </div>
                            <div class="field" style="padding: 0px;">
                                <div class="divleft" style="width: 155px;">
                                    <div style="width: 25px;" class="label">
                                        <label for="rg_rep">RG:</label>
                                    </div>
                                    <p class="forte" style="margin-left: 10px; margin-right: 0px; padding-top: 5px; color: #356BA0;float: left; font-size: 11px;"><?php echo $repre->rpl_rg;?></p>
                                </div>
                                <div class="divleft" style="width: 285px;">
                                    <div style="width: 42px; padding-left: 1px;" class="label">
                                        <label for="endereco_rep">End.:</label>
                                    </div>
                                    <p class="forte" style="margin-left: 10px; margin-right: 0px; padding-top: 5px; color: #356BA0;float: left; font-size: 11px;"><?php echo utf8_decode($repre->rpl_endereco);?></p>
                                </div>
                                <div class="divleftlast" style="width: 253px; margin: 0px;">
                                    <div style="width: 51px; padding-left: 1px;" class="label">
                                        <label for="bairro_rep">Bairro:</label>
                                    </div>
                                    <p class="forte" style="margin-left: 10px; margin-right: 0px; padding-top: 5px; color: #356BA0;float: left; font-size: 11px;"><?php echo utf8_decode($repre->rpl_bairro);?></p>
                                </div>
                            </div>
                            <div class="field" style="padding: 0px;">
                                <div class="divleft" style="width: 191px;">
                                    <div style="width: 55px;" class="label">
                                        <label for="complemento_rep">Compl.:</label>
                                    </div>
                                    <p class="forte" style="margin-left: 10px; margin-right: 0px; padding-top: 5px; color: #356BA0;float: left; font-size: 11px;"><?php echo $repre->rpl_complemento;?></p>
                                </div>
                                <div class="divleft" style="width: 145px;">
                                    <div style="width: 34px; padding-left: 1px;" class="label">
                                        <label for="cep_rep">CEP.:</label>
                                    </div>
                                    <p class="forte" style="margin-left: 10px; margin-right: 0px; padding-top: 5px; color: #356BA0;float: left; font-size: 11px;"><?php echo $repre->rpl_cep;?></p>
                                </div>
                                <div class="divleftlast" style="width: 290px; margin: 0px;">
                                    <div style="width: 59px; padding-left: 1px;" class="label">
                                        <label for="cidade_rep">Cidade:</label>
                                    </div>
                                    <p class="forte" style="margin-left: 10px; margin-right: 0px; padding-top: 5px; color: #356BA0;float: left; font-size: 11px;"><?php echo $repre->rpl_cidade.' - '.$repre->rpl_uf;?></p>
                                </div>
                                
                            </div>
                            <div class="field" style="padding: 0px;">
                                <div class="divleft" style="width: 230px;">
                                    <div style="width: 65px;" class="label">
                                        <label for="fone_rep">Fone:</label>
                                    </div>
                                    <p class="forte" style=" padding-top: 5px; margin-right: 0px; color: #356BA0;float: left; margin: 0 auto; font-size: 11px;"><?php echo $repre->rpl_fone;?></p>
                                </div>
                                <div class="divleft" style="width: 198px;">
                                    <div style="width: 65px; padding-left: 1px;" class="label">
                                        <label for="cel_rep">Celular:</label>
                                    </div>
                                    <p class="forte" style=" padding-top: 5px; margin-right: 0px; color: #356BA0;float: left; margin: 0 auto; font-size: 11px;"><?php echo $repre->rpl_celular;?></p>
                                </div>
                                <div class="divleftlast" style="width: 266px; margin: 0px;">
                                    <div style="width: 59px; padding-left: 1px;" class="label">
                                        <label for="email_rep">E-mail:</label>
                                    </div>
                                    <p class="forte" style=" padding-top: 5px; margin-right: 0px; color: #356BA0;float: left; margin: 0 auto; font-size: 11px;"></p>
                                </div>
                            </div>
                            <div style="text-align: center; margin-top: 10px;" class="buttons">
                                <div style="margin-left: 7px;" class="highlight">
                                    <button type="button" value="excluir" class="excluir_rep" id="<?php echo $repre->rpl_cod;?>">Excluir</button>
                                    <input type="hidden" name="cod_representante" value="<?php echo $repre->rpl_cod;?>"/>
                                </div>
                            </div>
                            <hr/>
                            <br/>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </form>
            </div>
            <div id="relatorios">
                    <?php echo $mensagem; ?>
                    <form id="formRel" action="<?php echo base_url() . 'credor/cadRel'; ?>" method="post">
					<input type="hidden" name="cod" value="<?php echo $dados->cre_cod; ?>" />
                    <div class="form">
                        <div class="fields">
                            <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                                Acesso a Relat�rios na Vis�o Credor
                            </div>
                            <div class="field" style="padding: 10px;">
                            	<?php
                            	$creRelatorios = explode(',',$dados->cre_relatorios);
                            	?>
                        		<ul>
                        			<?php foreach ($relatorios as $rel): ?>
                        			<li><input <?php echo in_array($rel->crerel_cod,$creRelatorios)?'checked':''; ?> name="cre_relatorio[]" type="checkbox" value="<?php echo $rel->crerel_cod; ?>" /> <?php echo utf8_decode($rel->crerel_nome); ?></li>
                        			<?php endforeach; ?>
                        		</ul>
	                            <div style="text-align: center; margin-top: 10px;" class="buttons">
	                                <div style="margin-left: 7px;" class="highlight">
	                                    <input type="submit" value="Salvar" title="Salvar"/>
	                                </div>
	                            </div>                            	
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>