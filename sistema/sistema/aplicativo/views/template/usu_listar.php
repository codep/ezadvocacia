<script type="text/javascript">
    $(document).ready(function(){

        var interruptor=0;
            
        $('.excluir').click(function(e){
            e.preventDefault()
            interruptor=1;
            link = $(this).attr('id');
            window.location.href=link;
        });

        $('.editausu').click(function(e){
            if(interruptor==0){
                link = $(this).attr('id');
                window.location.href=link;
            }else{
                e.preventDefault();
                interruptor=0;
            }
        });
    });
</script>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Lista de usu�rios cadastrados</h5>
                <div class="form" style="clear: none; float: right; padding-top:5px; height: 27px;">
                    <div class="fields">
                        <div class="field" style="border: none; margin: 0px; padding: 0px;">
                            <div class="highlight">
                                <a href="<?php echo base_url() . 'usuarios/novo' ?>">
                                    <input style="width: 155px; font-size: 13px; font-weight: bold;" type="submit" name="cadastrar" value="Novo usu�rio" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $mensagem; ?>
            <form id="form" action="" method="post">
                <div class="form">
                    <div class="fields">
                        <div class="table">
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th class="left">Nome</th>
                                        <th>Fun��es</th>
                                        <th>Grupo</th>
                                        <th class="last">Excluir</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($usuarios as $usuario): ?>
                                        <tr id="<?php echo base_url() . 'usuarios/editar/cod:' . $usuario->usu_cod; ?>" class="editausu">
                                            <td class="title"><?php echo utf8_decode($usuario->usu_nome); ?></td>
                                            <td><?php echo utf8_decode($usuario->usu_funcoes); ?></td>
                                            <td><?php echo utf8_decode($usuario->gru_titulo); ?></td>
                                            <td style="text-align: center; vertical-align: middle;" id="<?php echo base_url() . 'usuarios/excluir/cod:' . $usuario->usu_cod; ?>" class="last excluir">
                                                <a href="#">
                                                    <img src="<?php echo $img . 'excluir.png' ?>" alt="+"/>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
