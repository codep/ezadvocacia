<script type="text/javascript">
        $(document).ready(function(){
            $('.cadastrar').click(function(e){
                    e.preventDefault()
                    var senha;
                    var senha_rep;
                    senha=$('#senha').attr('value');
                    senha_rep=$('#senha_rep').attr('value');
                    if((senha==senha_rep)){
                            $('#formCad').submit();
                    }else{
                        alert('As senhas digitadas est�o diferentes, por favor confira');
                    }

            });
        });
</script>
<div id="content">
<?php echo $sidebar; ?>

    <div id="right">
        <div class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Minha conta</h5>
            </div>
            <form id="formCad" action="<?php echo base_url().'usuarios/mudaSenha' ?>" method="post">
                <div class="form">
                    <div class="fields">
                        <div class="field  field-first">
                            <div class="divleft" style="width: 116px;">
                                <div class="label">
                                    <label>C�digo:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dadosConta->usu_cod ?></p>
                            </div>
                            <div class="divleft" style=" margin-left: 4px; width: 173px;">
                                <div class="label">
                                    <label>Sexo:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dadosConta->usu_sexo=='f'?'Feminino':'Masculino' ?></p>
                            </div>
                            <div class="divleftlast" style="width: 210px;">
                                <div class="label">
                                    <label>Tipo de Usu�rio:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dadosConta->gru_titulo ?></p>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleftlast" style="width: 371px;">
                                <div style="width: 45px;" class="label">
                                    <label for="nome">Nome:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dadosConta->usu_nome ?></p>
                            </div>   
                        </div>
                       <div class="field">
                            <div class="divleft" style="width: 300px;">
                                <div style="width: 25px;" class="label">
                                    <label for="rg">RG:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dadosConta->usu_rg ?></p>
                            </div>
                            <div class="divleftlast" style="width: 231px;">
                                <div style="width: 30px; padding-left: 1px;" class="label">
                                    <label for="cpf">CPF:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dadosConta->usu_cpf ?></p>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 350px;">
                                <div style="width: 40px; padding-left: 1px;" class="label">
                                    <label for="endereco">End.:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dadosConta->usu_endereco ?></p>
                            </div>
                            <div class="divleftlast" style="width: 233px; margin: 0px;">
                                <div style="width: 51px; padding-left: 1px;" class="label">
                                    <label for="bairro">Bairro:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dadosConta->usu_bairro ?></p>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 210px;">
                                <div style="width: 50px;" class="label">
                                    <label for="complemento">Compl.:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dadosConta->usu_complemento ?></p>
                            </div>
                            <div class="divleft" style="width: 132px;">
                                <div style="width: 34px; padding-left: 1px;" class="label">
                                    <label for="cep">CEP.:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dadosConta->usu_cep ?></p>
                            </div>
                            <div class="divleft" style="width: 250px; margin: 0px;">
                                <div style="width: 50px; padding-left: 1px;" class="label">
                                    <label for="cidade">Cidade:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dadosConta->usu_cidade?></p>
                            </div>
                            <div class="divleftlast" style="width: 90px;">
                                <div style="width: 50px; padding-left: 1px;" class="label">
                                    <label for="cep">Estado:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dadosConta->usu_uf ?></p>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 170px;">
                                <div style="width: 40px;" class="label">
                                    <label for="telefone">Fone:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dadosConta->usu_fone ?></p>
                            </div>
                            <div class="divleft" style="width: 180px;">
                                <div style="width: 50px; padding-left: 1px;" class="label">
                                    <label for="celular">Celular:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dadosConta->usu_celular ?></p>
                            </div>
                            <div class="divleftlast" style="width: 312px; margin: 0px;">
                                <div style="width: 50px; padding-left: 1px;" class="label">
                                    <label for="email">E-mail:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dadosConta->usu_email ?></p>
                            </div>
                        </div>
                        <div style="height: 55px;" class="field">
                            <div class="divleftlast" style="width: 701px;">
                                <div style="width: 84px;" class="label">
                                    <label for="funcoes">Fun��es:</label>
                                </div>
                                <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $dadosConta->usu_funcoes ?></p>
                            </div>
                        </div>
                        <div class="blocoTitulo" style="margin-top: 15px;">
                            Dados de acesso ao sistema
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 212px;">
                                <div style="width: 69px;" class="label">
                                    <label for="usuario">Usu�rio:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 107px; background-color: #F6F6F6;" type="text" id="usuario" name="usuario" readonly value="<?php echo $dadosConta->usu_usuario_sis ?>"/>
                                    <input type="hidden" name="cod" value="<?php echo $dadosConta->usu_cod ?>"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 235px;">
                                <div style="width: 90px; padding-left: 1px;" class="label">
                                    <label for="senha">Nova senha:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 114px;" type="password" id="senha" name="senha"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 247px; margin: 0px;">
                                <div style="width: 106px; padding-left: 1px;" class="label">
                                    <label for="senha_rep">Repita a senha:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 114px;" type="password" id="senha_rep" name="senha_rep"/>
                                </div>
                            </div>
                        </div>
                        <div style="text-align: center; margin-top: 18px;" class="buttons">
                            <a style="text-decoration: none;" href="<?php echo base_url().'home'?>">
                                <input type="reset" name="cancelar" id="voltar" value="Cancelar" />
                            </a>
                            <div style="margin-left: 7px;" class="highlight">
                                <input class="cadastrar" type="submit" name="cadastrar" value="Salvar" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
