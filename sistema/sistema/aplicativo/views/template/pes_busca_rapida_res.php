
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Pesquisar cobranšas</h5>
            </div>
            <div class="blocoTitulo">
                Resultados da pesquisa por<br/>
                <span style="font-size: 10px"><?php echo $pesquisaPor == 'fone' ? 'Telefone: ' : 'Parente: '; ?><?php echo $valorDigitado; ?></span>
            </div>
            <div class="table">
                <table id="products">
                    <thead>
                        <tr>
                            <th class="left">Cod.</th>
                            <th>Nome</th>
                            <th>Enderešo</th>
                            <th>Cidade</th>
                            <th class="last">Ficha</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($inadimplentes as $inadimplente):?>
                        <tr>
                            <td><?php echo $inadimplente->ina_cod;?></td>
                            <td><?php echo $inadimplente->ina_nome;?></td>
                            <td><?php echo $inadimplente->ina_endereco;?></td>
                            <td><?php echo $inadimplente->ina_cidade;?></td>
                            <td class="last"><a href="<?php echo base_url().'inadimplente/editar/cod:'.$inadimplente->ina_cod ?>">Visualizar</a>  </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
