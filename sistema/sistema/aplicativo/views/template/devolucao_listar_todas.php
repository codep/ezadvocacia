<script type="text/javascript">
    $(document).ready(function(){
        $('.devolver').click(function(){
        	
        	var cob_cod = $(this).parents('tr:first').find('.cob_cod').html();
        	var ina_nome = $(this).parents('tr:first').find('.ina_nome').html();
        	var div_total = $(this).parents('tr:first').find('.div_total').html();
        	
            if(confirm('Tem certeza que deseja devolver esta d�vida?\n\nC�digo: '+cob_cod+'\nInadimplente: '+ina_nome+'\nTotal: '+div_total)){
                
            }else{
                return false;
            }
        });
    });
</script>
<div id="content">

    <?php echo $sidebar; ?>
    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Devolu��o</h5>
            </div><?php echo $mensagem ?>
            <div id="virgem">
                <div class="form">
                    <div class="fields">
                        <div>
                            
                                <div class="field  field-first">
                                    <form id="formFiltro" method="post"  action="<?php base_url().'devolucao/listar/';?>">
                                    <div class="divleftlast" style="width: 45px; margin-left: 0px;">
                                        <div style="width: 69px; padding-left: 1px;" class="label">
                                            <label for="nome">Inadimplente:</label>
                                        </div>
                                    </div>
                                    <div class="divleft" style="width: 215px; margin-left: 50px;">
                                        <div class="input" id="campoBusca1">
                                            <input style="width: 188px;" type="text" id="codigoFiltro" name="codigoFiltro"/>
                                        </div>
                                    </div>

                                    <div class="divleftlast" style="width: 87px; margin: 0px;">
                                        <div class="buttons">
                                            <div class="highlight">
                                                <input style="width: 85px;" type="submit" name="filtrarFiltro" value="Filtrar" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                    <div class="paginacaoContainer" style='float: right; margin-top: 9px;'> <?php echo $paginacao; ?> </div>
                                </div>
                            
                            
                        </div>
                        
                        <?php if (sizeof($dividasEAcordos['dividas']) > 0 ): ?><!-- if dividas ou acordos -->
                        <?php //if (sizeof($dividasEAcordos['dividas']) > 0 || sizeof($dividasEAcordos['acordos']) > 0): ?><!-- if dividas ou acordos -->
                            <div class="table" style="padding: 0px 5px 10px; border-bottom: 1px solid #ddd;">
                                <table id="products">
                                    <thead>
                                        <tr>
                                            <th class="left" style="width: 30px;">Cod.</th>
                                            <th>Inadimplente</th>
                                            <th>Credor</th>
                                            <th>Emiss�o</th>
                                            <th>Cadastro</th>
                                            <th>Valor(R$)</th>
                                            <th class="last">Devolver</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (sizeof($dividasEAcordos['dividas']) > 0): ?> <!-- ##### if dividas ##### -->
                                            <?php foreach ($dividasEAcordos['dividas'] as $divida): ?>
                                                <tr>
                                                    <td class="title cob_cod" style="width: 30px;"><?php echo $divida->cob_cod; ?></td>
                                                    <td class="ina_nome"><?php echo utf8_decode($divida->ina_nome); ?></td>
                                                    <td><?php echo utf8_decode($divida->cre_nome_fantasia); ?></td>
                                                    <td id="tdEmissao"><?php echo convDataBanco($divida->div_emissao); ?></td>
                                                    <td><?php echo convDataBanco($divida->div_cadastro); ?></td>
                                                    <td class="div_total"><?php echo $divida->div_total; ?></td>
                                                    <td class="last">
                                                        <a class="devolver" href="<?php echo 'devolver/codCobranca:' . $divida->cob_cod.'/inad:'.utf8_decode($divida->ina_nome); ?>">
                                                            <img src="<?php echo $img . 'devolver.png' ?>" alt="Devolver d�vida"/>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                            <tr>
                                                <td class="last" colspan="7">
                                                    <div class="field  field-first" style="padding-bottom: 1px;">
                                                        <div class="divleftlast" style="width: 395px;">
                                                            <div style="padding-left: 0px;" class="label">
                                                                <label for="nome">Total de d�vidas encontradas: <i><?php echo $totalInadimplentes; ?></i></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>                                                
                                            </tr>
                                        <?php endif; ?><!-- ##### endif dividas ##### -->
                                    </tbody>
                                </table>
                            </div>
                        <?php else: ?><!-- Se nao encontrar nenhuma divida e nenhum acordo -->
                            <div class="blocoTitulo" style="margin-top: 11px; width: 682px;">NADA ENCONTRADO</div>
                        <?php endif; ?><!-- endif dividas ou acordos -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>