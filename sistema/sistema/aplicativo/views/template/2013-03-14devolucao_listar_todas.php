<script type="text/javascript">
    $(document).ready(function(){
        $('.devolver').click(function(){
            if(confirm('Tem certeza que deseja devovler esta d�vida?')){
                
            }else{
                return false;
            }
        });
    });
</script>
<div id="content">

    <?php echo $sidebar; ?>
    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Devolu��o</h5>
            </div><?php echo $mensagem ?>
            <div id="virgem">
                <div class="form">
                    <div class="fields">
<!--                        <form id="formFiltro" method="post"  action="listar">
                            <div class="field  field-first">

                                <div class="divleftlast" style="width: 45px; margin-left: 0px;">
                                    <div style="width: 69px; padding-left: 1px;" class="label">
                                        <label for="nome">Filtro:</label>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 215px;">
                                    <div style="margin-left: 6px;" class="select">
                                        <select style="width: 204px;" id="filtro01" name="filtro1">
                                            <option value="" style="font-weight:bold"< ?php echo $aux == '' ? 'selected' : ''; ?>>[TODAS D�VIDAS]</option>
                                            <option value="c.cob_cod" < ?php echo $aux == 'div_cod' ? 'selected' : ''; ?>>C�digo</option>
                                            <option value="ina_nome" < ?php echo $aux == 'ina_nome' ? 'selected' : ''; ?>>Inadimplente</option>
                                            <option value="div_emissao" < ?php echo $aux == 'div_emissao' ? 'selected' : ''; ?>>Emiss�o</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 213px;">
                                    <div class="input" id="campoBusca1">
                                        <input style="width: 188px;" type="text" id="nome1" name="nome"/>
                                    </div>
                                    <div class="input" id="campoBusca2">
                                        <input style="width: 188px;" type="text" id="nome2" name="nome" class="dataMascara"/>
                                    </div>
                                </div>

                                <div class="divleftlast" style="width: 87px; margin: 0px;">
                                    <div class="buttons">
                                        <div class="highlight">
                                            <input style="width: 85px;" type="submit" name="filtrar" value="Filtrar" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>-->
                        <?php if (sizeof($dividasEAcordos['dividas']) > 0 || sizeof($dividasEAcordos['acordos']) > 0): ?><!-- if dividas ou acordos -->
                            <div class="table" style="padding: 0px 5px 10px; border-bottom: 1px solid #ddd;">
                                <table id="products">
                                    <thead>
                                        <tr>
                                            <th class="left" style="width: 30px;">Cod.</th>
                                            <th>Inadimplente</th>
                                            <th>Credor</th>
                                            <th>Emiss�o</th>
                                            <th>Cadastro</th>
                                            <th>Valor(R$)</th>
                                            <th class="last">Devolver</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (sizeof($dividasEAcordos['dividas']) > 0): ?> <!-- ##### if dividas ##### -->
                                            <?php foreach ($dividasEAcordos['dividas'] as $divida): ?>
                                                <tr>
                                                    <td class="title" style="width: 30px;"><?php echo $divida->cob_cod; ?></td>
                                                    <td><?php echo utf8_decode($divida->ina_nome); ?></td>
                                                    <td><?php echo utf8_decode($divida->cre_nome_fantasia); ?></td>
                                                    <td id="tdEmissao"><?php echo convDataBanco($divida->div_emissao); ?></td>
                                                    <td><?php echo convDataBanco($divida->div_cadastro); ?></td>
                                                    <td><?php echo $divida->div_total; ?></td>
                                                    <td class="last">
                                                        <a class="devolver" href="<?php echo 'devolver/codCobranca:' . $divida->cob_cod.'/inad:'.utf8_decode($divida->ina_nome); ?>">
                                                            <img src="<?php echo $img . 'devolver.png' ?>" alt="Devolver d�vida"/>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                            <tr>
                                                <td class="last" colspan="7">
                                                    <div class="field  field-first" style="padding-bottom: 1px;">
                                                        <div class="divleftlast" style="width: 395px;">
                                                            <div style="padding-left: 0px;" class="label">
                                                                <label for="nome">Total de d�vidas encontradas: <i><?php echo sizeof($dividasEAcordos['dividas']); ?></i></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>                                                
                                            </tr>
                                        <?php endif; ?><!-- ##### endif dividas ##### -->
                                    </tbody>
                                </table>
                            </div>
                        <?php else: ?><!-- Se nao encontrar nenhuma divida e nenhum acordo -->
                            <div class="blocoTitulo" style="margin-top: 11px; width: 682px;">NADA ENCONTRADO</div>
                        <?php endif; ?><!-- endif dividas ou acordos -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>