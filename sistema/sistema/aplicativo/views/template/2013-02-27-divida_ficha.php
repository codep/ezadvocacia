<script type="text/javascript">
    $(document).ready(function(){

        $('.ACHeader ').hover(function(){
            $(this).css('background-color', '#CCC');
        },
        function(){
            $(this).css('background-color', '#DDD');

        });

        $('.accordion').accordion({
            autoheight:false
        });
        
        $('input[name="valorRecebido"]').priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
            
        });
        
        $('input[name="desconto"]').priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });
        
        v1 = '';
        base_url = "<?php echo base_url(); ?>";
        $('.fechar').click(function(e){
            e.preventDefault();
            $('#dialog-form').dialog('close');
        });
        
        $('.fechar2').click(function(e){
            e.preventDefault();
            $('#dialog-form2').dialog('close');
        });
        
        $('.dialog-message-open').click(function(){
            $('#cobCod').attr('value', $(this).attr('id'));
        });
        
        $(".dialog-form-open2").click(function () {
            $("#dialog-form2").dialog("open");
            return false;
        });
        
        $(".ACHeader").click(function () {
            $('#cobCodJud').attr('value', $(this).attr('id'));
        });
        
        credorClicado = '';
        
        $(".ACHeaderAcordo").click(function () {
            $('#cobCodAco').attr('value', $(this).attr('id'));
            $('#cobCodRec').attr('value', $(this).attr('id'));
            $('#cobCodJud2').attr('value', $(this).attr('id'));
            credorClicado = $(this).parent().attr('id');
        });
        
        $('.telefone').mask('(99)9999-9999');
        
        //------------------------------------------------------------------------------
        //---------------------------Fun��o para validar CNPJ---------------------------
        //------------------------------------------------------------------------------
        function validaCNPJ() {
            CNPJ = $('input[name="cpf_cnpj"]').val();
            erro = new String;
            if (CNPJ.length < 18) erro += "� necessario preencher corretamente o n�mero do CNPJ! \n\n";
            if ((CNPJ.charAt(2) != ".") || (CNPJ.charAt(6) != ".") || (CNPJ.charAt(10) != "/") || (CNPJ.charAt(15) != "-")){
                if (erro.length == 0) erro += "� necess�rio preencher corretamente o n�mero do CNPJ! \n\n";
            }
            //substituir os caracteres que n�o s�o n�meros
            if(document.layers && parseInt(navigator.appVersion) == 4){
                x = CNPJ.substring(0,2);
                x += CNPJ. substring (3,6);
                x += CNPJ. substring (7,10);
                x += CNPJ. substring (11,15);
                x += CNPJ. substring (16,18);
                CNPJ = x;
            } else {
                CNPJ = CNPJ. replace (".","");
                CNPJ = CNPJ. replace (".","");
                CNPJ = CNPJ. replace ("-","");
                CNPJ = CNPJ. replace ("/","");
            }
            var nonNumbers = /\D/;
            if (nonNumbers.test(CNPJ)) erro += "A verifica��o de CNPJ suporta apenas n�meros! \n\n";
            var a = [];
            var b = new Number;
            var c = [6,5,4,3,2,9,8,7,6,5,4,3,2];
            for (i=0; i<12; i++){
                a[i] = CNPJ.charAt(i);
                b += a[i] * c[i+1];
            }
            if ((x = b % 11) < 2) { a[12] = 0 } else { a[12] = 11-x }
            b = 0;
            for (y=0; y<13; y++) {
                b += (a[y] * c[y]);
            }
            if ((x = b % 11) < 2) { a[13] = 0; } else { a[13] = 11-x; }
            if ((CNPJ.charAt(12) != a[12]) || (CNPJ.charAt(13) != a[13])){
                erro +="CNPJ INV�LIDO, Por favor verifique!";
            }
            if (erro.length > 0){
                alert(erro);
                return false;
            }
            return true;
        }
        //------------------------------------------------------------------------------
        //---------------------------Fun��o para validar CPF----------------------------
        //------------------------------------------------------------------------------
        function validaCPF() {
            cpf = $('input[name="cpf_cnpj"]').val();
            
            cpf = cpf.replace(".", "");
            cpf = cpf.replace(".", "");
            cpf = cpf.replace("-", "");
            
            erro = new String;
            if (cpf.length < 11) erro += "Sao necessarios 11 digitos para verificacao do CPF! \n\n";
            var nonNumbers = /\D/;
            if (nonNumbers.test(cpf)) erro += "A verificacao de CPF suporta apenas numeros! \n\n";
            if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999"){
                erro += "Numero de CPF invalido!"
            }
            var a = [];
            var b = new Number;
            var c = 11;
            for (i=0; i<11; i++){
                a[i] = cpf.charAt(i);
                if (i < 9) b += (a[i] * --c);
            }
            if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
            b = 0;
            c = 11;
            for (y=0; y<10; y++) b += (a[y] * c--);
            if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
            if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10])){
                erro +="Numero de CPF invalido!, Por Favor confira";
            }
            if (erro.length > 0){
                alert(erro);
                return false;
            }
            return true;
        }
        //------------------------------------------------------------------------------
        //--------------------------- Fim das Valida��es -------------------------------
        //------------------------------------------------------------------------------
        
        //SE O RADIO PESSOA JUR�DICA VIER MARCADO
        if($('input[name="pessoa"][value="j"]').is(':checked'))
        {
            //MUDA O LABEL DO PARA NOME FANTASIA
            $('label[for="nome"]').text('Nome Fantasia:');
            //DESMARCAR O SEXO
            $("input[name='sexo']").each(function() {
                //desmarcar
                this.checked = false;
            });
            //ATRIBUI A LETRA J A VARIAVEL PESSOA QUE SERA USADA NA VERIFICA��O DO CPF/CNPJ
            pessoa='j';
            //DESMARCAR ESTADO CIVIL AINDA N�O FUNCIONA
            $('#est_civil option[value="0"]').attr('selected', true);
            //RESETAR CPF/CNPJ
            $('#cpf_cnpj').html('<input style="width: 124px;" type="text" name="cpf_cnpj"/>');
            //HABILITAR CAMPO E APLICAR M�SCARA PARA CPF
            $('input[name="cpf_cnpj"]').attr('disabled',false);
            $('input[name="cpf_cnpj"]').mask('99.999.999/9999-99');
            $('input[name="cpf_cnpj"]').attr('value', '<?php echo $inadDados->ina_pessoa == "j" ? "$inadDados->ina_cpf_cnpj" : ""; ?>');
        }
        //SE O RADIO PESSOA F�SICA VIER MARCADO
        if($('input[name="pessoa"][value="f"]').is(':checked'))
        {
            //ATRIBUI A LETRA F A VARIAVEL PESSOA QUE SERA USADA NA VERIFICA��O DO CPF/CNPJ
            pessoa='f';
            //ALTERA O LABEL DE NOME FANTASIA PARA NOME
            $('label[for="nome"]').text('Nome:');
            $('#sexo_est_civil').css('padding-top','0px');
            $('input[name="cpf_cnpj"]').mask('999.999.999-99');
        }
        //AO CLICAR EM PESSOA JURIDICA
        $('input[name="pessoa"][value="j"]').click(function() {
            //ATRIBUI A LETRA J A VARIAVEL PESSOA QUE SERA USADA NA VERIFICA��O DO CPF/CNPJ
            pessoa='j';
            $('label[for="nome"]').text('Nome Fantasia:');
            //DESMARCAR O SEXO
            $("input[name='sexo']").each(function() {
                //desmarcar
                this.checked = false;
            });
            //DESMARCAR ESTADO CIVIL AINDA N�O FUNCIONA
            $('#est_civil option[value="0"]').attr('selected', true);
            //RESETAR CPF/CNPJ
            $('#cpf_cnpj').html('<input style="width: 124px;" type="text" name="cpf_cnpj"/>');
            //HABILITAR CAMPO E APLICAR M�SCARA PARA CPF
            $('input[name="cpf_cnpj"]').attr('disabled',false).mask('99.999.999/9999-99');
            $('input[name="cpf_cnpj"]').attr('value', '<?php echo $inadDados->ina_pessoa == "j" ? "$inadDados->ina_cpf_cnpj" : ""; ?>');
        });
        //AO CLICAR EM PESSOA F�SICA
        $('input[name="pessoa"][value="f"]').click(function() {
            //ATRIBUI A LETRA F A VARIAVEL PESSOA QUE SERA USADA NA VERIFICA��O DO CPF/CNPJ
            pessoa='f';
            //SETA NULO NO INPUT RAZ�O SOCIAL
            $('input[name="razao_social"]').val('<?php echo $inadDados->ina_pessoa == "j" ? "$inadDados->ina_nome" : ""; ?>');
            //ALTERA O LABEL DE NOME FANTASIA PARA NOME
            $('label[for="nome"]').text('Nome:');
            //RESETAR CPF/CNPJ
            $('#cpf_cnpj').html('<input style="width: 124px;" type="text" name="cpf_cnpj" />');
            //APLICAR M�SCARA PARA CNPJ
            $('input[name="cpf_cnpj"]').attr('disabled',false).mask('999.999.999-99');
            $('input[name="cpf_cnpj"]').attr('value', '<?php echo $inadDados->ina_pessoa == "f" ? "$inadDados->ina_cpf_cnpj" : ""; ?>');
        });
        //VALIDA��O DOS CAMPOS OBRIGAT�RIOS DO FURMULARIO DE INADIMPLENTE
        $('#formUP').submit(function(){
            //SE O USU�RIO DIGITOU CPF OU CNPJ A VALIDA��O DEVE SER FEITA
            if(($('input[name="cpf_cnpj"]').val())!='')
            {
                //SE A VARIAVEL PESSOA FOR F(F�SICA) ENT�O VALIDA O CPF SEN�O VALIDA O CNPJ
                if(pessoa=='f'){
                    //ATRIBUI O RETURN DA FUN��O SE FOR FALSO N�O DEIXA ENVIAR O FORM
                    ret = validaCPF();
                    if(ret==false){
                        return false;
                    }
                }
                else if(pessoa=='j')
                {
                    ret = validaCNPJ();
                    if(ret==false){
                        return false;
                    }
                }
            }
            //TESTA PARA VER SE OS CAMPOS OBRIGAT�RIOS EST�O PREENCHIDOS
            var pessoaVazio = true;
            $('input[name="pessoa"]').each(function() {
                if ( $(this).is(':checked') ) {
                    pessoaVazio = false;
                }
            });
            if($('input[name="nome_fantasia"]').val() == '')
            {
                $('input[name="nome_fantasia"]').focus();
                return false;
            }
            else if(pessoaVazio)
            {
                alert('Indique pessoa F�SICA ou JUR�DICA.');
                return false;
            }
            else if($('#cidade option:selected').val() == '' || $('#uf option:selected').val() == '')
            {
                alert('Selecione uma CIDADE e um ESTADO');
                return false;
            }
            else
            {
                var marcado = false;
            }
        });
        //VALDA��O DOS CAMPOS OBRIGATORIOS DO FORMULARIO DE PARENTES
        $('#formPAR').submit(function(){
            if($('input[name="parentesco_01_nome"]').val() == '')
            {
                $('input[name="parentesco_01_nome"]').focus();
                return false;
            }
            $('#par_nome').attr('disabled',false);
            $('#par_fone').attr('disabled',false);
        });
        
        //VALDA��O DOS CAMPOS OBRIGATORIOS DO FORMULARIO DE BENS
        $('#formBEM').submit(function(){
            if($('input[name="titulo"]').val() == '')
            {
                $('input[name="titulo"]').focus();
                return false;
            }
        });
        
        //LISTANDO AS CIDADES
        $('select[id="ina_uf"]').change(function(){
            popularCidade('ina_');
        });
        $('select[id="con_uf"]').change(function(){
            //            alert('Entrou no: PAI');
            popularCidade('con_');
        });
        $('select[id="pai_uf"]').change(function(){
            popularCidade('pai_');
        });
        $('select[id="mae_uf"]').change(function(){
            popularCidade('mae_');
        });
        //------------------------------------------------------------------------------
        //---------------------------- Op��es da d�vida --------------------------------
        //------------------------------------------------------------------------------
        $('.formDividas').submit(function(e){
            e.preventDefault();
            aux = $(this).find(':eq(2)').val();
            if(aux=='0'){
                alert('Voc� precisa escolher uma op��o!');
                return false;
            }else if(aux=='novoRO'){
                cods = $(this).attr('id');
                //                alert('Novo RO: '+cods);
                window.location.href=base_url+'ro/novo/cod:'+cods;
                return false;
            }else if(aux=='acordo'){

                $(this).attr('action', base_url+'acordo/novo');

                var cob_cod = $('.dividas_table.selected > a.selected').attr('id');
                var parcelas = '';
                $('.dividas_table.selected > div > table > tbody > tr > td > input.checkbox_dividas').each(function(){
                    if($(this).attr('checked'))
                    {
                        parcelas += $(this).val();
                        parcelas += '-';
                    }
                });
                
                if(parcelas == ''){
                    alert('Selecione as parcelas para realizar o acordo!');
                    return false;
                }
                
                $('#acordo_tipo').val("divida");
                $('#dados_divida').val(cob_cod+'|'+parcelas);
                $('#formDadosAcordo').submit();
                //                alert('Cobran�a: '+cob_cod+' | Parcelas: '+parcelas);

            }else if(aux=='judicial'){
                $("#dialog-form2").dialog("open");
                return false;
            }else{
                return false;
            }
            return false;
        });
        
        cobranca = '';
        
        $('#formReceber').submit(function(e){
            $('#submitReceberBtn').hide();
            $('#dadosHidden').val(dados_hidden);
            $('#urlVoltar').val(window.location.href);
        });

        $('.formJudicial').submit(function(e){
            e.preventDefault();
            
            aux = $(this).find(':eq(2)').val();
            
            $('#juridico > .form > .fields > .accordion > .table > a.selected').each(function(){
                if($(this).hasClass('selected')) cobranca = $(this).attr('id');
            });
            
            if(aux=='0'){
                alert('Voc� precisa escolher uma op��o!');
                return false;
            }else if(aux=='novoRO'){
                cods = $(this).attr('id');
                //                alert('Novo RO: '+cods);
                window.location.href=base_url+'ro/novo/cod:'+cods;
                return false;
            }else if(aux=='reacordo'){
                
                if(credorClicado != $(this).parent().attr('id'))
                {
                    alert('Favor selecionar um acordo para este credor');
                    return false;
                }
                
                $(this).attr('action', base_url+'acordo/reacordo');

                var cob_cod = $('.judicial_table.selected > a').attr('id');
                var parcelas = '';
                $('.judicial_table.selected > div > table > tbody > tr > td > input.checkbox_dividas').each(function(){
                    if($(this).attr('checked'))
                    {
                        parcelas += $(this).val();
                        parcelas += '-';
                    }
                });
                
                if(parcelas == ''){
                    alert('Selecione as parcelas para realizar o acordo!');
                    return false;
                }
                
                $('#acordo_tipoRe').val("divida");
                $('#dados_dividaRe').val(cob_cod+'|'+parcelas);
                $('#setor_reacordo').val('JUD');
                
                
                alert(''+cob_cod+'|'+parcelas);
                
                $('#formDadosReAcordo').submit();
                
            }else if(aux=='judicial'){
                alert('Judicial');
            }else if(aux=='extincao'){
                if(credorClicado != $(this).parent().attr('id'))
                {
                    alert('Favor selecionar um acordo para este credor');
                    return false;
                }
                $("#dialog-form3").dialog("open");
                return false;
            }else if(aux=='receber'){
                if(credorClicado != $(this).parent().attr('id'))
                {
                    alert('Favor selecionar um acordo para este credor');
                    return false;
                }
                //ZERANDO OS VALORES DO FORM
       
                $('#dados_ch').val('');
                $('.tipoValor').each(function(){
                    $(this).removeAttr('checked');
                });
                $('#valorRecebido').val('');
                $('#desconto').val('');
                $('#btsReceber').hide();
                $('#camposReceber').hide();
                $('#listarParcelas').html('');
                $("#dialog-form").dialog("open");
                return false;
            }else if(aux=='retornoADM'){
                $('#cobCod').attr('value', $(this).attr('id'));
                $('.dialog-message').dialog("open");
                return false;
            }
            else{
                alert("HADOUKEN!!!>>>> "+aux); 
                return false;
            }
            
            
            return false;
        });
        
        $('.formAcordos').submit(function(e){
            e.preventDefault();
            
            aux = $(this).find(':eq(2)').val();
            
            $('#acordos > .form > .fields > .accordion > .table > a.selected').each(function(){
                if($(this).hasClass('selected')) cobranca = $(this).attr('id');
            });

            if(aux=='0'){
                alert('Voc� precisa escolher uma op��o!');
                return false;
            }else if(aux=='novoRO'){
                cods = $(this).attr('id');
                //                alert('Novo RO: '+cods);
                window.location.href=base_url+'ro/novo/cod:'+cods;
                return false;
            }else if(aux=='reacordo'){
                if(credorClicado != $(this).parent().attr('id'))
                {
                    alert('Favor selecionar um acordo para este credor');
                    return false;
                }
                $(this).attr('action', base_url+'acordo/reacordo');

                var cob_cod = $('.acordos_table.selected > a.selected').attr('id');
                var parcelas = '';
                $('.acordos_table.selected > div > table > tbody > tr > td > input.checkbox_dividas').each(function(){
                    if($(this).attr('checked'))
                    {
                        parcelas += $(this).val();
                        parcelas += '-';
                    }
                });
                
                if(parcelas == ''){
                    alert('Selecione as parcelas para realizar o acordo!');
                    return false;
                }
                
                $('#acordo_tipoRe').val("divida");
                $('#dados_dividaRe').val(cob_cod+'|'+parcelas);
                $('#setor_reacordo').val('ADM');
                
//                                alert(''+cob_cod+'|'+parcelas);
                
                $('#formDadosReAcordo').submit();
                
            }else if(aux=='judicial'){
                if(credorClicado != $(this).parent().attr('id'))
                {
                    alert('Favor selecionar um acordo para este credor');
                    return false;
                }
                $("#dialog-form4").dialog("open");
                return false;
            }else if(aux=='extincao'){
                if(credorClicado != $(this).parent().attr('id'))
                {
                    alert('Favor selecionar um acordo para este credor');
                    return false;
                }
                $("#dialog-form3").dialog("open");
                return false;
            }else if(aux=='receber'){
                //ZERANDO OS VALORES DO FORM
                if(credorClicado != $(this).parent().attr('id'))
                {
                    alert('Favor selecionar um acordo para este credor');
                    return false;
                }
            
                $('#dados_ch').val('');
                $('.tipoValor').each(function(){
                    $(this).removeAttr('checked');
                });
                $('#valorRecebido').val('');
                $('#desconto').val('');
                $('#btsReceber').hide();
                $('#camposReceber').hide();
                $('#listarParcelas').html('');
                $("#dialog-form").dialog("open");
                return false;
            }else{
                //                alert("HADOUKEN!!!>>>> "+aux); 
                return false;
            }
            
            
            return false;
        });
        dados_hidden='';
        
        tipoValor = '';
        //CAIXA DE DIALOGO RECEBER
        $('#camposReceber').hide();
        $('#btsReceber').hide();
        $('#listarParcelas').html('');
        
        $('#dados_cheque').hide();
        
        $('#tipo_doc').change(function(){
            if($(this).val() == '2'){
                $('#dados_cheque').fadeIn();
            }
            else{
                $('#dados_cheque').fadeOut();
            }
        });
        
        $('.tipoValor').click(function(){
            $('#camposReceber').fadeIn();
            if($(this).val() == '0'){
                tipoValor = '_semJuros';
            }
            else {
                tipoValor = '_comJuros';
            }
            //VALIDA��O - TROCA DE TIPO
            $('#btsReceber').fadeOut();
            $('#listarParcelas').fadeOut(function(){
                $(this).html('')
            });
            //            alert(tipoValor);
        });
        
        //VALIDA�AO - CASO SEJA DADO FOCO EM UM DOS CAMPOS DADOS DO CHEQUE, VALOR RECEBIDO OU DESCONTO
        $('#valorRecebido').focus(function(){
            $('#btsReceber').fadeOut();
            $('#listarParcelas').fadeOut(function(){
                $(this).html('')
            });
        });
        
        //AO CLICAR NO BOT�O "ATUALIZAR"
        $('#atualizar').css('cursor','pointer').click(function(){
            
            valor_recebido = $('#valorRecebido').val();
            desconto = $('#desconto').val();
            
            valor_recebido = parseFloat((valor_recebido.replace('.','')).replace(',','.'));
            desconto = parseFloat((desconto.replace('.','')).replace(',','.'));
            
            //VALIDA�AO - FALTA DE VALORES OU DESCONTO MAIOR QUE O VALOR
            if(valor_recebido == '' || valor_recebido == '0,00' || desconto > valor_recebido ||  $('#tipo_doc').val() == '0')
            {
                alert('Confira os valores');
                return false;
            }
            
            valor_recebido = $('#valorRecebido').val();
            desconto = $('#desconto').val();
            if(!desconto) desconto = '0.00';
            total_recebido = parseFloat((valor_recebido.replace('.','')).replace(',','.')) + parseFloat((desconto.replace('.','')).replace(',','.'));
            parcelas_abatidas = '';
            
            dados_hidden='';
            ultima_parcela = '';
            saldo_atual = total_recebido;

            $('#listarParcelas').fadeOut(function(){
                $(this).html('');//zera o valor do label
                $('.par_'+cobranca+tipoValor).each(function(){
                    
                    dados_parcela = $(this).attr('id').split('|');
                    
                    aux = $(this).val();
                    
                    juros = $(this).attr('name');
                   
                    valor_atual = ((aux.replace('R$ ', '')).replace('.','')).replace(',','.');
                    
                    saldo_atual = parseFloat(saldo_atual).toFixed(2);
                    valor_atual = parseFloat(valor_atual).toFixed(2);
                    
                    if(parseFloat(saldo_atual) >= parseFloat(valor_atual))
                    {
                        parcelas_abatidas += dados_parcela[0]+'(R$ '+parseFloat(valor_atual).toFixed(2)+'), ';
                        dados_hidden += dados_parcela[1]+'|'+parseFloat((valor_atual-saldo_atual)).toFixed(2)+'|quitada|'+tipoValor+'|'+juros+'|'+dados_parcela[2]+'|'+dados_parcela[3]+'|'+dados_parcela[4]+'|'+dados_parcela[0]+';';
                        saldo_atual -= valor_atual;
                    }
                    else
                    {
                        ultima_parcela = dados_parcela[0]+'(R$ '+parseFloat(saldo_atual).toFixed(2)+'), ';
                        dados_hidden += dados_parcela[1]+'|'+parseFloat((valor_atual-saldo_atual)).toFixed(2)+'|pendente|'+tipoValor+'|'+juros+'|'+dados_parcela[2]+'|'+dados_parcela[3]+'|'+dados_parcela[4]+'|'+dados_parcela[0]+';';
                        saldo_atual = 0;
                        return false;
                    }

                });
                if(saldo_atual.toFixed(2) == 0){
                    $('#listarParcelas').append('Parcelas: '+parcelas_abatidas).append('<i style="color:red">'+ultima_parcela+'</i>');
                    $('#listarParcelas').fadeIn();
                    $('#btsReceber').fadeIn();
                }
                else{
                    $('#btsReceber').hide();
                    alert('Valor excedente: '+parseFloat(saldo_atual).toFixed(2));
                    return false;
                }
            });
            //PRA N�O ESQUCER
        });
        
        
        /**************** INICIO FUN�OES DIFEREN�A ENTRE DATAS *****************/
        
        var dateDif = {
            dateDiff: function(strDate1,strDate2)
            {
                return (((Date.parse(strDate2))-(Date.parse(strDate1)))/(24*60*60*1000)).toFixed(0);
            }
        }
        
        function diasEntreDatas(dataInicial, dataFinal)
        {
            var mes, dataAtual, dataInicial, arrDataInicial, novaDataInicial, diasEntreDatas;
            mes = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            
            arrDataFinal = dataFinal.split('/');
            arrDataInicial = dataInicial.split('/');
            novaDataInicial = mes[(arrDataInicial[1] - 1)] + ' ' + arrDataInicial[0] + ' ' + arrDataInicial[2];
            novaDataFinal = mes[(arrDataFinal[1] - 1)] + ' ' + arrDataFinal[0] + ' ' + arrDataFinal[2];
            diasEntreDatas = dateDif.dateDiff(novaDataInicial, novaDataFinal);
            //            alert(diasEntreDatas + " dias");
            return diasEntreDatas;
        }
        
        /**************** FIM FUN�OES DIFEREN�A ENTRE DATAS *****************/
        
        
        
        //--------------------------- VALIDA��ES -------------------------------
        
        $('#formExtAcordo').submit(function(e){
            if($('input[name="devdoc_mot"]').val() == ''){
                alert('Aten��o, O motivo deve ser preenchido!');
                $('input[name="devdoc_mot"]').focus();
                return false;
            }else if($('input[name="renegociacao_mot"]').val() == ''){
                alert('Aten��o, O motivo deve ser preenchido!');
                $('input[name="renegociacao_mot"]').focus();
                return false;
            }else if($('input[name="reabilitacao_mot"]').val() == ''){
                alert('Aten��o, O motivo deve ser preenchido!');
                $('input[name="reabilitacao_mot"]').focus();
                return false;
            }else if($('input[name="ext_jud_mot"]').val() == ''){
                alert('Aten��o, O motivo deve ser preenchido!');
                $('input[name="ext_jud_mot"]').focus();
                return false;
            }
        });
        
        //----------------------------------------------------------------------
        //----------------------------------------------------------------------
        function popularCidade(entidade)
        {
            
            //            alert('Entrou na Fun��o: '+entidade);
            //$('select[id="uf"]').change(function(){
            
            v1 = '';
            //captura os options selecionados
            v1 = $('select[id="'+entidade+'uf"] option:selected').text();
            if(v1 == '') v1='UF';
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() . 'inadimplente/listar_cidades'; ?>",
                data: 'v1='+v1,
                dataType: 'json',
                error: function(xhr, status, er) {
                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                },
                success: function(retorno)
                {
                    $('#'+entidade+'cidade').html('<option value="">SELECIONE UMA CIDADE</option>');//zera os valores antigos e escreve no elemento o valor selecione
                    $qtd = '';
                    $valor = '';
                    $.each(retorno.cidadesbrasil, function(i, v){//para cada �ndice retornado executa a fun��o
                        if(v.cidadeatual != ''){
                            $('#'+entidade+'cidade-button').fadeOut('normal',function(){
                                //ESCONDE O OPTION INICIAL SEM VALOR
                            });
                            $('#'+entidade+'cidade').append('<option value="'+unescape(v.cidadeatual)+'">'+unescape(v.cidadeatual)+'</option>');//unescape descriptografa o padr�o URL de dados retornado no PHP
                        }
                        $qtd = i;
                        $valor = v.cidadeatual;
                    });
                    if ($qtd != 0 && $valor != ''){
                        $('#'+entidade+'cidade').fadeIn('normal', function(){
                            //CIDADES CARREGADAS COM SUCESSO
                        });
                    }
                    else
                    {
                        alert('Voc� deve selecionar um ESTADO v�lido.');
                        $('#'+entidade+'cidade').html('<option value=""><-- SELECIONE UM ESTADO</option>');//zera os valores antigos e escreve no elemento o valor selecione
                    }
                }
            });
        }
        base_url = "<?php echo base_url(); ?>";
        
    $("#btnEnvAcJud").click(function(){
        $(this).hide();
    });
    
    $('#confEnvioJudBtn').click(function(){
        $(this).hide();
    });
    
    $('.dividas_table a').click(function(){
        var aux = 0;
        $('.dividas_table.selected > div > table > tbody > tr > td > input.checkbox_dividas').each(function(){
            if($(this).attr('checked'))
            {
                $(this).click();
                aux = 1;
            }
        });
        if(aux == 1){
            alert('Aten��o!\n Somente as parcelas da d�vida ativa ser�o selecionadas')
        }
    });
    $('.acordos_table a').click(function(){
        var aux = 0;
        $('.acordos_table.selected > div > table > tbody > tr > td > input.checkbox_acordos').each(function(){
            if($(this).attr('checked'))
            {
                $(this).click();
                aux = 1;
            }
        });
        if(aux == 1){
            alert('Aten��o!\n Somente as parcelas da d�vida ativa ser�o selecionadas')
        }
    });
    
    });
</script>
<style type="text/css">
    .left{
        width: 140px !important;
    }
    .trfirst{
        height: 30px;
    }
    .trfirst th{
        padding: 5px !important;
    }
    .valor{
        width: 120px !important;
    }
    .ui-selectmenu-open{
        z-index: 3000 !important;
    }
    .estrela1{
        display: block;
        float:left;
        width: 20px;
        height: 20px;
        background: url("<?php echo $img . 'star1.png'; ?>") top no-repeat;
    }
    .estrela2{
        display: block;
        float:left;
        width: 20px;
        height: 20px;
        background: url("<?php echo $img . 'star2.png'; ?>") top no-repeat;
    }
    .estrela3{
        display: block;
        float:left;
        width: 20px;
        height: 20px;
        background: url("<?php echo $img . 'star3.png'; ?>") top no-repeat;
    }
    .estrela4{
        display: block;
        float:left;
        width: 20px;
        height: 20px;
        background: url("<?php echo $img . 'star4.png'; ?>") top no-repeat;
    }
    .estrela5{
        display: block;
        float:left;
        width: 20px;
        height: 20px;
        background: url("<?php echo $img . 'star5.png'; ?>") top no-repeat;
    }
    .blocoestrelas .selected{
        background-position: bottom !important;
    }
</style>
<!-- Caixa de di�logo RO-->
<div id="dialog-form" class="form" style="padding-left: 5px; padding-right: 5px;" title="Receber">
    <form id="formReceber" action="<?php echo base_url() . 'divida/receber'; ?>"  method="post">
        <div class="fields" style="width: 405px; margin: 0 auto;">
            <div class="field" style="width: 400px;">
                <div style="margin-top: 15px; padding-top: 7px;">
                    <!--                    <div style="float: left; margin-right: 2px;">
                                            <input type="checkbox" id="data_agendamento" name="data_agendamento"/>
                                        </div>-->
                    <div style="float: left;">
                        <div style="width: 195px; padding-left: 1px; margin-top: 2px;" class="label">
                            <label for="tarefa">Calcular valores:</label>
                        </div>
                        <input type="radio" name="valores" value="0" class="tipoValor"/>Valores originais (sem juros)
                        <input type="radio" name="valores" value="1" class="tipoValor"/>Valores Atualizados (com juros)
                        <input type="hidden" name="cobCodRec" id="cobCodRec" value="0" />
                        <input type="hidden" name="urlVoltar" id="urlVoltar" value="" />
                        <input type="hidden" name="InadCodRec" id="InadCodRec" value="<?php echo $inadDados->ina_cod; ?>" />
                    </div>
                </div>
            </div>
            <div id="camposReceber">
                <div style="width: 400px; margin: 0 auto; margin-top: 20px; padding-bottom: 15px; border-bottom: 1px solid #ddd; z-index:3000">
                    <select style="width: 400px;" id="tipo_doc" name="tipo_doc">
                        <option value="0">[TIPO DE RECEBIMENTO]</option>
                        <option value="1">Balc�o - DINHEIRO</option>
                        <option value="2">Balc�o - CHEQUE</option>
                        <option value="3">Credor</option>
                        <option value="4">Em conta</option>
                    </select>
                </div>
                <div class="field" style="width: 400px;" id="dados_cheque">
                    <div style="margin-top: 0px; padding-top: 0px;">
                        <div style="width: 193px; padding-left: 1px; float: left;" class="label">
                            <label style="float: left;" for="dados_ch">Dados do Cheque:</label>
                        </div>
                        <div class="input" style="width: 383px; float: left !important;">
                            <input style="float: left;" type="text" id="dados_ch" name="dados_ch" value=""/>
                        </div>
                        <div style="width: 350px; margin-top: 5px; padding-bottom: 1px; padding-left: 1px; float: left;" class="label">
                            <label style="float: left; font-weight: normal; font-style: italic;">[banco],[ag�ncia],[CPF],[p�s-datado],[n� cheque]</label>
                        </div>
                    </div>

                </div>
                <div class="field" style="width: 400px;">
                    <div style="margin-top: 0px; padding-top: 0px;">
                        <div style="width: 103px; padding-left: 1px; float: left;" class="label">
                            <label style="float: left;" for="valorRecebido">Valor Recebido:</label>
                        </div>
                        <div class="input" style="width: 383px; float: left !important;">
                            <input style="float: left;" type="text" id="valorRecebido" name="valorRecebido"/>
                        </div>
                    </div>
                </div>
                <div class="field" style="width: 400px;">
                    <div style="margin-top: 0px; padding-top: 0px;">
                        <div style="width: 103px; padding-left: 1px; float: left;" class="label">
                            <label style="float: left;" for="desconto">Desconto:</label>
                        </div>
                        <div class="input" style="width: 383px; float: left !important;">
                            <input style="float: left;" type="text" id="desconto" name="desconto"/>
                        </div>
                    </div>
                </div>
                <div class="field" style="width: 400px; padding-bottom: 3px;">
                    <div style="margin-top: 0px; padding-top: 0px;">
                        <div style="width: 430px; padding-left: 1px; float: left;" class="label">
                            <a href="#" style="float: left;">
                                <img src="<?php echo $img . 'atu_azul.jpg' ?>" alt="atualizar" id="atualizar"/>
                            </a>
                            <label style="float: left; margin: 7px 0px 0px 5px;" for="parcelasPagas" id="listarParcelas"></label>
                        </div>
                    </div>
                </div>
                <div style="text-align: center; margin-top: 10px; margin-bottom: 10px;" class="buttons" id="btsReceber">
                    <input type="reset" name="cancelar" value="Cancelar" class="fechar"/>
                    <div style="margin-left: 7px;" class="highlight">
                        <input type="hidden" name="parcelas_recebidas" value="" id="dadosHidden"/>
                        <input type="submit" name="submit.highlight" id="submitReceberBtn" value="Receber" />
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- FIM Caixa de di�logo RO-->
<!-- Caixa de di�logo Encaminhar para Adm-->
<div class="dialog-message" title="Novo Prazo Juridico">
    <p>Novo prazo jur�dico:</p>
    <form action="<?php echo base_url() . 'divida/retornoAdm'; ?>" method="post" id="formSimples">
        <div class="input" style="float: left !important;">
            <input style="float: left; width: 50px !important;" type="text" id="novoPrazo" name="novoPrazo" value="30"/>
            <input type="hidden" name="cobCod" id="cobCod" value="0" />
            <input type="hidden" name="InadCod" id="InadCod" value="<?php echo $inadDados->ina_cod; ?>" />
        </div>
    </form>
</div>
<!-- FIM Caixa de di�logo Encaminhar para Adm-->

<!-- Caixa de di�logo Juridico -->
<div id="dialog-form2" class="form" style="padding-left: 5px; padding-right: 5px;" title="Enviar ao Judicial">
    <form id="formEnviarJud" action="<?php echo base_url() . 'divida/enviaJud'; ?>" method="post">
        <div class="fields" style="width: 400px; margin: 0 auto; padding-top: 15px;">
            <p style="margin: 0px;">Aten��o, Ao enviar a cobran�a para o setor Judicial o(s) supervisor(es) ser�o notificados e poder�o desfazer a altera��o</p>
            <div style="width: 400px; margin: 0 auto; margin-top: 5px; padding-bottom: 15px; border-bottom: 1px solid #ddd;">
                <select style="width: 400px;" id="usuario" name="usuario">
                    <option value="0">[ESCOLHA O RESPONS�VEL]</option>
                    <?php foreach ($cobradores as $cobrador): ?>
                        <option value="<?php echo $cobrador->usu_cod; ?>" <?php echo $cobrador->usu_cod == $this->session->userdata('usucod') ? 'selected' : '' ?>><?php echo $cobrador->usu_usuario_sis; ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="hidden" id="cobCodJud" name="cobCodJud" value="0"/>
                <input type="hidden" name="usuResp" value="<?php echo $this->session->userdata('usunome'); ?>"/>
                <input type="hidden" name="InadCod" id="InadCod" value="<?php echo $inadDados->ina_cod; ?>" />
            </div>
            <div style="text-align: center; margin-top: 10px; margin-bottom: 10px;" class="buttons">
                <input type="reset" class="fechar2" name="cancelar" value="Cancelar" />
                <div style="margin-left: 7px;" class="highlight">
                    <input id="confEnvioJudBtn" type="submit" name="submit.highlight" value="Confirmar" />
                </div>
            </div>
        </div>
    </form>
</div>
<!-- FIM Caixa de di�logo Juridico-->

<!-- Caixa de di�logo Juridico -->
<div id="dialog-form4" class="form" style="padding-left: 5px; padding-right: 5px;" title="Enviar acordo ao Judicial">
    <form id="formEnviarJud2" action="<?php echo base_url() . 'divida/enviaAcoJud'; ?>" method="post">
        <div class="fields" style="width: 400px; margin: 0 auto; padding-top: 15px;">
            <p style="margin: 0px;">Aten��o, Ao enviar a cobran�a para o setor Judicial o(s) supervisor(es) ser�o notificados e poder�o desfazer a altera��o</p>
            <div style="width: 400px; margin: 0 auto; margin-top: 5px; padding-bottom: 15px; border-bottom: 1px solid #ddd;">
                <select style="width: 400px;" id="usuario" name="usuario">
                    <option value="0">[ESCOLHA O RESPONS�VEL]</option>
                    <?php foreach ($cobradores as $cobrador): ?>
                        <option value="<?php echo $cobrador->usu_cod; ?>" <?php echo $cobrador->usu_cod == $this->session->userdata('usucod') ? 'selected' : '' ?>><?php echo $cobrador->usu_usuario_sis; ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="hidden" id="cobCodJud2" name="cobCodJud" value="0"/>
                <input type="hidden" name="usuResp" value="<?php echo $this->session->userdata('usunome'); ?>"/>
                <input type="hidden" name="InadCod" id="InadCod2" value="<?php echo $inadDados->ina_cod; ?>" />
            </div>
            <div style="text-align: center; margin-top: 10px; margin-bottom: 10px;" class="buttons">
                <input type="reset" class="fechar2" name="cancelar" value="Cancelar" />
                <div style="margin-left: 7px;" class="highlight">
                    <input id="btnEnvAcJud" type="submit" name="submit.highlight" value="Confirmar" />
                </div>
            </div>
        </div>
    </form>
</div>
<!-- FIM Caixa de di�logo Juridico-->

<!-- Caixa de di�logo Extin��o de acordo -->
<div id="dialog-form3" class="form" style="padding-left: 5px; padding-right: 5px;" title="Extin��o de acordo">
    <form id="formExtAcordo" action="<?php echo base_url() . 'divida/extAcordo'; ?>" method="post">
        <div class="fields" style="width: 700px; margin: 0 auto; padding-top: 15px;">
            <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                Extin��o de acordo
            </div>
            <div class="field" style="text-align: center;">
                <div class="divleft" style="border: none; width: 236px; margin: 0 auto; display: table-cell; text-align: center;">
                    <div style="margin-left: 242px; " class="select">
                        <select style="width: 230px;" id="motivo_ext" name="motivo_ext">
                            <option value="0">[MOTIVO DA EXTIN��O]</option>
                            <option value="1">Devolu��o ao credor</option>
                            <option value="2">Pagamento integral</option>
                            <option value="3">Solicitado pelo Credor</option>
                            <option value="4">Outros</option>
                        </select>
                    </div>
                    <input type="hidden" id="cobCodAco" name="cobCodAco" value="0"/>
                    <input type="hidden" name="usuResp" value="<?php echo $this->session->userdata('usunome'); ?>"/>
                    <input type="hidden" name="InadCod" id="InadCod" value="<?php echo $inadDados->ina_cod; ?>" />
                </div>
            </div>
            <div class="field" style="padding: 2px; height: 55px;">
                <div class="divleftlast" style="width: 300px; margin-left: 0px;">
                    <div class="label" style="width: 261px;">
                        <label>Devolu��o do documento original?</label>
                    </div>
                    <input style="margin-top: 7px; " type="radio" name="devdoc" value="1"/>Sim
                    <input type="radio" name="devdoc" value="0"/>N�o
                </div>
                <div class="divleftlast" style="width: 390px;">
                    <div style="width: 52px; padding-left: 1px;" class="label">
                        <label for="devdoc_mot">Motivo:</label>
                    </div>
                    <div class="input">
                        <input style="width: 380px;" type="text" id="devdoc_mot" name="devdoc_mot"/>
                    </div>
                </div>
            </div>
            <div class="field" style="padding: 2px; height: 55px;">
                <div class="divleftlast" style="width: 300px; margin-left: 0px;">
                    <div class="label" style="width: 261px;">
                        <label>Devolu��o de renegocia��o?</label>
                    </div>
                    <input style="margin-top: 7px; " type="radio" name="renegociacao" value="1"/>Sim
                    <input type="radio" name="renegociacao" value="0" />N�o
                </div>
                <div class="divleftlast" style="width: 390px;">
                    <div style="width: 52px; padding-left: 1px;" class="label">
                        <label for="renegociacao_mot">Motivo:</label>
                    </div>
                    <div class="input">
                        <input style="width: 380px;" type="text" id="renegociacao_mot" name="renegociacao_mot"/>
                    </div>
                </div>
            </div>
            <div class="field" style="padding: 2px; height: 55px;">
                <div class="divleftlast" style="width: 300px; margin-left: 0px;">
                    <div class="label" style="width: 261px;">
                        <label>Solicitado reabilita��o?</label>
                    </div>
                    <input style="margin-top: 7px; " type="radio" name="reabilitacao" value="1"/>Sim
                    <input type="radio" name="reabilitacao" value="0"/>N�o
                </div>
                <div class="divleftlast" style="width: 390px;">
                    <div style="width: 52px; padding-left: 1px;" class="label">
                        <label for="reabilitacao_mot">Motivo:</label>
                    </div>
                    <div class="input">
                        <input style="width: 380px;" type="text" id="reabilitacao_mot" name="reabilitacao_mot"/>
                    </div>
                </div>
            </div>
            <div class="field" style="padding: 2px; height: 55px;">
                <div class="divleftlast" style="width: 300px; margin-left: 0px;">
                    <div class="label" style="width: 261px;">
                        <label>Executado pedido de extin��o judicial?</label>
                    </div>
                    <input style="margin-top: 7px; " type="radio" name="ext_jud" value="1"/>Sim
                    <input type="radio" name="ext_jud" value="0"/>N�o
                </div>
                <div class="divleftlast" style="width: 390px;">
                    <div style="width: 52px; padding-left: 1px;" class="label">
                        <label for="ext_jud_mot">Motivo:</label>
                    </div>
                    <div class="input">
                        <input style="width: 380px;" type="text" id="ext_jud_mot" name="ext_jud_mot"/>
                    </div>
                </div>
            </div>
            <div style="text-align: center; margin-top: 10px; margin-bottom: 10px;" class="buttons">
                <div style="margin-left: 7px;" class="highlight">
                    <input type="submit" name="submit.highlight" value="Finalizar"/>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- FIM Caixa de di�logo Extin��o de acordo -->

<div id="content">

    <?php echo $sidebar; ?>

    <div id="right" style="min-height: 798px;">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Ficha (<a href="<?php echo base_url() . 'inadimplente/editar/cod:' . $inadDados->ina_cod ?>" target="_blanck" style="color:white"><?php echo $inadDados->ina_nome ?></a>)</h5>
                <ul class="links">
                    <li><a href="#inad">Cadastro</a></li>
                    <li><a href="#dividas">D�vidas</a></li>
                    <li><a href="#acordos">Acordos</a></li>
                    <li><a href="#juridico">Jur�dico</a></li>
                </ul>
            </div>
            <?php if ($inadDados->ina_negativado == 1): ?>
                <div id="box-messages">
                    <div class="messages">
                        <div id="message-error" class="message message-error">
                            <div class="image">
                                <img src="<?php echo $img . 'icons/error.png' ?>" alt="Error" height="32" />
                            </div>
                            <div class="text">
                                <h6>Inadimplente NEGATIVADO</h6>
                            </div>
                            <div class="dismiss">
                                <a href="#message-error"></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div id="inad">
                <form id="formUP" action="<?php echo base_url() . 'divida/atualizarInadimplente/cod:' . $inadDados->ina_cod ?>" method="post">
                    <?php echo $mensagem ?>
                    <div class="form">
                        <?php if($existeImportacao == 1):?>
                        <div class="fields">
                            <div class="field  field-first">
                                <div class="divleftlast" style="width: 493px; margin: 0px;">
                                    <div class="buttons" style="margin: 10px 0 0 283px;">
                                        <div class="highlight">
                                            <a href="<?php echo base_url().'inadimplente/inadDadosImportados/inaCod:'.$inadDados->ina_cod;?>" target="_blank" class="dialog-form-open ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">Visualiar dados Importados</a>
                                        </div>
                                    </div>                       
                                </div>
                            </div>
                        </div>
                        <?php endif;?>
                        <div class="fields">
                            <div class="field  field-first">
                                <div class="divleft" style="width: 116px;">
                                    <div class="label">
                                        <label>C�digo:</label>
                                    </div>
                                    <input type="hidden" name="cod"  value="<?php echo $inadDados->ina_cod; ?>"/>
                                    <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inadDados->ina_cod; ?></p>
                                </div>
                                <div class="divleft" style="width: 200px;">
                                    <div class="label">
                                        <label>Pessoa:</label>
                                    </div>
                                    <input style="margin-top: 7px; " type="radio" name="pessoa" value="f" id="pfisica" <?php echo $inadDados->ina_pessoa == 'f' ? 'checked' : ''; ?>>F�sica
                                    <input type="radio" name="pessoa" value="j" id="pjuridica" <?php echo $inadDados->ina_pessoa == 'j' ? 'checked' : ''; ?>>Jur�dica
                                </div>
                                <div id="atrsPessoaFisica" style="width:375px; height:30px; float: right; overflow: hidden">
                                    <div id="sexo_est_civil">
                                        <div class="divleft" style="width: 173px;" id="sexo" >
                                            <div class="label">
                                                <label>Sexo:</label>
                                            </div>
                                            <input type="radio" name="sexo" value="m" style="margin-top: 7px;" <?php echo $inadDados->ina_sexo == 'm' ? 'checked' : ''; ?>>Masc.
                                            <input type="radio" name="sexo" value="f" <?php echo $inadDados->ina_sexo == 'f' ? 'checked' : ''; ?> >Fem.
                                        </div>
                                        <div class="divleftlast" style="width: 184px;" id="estadocivil">
                                            <div class="select">
                                                <select style="width: 186px; margin-left: 5px;" id="est_civil" name="estado_civil">
                                                    <option value="0" class="ec">Estado civil</option>
                                                    <option value="1" class="ec" <?php echo $inadDados->ina_estado_civil == 1 ? 'selected' : ''; ?>>Solteiro(a)</option>
                                                    <option value="2" class="ec" <?php echo $inadDados->ina_estado_civil == 2 ? 'selected' : ''; ?>>Casado(a)</option>
                                                    <option value="3" class="ec" <?php echo $inadDados->ina_estado_civil == 3 ? 'selected' : ''; ?>>Separado(a)</option>
                                                    <option value="4" class="ec" <?php echo $inadDados->ina_estado_civil == 4 ? 'selected' : ''; ?>>Divorciado(a)</option>
                                                    <option value="5" class="ec" <?php echo $inadDados->ina_estado_civil == 5 ? 'selected' : ''; ?>>Viuvo(a)</option>
                                                    <option value="6" class="ec" <?php echo $inadDados->ina_estado_civil == 6 ? 'selected' : ''; ?>>Amasiado(a)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 371px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="nome">Nome:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 248px;" type="text" id="nome" name="nome_fantasia" maxlength="120" value="<?php echo utf8_decode($inadDados->ina_nome); ?>"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 231px;">
                                    <div style="width: 76px; padding-left: 1px;" class="label">
                                        <label for="cpf_cnpj">CPF/CNPJ:</label>
                                    </div>
                                    <div class="input" id="cpf_cnpj_div">
                                        <?php
                                        if (($inadDados->ina_cpf_cnpj == '') || ($usuarioDados->grupo_usuarios_gru_cod == '1') || ($usuarioDados->grupo_usuarios_gru_cod == '2')) {
                                            echo '<input style="width: 124px; background-color:#F6F6F6;" type="text" id="cpf_cnpj" name="cpf_cnpj" maxlength="18" value="' . $inadDados->ina_cpf_cnpj . '"/>';
                                        } else {
                                            echo '<input disabled style="" type="text" id="cpf_cnpj_falso" name="cpf_cnpj_falso" maxlength="18" value="' . $inadDados->ina_cpf_cnpj . '"/>';
                                            echo '<input type="hidden" name="cpf_cnpj" value="' . $inadDados->ina_cpf_cnpj . '"/>';
                                        }
                                        ?>

                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 211px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="rg_ie">RG/I.E.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 89px;" type="text" id="rg_ie" maxlength="30" name="rg_ie" value="<?php echo $inadDados->ina_rg_ie; ?>"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 250px;">
                                    <div style="width: 42px; padding-left: 1px;" class="label">
                                        <label for="endereco">End.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 178px;" type="text" id="endereco" maxlength="130" name="endereco" value="<?php echo utf8_decode($inadDados->ina_endereco); ?>"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 233px; margin: 0px;">
                                    <div style="width: 51px; padding-left: 1px;" class="label">
                                        <label for="bairro">Bairro:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 155px;" type="text" id="bairro" maxlength="50" name="bairro" value="<?php echo utf8_decode($inadDados->ina_bairro); ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 211px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="complemento">Compl.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 89px;" type="text" id="complemento" name="complemento" maxlength="40" value="<?php echo utf8_decode($inadDados->ina_complemento); ?>"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 132px;">
                                    <div style="width: 34px; padding-left: 1px;" class="label">
                                        <label for="cep">CEP.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 65px;" type="text" id="cep" name="cep" maxlength="8" value="<?php echo $inadDados->ina_cep; ?>"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 109px;">
                                    <div style="width: 27px; padding-left: 1px;" class="label">
                                        <label for="uf">UF:</label>
                                    </div>
                                    <div class="select">
                                        <select style="width: 65px; margin-left: 7px;" id="ina_uf" name="uf">
                                            <option>UF</option>
                                            <?php foreach ($estados as $uf): ?>
                                                <option value="<?php echo $uf->cid_estado; ?>" <?php echo $inadDados->ina_uf == $uf->cid_estado ? 'selected' : ''; ?>><?php echo $uf->cid_estado; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 230px; margin: 0px; float: left">
                                    <div class="select">
                                        <select style="width: 230px; height: 28px; border: 1px solid #D4D0C8; padding: 5px; font-size: 11px; font-family: Verdana" id="ina_cidade" name="cidade">
                                            <option style="border: 1px solid;"><?php echo utf8_decode($inadDados->ina_cidade); ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 240px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="fone_res">Fone Res.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 117px;" type="text" id="fone_res" class="telefone" name="foneres" value="<?php echo $inadDados->ina_foneres ?>"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 238px;">
                                    <div style="width: 77px; padding-left: 1px;" class="label">
                                        <label for="fone_rec">Fone Rec.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 130px;" type="text" id="fone_rec" class="telefone" name="fonerec" value="<?php echo $inadDados->ina_fonerec ?>"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 216px; margin: 0px;">
                                    <div style="width: 74px; padding-left: 1px;" class="label">
                                        <label for="fone_com">Fone Com.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 115px;" type="text" id="fone_com" class="telefone" name="fonecom" value="<?php echo $inadDados->ina_fonecom ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 240px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="fone_cel1">Celular(1):</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 117px;" type="text" id="fone_cel1" class="telefone" name="cel1" value="<?php echo $inadDados->ina_cel1 ?>"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 238px;">
                                    <div style="width: 77px; padding-left: 1px;" class="label">
                                        <label for="fone_cel2">Celular(2):</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 130px;" type="text" id="fone_cel2" class="telefone" name="cel2" value="<?php echo $inadDados->ina_cel2 ?>"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 216px; margin: 0px;">
                                    <div style="width: 74px; padding-left: 1px;" class="label">
                                        <label for="fone_cel3">Celular(3):</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 115px;" type="text" id="fone_cel3" class="telefone" name="cel3" value="<?php echo $inadDados->ina_cel3 ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div style="height: 75px;" class="field">
                                <div class="divleftlast" style="width: 701px;">
                                    <div style="width: 84px;" class="label">
                                        <label for="informacoes">Informa��es relevantes:</label>
                                    </div>
                                    <div class="textarea">
                                        <textarea style="width: 586px; height: 50px;" id="informacoes" name="info" cols="500" rows="4"><?php echo $inadDados->ina_info ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="blocoTitulo">
                                Parentes de 1� Grau
                            </div>
                            <div class="field" style="border-bottom: none; padding: 0px;">
                                <div class="divleft" style="width: 502px; height: 40px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="conjuge">Conjuge:</label>
                                    </div>
                                    <div class="input">
                                        <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inadDados->ina_conj_nome ?></p>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 196px;">
                                    <div style="width: 41px; padding-left: 1px;" class="label">
                                        <label for="fone_conj">Fone:</label>
                                    </div>
                                    <div class="input">
                                        <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inadDados->ina_conj_fone ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="field" style="padding: 1px;" > 
                                <div class="divleft" style="width: 390px; height: 40px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="endereco_conj">Endere�o:</label>
                                    </div>
                                    <div class="input">
                                        <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inadDados->ina_conj_endereco ?></p>
                                    </div>
                                </div>

                                <div class="divleftlast" style="width: 260px; margin: 0px;">
                                    <div style="width: 59px; padding-left: 1px;" class="label">
                                        <label for="cidade_coj">Cidade:</label>
                                    </div>
                                    <div class="input">
                                        <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inadDados->ina_conj_cidade . ' - ' . $inadDados->ina_conj_uf; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="field" style="border-bottom: none; padding: 0px;">
                                <div class="divleft" style="width: 502px; height: 40px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="pai">Pai:</label>
                                    </div>
                                    <div class="input">
                                        <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inadDados->ina_pai_nome; ?></p>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 196px;">
                                    <div style="width: 41px; padding-left: 1px;" class="label">
                                        <label for="pai_fone">Fone:</label>
                                    </div>
                                    <div class="input">
                                        <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inadDados->ina_pai_fone ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="field" style="padding: 2px;">
                                <div class="divleft" style="width: 390px; height: 40px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="pai_endereco">Endere�o:</label>
                                    </div>
                                    <div class="input">
                                        <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inadDados->ina_pai_endereco; ?></p>
                                    </div>
                                </div>

                                <div class="divleftlast" style="width: 260px; margin: 0px;">
                                    <div style="width: 59px; padding-left: 1px;" class="label">
                                        <label for="cidade_coj">Cidade:</label>
                                    </div>
                                    <div class="input">
                                        <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inadDados->ina_pai_cidade . ' - ' . $inadDados->ina_pai_uf; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="field" style="border-bottom: none; padding: 0px;">
                                <div class="divleft" style="width: 502px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="mae_nome">M�e:</label>
                                    </div>
                                    <div class="input">
                                        <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inadDados->ina_mae_nome; ?></p>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 196px;">
                                    <div style="width: 41px; padding-left: 1px;" class="label">
                                        <label for="mae_fone">Fone:</label>
                                    </div>
                                    <div class="input">
                                        <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inadDados->ina_mae_fone; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="field" style="padding: 2px;">
                                <div class="divleft" style="width: 390px; height: 40px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="mae_endereco">Endere�o:</label>
                                    </div>
                                    <div class="input">
                                        <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inadDados->ina_mae_endereco; ?></p>
                                    </div>
                                </div>

                                <div class="divleftlast" style="width: 260px; margin: 0px;">
                                    <div style="width: 59px; padding-left: 1px;" class="label">
                                        <label for="cidade_coj">Cidade:</label>
                                    </div>
                                    <div class="input">
                                        <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inadDados->ina_mae_cidade . ' - ' . $inadDados->ina_mae_uf; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="fields">
                                <div>
                                    <div class="blocoTitulo">
                                        Parentes Cadastrados
                                    </div>
                                </div>
                                <div class="table" style="padding: 0px 19px 10px;">
                                    <table id="products">
                                        <?php if ($inaParentes != null): ?>
                                            <thead>
                                                <tr>
                                                    <th class="left">Parentesco</th>
                                                    <th style="width: 205px">Nome</th>
                                                    <th style="">Telefones</th>
                                                    <th class="last">A��o</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($inaParentes as $parente): ?>
                                                    <tr>
                                                        <td class="title"><?php echo utf8_decode($parente->par_parentesco); ?></td>
                                                        <td><?php echo utf8_decode($parente->par_nome); ?></td>
                                                        <td><?php echo $parente->par_fone1 . ' - ' . $parente->par_fone2 . ' - ' . $parente->par_fone3 . ' - ' . $parente->par_fone4; ?></td>
                                                        <td class="last"><?php echo $parente->par_inad_cod != '0' ? '<a href="' . base_url() . 'inadimplente/editar/cod:' . $parente->par_inad_cod . '">Cadastro</a>' : ''; ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            <?php echo $inaParentes == null ? '<i>Nenhum inadimplente cadastrado</i>' : '' ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div style="text-align: center; margin-top: 10px;" class="buttons">
                                <div class="highlight">
                                    <input type="submit" name="cadastrar" value="Alterar" />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div id="dividas">
                <div class="form">
                    <div class="fields">
                        <div class="accordion">
                            <!-------------------------------------Credor-------------------------------->
                            <?php foreach ($credores as $credor): ?>
                                <div>
                                    <div style="background-color: #C4D6E0; padding-left: 10px; text-align: left;margin-top: 14px; margin-left: 5px; margin-right: 4px; width: 507px; float: left; margin-bottom: 3px; padding-bottom: 10px; padding-top: 8px;" class="blocoTitulo">
                                        Credor: <?php echo $credor->cre_nome_fantasia ?>
                                    </div>
                                    <?php
                                    $cods = '';
                                    foreach ($dividas[$credor->credor_cre_cod] as $divida) {
                                        $cods .= $divida->cob_cod . '-' .$cobrancasGeradas;
                                    };
                                    ?>
                                    <form action="" method="post"  id="<?php echo $cods; ?>" class="formDividas"> 
                                        <div style="background-color: #fff; height: 30px; margin-top: 10px; width: 131px; padding-left: 4px; float: left; padding-top: 4px; padding-bottom: 5px;" class="blocoTitulo">
                                            <div class="select">
                                                <select style="width: 133px;" id="opcoesDivida" name="opcoesDivida">
                                                    <option value="0">[A��ES]</option>
                                                    <option value="novoRO">Novo RO</option>
                                                    <option value="acordo">Acordo</option>
                                                    <option value="judicial">Judicial</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div style="background-color: #fff; margin-top: 10px; margin-left: 0px; width: 45px; float: left; padding-top: 4px; padding-bottom: 4px;" class="blocoTitulo">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 45px; height: 32px;" type="submit" name="Registro de Operacoes" value="OK" />
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="table dividas_table" style="padding: 0px 5px 10px;">
                                    <!-------------------------------- Dividas ------------------------------------>

                                    <?php foreach ($dividas[$credor->credor_cre_cod] as $divida): ?>
                                        <a href="#" class="ACHeader" id="<?php echo $divida->cob_cod; ?>" style="clear: both; <?php echo $divida->div_prazo <= 0 ? 'background-color: #f33;' : 'background-color: #DDD;'; ?>  padding-left: 10px; text-align: left;margin-top: 5px; margin-left: 0px; margin-right: 0px; width: 692px; float: left; margin-bottom: 3px; padding-bottom: 10px; padding-top: 8px; <?php echo $divida->div_prazo <= 0 ? 'color: #fff;' : 'color: #000;'; ?>; font-weight: bold;">Dados da d�vida c�digo: <?php echo $divida->cob_cod; ?></a>
                                        <div style="clear: both;">
                                            <table id="products" class="meialinha">
                                                <?php
                                                $i = 1;
                                                foreach ($parDividas[$divida->div_cod] as $par) {
                                                    $i++;
                                                }
                                                ?>
                                                <thead>
                                                    <tr class=" trfirst">
                                                        <th class="left">Dados do t�tulo 1</th>
                                                        <th>N�Doc</th>
                                                        <th>Parcela</th>
                                                        <th>Vencimento</th>
                                                        <th class="valor">Valor</th>
                                                        <th class="last"><input type="checkbox" onclick="
                                                            $('.pacelas_<?php echo $divida->div_cod; ?>').click();"
                                                                                name="pacelas_<?php echo $divida->div_cod; ?>[]"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="title" rowspan="<?php echo $i; ?>">
                                                            <b>Tipo de doc.: </b><?php echo $divida->div_documento; ?><br/>
                                                            <b>Emiss�o: </b><?php echo convData($divida->div_emissao, 'd'); ?><br/>
                                                            <b>Cadastro: </b><?php echo convData($divida->div_cadastro, 'd'); ?><br/>
                                                            <b>Total: </b><?php echo convMoney($divida->div_total); ?><br/><br/>
                                                            <?php if ($divida->cob_avaliacao != 0): ?>
                                                                <b>Avalia��o</b><br/>
                                                                <div class="blocoestrelas">
                                                                    <div class="estrela1 <?php echo $divida->cob_avaliacao == 1 ? 'selected' : ''; ?>"></div>
                                                                    <div class="estrela2 <?php echo $divida->cob_avaliacao == 2 ? 'selected' : ''; ?>"></div>
                                                                    <div class="estrela3 <?php echo $divida->cob_avaliacao == 3 ? 'selected' : ''; ?>"></div>
                                                                    <div class="estrela4 <?php echo $divida->cob_avaliacao == 4 ? 'selected' : ''; ?>"></div>
                                                                    <div class="estrela5 <?php echo $divida->cob_avaliacao == 5 ? 'selected' : ''; ?>"></div>
                                                                </div>
                                                            <?php endif; ?>
                                                            <?php if ($divida->cob_avaliacao == 0): ?>
                                                                <br/><b>N�o Avaliada</b>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                    <?php foreach ($parDividas[$divida->div_cod] as $parcela): ?>
                                                        <tr class="TRpacelas_<?php echo $divida->div_cod; ?>" id="par_<?php echo $parcela->pad_codigo; ?>">
                                                            <td><?php echo $parcela->pad_doc_num; ?></td>
                                                            <td><?php echo $parcela->pad_par_num; ?></td>
                                                            <td><?php echo convData($parcela->pad_vencimento, 'd'); ?></td>
                                                            <td><?php echo convMoney($parcela->pad_valor); ?></td>
                                                            <td class="last" style="text-align: center;">
                                                                <input id="par_<?php echo $parcela->pad_codigo; ?>" <?php echo $parcela->pad_acordada == 0 ? '' : ' disabled'; ?> type="checkbox" onclick="auxA=$(this).attr('id');
                                                                    auxB=$('#'+auxA).attr('class');
                                                                    auxB==('TRpacelas_<?php echo $divida->div_cod; ?> selected')?$('#'+auxA).removeClass('selected'):$('#'+auxA).addClass('selected');
                                                                       " class="pacelas_<?php echo $divida->div_cod; ?> checkbox_dividas" name="pacelas_<?php echo $divida->div_cod; ?>[]" value="<?php echo $parcela->pad_codigo; ?>" />
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                        <tr>
                                                            <td><b>Informa��es Relevantes:</b></td>
                                                            <td colspan="5" class="last"><i><?php echo $divida->div_info ?></i></td>
                                                        </tr>
                                                        
                                                        
                                                </tbody>
                                            </table>
                                        </div>
                                        <!--                                        </a>-->
                                    <?php endforeach; ?>
                                    <!-------------------------------- Dividas ------------------------------------>
                                </div>
                            <?php endforeach; ?>
                            <!-------------------------------- CREDOR ------------------------------------->
                        </div>
                    </div>
                </div>
            </div>
            <div id="juridico">
                <div class="form">
                    <div class="fields">
                        <?php if ($JUDRecemCriados != null): ?>
                            <div class="blocoTitulo2">
                                Acordos rec�m enviados
                            </div>

                            <table class="tabelaDados">
                                <thead>
                                    <tr>
                                        <td>Credor</td>
                                        <td>Valor Remanescente</td>
                                        <td>Data do Envio</td>
                                        <td class="last">A��es</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($JUDRecemCriados as $acordo): ?>
                                        <tr>
                                            <td><?php echo $acordo->cre_nome_fantasia; ?></td>
                                            <td><?php echo convMoney($acordo->aco_valor_original); ?></td>
                                            <td><?php echo convData($acordo->aco_emissao, 'd'); ?></td>
                                            <td class="last" style=" text-align: center"><a href="<?php echo base_url() . 'divida/detalhesCobranca/c:' . $acordo->aco_cod ?>">Ver dados</a> | <a href="<?php echo base_url() . 'acordo/acordoRecemEnviado/cod:' . $acordo->cob_cod ?>">Gerar acordo</a> | <a href="<?php echo base_url() . 'ro/novo/cod:' . $acordo->cob_cod ?>">ROs</a> | <a href="<?php echo base_url() . 'divida/retornaRecemEnviadoaoAdm/c:' . $acordo->cob_cod . '-' . $acordo->aco_cob_cod_geradora . '-'. $inadDados->ina_cod ?>">Retornar ao Adm</a></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>

                            </table>
                        <?php endif; ?>
                        <div class="blocoTitulo2">
                            Acordos j� firmados
                        </div>
                        <!-- ---------------------------- Credores --------------------------------- -->
                        <div class="accordion">
                            <?php foreach ($credoresJUD as $credor): ?>
                                <div id="<?php echo $credor->credor_cre_cod; ?>">
                                    <div style="background-color: #C4D6E0; padding-left: 10px; text-align: left;margin-top: 14px; margin-left: 5px; margin-right: 4px; width: 507px; float: left; margin-bottom: 3px; padding-bottom: 10px; padding-top: 8px;" class="blocoTitulo">
                                        Credor: <?php echo $credor->cre_nome_fantasia ?>
                                    </div>
                                    <?php
                                    $cods = '';
                                    foreach ($acordosJUD[$credor->credor_cre_cod] as $divida) {
                                        $cods .= $divida->cob_cod . '-';
                                    };
                                    ?>
                                    <form action="" method="post"  id="<?php echo $cods; ?>" class="formJudicial"> 
                                        <div style="background-color: #fff; height: 30px; margin-top: 10px; width: 131px; padding-left: 4px; float: left; padding-top: 4px; padding-bottom: 5px;" class="blocoTitulo">
                                            <div class="select">
                                                <select style="width: 133px;" class="opcoesAcordos" name="opcoesAcordos">
                                                    <option value="1">[A��ES]</option>
                                                    <option value="extincao">Extin��o</option>
                                                    <option value="novoRO">Novo RO</option>
                                                    <option value="receber">Receber</option>
                                                    <option value="reacordo">Renegocia��o</option>
                                                    <option value="retornoADM">Retornar ao Adm.</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div style="background-color: #fff; margin-top: 10px; margin-left: 0px; width: 45px; float: left; padding-top: 4px; padding-bottom: 4px;" class="blocoTitulo">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 45px; height: 32px;" class="dialog-form-open" type="submit" name="Registro de Operacoes" value="OK" />
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="table judicial_table" style="padding: 0px 5px 10px;" id="<?php echo $credor->credor_cre_cod; ?>">
                                    <!--******************************* Acordos *********************************-->
                                    <?php foreach ($acordosJUD[$credor->credor_cre_cod] as $divida): ?>
                                        <?php
                                        $i = 1;
                                        foreach ($parAcordosJUD[$divida->aco_cod] as $par) {
                                            $i++;
                                        }
                                        ?>
                                        <a href="#" class="ACHeaderAcordo" id="<?php echo $divida->cob_cod; ?>" style="clear: both; background-color: #DDD; padding-left: 10px; text-align: left;margin-top: 5px; margin-left: 0px; margin-right: 0px; width: 692px; float: left; margin-bottom: 3px; padding-bottom: 10px; padding-top: 8px; color:#000; font-weight: bold;">Dados do acordo c�digo: <?php echo $divida->cob_cod; ?></a>

                                        <div style="clear: both;">

                                            <table id="products" class="meialinha">
                                                <thead>
                                                    <tr class="trfirst">
                                                        <th class="left" style="width: 210px !important;">Dados do acordo</th>
                                                        <th>Parcela</th>
                                                        <th>Vencimento</th>
                                                        <th class="valor">Valor</th>
                                                        <th class="valor">Saldo</th>
                                                        <th class="valor">Saldo c/ juros</th>
                                                        <th>Situa��o</th>
                                                        <th class="last"><input type="checkbox" onclick="
                                                            $('.pacelas_<?php echo $divida->aco_cod; ?>').click();"
                                                                                name="pacelas_<?php echo $divida->aco_cod; ?>[]"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="title" rowspan="<?php echo $i; ?>">
                                                            <b>Tipo: </b><?php echo $divida->aco_tipo == '1' ? 'Firmado' : 'Promessa' ?><br/>
                                                            <b>Proced�ncia : </b><?php switch ($divida->aco_procedencia) {
                                                                                        case 'ADM':
                                                                                            echo "Administrativo";
                                                                                            break;
                                                                                        case 'JUD':
                                                                                            echo "Judicial";
                                                                                            break;
                                                                                        case 'REA':
                                                                                            echo "Renegocia��o";
                                                                                            break;
                                                                                    } ?><br/>
                                                            <b>Emiss�o: </b><?php echo convData($divida->aco_emissao, 'd'); ?><br/>
                                                            <b>Val. original: </b><?php echo convMoney($divida->aco_valor_original); ?><br/>
                                                            <b>Val. atualizado: </b><?php echo convMoney($divida->aco_valor_atualizado); ?><br/>
                                                            <?php if ($divida->cob_avaliacao != 0): ?><br/>
                                                                <b>Avalia��o</b><br/>
                                                                <div class="blocoestrelas">
                                                                    <div class="estrela1 <?php echo $divida->cob_avaliacao == 1 ? 'selected' : ''; ?>"></div>
                                                                    <div class="estrela2 <?php echo $divida->cob_avaliacao == 2 ? 'selected' : ''; ?>"></div>
                                                                    <div class="estrela3 <?php echo $divida->cob_avaliacao == 3 ? 'selected' : ''; ?>"></div>
                                                                    <div class="estrela4 <?php echo $divida->cob_avaliacao == 4 ? 'selected' : ''; ?>"></div>
                                                                    <div class="estrela5 <?php echo $divida->cob_avaliacao == 5 ? 'selected' : ''; ?>"></div>
                                                                </div>
                                                            <?php endif; ?>
                                                            <?php if ($divida->cob_avaliacao == 0): ?>
                                                                <br/><b>N�o Avaliada</b>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                    <!--*********************************** Parcelas ****************************-->
                                                    <?php foreach ($parAcordosJUD[$divida->aco_cod] as $parcelas): ?>
                                                        <tr class="TRpacelas_<?php echo $divida->aco_cod; ?>" id="par_<?php echo $parcelas->paa_cod; ?>">
                                                            <td style="color: <?php echo parcelaSituacao($parcelas->paa_situacao, $parcelas->paa_vencimento, 'C') ?>;"><?php echo $parcelas->paa_parcela; ?></td>
                                                            <td style="color: <?php echo parcelaSituacao($parcelas->paa_situacao, $parcelas->paa_vencimento, 'C') ?>;"><?php echo convData($parcelas->paa_vencimento, 'd'); ?></td>
                                                            <td style="color: <?php echo parcelaSituacao($parcelas->paa_situacao, $parcelas->paa_vencimento, 'C') ?>;"><?php echo convMoney($parcelas->paa_valor); ?></td>
                                                            <td style="color: <?php echo parcelaSituacao($parcelas->paa_situacao, $parcelas->paa_vencimento, 'C') ?>;"><?php echo convMoney($parcelas->paa_saldo); ?></td>
                                                            <td style="color: <?php echo parcelaSituacao($parcelas->paa_situacao, $parcelas->paa_vencimento, 'C') ?>;"><?php echo convMoney($parcelas->paa_saldo_atual); ?></td>
                                                            <td style="color: <?php echo parcelaSituacao($parcelas->paa_situacao, $parcelas->paa_vencimento, 'C') ?>;"><?php echo parcelaSituacao($parcelas->paa_situacao, $parcelas->paa_vencimento, 'N') ?></td>
                                                            <td class="last"  style="color: #00f; text-align: center;">
                                                                <input id="par_<?php echo $parcelas->paa_cod; ?>" type="checkbox" onclick="auxA=$(this).attr('id');
                                                                    auxB=$('#'+auxA).attr('class');
                                                                    auxB==('TRpacelas_<?php echo $divida->aco_cod; ?> selected')?$('#'+auxA).removeClass('selected'):$('#'+auxA).addClass('selected');
                                                                       " class="pacelas_<?php echo $divida->aco_cod; ?> checkbox_dividas" name="pacelas_<?php echo $divida->aco_cod; ?>[]" value="<?php echo $parcelas->paa_cod; ?>" <?php echo $parcelas->paa_situacao == '1'?'disabled':'' ?> />

                                                                                                                                                                                    <!--                                                                <input type="checkbox" < ?php echo $parcelas->paa_situacao == '1' ? 'disabled' : ''; ?> name="titulo[1]_todos" id="< ?php echo $parcelas->paa_parcela . '|' . $parcelas->paa_cod; ?>"/>-->
                                                                <input type="hidden" <?php echo $parcelas->paa_situacao == '1' ? 'disabled' : ''; ?> name="<?php echo $parcelas->paa_juro_diario; ?>" id="<?php echo $parcelas->paa_parcela . '|' . $parcelas->paa_cod . '|' . $parcelas->paa_saldo . '|' . $parcelas->paa_saldo_atual . '|' . $parcelas->paa_vencimento; ?>" class="<?php echo $parcelas->paa_situacao != '1' ? 'par_' . $divida->cob_cod . '_semJuros' : ''; ?>" value="<?php echo convMoney($parcelas->paa_saldo); ?>"/>
                                                                <input type="hidden" <?php echo $parcelas->paa_situacao == '1' ? 'disabled' : ''; ?> name="<?php echo $parcelas->paa_juro_diario; ?>" id="<?php echo $parcelas->paa_parcela . '|' . $parcelas->paa_cod . '|' . $parcelas->paa_saldo . '|' . $parcelas->paa_saldo_atual . '|' . $parcelas->paa_vencimento; ?>" class="<?php echo $parcelas->paa_situacao != '1' ? 'par_' . $divida->cob_cod . '_comJuros' : ''; ?>" value="<?php echo convMoney($parcelas->paa_saldo_atual); ?>"/>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php endforeach; ?>
                                    <!--******************************* End Acordos *****************************-->
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <!-- --------------------------- End Credores ------------------------------ -->
                    </div>
                </div>
            </div>
            <div id="acordos">
                <div class="form">
                    <div class="fields">
                        <!-- ---------------------------- Credores --------------------------------- -->
                        <div class="accordion">
                            <?php foreach ($credoresAC as $credor): ?>
                                <div id="<?php echo $credor->credor_cre_cod; ?>">
                                    <div style="background-color: #C4D6E0; padding-left: 10px; text-align: left;margin-top: 14px; margin-left: 5px; margin-right: 4px; width: 507px; float: left; margin-bottom: 3px; padding-bottom: 10px; padding-top: 8px;" class="blocoTitulo">
                                        Credor: <?php echo $credor->cre_nome_fantasia ?>
                                    </div>
                                    <?php
                                    $cods = '';
                                    foreach ($dividasAC[$credor->credor_cre_cod] as $divida) {
                                        $cods .= $divida->cob_cod . '-'.$cobrancasGeradasAC;
                                    };
                                    ?>
                                    <form action="" method="post"  id="<?php echo $cods; ?>" class="formAcordos"> 
                                        <div style="background-color: #fff; height: 30px; margin-top: 10px; width: 131px; padding-left: 4px; float: left; padding-top: 4px; padding-bottom: 5px;" class="blocoTitulo">
                                            <div class="select">
                                                <select style="width: 133px;" class="opcoesAcordos" name="opcoesAcordos">
                                                    <option value="1">[A��ES]</option>
                                                    <option value="extincao">Extin��o</option>
                                                    <option value="judicial">Judicial</option>
                                                    <option value="novoRO">Novo RO</option>
                                                    <option value="receber">Receber</option>
                                                    <option value="reacordo">Renegocia��o</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div style="background-color: #fff; margin-top: 10px; margin-left: 0px; width: 45px; float: left; padding-top: 4px; padding-bottom: 4px;" class="blocoTitulo">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 45px; height: 32px;" class="dialog-form-open" type="submit" name="Registro de Operacoes" value="OK" />
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="table acordos_table" style="padding: 0px 5px 10px;" id="<?php echo $credor->credor_cre_cod; ?>">
                                    <!--******************************* Acordos *********************************-->
                                    <?php foreach ($dividasAC[$credor->credor_cre_cod] as $divida): ?>
                                        <?php
                                        $i = 1;
                                        foreach ($parDividasAC[$divida->aco_cod] as $par) {
                                            $i++;
                                        }
                                        ?>
                                        <a href="#" class="ACHeaderAcordo" id="<?php echo $divida->cob_cod; ?>" style="clear: both; background-color: #DDD; padding-left: 10px; text-align: left;margin-top: 5px; margin-left: 0px; margin-right: 0px; width: 692px; float: left; margin-bottom: 3px; padding-bottom: 10px; padding-top: 8px; color:#000; font-weight: bold;">Dados do acordo c�digo: <?php echo $divida->cob_cod;
                                ; ?></a>

                                        <div style="clear: both;">

                                            <table id="products" class="meialinha">
                                                <thead>
                                                    <tr class="trfirst">
                                                        <th class="left" style="width: 210px !important;">Dados do acordo</th>
                                                        <th>Parcela</th>
                                                        <th>Vencimento</th>
                                                        <th class="valor">Valor</th>
                                                        <th class="valor">Saldo</th>
                                                        <th class="valor">Saldo c/ juros</th>
                                                        <th>Situa��o</th>
                                                        <th class="last"><input type="checkbox" onclick="
                                                            $('.pacelas_<?php echo $divida->aco_cod; ?>').click();"
                                                                                name="pacelas_<?php echo $divida->aco_cod; ?>[]"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="title" rowspan="<?php echo $i; ?>">
                                                            <b>Tipo: </b><?php echo $divida->aco_tipo == '1' ? 'Firmado' : 'Promessa' ?><br/>
                                                            <b>Proced�ncia : </b><?php switch ($divida->aco_procedencia) {
                                                                                        case 'ADM':
                                                                                            echo "Administrativo";
                                                                                            break;
                                                                                        case 'JUD':
                                                                                            echo "Judicial";
                                                                                            break;
                                                                                        case 'REA':
                                                                                            echo "Renegocia��o";
                                                                                            break;
                                                                                    } ?><br/>
                                                            <b>Emiss�o: </b><?php echo convData($divida->aco_emissao, 'd'); ?><br/>
                                                            <b>Val. original: </b><?php echo convMoney($divida->aco_valor_original); ?><br/>
                                                            <b>Val. atualizado: </b><?php echo convMoney($divida->aco_total_parcelado); ?><br/>
                                                            <?php if ($divida->cob_avaliacao != 0): ?><br/>
                                                                <b>Avalia��o</b><br/>
                                                                <div class="blocoestrelas">
                                                                    <div class="estrela1 <?php echo $divida->cob_avaliacao == 1 ? 'selected' : ''; ?>"></div>
                                                                    <div class="estrela2 <?php echo $divida->cob_avaliacao == 2 ? 'selected' : ''; ?>"></div>
                                                                    <div class="estrela3 <?php echo $divida->cob_avaliacao == 3 ? 'selected' : ''; ?>"></div>
                                                                    <div class="estrela4 <?php echo $divida->cob_avaliacao == 4 ? 'selected' : ''; ?>"></div>
                                                                    <div class="estrela5 <?php echo $divida->cob_avaliacao == 5 ? 'selected' : ''; ?>"></div>
                                                                </div>
                                                            <?php endif; ?>
                                                            <?php if ($divida->cob_avaliacao == 0): ?>
                                                                <br/><b>N�o Avaliada</b>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                    <!--*********************************** Parcelas ****************************-->
                                                    <?php foreach ($parDividasAC[$divida->aco_cod] as $parcelas): ?>
                                                        <tr class="TRpacelas_<?php echo $divida->aco_cod; ?>" id="par_<?php echo $parcelas->paa_cod; ?>">
                                                            <td style="color: <?php echo parcelaSituacao($parcelas->paa_situacao, $parcelas->paa_vencimento, 'C') ?>;"><?php echo $parcelas->paa_parcela; ?></td>
                                                            <td style="color: <?php echo parcelaSituacao($parcelas->paa_situacao, $parcelas->paa_vencimento, 'C') ?>;"><?php echo convData($parcelas->paa_vencimento, 'd'); ?></td>
                                                            <td style="color: <?php echo parcelaSituacao($parcelas->paa_situacao, $parcelas->paa_vencimento, 'C') ?>;"><?php echo convMoney($parcelas->paa_valor); ?></td>
                                                            <td style="color: <?php echo parcelaSituacao($parcelas->paa_situacao, $parcelas->paa_vencimento, 'C') ?>;"><?php echo convMoney($parcelas->paa_saldo); ?></td>
                                                            <td style="color: <?php echo parcelaSituacao($parcelas->paa_situacao, $parcelas->paa_vencimento, 'C') ?>;"><?php echo convMoney($parcelas->paa_saldo_atual); ?></td>
                                                            <td style="color: <?php echo parcelaSituacao($parcelas->paa_situacao, $parcelas->paa_vencimento, 'C') ?>;"><?php echo parcelaSituacao($parcelas->paa_situacao, $parcelas->paa_vencimento, 'N') ?></td>
                                                            <td class="last"  style="color: #00f; text-align: center;">

                                                                <input id="par_<?php echo $parcelas->paa_cod; ?>" type="checkbox" onclick="auxA=$(this).attr('id');
                                                                    auxB=$('#'+auxA).attr('class');
                                                                    auxB==('TRpacelas_<?php echo $divida->aco_cod; ?> selected')?$('#'+auxA).removeClass('selected'):$('#'+auxA).addClass('selected');
                                                                       " class="pacelas_<?php echo $divida->aco_cod; ?> checkbox_dividas" name="pacelas_<?php echo $divida->aco_cod; ?>[]" value="<?php echo $parcelas->paa_cod; ?>" <?php echo $parcelas->paa_situacao != '0'?'disabled':'' ?> />

                                                                                                                                                                                    <!--                                                                <input type="checkbox" < ?php echo $parcelas->paa_situacao == '1' ? 'disabled' : ''; ?> name="titulo[1]_todos" id="< ?php echo $parcelas->paa_parcela . '|' . $parcelas->paa_cod; ?>"/>-->
                                                                <input type="hidden" <?php echo $parcelas->paa_situacao == '1' ? 'disabled' : ''; ?> name="<?php echo $parcelas->paa_juro_diario; ?>" id="<?php echo $parcelas->paa_parcela . '|' . $parcelas->paa_cod . '|' . $parcelas->paa_saldo . '|' . $parcelas->paa_saldo_atual . '|' . $parcelas->paa_vencimento; ?>" class="<?php echo $parcelas->paa_situacao != '1' ? 'par_' . $divida->cob_cod . '_semJuros' : ''; ?>" value="<?php echo convMoney($parcelas->paa_saldo); ?>"/>
                                                                <input type="hidden" <?php echo $parcelas->paa_situacao == '1' ? 'disabled' : ''; ?> name="<?php echo $parcelas->paa_juro_diario; ?>" id="<?php echo $parcelas->paa_parcela . '|' . $parcelas->paa_cod . '|' . $parcelas->paa_saldo . '|' . $parcelas->paa_saldo_atual . '|' . $parcelas->paa_vencimento; ?>" class="<?php echo $parcelas->paa_situacao != '1' ? 'par_' . $divida->cob_cod . '_comJuros' : ''; ?>" value="<?php echo convMoney($parcelas->paa_saldo_atual); ?>"/>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php endforeach; ?>
                                    <!--******************************* End Acordos *****************************-->
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <!-- --------------------------- End Credores ------------------------------ -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form action="<?php echo base_url() . 'acordo/novo'; ?>" method="post" id="formDadosAcordo">
        <input type="hidden" name="acordo_tipo" value="" id="acordo_tipo"/>
        <input type="hidden" name="dados_divida" value="" id="dados_divida"/>
    </form>
    <form action="<?php echo base_url() . 'acordo/reacordo'; ?>" method="post" id="formDadosReAcordo">
        <input type="hidden" name="acordo_tipo" value="" id="acordo_tipoRe"/>
        <input type="hidden" name="dados_divida" value="" id="dados_dividaRe"/>
        <input type="hidden" name="setor_reacordo" value="" id="setor_reacordo"/>
    </form>
</div>