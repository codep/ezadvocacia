<?php

class Login extends Controller {

    function Home() {
        parent::Controller();
    }

    function index() {
        //passar o titulo para a p�gina
        $this->data['title'] = "Recupera :: Sistema de cobran�a :: Login";
        //adicionar o css
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/Blue')); // CSS HOME
        //carregar o helper com as fun��es necess�rias
        $this->load->helper("funcoes_helper");
        //adicionar os javascripts
        $this->inicore->addjs(array('jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'tiny_mce/jquery.tinymce.js', 'smooth.form.js'));
        // verifica se usu�rio ja esta logado se estiver redireciona para home
        if ($this->session->userdata('status') == true) {
            redirect(base_url() . 'home');
        }
        $this->data['ulogin'] = '';
        // recebendo dados enviados por post
        $usuario = strtolower($this->input->post('usuario', true));
        // passando os dados do post por uma fun��o anti-sqlinject
        $usuario = preg_replace(sql_regcase("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/"), "", $usuario);
        //recebendo dados por post
        $senha = $this->input->post('senha', true);
        // passando os dados do post por uma fun��o anti-sqlinject
        $senha = preg_replace(sql_regcase("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/"), "", $senha);
        //ciptografando a senha para compara-la com a que est� no banco
        $senha = sha1($usuario . $senha);
        //se os campos necess�rios forem digitados
        if ($usuario && $senha) {
            //verifica se existe um usu�rio cadastrad no sistema com este login
            $sql = "SELECT usu_cod, usu_usuario_sis, usu_nome, usu_senha_sis, usu_hr_sis_entrada, usu_hr_sis_saida, gru_per_menu_nivel1, gru_per_menu_nivel2, gru_per_menu_nivel3 FROM usuarios U INNER JOIN grupo_usuarios G ON(G.gru_cod=U.grupo_usuarios_gru_cod) WHERE usu_usuario_sis = ? AND usu_ativo = 1";
            $query = $this->db->query($sql, array($usuario, $senha));
            //caso haja um registro o usu�rio foi encontrado
            if ($query->num_rows > 0) {
                //testa as duas senhas criptografadas para ver se a digitada esta correta
                if ($query->row()->usu_senha_sis == $senha) {
                    //passa a hr de entrada e saida do usuario para duas vari�veis
                    $hrentrada = $query->row()->usu_hr_sis_entrada;
                    $hrsaida = $query->row()->usu_hr_sis_saida;
                    //transforma as horas de acesso do usu�rio em minutos
                    $hrentrada = (substr($hrentrada, 0, 2) * 60) + (substr($hrentrada, 3, 2));
                    $hrsaida = (substr($hrsaida, 0, 2) * 60) + (substr($hrsaida, 3, 2));
                    //pega a hora atual e transforma em minutos
                    $hratual = (substr(date('H:i'), 0, 2) * 60) + (substr(date('H:i'), 3, 2));
                    //testa para ver se o usu�rio n�o esta acessando o sistema em horario indevido
                    if (($hrentrada > $hratual) || ($hrsaida < $hratual)) {
                        //se n�o tiver permiss�o para acesso neste hor�rio,
                        //exibe mensagem de erro e n�o loga no sistema
                        $this->inicore->setMensagem('error', 'Hor�rio de acesso indevido, entre em contato com o Administrador do sistema', true);
                        redirect(base_url() . 'login');
                    } else {
                        //pega o nome do usuario no banco e separa somente o
                        //primeiro nome para ser exibido no header
                        $nome = $query->row()->usu_nome;
                        $aux = strpos($nome, ' ');
                        $nome = substr($nome, 0, $aux);
                        // montanto array de dados da sess�o
                        $dadosusu = array(
                            //c�digo do usu�rio logado
                            'usucod' => $query->row()->usu_cod,
                            //nome completo do usu�rio
                            'usunome' => $query->row()->usu_nome,
                            //primeiro nome do usuario
                            'usuwho' => $nome,
                            //hr minima de acesso permitida ao usuario
                            'hrentrada' => $query->row()->usu_hr_sis_entrada,
                            //hr maxima de acesso permitida ao usu�rio
                            'hrsaida' => $query->row()->usu_hr_sis_saida,
                            //permiss�es de acesso a menus principais
                            'menu1' => $query->row()->gru_per_menu_nivel1,
                            //permissoes de acesso a menus secundarios
                            'menu2' => $query->row()->gru_per_menu_nivel2,
                            //permissoes de acesso a menus terciarios
                            'menu3' => $query->row()->gru_per_menu_nivel3,
                            //setando o status da sess�o para true(ativa)
                            'status' => true
                        );
                        //joga dados na sess�o
                        $this->session->set_userdata($dadosusu);
                        // redireciona o usu�rio para a tela de inicio
                        redirect(base_url() . 'home');
                    }
                } else {
                    //se a senha for incorreta retorna para a tela de logim com uma mensagem de erro
                    $this->data['ulogin'] = $query->row()->usu_usuario_sis;
                    $this->inicore->setMensagem('error', 'SENHA incorreta, favor verificar e digitar novamente', false);
                }
            } else {
                //caso o usu�rio n�o seja encontrado n�o acessa o sistema e
                //volta para a tela de login com mensagem de erro
                $this->inicore->setMensagem('error', 'Usu�rio inv�lido, por favor verifique', false);
            }
        }
        //caso aconte�a algum erro na hora de logar no sistema, redireciona
        //para a tela de login
        $this->inicore->loadview('login', $this->data);
    }

    //fun��o usada para destruir a sess�o e sair do sistema
    function sair() {
        $this->session->sess_destroy();
        redirect(base_url() . 'login');
    }

}

?>