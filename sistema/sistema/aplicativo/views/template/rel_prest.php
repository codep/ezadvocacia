<script type="text/javascript">
    $(document).ready(function(){
        var base_url = "<?php echo base_url()?>";
        $('.dataMascara').mask('99/99/9999');

        function ocultaTudo(){
            for(var i = 1; i<=4; i++)
            {
                $('#dadosCredor0'+i).hide();
                $('#pessoa0'+i).hide();
                $('#periodo0'+i).hide();
                $('#btgerar0'+i).hide();
                $('input[type="text"]').val('');
            }
            $('#periodo03').show();/*esse campo nao pode ficar oculto, para nao alterar nome do campo eu fiz isso*/
        }
        
        //VALIDA��ES
        $('input[name="cpf_cnpj"]').attr('disabled',true);
        

        ocultaTudo();
        
        var entidade = '';
        var valor = '';
        var i = '';

        $('input[type="radio"]').change(function(e){
            e.preventDefault();

            //IDENTIFICA O RADIO PARA EVITAR PROCESSAMENTO DESNECESS�RIO OU CONFLITO DE VARI�VEIS
            if($(this).attr('name').substring(0,10) == 'cre_filtro')
            {
                entidade = $(this).attr('name');
                valor = $(this).val();

                //SE CLICADO EM PESQUISAR
                if(valor == 'credor_especifico')
                {
                    i = $(this).attr('name').substring(10,12);
                    ocultaTudo();

                    //DESMARCA OS RADIOS EXCETO O MARCADO ATUAL
                    $('input[type="radio"]').each(function(){
                        if($(this).attr('name') != entidade)
                            this.checked = false;
                    });

                    $('#pessoa'+i).fadeIn('fast', function(){
                        //$('#pessoa'+i).change(function(){
                    });

                    //SE CLICADO NO PESSOA F�SICA
                    $('input[name="cre_pessoa"][value="f"]').click(function(){

                        //LIMPANDO OS INPUTS TEXT
                        $('input[type="text"]').val('');

                        //RESETAR CPF/CNPJ
                        $('#cre_cpf_cnpj'+i).html('<input style="width: 124px;" id="credor_cpf_cnpj'+i+'" type="text" name="cpf_cnpj" />');

                        //APLICAR M�SCARA PARA CNPJ
                        $('#credor_cpf_cnpj'+i).attr('disabled',false).mask('999.999.999-99');
                        $('#credor_cpf_cnpj'+i).attr('value', '');

                        $('#dadosCredor'+i).fadeIn('fast');
                        $('#periodo'+i).fadeIn('fast');
                        $('#btgerar'+i).fadeIn('fast');
                    });

                    //SE CLICADO NO PESSOA JUR�DICA
                    $('input[name="cre_pessoa"][value="j"]').click(function(){

                        //LIMPANDO OS INPUTS TEXT
                        $('input[type="text"]').val('');

                        //RESETAR CPF/CNPJ
                        $('#cre_cpf_cnpj'+i).html('<input style="width: 124px;" id="credor_cpf_cnpj'+i+'" type="text" name="cpf_cnpj" />');

                        //APLICAR M�SCARA PARA CNPJ
                        $('#credor_cpf_cnpj'+i).attr('disabled',false).mask('99.999.999/9999-99');
                        $('#credor_cpf_cnpj'+i).attr('value', '');

                        $('#dadosCredor'+i).fadeIn('fast');
                        $('#periodo'+i).fadeIn('fast');
                        $('#btgerar'+i).fadeIn('fast');
                    });
                    

                    
                }
                else if(valor == 'todos')
                {
                    ocultaTudo();
                    i = $(this).attr('name').substring(10,12);
                    //DESMARCA OS RADIOS EXCETO O MARCADO ATUAL
                    $('input[type="radio"]').each(function(){
                        if($(this).attr('name') != entidade)
                            this.checked = false;
                    });

                    $('#periodo'+i).fadeIn('fast');
                    $('#btgerar'+i).fadeIn('fast');
                }
            }

        });

        //LISTANDO OS RESULTADOS DA PESQUISA
        /* *************************************************************************
         * PESQUISA INADIMPLETE OU CREDOR POR NOME OU CPF/CNPJ
         * pesquisaIC($entidade, $pessoa, $tipoDado, $valor)
         * $entidade -------- i=inadimplentes, c=credores
         * $pessoa ---------- j=jur�dica, f=f�sica
         * $tipoDado -------- n=nome, c=cpf/cnpj
         * $valor ----------- valor do nome ou cpf/cnpj
         *
         * NECESS�RIO CONFIGURAR O VALOR DOS ATRIBUTOS NAME DOS INPUTS
         ************************************************************************* */
        $(".dialog-form-open").click(function () {
            //�NDICE DO RELAT�RIO EM
            var indice = $(this).attr('name').substring(3,5);

            //ENTIDADE DO TIPO CREDOR
            var entidade = 'c';

            //RECEBENDO O TIPO DE PESSOA [f ou j]
            var pessoa = '';

            $('input[name="cre_pessoa"]').each(function() {
                    if ( $(this).is(':checked') ) {
                        pessoa = $(this).attr('value');
                    }
            });

            //RECEBENDO O TIPO DE DADOS [credor_nome ou cpf_cnpj]
            var tipodado = '';

            //RECEBENDO O VALOR DIGITADO EM credor_nome ou cpf_cnpj
            var valor = '';
            if(($('#credor_nome'+indice).val() == '') && ($('#credor_cpf_cnpj'+indice).val() == '')){
                $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Para pesquisar � necess�rio digitar o NOME ou CPF/CNPJ</h3></td></tr>');//zera os valores antigos e escreve no elemento o valor selecione
                alert('Digite um valor');
                return false;
            }
            else {
                if ($('#credor_cpf_cnpj'+indice).val() != ''){
                    valor = $('input[name="cpf_cnpj"]').val();
                    tipodado = 'c';
                }

                else if ($('#credor_nome'+indice).val() != ''){
                    valor = $('#credor_nome'+indice).val();
                    tipodado = 'n';
                }
            }


            //FUN��O AJAX
            $.ajax({
                type: "POST",//TIPO DE DADOS
                url: base_url+'credor/listar_pesquisa',//FUN��O DO CONTROLLER PARA TRATAR A LISTAGEM
                data: 'entidade='+entidade+'&pessoa='+pessoa+'&tipodado='+tipodado+'&valor='+valor,//DADOS ENVIADOS
                dataType: 'json',//TIPO DE DADOS A SER RECEBIDO

                //CASO HAJA ERRO DE PROCESSAMENTO
                error: function(xhr, status, er) {
                      alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                },

                //CASO A FUN��O DO CONTROLLER SEJA EXECUTADA COM SUCESSO E OS DADOS RECEBIDOS COM SUCESSO
                success: function(retorno)
                {
                    $("#dialog-form").dialog("open");///ABRE A CAIXA DE DI�LOGO
                    $('#dadosPesquisa').html('');//zera os valores antigos e escreve no elemento o valor selecione
                    vazio = false;//VARI�VEL PARA IDENTIFICAR SE ALGUM DADO �NTEGRO TENHA SIDO RECEBIDO

                    $.each(retorno.resultado, function(i, v){//para cada �ndice retornado executa a fun��o
                        if(v.nome != null){
                          //CONCATENA AS LINHAS DA TABELA COM OS VALORES RECEBIDOS
                          $('#dadosPesquisa').append(
                            '<tr class="tuplas" id="'+unescape(v.codigo)+'">'+
                                '<td>'+unescape(v.nome)+
                                '<td>'+unescape(v.cpf_cnpj)+
                                '<td>'+unescape(v.endereco)+
                                '<td class="last">'+unescape(v.cidade)+
                            '</tr>'
                          );
                        }
                        else vazio = true;//CASO OS DADOS RETORNADOS SEJAM NULOS
                    });
                    $('.tuplas').css('cursor','pointer');//SETA O TIPO DO CURSOR

                    //AO CLICAR SOBRE UMA LINHA DA TABALEA COM A CLASSE TUPLA
                    $('.tuplas').click(function(e){
                        e.preventDefault();
                        codigo = $(this).attr('id');
//                         link = base_url+'credor/editar/cod:'+codigo;
//                         window.open(link, 'blank');
                        $('input[name="cre_cod'+indice+'"]').val(codigo);

                        //ACRESCENTA O VALOR DO NOME DO PRIMEIRO FILHO (TD) DA TR CLICADA
                        $('#credor_nome'+indice).val($(this).find(":eq(0)").text());

                        //ACRESCENTA O VALOR DO CPF_CNPJ DO SEGUNDO FILHO (TD) DA TR CLICADA
                        $('#credor_cpf_cnpj'+indice).val($(this).find(":eq(1)").text())

                        $('.ui-dialog-titlebar-close').trigger('click');
                    });

                    //CASO OS DADOS RETORNADOS SEJAM NULOS
                    if (vazio)
                    {   //EXIBE MENSAGEM
                        $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Nenhum dado encontrado pesquisando por ['+valor+']</h3></td></tr>');
                    }
                }
            });
        });

    });
</script>
<div id="dialog-form" style="padding-left: 5px; padding-right: 5px; " title="Pesquisar dados">
    <p style="text-align: center">
        <strong>Dados encontrados:</strong>
    </p>
    <div id="content" style="width: 750px;">
        <div id="right" style="margin-left: 0px;">
            <div class="box">
                <div class="table" style="padding-left: 0px; font-size: 10px; width: 750px;">
                    <form action="" method="post">
                        <table id="products">
                            <thead>
                                <tr>
                                    <th class="left" style="width: 20%;">Nome</th>
                                    <th style="width: 14%;">CPF/CNPJ</th>
                                    <th>ENDERE�O</th>
                                    <th class="last">CIDADE</th>
                                </tr>
                            </thead>
                            <tbody id="dadosPesquisa">
                                <!--RESULTADO DA PESQUISA AQUI!! -->
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Relat�rios de presta��o de contas</h5>
            </div>
            <?php echo $mensagem ?>
            <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                Presta��o de contas
            </div>
            <form id="form01" action="<?php echo base_url() . 'relatorios/rel_prest01' ?>"  method="post" target="_blank">
                <div class="form" id="rel_prest01">
                    <div class="fields">
                        <div class="field  field-first" style="margin-top:6px;">
                            <div class="divleftlast" style="height:29px; width: 210px;">
                                <div class="label">
                                    <label>Credor:</label>
                                </div>
<!--                                <input style="margin-top: 7px; " disabled type="radio" name="cre_filtro01" id="todosTrava" value="todos" />Todos-->
                                <input type="radio" name="cre_filtro01" value="credor_especifico" />Pesquisar
                            </div>
                            <div class="divleftlast" style="height:29px; width: 200px;" id="pessoa01">
                                <div class="label">
                                    <label>Pessoa:</label>
                                </div>
                                <input style="margin-top: 7px; " type="radio" name="cre_pessoa" value="f"/>F�sica
                                <input type="radio" name="cre_pessoa" value="j" />Jur�dica
                            </div>
                        </div>
                        <div class="field" id="dadosCredor01">
                            <div class="divleft" style="width: 371px;">
                                <div style="width: 49px;" class="label">
                                    <label for="cre_nome">Nome:</label>
                                </div>
                                <div class="input" id="cre_nome01">
                                    <input style="width: 288px;" type="text" id="credor_nome01" name="cre_nome" />
                                    <input type="hidden" name="cre_cod01" value=""/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 231px;">
                                <div style="width: 76px; padding-left: 1px;" class="label">
                                    <label for="cre_cpf_cnpj">CNPJ/CPF:</label>
                                </div>
                                <div class="input" id="cre_cpf_cnpj01">
                                    <input style="width: 124px;" type="text" id="credor_cpf_cnpj01" name="cre_cpf_cnpj01"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <button style="width: 85px; padding: 4px;" name="cre01" type="button" class="dialog-form-open">Pesquisar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field" id="periodo01">
                            <div class="divleft" style="margin-left: 2px; width:150px;">
                                <div style="width: 28px; padding-left: 1px;" class="label">
                                    <label for="de">De:</label>
                                </div>
                                <div style=" width: 109px;" class="input">
                                    <input type="text"  name="de01" class="dataMascara"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="margin-left: 2px; width:150px;">
                                <div style="width: 28px; padding-left: 1px;" class="label">
                                    <label for="ate">At�:</label>
                                </div>
                                <div style=" width: 109px;" class="input">
                                    <input type="text"  name="ate01" class="dataMascara"/>
                                </div>
                            </div>
                        </div>
                        <div class="field" id="btgerar01">
                            <div class="divleftlast" style="text-align: center">
                                <div class="buttons" style="width: 700px">
                                    <div class="highlight" style=" margin: 0 auto">
                                        <input style="width: 185px; margin: 0 auto" type="submit" name="filtrar_rel_prest01" value="Gerar relat�rio" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- *************** Presta��o de contas 03 *************** -->
            <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                Acordos e Previs�es a vencer 
            </div>
            <form id="form03" action="<?php echo base_url() . 'relatorios/rel_prest03' ?>"  method="post" target="_blank">
                <div class="form">
                    <div class="fields" id="rel_adm03">
                        <div class="field  field-first" style="margin-top:6px;">
                            <div class="divleftlast" style="height:29px; width: 210px;">
                                <div class="label">
                                    <label>Credor:</label>
                                </div>
                                <input style="margin-top: 7px; " type="radio" name="cre_filtro03" value="todos" checked />Todos
                                <input type="radio" name="cre_filtro03" value="credor_especifico" />Pesquisar
                            </div>
                            <div class="divleftlast" style="height:29px; width: 200px;" id="pessoa03">
                                <div class="label">
                                    <label>Pessoa:</label>
                                </div>
                                <input style="margin-top: 7px; " type="radio" name="cre_pessoa" value="f" />F�sica
                                <input type="radio" name="cre_pessoa" value="j" />Jur�dica
                            </div>
                        </div>
                        <div class="field" id="dadosCredor03">
                            <div class="divleft" style="width: 371px;">
                                <div style="width: 49px;" class="label">
                                    <label for="cre_nome">Nome:</label>
                                </div>
                                <div class="input" id="cre_nome03">
                                    <input style="width: 288px;" type="text" id="credor_nome03" name="cre_nome" />
                                    <input type="hidden" name="cre_cod03" value=""/>
                                </div>
                            </div>

                            <div class="divleft" style="width: 231px;">
                                <div style="width: 76px; padding-left: 1px;" class="label">
                                    <label for="cre_cpf_cnpj">CNPJ/CPF:</label>
                                </div>
                                <div class="input" id="cre_cpf_cnpj03">
                                    <input style="width: 124px;" type="text" id="credor_cpf_cnpj03" name="cpf_cnpj"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <button style="width: 85px; padding: 4px;" name="cre03" type="button" class="dialog-form-open">Pesquisar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field" id="periodo03">
                            <div class="divleftlast" style="margin-left: 2px; width:150px;">
                                <div style="width: 28px; padding-left: 1px;" class="label">
                                    <label for="ate">At�:</label>
                                </div>
                                <div style=" width: 109px;" class="input">
                                    <input type="text"  name="ate03" class="dataMascara"/>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleftlast" style="text-align: center">
                                <div class="buttons" style="width: 700px">
                                    <div class="highlight" style=" margin: 0 auto">
                                        <input style="width: 185px; margin: 0 auto" type="submit" name="filtrar_rel_prest03" value="Gerar relat�rio" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- *************** FIM Presta��o de contas 03 *************** -->
        </div>
    </div>
</div>