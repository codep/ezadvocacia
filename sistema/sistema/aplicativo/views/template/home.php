<html>
    <body>
        <div id="content">
            <?php echo $sidebar; ?>
            <div id="right">
				<?php /* 
                <div class="box">
                    <div class="title">
                        <h5>Mensagens</h5>
                    </div>
                    <?php echo $mensagem ?>
                    <div id="box-messages">
                        <div class="messages">
                            <?php echo $mensagens==null?'<i>Nenhuma mensagem cadastrada para voc� recentemente</i>':'' ?>
                            <?php foreach ($mensagens as $mens):?>
                            <?php
                                $tipo=$mens->men_tipo;
                                switch ($tipo){
                                    case '1':
                                        $tipo='error';
                                        break;
                                    case '2':
                                        $tipo='warning';
                                        break;
                                    case '3':
                                        $tipo='notice';
                                        break;
                                    case '4':
                                        $tipo='success';
                                        break;
                                }
                               
                            ?>
                                <div id="message-<?php echo $tipo; ?>" class="message message-<?php echo $tipo; ?>">
                                        <div class="image">
                                                <img src="<?php echo $img.'icons/'.$tipo.'.png'?>" alt="<?php echo $tipo; ?>" height="32" />
                                        </div>
                                        <div class="text">
                                                <h6><?php echo utf8_decode($mens->men_titulo); ?></h6>
                                                <span style="width: 600px; text-align: justify"><?php echo utf8_decode($mens->men_texto); ?></span>
                                        </div>
                                        
                                </div>
                            <?php endforeach;?>
                        </div>
                    </div>
	                </div>
					*/ ?>
                    <div class="box">
                        <div class="title">
                            <h5>Tarefas para hoje</h5>
                        </div>
                        <div id="dia">
                            <div class="form">
                                <div class="fields">
                                    <div class="table" style="padding: 0px 5px 10px;">
                                        <?php if ($tarefas!=null): ?>
                                        <table id="products">
                                            <thead>
                                                <tr>
                                                    <th class="left">T�tulo</th>
                                                    <th style="width: 205px">Descri��o</th>
                                                    <th>Agendado</th>
                                                    <th>Executar</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($tarefas as $tarefa): ?>
                                                    <tr>
                                                        <td class="title"><?php echo utf8_decode($tarefa->tar_titulo) ?></td>
                                                        <td><?php echo utf8_decode($tarefa->tar_descricao)?></td>
                                                        <td>Em <?php echo convData($tarefa->tar_criacao,'d')?> por <?php echo $tarefa->tar_criador?></td>
                                                        <td>Em <?php echo convData($tarefa->tar_agendada,'d')?> por <?php echo $tarefa->usu_usuario_sis?></td>
                                                    </tr>
                                                <?php endforeach;?>
                                            </tbody>
                                        </table>
                                        <?php endif; ?>
                                        <?php echo $tarefas==null?'<i>Nenhuma tarefa para voc� hoje</i>':'' ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </body>
</html>