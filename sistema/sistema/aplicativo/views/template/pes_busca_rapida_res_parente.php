
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Pesquisar cobran�as</h5>
            </div>
            <div class="blocoTitulo">
                Resultados da pesquisa por<br/>
                <span style="font-size: 10px"><?php echo $pesquisaPor == 'fone' ? 'Telefone: ' : 'Parente: '; ?><?php echo $valorDigitado; ?></span>
            </div>
            <div class="table">
                <table id="products">
                    <thead>
                        <tr>
                            <th class="left">Cod.</th>
                            <th>Nome</th>
                            <th>Endere�o</th>
                            <th>Cidade</th>
                            <th>Telefone</th>
                            <!-- <th>Parentesco</th> -->
                            <th>Parente</th>
                            <th class="last">Ficha</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($inadimplentes as $inadimplente): ?>
                            <tr>
                                <td><?php echo $inadimplente->ina_cod; ?></td>
                                <td><?php echo $inadimplente->ina_nome; ?></td>
                                <td><?php echo $inadimplente->ina_endereco; ?></td>
                                <td><?php echo $inadimplente->ina_cidade; ?></td>
                                <td><?php echo $inadimplente->ina_foneres . ' - ' . $inadimplente->ina_cel1; ?></td>
                                <!-- <td><?php
                        $a = $valorDigitado;
                        $b = $inadimplente->par_pri_nome;

                        $aux = strpos($b, $a).'';
                        
                        if($inadimplente->par_nome == ''){
                            echo '<i>Parente de primeiro grau</i>';
                        }else{
                            if($aux!=''){
                                echo '<b>' . utf8_decode($inadimplente->par_pri_nome) . '</b> � <i>' . utf8_decode($inadimplente->par_parentesco) . '</i> de <b>' . utf8_decode($inadimplente->ina_pri_nome) . '</b>';
                            }else{
                                echo '<i>Parente de primeiro grau</i>';
                            }
                        }

                        echo $inadimplente->par_nome == '' ? '' : '';
                        
                            ?></td> -->
                                <td><!-- Coluna parente -->
                                    <?php
                                        echo $inadimplente->ina_conj_nome;
                                        if($inadimplente->ina_conj_nome != ''){if($inadimplente->ina_mae_nome != ''){echo ' - '; echo $inadimplente->ina_mae_nome;}}else{echo $inadimplente->ina_mae_nome;}
                                        if($inadimplente->ina_conj_nome != '' || $inadimplente->ina_mae_nome != ''){if($inadimplente->ina_pai_nome != ''){echo ' - '; echo $inadimplente->ina_pai_nome;}}else{echo $inadimplente->ina_pai_nome;}
                                        if($inadimplente->ina_conj_nome != '' || $inadimplente->ina_mae_nome != '' || $inadimplente->ina_pai_nome){if($inadimplente->par_nome != ''){echo ' - '; echo $inadimplente->par_nome;}}else{echo $inadimplente->par_nome;}
                                    ?>
                                </td><!-- Fim coluna parente -->
                                <td class="last"><a href="<?php echo base_url() . 'inadimplente/editar/cod:' . $inadimplente->ina_cod ?>">Visualizar</a>  </td>
                            </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
