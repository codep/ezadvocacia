<script type="text/javascript">
        $(document).ready(function(){
            $('.visualizar').click(function(e){
                    window.location.href='http://webhost/cobranca/divida/detalhes';
            });
            $('input[value="Ficha"]').click(function(e){
                    window.location.href='http://webhost/cobranca/divida/ficha';
            });

            if(<?php echo isset ($aba)?>){
                $('a[href="<?php echo $aba?>"]').trigger('click');
            }

        });
</script>
<div id="content">

<?php echo $sidebar; ?>

    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>D�vidas judiciais</h5>
                <ul class="links">
                    <li><a href="#soliDoc">Solicitar DOC</a></li>
                    <li><a href="#aguardando">Aguardando</a></li>
                    <li><a href="#ajuizado">Ajuizado</a></li>
                    <li><a href="#andamento">Andamento</a></li>
                    <li><a href="#acordos">Acordos</a></li>
                    <li><a href="#atraso">Atraso</a></li>
                </ul>
            </div>
            <div id="soliDoc">
                <div class="form">
                    <div class="fields">
                        <form action="<?php echo base_url().'judicial/listar' ?>" method="post">
                            <div class="field  field-first">
                                <div class="divleft" style="width: 286px;">
                                    <div style="width: 69px; padding-left: 1px;" class="label">
                                        <label>Filtro:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 188px;" type="text" name="filtro"/>
                                        <input type="hidden" name="aba" value="#soliDoc"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 87px; margin: 0px;">
                                    <div class="buttons">
                                        <div class="highlight">
                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="table" style="padding: 0px 5px 10px;">
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th class="left" style="width: 30px;">Cod.</th>
                                        <th>Inadimplente</th>
                                        <th>Credor</th>
                                        <th>Valor</th>
                                       <th>�ltimo RO</th>
                                        <th class="last">Visualizar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($solicitandoDocs as $reg): ?>
                                        <tr>
                                            <td class="title" style="width: 30px;"><?php echo $reg->cob_cod;?></td>
                                            <td><?php echo $reg->ina_nome;?></td>
                                            <td><?php echo $reg->cre_nome_fantasia;?></td>
                                            <td style="min-width: 85px!important;"><?php echo convMoney($reg->div_total);?></td>
                                            <td>
                                                <?php echo convData($reg->ro_data,'d');?>
                                            </td>
                                            <td class="last">
                                                <a href="<?php echo base_url().'divida/ficha/cod:'.$reg->ina_cod; ?>" style="text-decoration: none;" >
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="Ficha" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="aguardando">
                <div class="form">
                    <div class="fields">
                        <form action="<?php echo base_url().'judicial/listar' ?>" method="post">
                            <div class="field  field-first">
                                <div class="divleft" style="width: 286px;">
                                    <div style="width: 69px; padding-left: 1px;" class="label">
                                        <label>Filtro:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 188px;" type="text" name="filtro"/>
                                        <input type="hidden" name="aba" value="#aguardando"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 87px; margin: 0px;">
                                    <div class="buttons">
                                        <div class="highlight">
                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="table" style="padding: 0px 5px 10px;">
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th class="left" style="width: 30px;">Cod.</th>
                                        <th>Inadimplente</th>
                                        <th>Credor</th>
                                        <th>Valor</th>
                                        <th>�ltimo RO</th>
                                        <th class="last">Visualizar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($aguardando as $reg): ?>
                                    <tr>
                                        <td class="title" style="width: 30px;"><?php echo $reg->cob_cod;?></td>
                                        <td><?php echo $reg->ina_nome;?></td>
                                        <td><?php echo $reg->cre_nome_fantasia;?></td>
                                        <td><?php echo convMoney($reg->div_total);?></td>
                                        <td>
                                            <?php echo convData($reg->ro_data,'d').' - '.$reg->ro_nome;?>
                                        </td>
                                        <td class="last">
                                            <a href="<?php echo base_url().'divida/ficha/cod:'.$reg->ina_cod; ?>" style="text-decoration: none;" >
                                                <div class="buttons">
                                                    <div class="highlight">
                                                        <input style="width: 85px;" type="submit" name="Ficha" value="Ficha" />
                                                    </div>
                                                </div>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ajuizado">
                <div class="form">
                    <div class="fields">
                        <form action="<?php echo base_url().'judicial/listar' ?>" method="post">
                            <div class="field  field-first">
                                <div class="divleft" style="width: 286px;">
                                    <div style="width: 69px; padding-left: 1px;" class="label">
                                        <label>Filtro:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 188px;" type="text" name="filtro"/>
                                        <input type="hidden" name="aba" value="#ajuizado"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 87px; margin: 0px;">
                                    <div class="buttons">
                                        <div class="highlight">
                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="table" style="padding: 0px 5px 10px;">
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th class="left" style="width: 30px;">Cod.</th>
                                        <th>Inadimplente</th>
                                        <th>Credor</th>
                                        <th>Valor</th>
                                        <th>�ltimo RO</th>
                                        <th class="last">Visualizar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($ajuizado as $reg): ?>
                                        <tr>
                                            <td class="title" style="width: 30px;"><?php echo $reg->cob_cod;?></td>
                                            <td><?php echo $reg->ina_nome;?></td>
                                            <td><?php echo $reg->cre_nome_fantasia;?></td>
                                            <td><?php echo convMoney($reg->div_total);?></td>
                                            <td>
                                                <?php echo convData($reg->ro_data,'d').' - '.$reg->ro_nome;?>
                                            </td>
                                            <td class="last">
                                                <a href="<?php echo base_url().'divida/ficha/cod:'.$reg->ina_cod; ?>" style="text-decoration: none;" >
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="Ficha" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="andamento">
                <div class="form">
                    <div class="fields">
                        <form action="<?php echo base_url().'judicial/listar' ?>" method="post">
                            <div class="field  field-first">
                                <div class="divleft" style="width: 286px;">
                                    <div style="width: 69px; padding-left: 1px;" class="label">
                                        <label>Filtro:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 188px;" type="text" name="filtro"/>
                                        <input type="hidden" name="aba" value="#andamento"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 87px; margin: 0px;">
                                    <div class="buttons">
                                        <div class="highlight">
                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="table" style="padding: 0px 5px 10px;">
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th class="left" style="width: 30px;">Cod.</th>
                                        <th>Inadimplente</th>
                                        <th>Credor</th>
                                        <th>Valor</th>
                                        <th>Vencimento</th>
                                        <th class="last">Visualizar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($andamento as $reg): ?>
                                        <tr>
                                            <td class="title" style="width: 30px;"><?php echo $reg->cob_cod;?></td>
                                            <td><?php echo $reg->ina_nome;?></td>
                                            <td><?php echo $reg->cre_nome_fantasia;?></td>
                                            <td><?php echo convMoney($reg->div_total);?></td>
                                            <td>
                                                <?php echo convData($reg->ro_data,'d').' - '.$reg->ro_nome;?>
                                            </td>
                                            <td class="last">
                                                <a href="<?php echo base_url().'divida/ficha/cod:'.$reg->ina_cod; ?>" style="text-decoration: none;" >
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="Ficha" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="acordos">
                <div class="form">
                    <div class="fields">
                        <form action="<?php echo base_url().'judicial/listar' ?>" method="post">
                            <div class="field  field-first">
                                <div class="divleft" style="width: 286px;">
                                    <div style="width: 69px; padding-left: 1px;" class="label">
                                        <label>Filtro:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 188px;" type="text" name="filtro"/>
                                        <input type="hidden" name="aba" value="#acordos"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 87px; margin: 0px;">
                                    <div class="buttons">
                                        <div class="highlight">
                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="table" style="padding: 0px 5px 10px;">
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th class="left" style="width: 30px;">Cod.</th>
                                        <th>Inadimplente</th>
                                        <th>Credor</th>
                                        <th>Valor</th>
                                       <th>�ltimo RO</th>
                                        <th class="last">Visualizar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($acordos as $reg): ?>
                                    <tr>
                                        <td class="title" style="width: 30px;"><?php echo $reg->cob_cod?></td>
                                        <td><?php echo $reg->ina_nome?></td>
                                        <td><?php echo $reg->cre_nome_fantasia?></td>
                                        <td><?php echo convMoney($reg->aco_valor_atualizado);?></td>
                                        <td>
                                            <?php echo convData($reg->ro_data,'d');?>
                                        </td>
                                        <td class="last">
                                            <a href="<?php echo base_url().'divida/ficha/cod:'.$reg->ina_cod; ?>" style="text-decoration: none;" >
                                                <div class="buttons">
                                                    <div class="highlight">
                                                        <input style="width: 85px;" type="submit" name="Ficha" value="Ficha" />
                                                    </div>
                                                </div>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="atraso">
                <div class="form">
                    <div class="fields">
                        <form action="<?php echo base_url().'judicial/listar' ?>" method="post">
                            <div class="field  field-first">
                                <div class="divleft" style="width: 286px;">
                                    <div style="width: 69px; padding-left: 1px;" class="label">
                                        <label>Filtro:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 188px;" type="text" name="filtro"/>
                                        <input type="hidden" name="aba" value="#atraso"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 87px; margin: 0px;">
                                    <div class="buttons">
                                        <div class="highlight">
                                            <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="table" style="padding: 0px 5px 10px;">
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th class="left" style="width: 30px;">Cod.</th>
                                        <th>Inadimplente</th>
                                        <th>Credor</th>
                                        <th>Valor</th>
                                        <th>Vencimento</th>
                                        <th class="last">Visualizar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($acordosAtr as $reg): ?>
                                        <tr>
                                            <td class="title" style="width: 30px;"><?php echo $reg->cob_cod;?></td>
                                            <td><?php echo $reg->ina_nome;?></td>
                                            <td><?php echo $reg->cre_nome_fantasia;?></td>
                                            <td><?php echo convMoney($reg->aco_valor_atualizado);?></td>
                                            <td>
                                                <?php foreach ($parcelas[$reg->cob_cod] as $venc): ?>
                                                <?php echo convData($venc->paa_vencimento,'d') ?>
                                                <?php endforeach;?>
                                            </td>
                                            <td class="last">
                                                <a href="<?php echo base_url().'divida/ficha/cod:'.$reg->ina_cod; ?>" style="text-decoration: none;" >
                                                    <div class="buttons">
                                                        <div class="highlight">
                                                            <input style="width: 85px;" type="submit" name="Ficha" value="Ficha" />
                                                        </div>
                                                    </div>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>