<script type="text/javascript">
    $(document).ready(function(){
        $('#valor').mask('99.99');
        $('.cadastrar').click(function(e){
            e.preventDefault()
            if($('input[name="nome"]').val() == ''){
                $('input[name="nome"]').focus();
                return false;
            }else if($('input[name="valor"]').val() == ''){
                $('input[name="valor"]').focus();
                return false;
            }else {
                $('#formCad').submit();
            }
        });
    });
</script>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Cadastro de repasse</h5>
            </div>
            <?php echo $mensagem; ?>
            <form id="formCad" action="<?php echo base_url() . 'cadrepasse/incluir' ?>" method="post">
                <div class="form">
                    <div class="fields">
                        <div class="field  field-first">
                            <div class="divleft" style="width: 428px;">
                                <div style="width: 150px; padding-left: 1px;" class="label">
                                    <label for="nome">Novo tipo de repasse:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 246px;" maxlength="28" type="text" id="nome" name="nome"/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 156px;">
                                <div style="width: 39px; padding-left: 1px;" class="label">
                                    <label for="valor">(%):</label>
                                </div>
                                <div class="input">
                                    <input style="width: 88px;" type="text" id="valor" name="valor"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 107px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <input style="width: 105px;" class="cadastrar" type="submit" name="cadastrar" value="Cadastrar" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 11px;" class="blocoTitulo">
                            Tipos de repasse cadastrados
                        </div>
                        <div style="width: 710px; min-height: 700px; margin: 27px 0px 0px 5px;">
                            <?php foreach ($repasses as $repasse): ?>
                                <p class="opitem2" style="width: 700px;<?php echo $repasse->rep_cod == $ultimoRepasse->rep_cod ? 'font-weight: bold;' : '' ?>"><?php echo utf8_decode($repasse->rep_nome) . ' (' . substr($repasse->rep_valor, '0', '2') . '%)' ?></p>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
