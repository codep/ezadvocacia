<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img . 'logo_elisangela.jpg'; ?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br|www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo $title; ?></p>
        </div>
    </div>
    <?php foreach ($credores as $credor): ?>
    <h2 style="font-size: 14pt;"><b>Credor:&nbsp;</b><?php echo $credor->cre_cpf_cnpj; ?> - <?php echo $credor->cre_nome_fantasia; ?></h2><br/>
        <?php foreach ($recuperadoresComRo[$credor->credor_cre_cod] as $recuperador) : ?>
            <b>Recuperador: </b><i><?php echo $recuperador->usu_nome ?></i><br/><br/>
            <table cellpadding="0" cellspacing="0" style="width: 100%; text-align: center">
                <thead>
                    <tr>
                        <th>Data / Hora</th>
                        <th>Opera��o</th>
                        <th>CPF/CNPJ</th>
                        <th>Inadimplente</th>
                        <th class="last">Observa��o</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0;
                    foreach ($roUsuariosCredor[$credor->credor_cre_cod . $recuperador->usuarios_usu_cod] as $ro): ?>
                        <tr>
                            <td class="tdInaNome">&nbsp;<?php echo convData($ro->ros_data, 'd').' - '.$ro->ros_hora ?>&nbsp;&nbsp;</td>
                            <td class="tdCreNomeFantasia"><?php echo $ro->ope_nome ?></td>
                            <td class="tdCreNomeFantasia"><?php echo $ro->ina_cpf_cnpj ?></td>
                            <td class="td3"><?php echo utf8_decode(ucwords(strtolower($ro->ina_nome))) ?></td>
                            <td class="td4 last">
            <?php echo utf8_decode(strtolower($ro->ros_detalhe)) ?>
                            </td>
                        </tr>

            <?php $i++;
        endforeach; ?>
                    <tr>
                        <td id="tdTotalA" colspan="4">Total de opera��es</td>
                        <td id="tdTotalB" class="last"><?php echo $i ?></td>
                    </tr>
                </tbody>
            </table>
            <br/>
            <hr/>
    <?php endforeach; ?>
<?php endforeach; ?>
</div>
