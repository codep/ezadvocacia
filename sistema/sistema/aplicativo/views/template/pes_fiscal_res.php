<script type="text/javascript">
    $(document).ready(function(){

        $('.editaInad').click(function(){
                cod = $(this).attr('id');
                window.location.href="<?php echo base_url().'divida/ficha/cod:';?>"+cod;
        });
        
		$('input#search').quicksearch('#products tbody tr:not(.titulo)', {});
/*
		//AUTO FOCUS
		window.setTimeout(function(){
			$('input#search').focus();	
		},300);
*/	        
        
        $(".dataMascara").datepicker();
        
        //-------------- STATUS FISCAL
    	$('.btStatus').click(function(e){
			e.preventDefault();
			$bt = $(this);
	        $("#dialog-form22").dialog("open");
        	var campos = ['cob_cod','fiscal_status'];
        	$.each(campos,function(i,v){
        		if ($("#dialog-form22 input[name="+v+"]").attr('type')=='radio') {
        			$("#dialog-form22 input[name="+v+"][value='"+$bt.attr('data-'+v)+"']").prop('checked',true);
        		} else {
        			$("#dialog-form22 input[name="+v+"]").val($bt.attr('data-'+v));
        		}
        	});
        	return false;
	     });
        $('#formStatusFiscal').submit(function(){
        	var $form = $(this);
        	var validacao = true;
        	var campos = ['input[name=fiscal_status]'];
        	$.each(campos,function(i,v){
	            if($(v).val() == '')
	            {
	            	alert('Selecione uma op��o!');
	                $(v).focus();
	                return validacao = false;
	            }
        	});
        	if (validacao==true) {
	            $.ajax({
	                type: $form.attr('method'),
	                url: $form.attr('action'),
	                data: $form.serialize(),
	                dataType: 'json',
	                error: function(xhr, status, er) {
	                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
	                },
	                success: function(retorno)
	                {
	                	//console.log(retorno);
	                	if (retorno.status==true || retorno.status==1) {
	                		$bt = $('.btStatus[data-cob_cod="'+retorno.cob_cod+'"]');
	                		$bt.attr('data-fiscal_status',retorno.fiscal_status);
	                		$bt.find('.square').attr('class','square '+retorno.fiscal_status_class);
	                		//ATUALIZANDO O DATA DO BOT�O
                			$("#dialog-form22").dialog('close');
	                	} else {
	                		alert('Ocorreu um erro durante a atualiza��o dos dados, verifique e tente novamente.');
	                	}
	                }
	            });        		
        	}
			return false;
    	});
        
        //-------------- DATA FISCAL
    	$('.btData').click(function(e){
			e.preventDefault();
			$bt = $(this);
	        $("#dialog-form21").dialog("open");
        	var campos = ['cob_cod','fiscal_data'];
        	$.each(campos,function(i,v){
        		if ($bt.attr('data-'+v)=='//') $bt.attr('data-'+v,'');
        		$("#dialog-form21 input[name="+v+"]").val($bt.attr('data-'+v));
        	});
        	return false;
	     });
        $('#formDataFiscal').submit(function(){
        	var $form = $(this);
        	var validacao = true;
        	var campos = ['input[name="fiscal_data"]'];
        	$.each(campos,function(i,v){
	            if($(v).val() == '')
	            {
	            	alert('Todos os campos s�o obrigat�rios');
	                $(v).focus();
	                return validacao = false;
	            }
        	});
        	if (validacao==true) {
	            $.ajax({
	                type: $form.attr('method'),
	                url: $form.attr('action'),
	                data: $form.serialize(),
	                dataType: 'json',
	                error: function(xhr, status, er) {
	                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
	                },
	                success: function(retorno)
	                {
	                	//console.log(retorno);
	                	if (retorno.status==true || retorno.status==1) {
	                		$bt = $('.btData[data-cob_cod="'+retorno.cob_cod+'"]');
	                		$bt.attr('data-fiscal_data',retorno.fiscal_data);
	                		$('.fiscal_data[data-cob_cod="'+retorno.cob_cod+'"]').html(retorno.fiscal_data);
	                		//console.log($bt);
	                		//ATUALIZANDO O DATA DO BOT�O
                			$("#dialog-form21").dialog('close');
	                	} else {
	                		alert('Ocorreu um erro durante a atualiza��o dos dados, verifique e tente novamente.');
	                	}
	                }
	            });        		
        	}
			return false;
    	});
        
    });
</script>
<style>
	.squarebox { display:block; padding: 5px; clear: both; }
	.squarebox input[type="radio"] { width: 15px !important; margin-right: 5px !important; }
	.square {
		width: 16px;
		height: 16px;
		display: block;
		float: left;
		margin-right: 5px;
		border: 1px dotted #ccc;
	}
	.square.all { background-color: #ffffff; }
	.square.ado { background-color: #ffd95c; }
	.square.dco { background-color: #b4eeb4; }
	.square.din { background-color: #00bfff; }
	.square.dca { background-color: #f9821b; }
	.square.dia { background-color: #b23aee; }
	.square.pes { background-color: #800000; }
	.square.ade { background-color: #d4d4d4; }
	.square.gbd { background-color: #836476; }
	#dialog-form21 { display: none; }
</style>
<!-- Caixa de di�logo Data Fiscal -->
<div id="dialog-form21" class="form" style="padding-left: 5px; padding-right: 5px;" title="Data Fiscal">
    <form id="formDataFiscal" action="<?php echo base_url() . 'pesquisar/fiscal_setData'; ?>" method="post">
    	<input type="hidden" name="cob_cod" value="" />
        <div class="fields" style="width: 700px; margin: 0 auto; padding-top: 15px;">
            <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                Informa��es para Data Fiscal
            </div>
            <div class="field" style="padding: 2px; height: 55px;">
                <div class="divleftlast" style="width: 400px;">
                    <div style="width: 150px; padding-left: 1px;" class="label">
                        <label for="fiscal_data">Data Fiscal:</label>
                    </div>
                    <div class="input" style="width: 100px;">
                    	<input style="width: 100px;" type="text" name="fiscal_data" class="dataMascara" />
                    </div>
                </div>
            </div>
            <div style="text-align: center; margin-top: 10px; margin-bottom: 10px;" class="buttons">
                <div style="margin-left: 7px;" class="highlight">
                    <input type="submit" name="submit.highlight" value="Atualizar"/>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- FIM Caixa de di�logo Data Fiscal -->
<!-- Caixa de di�logo Status Fiscal -->
<div id="dialog-form22" class="form" style="padding-left: 5px; padding-right: 5px;" title="Status Fiscal">
    <form id="formStatusFiscal" action="<?php echo base_url() . 'pesquisar/fiscal_setStatus'; ?>" method="post">
    	<input type="hidden" name="cob_cod" value="" />
        <div class="fields" style="width: 700px; margin: 0 auto; padding-top: 15px;">
            <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                Altera��o de Status Fiscal
            </div>
            <div class="field" style="padding: 2px; height: 355px;">
                <div class="divleftlast" style="width: 300px;">
                    <div style="width: 150px; padding-left: 1px;" class="label">
                        <label for="fiscal_status">Status Fiscal:</label>
                    </div>
                    <div class="" style="width: 400px;">
						<span class="squarebox"><input type="radio" id="fiscal_status_all" name="fiscal_status" value="0" /><span class="square all"></span><label for="fiscal_status_all">Cobran�a sem status fiscal </label></span>
						<span class="squarebox"><input type="radio" id="fiscal_status_ado" name="fiscal_status" value="1" /><span class="square ado"></span><label for="fiscal_status_ado">Aguardando Documento</label></span>
						<span class="squarebox"><input type="radio" id="fiscal_status_dco" name="fiscal_status" value="2" /><span class="square dco"></span><label for="fiscal_status_dco">Documento Completo</label></span>
						<span class="squarebox"><input type="radio" id="fiscal_status_din" name="fiscal_status" value="3" /><span class="square din"></span><label for="fiscal_status_din">Documento Incompleto</label></span>
						<span class="squarebox"><input type="radio" id="fiscal_status_dca" name="fiscal_status" value="4" /><span class="square dca"></span><label for="fiscal_status_dca">Documento Completo com Acordo</label></span>
						<span class="squarebox"><input type="radio" id="fiscal_status_dia" name="fiscal_status" value="5" /><span class="square dia"></span><label for="fiscal_status_dia">Documento Incompleto com Acordo</label></span>
						<span class="squarebox"><input type="radio" id="fiscal_status_pes" name="fiscal_status" value="6" /><span class="square pes"></span><label for="fiscal_status_pes">Para Estornar</label></span>
						<span class="squarebox"><input type="radio" id="fiscal_status_ade" name="fiscal_status" value="7" /><span class="square ade"></span><label for="fiscal_status_ade">Aguardando Devolu��o</label></span>
						<span class="squarebox"><input type="radio" id="fiscal_status_gbd" name="fiscal_status" value="8" /><span class="square gbd"></span><label for="fiscal_status_gbd">Gerado Border� de Devolu��o</label></span>
                    </div>
                </div>
            </div>
            <div style="text-align: center; margin-top: 10px; margin-bottom: 10px;" class="buttons">
                <div style="margin-left: 7px;" class="highlight">
                    <input type="submit" name="submit.highlight" value="Atualizar"/>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- FIM Caixa de di�logo Status Fiscal -->
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div class="box">
            <div class="title">
                <h5>Pesquisar Cobran�as com Status Fiscal</h5>
            </div>
            <?php if (!empty($credor)) : ?>
            <h3 style="text-align: center;"><?php echo $credor->cre_nome_fantasia; ?></h3>
            <?php endif; ?>
            <div class="blocoTitulo">
                <span style="font-size: 10px">Filtro: <input type="text" id="search" style="font-size: 18px; padding: 5px; width: 500px;" /></span>
            </div>
            <?php
            	$rowTipos='';
            ?>
            <div class="table">
                <table id="products">
                    <thead>
                        <tr>
                        	<?php if (empty($credor)) : ?>
                        	<th>Credor</th>
                        	<?php endif; ?>
                            <th>Inadimplente</th>
                            <th>Data</th>
                            <th width="100">Valor Total</th>
                            <th width="100"></th>
                        </tr>
                    </thead>
                    <tbody>                        
                    	<?php foreach ($cobrancas as $ina => $ina_cobrancas): ?>
                		<?php
                			//CONTAGEM DE TIPOS
                			if (!isset($rowTipos[$ina_cobrancas['fiscal_status']])) $rowTipos[$ina_cobrancas['fiscal_status']]=0;
							$rowTipos[$ina_cobrancas['fiscal_status']]++;
            			?>
                        <tr>
                        	<?php if (empty($credor)) : ?>
                        	<td><?php echo utf8_decode($ina_cobrancas['cre_nome']); ?> </td>
                        	<?php endif; ?>
                        	<td style="text-align: left;">
                        		<a class="btStatus tooltip" data-cob_cod="<?php echo implode(',',$ina_cobrancas['codigos']); ?>" data-fiscal_status="<?php echo $ina_cobrancas['fiscal_status']; ?>" href="" title="<?php echo $ina_cobrancas['fiscal_status_desc']; ?>"><span class="square <?php echo $ina_cobrancas['fiscal_status_class']; ?>"></span></a>
                    		<?php echo utf8_decode($ina_cobrancas['ina_nome']); ?> </td>
                        	<td><a class="btData fiscal_data" data-cob_cod="<?php echo implode(',',$ina_cobrancas['codigos']); ?>" data-fiscal_data="<?php echo convDataBanco($ina_cobrancas['fiscal_data']); ?>" href=""><?php echo !empty($ina_cobrancas['fiscal_data']) ? convDataBanco($ina_cobrancas['fiscal_data']) : '00/00/0000'; ?></a></td>
                            <td>
                            	<?php echo convMoney($ina_cobrancas['valor_total']); ?> (<?php echo count($ina_cobrancas['codigos']); ?>)
                            	<br /><?php echo implode(',',$ina_cobrancas['setores']); ?>
                        	</td>
                            <td class="last">
                                <div class="buttons">
                                	<?php //echo implode(',',$ina_cobrancas['codigos']); //DEBUG ?>
                                    <input style="width: 40px;" class="visualizar" onclick="window.location.href='<?php echo base_url().'ro/novo/cod:'. (implode('-',$ina_cobrancas['codigos'])) .'-'; ?>'; return false;" type="button" value="RO" />
                                    <input style="width: 55px;" class="visualizar" onclick="window.location.href='<?php echo base_url().'divida/ficha/cod:'.$ina_cobrancas['ina_cod']; ?>'; return false;" type="button" value="FICHA" />
                                </div>
                            </td>
                        </tr>
                        <?php endforeach;?>                
                    </tbody>
                    <tfoot>
                    	<tr>
                    		<td colspan="<?php echo empty($credor)?'3':'2'; ?>" style="text-align: right;">Total:</td>
                    		<td><?php echo convMoney($valor_geral); ?></td>
                    	</tr>
                    </tfoot>
                </table>
                <div class="pagination pagination-left">
                    <div class="results">
                        <span>Total de <?php echo count($cobrancas); ?> inadimplentes encontrados</span>
                    </div>
                </div>
                <br/>
                <h4 style="margin: 0px; margin-bottom: 5px;">Status Fiscal:</h4>
                <?php $fiscal_status_arr = array( 
                	array('codigo'=>0,'desc'=>'Cobran�a sem status fiscal','class'=>'all'),
                	array('codigo'=>1,'desc'=>'Aguardando Documento','class'=>'ado'),
                	array('codigo'=>2,'desc'=>'Documento Completo','class'=>'dco'),
                	array('codigo'=>3,'desc'=>'Documento Incompleto','class'=>'din'),
                	array('codigo'=>4,'desc'=>'Documento Completo com Acordo','class'=>'dca'),
                	array('codigo'=>5,'desc'=>'Documento Incompleto com Acordo','class'=>'dia'),
                	array('codigo'=>6,'desc'=>'Para Estornar','class'=>'pes'),
                	array('codigo'=>7,'desc'=>'Aguardando Devolu��o','class'=>'ade'),
                	array('codigo'=>8,'desc'=>'Gerado Border� de Devolu��o','class'=>'gbd'),
				);
				?>
				<?php foreach($fiscal_status_arr as $fs) : ?>
				<span class="squarebox"><span class="square <?php echo $fs['class']; ?>"></span><?php echo $fs['desc']; ?> (<?php echo isset($rowTipos[$fs['codigo']]) ? $rowTipos[$fs['codigo']] : 0; ?>)</span>
				<?php endforeach; ?>
                <br/>
            </div>
<!--            <form id="form" action="">-->
                <div class="form">
                    <div class="fields">
                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <a href="<?php echo base_url().'pesquisar/fiscal' ?>" style="text-decoration: none;" >
                                <div style="margin-left: 7px;" class="highlight">
                                    <input type="submit" name="submit.highlight" value="Nova pesquisa" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
<!--            </form>-->
        </div>
    </div>
</div>
