<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img.'logo_elisangela.jpg';?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br|www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo $title ;?></p>
        </div>
    </div>
    <table id="tableJur03" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="thInadimplente">Inadimplente</th>
                <th id="thCredor">Credor</th>
                <th id="th3">Tipo</th>
                <th id="th4">Data �ltimo RO</th>
                <th id="th5" class="last">�ltimo RO</th>
            </tr>
        </thead>
        <tbody>
            <?php $totalDeInad = 0; foreach ($cobsJudOciosas as $cobJudOciosa) : ?>
            <tr>
           
                <td class="tdInaNome"><?php echo utf8_decode($cobJudOciosa->ina_nome); ?></td>
                <td class="tdCreNomeFantasia"><?php echo utf8_decode($cobJudOciosa->cre_nome_fantasia); ?></td>
                <td class="td3"><?php echo utf8_decode($cobJudOciosa->tipo); ?></td>
                <td class="td4">
                            <?php
                              $dataUltRo = convDataBanco($cobJudOciosa->data_ult_ro);
                              echo ($dataUltRo == "//") ? "" : $dataUltRo; //se a data vier vazia o m�todo convDataBanco retorna s� as barra, se isso acontecer imprime nada
                              echo("<br/>");
                              echo $cobJudOciosa->hora_ult_ro;
                            ?><br/>
                </td>
                <td class="td5 last"><?php echo  $cobJudOciosa->ult_ros_detalhe == null ? 'VIRGEM' : $cobJudOciosa->ult_ros_detalhe;?></td>
                <?php $totalDeInad ++; endforeach; ?>
            </tr>
            <tr>
                <td colspan="4" id="tdTotalA">TOTAL DE COBRAN�AS EM ATRASO</td>
                <td id="tdTotalB" class="last"><?php echo "$totalDeInad"; ?></td>
            </tr>

        </tbody>
    </table>
</div>
