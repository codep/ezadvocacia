<script type="text/javascript">
    $(document).ready(function(){
        var base_url = "<?php echo base_url() ?>";
        //OCULTANDO OS DADOS A SEREM EXIBIDOS AP�S A ESCOLHA DO TIPO DE PESSOA [f] ou [j]
        //$('#dadosCredor').hide();
        $('#dadosInadimplente').hide();
        //$('#repasses').hide();
        //$('#dadosDivida').hide();

        //INADIMPLENTE: AO CLICAR EM UM TIPO DE PESSOA
        $('input[name="ina_pessoa"]').click(function() {

            //RESETAR CAMPOS
            $('#ina_nome').html('<input style="width: 248px;" type="text" id="inad_nome" name="ina_nome"/>');
            $('#ina_cpf_cnpj').html('<input style="width: 124px;" type="text" id="inad_cpf_cnpj" name="ina_cpf_cnpj"/>');
            $('#hiddenInadimplente').html('<input type="hidden" name="ina_cod" value=""/>');

            //EFEITO DE EXIBI��O
            $('#dadosInadimplente').fadeIn(function(){
                //EXIBE OS DADOS DO INADIMPLENTE
            });

            //SE FOR PESSOA F�SICA
            if($(this).attr('value') == 'f')
            //SETA A M�SCARA DE CPF
                $('input[name="ina_cpf_cnpj"]').mask('999.999.999-99');
            //SE FOR PESSOA JUR�DICA
            else if($(this).attr('value') == 'j')
            //SETA A M�SCARA DE CNPJ
                $('input[name="ina_cpf_cnpj"]').mask('99.999.999/9999-99');

        });


















        //========================== VALIDA��O DE FORMULARIO ===========================
        $('#formDiv').submit(function(){


            //            var pessoaVazio = true;
            //            $('input[name="cre_pessoa"]').each(function() {
            //                if ( $(this).is(':checked') ) {
            //                    pessoaVazio = false;
            //                }
            //            });

            
            //            if($('input[name="cre_nome"]').val() == ''){
            //                $('input[name="cre_nome"]').focus();
            //                return false;
            //            }

            var pessoaInaVazio = true;
            $('input[name="ina_pessoa"]').each(function() {
                if ( $(this).is(':checked') ) {
                    pessoaInaVazio = false;
                }
            });

            //            if(pessoaInaVazio){alert('validando');
            //                alert('Indique pessoa F�SICA ou JUR�DICA para o INADIMPLENTE.');
            //                return false;
            //            }
            if($('input[name="ina_nome"]').val() == ''){
                alert('Digite algo para pesquisar.');
                $('input[name="ina_nome"]').focus();
                return false;
            }

            //            else if($('input[name="emissao"]').val() == ''){
            //                $('input[name="emissao"]').focus();
            //                return false;
            //            }

            //            else if($('input[name="qtd_parcela"]').val() == ''){
            //                $('input[name="qtd_parcela"]').focus();
            //                return false;
            //            }

            //            else if($('input[name="venc_inicial"]').val() == ''){
            //                $('input[name="venc_inicial"]').focus();
            //                return false;
            //            }

            //            else if($('input[name="valor_parc"]').val() == ''){
            //                $('input[name="valor_parc"]').focus();
            //                return false;
            //            }

            //            else if($('#repassesCredor option:selected').val() == ''){
            //                alert('Selecione o valor de REPASSE');
            //                return false;
            //            }


            //            else if($('input[name="num_par[]"]').val() == ''){
            //                $('input[name="num_par[]"]').focus();
            //                return false;
            //            }
            //            else if($('input[name="valor_par[]"]').val() == ''){
            //                $('input[name="valor_par[]"]').focus();
            //                return false;
            //            }
            //            else if($('input[name="venc_par[]"]').val() == ''){
            //                $('input[name="venc_par[]"]').focus();
            //                return false;
            //            }
            //            else if (ok==false){
            //                alert('Confira as parcelas da d�vida, e/ou clique no bot�o gerar');
            //                return false
            //            }

            //            else{
            //
            //            }

            $(".parcelas_divida").each(function() {

            });


        });

        $('#cancelar').click(function(){
            var resposta = confirm('Tem certeza de que deseja cancelar? \nTODOS OS DADOS DIGITADOS SER�O PERDIDOS!');

            if(resposta){
                location.reload();
            }
            else
                return resposta;
        });
        //======================= FIM DA VALIDA��O DE FORMULARIO =======================











        //LISTANDO OS RESULTADOS DA PESQUISA
        /* *************************************************************************
         * PESQUISA INADIMPLETE OU CREDOR POR NOME OU CPF/CNPJ
         * pesquisaIC($entidade, $pessoa, $tipoDado, $valor)
         * $entidade -------- i=inadimplentes, c=credores
         * $pessoa ---------- j=jur�dica, f=f�sica
         * $tipoDado -------- n=nome, c=cpf/cnpj
         * $valor ----------- valor do nome ou cpf/cnpj
         *
         * NECESS�RIO CONFIGURAR O VALOR DOS ATRIBUTOS NAME DOS INPUTS
         ************************************************************************* */
        $("#pesquisarIC").click(function () {
            
            //ENTIDADE DO TIPO INADIMPLENTE
            entidade = 'i';

            //RECEBENDO O TIPO DE PESSOA [f ou j]
            pessoa = '';
            $('input[name="pessoa"]').each(function() {
                if ( $(this).is(':checked') ) {
                    pessoa = $(this).attr('value');
                }
            });

            //RECEBENDO O TIPO DE DADOS [nome_fantasia ou cpf_cnpj]
            tipodado = '';
            valor = '';
            //RECEBENDO O VALOR DIGITADO EM nome_fantasia ou cpf_cnpj
            
            if(($('input[name="nome"]').val() == '') && ($('input[name="cpf_cnpj"]').val() == '')){
                $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Para pesquisar � necess�rio digitar o NOME ou CPF/CNPJ</h3></td></tr>');//zera os valores antigos e escreve no elemento o valor selecione
                return false;
            }
            else {
                if ($('input[name="cpf_cnpj"]').val() != ''){
                    valor = $('input[name="cpf_cnpj"]').val();
                    tipodado = 'c';
                }

                else if ($('input[name="nome_fantasia"]').val() != ''){
                    valor = $('input[name="nome_fantasia"]').val();
                    tipodado = 'n';
                }
            }
            
            //FUN��O AJAX
            $.ajax({
                
                type: "POST",//TIPO DE DADOS
                url: base_url+'inadimplente/listar_pesquisa',//FUN��O DO CONTROLLER PARA TRATAR A LISTAGEM
                data: 'entidade='+entidade+'&pessoa='+pessoa+'&tipodado='+tipodado+'&valor='+valor,//DADOS ENVIADOS
                dataType: 'json',//TIPO DE DADOS A SER RECEBIDO
                
                //CASO HAJA ERRO DE PROCESSAMENTO
                error: function(xhr, status, er) {
                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                },

                //CASO A FUN��O DO CONTROLLER SEJA EXECUTADA COM SUCESSO E OS DADOS RECEBIDOS COM SUCESSO
                success: function(retorno)
                {
                    $("#dialog-form").dialog("open");///ABRE A CAIXA DE DI�LOGO
                    $('#dadosPesquisa').html('');//zera os valores antigos e escreve no elemento o valor selecione
                    vazio = false;//VARI�VEL PARA IDENTIFICAR SE ALGUM DADO �NTEGRO TENHA SIDO RECEBIDO

                    $.each(retorno.resultado, function(i, v){//para cada �ndice retornado executa a fun��o
                        if(v.nome != null){
                            //CONCATENA AS LINHAS DA TABELA COM OS VALORES RECEBIDOS
                            $('#dadosPesquisa').append(
                            '<tr class="tuplas" id="'+unescape(v.codigo)+'">'+
                                '<td>'+unescape(v.nome)+'</td>'+
                                '<td>'+unescape(v.cpf_cnpj)+'</td>'+
                                '<td>'+unescape(v.endereco)+'</td>'+
                                '<td class="last">'+unescape(v.cidade)+'</td>'+
                                '</tr>'
                        );
                        }
                        else vazio = true;//CASO OS DADOS RETORNADOS SEJAM NULOS
                    });
                    $('.tuplas').css('cursor','pointer');//SETA O TIPO DO CURSOR

                    //AO CLICAR SOBRE UMA LINHA DA TABALEA COM A CLASSE TUPLA
                    $('.tuplas').click(function(e){
                        e.preventDefault();
                        //                        alert('deu certo');

                        //CAPTURANDO O C�DIGO, O NOME E O TELEFONE
                        codigo = $(this).attr('id');
                        nome = $(this).find(":eq(0)").text();
                        fone = $(this).find(":eq(1)").text();

                        //ATRIBUINDO VALORS CAMPOS
                        $('#nome').val(nome).attr('readonly',true);
                        $('#cpf_cnpj_input').val(fone).attr('readonly',true);
                        $('#inad_cod').val(codigo);
                        

                        //                        $('#'+origem+'cod').val(codigo);


                        $('.ui-dialog-titlebar-close').trigger('click');
                    });
                    
                    //CASO OS DADOS RETORNADOS SEJAM NULOS
                    if (vazio)
                    {   //EXIBE MENSAGEM
                        $('#dadosPesquisa').html('<tr><td colspan="4"><h3>Nenhum dado encontrado pesquisando por ['+valor+']</h3></td></tr>');
                    }
                }
            });
        });

    });
</script>
<div id="dialog-form" style="padding-left: 5px; padding-right: 5px; " title="Pesquisar dados">
    <p style="text-align: center">
        <strong>Dados encontrados:</strong>
    </p>
    <div id="content" style="width: 750px;">
        <div id="right" style="margin-left: 0px;">
            <div class="box">
                <div class="table" style="padding-left: 0px; font-size: 10px; width: 750px;">
                    <form action="" method="post">
                        <table id="products">
                            <thead>
                                <tr>
                                    <th class="left" style="width: 20%;">NOME</th>
                                    <th style="width: 14%;">CPF/CNPJ</th>
                                    <th>ENDERE�O</th>
                                    <th class="last">CIDADE</th>
                                </tr>
                            </thead>
                            <tbody id="dadosPesquisa">
                                <!--RESULTADO DA PESQUISA AQUI!! -->
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="content">

    <?php echo $sidebar; ?>

    <div id="right">
        <div class="box" style="min-height: 900px;">
            <div class="title">
                <h5>Remanejar inadimplente</h5>
            </div>
            <?php echo $mensagem; ?><!--id="form"-->
            <form  id="formDiv" action="<?php echo base_url() . 'inadimplente/remanjarInadimplente' ?>" method="post">
                <div class="form">
                    <div class="fields">
                        <div style="margin-top: 12px; margin-bottom: 13px;" class="blocoTitulo">
                            Dados do inadimplente
                        </div>
                        <div class="field  field-first">
                            <div class="divleftlast" style="width: 200px;">
                                <div class="label">
                                    <label>Pessoa:</label>
                                </div>
                                <input style="margin-top: 7px; " type="radio" name="pessoa" value="f" id="pfisica">F�sica
                                <input type="radio" name="pessoa" value="j" id="pjuridica">Jur�dica
                            </div>
                            <div id="atrsPessoaFisica" style="width:375px; height:30px; float: right; overflow: hidden">
                            </div>
                        </div>
                        <div class="field">
                            <div class="divleft" style="width: 371px;">
                                <div style="width: 89px;" class="label">
                                    <label for="nome">Nome:</label>
                                </div>
                                <div class="input">
                                    <input style="width: 248px;" type="text" id="nome" name="nome_fantasia" maxlength="120" value=""/>
                                    <input type="hidden" id="inad_cod" name="inad_cod"value=""/>
                                </div>
                            </div>
                            <div class="divleft" style="width: 231px;">
                                <div style="width: 76px; padding-left: 1px;" class="label">
                                    <label for="cpf_cnpj">CPF/CNPJ:</label>
                                </div>
                                <div class="input" id="cpf_cnpj">
                                    <input style="width: 124px;" type="text" id="cpf_cnpj_input" name="cpf_cnpj" maxlength="18"/>
                                </div>
                            </div>
                            <div class="divleftlast" style="width: 87px; margin: 0px;">
                                <div class="buttons">
                                    <div class="highlight">
                                        <button style="width: 85px; padding: 4px;" id="pesquisarIC" name="pesquisarIC" type="button" class="dialog-form-open">Pesquisar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="margin-top: 12px; margin-bottom: 3px; padding-bottom: 12px;" class="blocoTitulo recuperador">
                        Novo Recuperador: <span id="recuperador_padrao"></span>
                    </div>
                    <div class="fields">
                        <div class="field">

                            <div style="border: none; width: 245px; float: left">
                                <div style="margin-left: 225px;" class="select">
                                    <select style="width: 260px;" id="recuperador" name="recuperador">
                                        <?php if (sizeof($recuperadores) == 0) : ?>
                                            <option value="nulo"> NENHUM RECUPERADOR ATIVO</option>
                                        <?php else : ?>
                                            <option value="nulo">Selecione um recuperador</option>
                                            <?php foreach ($recuperadores as $recuperador) : ?>
                                                <option value="<?php echo $recuperador->usu_cod; ?>"><?php echo $recuperador->usu_usuario_sis; ?><?php echo $recuperador->usu_ativo == '0'?' (DESATIVADO)':''; ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="field">
                            <div class="divleftlast" style="margin-left: 255px;margin-bottom: 10px;">
                                <div style="text-align: center; margin-top: 10px;" class="buttons">
                                    <input type="reset" class="cancelar" id="cancelar" name="cancelar" value="Cancelar" />
                                    <div style="margin-left: 7px;" class="highlight">
                                        <input type="submit" name="submit.highlight" value="Remanejar" />
                                    </div>
                                </div>
                                <div style="text-align: center; margin-left: 10px;" class="buttons">
<!--                                <input type="reset" name="cancelar" value="Cancelar" />-->
                                <!--                                <a href="</?php echo base_url() . 'cadbem/novo' ?>">
                                                                <div style="margin-left: 7px;" class="highlight">
                                                                    <input type="submit" name="bem" value="Bens" />
                                                                </div>
                                                            </a>-->
                                <div style="margin-left: 7px;" class="highlight">
<!--                                    <input type="submit" name="cadastrar" value="Cadastrar" />-->
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>