<script type="text/javascript">
        $(document).ready(function(){
            $('.visualizar').click(function(e){
                    window.location.href='http://webhost/cobranca/divida/detalhes';
            });
            $('input[value="Ficha"]').click(function(e){
                    window.location.href='http://webhost/cobranca/divida/ficha';
            });

        });
</script>
<html>
    <body>
        <div id="content">

        <?php echo $sidebar; ?>

            <div id="right">
                <div id="box-tabs" class="box" style="min-height: 798px;">
                    <div class="title">
                        <h5>Todas Previs�es de Recebimentos</h5>
                        <ul class="links">
                            <li><a href="#hoje">Hoje</a></li>
                            <li><a href="#semana">Semana</a></li>
                            <li><a href="#mes">M�s</a></li>
                            <li><a href="#proxmes">Proximo m�s</a></li>
                        </ul>
                    </div>
                    <div id="hoje">
                        <div class="form">
                            <div class="fields">
                                <form id="form" method="post" action="previsoes">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 345px;">
                                            <div style="width: 129px; padding-left: 1px;" class="label">
                                                <label for="nome">Selecionar credor:</label>
                                            </div>
                                            <div class="select">
                                                <select style="width: 205px; margin-left: 5px;" id="est_civil" name="filtroCredor">
                                                    <option value="todos">---Todos---</option>
                                                      <?php foreach ($todosCredores as $credor): ?>
                                                    <option value="<?php echo $credor->cre_cod ?>"<?php echo $credor->cre_cod == $filtroCredor ? 'selected' : ''; ?>><?php echo $credor->cre_nome_fantasia ?></option>
                                                      <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="filtrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 40px;">C�d</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Vencimento</th>
                                                <th>Recuperador</th>
                                                <th class="last" style="width: 85px;">Valor</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $totalRecebido = 0; $totalDescontos=0; foreach ($previsoesHoje as $prevHoje): ?>
                                            <tr>
                                                <td class="title" style="width: 50px;"><?php echo $prevHoje->cobranca_cob_cod?></td>
                                                <td><?php echo utf8_decode($prevHoje->ina_nome);?></td>
                                                <td><?php echo utf8_decode($prevHoje->cre_nome_fantasia);?></td>
                                                <td><?php echo convDataBanco($prevHoje->paa_vencimento);?></td>
                                                <td><?php echo utf8_decode($prevHoje->recuperador);?></td>
                                                <td class="last"><?php echo convMoney($prevHoje->paa_valor);?></td>
                                            </tr>
                                            <?php $totalRecebido += $prevHoje->paa_valor;
                                                  endforeach; ?>
                                            <tr>
                                            <tr>
                                                <td class="title" colspan="5" style="width: 30px;"><b>Total</b></td>
                                                <td class="last" colspan="2">
                                                    <b><?php echo convMoney($totalRecebido);?></b>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="semana">
                        <div class="form">
                            <div class="fields">
                                <form id="form" method="post" action="previsoes">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 345px;">
                                            <div style="width: 129px; padding-left: 1px;" class="label">
                                                <label for="nome">Selecionar credor:</label>
                                            </div>
                                            <div class="select">
                                                <select style="width: 205px; margin-left: 5px;" id="est_civil" name="filtroCredor">
                                                    <option value="todos">---Todos---</option>
                                                    <?php foreach ($todosCredores as $credor): ?>
                                                    <option value="<?php echo $credor->cre_cod ?>"<?php echo $credor->cre_cod == $filtroCredor ? 'selected' : ''; ?>><?php echo $credor->cre_nome_fantasia ?></option>
                                                  <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="filtrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 40px;">C�d</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Vencimento</th>
                                                <th>Recuperador</th>
                                                <th class="last" style="width: 85px;">Valor</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php $totalRecebido = 0; $totalDescontos=0; foreach ($previsoesSemana as $prevSem): ?>
                                            <tr>
                                                <td class="title" style="width: 50px;"><?php echo $prevSem->cobranca_cob_cod?></td>
                                                <td><?php echo utf8_decode($prevSem->ina_nome);?></td>
                                                <td><?php echo utf8_decode($prevSem->cre_nome_fantasia);?></td>
                                                <td><?php echo convDataBanco($prevSem->paa_vencimento);?></td>
                                                <td><?php echo utf8_decode($prevSem->recuperador);?></td>
                                                <td class="last"><?php echo convMoney($prevSem->paa_valor);?></td>
                                            </tr>
                                            <?php $totalRecebido += $prevSem->paa_valor;
                                            endforeach; ?>
                                            <tr>
                                                <td class="title" colspan="5" style="width: 30px;"><b>Total</b></td>
                                                <td colspan="2" class="last">
                                                    <b><?php echo convMoney($totalRecebido);?></b>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="mes">
                        <div class="form">
                            <div class="fields">
                                <form id="form" method="post" action="previsoes">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 345px;">
                                            <div style="width: 129px; padding-left: 1px;" class="label">
                                                <label for="nome">Selecionar credor:</label>
                                            </div>
                                            <div class="select">
                                                <select style="width: 205px; margin-left: 5px;" id="est_civil" name="filtroCredor">
                                                    <option value="todos">---Todos---</option>
                                                    <?php foreach ($todosCredores as $credor): ?>
                                                    <option value="<?php echo $credor->cre_cod ?>"<?php echo $credor->cre_cod == $filtroCredor ? 'selected' : ''; ?>><?php echo $credor->cre_nome_fantasia ?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="filtrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 40px;">C�d</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Vencimento</th>
                                                <th>Recuperador</th>
                                                <th class="last" style="width: 85px;">Valor</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $totalRecebido = 0; $totalDescontos=0; foreach ($previsoesMes as $prevMes): ?>
                                            <tr>
                                                <td class="title" style="width: 50px;"><?php echo $prevMes->cobranca_cob_cod?></td>
                                                <td><?php echo utf8_decode($prevMes->ina_nome);?></td>
                                                <td><?php echo utf8_decode($prevMes->cre_nome_fantasia);?></td>
                                                <td><?php echo convDataBanco($prevMes->paa_vencimento);?></td>
                                                <td><?php echo utf8_decode($prevMes->recuperador);?></td>
                                                <td class="last"><?php echo convMoney($prevMes->paa_valor);?></td>
                                                </tr>
                                             <?php $totalRecebido += $prevMes->paa_valor;
                                                   endforeach;
                                            ?>
                                            <tr>
                                                <td class="title" colspan="5" style="width: 30px;"><b>Total</b></td>
                                                <td colspan="2" class="last">
                                                    <b><?php echo convMoney($totalRecebido - $totalDescontos);?></b>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="proxmes">
                        <div class="form">
                            <div class="fields">
                                <form id="form" method="post" action="previsoes">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 345px;">
                                            <div style="width: 129px; padding-left: 1px;" class="label">
                                                <label for="nome">Selecionar credor:</label>
                                            </div>
                                            <div class="select">
                                                <select style="width: 205px; margin-left: 5px;" id="est_civil" name="filtroCredor">
                                                    <option value="todos">---Todos---</option>
                                                    <?php foreach ($todosCredores as $credor): ?>
                                                    <option value="<?php echo $credor->cre_cod ?>"<?php echo $credor->cre_cod == $filtroCredor ? 'selected' : ''; ?>><?php echo $credor->cre_nome_fantasia ?></option>
                                                    <?php endforeach;?>                                                   
                                                </select>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 40px;">C�d</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Vencimento</th>
                                                <th>Recuperador</th>
                                                <th class="last" style="width: 85px;">Valor</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $totalRecebido = 0; $totalDescontos=0; foreach ($previsoesProxMes as $prevProxMes): ?>
                                            <tr>
                                                <td class="title" style="width: 30px;"><?php echo $prevProxMes->cobranca_cob_cod?></td>
                                                <td><?php echo utf8_decode($prevProxMes->ina_nome);?></td>
                                                <td><?php echo utf8_decode($prevProxMes->cre_nome_fantasia);?></td>
                                                <td><?php echo convDataBanco($prevProxMes->paa_vencimento);?></td>
                                                <td><?php echo utf8_decode($prevProxMes->recuperador);?></td>
                                                <td class="last"><?php echo convMoney(($prevProxMes->paa_valor));?></td>
                                            </tr>
                                            <?php $totalRecebido += $prevProxMes->paa_valor;
                                                   endforeach;
                                            ?>
                                            <tr>
                                                <td class="title" colspan="5" style="width: 30px;"><b>Total</b></td>
                                                <td colspan="2" class="last">
                                                    <b><?php echo convMoney($totalRecebido);?></b>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>