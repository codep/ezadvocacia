<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img.'logo_elisangela.jpg';?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br|www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo $title ;?></p>
        </div>
    </div>
    <?php if($trabalhada == "") :?>
    <!-- EXIBE AS D�VIDAS VIRGENS -->
    <table id="tableGer04" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th class="thInadimplente">Inadimplente</th>
                <th class="thCredor">Credor</th>
                <th class="th3">Data de Cadastro</th>
                <th class="th4 last">�ltimo RO</th>
            </tr>
        </thead>
        <tbody>
            <?php if(sizeof($dividasVirgens) == 0) :?>
            <tr>
                <td class="last" colspan="4"> NENHUMA D�VIDA VIRGEM ENCONTRADA NESTE PER�ODO</td>
            </tr>
            <?php endif;?>
                <?php foreach ($dividasVirgens as $dividVirgem) :?>
                <tr>
                    <td class="tdInaNome"><?php echo utf8_decode($dividVirgem->ina_nome);?></td>
                    <td class="tdCreNomeFantasia"><?php echo utf8_decode($dividVirgem->cre_nome_fantasia);?></td>
                    <td class="td3">
                        <?php
                        $dataCadastrada = convDataBanco($dividVirgem->data_cadastro);
                        echo ($dataCadastrada == "//") ? "" : $dataCadastrada; //se a data vier vazia o m�todo convDataBanco retorna s� as barra, se isso acontecer imprime nada
                        ?>
                    </td>
                    <td class="td4 last"><?php echo utf8_decode($dividVirgem->ult_ros_detalhe);?></td>
                </tr>
                <?php endforeach;?>
        </tbody>
    </table>
    <?php endif;?>
    <!-- FIM DA EXIBI��O DAS D�VIDAS VIRGENS -->
    <!-- EXIBE AS D�VIDAS TRABALHADAS -->
    <table id="tableGer04a" cellpadding="0" cellspacing="0">
        <thead>
            <tr><th colspan="4" class="last">D�VIDAS</th></tr>
            <tr>
                <th class="thInadimplente">Inadimplente</th>
                <th class="thCredor">Credor</th>
                <th class="th3">Data de Cadastro</th>
                <th class="th4 last">�ltimo RO</th>
            </tr>
        </thead>
        <tbody>
            <?php if(sizeof($dividasTrabalhadas) == 0) :?>
                <tr>
                    <td colspan="4" class="last"> NENHUMA D�VIDA TRABALHADA ENCONTRADA NESTE PER�ODO</td>
                </tr>
            <?php endif;?>

            <?php foreach ($dividasTrabalhadas as $dividTrabalhada) :?>
            <tr>
                <td class="tdInaNome"><?php echo utf8_decode($dividTrabalhada->ina_nome);?></td>
                <td class="tdCreNomeFantasia"><?php echo utf8_decode($dividTrabalhada->cre_nome_fantasia);?></td>
                <td class="td3">
                  <?php
                    $dataCadastrada = convDataBanco($dividTrabalhada->data_cadastro);
                    echo ($dataCadastrada == "//") ? "" : $dataCadastrada; //se a data vier vazia o m�todo convDataBanco retorna s� as barra, se isso acontecer imprime nada
                  ?>
                </td>
                <td class="td4 last">
                    <?php echo utf8_decode($dividTrabalhada->ult_ros_detalhe);?>
                </td>
            </tr>
            <tr>
                <td class="td5" style=" border-bottom: 2px solid;"><strong>�ltimas Opera��es</strong></td>
                <td colspan="3" class="last" style=" border-bottom: 2px solid;">
                    <?php foreach ($dezUltimosRos[$dividTrabalhada->cobranca_cob_cod] as $ros) : ?>
                    <?php echo convDataBanco($ros->ros_data) ." "; echo $ros->ros_hora ." "; echo $ros->ope_nome;?><br/>
                    <?php endforeach; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <!-- FIM DA EXIBI��O DAS D�VIDAS TRABALHADAS -->
    <!-- EXIBE OS ACORDOS ENCONTRADOS -->
    <table id="tableGer04b" cellpadding="0" cellspacing="0">
        <thead>
            <tr><th colspan="4" class="last">ACORDOS</th></tr>
            <tr>
                <th class="thInadimplente">Inadimplente</th>
                <th class="thCredor">Credor</th>
                <th class="th3">Data de Cadastro</th>
                <th class="th4 last">�ltimo RO</th>
            </tr>
        </thead>
        <tbody>
            <?php if(sizeof($todosAcordos) == 0) :?>
            <tr>
                <td colspan="4" class="last"> NENHUM ACORDO ENCONTRADO NESTE PER�ODO</td>
            </tr>
            <?php endif;?>

            <?php foreach ($todosAcordos as $acordo) :?>
            <tr>
                <td class="tdInaNome"><?php echo utf8_decode($acordo->ina_nome);?></td>
                <td class="tdCreNomeFantasia"><?php echo utf8_decode($acordo->cre_nome_fantasia);?></td>
                <td class="td3">
                    <?php
                       $dataCadastrada = convDataBanco($acordo->data_cadastro);
                       echo ($dataCadastrada == "//") ? "" : $dataCadastrada; //se a data vier vazia o m�todo convDataBanco retorna s� as barra, se isso acontecer imprime nada
                    ?>
                </td>
                <td class="td4 last"><?php echo utf8_decode($acordo->ult_ros_detalhe);?></td>
            </tr>
            <tr>
                <td class="td5" style=" border-bottom: 2px solid;"><strong>�ltimas Opera��es</strong></td>
                <td colspan="3" class="last" style=" border-bottom: 2px solid;">
                    <?php foreach ($dezUltimosRos[$acordo->cobranca_cob_cod] as $ros) : ?>
                    <?php echo convDataBanco($ros->ros_data) ." "; echo $ros->ros_hora ." "; echo $ros->ope_nome;?><br/>
                    <?php endforeach; ?>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
<!-- FIM DA EXIBI��O DOS ACORDOS ENCONTRADOS -->
</div>