<script type="text/javascript">
        $(document).ready(function(){
            $('.editausu').click(function(e){
                    link = $(this).attr('id');
                    window.location.href=link;
            });
        });
</script>
<div id="content">

<?php echo $sidebar; ?>

    <div id="right">
        <div class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Lista de grupos de usu�rios</h5>
                <div class="form" style="clear: none; float: right; padding-top:5px; height: 27px;">
                    <div class="fields">
                        <div class="field" style="border: none; margin: 0px; padding: 0px;">
                            <div class="highlight">
                                <a href="<?php echo base_url().'cadtipousu/novo'?>">
                                    <input style="width: 155px; font-size: 13px; font-weight: bold;" type="submit" name="cadastrar" value="Novo grupo" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form id="form" action="" method="post">
                <?php echo $mensagem; ?>
                <div class="form">
                    <div class="fields">
                        <div class="table">
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th class="left">Nome do grupo</th>
                                        <th class="last">Excluir</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($grupos as $grupo):?>
                                    <tr>
                                        <td id="<?php echo base_url().'cadtipousu/editar/cod:'.$grupo->gru_cod;?>" class="title editausu"><?php echo utf8_decode($grupo->gru_titulo) ?></td>
                                        <td style="text-align: center; vertical-align: middle;" class="last">
                                            <a href="<?php echo base_url().'cadtipousu/excluir/cod:'.$grupo->gru_cod;?>">
                                                <img src="<?php echo $img.'excluir.png'?>" alt="+"/>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>