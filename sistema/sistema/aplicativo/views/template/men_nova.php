<script type="text/javascript">
    $(document).ready(function(){
        
        $('#mensagem').maxLength(500,'divCount');

        //VALIDA��ES - CAMPOS OBRIGAT�RIOS
        $('form').submit(function(){
            if($('#titulo').val() == ''){
                $('#titulo').focus();
                return false;
            }
            else if($('#destinatario').val() == ''){
                alert('[Selecione o destinat�rio]');
                return false;

            }
            else if($('#tipo_msg').val() == ''){
                alert('[Selecione o tipo da mensagem]');
                return false;

            }
            else if($('#mensagem').val() == ''){
                alert('� necess�rio digitar uma MENSAGEM');
                return false;
            }
        });
    });
</script>
<html>
    <body>
        <div id="content">
            <?php echo $sidebar; ?>
            <div id="right">
                <div class="box">
                    <div class="title">
                        <h5><?php echo isset($resposta)?'Responder mensagem':'Nova Mensagem' ?></h5>
                    </div>
                    <?php echo $mensagem ?>
                    <form id="form" action="<?php echo base_url().'mensagens/novaMensagem' ?>" method="post">
                        <div class="form">
                            <div class="fields">
                                <div class="field">
                                    <div class="divleftlast" style="margin-left: 0px; width: 706px;">
                                        <div style="width: 89px;" class="label">
                                            <label for="titulo">T�tulo:</label>
                                        </div>
                                        <div class="input">
                                            <input style="width: 591px;" type="text" maxlength="80" id="titulo" name="titulo" <?php echo isset($resposta)?'value="RE: '.$resposta->men_titulo.'"':'' ?>/>
                                        </div>
                                    </div>
                                </div>
                                <div style="padding-top: 5px; padding-left: 92px;" class="field">
                                    <div class="divleft" style="width: 309px;">
                                        <div style="margin-left: 6px;" class="select">
                                            <select style="width: 294px;" id="destinatario" name="destinatario">
                                                <?php if(!isset($resposta)): ?>
                                                    <option value="" selected>[Selecione o destinat�rio]</option>
                                                    <?php foreach ($usuUsuarioSis as $destinatario): ?>
                                                        <option value="<?php echo $destinatario->usu_cod; ?>"><?php echo $destinatario->usu_usuario_sis ?></option>
                                                    <?php endforeach; ?>
                                                    <option value="todos">---Todos---</option>
                                                <?php endif;?>
                                                <?php if(isset($resposta)): ?>
                                                <option value="<?php echo $resposta->usu_cod ?>"><?php echo $resposta->usu_nome ?></option>
                                                <?php endif;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="divleftlast" style="width: 300px; margin-left: 0px;">
                                        <div style="margin-left: 6px;" class="select">
                                            <select style="width: 290px;" id="tipo_msg" name="tipo_msg">
                                                <option value="">[Selecione o tipo da mensagem]</option>
                                                <option value="1">Urgente</option>
                                                <option value="2">Aviso</option>
                                                <option value="3">Notifica��o</option>
                                                <option value="4">Comum</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div style="height: 125px;" class="field">
                                    <div class="divleftlast" style="width: 701px;">
                                        <div style="width: 84px;" class="label">
                                            <label for="mensagem">Mensagem:</label>
                                        </div>
                                        <div class="textarea">
                                            <textarea style="width: 586px; height: 100px;" id="mensagem" name="mensagem" cols="500" rows="5"></textarea>
                                        </div>
                                        <div class="divCount" style="margin-left: 92px; font-weight: bold;"></div>
                                    </div>
                                </div>
                                <div style="text-align: center; margin-top: 10px;" class="buttons">
                                    <input type="reset" name="cancelar" value="Limpar" />
                                    <div style="margin-left: 7px;" class="highlight">
                                        <input type="submit" name="submit.highlight" value="Enviar" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>