<script type="text/javascript">
    $(document).ready(function(){
        
        var base_url = "<?php echo base_url()?>";

        function ocultaTudo(){
            $('#agendaCadastra01').hide();
            $('#periodoAgendado01').hide();
            $('#periodoCadastro01').hide();
            $('#btgerar01').hide();
            $('input[type="text"]').val('');
        }

        ocultaTudo();

        //AO SELECIONAR ALGUMA OP��O DO COMBOBOX FA�A
        $('select[name="comboTipoOperacao01"]').change(function(){

            //OCULTA OS CAMPOS E RESETA OS INPUTS TEXT
            ocultaTudo();

            //SE SELECIONAR ALGUMA OP��O V�LIDA EXIBE OS CAMPOS CONSEQUENTES
            if($('select[name="comboTipoOperacao01"] option:selected').text() != "[Selecione o Tipo de Opera��o]")
                $('#agendaCadastra01').fadeIn('fast');

            $('input[type="radio"]').each(function(){
                 this.checked = false;
            });
        });

        //AO MUDAR OS RADIOS "rdbTipoOperacao"
        $('input[name="rdbTipoOperacao"]').change(function(){
            
            //VARI�VEL AUXILIAR
            var valorAtual = $(this).val();

            if($(this).val() == 'agendado_para')
            {
                //PASSA POR TODOS OS RADIOS DA P�GINA VERIFICANDO SE � O "agendado_para"
                $('input[type="radio"]').each(function(){
                    if($(this).val() != valorAtual)
                        this.checked = false;
                });
                $('input[type="text"]').val('');
                $('#periodoCadastro01').hide();
                $('#periodoAgendado01').fadeIn('fast', function(){
                    $('#btgerar01').fadeIn('fast');
                });
            }
            
            else if($(this).val() == 'cadastrado_em')
            {
                //PASSA POR TODOS OS RADIOS DA P�GINA VERIFICANDO SE � O "cadastrado_em"
                $('input[type="radio"]').each(function(){
                    if($(this).val() != valorAtual)
                        this.checked = false;
                });
                $('input[type="text"]').val('');
                $('#periodoAgendado01').hide();
                $('#periodoCadastro01').fadeIn('fast', function(){
                    $('#btgerar01').fadeIn('fast');
                });
            }
        });

    });
</script> 
     <div id="content">
         <?php echo $sidebar; ?>
            <div id="right">
                <div id="box-tabs" class="box" style="min-height: 798px;">
                    <div class="title">
                        <h5>Relat�rio por RO</h5>
                    </div>
                    <?php echo $mensagem ?>
                    <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                            Relat�rio por RO
                    </div>
                    <form id="form_rel_ro" action="<?php echo base_url().'relatorios/rel_ro01' ?>"  method="post" target="_blank">
                        <div class="form">
                            <div class="fields">
                                <div class="field">
                                    <div class="divleft" style="width: 140px; border-right: none;">
                                        <div style="width: 125px;" class="label">
                                            <label for="tipo_operacao">Tipo de Opera��o:</label>
                                        </div>
                                    </div>
                                    <div class="input" id="tipoOperacao" >
                                        <select style="width: 250px;" name="comboTipoOperacao01">
                                            <option value="">[Selecione o Tipo de Opera��o]</option>
                                            <?php foreach ($operacoes as $operacao): ?>
                                            <option value="<?php echo $operacao->ope_cod ?>"><?php echo utf8_decode($operacao->ope_nome); ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="field  field-first" style="margin-top:6px;" id="agendaCadastra01">
                                    <div class="divleft" style="height:29px; width: 210px;">
                                        <input style="margin-top: 7px; " type="radio" name="rdbTipoOperacao" value="agendado_para" /> Agendado para
                                    </div>
                                    <div class="divleftlast" style="height:29px; width: 200px;">
                                        <input style="margin-top: 7px; " type="radio" name="rdbTipoOperacao" value="cadastrado_em" />Cadastrado em
                                    </div>
                                </div>
                                <div class="field" id="periodoAgendado01">
                                    <div class="divleft" style="margin-left: 2px; width:150px;">
                                        <div style="width: 28px; padding-left: 1px;" class="label">
                                            <label for="de">De:</label>
                                        </div>
                                        <div style=" width: 109px;" class="input">
                                            <input type="text" readonly name="data_de" class="date"/>
                                        </div>
                                    </div>
                                    <div class="divleft" style="margin-left: 2px; width:150px; border: none;">
                                        <div style="width: 28px; padding-left: 1px;" class="label">
                                            <label for="ate">At�:</label>
                                        </div>
                                        <div style=" width: 109px;" class="input">
                                            <input type="text" readonly name="data_ate" class="date"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="field" id="periodoCadastro01">
                                    <div class="divleft" style="margin-left: 2px; width:250px; border-right: none;">
                                        <div style="width: 50px; padding-left: 1px;" class="label">
                                            <label for="desde">Desde:</label>
                                        </div>
                                        <div style="width: 109px; margin-left: 7px; float: left;" class="input">
                                            <input type="text" readonly name="data_desde" class="date"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="field" id="btgerar01">
                                    <div class="divleftlast" style="text-align: center">
                                        <div class="buttons" style="width: 700px">
                                            <div class="highlight" style=" margin: 0 auto">
                                                    <input style="width: 185px; margin: 0 auto" type="submit" name="filtrar_rel_adm02" value="Gerar relat�rio" />
                                            </div>
                                          </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </form>                    
                </div>
           </div>
      </div>