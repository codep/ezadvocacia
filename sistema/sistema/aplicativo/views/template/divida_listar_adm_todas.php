<script type="text/javascript">
    $(document).ready(function(){
        //            $('.visualizar').click(function(e){
        //                aux = $(this).attr('id');
        //                alert('clicou em: '+aux);
        //                    window.location.href='<?php echo base_url() ?>divida/detalhes';
        //            });
        $('input[value="Ficha"]').click(function(e){
            aux = $(this).parent().attr('id');
            window.location.href='<?php echo base_url() ?>divida/ficha/cod:'+aux;
        });
        validacao = "<?php if (isset($aba))
    echo $aba ?>";
        if(validacao != ''){
            $('a[href='+validacao+']').trigger('click');
        }
          
    });
</script>
<html>
    <body>
        <div id="content">
<?php echo $sidebar; ?>
            <div id="right">
                <div id="box-tabs" class="box" style="min-height: 798px;">
                    <div class="title">
                        <h5>Todas d�vidas administrativas</h5>
                        <ul class="links">
                            <li><a href="#virgem">Virgem</a></li>
                            <li><a href="#andamento">Em andamento</a></li>
                            <li><a href="#acordo">Em acordo</a></li>
                            <li><a href="#atraso">Em atraso</a></li>
                        </ul>
                    </div>
                    <div id="virgem">
                        <div class="form">
                            <div class="fields">
                                <form id="form" method="post" action="<?php echo base_url() . 'divida/listartodasadm' ?>">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 286px;">
                                            <div style="width: 69px; padding-left: 1px;" class="label">
                                                <label for="nome">Filtro:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 188px;" type="text" id="txPesquisar" name="pesquisar"/>
                                                <input type="hidden" name="aba" value="#virgem"/>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="filtrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 30px;">Cod.</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Valor</th>
                                                <th>�ltimo RO</th>
                                                <th class="last">Visualizar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php foreach ($todasCobrancasVirgensAdm as $todaCobVirgAdm): ?>
                                                <tr>
                                                    <td class="title" style="width: 30px;"><?php echo $todaCobVirgAdm->cob_cod; ?></td>
                                                    <td><?php echo utf8_decode($todaCobVirgAdm->ina_nome); ?></td>
                                                    <td><?php echo utf8_decode($todaCobVirgAdm->cre_nome_fantasia); ?></td>
                                                    <td><?php echo convMoney($todaCobVirgAdm->div_total); ?></td>
                                                    <td>
    <?php echo utf8_decode($todaCobVirgAdm->ultimo_ro); ?>
                                                    </td>
                                                    <td class="last">
                                                        <div class="buttons">
                                                            <div class="highlight" id="<?php echo $todaCobVirgAdm->ina_cod ?>">
                                                                <input style="width: 85px;" class="visualizar"  type="submit" name="cadastrar" value="Ficha" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
<?php endforeach; ?>
                                        </tbody>                                        
                                    </table>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="acordo">
                        <div class="form">
                            <div class="fields">
                                <form id="form" action="<?php echo base_url() . 'divida/listartodasadm' ?>" method="post">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 286px;">
                                            <div style="width: 69px; padding-left: 1px;" class="label">
                                                <label for="nome">Filtro:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 188px;" type="text" id="nome" name="pesquisar"/>
                                                <input type="hidden" name="aba" value="#acordo"/>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 30px;">Cod.</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Valor</th>
                                                <th>�ltimo RO</th>
                                                <th class="last">Visualizar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php foreach ($todasCobrancasEmAcordoAdm as $todaCobAcor): ?>
                                                <tr>
                                                    <td class="title" style="width: 30px;"><?php echo $todaCobAcor->cob_cod; ?></td>
                                                    <td><?php echo utf8_decode($todaCobAcor->ina_nome); ?></td>
                                                    <td><?php echo utf8_decode($todaCobAcor->cre_nome_fantasia); ?></td>
                                                    <td><?php echo convMoney($todaCobAcor->aco_valor_atualizado); ?></td>
                                                    <td>
    <?php echo utf8_decode($todaCobAcor->ultimo_ro); ?>
                                                    </td>
                                                    <td class="last">
                                                        <div class="buttons">
                                                            <div class="highlight" id="<?php echo $todaCobAcor->ina_cod ?>">
                                                                <input style="width: 85px;" class="visualizar"  type="submit" name="cadastrar" value="Ficha" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
<?php endforeach; ?>
                                        </tbody>
                                    </table>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="andamento">
                        <div class="form">
                            <div class="fields">
                                <form id="form" action="<?php echo base_url() . 'divida/listartodasadm' ?>" method="post">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 286px;">
                                            <div style="width: 69px; padding-left: 1px;" class="label">
                                                <label for="nome">Filtro:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 188px;" type="text" id="nome" name="pesquisar"/>
                                                <input type="hidden" name="aba" value="#andamento"/>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="btnFiltrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 30px;">Cod.</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Valor</th>
                                                <th>�ltimo RO</th>
                                                <th class="last">Visualizar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php foreach ($todasCobrancasEmAndamentoAdm as $todasCobAnd): ?>
                                                <tr>
                                                    <td class="title" style="width: 30px;"><?php echo $todasCobAnd->cob_cod; ?></td>
                                                    <td><?php echo utf8_decode($todasCobAnd->ina_nome); ?></td>
                                                    <td><?php echo utf8_decode($todasCobAnd->cre_nome_fantasia); ?></td>
                                                    <td><?php echo convMoney($todasCobAnd->div_total); ?></td>

                                                    <td>
    <?php echo utf8_decode($todasCobAnd->ultimo_ro); ?>
                                                    </td>
                                                    <td class="last">
                                                        <div class="buttons">
                                                            <div class="highlight" id="<?php echo $todasCobAnd->ina_cod ?>">
                                                                <input style="width: 85px;" class="visualizar"  type="submit" name="cadastrar" value="Ficha" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
<?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="atraso">
                        <div class="form">
                            <div class="fields">
                                <form id="form" action="<?php echo base_url() . 'divida/listartodasadm' ?>" method="post">
                                    <div class="field  field-first">
                                        <div class="divleft" style="width: 286px;">
                                            <div style="width: 69px; padding-left: 1px;" class="label">
                                                <label for="nome">Filtro:</label>
                                            </div>
                                            <div class="input">
                                                <input style="width: 188px;" type="text" id="nome" name="pesquisar"/>
                                                <input type="hidden" name="aba" value="#atraso"/>
                                            </div>
                                        </div>
                                        <div class="divleftlast" style="width: 87px; margin: 0px;">
                                            <div class="buttons">
                                                <div class="highlight">
                                                    <input style="width: 85px;" type="submit" name="cadastrar" value="Filtrar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style="width: 30px;">Cod.</th>
                                                <th>Inadimplente</th>
                                                <th>Credor</th>
                                                <th>Valor</th>
                                                <th>Vencimento</th>
                                                <th class="last">Visualizar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php foreach ($todasCobrancasEmAtrasoAdm as $todaCobAtr): ?>
                                                <tr>
                                                    <td class="title" style="width: 30px;"><?php echo $todaCobAtr->cob_cod; ?></td>
                                                    <td><?php echo utf8_decode($todaCobAtr->ina_nome); ?></td>
                                                    <td><?php echo utf8_decode($todaCobAtr->cre_nome_fantasia); ?></td>
                                                    <td><?php echo convMoney($todaCobAtr->div_total); ?></td>
                                                    <td><?php echo convDataBanco($todaCobAtr->div_venc_inicial); ?></td>
                                                    <td class="last">
                                                        <div class="buttons">
                                                            <div class="highlight" id="<?php echo $todaCobAtr->ina_cod ?>">
                                                                <input style="width: 85px;" class="visualizar"  type="submit" name="cadastrar" value="Ficha" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
<?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>