<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img.'logo_elisangela.jpg';?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio  - SP  - 17 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br|www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo"><?php echo $title ;?></p>
        </div>
    </div>
    <table id="tableAce01" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="th3">Recuperador</th>
                <th id="th4" class="last">Data/Hora</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($relAcessosRecuperadores as $aceRecup) : ?>
            <tr>
                <td class="td3"><?php echo $aceRecup->usuario; ?></td>
                <td class="td4 last">
                    <?php 
                        echo convDataBanco($aceRecup->_data); echo "&nbsp &nbsp";
                        echo $aceRecup->hora;
                    ?>
                </td>
                <?php endforeach; ?>
            </tr>
        </tbody>
    </table>
</div>
