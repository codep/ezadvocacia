<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* * ************************************************************************
 * FUN��O QUE CONVERTE QUALQUER TEXTO PARA O FORMATO URL AMIGAVEL
 * EX: An�is de Fundi��o > aneis-de-fundicao
 * @string
 * - A STRING QUE SER� CONVERTIDA. CASO VAZIA RETORNAR� 'FALSE'
 * ************************************************************************ */

function url_amigavel($string='') {
    if ($string == '')
        return '';
    $string = strtr(strtolower($string), "����������������������纪", "aaaaaeeeeiiiiooooouuuucoa");
    $string = utf8_encode(html_entity_decode(strip_tags(htmlspecialchars_decode($string))));
    return url_title($string);
}

/* * ************************************************************************
 * FUN��O QUE PROCURA NA URL O PARAMETRO GET NO ESTILO URL AMIGAVEL
 * ************************************************************************ */

function get($var='') {

    if ($var == '')
        return false;
    $CI = & get_instance();
    if (!isset($CI->param)) {
        $uri = $CI->uri->segment_array();
        foreach ($uri as $val) {
            if (strpos($val, ':')) {
                $CI->param[substr($val, 0, strpos($val, ':'))] = substr($val, strpos($val, ':') + 1);
            }
        }
    }
    $retorno = isset($CI->param[$var]) ? $CI->param[$var] : false;

    return $retorno;
}

//FUN��O RESPONS�VEL POR TRATAR DATAS
function dataex($data) {
    $meses = array("", "Janeiro", "Fevereiro", "Mar�o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
    $mes = $meses[date("n", strtotime($data))];
    $dia = date("d", strtotime($data));
    $ano = date("Y", strtotime($data));
    return $dia . ' de ' . $mes . ' de ' . $ano;
}

/* * ************************************************************************
 * FUN��O QUE CONVERTE VALORES DE DATA E HORA ENTRE COMPUTACIONAL E HUMANO
 * EX: 2009-06-18 05:10:25 para 18/06/2009 05:10:25
 * @datahora
 * - DATA, HORA ou DataHora para que seja convertido
 * - O retorno ser� alternado entre o formato computacional ou humano
 * - Caso seja vazio, ir� pegar a data e hora atual
 * @formato
 * - Voc� dever� informar o que quer que ele retorne
 *      h = retorna apenas hora
 *      d = retorna apenas data
 *      dh = retorna dia e hora (PADR�O)
 * ************************************************************************ */

function convData($datahora = '', $formato='dh') {
    if ($datahora == '')
        $datahora = date('Y-m-d H:i:s');
    else if ($datahora == '0000-00-00')
        return false;

    //VERIFICA O FORMATO RECEBIDO E CONVERTE PARA COMPUTACIONAL
    if (strpos($datahora, ':') === false)
        $datahora = trim($datahora) . ' ' . date('H:i:s');
    if (strpos($datahora, '-') === false && strpos($datahora, '/') >= 0) {
        $arrdatahora = explode(' ', $datahora);
        $datahora = strtotime($arrdatahora[0]);
        $datahora = date('Y-m-d', $datahora) . ' ' . $arrdatahora[1];
    } else if (strpos($datahora, '/') === false && strpos($datahora, '-') >= 0) {
        $arrdatahora = explode(' ', $datahora);
        $datahora = strtotime($arrdatahora[0]);
        $datahora = date('d/m/Y', $datahora) . ' ' . $arrdatahora[1];
    } else {
        $datahora = date('Y-m-d') . ' ' . trim($datahora);
    }

    //VERIFICANDO EM QUAL FORMATO RETORNAR
    switch ($formato) {
        case 'h':
            $resultado = explode(' ', $datahora);
            $resultado = $resultado[1];
            break;
        case 'd':
            $resultado = explode(' ', $datahora);
            $resultado = $resultado[0];
            break;
        case 'dh':
            $resultado = $datahora;
            break;
    }

    return $resultado;
}

/* * ************************************************************************
 * FUN��O RESPONS�VEL POR REDIMENSIONAR IMAGENS
 *
 * PAR�METROS
 *  $lMini - largura do bloco
 *  $aMini - altura do bloco
 *  $imagem - imagem no tamanho horiginal
 *
 * A IMAGEM � REDIMENSIONADA DE ACORDO COM A PROPOR��O DOS VALORES INFORMADOS
 * NO PAR�METRO
 * *********************************************************************** */

function resizeimg($lMini, $aMini, $imagem) {

    $imginfo = getimagesize($imagem);
    $lImagem = $imginfo[0]; // largura da imagem original
    $aImagem = $imginfo[1]; // altura da imagem original

    $proH = $lMini / $aMini; //propor��o horizontal � a largura da miniatura dividida pela altura

    if ($lImagem / $aImagem >= $proH) {

        // reduzir a imagem para a altura aMini e largura proporcional
        return $imagem . '/l:' . $lMini;
    } else {

        // reduzir a imagem para a largura lMini e altura proporcional
        return $imagem . '/a:' . $aMini;
    }
}

/* * ************************************************************************
 * Fun��o que retorna a url anterior salva
 * ************************************************************************ */

function getBackUrl($tipo='ref') {
    $CI = & get_instance();
    if ($tipo == 'ref') {
        $backurl = $CI->session->userdata('refurl');
        if (!$backurl || $backurl == $CI->uri->uri_string()) {
            $backurl = $CI->session->userdata('bkprefurl');
        }
    } else if ($tipo == 'bkp') {
        $backurl = $CI->session->userdata('bkprefurl');
        if (!$backurl || $backurl == $CI->uri->uri_string()) {
            $backurl = '';
        }
    }

    return substr($backurl, 1);
}

/* * ************************************************************************
 * converte um valor para tipo dinheiro
 * ************************************************************************ */

function convMoney($valor) {
    if ($valor == '' || $valor == null) {
        $valor = 'Sob Consulta';
	} else {
	    // convertendo valor para tipo moeda
	    $valor = 'R$ ' . number_format($valor, 2, ',', '.');
	}
    return $valor;
}

/* * ************************************************************************
 * REMOVE ACENTOS E CARACTERES ESPECIAIS
 * ************************************************************************ */

function removeCE($string) {
    $palavra = strtr($string, "���������������������������������������������������������������������", "SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy");
    $palavranova1 = str_replace("�", "", $palavra);
    $palavranova2 = str_replace("`", "", $palavranova1);
    $palavranova2 = str_replace("'", "", $palavranova2);
    $palavranova2 = str_replace("�", "", $palavranova2);
    $palavranova2 = str_replace("�", "", $palavranova2);
    $palavranova2 = str_replace("�", "", $palavranova2);
    $palavranova2 = str_replace("�", "", $palavranova2);
    $palavranova2 = str_replace("�", "", $palavranova2);
    $palavranova2 = str_replace("�", "", $palavranova2);
    $palavranova2 = str_replace("�", "", $palavranova2);

    return trim(utf8_encode($palavranova2));
}

/* * ************************************************************************
 * REMOVE ACENTOS E CARACTERES ESPECIAIS
 * ************************************************************************ */

function removeCE_Upper($string) {
    $palavra = strtr($string, "���������������������������������������������������������������������", "SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy");
    $palavranova1 = str_replace("�", "", $palavra);
    $palavranova2 = str_replace("`", "", $palavranova1);
    $palavranova2 = str_replace("'", "", $palavranova2);
    $palavranova2 = str_replace("�", "", $palavranova2);
    $palavranova2 = str_replace("�", "", $palavranova2);
    $palavranova2 = str_replace("�", "", $palavranova2);
    $palavranova2 = str_replace("�", "", $palavranova2);
    $palavranova2 = str_replace("�", "", $palavranova2);
    $palavranova2 = str_replace("�", "", $palavranova2);
    $palavranova2 = str_replace("�", "", $palavranova2);

    return trim(strtoupper(utf8_encode($palavranova2)));
}

//pega o primeiro dia da semana
function getPDS() {
//    Paga o numero do dia da semana, sendo que segunda � dia 1, e faz a diferenca
    $aux = date("w");
    //pega a diferenca de segunda para o dia de hoje
    $aux-=1;

    //pega a data do dia atual
    $ano = substr(date("d/m/Y"), 6, 4);
    $mes = intval(substr(date("d/m/Y"), 3, 2));
    $dia = substr(date("d/m/Y"), 0, 2);

    //substrai do dia atual a diferen�a ate segunda feira

    $dia-=$aux;

    return $ano . '-' . $mes . '-' . $dia;
}

//pega o ultimo dia da semana
function getUDS() {
//    Paga o numero do dia da semana, sendo que segunda � dia 1, e faz a diferenca
    $aux = date("w");
    //pega a diferenca de segunda para o dia de hoje
    $aux = (7 - $aux);

    //pega a data do dia atual
    $ano = substr(date("d/m/Y"), 6, 4);
    $mes = intval(substr(date("d/m/Y"), 3, 2));
    $dia = substr(date("d/m/Y"), 0, 2);

    //substrai do dia atual a diferen�a ate segunda feira

    $dia+=$aux;

    return $ano . '-' . $mes . '-' . $dia;
}

/* * ************************************************************************
 * PESQUISA INADIMPLETE OU CREDOR POR NOME OU CPF/CNPJ
 * $entidade -------- i=inadimplentes, c=credores
 * $pessoa ---------- j=jur�dica, f=f�sica
 * $tipoDado -------- n=nome, c=cpf/cnpj
 * $valor ----------- valor do nome ou cpf/cnpj
 * ************************************************************************ */

function pesquisaIC($entidade, $pessoa, $tipoDado, $valor) {
    $CI = & get_instance();
    $CI->load->database();

//    $valor = removeCE_Upper($valor1);

    if ($entidade == 'i') {
        if ($tipoDado == 'n')
            $tipo = 'ina_nome';
        if ($tipoDado == 'c')
            $tipo = 'ina_cpf_cnpj';

        if ($pessoa == 'f') {

            $query = $CI->db->query("SELECT ina_cod as codigo, ina_nome as nome, ina_cpf_cnpj as cpf_cnpj, ina_endereco as endereco, ina_cidade as cidade, ina_foneres as fone from inadimplentes e WHERE e.ina_pessoa='f' AND e.$tipo LIKE '%$valor%'");
        } else if ($pessoa == 'j') {
            $query = $CI->db->query("SELECT ina_cod as codigo, ina_nome as nome, ina_cpf_cnpj as cpf_cnpj, ina_endereco as endereco, ina_cidade as cidade, ina_foneres as fone from inadimplentes e WHERE e.ina_pessoa='j' AND e.$tipo LIKE '%$valor%'");
        }
    } else if ($entidade == 'c') {
        if ($tipoDado == 'n')
            $tipo = 'cre_nome_fantasia';
        if ($tipoDado == 'c')
            $tipo = 'cre_cpf_cnpj';

        if ($pessoa == 'f') {
            $query = $CI->db->query("SELECT e.cre_cod as codigo, e.usuarios_responsavel_cod as recuperador_cod, u.usu_usuario_sis as recuperador_nome, e.cre_nome_fantasia as nome, e.cre_cpf_cnpj as cpf_cnpj, e.cre_endereco as endereco, e.cre_cidade as cidade, e.cre_prazo as prazo from credores e, usuarios u WHERE e.cre_pessoa='f' AND e.$tipo LIKE '%$valor%' AND u.usu_cod = e.usuarios_responsavel_cod AND e.cre_ativo='1'");
        } else if ($pessoa == 'j') {
            $query = $CI->db->query("SELECT e.cre_cod as codigo, e.usuarios_responsavel_cod as recuperador_cod, u.usu_usuario_sis as recuperador_nome, e.cre_nome_fantasia as nome, e.cre_cpf_cnpj as cpf_cnpj, e.cre_endereco as endereco, e.cre_cidade as cidade, e.cre_prazo as prazo from credores e, usuarios u WHERE e.cre_pessoa='j' AND e.$tipo LIKE '%$valor%' AND u.usu_cod = e.usuarios_responsavel_cod AND e.cre_ativo='1'");
        }
    }
    else
        return false;

    return $query->result();
}

function pesquisaICBlur($entidade, $pessoa, $tipoDado, $valor) {
    $CI = & get_instance();
    $CI->load->database();

//    $valor = removeCE_Upper($valor1);

    if ($entidade == 'i') {
        if ($tipoDado == 'n')
            $tipo = 'ina_nome';
        if ($tipoDado == 'c')
            $tipo = 'ina_cpf_cnpj';

        if ($pessoa == 'f') {

            $query = $CI->db->query("SELECT ina_cod as codigo, ina_nome as nome, ina_cpf_cnpj as cpf_cnpj, ina_endereco as endereco, ina_cidade as cidade, ina_foneres as fone from inadimplentes e WHERE e.ina_pessoa='f' AND e.$tipo LIKE '%$valor%' LIMIT 1");
        } else if ($pessoa == 'j') {
            $query = $CI->db->query("SELECT ina_cod as codigo, ina_nome as nome, ina_cpf_cnpj as cpf_cnpj, ina_endereco as endereco, ina_cidade as cidade, ina_foneres as fone from inadimplentes e WHERE e.ina_pessoa='j' AND e.$tipo LIKE '%$valor%' LIMIT 1");
        }
    } else if ($entidade == 'c') {
        if ($tipoDado == 'n')
            $tipo = 'cre_nome_fantasia';
        if ($tipoDado == 'c')
            $tipo = 'cre_cpf_cnpj';

        if ($pessoa == 'f') {
            $query = $CI->db->query("SELECT e.cre_cod as codigo, e.usuarios_responsavel_cod as recuperador_cod, u.usu_usuario_sis as recuperador_nome, e.cre_nome_fantasia as nome, e.cre_cpf_cnpj as cpf_cnpj, e.cre_endereco as endereco, e.cre_cidade as cidade, e.cre_prazo as prazo from credores e, usuarios u WHERE e.cre_pessoa='f' AND e.$tipo LIKE '%$valor%' AND u.usu_cod = e.usuarios_responsavel_cod  AND e.cre_ativo='1' LIMIT 1");
        } else if ($pessoa == 'j') {
            $query = $CI->db->query("SELECT e.cre_cod as codigo, e.usuarios_responsavel_cod as recuperador_cod, u.usu_usuario_sis as recuperador_nome, e.cre_nome_fantasia as nome, e.cre_cpf_cnpj as cpf_cnpj, e.cre_endereco as endereco, e.cre_cidade as cidade, e.cre_prazo as prazo from credores e, usuarios u WHERE e.cre_pessoa='j' AND e.$tipo LIKE '%$valor%' AND u.usu_cod = e.usuarios_responsavel_cod  AND e.cre_ativo='1' LIMIT 1");
        }
    }
    else
        return false;

    return $query->result();
}

function convDataBanco($data) {
    /* pega a data que vem do db nos formatos (2011-03-25 11:12:42 ou 2011-03-25)
     *  a data pode vir do banco de um formato ou de outro que retornar� nesse formato dd/mm/aaaa) */
    if (strlen($data) == 10) {
        $dia = substr($data, 8, 2);
        $mes = substr($data, 5, -3);
        $ano = substr($data, 0, -6);
        $data = $dia . '/' . $mes . '/' . $ano;
        return $data;
    } else {
        $dia = substr($data, 8, -9);
        $mes = substr($data, 5, -12);
        $ano = substr($data, 0, -15);

        $data = $dia . '/' . $mes . '/' . $ano;
        return $data;
    }
}

function convDataParaDb($data) {
    /* Converte a data do formato 31/12/2011 para 2011-12-31
     * OBS: Se $data chegar vazia o m�todo retornar� --
     */
    $dia = substr($data, 0, -8);
    $mes = substr($data, 3, -5);
    $ano = substr($data, 6);
    $data = $ano . '-' . $mes . '-' . $dia;
    return $data;
}

function tipoDeMensagem($msgTipo) {
    switch ($msgTipo) {

        case 1 :
            return "#FBC2C4";
            break;

        case 2 :
            return "#FFD324";
            break;

        case 3 :
            return "#8FBDE0";
            break;
        case 4 :
            return "#C6D880";
            break;
    }
}

function extenso($valor, $moedaSing, $moedaPlur, $centSing, $centPlur) {

    $centenas = array(0,
        array(0, "cento", "cem"),
        array(0, "duzentos", "duzentos"),
        array(0, "trezentos", "trezentos"),
        array(0, "quatrocentos", "quatrocentos"),
        array(0, "quinhentos", "quinhentos"),
        array(0, "seiscentos", "seiscentos"),
        array(0, "setecentos", "setecentos"),
        array(0, "oitocentos", "oitocentos"),
        array(0, "novecentos", "novecentos"));

    $dezenas = array(0,
        "dez",
        "vinte",
        "trinta",
        "quarenta",
        "cinq�enta",
        "sessenta",
        "setenta",
        "oitenta",
        "noventa");

    $unidades = array(0,
        "um",
        "dois",
        "tr�s",
        "quatro",
        "cinco",
        "seis",
        "sete",
        "oito",
        "nove");

    $excecoes = array(0,
        "onze",
        "doze",
        "treze",
        "quatorze",
        "quinze",
        "dezeseis",
        "dezesete",
        "dezoito",
        "dezenove");

    $extensoes = array(0,
        array(0, "", ""),
        array(0, "mil", "mil"),
        array(0, "milh�o", "milh�es"),
        array(0, "bilh�o", "bilh�es"),
        array(0, "trilh�o", "trilh�es"));

    $valorForm = trim(number_format($valor, 2, ".", ","));
    $valorForm .='';
    $valorExt = '';
    $inicio = 0;

    if ($valor <= 0) {

        return ( $valorExt );
    }

    for ($conta = 0; $conta <= strlen($valorForm) - 1; $conta++) {
        if (strstr(",.", substr($valorForm, $conta, 1))) {
            $partes[] = str_pad(substr($valorForm, $inicio, $conta - $inicio), 3, " ", STR_PAD_LEFT);
            if (substr($valorForm, $conta, 1) == ".") {
                break;
            }
            $inicio = $conta + 1;
        }
    }

    $centavos = substr($valorForm, strlen($valorForm) - 2, 2);

    if (!( count($partes) == 1 and intval($partes[0]) == 0 )) {
        for ($conta = 0; $conta <= count($partes) - 1; $conta++) {

            $centena = intval(substr($partes[$conta], 0, 1));
            $dezena = intval(substr($partes[$conta], 1, 1));
            $unidade = intval(substr($partes[$conta], 2, 1));

            if ($centena > 0) {

                $valorExt .= $centenas[$centena][($dezena + $unidade > 0 ? 1 : 2)] . ( $dezena + $unidade > 0 ? " e " : "" );
            }

            if ($dezena > 0) {
                if ($dezena > 1) {
                    $valorExt .= $dezenas[$dezena] . ( $unidade > 0 ? " e " : "" );
                } elseif ($dezena == 1 and $unidade == 0) {
                    $valorExt .= $dezenas[$dezena];
                } else {
                    $valorExt .= $excecoes[$unidade];
                }
            }

            if ($unidade > 0 and $dezena != 1) {
                $valorExt .= $unidades[$unidade];
            }

            if (intval($partes[$conta]) > 0) {
                $valorExt .= " " . $extensoes[(count($partes) - 1) - $conta + 1][(intval($partes[$conta]) > 1 ? 2 : 1)];
            }

            if ((count($partes) - 1) > $conta and intval($partes[$conta]) > 0) {
                $conta3 = 0;
                for ($conta2 = $conta + 1; $conta2 <= count($partes) - 1; $conta2++) {
                    $conta3 += ( intval($partes[$conta2]) > 0 ? 1 : 0);
                }

                if ($conta3 == 1 and intval($centavos) == 0) {
                    $valorExt .= " e ";
                } elseif ($conta3 >= 1) {
                    $valorExt .= ", ";
                }
            }
        }

        if (count($partes) == 1 and intval($partes[0]) == 1) {
            $valorExt .= $moedaSing;
        } elseif (count($partes) >= 3 and ((intval($partes[count($partes) - 1]) + intval($partes[count($partes) - 2])) == 0)) {
            $valorExt .= " de " + $moedaPlur;
        } else {
            $valorExt = trim($valorExt) . " " . $moedaPlur;
        }
    }

    if (intval($centavos) > 0) {

        $valorExt .= ( !empty($valorExt) ? " e " : "");

        $dezena = intval(substr($centavos, 0, 1));
        $unidade = intval(substr($centavos, 1, 1));

        if ($dezena > 0) {
            if ($dezena > 1) {
                $valorExt .= $dezenas[$dezena] . ( $unidade > 0 ? " e " : "" );
            } elseif ($dezena == 1 and $unidade == 0) {
                $valorExt .= $dezenas[$dezena];
            } else {
                $valorExt .= $excecoes[$unidade];
            }
        }

        if ($unidade > 0 and $dezena != 1) {
            $valorExt .= $unidades[$unidade];
        }

        $valorExt .= " " . ( intval($centavos) > 1 ? $centPlur : $centSing );
    }

    return ($valorExt);
}

function parcelaSituacao($sit, $data, $tipo='N') {
    $dataAtual = date('Y-m-d');

    if ($sit == 1) {
//        caso a parcela ja tenha sido paga
        if ($tipo == 'N') {
            $retorno = 'Pago';
        } else {
            $retorno = '#00f';
        }
    } else if ($sit == 2) {
//        caso a parcela ja tenha sido paga
        if ($tipo == 'N') {
            $retorno = 'Acordada';
        } else {
            $retorno = '#666';
        }
    } else {
//        caso a parcela ainda n�o tenha sido paga
        if ($tipo == 'N') {
//            caso o usuario queira o nome da situa��o
            if ($data <= $dataAtual) {
                //caso a parcela esteja vencida
                $retorno = 'Vencida';
            } else {
                //caso a parcela ainda n�o esteja vencida
                $retorno = 'Vincenda';
            }
        } else {
//            caso o usuario queira a cor da situa�ao
            if ($data < $dataAtual) {
                //caso a parcela esteja vencida
                $retorno = '#f00';
            } else {
                //caso a parcela ainda n�o esteja vencida
                $retorno = '#000';
            }
        }
    }
    return($retorno);
}

function diffDate($d1, $d2, $type='', $sep='-') {
    $d1 = explode($sep, $d1);
    $d2 = explode($sep, $d2);
    switch ($type) {
        case 'A':
            $X = 31536000;
            break;
        case 'M':
            $X = 2592000;
            break;
        case 'D':
            $X = 86400;
            break;
        case 'H':
            $X = 3600;
            break;
        case 'MI':
            $X = 60;
            break;
        default:
            $X = 1;
    }
    return floor((mktime(0, 0, 0, $d2[1], $d2[2], $d2[0]) - mktime(0, 0, 0, $d1[1], $d1[2], $d1[0])) / $X);
}

function calculaJuros() {
    return diffDate('2011-01-04', '2011-06-09', 'D');
}

function auditar($tipo, $user, $entidade = 'INDEFINIDO', $detalhes = 'NAO FORAM INFORMADOS DETALHES') {
    $CI = & get_instance();
    $CI->load->database();

    $query = '';
    $query .= '\'' . utf8_decode($tipo) . '\',';
    $query .= '\'' . $entidade . '\',';
    $query .= '\'' . $user . '\',';
    $query .= '\'' . utf8_decode($detalhes) . '\'';

    $resultado = $CI->db->query("CALL SP_CADASTRA_AUD(" . $query . ");");

    return $resultado;
}

