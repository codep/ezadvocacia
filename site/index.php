<?php
require 'flight/Flight.php';

class Layout {
	public $body='';
	public function render($b='',$data=array()) {
		if (!empty($b)) $this->body=$b;
		Flight::render('header', array(), 'header_content');
		Flight::render('menu', array('menu'=>$this->body), 'menu_content');
		Flight::render($this->body, $data, 'body_content');
		Flight::render('layout', array());
	}
}
$layout = new Layout();

Flight::route('/', function() use (&$layout) {
	$layout->render('home');
});

Flight::route('/quem-somos',function() use (&$layout) {
	$layout->render('quem-somos');
});

Flight::route('/servicos',function() use (&$layout) {
	$layout->render('servicos');
});

Flight::route('/contato',function() use (&$layout) {
	$layout->render('contato');
});

Flight::start();

?>
