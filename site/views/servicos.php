<div id="page">
	<div id="content">
		<h1>Nossos serviços</h1>
		<div class="hr"></div><!-- TABS -->
		<div id="tabs">
			<ul>
				<li>
					<a href="#tabs-1">Serviço 1</a>
				</li>
				<li>
					<a href="#tabs-2">Serviço 2</a>
				</li>
				<li>
					<a href="#tabs-3">Serviço 3</a>
				</li>
			</ul>
			<div id="tabs-1">
				<p><img class="image" src="assets/images/tabs_carousel_pic.jpg" alt="image" />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sit amet ipsum tortor.Sed volutpat purus purus, vitae fringilla velit. Nunc est nunc, dictum vestibulum ultrices sit amet, facilisis non est. Aliquam laoreet molestie felis, ac lobortis ipsum bibendum et. Integer vel sem molestie magna vestibulum varius. Pellentesque quis eros tortor, eu scelerisque eros. Maecenas eu est at arcu interdum cursus. Curabitur porttitor elementum sodales. Praesent non metus ac nisl hendrerit gravida. Aliquam ultrices porta tempor. Quisque massa mi, semper quis placerat ut, interdum at mi.Quisque mi neque, scelerisque in gravida vel, blandit sit amet mauris. Praesent posuere, dui at congue rhoncus, metus ante euismod ipsum, vel tempus ante felis ut justo. Nulla mollis iaculis diam vitae placerat.
				</p>
			</div>
			<div id="tabs-2">
				<p>
					Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.
				</p>
			</div>
			<div id="tabs-3">
				<p>
					Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.
				</p>
				<p>
					Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.
				</p>
			</div>
		</div>
		<h2>Clientes</h2>
		<!-- CAROUSEL -->
		<div id="carousel">
			<a class="buttons prev" href="#">left</a>
			<div class="viewport">
				<ul class="overview">
					<li><img src="assets/images/tabs_carousel_pic.jpg" alt="" />
					</li>
					<li><img src="assets/images/tabs_carousel_pic.jpg" alt="" />
					</li>
					<li><img src="assets/images/tabs_carousel_pic.jpg" alt="" />
					</li>
					<li><img src="assets/images/tabs_carousel_pic.jpg" alt="" />
					</li>
				</ul>
			</div><a class="buttons next" href="#">right</a>
		</div>
	</div><!-- #content ends -->
</div>