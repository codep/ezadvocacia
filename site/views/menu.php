<div id="sidebar-content">
	<!-- LOGO -->
	<div id="logo">
		<a href="assets/index.html"><img src="assets/images/logo.png" alt="" /></a>
	</div><!-- MENU -->
	<ul id="menu">
		<li class="<?php echo (empty($menu) || $menu=='home') ? 'current' : ''; ?>">
			<a href=".">Home</a>
		</li>
		<li class="<?php echo ($menu=='quem-somos') ? 'current' : ''; ?>">
			<a href="quem-somos">Quem Somos</a>
		</li>
		<li class="<?php echo ($menu=='servicos') ? 'current' : ''; ?>">
			<a href="servicos">Servicos</a>
		</li>
		<li class="<?php echo ($menu=='contato') ? 'current' : ''; ?>">
			<a href="contato">Contato</a>
		</li>
	</ul>
</div>