<div id="page">
	<div id="content">
		<h1>Quem Somos</h1><div class="hr"></div>
		<ul class="list_num">
			<li>
				<span>1</span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus varius, risus vel euismod tempor, justo nibh lacinia leo, quis eleifend diam tortor quis turpis.
			</li>
			<li>
				<span>2</span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus varius, risus vel euismod tempor, justo nibh lacinia leo, quis eleifend diam tortor quis turpis.
			</li>
			<li>
				<span>3</span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus varius, risus vel euismod tempor, justo nibh lacinia leo, quis eleifend diam tortor quis turpis.
			</li>
		</ul>
		<blockquote class="align_right">
			<p>
				This is a quote in a lorem ipsum text. Vivamus varius, risus vel euismod tempor!
			</p>
		</blockquote>
		<p class="justify">
			<span class="dropcap">L</span>orem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque id rhoncus orci. Mauris elementum, quam sed volutpat iaculis, nisi mauris tempor erat, sit amet vestibulum lectus neque quis justo. Duis purus purus, semper eu consectetur nec, interdum id felis. In at nulla sit amet velit sollicitudin cursus at id magna. Pellentesque lorem tellus, sollicitudin in mollis eu, gravida sit amet nunc. Cras sit amet tristique eros. Etiam porttitor ultricies lacus id ornare. Phasellus lorem velit, suscipit nec ullamcorper scelerisque, condimentum at urna.
		</p>
		<blockquote class="align_left">
			<p>
				This is a quote in a lorem ipsum text. Vivamus varius, risus vel euismod tempor!
			</p>
		</blockquote>
		<p class="justify">
			Nullam consectetur commodo lorem a porta. Etiam lacinia, ligula id blandit pharetra, sapien felis interdum augue, ut lacinia orci est et nisi. Ut dapibus sodales ligula, eu eleifend erat mollis sed. Quisque elit neque, ultrices sit amet hendrerit at, ullamcorper ut leo. Mauris mollis eros ut orci dignissim faucibus. Donec quis nisl urna, a pretium leo. Nullam ac libero non dolor interdum pulvinar vel ac massa. Suspendisse tempus tincidunt nunc sit amet cursus. Vestibulum et facilisis libero.
		</p>
		<blockquote class="">
			<p>
				Lorem ipsum dolor sit amet, adipisicing elit, sed do eiusmod tempor ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<cite>- Joseph Smith, CEO</cite>
			</p>
		</blockquote>
	</div><!-- #content ends -->
</div>