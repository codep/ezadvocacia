<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>EZ Advocacia - Especializada em Cobrança</title>
		<meta name="description" content="EZ Advocacia - Especializada em Cobrança" />
		<!-- CSS -->
		<link href='assets/fonts/sansation.css' rel="stylesheet" type="text/css" />
		<!-- Get any font from here easily: http://www.google.com/webfonts -->
		<link href="assets/css/style.css" rel="stylesheet" type="text/css" />
		<link href="assets/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/nivo-slider.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/styler-farbtastic.css" rel="stylesheet" type="text/css" />
		<!-- UPDATE BROWSER WARNING IF IE 7 OR LOWER -->
		<!--[if lt IE 8]>
			<link href="assets/css/stop_ie.css" rel="stylesheet" type="text/css" />
		<![endif]--
		<!-- JAVASCRIPTS -->
		<script type="text/javascript" src="assets/js/jquery.min.js"></script>
		<script type="text/javascript" src="assets/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script type="text/javascript" src="assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<script type="text/javascript" src="assets/js/jquery.nivo.slider.js"></script>
		<script type="text/javascript" src="assets/js/jquery.bgslider.js"></script>
		<script type="text/javascript" src="assets/js/preloader.js"></script>
		<script type="text/javascript" src="assets/js/farbtastic.js"></script>
		<script type="text/javascript" src="assets/js/basic.js"></script>
		<script type="text/javascript" src="assets/js/styler.js"></script>
		<script type="text/javascript" src="assets/js/jquery.tinycarousel.js"></script>
		<!-- PAGE OPENING ANIMATION -->
		<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery('#page').css({
					'display' : 'inline',
					'width' : '300px',
					'overflow' : 'hidden',
					'margin-right' : '340px'
				});
				jQuery('#sidebar').css({
					'margin-left' : '326px'
				});
			});
			jQuery(window).load(function() {
				jQuery('#hp_preloader').delay(800).animate({
					'opacity' : '0'
				}, 1400, function() {
					jQuery('#slider-nivo').nivoSlider({
						controlNav : true,
						controlNavThumbs : false,
						keyboardNav : false,
						pauseOnHover : false,
						prevText : '',
						nextText : '',
						effect : 'fade',
						animSpeed : 300,
						pauseTime : 4000
					});
					jQuery(this).remove();
					jQuery('#sidebar').delay(800).animate({
						'margin-left' : '0px'
					}, 2100);
					jQuery('#page').delay(800).animate({
						'margin-right' : '0px',
						'width' : '666px'
					}, 2100);
				});
			});
		</script>
		<meta charset="UTF-8">
	</head>
	<body>
		<?php echo $header_content; ?>
		<!-- SITE WRAPPER -->
		<div id="wrapper">
			<!-- PAGE WITH CONTENTS -->
			<?php echo $body_content; ?>
			<!-- #page ends -->
			<!-- SIDEBAR -->
			<div id="sidebar">
				<div id="sidebar-color"></div>
				<!-- SIDEBAR COLOR LAYER -->
				<div id="sidebar-border"></div>
				<!-- SIDEBAR BORDER LAYER -->
				<div id="sidebar-light"></div>
				<!-- SIDEBAR RADIAL GRADIENT LIGHT LAYER -->
				<div id="sidebar-texture"></div>
				<!-- SIDEBAR TEXTURE LAYER -->
				<!-- SIDEBAR CONTENT LAYER -->
				<?php echo $menu_content; ?>
				<!-- #sidebar-content ends -->
				<!-- SOCIAL ICONS ON SIDEBAR'S BOTTOM PART -->
				<ul id="sidebar-bottom">
					<?php /*
					<li>
						<a href="#"><img src="assets/images/sidebar_icons/facebook.png" alt="" /></a>
					</li>
					<li>
						<a href="#"><img src="assets/images/sidebar_icons/twitter.png" alt="" /></a>
					</li>
					<li>
						<a href="#"><img src="assets/images/sidebar_icons/rss.png" alt="" /></a>
					</li>
					 *  */ ?>
					 */
				</ul><!-- COPYRIGHT TEXT -->
				<p id="copyright">
					Desenvolvido por <a href="http://www.codepoint.com.br">CodePoint</a>
				</p>
			</div><!-- #sidebar ends -->
		</div>
		<!-- #wrapper ends -->
		<!-- BACKGROUND SLIDER -->
		<div id="bg_slider">
			<img src="assets/images/bgslider-1.jpg" alt="" />
			<img src="assets/images/bgslider-2.jpg" alt="" />
			<img src="assets/images/bgslider-3.jpg" alt="" />
		</div>
	</body>
</html>