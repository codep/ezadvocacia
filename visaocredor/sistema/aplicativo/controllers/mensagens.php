<?php

class Mensagens extends Controller {

    function Mensagens() {
        parent::Controller();
    }

    function _remap($link) {

        $this->data['title'] = "EZ Advocacia e Cobran�as :: Mensagens";

        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME

        $this->load->helper("funcoes_helper");

        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'jquery.maxlength.js','plugin/jquery.maskedinput'));

        $this->load->model('mensagens_model', 'md');
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        //IR PARA O RESPECTIVO FLUXO DA URL
        if ($link == "entrada") {
            $this->_entrada();
        } else if ($link == "saida") {
            $this->_saida();
        } else if ($link == "nova") {
            $this->_nova();
        } else if ($link == "novaMensagem") {
            $this->_novaMensagem();
        } else if ($link == "excluirMensagem") {
            $this->_excluirMensagem();
        } else if ($link == "marcarComoLida") {
            $this->_marcarComoLida();
        } else if ($link == "responder") {
            $this->_responder();
        }
    }

//    function _entrada() {
//        /* Pega todas as mensagens do usu�rio da sess�o (caixa de entrada) */
//        $codUsuariDaSessao = $this->session->userdata('usucod'); //pega o c�digo do usu�rio da sess�o
//        $this->data['minhasMensagensRecebidas'] = $this->md->getMinhasMensagensRecebidas($codUsuariDaSessao);
//        $this->inicore->loadSidebar();
//        $this->inicore->loadview('men_entrada', $this->data);
//    }

    function _novaMensagem() {/* Recebe os dados digitados no formul�rio de nova mensagem */
    $inaCod = $this->input->post('inaCod');//vari�vel usada apenas para redirecionar o usu�rio para a ficha que ele estava antes de enviar a mensagem
    $recupCod = $this->input->post('recuperadorPadrao');//vari�vel usada apenas para redirecionar o usu�rio para a ficha que ele estava antes de enviar a mensagem

        $this->load->library('form_validation');//carregando o helper de formul�rio
        /* array que armazena os dados digitados nos campos do formul�rio */
        $dados = array(
            array(
                'field' => 'titulo',
                'label' => 'T�tulo',
                'rules' => 'required' //indica que o campo � necess�rio
            ),
            array(
                'field' => 'destinatario',
                'label' => 'Destinat�rio',
                'rules' => 'required'
            ),
            array(
                'field' => 'mensagem',
                'label' => 'Mensagem',
                'rules' => 'required'
                ));

        $this->form_validation->set_message('required', "O campo %s � obrigat�rio"); //mensagem retornada se o campo n�o for preenchido. ("%s" imprime o nome do campo que n�o foi preenchido).
        $this->form_validation->set_rules($dados); //valida os campos que foi armazenado no array acima.

        if ($this->form_validation->run() == FALSE) {;
            /* se algum campo obrigat�rio n�o for preenchido da uma mensagem de erro e redireciona para a lista de inadimplentes*/
            $mensagem = validation_errors();
            $this->inicore->setMensagem('error', $mensagem);
            redirect(base_url() . 'inadimplentes');
        } else if ($this->input->post("destinatario") == 'recuperador') {//se selecionado recuperador no combobox

            $dados = array(
                'usuarios_usu_cod' => $this->input->post('recuperadorPadrao'), //recebe o cod do destinat�rio. Campo hidden no form da view
                'men_titulo' => utf8_encode(removeCE_Upper($this->input->post('titulo'))),
                'men_remetente' => $this->session->userdata('usucod'), //pega o nome fantasia do credor logado
                'men_texto' => utf8_encode(removeCE_Upper($this->input->post('mensagem'))), //campo mensagem
            );
            
        $this->md->setNovaMensagem($dados); //chama o m�todo setNovaMensagem() do model

        $this->inicore->setMensagem('success', 'Mensagem enviada com sucesso para o recuperador!');
        redirect(base_url() . 'inadimplentes/ficha/inaCod:'.$inaCod. '/recupCod:'. $recupCod);

        } else if($this->input->post("destinatario") == 'diretoria') {// se escolher diretoria no combobox vai mensagem para todos usu�rios dos tipo master e administrador
            $diretores = $this->md->getSupervisores();

            //no foreach esse array vai recebar a posi��o usuarios_usu_cod que armazena o c�digo de cada usu�rio master e administrador
            $dados = array(
                'men_titulo' => utf8_encode(removeCE_Upper($this->input->post('titulo'))),
                'men_remetente' => $this->session->userdata('usucod'), //pega o nome fantasia do credor logado
                'men_texto' => utf8_encode(removeCE_Upper($this->input->post('mensagem'))), //campo mensagem
            );

            //esse foreach insere a mensagem para cada usu�rio master e administrador
            foreach ($diretores as $diretor){
                $dados['usuarios_usu_cod'] = $diretor->usu_cod;
                $this->md->setNovaMensagem($dados); //chama o m�todo setNovaMensagem() do model
            }

            $this->inicore->setMensagem('success', 'Mensagem enviada com sucesso para todos os diretores.');
            redirect(base_url() . 'inadimplentes/ficha/inaCod:'.$inaCod. '/recupCod:'. $recupCod);
        }else if($this->input->post("destinatario") == 'recuperadorDiretoria'){//se o usu�rio escolher Recuperador e Diretoria no combobox
            //Manda mensagem para o recuperador e para todos os diretores

            $diretores = $this->md->getSupervisores();//pega todos os diretores

            /*------------- Enviando a mensagem para o recuperador padr�o -------------*/
            $dados = array(
                'usuarios_usu_cod' => $this->input->post('recuperadorPadrao'), //recebe o cod do destinat�rio. Campo hidden no form da view
                'men_titulo' => utf8_encode(removeCE_Upper($this->input->post('titulo'))),
                'men_remetente' => $this->session->userdata('usucod'), //pega o nome fantasia do credor logado
                'men_texto' => utf8_encode(removeCE_Upper($this->input->post('mensagem'))), //campo mensagem
            );
            $this->md->setNovaMensagem($dados); //chama o m�todo setNovaMensagem() do model
            /*------------- Fim enviando a mensagem para o recuperador padr�o -------------*/

            /*-------------------- Enviando mensagem para todos os diretores --------------------*/
            //esse foreach insere a mensagem para cada usu�rio master e administrador
            foreach ($diretores as $diretor){
                $dados['usuarios_usu_cod'] = $diretor->usu_cod;
                $this->md->setNovaMensagem($dados); //chama o m�todo setNovaMensagem() do model
            }
            /*-------------------- Fim enviando mensagem para todos os diretores --------------------*/

            $this->inicore->setMensagem('success', 'Mensagem enviada com sucesso para todos os diretores e para o recuperador');
            redirect(base_url() . 'inadimplentes/ficha/inaCod:'.$inaCod. '/recupCod:'. $recupCod);
        }else{
            $this->inicore->setMensagem('error', 'Sua mensagem n�o pode ser enviada');
            redirect(base_url() . 'inadimplentes');
        }
    }

}
