<?php
//O contralador de home � um controlador bem simples n�o ha nada que mere�a um
//comet�rio escpcifico, pois ele simplesmete carrega os parametros e informa��es
//necessarios para a exibi��o da  home
class Home extends Controller {

    function Home() {
        parent::Controller();
        // CONFIGURA��ES DE PAGINA
        $this->data = array(
            'title' => 'EZ Advocacia e Cobranças :: Vis�o do Credor',
            'description' => '',
            'keywords' => ''
        );
        $this->load->model('Homemodel', 'md');
    }

    function index() {
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/Blue'));
        $this->load->helper("funcoes_helper");
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js','plugin/jquery.maskedinput'));
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        $this->inicore->loadSidebar();
        //passa para a sess�o o menu que ficar� aberto
        $this->session->set_userdata('menusel', '0');

//        redirect(base_url() . 'inadimplentes');//esta linha esta aqui para nao aparecer a home, mais pra frente vai tirar essa linha quando a pagina home estiver ok

        $this->inicore->loadview('home', $this->data);
    }
}
