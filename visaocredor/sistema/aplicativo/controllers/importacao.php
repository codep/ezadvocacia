<?php

class Importacao extends Controller {

    function Importacao() {
        parent::Controller();
    }

    function _remap($link) {

        $this->data['title'] = "EZ Advocacia e Cobran�as ::  Importa��o";

        $this->inicore->addcss(array('reset', 'style', 'colors/blue')); // CSS HOME

        $this->load->helper("funcoes_helper");

        $this->load->model('Importmodel', 'md');

        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput', 'jquery.moeda.js'));
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
        //IR PARA O RESPECTIVO FLUXO DA URL
        if ($link == "importar") {
            $this->_importar();
        } else if ($link == "importarjson") {
            $this->_importarjson();
        } else if ($link == "principal") {
            $this->_importacao();
        } else if ($link == "exibirDados") {
            $this->_exibirDados();
        } else {
            $this->inicore->setMensagem('error', 'Comportamento inesperado.', true);
            redirect(getBackUrl());
        }
    }

    function _importarjson() {
    	
		try {
			
	        // Pasta onde o arquivo vai ser salvo
	        $_UP['pasta'] = APPPATH . 'upload/';
	        // Tamanho m�ximo do arquivo (em Bytes)
	        $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
	        // Array com as extens�es permitidas
	        $_UP['extensoes'] = array('json');
	        $_UP['renomeia'] = true;
			
	        // Array com os tipos de erros de upload do PHP
	        $_UP['erros'][0] = 'N�o houve erro';
	        $_UP['erros'][1] = 'O arquivo no upload � maior do que o limite do PHP';
	        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
	        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
	        $_UP['erros'][4] = 'N�o foi feito o upload do arquivo';
			
	        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
	        if ($_FILES['arquivo']['error'] != 0) {
	        	throw new Exception("N�o foi poss�vel fazer o upload, erro:<br />" . $_UP['erros'][$_FILES['arquivo']['error']], 1);
	        }
	        // Caso script chegue a esse ponto, nao houve erro com o upload e o PHP pode continuar
	        // Faz a verificacao da extensao do arquivo
	        $extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
	
	        if (array_search($extensao, $_UP['extensoes']) === false) {
	        	throw new Exception("Por favor, envie arquivos com as seguintes extens�es: .json", 1);
	        }
	        // Faz a verificacaoo do tamanho do arquivo
	        else if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
	        	throw new Exception("O arquivo enviado � muito grande, envie arquivos de at� 2Mb.", 1);
	        }
	        // O arquivo passou em todas as verifica��es, hora de tentar mov�-lo para a pasta
	        else {
	        	
	            $_FILES['arquivo']['name'] = url_amigavel($_FILES['arquivo']['name']); //remove caracteres especiais
	            // Primeiro verifica se deve trocar o nome do arquivo
	
	            if ($_UP['renomeia'] == true) {
	                // Cria um nome baseado no UNIX TIMESTAMP atual
	                $nome_final = time() . $_FILES['arquivo']['name'];
	            } else {
	                // Mant�m o nome original do arquivo
	                $nome_final = $_FILES['arquivo']['name'];
	            }
	
	            // Depois verifica se � poss�vel mover o arquivo para a pasta escolhida
	            $destFile = $_UP['pasta'] . $nome_final;
	            if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $destFile)) {
	                // Upload efetuado com sucesso
	                chmod($destFile, 0666); //SETA PERMISS�ES NO ARQUIVO
	                //exibe uma mensagem e um link para o arquivo
	            } else {
	                // N�o foi poss�vel fazer o upload, provavelmente a pasta est� incorreta
	                throw new Exception("N�o foi poss�vel enviar o arquivo, erro interno de permiss�o do servidor, por favor entre em contato com o administrador do sistema.", 1);
	            }
				
			}

	        if (!isset($destFile) && empty($destFile) && !is_readable($destFile) ) {
	        	throw new Exception("Desculpe, ocorreu um erro ao acessar o arquivo ou voc� n�o tem permiss�o para ler esse arquivo. Consulte o Administrador do sistema. ", 1);
	        }
			
			$ponteiro = fopen($destFile, "r");			
	        $dadosTxt = '';
	        while (!feof($ponteiro)) {
	            $dadosTxt .= fgets($ponteiro, 4096);
	        }
	        fclose($ponteiro);
			
			$dadosTxt = removeCE_Upper($dadosTxt); //REMOVE CARACTERES ESPECIAIS
			$dadosTxt = str_replace(array("\r", "\n"), " ", $dadosTxt); //REMOVE QUEBRA DE LINHAS
			$dadosTxt = str_replace('\"', '"', $dadosTxt); //REMOVE STRIP DOUBLE QUOTE
			$dadosTxt = trim($dadosTxt);
			
			//echo '<pre>'; var_dump($dadosTxt); echo '<pre>'; die(); //DEBUG
			if (empty($dadosTxt)) {
				throw new Exception('Ocorreu um erro ao efetuar a leitura da arquivo. Por favor verifique se ele est� conforme o layout necess�rio, vazio ou se foi corrompido. <span style="font-size:8px; display:block;">Arquivo: ' . $fileName . '</span>', 1);	
			}

			if ($this->_isJSON($dadosTxt)==false) {
				throw new Exception('O arquivo enviado n�o possui uma estrutura JSON V�LIDA! <span style="font-size:8px; display:block;">Arquivo: ' . $fileName . '</span>', 1);	
			}
						
			$dadosJson = json_decode($dadosTxt, true);
			$dadosTmp = '';
			foreach ($dadosJson as $creCNPJ => $inadimplentes) {
				//TESTA SE EST� NO FORMATO CORRETO
				$expreg = '/(^\d{3}\.\d{3}\.\d{3}\-\d{2}$)|(^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$)/';
				if(!preg_match($expreg, $creCNPJ)) {
					//CASO N�O ESTEJA, APLICA A FORMATA��O
					if (strlen($creCNPJ)==14) {
						$creCNPJ = $this->_mask($creCNPJ, '##.###.###/####-##');
					} else if (strlen($creCNPJ)==11) {
						$creCNPJ = $this->_mask($creCNPJ, '###.###.###-##');
					} else {
						throw new Exception('O documento ['.$creCNPJ.'] do credor � INV�LIDO! Formato esperado: CNPJ: 00.000.000/0000-00 (ou 14 n�meros) ou CPF: 000.000.000-00 (ou 11 n�meros)');
					}
				}
				//RECUPERA O C�DIGO DO CREDOR				
				$creCod = $this->md->getCreCod($creCNPJ)->cre_cod;
				if (empty($creCod)) {
					throw new Exception('CREDOR INV�LIDO! O credor n�o possui cadastro em nosso sistema!');
				}
				if (!isset($inadimplentes['INADIMPLENTES'])) {
					throw new Exception('Dados do inadimplente est�o incorretos, voc� precisa informar o bloco INADIMPLENTES!');
				}
				$erros='';
				foreach($inadimplentes['INADIMPLENTES'] as $k=>$inadimplente) {
					
					try {
						
						//echo '<pre>'; var_dump($inadimplente); echo '<pre>'; die(); //DEBUG
						$dadosBase = array(
							'TIPOPESSOA'=>'',
							'SEXO'=>'',
							'ESTADOCIVIL'=>'', //$estadoCivil,
							'NOME'=>'',
							'CPFCNPJ'=>'',
							'RGIE'=>'',
							'ENDERECO'=>'',
							'BAIRRO'=>'',
							'COMPLEMENTO'=>'',
							'CEP'=>'', //$inaCep,
							'CIDADE'=>'',
							'UF'=>'',
							'TELEFONERES'=>'',
							'TELEFONEREC'=>'',
							'TELEFONECOM'=>'', 
							'CELULAR1'=>'',
							'CELULAR2'=>'',
							'CELULAR3'=>'',
							'EMAIL'=>'',
							'EMAILALT'=>'', //EMAIL2
							'OBSERVACOES'=>'',
							'CONJUGENOME'=>'',
							'CONJUGECPF'=>'',
							'CONJUGEFONE'=>'', //$inaConjFone,
							'CONJUGEENDERECO'=>'', //$inaConjEnd,
							'CONJUGECIDADE'=>'', //$inaConjCidade,
							'CONJUGEUF'=>'', //$inaConjUF,
							'PAINOME'=>'', //$inaPaiNome,
							'PAICPF'=>'',
							'PAIFONE'=>'', //$inaPaiFone,
							'PAIENDERECO'=>'', //$inaPaiEnd,
							'PAICIDADE'=>'', //$inaPaiCidade,
							'PAIUF'=>'', //$inaPaiUF,
							'MAENOME'=>'', //$inaMaeNome,
							'MAECPF'=>'',
							'MAEFONE'=>'', //$inaMaeFone,
							'MAEENDERECO'=>'', //$inaMaeEnd,
							'MAECIDADE'=>'', //$inaMaeCidade,
							'MAEUF'=>'', //$inaMaeUF
							'DIVIDAS'=>'' //BLOCO COM AS DIVIDAS
						);
						//PREENCHE OS CAMPOS DO DADOS BASE
						foreach($dadosBase as $k=>$db) if (isset($inadimplente[$k])) $dadosBase[$k] = $inadimplente[$k];
						
						if (empty($dadosBase['DIVIDAS']) || !is_array($dadosBase['DIVIDAS'])) {
							throw new Exception('As d�vidas do inadimplente '.$dadosBase['NOME'].' n�o foram informadas.');
						} else {
							//PREENCHE OS CAMPOS DO DADOS BASE
							foreach($dadosBase['DIVIDAS'] as $k=>$divida) {
								//echo '<pre>'; var_dump($divida); echo '<pre>'; die(); //DEBUG
								//DADOS DIVIDAS
								$dadosDIV = array(
									'TIPODOC'=>'',
									'BANCO'=>'',
									'AGENCIA'=>'',
									'ALINEA'=>'',
									'DATAEMISSAO'=>'',
									'NUMPARCELAS'=>'',
									'VENCIMENTOINICIAL'=>'',
									'INFORMACOES'=>'',
									'VALORTOTAL'=>'',
									'PARCELAS'=>''
								); 
								foreach($dadosDIV as $kk=>$dv) if (isset($divida[$kk])) $dadosDIV[$kk] = $divida[$kk];
								
								if (empty($dadosDIV['PARCELAS']) || !is_array($dadosDIV['PARCELAS'])) {
									throw new Exception('As parcelas da d�vida '.($k+1).' do inadimplente '.$dadosBase['NOME'].' n�o foram informadas');
								} else {
									$parcTotal=0;
									
									//DADOS BASE PARA AS PARCELAS
									foreach ($dadosDIV['PARCELAS'] as $kk => $parcela) {
										$dadosPARC = array(
											'NUMPARCELA'=>'',
											'NUMDOC'=>'',
											'VENCIMENTO'=>'',
											'VALOR'=>''
										);
										foreach($dadosPARC as $kkk=>$dp) if (isset($parcela[$kkk])) $dadosPARC[$kkk] = $parcela[$kkk];
										
										//SETA O PRIMEIRO VENCIMENTO NO BLOCO DIVIDA
										if (empty($dadosDIV['VENCIMENTOINICIAL']) && $kk==0) $dadosDIV['VENCIMENTOINICIAL']=$dadosPARC['VENCIMENTO'];
										
										//PARA O TOTAL DA PARCELA NO PARCTOTAL
										$parcTotal= $parcTotal + floatVal($dadosPARC['VALOR']);
										
										//ATUALIZA O BLOCO DIVIDA
										$dadosDIV['PARCELAS'][$kk] = $dadosPARC;
									}
	
									//ATUALIZA O TOTAL DE PARCELAS, CASO DIFERENTE
									if (empty($dadosDIV['NUMPARCELAS']) || count($dadosDIV['PARCELAS']) != $dadosDIV['NUMPARCELAS']) $dadosDIV['NUMPARCELAS']=count($dadosDIV['PARCELAS']); 
	
									//ATUALIZA O TOTAL DA DIVIDA, CASO DIFERENTE
									if (empty($dadosDIV['VALORTOTAL']) || $dadosDIV['VALORTOTAL']!=$parcTotal) $dadosDIV['VALORTOTAL']=$parcTotal;
								}
								
								//AGRUPA NOVOS VALORES A D�VIDA
								$dadosBase['DIVIDAS'][$k] = $dadosDIV;
							}
						}
						//echo '<pre>'; var_dump($dadosBase); echo '<pre>'; die(); //DEBUG
						
						//TESTA SE EST� NO FORMATO CORRETO
						$expreg = '/(^\d{3}\.\d{3}\.\d{3}\-\d{2}$)|(^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$)/';
						if(!preg_match($expreg, $dadosBase['CPFCNPJ'])) {
							if (strlen($dadosBase['CPFCNPJ'])==14) {
								$dadosBase['CPFCNPJ'] = $this->_mask($dadosBase['CPFCNPJ'], '##.###.###/####-##');
							} else if (strlen($dadosBase['CPFCNPJ'])==11) {
								$dadosBase['CPFCNPJ'] = $this->_mask($dadosBase['CPFCNPJ'], '###.###.###-##');
							} else {
								throw new Exception('O documento ['.$dadosBase['CPFCNPJ'].'] do inadimplente '.$dadosBase['NOME'].' � INV�LIDO! Formato esperado: CNPJ: 00.000.000/0000-00 (ou 14 n�meros) ou CPF: 000.000.000-00 (ou 11 n�meros)');
							}
							
						}	
						//RECUPERA O C�DIGO DO INADIMPLENTE
						$inaCod = $this->md->getInaCod($dadosBase['CPFCNPJ']);
						$inaCod = isset($inaCod->ina_cod) ? $inaCod->ina_cod : '';
						
		                if (empty($inaCod)) {
		                	//VERIFICA SE FOI INFORMADO O TIPO PESSOA PARA ESSE INADIMPLENTE, CASO N�O FOR�A 'F'
		                	if (!isset($dadosBase['TIPOPESSOA']) || empty($dadosBase['TIPOPESSOA']) || (strtoupper($dadosBase['TIPOPESSOA'])!='F' && strtoupper($dadosBase['TIPOPESSOA'])!='J')) {
		                		$dadosBase['TIPOPESSOA']='F';
		                	}
							//CADASTRA O INADIMPLENTE
							$ret = $this->md->cadInadArray($dadosBase);
							//RECUPERA O C�DIGO DO INADIMPLENTE CADASTRADO
							$inaCod = $this->md->getInaCod($dadosBase['CPFCNPJ'])->ina_cod;
							$inaDup = 0;
		                } else {
	                        $inaDup = 1;
		                }
						$ret = $this->md->cadInadArray($dadosBase, $inaCod, $inaDup);
						
						//CADASTRAR DIVIDAS
						if (isset($dadosBase['DIVIDAS']) && is_array($dadosBase['DIVIDAS']) && count($dadosBase['DIVIDAS'])>0) {
							foreach($dadosBase['DIVIDAS'] as $di=>$divida) {
								
								//VERIFICA SE A D�VIDA � DUPLICADA
								$divCod = $this->md->dupDivArray($divida, $creCod);
								if (empty($divCod)) {
									//GERA UMA NOVA D�VIDA E RETORNA O CODIGO DELA.
									$divCod = $this->md->cadDivArray($divida, $creCod);	
									//CADASTRA PARCELAS
									foreach($divida['PARCELAS'] as $parcela) {
										$ret = $this->md->cadDivParcArray($parcela, $divCod);
									}
									$dadosBase['DIVIDAS'][$di]['DUPLICADA']=$divDup=0;
									
									//SE CHEGOU AT� AQUI SEM ERROS, ENT�O CADASTRA A IMPORTA��O!
									$this->md->cadastraImportacao($inaCod, $divDup, 0, $divCod);
								} else {
									$dadosBase['DIVIDAS'][$di]['DUPLICADA']=$divDup=1;
								}
								
							}
						}	
						
						$dadosTmp[$creCNPJ][]=$dadosBase;
						
					} catch(Exception $e) {
						$erros[$k][]=$e->getMessage();
					}
				}//ENDFOREACH INADIMPLENTE
				$this->data['erros'] = $erros;
				
			}//ENDFOREACH CREDOR
					
			//echo '<pre>'; var_dump($dadosJson); echo '<pre>'; die(); //DEBUG
		} catch(Exception $e) {
			$this->data['mensagem'] = $e->getMessage();
		}	
		
		$this->data['registros'] = $dadosTmp;
        $this->data['title'] = "EZ Cobran�a :: Importa��o";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue'));
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput'));
        $this->session->set_userdata('menusel', '38'); //DETERMINA O MENU QUE FICARA ABERTO
        $this->inicore->loadSidebar(); //barra lateral
        $this->inicore->loadview('importarjson', $this->data);
    }

    function _importar() {
    	
		try {
	        
	        //die($_FILES['arquivo']['name'].' --- '.$_FILES['arquivo']['tmp_name']);
	
	        $dividasIncluidas = '';
	//        die('Ainda nao ta pronto, n�o fu�e');
	        // Pasta onde o arquivo vai ser salvo
	        $_UP['pasta'] = APPPATH . 'upload/';
	        // Tamanho m�ximo do arquivo (em Bytes)
	        $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
	        // Array com as extens�es permitidas
	        $_UP['extensoes'] = array('txt');
	
	        $_UP['renomeia'] = true;
	
	        // Array com os tipos de erros de upload do PHP
	        $_UP['erros'][0] = 'N�o houve erro';
	        $_UP['erros'][1] = 'O arquivo no upload � maior do que o limite do PHP';
	        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
	        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
	        $_UP['erros'][4] = 'N�o foi feito o upload do arquivo';
	
	        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
	        if ($_FILES['arquivo']['error'] != 0) {
	        	throw new Exception("N�o foi poss�vel fazer o upload, erro:<br />" . $_UP['erros'][$_FILES['arquivo']['error']], 1);
	        }
	
	        // Caso script chegue a esse ponto, n�o houve erro com o upload e o PHP pode continuar
	        // Faz a verifica��o da extens�o do arquivo
	        $extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
	
	        if (array_search($extensao, $_UP['extensoes']) === false) {
	        	throw new Exception("Por favor, envie arquivos com as seguintes extens�es: txt", 1);
	        }
	        // Faz a verifica��o do tamanho do arquivo
	        else if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
	        	throw new Exception("O arquivo enviado � muito grande, envie arquivos de at� 2Mb.", 1);
	        }
	        // O arquivo passou em todas as verifica��es, hora de tentar mov�-lo para a pasta
	        else {
	        	
	            $_FILES['arquivo']['name'] = url_amigavel($_FILES['arquivo']['name']); //remove caracteres especiais
	            // Primeiro verifica se deve trocar o nome do arquivo
	
	            if ($_UP['renomeia'] == true) {
	                // Cria um nome baseado no UNIX TIMESTAMP atual
	                $nome_final = time() . $_FILES['arquivo']['name'];
	            } else {
	                // Mant�m o nome original do arquivo
	                $nome_final = $_FILES['arquivo']['name'];
	            }
	
	            // Depois verifica se � poss�vel mover o arquivo para a pasta escolhida
	            $destFile = $_UP['pasta'] . $nome_final;
	            if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $destFile)) {
	                // Upload efetuado com sucesso
	                chmod($destFile, 0666); //SETA PERMISS�ES NO ARQUIVO
	                //exibe uma mensagem e um link para o arquivo
	            } else {
	                // N�o foi poss�vel fazer o upload, provavelmente a pasta est� incorreta
	                throw new Exception("N�o foi poss�vel enviar o arquivo, erro interno de permiss�o do servidor, por favor entre em contato com o administrador do sistema.", 1);
	            }
	        }

	        if (!isset($destFile) && empty($destFile) && !is_readable($destFile) ) {
	        	throw new Exception("Desculpe, ocorreu um erro ao acessar o arquivo ou voc� n�o tem permiss�o para ler esse arquivo. Consulte o Administrador do sistema. ", 1);
	        }
	
			//echo realpath(getcwd() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR);
			//$fileName = base_url()."sistema/aplicativo/upload/$nome_final";
			//$ponteiro = fopen($fileName, "r");
			
			$ponteiro = fopen($destFile, "r");			
	        $dadosTxt = '';
	        while (!feof($ponteiro)) {
	            $dadosTxt .= fgets($ponteiro, 4096);
	        }
	        fclose($ponteiro);
			
			$dadosTxt = removeCE_Upper($dadosTxt); //REMOVE CARACTERES ESPECIAIS
			$dadosTxt = str_replace(array("\r", "\n"), " ", $dadosTxt); //REMOVE QUEBRA DE LINHAS
			
			//echo '<pre>'; var_dump($dadosTxt); echo '<pre>'; die(); //DEBUG
		
			//$dadosTxt = file_get_contents($fileName);
			if (empty($dadosTxt)) {
				throw new Exception('Ocorreu um erro ao efetuar a leitura da arquivo. Por favor verifique se ele est� conforme o layout necess�rio, vazio ou se foi corrompido. <span style="font-size:8px; display:block;">Arquivo: ' . $fileName . '</span>', 1);	
			}
			
			$erros=''; //ERROS NOS REGISTROS
			$dadosTmp = '';
	        $registros = explode('#', $dadosTxt);
			//echo '<pre>'; var_dump($registros); echo '<pre>'; die(); //DEBUG
	        foreach ($registros as $i => $registro) {
	        	if (empty($registro)) continue; //PULA REGISTROS EM BRANCO
	        	//if ($i>13) continue; //DEBUG
	        	try {
	        		$dadosTmp[$i]='';
		            $dados = explode('|', $registro);
					
					//echo '<pre>'; var_dump($dados); echo '<pre>'; die(); //DEBUG
		
		            if (is_array($dados) && count($dados) == 13) {
		
		                $dadosTmp[$i]['docCredor'] = $docCredor = $dados[0];
		                $dadosTmp[$i]['docInadimplente'] = $docInadimplente = preg_replace('/[^0-9]/', '', $dados[1]);
		                $dadosTmp[$i]['emissao'] = $emissao = $dados[2];
		                $dadosTmp[$i]['qtdeParcelas'] = $qtdeParcelas = $dados[3];
		                $dadosTmp[$i]['vencInicial'] = $vencInicial = $dados[4];
		                $dadosTmp[$i]['valorTotal'] = $valorTotal = $dados[5];
		                $dadosTmp[$i]['tipoDoc'] = $tipoDoc = $dados[6];
		                $dadosTmp[$i]['banco'] = $banco = $dados[7];
		                $dadosTmp[$i]['agencia'] = $agencia = $dados[8];
		                $dadosTmp[$i]['alinea'] = $alinea = $dados[9];
		                $dadosTmp[$i]['informacoes'] = $informacoes = $dados[10];
						
		                $inaCod = isset($this->md->getInaCod($docInadimplente)->ina_cod) ? $this->md->getInaCod($docInadimplente)->ina_cod : '';
		                $inadDados = $dados[12];
		                $dadosTmp[$i]['inadDados'] = $inadDados = explode('%', $inadDados);
		
		                if ($inaCod == '') {
		                    if (sizeof($inadDados) == '33') {
		                    	if (strtolower($inadDados[0])!="f" || strtolower($inadDados[0])!='j') $inadDados[0] = 'f';
		                        $this->md->cadInad($inadDados[0],$inadDados[1],$inadDados[2], $inadDados[3], $docInadimplente, $inadDados[4], $inadDados[5], $inadDados[6], $inadDados[7], $inadDados[8], $inadDados[9], $inadDados[10], $inadDados[11], $inadDados[12], $inadDados[13], $inadDados[14], $inadDados[15], $inadDados[16], $inadDados[17], $inadDados[18], $inadDados[19], $inadDados[20], $inadDados[21], $inadDados[22], $inadDados[23], $inadDados[24], $inadDados[25], $inadDados[26], $inadDados[27], $inadDados[28], $inadDados[29], $inadDados[30], $inadDados[31], $inadDados[32]);
		                        $inaCod = $this->md->getInaCod($docInadimplente)->ina_cod;
		                        $dadosTmp[$i]['duplicidade'] = $duplicidade = '0';
		                        $this->md->cadInadTemp($inadDados[0], $inadDados[1], $inadDados[2], $inadDados[3], $inadDados[4], $docInadimplente, $inadDados[5], $inadDados[6], $inadDados[7], $inadDados[8], $inadDados[9], $inadDados[10], $inadDados[11], $inadDados[12], $inadDados[13], $inadDados[14], $inadDados[15], $inadDados[16], $inadDados[17], $inadDados[18], $inadDados[19], $inadDados[20], $inadDados[21], $inadDados[22], $inadDados[23], $inadDados[24], $inadDados[25], $inadDados[26], $inadDados[27], $inadDados[28], $inadDados[29], $inadDados[30], $inadDados[31], $inadDados[32], $inaCod, $duplicidade);
		                    } else {
		                    	throw new Exception('Dados do inadimplente est�o incorretos, devem ser enviados constando as 33 colunas.',72);
		                    }
		                } else {
		                    if (sizeof($inadDados) == '33') {
		                        $dadosTmp[$i]['duplicidade'] = $duplicidade = '1';
		                        $this->md->cadInadTemp($inadDados[0], $inadDados[1], $inadDados[2], $inadDados[3], $inadDados[4], $docInadimplente, $inadDados[5], $inadDados[6], $inadDados[7], $inadDados[8], $inadDados[9], $inadDados[10], $inadDados[11], $inadDados[12], $inadDados[13], $inadDados[14], $inadDados[15], $inadDados[16], $inadDados[17], $inadDados[18], $inadDados[19], $inadDados[20], $inadDados[21], $inadDados[22], $inadDados[23], $inadDados[24], $inadDados[25], $inadDados[26], $inadDados[27], $inadDados[28], $inadDados[29], $inadDados[30], $inadDados[31], $inadDados[32], $inaCod, $duplicidade);
		                    } else {
		                    	throw new Exception('Dados do inadimplente est�o incorretos, devem ser enviados constando as 33 colunas.',85);
		                    }
		                }
		
						$creCod = $this->md->getCreCod($docCredor)->cre_cod;
		                if (!isset($creCod)) {
		                	throw new Exception('O credor informado n�o existe ou o documento foi enviado com a formata��o incorreta. Documento: '.$docCredor,98);
		                }
		
		                $imp_erro = '0';
		
		                $divida_cod = $this->md->cadastraDividaTemporaria($tipoDoc, $banco, $agencia, $alinea, $emissao, $qtdeParcelas, $vencInicial, $informacoes, $valorTotal, $creCod);
		                $dividasIncluidas .= $divida_cod . '-';
		
		                $this->md->cadastraImportacao($inaCod, $duplicidade, $imp_erro, $divida_cod);
		
		                $parcelas = $dados[11];
		
		                $parcelasDados = explode('&', $parcelas);
		
		                foreach ($parcelasDados as $parcela) {
		                    if ($parcela != '') {
		
		                        $parcela = explode('%', $parcela);
		
		                        $parnum = $parcela[0];
		                        $parvenc = $parcela[1];
		                        $parvalor = $parcela[2];
		
		                        $this->md->cadastraParcela($divida_cod, $parnum, $parvenc, $parvalor);
		                    }
		                }
		            } else {
		            	//echo '<pre>'; var_dump($dados); echo '<pre>'; die(); //DEBUG
		                ////////////////////********************************* EEEEEEEEEEERRRRRRRRRRROOOOOOOO
		                throw new Exception('Formato do arquivo n�o � compat�vel com o layout necess�rio, por favor verifique o arquivo e tente novamente.');
		            }
				} catch(Exception $e) {
					$erros[$i][]=$e->getMessage();
				}
				
	        }//FOREACH END
			$this->data['erros'] = $erros;

		} catch(Exception $e) {
			$this->data['mensagem'] = $e->getMessage();
		}
		
		/*
		if (empty($erros) && !isset($this->data['mensagem'])) {
			redirect(base_url() . 'importacao/exibirDados/cod:' . $dividasIncluidas);	
		} else {
			$this->data['registros'] = $dadosTmp;
		}
		*/
		$this->data['registros'] = $dadosTmp;
        $this->data['title'] = "EZ Cobran�a :: Importa��o";
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue'));
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput'));
        $this->session->set_userdata('menusel', '38'); //DETERMINA O MENU QUE FICARA ABERTO
        $this->inicore->loadSidebar(); //barra lateral
        $this->inicore->loadview('importar', $this->data);
    }

    function _importacao() {
        $this->session->set_userdata('menusel', '38'); //DETERMINA O MENU QUE FICARA ABERTO
		$this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue'));
		//$this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput','jquery.accordion.js'));
        $this->inicore->loadSidebar(); //barra lateral
        $this->inicore->loadview('importar_principal', $this->data);
    }

    function _exibirDados() {
        $cods = get('cod');
        $cods = str_replace('-', ',', $cods);
        $cods = substr($cods, 0, (strlen($cods) - 1));
        $this->data['dividas'] = $this->md->getDividasDados($cods);
        $this->session->set_userdata('menusel', '38'); //DETERMINA O MENU QUE FICARA ABERTO
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue'));
        $this->inicore->loadSidebar(); //barra lateral
        $this->inicore->loadview('importar_resultado', $this->data);
    }
	
	function _mask($val, $mask) {
		$maskared = '';
		$k = 0;
		for ($i = 0; $i <= strlen($mask) - 1; $i++) {
			if ($mask[$i] == '#') {
				$r = substr($val, $k++, 1);
				if ($r!='') $maskared .= $r;
			} else {
				if (isset ($mask[$i])) $maskared .= $mask[$i];
			}
		}
		return $maskared;
	}

	//function _isJSON($string){
	//   return is_string($string) && is_object(json_decode($string)) ? true : false;
	//}

	//5.3+	
	function _isJSON($string){
	   return is_string($string) && is_object(json_decode($string)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
	}

}

