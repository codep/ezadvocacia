<?php

class Inadimplentes extends Controller {

    function Inadimplentes() {
        parent::Controller();
        // CONFIGURA��ES DE PAGINA
        $this->data = array(
            'title' => 'EZ Advocacia e Cobran�as ::  Vis�o do Credor',
            'description' => '',
            'keywords' => ''
        );

        $this->load->model('inadimplente_model', 'md');
    }

    function _remap($link) {
        include 'testar_conexao.php';

        if ($link == 'index' || is_numeric($link)) {/* O link ser� num�rico quando o usu�rio clicar no link da pagina��o */
            $this->_listarInads();
        } else if ($link == 'ficha') {
            $this->_ficha();
        } else if ($link == 'detalhesCobranca') {
            $this->_detalhesCobranca();
        } else if ($link == 'avaliar') {
            $this->_avaliar();
        } else if ($link == 'ros'){
            $this->_ros();
        }
    }

    function _listarInads() {
        $usuCreCod = $this->session->userdata('usucod'); //c�d do credor logado
        
        $filtro1 = $this->input->post('filtro1', true);
        $filtro1Desc = $this->input->post('filtro1_desc', true);
		if (empty($filtro1)) $filtro1Desc='';
        
        if (!empty($filtro1) && !empty($filtro1Desc) ){
        	
            // COME�A O MONTAR O FILTRO
            $filtro = "WHERE ";

            if ($filtro1 == 'telefone') {//SE FOR UM TELEFONE, FAZ-SE UM TRATAMENTO ESPECIAL
                $filtro .=" I.ina_foneres LIKE '%$filtro1Desc%' OR I.ina_fonerec LIKE '%$filtro1Desc%'
                         OR I.ina_fonecom LIKE '%$filtro1Desc%' OR I.ina_cel1 LIKE '%$filtro1Desc%' OR I.ina_cel2 LIKE '%$filtro1Desc%'
                         OR I.ina_cel3 LIKE '%$filtro1Desc%'"; // BUSCA EM TODOS OS CAMPOS DE TELEFONE DO INADIMPLENTE
            } else if ($filtro1 == 'telenonePAR') {//SE FOR UM TELEFONE DE PARENTE TAMBEM TEM UM TRATAMENTO ESPECIAL
                $filtro .="I.ina_mae_fone LIKE '%$filtro1Desc%'
                        OR I.ina_pai_fone LIKE '%$filtro1Desc%'
                        OR I.ina_conj_fone LIKE '%$filtro1Desc%'"; // BSCA EM TODOS OS CAMPOS DE TELEFONES DE PARENTES
            } else if ($filtro1 == 'parentes') {//SE A PESQUISA FOR POR PARENTES, TAMBEM TEM UM TRATAMENTO ESPECIAL
                $filtro .="I.ina_conj_nome LIKE '%$filtro1Desc%'
                            OR I.ina_mae_nome LIKE '%$filtro1Desc%'
                            OR I.ina_pai_nome LIKE '%$filtro1Desc%'"; // BUSCA EM TODOS OS CAMPOS DE PARENTES
            } else {// SE N�O TIVER NENHUM TRATAMENTO ESPECIAL, MONTA UM FILTRO GEN�RICO COM O CAMPO SELECIONADO E O FILTRO DIGITADO
                $filtro .=" I.$filtro1 LIKE '%$filtro1Desc%'";
            }
			
        }

		//EXIBE NO FORMULARIO
        $this->data['filtro1'] = $filtro1; //PASSA PARA A VIEW O FILTRO SELECIONADO
        $this->data['filtro1_desc'] = $filtro1Desc; //PASSA PARA A VIEW O FILTRO DIGITADO

        /* -------------- IN�CIO PAGINA��O -------------- */
        $this->load->library('pagination'); //carrega pagina��o
        $config['base_url'] = base_url() . 'inadimplentes';
        $config['total_rows'] = $this->md->getRows($usuCreCod, $filtro);
        $config['per_page'] = '30'; //qtd que ser� exib�do por p�gina
        $config['uri_segment'] = 2;
        $config['last_link'] = '&gt;&gt;';
        $config['first_link'] = '&lt; &lt;';

        $this->pagination->initialize($config);
        $this->data['paginacao'] = $this->pagination->create_links();

        $pag = $this->uri->segment($config['uri_segment']);
        if (!isset($pag) || $pag == '' || !is_numeric($pag))
            $pag = 0;

        if ($config['total_rows'] == 0) {//se n�o encontrar nenhum inadimplente com cobran�a ativa redireciona para a home e d� uma mensagem
            //$this->inicore->setMensagem('notice', "Nenhum inadimplente encontrado!", true);
            //redirect(base_url() . 'home');
        }
        /* -------------- FIM PAGINA��O -------------- */

        $this->data['totalInadimplentes'] = $config['total_rows'];
        $this->data['inadsCredor'] = $this->md->getInadimplentes($usuCreCod, $pag, $config['per_page'], $filtro); //inadimplentes do credor logado
        
        $this->session->set_userdata('menusel', '0');
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue'));
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput','jquery.accordion.js'));
        $this->inicore->loadSidebar(); //passa para a sess�o o menu que ficar� aberto
        $this->inicore->loadview('list_inadimplente', $this->data);
    }

    function _ficha() {
        /* ------------ Declaracao das variaveis ------------ */
        $inaCod = get('inaCod'); //c�d do inadimplente selecionado
        $recupCod = get('recupCod');//cod do recuperador. Para inserir na tabela de acesso do credor
        $usuCreCod = $this->session->userdata('usucod'); //c�d do credor logado
        $creNomeFantasia = $this->session->userdata('usunome'); //nome fantasia do usu�rio logado
        $this->data['inadDados'] = '';
        $dividasAdm = '';
        $acordosAdm = '';
        $dividasJudAcordsRecemEnviados = '';
        $dividasJudAcordsFirmados = '';
        /* ------------ Fim declaracao das variaveis ------------ */

        if (!is_numeric($inaCod) || !is_numeric($recupCod)) {//se o c�d for diferente de num�rico ou for vazio.
            $this->inicore->setMensagem('error', 'N�o autorizado', true);
            redirect(base_url());
        }

        /* ------------- VERIFICANDO SE O RECUPERADOR EXISTE ------------- */
//        $aux = $this->md->verificaRecuperador($inaCod, $recupCod, $usuCreCod);
//        if($aux == 0){
//            $this->inicore->setMensagem('notice', 'N�o autorizado', true);
//            redirect(base_url());
//        }
        /* ------------- FIM VERIFICANDO SE O RECUPERADOR EXISTE ------------- */

        $this->data['inadDados'] = $this->md->getInadDados($inaCod); //procurando o inadimplente

        if ((sizeof($this->data['inadDados'])) == 0) {//se o inadimplente nao for encontrado
            $this->inicore->setMensagem('notice', 'Inadimplente n�o encontrado', true);
            redirect(base_url());
        }
        /* ####################### GRAVANDO NA TABELA acessos O COD DO CREDOR, A FICHA QUE ELE ACESSOU ETC ####################### */
        $data = array(
           'credores_cre_cod' => $usuCreCod ,
           'inadimplentes_ina_cod' => $inaCod ,
           'usuarios_responsavel_cod' => $recupCod
        );

        $this->db->insert('acessos', $data);
        /* ####################### FIM GRAVANDO NA TABELA acessos O COD DO CREDOR, A FICHA QUE ELE ACESSOU ETC ####################### */

        /* ----------- D�VIDAS E ACORDOS ADM ----------- */
        $dividasAdm = $this->md->getDividasAdm($inaCod, $usuCreCod); //pegando as d�vidas adm e suas parcelas
        $acordosAdm = $this->md->getAcordosAdm($inaCod, $usuCreCod); //pega os acordos adm e suas parcelas
        /* ----------- FIM D�VIDAS E ACORDOS ADM ----------- */

        /*         * ******************* DIVIDAS JUDS ******************** */
        $dividasJudAcordsRecemEnviados = $this->md->getAcordsJudRecemEnviados($usuCreCod, $inaCod);
        $dividasJudAcordsFirmados = $this->md->getAcordsJudFirmados($usuCreCod, $inaCod);
        /*         * ******************* FIM DIVIDAS JUDS ******************** */

        $this->data['creNomeFantasia'] = $creNomeFantasia;
        $this->data['dividasAdm'] = $dividasAdm;
        $this->data['acordosAdm'] = $acordosAdm;
        $this->data['dividasJudAcordsRecemEnviados'] = $dividasJudAcordsRecemEnviados;
        $this->data['dividasJudAcordsFirmados'] = $dividasJudAcordsFirmados;
        $this->data['inaCod'] = $inaCod; //essa vari�vel � usada para saber em que ficha o usu�rio est� para redirecion�-lo para a mesma ficha. OBS: Quando usar o redirect
        $this->data['recupCod'] = $recupCod;//essa vari�vel � usada para saber em que ficha o usu�rio est� para redirecion�-lo para a mesma ficha. OBS: Quando usar o redirect
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue'));
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput', 'jquery.accordion.js'));
        $this->inicore->loadSidebar(); //passa para a sess�o o menu que ficar� aberto
        $this->session->set_userdata('menusel', '0');
        $this->inicore->loadview('divida_ficha', $this->data);
    }

    function _detalhesCobranca() {
        $cod = get('c');
        $codDetalhes = get('cod');


        if ($codDetalhes != '') {
            $codGerador = $codDetalhes;
        } else {
            $codGerador = $this->md->getCodGerador($cod)->aco_cob_cod_geradora;
        }


        if (isset($this->md->getCodAcordoGerador($codGerador)->aco_cod)) {

            $acoCod = $this->md->getCodAcordoGerador($codGerador)->aco_cod;

            $this->data['cobDados'] = $this->md->getCobrancaDadosAco($acoCod);
            $this->data['parcelas'] = $this->md->getCobrancaParcelasAco($acoCod);

            $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue'));
            $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput', 'jquery.accordion.js'));
            $this->inicore->loadSidebar(); //passa para a sess�o o menu que ficar� aberto
            $this->session->set_userdata('menusel', '0');
            $this->inicore->loadview('cob_detalhes', $this->data);
        } else {
            $divCod = $this->md->getCodDividaGerador($codGerador)->div_cod;

            $this->data['cobDados'] = $this->md->getCobrancaDadosDiv($divCod);
            $this->data['parcelas'] = $this->md->getCobrancaParcelasDiv($divCod);

            $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue'));
            $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput', 'jquery.accordion.js'));
            $this->inicore->loadSidebar(); //passa para a sess�o o menu que ficar� aberto
            $this->session->set_userdata('menusel', '0');
            $this->inicore->loadview('cob_detalhes', $this->data);
        }
    }
    function _ros() {
        $cobsCods = $this->input->post('cobsCods');
        $cobsCods = str_replace('-', ',', $cobsCods);
        $inaCod = $this->input->post('inaCod');
        $recupCod = $this->input->post('recupCod');
//        die('cobrancas - '.$cobsCods);
        $ros = $this->md->_getRos($cobsCods);
        
        if(sizeof($ros) == 0){
            $this->inicore->setMensagem('error', 'A(s) cobran�a(s) ' .$cobsCods. ' n�o possui RO.', true);
            redirect(base_url() . 'inadimplentes/ficha/inaCod:' . $inaCod. '/recupCod:'. $recupCod);
        }

        $this->data['inaNome'] = $this->input->post('inaNome');
        $this->data['inaCod'] = $this->input->post('inaCod');
        $this->data['recupCod'] = $this->input->post('recupCod');
        $this->data['ros'] = $this->md->_getRos($cobsCods);
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue'));
        $this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput', 'jquery.accordion.js'));
        $this->inicore->loadSidebar(); //passa para a sess�o o menu que ficar� aberto
        $this->session->set_userdata('menusel', '0');
        $this->inicore->loadview('ro_novo',$this->data);

    }

    function _avaliar() {
        $cobranca = get('cob');
        $nota = get('val');
        $inaCod = get('inaCod');
        $recupCod = get('recupCod');//cod do recuperador. Para inserir na tabela de acesso do credor. OBS: Depois que o credor avaliar a d�vida ser�  redirecionado para a ficha do inadimplente.

        /*se alguma vari�vel for diferente de num�rico ou for vazio*/
        if(!is_numeric($cobranca) || !is_numeric($nota) || !is_numeric($inaCod) || !is_numeric($recupCod)){
            $this->inicore->setMensagem('error', 'N�o autorizado', true);
            redirect(base_url());
        }
        /*-----------------------------------*/
        
        if (!is_numeric($nota)) {
            $this->inicore->setMensagem('error', 'Avalia��o inv�lida!', true);
            redirect(base_url());
        }
        if ($nota < 1) {
            $nota = 1;
        }
        if ($nota > 5) {
            $nota = 5;
        }
        
        $inadimplente = $this->db->query("SELECT I.ina_nome FROM inadimplentes I WHERE I.ina_cod = $inaCod")->row();

        $data = array(
            'cob_avaliacao' => $nota
        );
        

        $this->db->where('cob_cod', $cobranca);
        $this->db->update('cobrancas', $data);
                
        $query = $this->db->query("SELECT DISTINCT co.usuarios_usu_cod, co.credor_cre_cod FROM cobrancas co WHERE co.cob_cod = $cobranca")->result();

        $data = array(
            'usuarios_usu_cod' => $query[0]->usuarios_usu_cod,
            'men_titulo' => removeCE_Upper('A cobran�a cod '. $cobranca. ' foi avaliada'),
            'men_remetente' => $query[0]->credor_cre_cod,
            'men_texto' => removeCE_Upper('A cobran�a do inadimplente '.$inadimplente->ina_nome.' cod '.$cobranca. '  foi avaliada e recebeu a nota '. $nota)
        );
        

      $this->db->insert('mensagens_credores', $data);

        $this->inicore->setMensagem('success', 'Avalia��o efetuda com sucesso!', true);
        redirect(base_url() . 'inadimplentes/ficha/inaCod:'.$inaCod.'/recupCod:'.$recupCod);
    }

}
