<?php

class Relatorios extends Controller {

    function Relatorios() {
        parent::Controller();
        // CONFIGURACOES PAGINA
        $this->data = array(
            'title' => 'EZ Advocacia e Cobran�as ::  Vis�o do Credor',
            'description' => '',
            'keywords' => ''
        );
    }

    function _remap($link) {
    	
		$params = $this->uri->rsegments;
		//var_dump($params); die(); //DEBUG
		
		$validAccess = false;
		if ($params[3]=="c") {
			$relCod = $params[4];
			//echo $relCod; //DEBUG
	    	//VERIFICA PERMISS�O DE ACESSO A ESSE LINK
	        $query = $this->db->query("SELECT C.cre_relatorios FROM credores C WHERE C.cre_cod = '".$this->session->userdata('usucod')."'");
			$credor = $query->row();
			$creRels = empty($credor->cre_relatorios) ? array() : explode(',', $credor->cre_relatorios);
			$this->data['crerel_cod'] = $relCod;
			if (in_array($relCod, $creRels)) $validAccess=true;
		}
		if ($validAccess==false) {
            $this->inicore->setMensagem('notice', 'Voc� n�o tem permiss�o de acesso ao relat�rio', true);
            redirect(base_url() . '/');
		}
		
        $this->inicore->addcss(array('reset', 'style', 'style_fixed','colors/blue','chosen.min')); // CSS HOME
        //CARREGA O HELPER
        $this->load->helper("funcoes_helper");
        //carrega o model
        $this->load->model('relatorios_model', 'md');
        //CARREGA OS JAVASCRIPTS
        //'jquery-1.4.2.min.js', 
        //'jquery-ui-1.8.custom.min.js'
        $this->inicore->addjs(array('jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js','plugin/chosen.jquery.min.js','plugin/chosen.proto.min.js','jquery.cookie.js','plugin/jquery.quicksearch.min.js'));
//------------------------------------------------------------------------------
        include 'testar_conexao.php';
//------------------------------------------------------------------------------
		if ($link == 'acordos') {
            $this->_acordos();
		} else if ($link == 'pesAcordos') {	
			$this->_pesAcordos();
        } else if ($link == 'pagamentos') {
            $this->_pagamentos();
		} else if ($link == 'pesPagamentos') {	
			$this->_pesPagamentos();
        } else if ($link == 'parcelas') {
            $this->_parcelas();
        } else if ($link == 'pesParcelas') {
            $this->_pesParcelas();
        }
    }
	
	function _acordos() {
		$usuCreCod = $this->session->userdata('usucod'); //cod do credor (usuario) logado
		
        $this->session->set_userdata('menusel', 'relatorios'); //DETERMINA O MENU QUE FICARA ABERTO
		$this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue'));
		//$this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput','jquery.accordion.js'));
		
        $this->inicore->loadSidebar(); //barra lateral
        $this->inicore->loadview('relatorios_acordos_filtro', $this->data);
	}

	function _pesAcordos() {
//        $this->benchmark->mark('code_start');

		//I=Intervalo, T=Todas
		$filtro_periodo = $this->input->post('filtro_periodo','');
			
		//VARIAVEIS
		$registros='';			
			
		//CREDOR
		$cre_cod = $this->session->userdata('usucod'); //cod do credor (usuario) logado
		if (empty($cre_cod)) $cre_cod=0;
		
		//PERIODO -------------------------- 
		if ($filtro_periodo=='I') {
			//INTERVALO -------------------------- 
			$periodo_data_de = convDataParaDb($this->input->post('periodo_data_de',''));
			$periodo_data_ate = convDataParaDb($this->input->post('periodo_data_ate',''));
			
			if ($periodo_data_de=='--') $periodo_data_de='';
			if ($periodo_data_ate=='--') $periodo_data_ate='';
			
			if (empty($periodo_data_de) || empty($periodo_data_ate)) {
				$this->inicore->setMensagem('notice', 'Voc� ativou o filtro por per�odo mas n�o selecionou as datas do intervalo.');
				redirect(base_url() . 'relatorios/acordos');		
			} else {
				$this->data['periodo'] = convDataBanco($periodo_data_de).' � '.convDataBanco($periodo_data_ate);
				$registros = $this->md->pesAcordos($cre_cod, $periodo_data_de, $periodo_data_ate);	
			}
			
		} else if ($filtro_periodo=='T') {
			//TODAS --------------------------
			$this->data['periodo'] = 'N�o informado';
			$registros = $this->md->pesAcordos($cre_cod);
		} else {
            $this->inicore->setMensagem('notice', 'Voc� n�o selecionou um per�odo v�lido.');
            redirect(base_url() . 'relatorios/acordos');
		}

		//ORDENAR CORRETAMENTE OS ACORDOS PARA EXIBIR NO RELAT�RIO
		$inadimplentes=''; $cre_nome_fantasia=''; $total_val=0; 
		foreach($registros as $k=>$acordo) {
			if ($k==0) $cre_nome_fantasia = $acordo->cre_nome_fantasia;
			if (!isset($inadimplentes[$acordo->ina_cod])) {
				$inadimplentes[$acordo->ina_cod] = array(
					'ina_cod'=> $acordo->ina_cod,
					'ina_nome'=> $acordo->ina_nome,
					'ina_cpf_cnpj'=> $acordo->ina_cpf_cnpj,
					'acordos_num'=>0,
					'acordos_val'=>0,
					'acordos'=>''
				);
			}
			if (!isset($inadimplentes[$acordo->ina_cod]['acordos'][$acordo->aco_cod])) {
				$inadimplentes[$acordo->ina_cod]['acordos_num']++;
				$inadimplentes[$acordo->ina_cod]['acordos'][$acordo->aco_cod] = array(
					'aco_cod' => $acordo->aco_cod,
					'aco_emissao'=> convDataBanco($acordo->aco_emissao),
					'aco_valor_original'=> $acordo->aco_valor_original,
					'aco_valor_atualizado'=> $acordo->aco_valor_atualizado,
					'aco_total_parcelado'=> $acordo->aco_total_parcelado,
					'parcelas_num' => intval($this->md->getTotalParcelasAcordo($acordo->aco_cod)),
					'parcelas_val' => 0,
					'parcelas' => ''
				); 				
			}

			$total_val= floatval($total_val) + floatval($acordo->paa_valor);
			$inadimplentes[$acordo->ina_cod]['acordos_val'] = floatval($inadimplentes[$acordo->ina_cod]['acordos_val']) + floatval($acordo->paa_valor);
			$inadimplentes[$acordo->ina_cod]['acordos'][$acordo->aco_cod]['parcelas_val'] = floatval($inadimplentes[$acordo->ina_cod]['acordos'][$acordo->aco_cod]['parcelas_val']) + floatval($acordo->paa_valor);
			$inadimplentes[$acordo->ina_cod]['acordos'][$acordo->aco_cod]['parcelas'][] = array(
				'parcela'=> $acordo->paa_parcela,
				'valor' => $acordo->paa_valor,
				'vencimento' => convDataBanco($acordo->paa_vencimento)
			);
			
		}
		
		$this->data['total_val'] = $total_val;
		$this->data['credor'] = $cre_nome_fantasia;
		$this->data['inadimplentes'] = $inadimplentes;
        //----------------------------------------------------------

        //se $cobDiv na posi��o 0 ou na posi��o 1 for igual 0, significa que a consulta n�o retornou nenhum resultado
        if (empty($registros)) {
            $this->inicore->setMensagem('notice', 'A pesquisa n�o retornou resultados.', true);
            redirect(base_url() . 'relatorios/acordos');
        } else {//se a consulta retornar algum resultado
            $this->inicore->addcss(array('relatorios')); // CSS
			$this->inicore->loadview('pes_relatorios_acordos_res', $this->data);
        }
	}

	function _pagamentos() {
		$usuCreCod = $this->session->userdata('usucod'); //cod do credor (usuario) logado
		
        $this->session->set_userdata('menusel', 'relatorios'); //DETERMINA O MENU QUE FICARA ABERTO
		$this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue'));
		//$this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput','jquery.accordion.js'));
		
        $this->inicore->loadSidebar(); //barra lateral
        $this->inicore->loadview('relatorios_pagamentos_filtro', $this->data);
	}

	function _pesPagamentos() {
//        $this->benchmark->mark('code_start');

		//I=Intervalo, T=Todas
		$filtro_periodo = $this->input->post('filtro_periodo','');
		$filtro_tipo = $this->input->post('filtro_tipo','');
			
		//VARIAVEIS
		$registros='';			
			
		//CREDOR
		$cre_cod = $this->session->userdata('usucod'); //cod do credor (usuario) logado
		if (empty($cre_cod)) $cre_cod=0;
		
		//PERIODO -------------------------- 
		if ($filtro_periodo=='I') {
			//INTERVALO -------------------------- 
			$periodo_data_de = convDataParaDb($this->input->post('periodo_data_de',''));
			$periodo_data_ate = convDataParaDb($this->input->post('periodo_data_ate',''));
			
			if ($periodo_data_de=='--') $periodo_data_de='';
			if ($periodo_data_ate=='--') $periodo_data_ate='';
			
			if (empty($periodo_data_de) || empty($periodo_data_ate)) {
				$this->inicore->setMensagem('notice', 'Voc� ativou o filtro por per�odo mas n�o selecionou as datas do intervalo.');
				redirect(base_url() . 'relatorios/pagamentos');		
			} else {
				$this->data['periodo'] = convDataBanco($periodo_data_de).' � '.convDataBanco($periodo_data_ate);
				$registros = $this->md->pesPagamentosPeriodo($cre_cod, $periodo_data_de, $periodo_data_ate);	
			}
			
		} else if ($filtro_periodo=='T') {
			//TODAS --------------------------
			$this->data['periodo'] = 'N�o informado';
			$registros = $this->md->pesPagamentosPeriodo($cre_cod);
		} else {
			$this->inicore->setMensagem('notice', 'Voc� n�o selecionou um per�odo v�lido.');
        	redirect(base_url() . 'relatorios/pagamentos');
		}

		//VARIAVEIS
		$inadimplentes=''; $cre_nome_fantasia=''; $total_val=0; $template='';

		//D=Detalhado, R=Resumido
		if ($filtro_tipo=='D') {
			$template = 'pes_relatorios_pagamentos_detalhado_res.php';
			
			//echo '<pre>'; var_dump($registros); die(); //DEBUG
			
			foreach($registros as $k=>$recebimento) {
				
				if ($k==0) $cre_nome_fantasia = $recebimento->cre_nome_fantasia;
				if (!isset($inadimplentes[$recebimento->ina_cod])) {
					$inadimplentes[$recebimento->ina_cod] = array(
						'ina_cod'=> $recebimento->ina_cod,
						'ina_nome'=> $recebimento->ina_nome,
						'ina_cpf_cnpj'=> $recebimento->ina_cpf_cnpj,
						'pagamentos_num'=>0,
						'pagamentos_val'=>0,
						'pagamentos'=>''
					);
				}
								
				if (!isset($inadimplentes[$recebimento->ina_cod]['pagamentos'][$recebimento->aco_cod])) {
					$totalParcelas = intval($this->md->getTotalParcelasAcordo($recebimento->aco_cod));
					$inadimplentes[$recebimento->ina_cod]['pagamentos_num']++;
					$inadimplentes[$recebimento->ina_cod]['pagamentos'][$recebimento->aco_cod] = array(
						'aco_cod' => $recebimento->aco_cod,
						'aco_emissao'=> convDataBanco($recebimento->aco_emissao),
						'parcelas_num' => $totalParcelas,
						'parcelas_val' => 0,
						'parcelas' => ''
					); 				
				}
	
				$total_val= floatval($total_val) + floatval($recebimento->reb_valor);
				$inadimplentes[$recebimento->ina_cod]['pagamentos_val'] = floatval($inadimplentes[$recebimento->ina_cod]['pagamentos_val']) + floatval($recebimento->reb_valor);
				
				//LOCALIZAR PARCELAS
				$parcelas = $this->md->getPagamentosParcelas($recebimento->reb_cod);
				foreach($parcelas as $parcela) {
					$valor_pago = floatval($parcela->paa_valor) - floatval($parcela->paa_saldo);
					$inadimplentes[$recebimento->ina_cod]['pagamentos'][$recebimento->aco_cod]['parcelas_val'] = floatval($inadimplentes[$recebimento->ina_cod]['pagamentos'][$recebimento->aco_cod]['parcelas_val']) + floatval($valor_pago);
					$inadimplentes[$recebimento->ina_cod]['pagamentos'][$recebimento->aco_cod]['parcelas'][$parcela->paa_cod] = array(
						'parcela'=> $parcela->paa_parcela,
						'valor_total' => $parcela->paa_valor,
						'saldo' => $parcela->paa_saldo,
						'valor' => $valor_pago,
						'vencimento' => convDataBanco($parcela->paa_vencimento),
						'pagamento' => convDataBanco($recebimento->reb_data)
					);					
				}				
			}
			
		} else if ($filtro_tipo=='R') {
			$template = 'pes_relatorios_pagamentos_resumido_res.php';
			
			foreach($registros as $k=>$recebimento) {
				
				if ($k==0) $cre_nome_fantasia = $recebimento->cre_nome_fantasia;
				if (!isset($inadimplentes[$recebimento->ina_cod])) {
					$totalValorAcordo = floatval($this->md->getTotalAcordoInadimplente($recebimento->ina_cod,$cre_cod));
					$inadimplentes[$recebimento->ina_cod] = array(
						'ina_cod'=> $recebimento->ina_cod,
						'ina_nome'=> $recebimento->ina_nome,
						'ina_cpf_cnpj'=> $recebimento->ina_cpf_cnpj,
						'valor_total' => $totalValorAcordo,
						'valor_pago' => 0,
						'valor_saldo' => $totalValorAcordo
					);
				}

				//CALCULANDO TOTAL PAGO NO PERIODO	
				$total_val= floatval($total_val) + floatval($recebimento->reb_valor);
				$inadimplentes[$recebimento->ina_cod]['valor_pago'] = floatval($inadimplentes[$recebimento->ina_cod]['valor_pago']) + floatval($recebimento->reb_valor);
				//CALCULANDO QUANTO AINDA RESTA PAGAR
				$inadimplentes[$recebimento->ina_cod]['valor_saldo'] = floatval($inadimplentes[$recebimento->ina_cod]['valor_saldo']) - floatval($recebimento->reb_valor);

			}
			
		} else {
			$this->inicore->setMensagem('notice', 'Voc� n�o selecionou um tipo v�lido.');
        	redirect(base_url() . 'relatorios/pagamentos');
		}

		//DATA PARA EXIBIR NO VIEW
		$this->data['total_val'] = $total_val;
		$this->data['credor'] = $cre_nome_fantasia;
		$this->data['inadimplentes'] = $inadimplentes;
        //----------------------------------------------------------

        //se $cobDiv na posi��o 0 ou na posi��o 1 for igual 0, significa que a consulta n�o retornou nenhum resultado
        if (empty($registros)) {
            $this->inicore->setMensagem('notice', 'A pesquisa n�o retornou resultados.', true);
            redirect(base_url() . 'relatorios/pagamentos');
        } else {//se a consulta retornar algum resultado
            $this->inicore->addcss(array('relatorios')); // CSS
			$this->inicore->loadview($template, $this->data);
        }
	}

	function _parcelas() {
		$usuCreCod = $this->session->userdata('usucod'); //cod do credor (usuario) logado
		
        $this->session->set_userdata('menusel', 'relatorios'); //DETERMINA O MENU QUE FICARA ABERTO
		$this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue'));
		//$this->inicore->addjs(array('jquery-1.4.2.min.js', 'jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'jquery.flot.min.js', 'tiny_mce/jquery.tinymce.js', 'smooth.js', 'smooth.menu.js', 'smooth.table.js', 'smooth.form.js', 'smooth.dialog.js', 'smooth.autocomplete.js', 'plugin/jquery.maskedinput','jquery.accordion.js'));
		
        $this->inicore->loadSidebar(); //barra lateral
        $this->inicore->loadview('relatorios_parcelas_filtro', $this->data);
	}

	function _pesParcelas() {
//        $this->benchmark->mark('code_start');

		//I=Intervalo, T=Todas
		$filtro_periodo = $this->input->post('filtro_periodo','');
			
		//VARIAVEIS
		$registros='';			
			
		//CREDOR
		$cre_cod = $this->session->userdata('usucod'); //cod do credor (usuario) logado
		if (empty($cre_cod)) $cre_cod=0;
		
		//PERIODO -------------------------- 
		if ($filtro_periodo=='I') {
			//INTERVALO -------------------------- 
			$periodo_data_de = convDataParaDb($this->input->post('periodo_data_de',''));
			$periodo_data_ate = convDataParaDb($this->input->post('periodo_data_ate',''));
			
			if ($periodo_data_de=='--') $periodo_data_de='';
			if ($periodo_data_ate=='--') $periodo_data_ate='';
			
			if (empty($periodo_data_de) || empty($periodo_data_ate)) {
				$this->inicore->setMensagem('notice', 'Voc� ativou o filtro por per�odo mas n�o selecionou as datas do intervalo.');
				redirect(base_url() . 'relatorios/parcelas');		
			} else {
				$this->data['periodo'] = convDataBanco($periodo_data_de).' � '.convDataBanco($periodo_data_ate);
				$registros = $this->md->pesParcelasPeriodo($cre_cod, $periodo_data_de, $periodo_data_ate);	
			}
			
		} else if ($filtro_periodo=='T') {
			//TODAS --------------------------
			$this->data['periodo'] = 'N�o informado';
			$registros = $this->md->pesParcelasPeriodo($cre_cod);
		} else {
			$this->inicore->setMensagem('notice', 'Voc� n�o selecionou um per�odo v�lido.');
        	redirect(base_url() . 'relatorios/parcelas');
		}

		//VARIAVEIS
		$parcelas=''; $cre_nome_fantasia=''; $total_val=0;
		foreach($registros as $k=>$parcela) {
			
			if ($k==0) $cre_nome_fantasia = $parcela->cre_nome_fantasia;
			
			$totalParcelas = intval($this->md->getTotalParcelasAcordo($parcela->aco_cod));
			
			$parcelas[$parcela->paa_cod] = array(
				'ina_cod'=> $parcela->ina_cod,
				'ina_nome'=> $parcela->ina_nome,
				'ina_cpf_cnpj'=> $parcela->ina_cpf_cnpj,
				'aco_cod' => $parcela->aco_cod,
				'parcela' => $parcela->paa_parcela,
				'parcelas' => $totalParcelas,
				'vencimento' => convDataBanco($parcela->paa_vencimento),
				'valor' => ($parcela->paa_saldo>0 ? ($parcela->paa_saldo) : $parcela->paa_valor)
			);

			//CALCULANDO TOTAL PAGO NO PERIODO	
			$total_val= floatval($total_val) + floatval($parcela->paa_valor);

		}

		//DATA PARA EXIBIR NO VIEW
		$this->data['total_val'] = $total_val;
		$this->data['credor'] = $cre_nome_fantasia;
		$this->data['parcelas'] = $parcelas;
        //----------------------------------------------------------

        //se $cobDiv na posi��o 0 ou na posi��o 1 for igual 0, significa que a consulta n�o retornou nenhum resultado
        if (empty($registros)) {
            $this->inicore->setMensagem('notice', 'A pesquisa n�o retornou resultados.', true);
            redirect(base_url() . 'relatorios/parcelas');
        } else {//se a consulta retornar algum resultado
            $this->inicore->addcss(array('relatorios')); // CSS
			$this->inicore->loadview('pes_relatorios_parcelas_res.php', $this->data);
        }
	}


}
