<?php

class Login extends Controller {

    function Login() {
        parent::Controller();
    }

    function index() {
        //passar o titulo para a p�gina
        $this->data['title'] = "EZ Advocacia e Cobran�as :: Login do credor";
        //adicionar o css
        $this->inicore->addcss(array('reset', 'style', 'style_fixed', 'colors/blue')); // CSS HOME
        //carregar o helper com as fun��es necess�rias
        $this->load->helper("funcoes_helper");
        //adicionar os javascripts
        $this->inicore->addjs(array('jquery-ui-1.8.custom.min.js', 'jquery.ui.selectmenu.js', 'tiny_mce/jquery.tinymce.js', 'smooth.form.js'));
        // verifica se usu�rio ja esta logado se estiver redireciona para home
        
        if ($this->session->userdata('status') == true) {
            redirect(base_url() . 'inadimplentes');
        }
        $this->data['ulogin'] = '';
        // recebendo dados enviados por post
        $usuario = strtolower($this->input->post('usuario', true));
        // passando os dados do post por uma fun��o anti-sqlinject
        $usuario = preg_replace(sql_regcase("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/"), "", $usuario);
        //recebendo dados por post
        $senha = $this->input->post('senha', true);
        // passando os dados do post por uma fun��o anti-sqlinject
        $senha = preg_replace(sql_regcase("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/"), "", $senha);
        //ciptografando a senha para compara-la com a que est� no banco
        $senha = sha1($usuario . $senha);
        //se os campos necess�rios forem digitados
        if ($usuario && $senha) {
            //verifica se existe um usu�rio cadastrad no sistema com este login
            $sql = "select cre_cod, cre_usuario, cre_nome_fantasia, cre_senha, cre_relatorios from credores WHERE cre_usuario = '$usuario' AND cre_ativo = 1";
            $query = $this->db->query($sql, array($usuario, $senha));

            //caso haja um registro o usu�rio foi encontrado
            if ($query->num_rows > 0) {
            	$row = $query->row();
                //testa as duas senhas criptografadas para ver se a digitada esta correta
                if ($row->cre_senha == $senha) {
                        //pega o nome do usuario no banco e separa somente o
                        //primeiro nome para ser exibido no header
                        $nome = $row->cre_nome_fantasia;
                        $aux = strpos($nome, ' ');
                        $nome = substr($nome, 0, $aux);
						if (empty($nome)) $nome = $row->cre_nome_fantasia;
                        
                        // montanto array de dados da sess�o
                        $dadosusu = array(
                            //c�digo do usu�rio logado
                            'usucod' => $row->cre_cod,
                            //nome completo do usu�rio
                            'usunome' => $row->cre_nome_fantasia,
                            //primeiro nome do usuario
                            'usuwho' => $nome,
                            //setando o status da sess�o para true(ativa)
                            'status' => true
                        );

                        //joga dados na sess�o
                        $this->session->set_userdata($dadosusu);
                        // redireciona o usu�rio para a tela de inicio
                                                
//                        redirect(base_url() . 'home');
                          redirect(base_url() . 'inadimplentes');
                        die('Aqui - '.$nome);
                } else {
                    //se a senha for incorreta retorna para a tela de logim com uma mensagem de erro
                    $this->data['ulogin'] = $query->row()->cre_usuario;
                    $this->inicore->setMensagem('error', 'SENHA incorreta, favor verificar e digitar novamente', false);
                }
            } else {
                //caso o usu�rio n�o seja encontrado n�o acessa o sistema e
                //volta para a tela de login com mensagem de erro
                $this->inicore->setMensagem('error', 'Usu�rio inv�lido, por favor verifique', false);
            }
        }
        //caso aconte�a algum erro na hora de logar no sistema, redireciona
        //para a tela de login
        $this->inicore->loadview('login', $this->data);
    }

    //fun��o usada para destruir a sess�o e sair do sistema
    function sair() {
        $this->session->sess_destroy();
        redirect(base_url() . 'login');
    }

}

?>