<script type="text/javascript">
    $(document).ready(function(){
        $('.fechar').click(function(e){
            e.preventDefault();
            $('#dialog-form').dialog('close');
        });
        
        $(".dialog-form-open").click(function () {
            $("#dialog-form").dialog("open");
            return false;
        });
    });
</script>
        <!-- Caixa de di�logo RO-->
<!--        <div id="dialog-form" class="form" style="padding-left: 5px; padding-right: 5px;" title="Novo registro">
            <form id="form" action="<?php echo base_url().'ro/incluir' ?>" method="post">
                <div style="width: 400px; margin: 0 auto; margin-top: 20px;">
                    <select style="width: 400px;" id="operacao" name="operacao">
                        <?php foreach ($operacoes as $ope):?>
                        <option value="<?php echo $ope->ope_cod;?>"><?php echo utf8_decode($ope->ope_nome);?></option>
                        <?php endforeach;?>
                    </select>
                    <input type="hidden" value="<?php echo $cobrancas ?>" name="cobrancas"/>
                    <input type="hidden" value="<?php echo $this->session->userdata('usucod');?>" name="usuario"/>
                    <input type="hidden" value="<?php echo date("Y-m-d");?>" name="data"/>
                    <input type="hidden" value="<?php echo date("H:i:s");?>" name="hora"/>
                    
                </div>
                <div class="fields" style="width: 400px; margin: 0 auto; margin-top: 15px;">
                    <div class="field" style="width: 400px;">
                        <div style="width: 84px; padding-top: 0px;" class="label">
                            <label for="obs">Observa��o:</label>
                        </div>
                        <div class="textarea">
                            <textarea style="width: 386px; height: 95px;" id="obs" name="obs" cols="50" rows="10 " class="editor"></textarea>
                        </div>
                        <div style="margin-top: 15px; padding-top: 7px;  border-top: 1px solid #ddd;">
                            <p><i>Para agendar uma nova tarefa preencha os campos abaixo</i></p>
                            <div style="float: left;margin-right: 30px;">
                                <div style="width: 75px; padding-left: 1px;" class="label">
                                    <label for="data_agendamento">Agendar:</label>
                                </div>
                                <div style=" width: 69px;" class="input">
                                    <input type="text" id="data_agendamento" name="data_agendamento" class="date" />
                                </div>
                            </div>
                            <div style="float: left;">
                                <div style="width: 95px; padding-left: 1px;" class="label">
                                    <label for="tarefa">Tarefa:</label>
                                </div>
                                <div style=" width: 269px;" class="input">
                                    <input type="text" id="tarefa" name="tarefa" style="width: 229px;" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center; margin-top: 10px; margin-bottom: 10px;" class="buttons">
                        <input type="reset" class="fechar" name="cancelar" value="Cancelar" />
                        <div style="margin-left: 7px;" class="highlight">
                            <input type="submit" name="submit.highlight" value="Cadastrar" />
                        </div>
                    </div>
                </div>
            </form>
        </div>-->
<!-- FIM Caixa de di�logo RO-->
        <div id="content">

        <?php echo $sidebar; ?>

            <div id="right">
                <div id="box" class="box" style="min-height: 798px;">
                    <div class="title">
                        <h5>Registro de opera��es</h5>
                        <div class="form" style="clear: none; float: right; padding-top:5px; height: 27px;">
                            <div class="fields">
                                <div class="field" style="border: none; margin: 0px; padding: 0px;">
                                    <div class="highlight">
                                        <a href="<?php  echo base_url().'inadimplentes/ficha/inaCod:'.$inaCod. '/recupCod:'. $recupCod;?>">
                                        <input style="width: 55px; font-size: 13px; font-weight: bold;" type="submit" name="cadastrar" value="Ficha" />
                                        </a>
                                    </div>
<!--                                    <div class="highlight">
                                        <input style="width: 155px; font-size: 13px; font-weight: bold;" class="dialog-form-open" type="submit" name="cadastrar" value="Novo registro" />
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="virgem">
                        <?php echo $mensagem ?>
                        <div class="form">
                            <div class="fields">
                                <div class="field  field-first" style="border: none;">
                                    <div class="divleftlast" style="width: 616px;">
                                        <div class="label">
                                            <label>Credor:</label>
                                        </div>
                                        <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $this->session->userdata('usunome');?> <br/></p>
                                    </div>
                                </div>
                                <div class="field  field-first">
                                    <div class="divleftlast" style="width: 466px;">
                                        <div class="label">
                                            <label>Inadimplente:</label>
                                        </div>
                                        <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inaNome ;?></p>
                                    </div>
                                </div>                                
                                <div class="table" style="padding: 0px 5px 10px;">
                                    <table id="products">
                                        <thead>
                                            <tr>
                                                <th class="left" style=" text-align: center; width: 100px;">Data</th>
                                                <th>Recuperador</th>
                                                <th>Opera��o</th>
                                                <th class="last">Detalhes</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($ros as $ro):?><!-- foreach ros -->
                                                <tr>
                                                    <td class="title"><?php echo convData($ro->ros_data,'d');?></td>
                                                    <td><?php echo $ro->recuperador;?></td>
                                                    <td><?php echo utf8_decode($ro->operacao);?></td>
                                                    <td class="last"><?php echo utf8_decode($ro->ros_detalhe);?></td>
                                                </tr>
                                            <?php endforeach;?><!-- endforeach ros -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>