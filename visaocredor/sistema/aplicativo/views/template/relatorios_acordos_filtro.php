<div id="content">
	<?php echo $sidebar; ?>
	<div id="right">
		<div id="box-tabs" class="box" style="min-height: 798px;">
			<div class="title">
				<h5>Relat�rio de Acordos Gerados</h5>
			</div>
			<?php echo $mensagem; ?>
			<form id="form_relatorios_acordos" action="<?php echo base_url().'relatorios/pesAcordos/c/'.$crerel_cod; ?>" method="post" target="_blank">
				<div class="form">
					<div class="fields" style="height: 450px">
						<table>
							<tr class="field">
								<td width="125" rowspan="2">
								<b>Per&iacute;odo</b>:</td>
								<td>
								<input type="radio" name="filtro_periodo" id="pesquisar_acordos_intervalo" value="I" />
								Intervalo </td>
								<td><label for="de">De:</label>
								<input type="text" id="pesquisar_acordos_intervalo_data_de" name="periodo_data_de" class="dataMascara" />
								<label for="ate">At�:</label>
								<input type="text" id="pesquisar_acordos_intervalo_data_ate" name="periodo_data_ate" class="dataMascara" />
								</td>
							</tr>
							<tr class="field">
								<td colspan="2">
								<input type="radio" name="filtro_periodo" id="pesquisar_acordos_intervalo_todas" value="T" />
								Todas </td>
							</tr>
						</table>
						<div style="clear:both"></div>

						<div class="divlast">
							<div class="buttons" style="margin-top: 10px;">
								<div class="highlight">
									<button type="submit">Pesquisar</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	(function($) {

		var cookieInputStatus = function(obj) {
			$.cookie($(obj).attr('id'), $(obj).val());
		};
		var cookieCheckboxStatus = function(obj) {
			var status = $(obj).is(':checked') ? 1 : 0;
			$.cookie($(obj).attr('id'), status);
		};
		var cookieRadioStatus = function(obj) {
			$.cookie($(obj).attr('name'), $(obj).val());
		};
		$(':checkbox').change(function() {
			cookieCheckboxStatus(this);
			//console.log($.cookie($(this).attr('id')));
		});
		$(':checkbox').each(function() {
			if ($.cookie($(this).attr('id')) == 1) {
				$(this).attr('checked', true);
			} else {
				$(this).removeAttr('checked');
			}
		});
		$(':radio').change(function() {
			cookieRadioStatus(this);
		});
		$(':radio').each(function() {
			if ($.cookie($(this).attr('name')) == $(this).val()) {
				$(this).attr('checked', true);
			} else {
				$(this).removeAttr('checked');
			}
		});
		$('#form_relatorios_acordos input:not(:checkbox, :radio), select').change(function() {
			cookieInputStatus(this);
		});
		$('#form_relatorios_acordos input:not(:checkbox, :radio), select').each(function() {
			var objVal = $.cookie($(this).attr('id'));
			if (objVal != undefined)
				$(this).val(objVal);
		});

		$('select.chosen').chosen();

		$(".dataMascara").datepicker();

	})(jQuery); 
</script>
