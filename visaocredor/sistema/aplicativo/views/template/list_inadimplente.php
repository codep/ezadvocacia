<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Lista de inadimplentes</h5>
            </div>
            <?php echo $mensagem; ?>
        	<div>
	            <form id="form" action="<?php echo base_url().'inadimplentes' ?>" method="post">
	                <div class="form">
	                    <div class="fields">
	                        <div class="field">
	                            <div class="divleft" style="border: none; width: 210px;">
	                                <div style="margin-left: 6px;" class="select">
	                                    <select style="width: 204px;" id="filtro1" name="filtro1">
	                                    	<option value="">Todos</option>
	                                        <option value="ina_nome">Nome</option>
	                                        <option value="telefone">Telefone</option>
	                                        <option value="ina_cidade">Cidade</option>
	                                        <option value="telenonePAR">Telefone (parentes)</option>
	                                        <option value="ina_endereco">Endere�o</option>
	                                        <option value="ina_cpf_cnpj">CPF</option>
	                                        <option value="parentes">Parentes</option>
	                                        <option value="ina_mae_nome">Nome da m�e</option>
	                                        <option value="ina_pai_nome">Nome do pai</option>
	                                    </select>
	                                    <script>$('#filtro1').val('<?php echo $filtro1; ?>');</script>
	                                </div>
	                            </div>
	                            <div class="divleft" style="width: 394px;">
	                                <div class="input">
	                                    <input style="width: 370px;" type="text" id="filtro1_desc" name="filtro1_desc" value="<?php echo $filtro1_desc; ?>" />
	                                </div>
	                            </div>
	                            <div class="divleftlast" style="width: 87px; margin: 0px;">
	                                <div class="buttons">
	                                    <div class="highlight">
	                                        <input style="width: 85px;" type="submit" name="pesquisar1" value="Pesquisar" />
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </form>
                <div class="form">
                    <div class="fields">
                        <div class="table" style="padding: 0px 5px 10px; border-bottom: 1px solid #ddd;">
                            <table id="products">
                                <thead>
                                    <tr>
                                        <th>Inadimplente</th>
                                        <th>CPF/CNPJ</th>
                                        <th>Cidade</th>
                                        <th class="last">Ficha</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($inadsCredor as $inads): ?>
                                    <tr class="visualizar" id="<?php echo base_url().'inadimplente/editar/cod:'.$inads->ina_cod ?>">
                                        <td><?php echo utf8_decode($inads -> ina_nome); ?></td>
                                        <td><?php echo utf8_decode($inads -> ina_cpf_cnpj); ?></td>
                                        <td><?php echo utf8_decode($inads -> ina_cidade); ?></td>
                                        <td class="last">
                                            <a href="<?php echo base_url() . 'inadimplentes/ficha/inaCod:' . $inads -> ina_cod . '/recupCod:' . $inads -> usuarios_responsavel_cod; ?>">
                                                FICHA
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="pagination pagination-left">
                            <div class="results">
                                <span>Total de inadimplentes: <i><?php echo "$totalInadimplentes"; ?></i></span>
                            </div>
                            <div class="pager">
                                <?php echo $paginacao; ?>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
