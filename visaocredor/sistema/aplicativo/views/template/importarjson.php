<script type="text/javascript">
</script>
<!--<div id="dialog-form" style="padding-left: 5px; padding-right: 5px; " title="Pesquisar dados">
    <p style="text-align: center">
        <strong>Dados encontrados:</strong>
    </p>
    <div id="content" style="width: 750px;">
        <div id="right" style="margin-left: 0px;">
            <div class="box">
                <div class="table" style="padding-left: 0px; font-size: 10px; width: 750px;">
                    <form action="" method="post">
                        <table id="products">
                            <thead>
                                <tr>
                                    <th class="left" style="width: 20%;">Nome</th>
                                    <th style="width: 14%;">CPF/CNPJ</th>
                                    <th>ENDERE�O</th>
                                    <th class="last">CIDADE</th>
                                </tr>
                            </thead>
                            <tbody id="dadosPesquisa">
                                RESULTADO DA PESQUISA AQUI!! 
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->
<style type="text/css">
    a{
        color: black;
    }
    .tabela{
        width: 702px !important;
        margin: 7px 0 7px 20px !important;
    }
    .tabela td{
        padding: 2px !important;
        text-align: center !important;
    }
    #content div.box table th{
    	padding: 5px;
    	vertical-align: middle;
	}
	.vertCenter{
	    vertical-align: middle;
	    text-align: center;
	}
	.okRed{
	    color: red;
	}
	.okRed:hover{
	    color: red !important;
	}

	.legendaBox {
		display: block; width: 10px; height: 8px; float:left;
	}
	.legendaLabel {
		display: block; float:left; width: 200px; margin-left: 5px;
	}
	.bgErro {
		/*background-color: #d9534f !important;
		color: #FFF;
		*/
		border: 2px solid #d9534f !important;
	}
	.bgSucesso {
		border: 2px solid #47a447 !important;            		
	}
	tr.bgDuplicado td {
		background-color: #ed9c28 !important;
		color: #FFF !important;
	}
	i.bgDuplicado {
		border: 2px solid #ed9c28 !important;
	}

</style>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Importa��o</h5>
            </div>
            <a style="display: block; margin-top: 15px; text-align: center; font-size: 20px;" href="<?php echo base_url().'importacao/principal' ?>">Voltar</a>
            
            <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
            	<?php echo $mensagem ?>
            </div>
            <?php if (is_array($registros) && count($registros)>0) : ?>
            <div style="display:block; margin: 10px auto; width: 702px;">
            	<i class="legendaBox bgErro"></i>
            	<strong class="legendaLabel"> ERRO</strong>
            	
            	<i class="legendaBox bgSucesso"></i>
            	<strong class="legendaLabel"> SUCESSO</strong>
            	
            	<i class="legendaBox bgDuplicado"></i>
            	<strong class="legendaLabel"> DUPLICADOS</strong>
            	
            	<div style="clear: both"></div>
            </div>
            <?php foreach ($registros as $creCNPJ => $inadimplentes): ?>
            
            <?php foreach ($inadimplentes as $i => $inadimplente): ?>
				<?php
					if (isset($erros[$i]))  {
						$cssClass = 'bgErro';	
					} else {
						$cssClass = 'bgSucesso';
					}
				?>
			<h3>CREDOR: <?php echo $creCNPJ; ?></h3>
            <table class="tabela <?php echo $cssClass; ?>">
                <thead>
                	<tr>
                		<th colspan="6"><?php echo $inadimplente['NOME'].' - '.$inadimplente['CPFCNPJ'] ?></th>
                	</tr>
                    <tr>
                        <td>Emissao</td>
                        <td>Cap</td>
                        <td>Venc</td>
                        <td>Parc</td>
                        <td class="last">Dup</td>
                    </tr>
                </thead>
                <tbody>
                	<?php foreach ($inadimplente['DIVIDAS'] as $d => $divida): ?>
						<?php
							$dupClass = (isset($divida['DUPLICADA']) && $divida['DUPLICADA']==1)?'bgDuplicado':''; 
						?>
                    <tr class="<?php echo $dupClass; ?>">
                        <td><?php echo convData($divida['DATAEMISSAO'],'d') ?></td>
                        <td><?php echo convMoney($divida['VALORTOTAL']) ?></td>
                        <td><?php echo convData($divida['VENCIMENTOINICIAL'],'d') ?></td>
                        <td><?php echo $divida['NUMPARCELAS'] ?></td>
                        <td class="vertCenter last"><?php echo $divida['DUPLICADA'] == 0?'N�o':'Sim' ?></td>
                    </tr>
                    <?php endforeach; ?>
					<?php if (isset($erros[$i])): ?>
                    <tr>
                    	<td colspan="6">
                    			<b>Erros:</b>
                    			<ul>
                    			<?php foreach ($erros[$i] as $erro): ?>
                    				<li><?php echo $erro; ?></li>
                    			<?php endforeach; ?>
                    			</ul>			
                		</td>
                    </tr>
					<?php endif; ?>                    
                </tbody>
            </table>
            <?php endforeach; ?>
            <?php endforeach; ?>
			<pre><?php //var_dump($registros); ?></pre>
            <?php endif; ?>
        </div>
    </div>
</div>