<style type="text/css">

    .blocoestrelas{
        width: 160px;
        height: 32px;
        margin-top: 6px;
    }
    .blocoestrelas div{
        cursor: pointer;
    }
    .estrela1{
        display: block;
        float:left;
        width: 20px;
        height: 20px;
        background: url("<?php echo $img . 'star1.png'; ?>") top no-repeat;
    }
    .estrela2{
        display: block;
        float:left;
        width: 20px;
        height: 20px;
        background: url("<?php echo $img . 'star2.png'; ?>") top no-repeat;
    }
    .estrela3{
        display: block;
        float:left;
        width: 20px;
        height: 20px;
        background: url("<?php echo $img . 'star3.png'; ?>") top no-repeat;
    }
    .estrela4{
        display: block;
        float:left;
        width: 20px;
        height: 20px;
        background: url("<?php echo $img . 'star4.png'; ?>") top no-repeat;
    }
    .estrela5{
        display: block;
        float:left;
        width: 20px;
        height: 20px;
        background: url("<?php echo $img . 'star5.png'; ?>") top no-repeat;
    }

    .estrela1:hover{
        background: url("<?php echo $img . 'star1.png'; ?>") bottom no-repeat;
    }
    .estrela2:hover{
        background: url("<?php echo $img . 'star2.png'; ?>") bottom no-repeat;
    }
    .estrela3:hover{
        background: url("<?php echo $img . 'star3.png'; ?>") bottom no-repeat;
    }
    .estrela4:hover{
        background: url("<?php echo $img . 'star4.png'; ?>") bottom no-repeat;
    }
    .estrela5:hover{
        background: url("<?php echo $img . 'star5.png'; ?>") bottom no-repeat;
    }
    .selected{
        background-position: bottom !important;
    }
    .btnRos{
        margin:18px 0 0 10px;
    }
</style>
<script type="text/javascript">
    jQuery(document).ready(function(){
        $('.accordion').accordion({
            autoheight:false
        }); 
        
        $('.blocoestrelas div').click(function(){
            window.location.href=$(this).attr('href');
            
        });
    });
</script>
<div id="content">

    <?php echo $sidebar; ?>

    <div id="right" style="min-height: 798px;">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Ficha (<?php echo $inadDados->ina_nome ?>)</h5>
                <ul class="links">
                    <li><a href="#inad">Cadastro</a></li>
                    <li><a href="#dividas">D�vidas</a></li>
                    <li><a href="#acordos">Acordos</a></li>
                    <li><a href="#juridico">Jur�dico</a></li>
                </ul>
            </div>
            <div id="inad">
                <form id="formUP" action="<?php echo base_url() . 'divida/atualizarInadimplente/cod:' . $inadDados->ina_cod ?>" method="post">
                    <?php echo $mensagem ?>
                    <div class="form">
                        <div class="fields">
                            <div class="field  field-first">
                                <div class="divleft" style="width: 116px;">
                                    <div class="label">
                                        <label>C�digo:</label>
                                    </div>
                                    <input type="hidden" name="cod"  value="<?php echo $inadDados->ina_cod; ?>"/>
                                    <p class="forte" style=" padding-top: 5px; color: #356BA0;float: left; margin: 0 auto;"><?php echo $inadDados->ina_cod; ?></p>
                                </div>
                                <div id="atrsPessoaFisica" style="width:375px; height:30px; float: right; overflow: hidden">
                                    <div id="sexo_est_civil">
                                        <div class="divleft" style="width: 173px;" id="sexo" >
                                            <div class="label">
                                                <label>Sexo:</label>
                                            </div>
                                            <input type="radio" name="sexo" value="m" style="margin-top: 7px;" <?php echo $inadDados->ina_sexo == 'm' ? 'checked' : ''; ?>>Masc.
                                            <input type="radio" name="sexo" value="f" <?php echo $inadDados->ina_sexo == 'f' ? 'checked' : ''; ?> >Fem.
                                        </div>
                                        <div class="divleftlast" style="width: 184px;" id="estadocivil">
                                            <div class="select">
                                                <select style="width: 186px; margin-left: 5px;" id="est_civil" name="estado_civil">
                                                    <option value="0" class="ec">Estado civil</option>
                                                    <option value="1" class="ec" <?php echo $inadDados->ina_estado_civil == 1 ? 'selected' : ''; ?>>Solteiro(a)</option>
                                                    <option value="2" class="ec" <?php echo $inadDados->ina_estado_civil == 2 ? 'selected' : ''; ?>>Casado(a)</option>
                                                    <option value="3" class="ec" <?php echo $inadDados->ina_estado_civil == 3 ? 'selected' : ''; ?>>Separado(a)</option>
                                                    <option value="4" class="ec" <?php echo $inadDados->ina_estado_civil == 4 ? 'selected' : ''; ?>>Divorciado(a)</option>
                                                    <option value="5" class="ec" <?php echo $inadDados->ina_estado_civil == 5 ? 'selected' : ''; ?>>Viuvo(a)</option>
                                                    <option value="6" class="ec" <?php echo $inadDados->ina_estado_civil == 6 ? 'selected' : ''; ?>>Amasiado(a)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 371px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="nome">Nome:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 248px;" type="text" readonly="readonly" id="nome" name="nome_fantasia" maxlength="120" value="<?php echo utf8_decode($inadDados->ina_nome); ?>"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 231px;">
                                    <div style="width: 76px; padding-left: 1px;" class="label">
                                        <label for="cpf_cnpj" >CPF/CNPJ:</label>
                                    </div>
                                    <div class="input" id="cpf_cnpj_div">
                                        <input style="<?php echo $inadDados->ina_cpf_cnpj == '' ? 'width: 124px;' : 'width: 124px; background-color:#F6F6F6;'; ?>" type="text" id="cpf_cnpj" name="cpf_cnpj" maxlength="18" value="<?php echo $inadDados->ina_cpf_cnpj; ?>" <?php echo $inadDados->ina_cpf_cnpj == '' ? '' : 'readonly'; ?>/>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 211px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="rg_ie">RG/I.E.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 89px;" type="text" readonly="readonly" id="rg_ie" maxlength="30" name="rg_ie" value="<?php echo $inadDados->ina_rg_ie; ?>"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 250px;">
                                    <div style="width: 42px; padding-left: 1px;" class="label">
                                        <label for="endereco">End.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 178px;" type="text" readonly="readonly" id="endereco" maxlength="130" name="endereco" value="<?php echo utf8_decode($inadDados->ina_endereco); ?>"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 233px; margin: 0px;">
                                    <div style="width: 51px; padding-left: 1px;" class="label">
                                        <label for="bairro">Bairro:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 155px;" type="text" readonly="readonly" id="bairro" maxlength="50" name="bairro" value="<?php echo utf8_decode($inadDados->ina_bairro); ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 211px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="complemento">Compl.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 89px;" type="text" readonly="readonly" id="complemento" name="complemento" maxlength="40" value="<?php echo utf8_decode($inadDados->ina_complemento); ?>"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 132px;">
                                    <div style="width: 34px; padding-left: 1px;" class="label">
                                        <label for="cep">CEP.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 65px;" type="text" readonly="readonly" id="cep" name="cep" maxlength="8" value="<?php echo $inadDados->ina_cep; ?>"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 109px;">
                                    <div style="width: 27px; padding-left: 1px;" class="label">
                                        <label for="uf">UF:</label>
                                    </div>
                                    <div class="select">
                                        <select style="width: 65px; margin-left: 7px;" id="ina_uf" name="uf">
                                            <option><?php echo $inadDados->ina_uf; ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 230px; margin: 0px; float: left">
                                    <div class="select">
                                        <select style="width: 230px; height: 28px; border: 1px solid #D4D0C8; padding: 5px; font-size: 11px; font-family: Verdana" id="ina_cidade" name="cidade">
                                            <option style="border: 1px solid;"><?php echo utf8_decode($inadDados->ina_cidade); ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 240px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="fone_res">Fone Res.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 117px;" type="text" readonly="readonly" id="fone_res" class="telefone" name="foneres" value="<?php echo $inadDados->ina_foneres ?>"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 238px;">
                                    <div style="width: 77px; padding-left: 1px;" class="label">
                                        <label for="fone_rec">Fone Rec.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 130px;" type="text" readonly="readonly" id="fone_rec" class="telefone" name="fonerec" value="<?php echo $inadDados->ina_fonerec ?>"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 216px; margin: 0px;">
                                    <div style="width: 74px; padding-left: 1px;" class="label">
                                        <label for="fone_com">Fone Com.:</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 115px;" type="text" readonly="readonly" id="fone_com" class="telefone" name="fonecom" value="<?php echo $inadDados->ina_fonecom ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="divleft" style="width: 240px;">
                                    <div style="width: 89px;" class="label">
                                        <label for="fone_cel1">Celular(1):</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 117px;" type="text" id="fone_cel1" readonly="readonly" class="telefone" name="cel1" value="<?php echo $inadDados->ina_cel1 ?>"/>
                                    </div>
                                </div>
                                <div class="divleft" style="width: 238px;">
                                    <div style="width: 77px; padding-left: 1px;" class="label">
                                        <label for="fone_cel2">Celular(2):</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 130px;" type="text" readonly="readonly" id="fone_cel2" class="telefone" name="cel2" value="<?php echo $inadDados->ina_cel2 ?>"/>
                                    </div>
                                </div>
                                <div class="divleftlast" style="width: 216px; margin: 0px;">
                                    <div style="width: 74px; padding-left: 1px;" class="label">
                                        <label for="fone_cel3">Celular(3):</label>
                                    </div>
                                    <div class="input">
                                        <input style="width: 115px;" type="text" readonly="readonly" id="fone_cel3" class="telefone" name="cel3" value="<?php echo $inadDados->ina_cel3 ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div style="height: 75px;" class="field">
                                <div class="divleftlast" style="width: 701px;">
                                    <div style="width: 84px;" class="label">
                                        <label for="informacoes">Informa��es relevantes:</label>
                                    </div>
                                    <div class="textarea">
                                        <textarea readonly="readonly" style="width: 586px; height: 50px;" id="informacoes" name="info" cols="500" rows="4"><?php echo $inadDados->ina_info ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <?php $menus2 = explode(",", $this->session->userdata('menu2'));
                            if (array_search('3', $menus2, TRUE) != ''): ?>
                                <div style="text-align: center; margin-top: 10px;" class="buttons">
                                    <div class="highlight">
                                        <input type="submit" name="cadastrar" value="Alterar" />
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </form>
            </div>
            <!-- -------------- ABA DIVIDAS -------------- -->
            <div id="dividas">
                <div class="form">
                    <?php echo "$mensagem"; ?>
                    <div>
                        <div style="background-color: #C4D6E0; padding-left: 10px; text-align: left;margin-top: 14px; margin-left: 5px; margin-right: 4px; width: 507px; float: left; margin-bottom: 3px; padding-bottom: 10px; padding-top: 8px;" class="blocoTitulo">
                            Credor: <?php echo "$creNomeFantasia"; ?>
                        </div>
                        <?php
                        $cobsCods = '';
                        $aux = 0;
                        foreach ($dividasAdm as $divid) {
                            if ($aux == 0) {
                                $cobsCods = $divid->cob_cod;
                            } if ($aux == 1) {
                                $cobsCods .= '-' . $divid->cob_cod;
                            } $aux = 1;
                        }
                        ?>
                        <?php if ($cobsCods != ''): ?>
                            <!-- *************** FORMULARIO PARA VER O HISTORICO DE ROS *************** -->
                            <form method="POST" action="<?php echo base_url() . 'inadimplentes/ros' ?>">
                                <input type="hidden" name="inaNome" value="<?php echo $inadDados->ina_nome; ?>"/>
                                <input type="hidden" name="inaCod" value="<?php echo $inaCod; ?>"/>
                                <input type="hidden" name="recupCod" value="<?php echo $recupCod; ?>"/>
                                <input type="hidden" name="cobsCods" value="<?php echo "$cobsCods"; ?>"/>
                                <div class="form" style="clear: none; float: left; margin-top: 8px; padding-top:5px; height: 27px;">
                                    <div class="fields">
                                        <div class="field" style="border: none; margin: 0px; padding: 0px;">
                                            <div class="highlight">
                                                <input style="width: 130px; font-size: 13px; font-weight: bold;" type="submit" name="ROs" value="Registro de ROs" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- *************** FIM FORMULARIO PARA VER O HISTORICO DE ROS *************** -->
                        <?php endif; ?>
                        <?php if ($cobsCods == ''): ?>
                            <div class="ACHeaderAcordo" style="clear: both; background-color: #DDD; padding-left: 10px; text-align: left;margin-top: 5px; margin-left: 0px; margin-right: 0px; width: 513px; float: left; margin-bottom: 3px; padding-bottom: 10px; padding-top: 8px; color:#000; font-weight: bold;">
                                Nada Consta
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="fields">                        
                        <div class="accordion">
                            <div class="table dividas_table" style="padding: 0px 5px 10px;">
                                <?php foreach ($dividasAdm as $divida): ?><!-- foreach dividas -->
                                    <a href="#" class="ACHeaderDivida" style="clear: both; background-color: #DDD; padding-left: 10px; text-align: left;margin-top: 5px; margin-left: 0px; margin-right: 0px; width: 692px; float: left; margin-bottom: 3px; padding-bottom: 10px; padding-top: 8px; color:#000; font-weight: bold;">
                                        Dados da d�vida c�digo:<?php echo $divida->cob_cod; ?>
                                    </a>
                                    <div style="clear: both;">
                                        <table id="products" class="meialinha">
                                            <thead>
                                                <tr class=" trfirst">
                                                    <th class="left">Dados do t�tulo</th>
                                                    <th>N�Doc</th>
                                                    <th>Parcela</th>
                                                    <th>Vencimento</th>
                                                    <th class="valor">Valor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="title" rowspan="<?php echo sizeof($divida->par_divida) + 1; ?>"><!-- rowspan="" -->
                                                        <b>Tipo de doc.: </b><?php echo $divida->div_documento; ?><br/>
                                                        <b>Emiss�o: </b><?php echo convData($divida->div_emissao, 'd'); ?><br/>
                                                        <b>Cadastro: </b><?php echo convData($divida->div_cadastro, 'd'); ?><br/>
                                                        <b>Total: </b><?php echo convMoney($divida->div_total); ?><br/>
                                                        <br/>
                                                        <b>Avalie essa cobran�a:</b><br/>
                                                        <div class="blocoestrelas">
                                                            <div href="<?php echo base_url() . 'inadimplentes/avaliar/cob:' . $divida->cob_cod . '/val:1/inaCod:' . $inaCod . '/recupCod:' . $recupCod; ?>" class="estrela1 <?php echo $divida->cob_avaliacao == 1 ? 'selected' : ''; ?>"></div>
                                                            <div href="<?php echo base_url() . 'inadimplentes/avaliar/cob:' . $divida->cob_cod . '/val:2/inaCod:' . $inaCod . '/recupCod:' . $recupCod; ?>" class="estrela2 <?php echo $divida->cob_avaliacao == 2 ? 'selected' : ''; ?>"></div>
                                                            <div href="<?php echo base_url() . 'inadimplentes/avaliar/cob:' . $divida->cob_cod . '/val:3/inaCod:' . $inaCod . '/recupCod:' . $recupCod; ?>" class="estrela3 <?php echo $divida->cob_avaliacao == 3 ? 'selected' : ''; ?>"></div>
                                                            <div href="<?php echo base_url() . 'inadimplentes/avaliar/cob:' . $divida->cob_cod . '/val:4/inaCod:' . $inaCod . '/recupCod:' . $recupCod; ?>" class="estrela4 <?php echo $divida->cob_avaliacao == 4 ? 'selected' : ''; ?>"></div>
                                                            <div href="<?php echo base_url() . 'inadimplentes/avaliar/cob:' . $divida->cob_cod . '/val:5/inaCod:' . $inaCod . '/recupCod:' . $recupCod; ?>" class="estrela5 <?php echo $divida->cob_avaliacao == 5 ? 'selected' : ''; ?>"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php foreach ($divida->par_divida as $parcela): ?><!-- foreach par_divida -->
                                                    <tr>
                                                        <td><?php echo $parcela->pad_doc_num; ?></td>
                                                        <td><?php echo $parcela->pad_par_num; ?></td>
                                                        <td><?php echo convData($parcela->pad_vencimento, 'd'); ?></td>
                                                        <td class="last"><?php echo convMoney($parcela->pad_valor); ?></td>
                                                    </tr>
                                                <?php endforeach; ?><!-- endforeach par_divida -->
                                            </tbody>
                                        </table>
                                        <!-- Fomul�rio de mensagem -->
                                        <form id="form" action="<?php echo base_url() . 'mensagens/novaMensagem' ?>" method="post">
                                            <div class="form">
                                                <div class="fields">
                                                    <div style="padding-top: 5px; padding-left: 92px;" class="field">
                                                        <input type="hidden" name="titulo" value="Coment�rio da d�vida: <?php echo $divida->cob_cod; ?>"/>
<!--                                                        <input type="hidden" name="recuperadorPadrao" value="<?php echo $divida->usuarios_usu_cod; ?>"/>-->
                                                        <input type="hidden" name="inaCod" value="<?php echo $inaCod; ?>"/>
                                                        <div style="width: 309px;">
                                                            <div style="margin-left: 6px;" class="select">
                                                                <select style="width: 294px;" id="destinatario" name="destinatario">
                                                                    <option value="selecione" selected>[Selecione o destinat�rio]</option>
                                                                    <option value="recuperador">Recuperador</option>
                                                                    <option value="diretoria">Diretoria</option>
                                                                    <option value="recuperadorDiretoria">Recuperador e Diretoria</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="height: 125px;" class="field">
                                                        <div class="divleftlast" style="width: 648px;">
                                                            <div style="width: 84px;" class="label">
                                                                <label for="mensagem">Mensagem:</label>
                                                            </div>
                                                            <div class="textarea">
                                                                <textarea style="width: 510px; height: 100px;" id="mensagem" name="mensagem" cols="500" rows="5"></textarea>
                                                            </div>
                                                            <div class="divCount" style="margin-left: 92px; font-weight: bold;"></div>
                                                        </div>
                                                    </div>
                                                    <div style="text-align: center; margin-top: 10px;" class="buttons">
                                                        <input type="reset" name="cancelar" value="Limpar" />
                                                        <div style="margin-left: 7px;" class="highlight">
                                                            <input type="submit" name="submit.highlight" value="Enviar" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- Fim fomul�rio de mensagem -->
                                    </div>
                                <?php endforeach; ?><!-- endforeach dividas -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- -------------- FIM ABA DIVIDAS -------------- -->
            <!-- ----------------------- ABA JURIDICO ----------------------- -->
            <div id="juridico">
                <div class="form">
                    <div class="fields">
                        <?php if (sizeof($dividasJudAcordsRecemEnviados) != 0): ?>
                            <div class="blocoTitulo2">
                                Acordos rec�m enviados
                            </div>
                            <table class="tabelaDados">
                                <thead>
                                    <tr>
                                        <td>Credor</td>
                                        <td>Valor Remanescente</td>
                                        <td>Data do Envio</td>
                                        <td class="last">A��es</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($dividasJudAcordsRecemEnviados as $recemEnviado): ?><!-- foreach acordos jud recem enviados -->
                                        <tr>
                                            <td><?php echo $recemEnviado->cre_nome_fantasia; ?></td>
                                            <td><?php echo convMoney($recemEnviado->aco_valor_original); ?></td>
                                            <td><?php echo convData($recemEnviado->aco_emissao, 'd'); ?></td>
                                            <td class="last" style=" text-align: center"><a href="<?php echo base_url() . 'inadimplentes/detalhesCobranca/c:' . $recemEnviado->aco_cod ?>">Ver dados</a></td>
                                        </tr>
                                    <?php endforeach; ?><!-- end foreach acordos jud recem enviados -->
                                </tbody>
                            </table>
                        <?php endif; ?>
                        <?php if (sizeof($dividasJudAcordsFirmados) != 0): ?>
                            <div class="blocoTitulo2">
                                Acordos j� firmados
                            </div>

                            <div>
                                <div style="background-color: #C4D6E0; padding-left: 10px; text-align: left;margin-top: 14px; margin-left: 5px; margin-right: 4px; width: 507px; float: left; margin-bottom: 3px; padding-bottom: 10px; padding-top: 8px;" class="blocoTitulo">
                                    Credor: <?php echo "$creNomeFantasia"; ?>
                                </div>
                                <?php
                                $cobsCods = '';
                                $aux = 0;
                                foreach ($dividasJudAcordsFirmados as $AcordJudFirmado) {
                                    if ($aux == 0) {
                                        $cobsCods = $AcordJudFirmado->cob_cod;
                                    } if ($aux == 1) {
                                        $cobsCods .= '-' . $AcordJudFirmado->cob_cod;
                                    } $aux = 1;
                                }
                                ?>
                                <?php if ($cobsCods != ''): ?>
                                    <!-- **************** FORMULARIO DE HISTORICO DE ROS **************** -->
                                    <form method="POST" action="<?php echo base_url() . 'inadimplentes/ros' ?>">
                                        <input type="hidden" name="inaNome" value="<?php echo $inadDados->ina_nome; ?>"/>
                                        <input type="hidden" name="inaCod" value="<?php echo $inaCod; ?>"/>
                                        <input type="hidden" name="recupCod" value="<?php echo $recupCod; ?>"/>
                                        <input type="hidden" name="cobsCods" value="<?php echo "$cobsCods"; ?>"/>
                                        <div class="form" style="clear: none; float: left; margin-top: 8px; padding-top:5px; height: 27px;">
                                            <div class="fields">
                                                <div class="field" style="border: none; margin: 0px; padding: 0px;">
                                                    <div class="highlight">
                                                        <input style="width: 130px; font-size: 13px; font-weight: bold;" type="submit" name="ROs" value="Registro de ROs" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- **************** FIM FORMULARIO DE HISTORICO DE ROS **************** -->
                                <?php endif; ?>
                                <div class="accordion">
                                    <?php foreach ($dividasJudAcordsFirmados as $AcordJudFirmado): ?><!-- foreach dividasJudAcordsFirmados -->

                                        <a href="#" class="ACHeaderAcordo" id="<?php echo $AcordJudFirmado->cob_cod; ?>" style="clear: both; background-color: #DDD; padding-left: 10px; text-align: left;margin-top: 5px; margin-left: 0px; margin-right: 0px; width: 692px; float: left; margin-bottom: 3px; padding-bottom: 10px; padding-top: 8px; color:#000; font-weight: bold;">Dados do acordo c�digo: <?php echo $AcordJudFirmado->cob_cod; ?></a>

                                        <div style="clear: both;">
                                            <table id="products" class="meialinha">
                                                <thead>
                                                    <tr class="trfirst">
                                                        <th class="left" style="width: 210px !important;">Dados do acordo</th>
                                                        <th>Parcela</th>
                                                        <th>Vencimento</th>
                                                        <th class="valor">Valor</th>
                                                        <th class="valor">Valor descontado</th>
                                                        <th class="valor">Data pagamento</th>
                                                        <th class="last">Situa��o</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="title" rowspan="<?php echo sizeof($AcordJudFirmado->par_acordo) + 1; ?>">
                                                            <b>Tipo: </b><?php echo $AcordJudFirmado->aco_tipo; ?><br/>
                                                            <b>Proced�ncia : </b><?php echo $AcordJudFirmado->aco_procedencia; ?><br/>
                                                            <b>Emiss�o: </b><?php echo convData($AcordJudFirmado->aco_emissao, 'd'); ?><br/>
                                                            <b>Val. original: </b><?php echo convMoney($AcordJudFirmado->aco_valor_original); ?><br/>
                                                            <b>Val. atualizado: </b><?php echo convMoney($AcordJudFirmado->aco_valor_atualizado); ?><br/><br/>
                                                            <b>Avalie essa cobran�a:</b><br/>
                                                            <div class="blocoestrelas">
                                                                <div href="<?php echo base_url() . 'inadimplentes/avaliar/cob:' . $AcordJudFirmado->cob_cod . '/val:1/inaCod:' . $inaCod . '/recupCod:' . $recupCod; ?>" class="estrela1 <?php echo $AcordJudFirmado->cob_avaliacao == 1 ? 'selected' : ''; ?>"></div>
                                                                <div href="<?php echo base_url() . 'inadimplentes/avaliar/cob:' . $AcordJudFirmado->cob_cod . '/val:2/inaCod:' . $inaCod . '/recupCod:' . $recupCod; ?>" class="estrela2 <?php echo $AcordJudFirmado->cob_avaliacao == 2 ? 'selected' : ''; ?>"></div>
                                                                <div href="<?php echo base_url() . 'inadimplentes/avaliar/cob:' . $AcordJudFirmado->cob_cod . '/val:3/inaCod:' . $inaCod . '/recupCod:' . $recupCod; ?>" class="estrela3 <?php echo $AcordJudFirmado->cob_avaliacao == 3 ? 'selected' : ''; ?>"></div>
                                                                <div href="<?php echo base_url() . 'inadimplentes/avaliar/cob:' . $AcordJudFirmado->cob_cod . '/val:4/inaCod:' . $inaCod . '/recupCod:' . $recupCod; ?>" class="estrela4 <?php echo $AcordJudFirmado->cob_avaliacao == 4 ? 'selected' : ''; ?>"></div>
                                                                <div href="<?php echo base_url() . 'inadimplentes/avaliar/cob:' . $AcordJudFirmado->cob_cod . '/val:5/inaCod:' . $inaCod . '/recupCod:' . $recupCod; ?>" class="estrela5 <?php echo $AcordJudFirmado->cob_avaliacao == 5 ? 'selected' : ''; ?>"></div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php foreach ($AcordJudFirmado->par_acordo as $parcelas): ?><!-- foreach parcelas do acordo -->
                                                        <tr style="color: <?php echo $parcelas->corCss; ?>">
                                                            <td><?php echo $parcelas->paa_parcela; ?></td>
                                                            <td><?php echo convData($parcelas->paa_vencimento, 'd'); ?></td>
                                                            <td><?php echo convMoney($parcelas->paa_valor); ?></td>
                                                            <td><?php echo convMoney($parcelas->valor_descontado); ?></td>
                                                            <td><?php echo $parcelas->dataUltimoPagamento; ?></td>
                                                            <td class="last"><?php echo $parcelas->situacao; ?></td>
                                                        </tr>
                                                    <?php endforeach; ?><!-- fim foreach parcelas do acordo -->
                                                </tbody>
                                            </table>
                                            <!-- Fomul�rio de mensagem -->
                                            <form id="form" action="<?php echo base_url() . 'mensagens/novaMensagem' ?>" method="post">
                                                <div class="form">
                                                    <div class="fields">
                                                        <div style="padding-top: 5px; padding-left: 92px;" class="field">
                                                            <input type="hidden" name="titulo" value="Coment�rio da d�vida: <?php echo $AcordJudFirmado->cob_cod; ?>"/>
                                                            <input type="hidden" name="recuperadorPadrao" value="<?php echo "$AcordJudFirmado->usuarios_usu_cod"; ?>"/>
                                                            <input type="hidden" name="inaCod" value="<?php echo "$inaCod"; ?>"/>
                                                            <div style="width: 309px;">
                                                                <div style="margin-left: 6px;" class="select">
                                                                    <select style="width: 294px;" id="destinatario" name="destinatario">
                                                                        <option value="selecione" selected>[Selecione o destinat�rio]</option>
                                                                        <option value="recuperador">Recuperador</option>
                                                                        <option value="diretoria">Diretoria</option>
                                                                        <option value="recuperadorDiretoria">Recuperador e Diretoria</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="height: 125px;" class="field">
                                                            <div class="divleftlast" style="width: 648px;">
                                                                <div style="width: 84px;" class="label">
                                                                    <label for="mensagem">Mensagem:</label>
                                                                </div>
                                                                <div class="textarea">
                                                                    <textarea style="width: 510px; height: 100px;" id="mensagem" name="mensagem" cols="500" rows="5"></textarea>
                                                                </div>
                                                                <div class="divCount" style="margin-left: 92px; font-weight: bold;"></div>
                                                            </div>
                                                        </div>
                                                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                                                            <input type="reset" name="cancelar" value="Limpar" />
                                                            <div style="margin-left: 7px;" class="highlight">
                                                                <input type="submit" name="submit.highlight" value="Enviar" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- Fim fomul�rio de mensagem -->
                                        </div>
                                    <?php endforeach; ?><!-- endforeach dividasJudAcordsFirmados -->
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php if ((sizeof($dividasJudAcordsRecemEnviados) == 0) && (sizeof($dividasJudAcordsFirmados)) == 0): ?>
                        <div class="ACHeaderAcordo" style="clear: both; background-color: #DDD; padding-left: 10px; text-align: left;margin-top: 5px; margin-left: 0px; margin-right: 0px; width: 513px; float: left; margin-bottom: 3px; padding-bottom: 10px; padding-top: 8px; color:#000; font-weight: bold;">
                            Nada Consta
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <!-- ----------------------- FIM ABA JURIDICO ----------------------- -->
            <!-- ---------------------------- ABA ACORDOS ---------------------------- -->
            <div id="acordos">
                <div class="form">
                    <div>
                        <div style="background-color: #C4D6E0; padding-left: 10px; text-align: left;margin-top: 14px; margin-left: 5px; margin-right: 4px; width: 507px; float: left; margin-bottom: 3px; padding-bottom: 10px; padding-top: 8px;" class="blocoTitulo">
                            Credor: <?php echo "$creNomeFantasia"; ?>
                        </div>
                        <?php
                        $cobsCods = '';
                        $aux = 0;
                        foreach ($acordosAdm as $acAdm) {
                            if ($aux == 0) {
                                $cobsCods = $acAdm->cob_cod;
                            } if ($aux == 1) {
                                $cobsCods .= '-' . $acAdm->cob_cod;
                            } $aux = 1;
                        }
                        ?>
                        <?php if ($cobsCods != ''): ?>
                            <form method="POST" action="<?php echo base_url() . 'inadimplentes/ros' ?>">
                                <input type="hidden" name="inaNome" value="<?php echo $inadDados->ina_nome; ?>"/>
                                <input type="hidden" name="inaCod" value="<?php echo $inaCod; ?>"/>
                                <input type="hidden" name="recupCod" value="<?php echo $recupCod; ?>"/>
                                <input type="hidden" name="cobsCods" value="<?php echo "$cobsCods"; ?>"/>
                                <div class="form" style="clear: none; float: left; margin-top: 8px; padding-top:5px; height: 27px;">
                                    <div class="fields">
                                        <div class="field" style="border: none; margin: 0px; padding: 0px;">
                                            <div class="highlight">
                                                <input style="width: 130px; font-size: 13px; font-weight: bold;" type="submit" name="ROs" value="Registro de ROs" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <?php endif; ?>
                        <?php if ($cobsCods == ''): ?>
                            <div class="ACHeaderAcordo" style="clear: both; background-color: #DDD; padding-left: 10px; text-align: left;margin-top: 5px; margin-left: 0px; margin-right: 0px; width: 513px; float: left; margin-bottom: 3px; padding-bottom: 10px; padding-top: 8px; color:#000; font-weight: bold;">
                                Nada Consta
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="fields">                        
                        <div class="accordion">
                            <div class="table acordos_table" style="padding: 0px 5px 10px;">
                                <?php foreach ($acordosAdm as $acAdm): ?><!-- foreach acordos -->
                                    <a href="#" class="ACHeaderAcordo" style="clear: both; background-color: #DDD; padding-left: 10px; text-align: left;margin-top: 5px; margin-left: 0px; margin-right: 0px; width: 692px; float: left; margin-bottom: 3px; padding-bottom: 10px; padding-top: 8px; color:#000; font-weight: bold;">
                                        Dados do acordo c�digo: <?php echo "$acAdm->cob_cod"; ?>
                                    </a>
                                    <div style="clear: both;">
                                        <table id="products" class="meialinha">
                                            <thead>
                                                <tr class="trfirst">
                                                    <th class="left" style="width: 210px !important;">Dados do acordo</th>
                                                    <th>Parcela</th>
                                                    <th>Vencimento</th>
                                                    <th class="valor">Valor</th>
                                                    <th class="valor">Valor descontado</th>
                                                    <th class="valor">Data pagamento</th>
                                                    <th class="last">Situa��o</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="title" rowspan="<?php echo sizeof($acAdm->par_acordo) + 1; ?>">
                                                        <b>Tipo: </b><?php echo $acAdm->aco_tipo; ?><br/>
                                                        <b>Proced�ncia : </b><?php echo $acAdm->aco_procedencia; ?><br/>
                                                        <b>Emiss�o: </b><?php echo convData($acAdm->aco_emissao, 'd'); ?><br/>
                                                        <b>Val. original: </b><?php echo convMoney($acAdm->aco_valor_original); ?><br/>
                                                        <b>Val. atualizado: </b><?php echo convMoney($acAdm->aco_valor_atualizado); ?><br/><br/>
                                                        <b>Avalie essa cobran�a:</b>
                                                        <div class="blocoestrelas">
                                                            <div href="<?php echo base_url() . 'inadimplentes/avaliar/cob:' . $acAdm->cob_cod . '/val:1/inaCod:' . $inaCod . '/recupCod:' . $recupCod; ?>" class="estrela1 <?php echo $acAdm->cob_avaliacao == 1 ? 'selected' : ''; ?>"></div>
                                                            <div href="<?php echo base_url() . 'inadimplentes/avaliar/cob:' . $acAdm->cob_cod . '/val:2/inaCod:' . $inaCod . '/recupCod:' . $recupCod; ?>" class="estrela2 <?php echo $acAdm->cob_avaliacao == 2 ? 'selected' : ''; ?>"></div>
                                                            <div href="<?php echo base_url() . 'inadimplentes/avaliar/cob:' . $acAdm->cob_cod . '/val:3/inaCod:' . $inaCod . '/recupCod:' . $recupCod; ?>" class="estrela3 <?php echo $acAdm->cob_avaliacao == 3 ? 'selected' : ''; ?>"></div>
                                                            <div href="<?php echo base_url() . 'inadimplentes/avaliar/cob:' . $acAdm->cob_cod . '/val:4/inaCod:' . $inaCod . '/recupCod:' . $recupCod; ?>" class="estrela4 <?php echo $acAdm->cob_avaliacao == 4 ? 'selected' : ''; ?>"></div>
                                                            <div href="<?php echo base_url() . 'inadimplentes/avaliar/cob:' . $acAdm->cob_cod . '/val:5/inaCod:' . $inaCod . '/recupCod:' . $recupCod; ?>" class="estrela5 <?php echo $acAdm->cob_avaliacao == 5 ? 'selected' : ''; ?>"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php foreach ($acAdm->par_acordo as $parcelas): ?><!-- foreach parcelas do acordo -->
                                                    <tr style="color: <?php echo "$parcelas->corCss"; ?>">
                                                        <td><?php echo $parcelas->paa_parcela; ?></td>
                                                        <td><?php echo convData($parcelas->paa_vencimento, 'd'); ?></td>
                                                        <td><?php echo convMoney($parcelas->paa_valor); ?></td>
                                                        <td><?php echo convMoney($parcelas->valor_descontado); ?></td>
                                                        <td><?php echo $parcelas->dataUltimoPagamento; ?></td>
                                                        <td class="last"><?php echo $parcelas->situacao; ?></td>
                                                    </tr>
                                                <?php endforeach; ?><!-- endforeach parcelas do acordo -->
                                            </tbody>
                                        </table>
                                        <!-- Fomul�rio de mensagem -->
                                    <form id="form" action="<?php echo base_url() . 'mensagens/novaMensagem' ?>" method="post">
                                        <div class="form">
                                            <div class="fields">
                                                <div style="padding-top: 5px; padding-left: 92px;" class="field">
                                                    <input type="hidden" name="titulo" value="Coment�rio da d�vida: <?php echo $acAdm->cob_cod; ?>"/>
                                                    <input type="hidden" name="recuperadorPadrao" value="<?php echo "$acAdm->usuarios_usu_cod"; ?>"/>
                                                    <input type="hidden" name="inaCod" value="<?php echo "$inaCod"; ?>"/>
                                                    <div style="width: 309px;">
                                                        <div style="margin-left: 6px;" class="select">
                                                            <select style="width: 294px;" id="destinatario" name="destinatario">
                                                                <option value="selecione" selected>[Selecione o destinat�rio]</option>
                                                                <option value="recuperador">Recuperador</option>
                                                                <option value="diretoria">Diretoria</option>
                                                                <option value="recuperadorDiretoria">Recuperador e Diretoria</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="height: 125px;" class="field">
                                                    <div class="divleftlast" style="width: 648px;">
                                                        <div style="width: 84px;" class="label">
                                                            <label for="mensagem">Mensagem:</label>
                                                        </div>
                                                        <div class="textarea">
                                                            <textarea style="width: 510px; height: 100px;" id="mensagem" name="mensagem" cols="500" rows="5"></textarea>
                                                        </div>
                                                        <div class="divCount" style="margin-left: 92px; font-weight: bold;"></div>
                                                    </div>
                                                </div>
                                                <div style="text-align: center; margin-top: 10px;" class="buttons">
                                                    <input type="reset" name="cancelar" value="Limpar" />
                                                    <div style="margin-left: 7px;" class="highlight">
                                                        <input type="submit" name="submit.highlight" value="Enviar" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- Fim fomul�rio de mensagem -->
                                    </div>
                                <?php endforeach; ?><!-- end foreach acordos -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ---------------------------- FIM ABA ACORDOS ---------------------------- -->
        </div>
    </div>
</div>
