<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img . 'logoez2.jpg'; ?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio - SP - (17) 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br | www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo">
            	Relat�rio Detalhado de Pagamentos por Devedores
            </p>
            <table class="rel_subinfo">
				<tr>
					<th>Per�odo:</th>
					<td><?php echo $periodo; ?></td>
				</tr>
				<tr>
					<th>Empresa:</th>
					<td><?php echo $credor; ?></td>
				</tr>
            </table>
        </div>
    </div>
    
	<table class="tblDados">
		<thead>
			<tr>
				<th style="width: 271px">Inadimplente</th>
				<th style="width: 200px">CPF/CNPJ</th>
				<th style="width: 160px">Total Pago Per�odo</th>
				<th style="width: 160px">Saldo Restante</th>
			</tr>
		</thead>
        <tbody class="padding">
        	<?php foreach ($inadimplentes as $inadimplente) : ?>
        	<tr>
        		<td data-ina_cod="<?php echo $inadimplente['ina_cod']; ?>"><?php echo $inadimplente['ina_nome']; ?></td>
        		<td><?php echo $inadimplente['ina_cpf_cnpj']; ?></td>
        		<td><?php echo convMoney($inadimplente['valor_pago']); ?></td>
        		<td><?php echo convMoney($inadimplente['valor_saldo']); ?></td>
        	</tr>
        	<?php  endforeach; ?>
        </tbody>
    </table>
    <table class="tblDados">
        <thead>
            <tr>
                <th class="right" style="">Total Geral:</th>
                <th class="center" style="width: 160px;"><?php echo convMoney($total_val); ?></th>
                <th class="center" style="width: 160px;"><?php echo convMoney($total_val); ?></th>
            </tr>
        </thead>
    </table>
</div>