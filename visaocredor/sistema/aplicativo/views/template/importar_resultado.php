<script type="text/javascript">
</script>
<style type="text/css">
</style>
<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Importa��o</h5>
            </div>
            <?php echo $mensagem ?>
            <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
                    Importa��o efetuada com sucesso:
            </div>
            <table class="tabela">
                <thead>
                    <tr>
                        <th>Emissao</th>
                        <th>Inad</th>
                        <th>Cap</th>
                        <th>Venc</th>
                        <th>Parc</th>
                        <th>Dup</th>
                        <th class="last">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dividas as $divida):?>
                    <tr>
                        <td><?php echo convData($divida->div_emissao,'d') ?></td>
                        <td><?php echo $divida->ina_nome ?></td>
                        <td><?php echo convMoney($divida->div_total) ?></td>
                        <td><?php echo convData($divida->div_venc_inicial,'d') ?></td>
                        <td><?php echo $divida->div_qtd_parc ?></td>
                        <td><?php echo $divida->imp_duplicidade == '0'?'N�o':'Sim' ?></td>
                        <td class="vertCenter last">[OK]</td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <a style="display: block; margin-top: 15px; text-align: center; font-size: 20px;" href="<?php echo base_url().'importacao/principal' ?>">Voltar</a>
        </div>
    </div>
</div>