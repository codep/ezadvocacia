<div class="conteudo">
    <div class="relatorio">
        <div class="linha1">
            <div class="logo">
                <img src="<?php echo $img . 'logoez2.jpg'; ?>" width="133" height="91"  alt="logo"/>
            </div>
            <div class="titulo">
                <h1>
                    ELIS�NGELA ZANUR�O - OAB/SP 251.797
                </h1>
                <h2>
                    AV. Jos� Ant�nio Pinto, 18, Vila Saudade  - CEP 15200-000
                </h2>
                <h2>
                    Jos� Bonif�cio - SP - (17) 3245-5028
                </h2>
                <h2 style="margin-top: 10px;">
                    elisangela@netnew.com.br | www.ezadvocacia.com.br
                </h2>
            </div>
        </div>
        <div class="linha2">
            <p class="rel_titulo">
            	Relat�rio de Acordos Gerados no Per�odo
            </p>
            <table class="rel_subinfo">
				<tr>
					<th>Per�odo:</th>
					<td><?php echo $periodo; ?></td>
				</tr>
				<tr>
					<th>Empresa:</th>
					<td><?php echo $credor; ?></td>
				</tr>
            </table>
        </div>
    </div>
    <?php foreach ($inadimplentes as $inadimplente) : ?>
    <table class="tblDados">
        <thead>
            <tr>
                <th class="left" style="width: 40%;"><?php echo utf8_decode($inadimplente['ina_nome']); ?></th>
                <th class="left" style="width: 40%;">CPF/CNPJ: <?php echo utf8_decode($inadimplente['ina_cpf_cnpj']); ?></th>
                <th class="left" style="width: 20%;">Acordos: <?php echo $inadimplente['acordos_num']; ?></th>
            </tr>
        </thead>
        <tbody>
            <tr>
            	<td colspan="3" class="container">
		    		<table class="tblDados">
		    			<thead>
		    				<tr>
		    					<th style="width: 160px">Acordo</th>
		    					<th style="width: 160px">Emiss�o</th>
		    					<th style="width: 160px">Parcela</th>
		    					<th style="width: 160px">Vencimento</th>
		    					<th style="width: 153px;">Valor</th>
		    				</tr>
		    			</thead>
				        <tbody>
				        	<?php $ka=0; ?>
				        	<?php $totalAcordos = count($inadimplente['acordos']); ?>
				        	<?php foreach($inadimplente['acordos'] as $acordo) : ?>
				        	<tr>
				        		<td><?php echo $acordo['aco_cod']; ?></td>
				        		<td><?php echo $acordo['aco_emissao']; ?></td>
				        		<td></td>
				        		<td></td>
				        		<td></td>
				        	</tr>
				        		<?php $totalParcela = count($acordo['parcelas']); ?>
					        	<?php foreach($acordo['parcelas'] as $kp=>$parcela) : ?>
					        	<tr>
					        		<td></td>
					        		<td></td>
					        		<td class="center"><?php echo $parcela['parcela'].'/'.$acordo['parcelas_num']; ?></td>
					        		<td class="center"><?php echo $parcela['vencimento']; ?></td>
					        		<td class="center"><?php echo convMoney($parcela['valor']); ?></td>
					        	</tr>
					        	<?php if (($kp+1)==$totalParcela): ?>
					        	<tr>
					        		<td colspan="4" class="right"><b>Sub-total:</b></td>
					        		<td><?php echo convMoney($acordo['parcelas_val']); ?></td>
				        		</tr>
				        		<?php endif; ?>
					        	<?php  endforeach; ?>
				        		<?php if (($ka+1)==$totalAcordos): ?>
					        	<tr>
					        		<td colspan="4" class="right"><b>Total:</b></td>
					        		<th><?php echo convMoney($inadimplente['acordos_val']); ?></th>
				        		</tr>				        			
			        			<?php endif; ?>
				        	<?php $ka++; ?>
				        	<?php  endforeach; ?>
				        </tbody>
			        </table>
            	</td>
            </tr>
        </tbody>
    </table>
    <?php  endforeach; ?>
    <table class="tblDados">
        <thead>
            <tr>
                <th class="right" style="">Total Geral:</th>
                <th class="center" style="width: 120px;"><?php echo convMoney($total_val); ?></th>
            </tr>
        </thead>
    </table>
</div>