<div id="content">
    <?php echo $sidebar; ?>
    <div id="right">
        <div id="box-tabs" class="box" style="min-height: 798px;">
            <div class="title">
                <h5>Importa��o</h5>
            </div>
            <?php echo $mensagem ?>
            <div>
	            <div style="margin-top: 16px; margin-bottom: 3px;" class="blocoTitulo">
	                    ATEN��O: O ENVIO � LIMITADO EM 2MB POR ARQUIVO!
	            </div>
	            <form id="form" action="<?php echo base_url().'importacao/importar'?>" enctype="multipart/form-data" method="post">
	                <div class="form">
	                    <div class="fields">
	                    	<b>IMPORTA��O .TXT</b>
	                        <div class="field">
	                            <div class="divleft" style="width: 604px;">
	                                <div class="input">
	                                	<input style="width: 590px;" type="file" id="file" name="arquivo" size="40" maxlength="255" value=""/>
	                                </div>
	                            </div>
	                            <div class="divleftlast" style="width: 87px; margin: 0px;">
	                                <div class="buttons">
	                                    <div class="highlight">
	                                        <input style="width: 85px;" type="submit" name="enviar1" value="Enviar" />
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </form>
	            <form id="form" action="<?php echo base_url().'importacao/importarjson'?>" enctype="multipart/form-data" method="post">
	                <div class="form">
	                    <div class="fields">
	                    	<b>IMPORTA��O .JSON</b> <a target="_blank" href="<?php echo base_url().'modelo_importacao.json'?>">(Visualizar modelo JSON)</a>
	                        <div class="field">
	                            <div class="divleft" style="width: 604px;">
	                                <div class="input">
	                                	<input style="width: 590px;" type="file" id="file" name="arquivo" size="40" maxlength="255" value=""/>
	                                </div>
	                            </div>
	                            <div class="divleftlast" style="width: 87px; margin: 0px;">
	                                <div class="buttons">
	                                    <div class="highlight">
	                                        <input style="width: 85px;" type="submit" name="enviar1" value="Enviar" />
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </form>
            </div>
        </div>
    </div>
</div>