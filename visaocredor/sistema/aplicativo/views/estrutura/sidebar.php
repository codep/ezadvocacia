<script type="text/javascript">
    $(document).ready(function(){
        //AO CLICAR EM PESSOA JURIDICA
        $('input[name="rbPesquiPor"][value="fone"]').click(function() {
            
            $('#buscaInput').html('<input style="width: 163px;" type="text" id="input-medium-br" name="inputBusRap"/>');
            
            $('input[name="inputBusRap"]').mask('(99)9999-9999');
            $('input[name="inputBusRap"]').attr('value', '');
        });

        $('input[name="rbPesquiPor"][value="parente"]').click(function() {

            $('#buscaInput').html('<input style="width: 163px;" type="text" id="input-medium-br" name="inputBusRap"/>');


            $('input[name="cpf_cnpj"]').attr('value', '');
        });
    });
</script>
<div id="left">
	<img src="<?php echo $img.'logoez.jpg'?>" alt="EZ Advocacia" style="margin: 8px 0 0 10px; width: 190px"/>
    <div id="menu">
        <h6 id="h-menu-cadastros" class="<?php echo $this->session->userdata('menusel')=='' || $this->session->userdata('menusel')=='cadastros' ? 'selected' : ''; ?>">
            <a href="#Cadastros" data-target="#menu-cadastros">
                <span>Menu do Credor</span>
            </a>
        </h6>
        <ul id="menu-cadastros" class="<?php echo $this->session->userdata('menusel')=='' || $this->session->userdata('menusel')=='cadastros' ? 'opened' : 'closed'; ?>">
            <li>
                <a href="<?php echo base_url();?>">Lista Inadimplentes</a>
            </li>
            <li>
                <a href="<?php echo base_url().'importacao/principal';?>">Importar dados</a>
            </li>
        </ul>
        
		<?php
		//RELAT�RIOS
        $query = $this->db->query("SELECT CR.crerel_cod, CR.crerel_nome, CR.crerel_link FROM credores_relatorios CR ORDER BY CR.crerel_nome ASC");
		$relatorios = $query->result();
		//ACESSO AOS RELATORIOS
        $query = $this->db->query("SELECT C.cre_relatorios FROM credores C WHERE C.cre_cod = '".$this->session->userdata('usucod')."'");
		$credor = $query->row();
        //relatorios
        $creRels = empty($credor->cre_relatorios) ? array() : explode(',', $credor->cre_relatorios);
		?>
		
        <?php if ($query->num_rows>0 && count($creRels)>0) : ?>
        <h6 id="h-menu-relatorios" class="<?php echo $this->session->userdata('menusel')=='relatorios' ? 'selected' : ''; ?>">
            <a href="#Cadastros" data-target="#menu-relatorios">
                <span>Relat�rios</span>
            </a>
        </h6>
        <ul id="menu-relatorios" class="<?php echo $this->session->userdata('menusel')=='relatorios' ? 'opened' : 'closed'; ?>">
        	<?php foreach($relatorios as $relatorio) : ?>
    		<?php if (!in_array($relatorio->crerel_cod,$creRels)) continue; //VERIFICA PERMISS�O AO RELAT�RIO ?>
            <li><a href="<?php echo base_url().'relatorios/'.$relatorio->crerel_link.'/c/'.$relatorio->crerel_cod; ?>"><?php echo utf8_decode($relatorio->crerel_nome); ?></a></li>
            <?php endforeach; ?>
        </ul>
		<?php endif; ?>
        
    </div>

    <div id="date-picker"></div>
<!--    <div>
        <div style="border-bottom: 1px solid #CDCDCD; border-top: 1px solid #CDCDCD;" class="box">
            <form id="form" action="< ?php echo base_url() . 'pesquisar/buscaRapida' ?>" method="post">
                <div class="form">
                    <div class="fields">
                        <div style="border: none; padding: 0px; margin: 0px;"  class="field  field-first">
                            <div class="divleftlast" style="width: 116px; margin: 0px; padding: 0px;">
                                <div style="padding-left: 0px;" class="label">
                                    <label>Busca R�pida:</label>
                                </div>
                            </div>
                        </div>
                        <div style="padding-top: 3px; height: 70px;" class="field">
                            <div class="divleft" style="width: 71px; border: none;">
                                <div class="input">
                                    <input type="radio" name="rbPesquiPor" value="fone" style="display: block; float: left; clear: both; margin-right: 5px;" />Por Telefone<br/><br/>
                                    <input type="radio" name="rbPesquiPor" value="parente" style="display: block; float: left; clear: both; margin-bottom: 5px; margin-right: 5px;" />Por Parente<br/>
                                    <div id="buscaInput">
                                        <input style="width: 163px;" type="text" id="input-medium-br" name="inputBusRap"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="text-align: center; margin-top: 10px;" class="buttons">
                            <div style="margin: 0 auto;" class="highlight">
                                <input type="submit" name="submit.highlight" value="Buscar" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>-->
</div>