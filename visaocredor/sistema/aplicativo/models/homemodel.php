<?php
class Homemodel extends Model {

    var $table = ''; // TABELA PRINCIPAL
    
    function  __construct()
    {
        // Call the Model constructor
        parent::Model();
    }
    function getMensagens($cod){
        $query = $this->db->query('SELECT M.men_titulo,M.men_tipo,M.men_texto,M.men_data FROM mensagens M INNER JOIN usuarios U ON(U.usu_cod = M.usuarios_usu_cod) WHERE M.usuarios_usu_cod='.$cod.' ORDER BY M.men_tipo ASC, M.men_data DESC LIMIT 5');
        return $query->result();
    }
    function getTarefas($data,$cod){
        $query = $this->db->query('SELECT T.tar_agendada,U.usu_usuario_sis, T.tar_criacao, T.tar_criador, T.tar_descricao, T.tar_titulo  FROM tarefas T INNER JOIN usuarios U ON (U.usu_cod=T.usuarios_usu_cod) WHERE T.tar_agendada=\''.$data.'\' AND T.usuarios_usu_cod='.$cod.' ORDER BY T.tar_titulo ASC LIMIT 10');
        return $query->result();
    }

}

?>