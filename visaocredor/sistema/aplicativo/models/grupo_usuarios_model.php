<?php
class Grupo_usuarios_model extends Model {
    function  __construct()
    {
        // Call the Model constructor
        parent::Model();
    }

    function getGrupos(){
        $query = $this->db->query('SELECT G.gru_cod,G.gru_titulo FROM grupo_usuarios G ORDER BY G.gru_titulo ASC');
        return $query->result();
    }
    function getGrupoInfo($cod){
        $query = $this->db->query("SELECT * FROM grupo_usuarios G WHERE G.gru_cod=$cod");
        return $query->row();
    }
    

}

?>