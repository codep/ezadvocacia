<?php

class Mensagens_model extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }
    function getSupervisores() {//pega todos os supervisores. OBS: se o credor selecionar diretores no form de mensagem 
        $query = $this->db->query("SELECT U.usu_nome,U.usu_cod,GU.gru_titulo FROM grupo_usuarios GU INNER JOIN usuarios U ON (U.grupo_usuarios_gru_cod=GU.gru_cod) WHERE (GU.gru_cod = '1') OR (GU.gru_cod = '2') OR (GU.gru_titulo = 'Administrador') ORDER BY U.usu_cod ASC");
        return $query->result();
    }

    function setNovaMensagem($dados) {
        /* Recebe os dados da nova mensagem e grava no banco */
        $this->db->insert('mensagens_credores', $dados);
    }
}

?>