<?php

class Importmodel extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getCreCod($docCredor) {
        $query = $this->db->query("SELECT C.cre_cod FROM credores C WHERE C.cre_cpf_cnpj = '$docCredor'");
        return $query->row();
    }

    function getInaCod($docInadimplente) {
    	$docInadimplente = preg_replace('/[^0-9]/', '', $docInadimplente);
        $query = $this->db->query("SELECT I.ina_cod FROM inadimplentes I WHERE REPLACE(REPLACE(REPLACE(I.ina_cpf_cnpj,'.',''),'-',''),'/','') = '$docInadimplente'");
        return $query->row();
    }

    function cadastraDividaTemporaria($tipoDoc, $banco, $agencia, $alinea, $emissao, $qtdeParcelas, $vencInicial, $informacoes, $valorTotal,$creCod) {

        $sync = microtime();

        $data = array(
            'div_documento' => $tipoDoc,
            'div_banco' => $banco,
            'div_agencia' => $agencia,
            'div_alinea' => $alinea,
            'div_emissao' => $emissao,
            'div_cadastro' => date('Y-m-d'),
            'div_qtd_parc' => $qtdeParcelas,
            'div_venc_inicial' => $vencInicial,
            'div_info' => utf8_encode($informacoes),
            'div_total' => $valorTotal,
            'div_cre_cod' => $creCod,
            'div_sync' => $sync
        );

        $this->db->insert('import_dividas', $data);

        $query = $this->db->query("SELECT ID.div_cod FROM import_dividas ID WHERE ID.div_sync = '$sync'");
        return $query->row()->div_cod;
    }

    function cadastraImportacao($inaCod, $duplicidade, $imp_erro, $divida_cod) {

        $data = array(
            'imp_inad_cod' => $inaCod,
            'imp_duplicidade' => $duplicidade,
            'imp_erro' => $imp_erro,
            'imp_divida_imp_cod' => $divida_cod
        );

        $this->db->insert('importacao', $data);
    }

    function cadastraParcela($divida_cod, $parnum, $parvenc, $parvalor) {
        $data = array(
            'impp_div_cod' => $divida_cod,
            'impp_num' => $parnum,
            'impp_venc' => $parvenc,
            'imp_valor' => $parvalor
        );

        $this->db->insert('import_dividas_parcelas', $data);
    }

    function cadInadTemp($tipoPessoa, $sexoPessoa, $estadoCivil, $inaNome, $docInadimplente, $inaRgIe, $inaEnd, $inaBairro, $inaCompl, $inaCep, $inaCidade, $inaEstado, $inaFoneRes, $inaFoneRec, $inaFoneCom, $inaCel1, $inaCel2, $inaCel3, $inaInfo, $inaConjNome, $inaConjFone, $inaConjEnd, $inaConjCidade, $inaConjUF, $inaPaiNome, $inaPaiFone, $inaPaiEnd, $inaPaiCidade, $inaPaiUF, $inaMaeNome, $inaMaeFone, $inaMaeEnd, $inaMaeCidade, $inaMaeUF,$inaCod,$inaDup){

        $data = array(
            'ina_pessoa' => $tipoPessoa,
            'ina_sexo' => $sexoPessoa,
            'ina_estado_civil' => $estadoCivil == ' '?'0':$estadoCivil,
            'ina_nome' => removeCE_Upper($inaNome),
            'ina_cpf_cnpj' => preg_replace('/[^0-9]/', '', $docInadimplente),
            'ina_rg_ie' => $inaRgIe,
            'ina_endereco' => removeCE_Upper($inaEnd),
            'ina_bairro' => removeCE_Upper($inaBairro),
            'ina_complemento' => removeCE_Upper($inaCompl),
            'ina_cep' => str_replace(array('.','-'), '', $inaCep),
            'ina_cidade' => removeCE_Upper($inaCidade),
            'ina_uf' => removeCE_Upper($inaEstado),
            'ina_foneres' => $inaFoneRes,
            'ina_fonerec' => $inaFoneRec,
            'ina_fonecom' => $inaFoneCom,
            'ina_cel1' => $inaCel1,
            'ina_cel2' => $inaCel2,
            'ina_cel3' => $inaCel3,
            'ina_info' => removeCE_Upper($inaInfo),
            'ina_conj_nome' => removeCE_Upper($inaConjNome),
            'ina_conj_fone' => $inaConjFone,
            'ina_conj_endereco' => removeCE_Upper($inaConjEnd),
            'ina_conj_cidade' => removeCE_Upper($inaConjCidade),
            'ina_conj_uf' => removeCE_Upper($inaConjUF),
            'ina_pai_nome' => removeCE_Upper($inaPaiNome),
            'ina_pai_fone' => $inaPaiFone,
            'ina_pai_endereco' => removeCE_Upper($inaPaiEnd),
            'ina_pai_cidade' => removeCE_Upper($inaPaiCidade),
            'ina_pai_uf' => removeCE_Upper($inaPaiUF),
            'ina_mae_nome' => removeCE_Upper($inaMaeNome),
            'ina_mae_fone' => $inaMaeFone,
            'ina_mae_endereco' => removeCE_Upper($inaMaeEnd),
            'ina_mae_cidade' => removeCE_Upper($inaMaeCidade),
            'ina_mae_uf' => removeCE_Upper($inaMaeUF),
            'ina_duplicado' => $inaDup,
            'ina_inad_original_cod' =>$inaCod
        );

        $this->db->insert('import_inadimplentes', $data);
    }
    
    function cadInad($tipoPessoa, $sexoPessoa, $estadoCivil, $inaNome, $docInadimplente, $inaRgIe, $inaEnd, $inaBairro, $inaCompl, $inaCep, $inaCidade, $inaEstado, $inaFoneRes, $inaFoneRec, $inaFoneCom, $inaCel1, $inaCel2, $inaCel3, $inaInfo, $inaConjNome, $inaConjFone, $inaConjEnd, $inaConjCidade, $inaConjUF, $inaPaiNome, $inaPaiFone, $inaPaiEnd, $inaPaiCidade, $inaPaiUF, $inaMaeNome, $inaMaeFone, $inaMaeEnd, $inaMaeCidade, $inaMaeUF){

        $data = array(
            'ina_pessoa' => $tipoPessoa,
            'ina_sexo' => $sexoPessoa,
            'ina_estado_civil' => $estadoCivil == ' '?'0':$estadoCivil,
            'ina_nome' => removeCE_Upper($inaNome),
            'ina_cpf_cnpj' => preg_replace('/[^0-9]/', '', $docInadimplente),
            'ina_rg_ie' => $inaRgIe,
            'ina_endereco' => removeCE_Upper($inaEnd),
            'ina_bairro' => removeCE_Upper($inaBairro),
            'ina_complemento' => removeCE_Upper($inaCompl),
            'ina_cep' => str_replace(array('.','-'), '', $inaCep),
            'ina_cidade' => removeCE_Upper($inaCidade),
            'ina_uf' => removeCE_Upper($inaEstado),
            'ina_foneres' => $inaFoneRes,
            'ina_fonerec' => $inaFoneRec,
            'ina_fonecom' => $inaFoneCom,
            'ina_cel1' => $inaCel1,
            'ina_cel2' => $inaCel2,
            'ina_cel3' => $inaCel3,
            'ina_info' => removeCE_Upper($inaInfo),
            'ina_conj_nome' => removeCE_Upper($inaConjNome),
            'ina_conj_fone' => $inaConjFone,
            'ina_conj_endereco' => removeCE_Upper($inaConjEnd),
            'ina_conj_cidade' => removeCE_Upper($inaConjCidade),
            'ina_conj_uf' => removeCE_Upper($inaConjUF),
            'ina_pai_nome' => removeCE_Upper($inaPaiNome),
            'ina_pai_fone' => $inaPaiFone,
            'ina_pai_endereco' => removeCE_Upper($inaPaiEnd),
            'ina_pai_cidade' => removeCE_Upper($inaPaiCidade),
            'ina_pai_uf' => removeCE_Upper($inaPaiUF),
            'ina_mae_nome' => removeCE_Upper($inaMaeNome),
            'ina_mae_fone' => $inaMaeFone,
            'ina_mae_endereco' => removeCE_Upper($inaMaeEnd),
            'ina_mae_cidade' => removeCE_Upper($inaMaeCidade),
            'ina_mae_uf' => removeCE_Upper($inaMaeUF)
        );

        $this->db->insert('inadimplentes', $data);
    }
    
    function getDividasDados($cods){
        $query = $this->db->query("SELECT INA.ina_nome,ID.*,I.* FROM import_dividas ID INNER JOIN importacao I ON (I.imp_divida_imp_cod = ID.div_cod) INNER JOIN inadimplentes INA ON (INA.ina_cod = I.imp_inad_cod) WHERE ID.div_cod IN ($cods)");
        return $query->result();
    }
	
	function cadInadArray($dadosIna='', $inaCod=0, $dup=0){
    	if (empty($dadosIna)) return false;
		
		$dados = array(
            'ina_pessoa' => $dadosIna['TIPOPESSOA'],
            'ina_sexo' => $dadosIna['SEXO'],
            'ina_estado_civil' => empty($dadosIna['ESTADOCIVIL']) ? 0 : intVal($dadosIna['ESTADOCIVIL']),
            'ina_nome' => removeCE_Upper($dadosIna['NOME']),
            'ina_cpf_cnpj' => preg_replace('/[^0-9]/', '', $dadosIna['CPFCNPJ']),
            'ina_rg_ie' => $dadosIna['RGIE'],
            'ina_endereco' => removeCE_Upper($dadosIna['ENDERECO']),
            'ina_bairro' => removeCE_Upper($dadosIna['BAIRRO']),
            'ina_complemento' => removeCE_Upper($dadosIna['COMPLEMENTO']),
            'ina_cep' => str_replace(array('.','-'), '', $dadosIna['CEP']),
            'ina_cidade' => removeCE_Upper($dadosIna['CIDADE']),
            'ina_uf' => removeCE_Upper($dadosIna['UF']),
            'ina_foneres' => $dadosIna['TELEFONERES'],
            'ina_fonerec' => $dadosIna['TELEFONEREC'],
            'ina_fonecom' => $dadosIna['TELEFONECOM'],
            'ina_cel1' => $dadosIna['CELULAR1'],
            'ina_cel2' => $dadosIna['CELULAR2'],
            'ina_cel3' => $dadosIna['CELULAR3'],
            'ina_email' => $dadosIna['EMAIL'],
            'ina_email_alt' => $dadosIna['EMAILALT'],
            'ina_info' => removeCE_Upper($dadosIna['OBSERVACOES']),
            'ina_conj_nome' => removeCE_Upper($dadosIna['CONJUGENOME']),
            'ina_conj_cpf' => $dadosIna['CONJUGECPF'],
            'ina_conj_fone' => $dadosIna['CONJUGEFONE'],
            'ina_conj_endereco' => removeCE_Upper($dadosIna['CONJUGEENDERECO']),
            'ina_conj_cidade' => removeCE_Upper($dadosIna['CONJUGECIDADE']),
            'ina_conj_uf' => removeCE_Upper($dadosIna['CONJUGEUF']),
            'ina_pai_nome' => removeCE_Upper($dadosIna['PAINOME']),
            'ina_pai_cpf' => removeCE_Upper($dadosIna['PAICPF']),
            'ina_pai_fone' => $dadosIna['PAIFONE'],
            'ina_pai_endereco' => removeCE_Upper($dadosIna['PAIENDERECO']),
            'ina_pai_cidade' => removeCE_Upper($dadosIna['PAICIDADE']),
            'ina_pai_uf' => removeCE_Upper($dadosIna['PAIUF']),
            'ina_mae_nome' => removeCE_Upper($dadosIna['MAENOME']),
            'ina_mae_cpf' => removeCE_Upper($dadosIna['MAECPF']),
            'ina_mae_fone' => $dadosIna['MAEFONE'],
            'ina_mae_endereco' => removeCE_Upper($dadosIna['MAEENDERECO']),
            'ina_mae_cidade' => removeCE_Upper($dadosIna['MAECIDADE']),
            'ina_mae_uf' => removeCE_Upper($dadosIna['MAEUF'])
		);
		
		//SE MAIOR QUE ZERO, ENT�O � PARA CADASTRAR NA TABELA DE IMPORTA��O
		if ($inaCod>0) {
			$dados['ina_duplicado']=$dup;
			$dados['ina_inad_original_cod']=$inaCod;
			$tabela = 'import_inadimplentes';
		} else {
			$tabela = 'inadimplentes';
		}

        return $this->db->insert($tabela, $dados);
	}


	function dupDivArray($dadosDiv='', $creCod=0) {
		
        $where = array(
            'div_documento="'.$dadosDiv['TIPODOC'].'"',
            'div_banco="'.$dadosDiv['BANCO'].'"',
            'div_agencia="'.$dadosDiv['AGENCIA'].'"',
            'div_alinea="'.$dadosDiv['ALINEA'].'"',
            'div_emissao="'.$dadosDiv['DATAEMISSAO'].'"',
            'div_qtd_parc="'.$dadosDiv['NUMPARCELAS'].'"',
            'div_total="'.$dadosDiv['VALORTOTAL'].'"',
            'div_cre_cod="'.$creCod.'"'
        );
        $query = $this->db->query("SELECT ID.div_cod FROM import_dividas ID WHERE ".implode(' AND ', $where));
		if ($query->num_rows() > 0) return $query->row()->div_cod;
		else return 0;
		
	}
    	
    function cadDivArray($dadosDiv='', $creCod=0) {
    	if (empty($dadosDiv) || empty($creCod)) return false;

        $sync = microtime();

        $data = array(
            'div_documento' => $dadosDiv['TIPODOC'],
            'div_banco' => $dadosDiv['BANCO'],
            'div_agencia' => $dadosDiv['AGENCIA'],
            'div_alinea' => $dadosDiv['ALINEA'],
            'div_emissao' => $dadosDiv['DATAEMISSAO'],
            'div_cadastro' => date('Y-m-d'),
            'div_qtd_parc' => $dadosDiv['NUMPARCELAS'],
            'div_venc_inicial' => $dadosDiv['VENCIMENTOINICIAL'],
            'div_info' => utf8_encode($dadosDiv['INFORMACOES']),
            'div_total' => $dadosDiv['VALORTOTAL'],
            'div_cre_cod' => $creCod,
            'div_sync' => $sync
        );

        $this->db->insert('import_dividas', $data);

        $query = $this->db->query("SELECT ID.div_cod FROM import_dividas ID WHERE ID.div_sync = '$sync'");
        return $query->row()->div_cod;
    }
	
    function cadDivParcArray($dadosParc='',$divCod=0) {
        $data = array(
            'impp_div_cod' => $divCod,
            'impp_num' => $dadosParc['NUMPARCELA'],
            'impp_doc_num' => $dadosParc['NUMDOC'],
            'impp_venc' => $dadosParc['VENCIMENTO'],
            'imp_valor' => $dadosParc['VALOR']
        );
        return $this->db->insert('import_dividas_parcelas', $data);
    }

}

?>