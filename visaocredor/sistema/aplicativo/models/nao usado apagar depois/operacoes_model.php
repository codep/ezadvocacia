<?php

class Operacoes_model extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getOperações() {
        $query = $this->db->query('SELECT * FROM operacoes WHERE ope_automatica=0 ORDER BY ope_nome ASC ');
        return $query->result();
    }

}

?>