<?php

class Relatorios_model extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }
    function getUsuarios(){
        //pega todos os usu�rios e seus c�digos, para usar nos relat�rios gerenciais. (filtro recuperador)
        $query = $this->db->query("SELECT u.usu_cod, u.usu_usuario_sis FROM usuarios u WHERE u.usu_ativo = 1 ORDER BY u.usu_usuario_sis ASC");
        return $query->result();
    }

    function getCredores() {
        /* Pega todos os credores e seus c�ds.*/
        $query = $this->db->query("SELECT cre.cre_cod, cre.cre_nome_fantasia FROM credores cre");
        return $query->result();
    }
    
    function getOperacoes(){//pega o c�digo e o nome de todas opera��es da tabela operacoes
        $query = $this->db->query("SELECT op.ope_cod, op.ope_nome FROM operacoes op");
        return $query->result();
        }

    //----------------------------------------------------------------------------------
    /* Relat�rio de Acordos e Previs�es ADM em Atraso do usu�rio logado.
     * Pega todos os inadimplentes que estar� em atraso at� a data que o usu�rio
     * selecionou, a coluna "Parcelas em Atraso" mostra a quantidade de parcelas
     * que o inadimplente tem em atraso at� a data atual.*/
        
    function rel_adm01($codUsuLog, $dataHoje, $filtro) {
        $query = $this->db->query("
            SELECT i.ina_nome, cre.cre_nome_fantasia, ac.aco_tipo,
            (
            /*Nome da opera��o do �ltimo RO. OBS: O nome fica na tabela operacoes*/
              SELECT op.ope_nome
                FROM ros
                  INNER JOIN operacoes op
                    ON(ros.operacoes_ope_cod = op.ope_cod)
                WHERE ros.cobranca_cob_cod = co.cob_cod
                  ORDER BY ros.ros_cod DESC LIMIT 1
            ) AS nome_op_ult_ro,

            (
            /*Data do �ltimo RO*/
              SELECT ros_data
                FROM ros
                  INNER JOIN operacoes op
                    ON(ros.operacoes_ope_cod = op.ope_cod)
              WHERE  ros.cobranca_cob_cod = co.cob_cod
                ORDER BY ros.ros_cod DESC LIMIT 1
            ) AS data_ult_ro,

            (
                /*Hora do �ltimo RO*/
                SELECT ros_hora
                  FROM ros
                    INNER JOIN operacoes op
                      ON(ros.operacoes_ope_cod = op.ope_cod)
                WHERE  ros.cobranca_cob_cod = co.cob_cod
                  ORDER BY ros.ros_cod DESC LIMIT 1
            )AS hora_ult_ro,

            (
             /*Detalhes do �ltimo RO*/
              SELECT ros_detalhe
                FROM ros
                  INNER JOIN operacoes op
                    ON(ros.operacoes_ope_cod = op.ope_cod)
              WHERE ros.cobranca_cob_cod = co.cob_cod
                ORDER BY ros.ros_cod DESC LIMIT 1
            )AS ult_ros_detalhe,
             (
              /*Primeiro vencimento(a parcela mais atrasada)*/
                 SELECT par_ac.paa_vencimento
                   FROM par_acordos par_ac
                     WHERE par_ac.acordos_aco_cod = ac.aco_cod
                       AND par_ac.paa_situacao = 0
                       AND par_ac.paa_vencimento <= '$dataHoje'
                       AND ac.aco_procedencia = 'ADM'
                          ORDER BY par_ac.paa_vencimento ASC LIMIT 1
             ) AS prime_venc,/*primeiro vencimento*/
             (
              /*Quantidade de parcelas atrasadas independente do per�odo selecionado pelo usu�rio. OBS: Parcelas atrasadas at� a data atual*/
                SELECT COUNT(par_ac.paa_cod)
                  FROM par_acordos par_ac
                    WHERE par_ac.acordos_aco_cod = ac.aco_cod
                       AND par_ac.paa_situacao = 0
                       AND par_ac.paa_vencimento <= '$dataHoje'
                       AND ac.aco_procedencia = 'ADM'
             )AS qtd_par_atras,/*Qtd. de parcelas atrasadas at� a data atual*/

            par_ac.paa_parcela AS numDaParcela, par_ac.paa_vencimento, par_ac.paa_cod, par_ac.acordos_aco_cod, ac.aco_cod, ac.aco_procedencia, ac.cobranca_cob_cod, co.cob_cod, i.ina_cod, co.credor_cre_cod, cre.cre_cod
              FROM par_acordos par_ac
                INNER JOIN acordos ac
                 ON(par_ac.acordos_aco_cod = ac.aco_cod AND ac.aco_procedencia = 'ADM')
                INNER JOIN cobrancas co
                 ON(ac.cobranca_cob_cod = co.cob_cod AND (co.cob_remocao NOT IN (1, 3)))
                INNER JOIN inadimplentes i
                 ON(co.inadimplentes_ina_cod = i.ina_cod)
                INNER JOIN credores cre
                 ON(co.credor_cre_cod = cre.cre_cod)
            WHERE  par_ac.paa_situacao = 0
              AND co.usuarios_usu_cod = $codUsuLog /*c�d usu logado*/
              $filtro
              GROUP BY i.ina_nome, cre.cre_nome_fantasia ASC");
        return $query->result();
    }

    function rel_adm02($codUsuLog, $filtro) {
        $query = $this->db->query("
            SELECT i.ina_nome, cre.cre_nome_fantasia, ac.aco_tipo, par_ac.paa_parcela AS numDaParcela, par_ac.paa_vencimento, par_ac.paa_valor, par_ac.paa_cod, par_ac.acordos_aco_cod, ac.aco_cod, ac.aco_procedencia, ac.cobranca_cob_cod, co.cob_cod, i.ina_cod, co.credor_cre_cod, cre.cre_cod
              FROM par_acordos par_ac
                INNER JOIN acordos ac
                  ON(par_ac.acordos_aco_cod = ac.aco_cod AND ac.aco_procedencia = 'ADM')
                INNER JOIN cobrancas co
                  ON(ac.cobranca_cob_cod = co.cob_cod AND (co.cob_remocao NOT IN (1, 3)))
                INNER JOIN inadimplentes i
                  ON(co.inadimplentes_ina_cod = i.ina_cod)
                INNER JOIN credores cre
                  ON(co.credor_cre_cod = cre.cre_cod)
            WHERE  par_ac.paa_situacao = 0
              AND co.usuarios_usu_cod = $codUsuLog /*c�d usu logado*/
              $filtro
              ORDER BY i.ina_nome, par_ac.paa_cod ASC");
        return $query->result();
    }

    function rel_adm03($codUsuLog, $filtro) {
        $query = $this->db->query("
              SELECT co.*, d.*, cre.*, i.*,
                (
              /*�ltimo RO. Opera��o*/
                SELECT operacoes.ope_nome
                  FROM ros
                    INNER JOIN operacoes
                     ON(ros.operacoes_ope_cod = operacoes.ope_cod)
                 WHERE ros.cobranca_cob_cod = co.cob_cod
                     ORDER BY ros.ros_cod DESC LIMIT 1
              )AS ultimo_ro_operacao,

              (
               /*Detalhes do �ltimo RO*/
                 SELECT ros.ros_detalhe
                   FROM ros
                 WHERE ros.cobranca_cob_cod = co.cob_cod
                   ORDER BY ros.ros_cod DESC LIMIT 1
              )AS ultimo_ro_detalhe,

              (
                /*hora do �ltimo RO*/
                SELECT ros.ros_hora
                  FROM ros
                WHERE ros.cobranca_cob_cod = co.cob_cod
                  ORDER BY ros.ros_cod DESC LIMIT 1
              )AS ultimo_ro_hora,

              (
                /*data do �ltimo RO*/
                SELECT ros.ros_data
                  FROM ros
                WHERE ros.cobranca_cob_cod = co.cob_cod
                  ORDER BY ros.ros_cod DESC LIMIT 1
              )AS ultimo_ro_data

            FROM cobrancas co
             INNER JOIN dividas d
             ON(d.cobranca_cob_cod = co.cob_cod AND (co.cob_remocao NOT IN (1, 3)))
             INNER JOIN inadimplentes i
             ON(co.inadimplentes_ina_cod = i.ina_cod)
              INNER JOIN credores cre
              ON(co.credor_cre_cod = cre.cre_cod)
            WHERE d.div_ext = 0
              AND co.usuarios_usu_cod = $codUsuLog /*c�digo do usu�rio logado.*/
              AND co.cob_setor = 'ADM'
              $filtro
              GROUP BY i.ina_nome, cre.cre_nome_fantasia");
        return $query->result();
    }
    
    function getCobrancasOciosas($filtro){
        $query = $this->db->query("SELECT C.cob_cod,ROS.ros_cod,ROS.ros_data,C.credor_cre_cod FROM 
        (SELECT R.* FROM ros R ORDER BY R.ros_data DESC) AS ROS 
        INNER JOIN cobrancas C ON (C.cob_cod = ROS.cobranca_cob_cod AND (C.cob_remocao NOT IN (1, 3))) $filtro GROUP BY C.cob_cod;
        ");
        return $query->result();
    }
    
    function getDividasOciosas($filtro){
        $query = $this->db->query("SELECT C.cob_cod,I.ina_nome,CR.cre_nome_fantasia,D.div_cadastro,(SELECT ros.ros_data FROM ros WHERE ros.cobranca_cob_cod=C.cob_cod ORDER BY ros.ros_data DESC LIMIT 1) AS ultimo_ro FROM cobrancas C 
        RIGHT JOIN dividas D ON(D.cobranca_cob_cod=C.cob_cod AND (C.cob_remocao NOT IN (1, 3)))
        INNER JOIN inadimplentes I ON(I.ina_cod=C.inadimplentes_ina_cod)
        INNER JOIN credores CR ON (CR.cre_cod=C.credor_cre_cod) $filtro;");
        return $query->result();
    }
    
    function getAcordosOciosos($filtro){
        $query = $this->db->query("SELECT C.cob_cod,I.ina_nome,CR.cre_nome_fantasia,A.aco_tipo,
            (SELECT COUNT(par_acordos.paa_cod) FROM par_acordos INNER JOIN acordos ON (acordos.aco_cod=par_acordos.acordos_aco_cod) WHERE acordos.cobranca_cob_cod=C.cob_cod ) AS num_parc, A.aco_emissao,(SELECT ros.ros_data FROM ros WHERE ros.cobranca_cob_cod=C.cob_cod ORDER BY ros.ros_data DESC LIMIT 1) AS ultimo_ro
            FROM cobrancas C 
            RIGHT JOIN acordos A ON (A.cobranca_cob_cod=C.cob_cod AND (C.cob_remocao NOT IN (1, 3)))
            INNER JOIN inadimplentes I ON (I.ina_cod=C.inadimplentes_ina_cod)
            INNER JOIN credores CR ON (C.credor_cre_cod=CR.cre_cod)
            $filtro;");
        return $query->result();
    }
    
    function getDividasVirgensOciosas($filtro){
        $query = $this->db->query("SELECT C.cob_cod,I.ina_nome,CR.cre_nome_fantasia FROM cobrancas C 
        INNER JOIN dividas D ON (D.cobranca_cob_cod=C.cob_cod AND (C.cob_remocao NOT IN (1, 3)))
        INNER JOIN inadimplentes I ON (I.ina_cod=C.inadimplentes_ina_cod)
        INNER JOIN credores CR ON (C.credor_cre_cod=CR.cre_cod)
        $filtro;");
        return $query->result();
    }

//|-------------------------------------------------------------------------------------------------|
//|------------------------------------  RELAT�RIOS JUR�DICOS  -------------------------------------|
//|-------------------------------------------------------------------------------------------------|
    
        function getRelJur01($filtro, $dataAtual){// pega os acordos jur�dicos em atraso
            $query = $this->db->query("
            SELECT co.*, op.*, cre.*, i.*,
             (/* quantidade de parcelas em atraso at� a data de hoje independente da data que o usu�rio escolha*/
               SELECT COUNT(par.paa_situacao)
                FROM par_acordos par
               WHERE (par.acordos_aco_cod = aco.aco_cod)
                AND (aco.cobranca_cob_cod = co.cob_cod)
                AND par.paa_vencimento <= '$dataAtual' -- data atual para ver se est� em atraso (p�go do sistema)
                AND  par.paa_situacao = 0 /*situa�ao 0 � parcela que n�o foi paga*/
             ) AS qtd_par_atras,
            (
            /*Nome da opera��o do �ltimo RO. OBS: O nome fica na tabela operacoes*/
              SELECT op.ope_nome
                FROM ros
                  INNER JOIN operacoes op
                    ON(ros.operacoes_ope_cod = op.ope_cod)
                WHERE ros.cobranca_cob_cod = co.cob_cod
                  ORDER BY ros.ros_cod DESC LIMIT 1
            ) AS nome_op_ult_ro,
            (
            /*Data do �ltimo RO*/
              SELECT ros_data
                FROM ros
                  INNER JOIN operacoes op
                    ON(ros.operacoes_ope_cod = op.ope_cod)
              WHERE  ros.cobranca_cob_cod = co.cob_cod
                ORDER BY ros.ros_cod DESC LIMIT 1
            ) AS data_ult_ro,

            (
                /*Hora do �ltimo RO*/
                SELECT ros_hora
                  FROM ros
                    INNER JOIN operacoes op
                      ON(ros.operacoes_ope_cod = op.ope_cod)
                WHERE  ros.cobranca_cob_cod = co.cob_cod
                  ORDER BY ros.ros_cod DESC LIMIT 1
            )AS hora_ult_ro,

            (
             /*Detalhes do �ltimo RO*/
              SELECT ros_detalhe
                FROM ros
                  INNER JOIN operacoes op
                    ON(ros.operacoes_ope_cod = op.ope_cod)
              WHERE ros.cobranca_cob_cod = co.cob_cod
                ORDER BY ros.ros_cod DESC LIMIT 1
            )AS ult_ros_detalhe,
            (
              /*Primeiro vencimento(a parcela mais atrasada)*/
                 SELECT par.paa_vencimento
                   FROM par_acordos par
                     WHERE par.acordos_aco_cod = aco.aco_cod
                       AND par.paa_situacao = 0 /*situa�ao 0 � parcela que n�o foi paga*/
                       AND par.paa_vencimento <= '$dataAtual' /*DATA DE HOJE � DIN�MICO*/
                       AND co.cob_setor = 'JUD'
                          ORDER BY par.paa_vencimento ASC LIMIT 1
             ) AS prime_venc,/*primeiro vencimento*/

            (
            /*Verificando se tem o RO multa, se tiver retorna 48 se n�o tiver retorna null*/
            SELECT ros.operacoes_ope_cod
              FROM ros
               WHERE ros.cobranca_cob_cod = co.cob_cod
                AND ros.operacoes_ope_cod = 48 GROUP BY ros.operacoes_ope_cod
            )AS multa

            FROM cobrancas co
              INNER JOIN acordos aco
              ON((aco.cobranca_cob_cod = co.cob_cod) AND co.cob_setor = 'JUD' AND (co.cob_remocao NOT IN (1, 3)))
               INNER JOIN par_acordos par
               ON(par.acordos_aco_cod = aco.aco_cod)
                INNER JOIN ros
                ON(ros.cobranca_cob_cod = co.cob_cod)
                 INNER JOIN operacoes op
                 ON(op.ope_cod = ros.operacoes_ope_cod)
                  INNER JOIN credores cre
                  ON(co.credor_cre_cod = cre.cre_cod)
                   INNER JOIN inadimplentes i
                   ON(co.inadimplentes_ina_cod = i.ina_cod)
             $filtro
        ");
            return $query->result();
            
        }
    //Pega os acordos jur�dicos a vencer na data selecionada de todos os credores ou do credor especificado
    function getRelJur02($filtroCredor, $filtro){
            $query = $this->db->query("
                SELECT cre.cre_nome_fantasia, i.ina_nome, par_ac.paa_parcela, par_ac.paa_vencimento, par_ac.paa_valor
                  FROM cobrancas co
                    INNER JOIN acordos aco
                    ON((co.cob_cod = aco.cobranca_cob_cod) AND co.cob_setor = 'JUD' AND (co.cob_remocao NOT IN (1, 3))) -- pega s� as cobrancas jur�dicas
                     INNER JOIN par_acordos par_ac
                     ON((aco.aco_cod = par_ac.acordos_aco_cod) AND par_ac.paa_situacao = 0) -- situa��o 0 � NAO PAGO
                      INNER JOIN inadimplentes i
                      ON(co.inadimplentes_ina_cod = i.ina_cod)
                       INNER JOIN credores cre
                       ON((co.credor_cre_cod = cre.cre_cod) $filtroCredor) -- acordos do credor especifico ou de todos os credores
                $filtro
                ");
            return $query->result();
        }

        function getRelJur03($filtro, $creCodigo){//pega as cobrancas que N�O receberam movimenta��o de RO no per�odo selecionado
            //PASSO 1: Pega todas as cobran�as que TIVERAM RO no per�odo selecionado
            $query = $this->db->query
                    ("
                        SELECT ros.cobranca_cob_cod
                          FROM ros
                            INNER JOIN cobrancas co
                            ON(ros.cobranca_cob_cod = co.cob_cod AND co.cob_setor = 'JUD' AND (co.cob_remocao NOT IN (1, 3))) $filtro GROUP BY ros.cobranca_cob_cod
                    ");
            $resultPrimeiraPesquisa = $query->result();
            $filtro = "";

            //PASSO 2: Verificando se o passo anterior retornou algum valor.
            
            //----------- VARI�VEIS USADAS -----------
            $tamanhoArray = sizeof($resultPrimeiraPesquisa);//Tamanho do array que recebe o resultado da pesquisa do PASSO 1
            $i = 0;//vari�vel usada no do while (s� para incremento).
            $cobsComRo="";//s�o as cobran�as que tiveram movimenta��o de RO no per�odo selecionado. vari�vel usada no do while
            //----------- FIM DECLARA��O DE VARI�VEIS -----------

            if($tamanhoArray == 0 && $creCodigo == 0)
                {
                //die("cobran�as jur�dicas sem RO no per�odo selecionado de todos os credores");
                $filtro = "WHERE co.cob_setor = 'JUD'";
                }
                else if($tamanhoArray == 0 && $creCodigo != 0)
                    {
                    //die("cobran�as jur�dicas sem RO no per�odo selecionado do credor especificado");
                    $filtro = "WHERE co.cob_setor = 'JUD' AND cre.cre_cod = $creCodigo";
                    }

            //CASO A CONSULTA RETORNE ALGUM VALOR
            /*pegando os c�digos das cobran�as que tiveram movimenta��o de RO no per�odo selecionado para colocar na
             *cl�usula NOT IN() para pegar as cobran�as que sobraram (n�o tiveram RO no per�odo selecionado).*/
        if($tamanhoArray != 0)
            {
            do//quando terminar o do while, a vari�vel $cobsComRo fica nesse padr�o. (EX: $cobsComRo = 3, 8, 76) onde os n�meros s�o os c�digos das cobran�as com RO no per�odo selecionado
            {
            $cobsComRo .= $resultPrimeiraPesquisa[$i]->cobranca_cob_cod;
            if($i < ($tamanhoArray - 1))
                {
                 $cobsComRo .= ", ";
                }
            $i++;
            }while($i < $tamanhoArray);

            }
//-----------------------------------------------------------------------------
            //MONTANDO O FILTRO PARA PEGAR AS COBRAN�AS QUE N�O TIVERAM RO NO PER�ODO SELECIONADO
            if($tamanhoArray != 0 && $creCodigo == 0)
                {
                //die("encontrou combran�as jur�dicas com RO no per�odo selecionado de qualquer credor, essas n�o ser�o selecionadas. (v�o entrar na cl�usula NOT IN)");
                $filtro = "WHERE co.cob_setor = 'JUD' AND co.cob_cod NOT IN ($cobsComRo)";
                }
                else if($tamanhoArray != 0 && $creCodigo != 0)
                    {
                    //die("encontrou cobran�as jur�dicas  com RO no per�odo selecionado do credor selecionado, essas n�o ser�o selecionadas. (v�o entrar na cl�usula NOT IN)");
                    $filtro = "WHERE co.cob_setor = 'JUD' AND co.cob_cod NOT IN ($cobsComRo) AND cre.cre_cod = $creCodigo";
                    }

           //------------------- PEGANDO AS COBRAN�AS QUE N�O TIVERAM RO NO PER�ODO SELECIONADO -------------------//
            $query = $this->db->query
                    ("
                        SELECT  i.ina_nome, cre.cre_nome_fantasia,
                         (
                         CASE co.cob_status
                         WHEN 0 THEN 'ADMINISTRATIVO'
                         WHEN 1 THEN 'SOLICITANDO DOC.'
                         WHEN 2 THEN 'AGUARDANDO'
                         WHEN 3 THEN 'AJUIZADO'
                         WHEN 4 THEN 'ANDAMENTO'
                         WHEN 99 THEN 'RECEM ENVIADA PARA O JUDICIAL'
                         END
                         )
                         AS tipo,
                        (
                         /*Detalhes do �ltimo RO*/
                          SELECT ros_detalhe
                            FROM ros
                              INNER JOIN operacoes op
                                ON(ros.operacoes_ope_cod = op.ope_cod)
                          WHERE ros.cobranca_cob_cod = co.cob_cod
                            ORDER BY ros.ros_cod DESC LIMIT 1
                        )AS ult_ros_detalhe,

                        (
                        /*Data do �ltimo RO*/
                          SELECT ros_data
                            FROM ros
                              INNER JOIN operacoes op
                                ON(ros.operacoes_ope_cod = op.ope_cod)
                          WHERE  ros.cobranca_cob_cod = co.cob_cod
                            ORDER BY ros.ros_cod DESC LIMIT 1
                        ) AS data_ult_ro,

                        (
                            /*Hora do �ltimo RO*/
                            SELECT ros_hora
                              FROM ros
                                INNER JOIN operacoes op
                                  ON(ros.operacoes_ope_cod = op.ope_cod)
                            WHERE ros.cobranca_cob_cod = co.cob_cod
                              ORDER BY ros.ros_cod DESC LIMIT 1
                        )AS hora_ult_ro

                        FROM cobrancas co
                        LEFT JOIN ros
                        ON(co.cob_cod = ros.cobranca_cob_cod )
                        INNER JOIN credores cre
                        ON(co.credor_cre_cod = cre.cre_cod)
                        INNER JOIN inadimplentes i
                        ON(co.inadimplentes_ina_cod = i.ina_cod)
                        $filtro GROUP BY co.cob_cod ORDER BY tipo, data_ult_ro
                    ");
            return $query->result();
        }
        


    //|--------------------------------------|
    //|---------- RELAT�RIO POR RO ----------|
    //|--------------------------------------|

    function getRelRo01($filtro, $codTipoOperacao){
        $query = $this->db->query("
            SELECT i.ina_nome, cre.cre_nome_fantasia, ro.ros_detalhe,ro.ros_data, ro.ros_agendado, op.ope_nome
              FROM cobrancas co
                INNER JOIN ros ro
                ON(co.cob_cod = ro.cobranca_cob_cod AND (co.cob_remocao NOT IN (1, 3)))
                 INNER JOIN inadimplentes i
                 ON(co.inadimplentes_ina_cod = i.ina_cod)
                  INNER JOIN credores cre
                  ON(co.credor_cre_cod = cre.cre_cod)
                   INNER JOIN operacoes op
                   ON((ro.operacoes_ope_cod = op.ope_cod) AND ro.operacoes_ope_cod = $codTipoOperacao)
                $filtro
                ");
        return $query->result();
    }
    //|--------------------------------------------------------------|
    //|------------------- RELAT�RIOS GERENCIAIS -------------------|
    //|--------------------------------------------------------------|
    function getRelGer01($filtro, $dataDe, $dataAte){
        $query = $this->db->query
                ("
                    SELECT i.ina_nome, cre.cre_nome_fantasia,
                    (
                     /*Data do �ltimo RO*/
                      SELECT ros_data
                        FROM ros
                          INNER JOIN operacoes op
                            ON(ros.operacoes_ope_cod = op.ope_cod)
                      WHERE  ros.cobranca_cob_cod = co.cob_cod
                        ORDER BY ros.ros_cod DESC LIMIT 1
                    ) AS data_ult_ro,
                    (
                        /*Hora do �ltimo RO*/
                        SELECT ros_hora
                          FROM ros
                            INNER JOIN operacoes op
                              ON(ros.operacoes_ope_cod = op.ope_cod)
                        WHERE  ros.cobranca_cob_cod = co.cob_cod
                          ORDER BY ros.ros_cod DESC LIMIT 1
                    )AS hora_ult_ro,
                    (
                    /*Nome da opera��o do �ltimo RO. OBS: O nome fica na tabela operacoes*/
                      SELECT op.ope_nome
                        FROM ros
                          INNER JOIN operacoes op
                            ON(ros.operacoes_ope_cod = op.ope_cod)
                        WHERE ros.cobranca_cob_cod = co.cob_cod
                          ORDER BY ros.ros_cod DESC LIMIT 1
                    ) AS nome_op_ult_ro,
                    (
                     /*Detalhes do �ltimo RO*/
                      SELECT ros_detalhe
                        FROM ros
                          INNER JOIN operacoes op
                            ON(ros.operacoes_ope_cod = op.ope_cod)
                      WHERE ros.cobranca_cob_cod = co.cob_cod
                        ORDER BY ros.ros_cod DESC LIMIT 1
                    )AS ult_ros_detalhe,
                    (
                     /*Parcela mais atrasada do per�odo selecionado*/
                     SELECT p_ac.paa_vencimento
                      FROM par_acordos p_ac
                       INNER JOIN acordos ac
                       ON(p_ac.acordos_aco_cod = ac.aco_cod)
                     WHERE co.cob_cod = ac.cobranca_cob_cod AND (p_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte')
                     ORDER BY p_ac.paa_vencimento ASC LIMIT 1
                    )AS parc_mais_atra /*parcela mais atrasada*/

                    FROM cobrancas co
                    INNER JOIN acordos ac
                    ON(ac.cobranca_cob_cod = co.cob_cod AND (co.cob_remocao NOT IN (1, 3)))
                    INNER JOIN inadimplentes i
                    ON(i.ina_cod = co.inadimplentes_ina_cod)
                    INNER JOIN credores cre
                    ON(cre.cre_cod = co.credor_cre_cod)
                    /*------ pega os acordos vencidos no per�odo selecionado ------*/
                    INNER JOIN par_acordos p_ac
                    ON(ac.aco_cod = p_ac.acordos_aco_cod AND (p_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte') AND p_ac.paa_situacao = 0)
                    /*---------------------------------------------------------------*/
                    $filtro GROUP BY co.cob_cod
                 ");
        return $query->result();
    }
    function getRelGer02($filtro, $dataDe, $dataAte){
        /*OBS: Na tabela acordos tem status para ver se � acordo ou previs�o*/
        $query = $this->db->query
                ("
                    SELECT i.ina_nome, cre.cre_nome_fantasia, p_ac.paa_parcela, p_ac.paa_vencimento, p_ac.paa_valor
                    FROM cobrancas co
                    INNER JOIN acordos ac
                    ON(co.cob_cod = ac.cobranca_cob_cod AND (co.cob_remocao NOT IN (1, 3)))
                    INNER JOIN par_acordos p_ac
                    ON(ac.aco_cod = p_ac.acordos_aco_cod AND (p_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte') AND p_ac.paa_situacao = 0)
                    INNER JOIN inadimplentes i
                    ON(co.inadimplentes_ina_cod = i.ina_cod)
                    INNER JOIN credores cre
                    ON(co.credor_cre_cod = cre.cre_cod)
                    $filtro ORDER BY i.ina_nome, p_ac.paa_vencimento
                ");
        return $query->result();
    }
    function getRelGer03($filtro, $dataDe, $dataAte){
/*------------------------- PASSO 1: PEGA AS COBRAN�AS SEM ACORDOS (D�VIDAS) NO PER�ODO SELECIONADO -------------------------*/
        $query = $this->db->query
                ("
                    SELECT co.cob_cod, i.ina_nome, cre.cre_nome_fantasia, d.div_cadastro,
                     (
                     /*Detalhes do �ltimo RO*/
                      SELECT ros_detalhe
                      FROM ros
                      INNER JOIN operacoes op
                      ON(ros.operacoes_ope_cod = op.ope_cod)
                      WHERE ros.cobranca_cob_cod = co.cob_cod
                      ORDER BY ros.ros_cod DESC LIMIT 1
                     )AS ult_ros_detalhe

                    FROM cobrancas co
                    /*------ Pega as d�vidas n�o extinta que tem a data de cadastro entre o per�odo selecionado ------*/
                    INNER JOIN dividas d
                    ON(co.cob_cod = d.cobranca_cob_cod AND (d.div_ext = 0) AND (co.cob_remocao NOT IN (1, 3)) AND (d.div_cadastro BETWEEN '$dataDe' AND '$dataAte'))
                    /*--------------------------------------------------*/
                    INNER JOIN inadimplentes i
                    ON(co.inadimplentes_ina_cod = i.ina_cod)
                    INNER JOIN credores cre
                    ON(co.credor_cre_cod = cre.cre_cod)
                    $filtro
                ");
        /*------------------ FIM DO PASSO 1 ------------------*/

        $cobsSemAcord = $query->result(); //RESULTADO DA PESQUISA DO PASSO 1. cobsSemAcord -> cobran�as sem acordo no per�odo selecionado.

        if(sizeof($cobsSemAcord) == 0)//se n�o encontrar nada na primeira pesquisa retorne 0
            {
            return 0;
            }

        $cobCod = 0;// vari�vel que armazena o c�digo da cobran�a, para pegar os 10 �ltimos ROS e usar esse mesmo c�digo como n�mero de �ndice no array com os 10 �ltimos ROS
/*-------------- PASSO 2: PEGA OS 10 �LTIMOS ROS DE CADA COBRAN�A ENCONTRADA NO PASSO 1 --------------*/
        foreach ($cobsSemAcord as $cobSemAc){
            $cobCod = $cobSemAc->cob_cod;//c�digo de cada cobran�a encontrada no passo 1
            $query = $this->db->query
                    ("
                        SELECT ros.ros_cod, ros.cobranca_cob_cod, ros.ros_data, ros.ros_hora, op.ope_nome
                        FROM ros
                        INNER JOIN operacoes op
                        ON(ros.operacoes_ope_cod = op.ope_cod)
                        WHERE ros.cobranca_cob_cod = $cobCod ORDER BY ros.ros_cod DESC LIMIT 10
                    ");
            $dezUltimosRo[$cobCod] = $query->result();//Array com os 10 �ltimos ROS de cada cobran�a
        }
/*-------------------------------- FIM DO PASSO 2 --------------------------------*/
        return $cobrancasERos = array ($cobsSemAcord, $dezUltimosRo);//Retornando um array com as cobran�as e seus 10 �ltimos ROS. cobrancasERos -> Cobran�as e os 10 �ltimos ROS
    }


    function getRelGer04($filtro, $dataDe, $dataAte, $trabalhada){

/*Pega todas as d�vidas virgens, d�vidas trabalhadas, acordos e os 10 �ltimos ROs de todas. Pesquisa pela data de cadastro das d�vidas e acordos. */

/*--------------- TODAS AS D�VIDAS N�O TRABALHADAS, CADASTRADA NO PER�ODO SELECIONADO ---------------*/
        if($trabalhada == "")
            {
            $query = $this->db->query
                ("
                    SELECT i.ina_nome, cre.cre_nome_fantasia, d.div_cadastro AS data_cadastro,
                    (
                     CASE d.div_virgem
                     WHEN 1 THEN 'VIRGEM'
                     END
                    )AS ult_ros_detalhe

                    FROM dividas d
                    INNER JOIN cobrancas co
                    ON(co.cob_cod = d.cobranca_cob_cod AND (d.div_cadastro BETWEEN '$dataDe' AND '$dataAte') AND ((d.div_virgem = 1)) AND co.cob_remocao NOT IN (1, 3))
                    INNER JOIN inadimplentes i
                    ON(co.inadimplentes_ina_cod = i.ina_cod)
                    INNER JOIN credores cre
                    ON(co.credor_cre_cod = cre.cre_cod)
                    $filtro ORDER BY d.div_cadastro
                ");
            $dividasVirgens = $query->result();
            }
            else
                {
                $dividasVirgens = array();
                }
/*-------------------- FIM DE TODAS AS D�VIDAS N�O TRABALHADAS --------------------*/

/*#################### TODAS AS D�VIDAS TRABALHADAS, CADASTRADA NO PER�ODO SELECIONADO ####################*/
        $query = $this->db->query
                ("
                    SELECT d.cobranca_cob_cod, i.ina_nome, cre.cre_nome_fantasia, d.div_cadastro AS data_cadastro,
                    (
                     /*Detalhes do �ltimo RO*/
                     SELECT ros_detalhe
                     FROM ros
                     INNER JOIN operacoes op
                      ON(ros.operacoes_ope_cod = op.ope_cod)
                     WHERE ros.cobranca_cob_cod = co.cob_cod
                     ORDER BY ros.ros_cod DESC LIMIT 1
                    )AS ult_ros_detalhe

                    FROM dividas d
                    INNER JOIN cobrancas co
                    ON(co.cob_cod = d.cobranca_cob_cod AND (d.div_cadastro BETWEEN '$dataDe' AND '$dataAte') AND (d.div_virgem = 0) AND co.cob_remocao NOT IN (1, 3))
                    INNER JOIN ros
                    ON(ros.cobranca_cob_cod = d.cobranca_cob_cod)
                    INNER JOIN inadimplentes i
                    ON(i.ina_cod = co.inadimplentes_ina_cod)
                    INNER JOIN credores cre
                    ON(cre.cre_cod = co.credor_cre_cod)
                    $filtro GROUP BY ros.cobranca_cob_cod ORDER BY d.div_cadastro
                ");
        $dividasTrabalhadas = $query->result();

/*#################### FIM DAS D�VIDAS TRABALHADAS, CADASTRADA NO PER�ODO SELECIONADO ####################*/

/****************** TODOS OS ACORDOS COM DATA DE CADASTRO NO PER�ODO SELECIONADO *******************/
        $query = $this->db->query
                ("
                    SELECT ac.cobranca_cob_cod, i.ina_nome, cre.cre_nome_fantasia, ac.aco_emissao AS data_cadastro,
                    (
                     /*Detalhes do �ltimo RO*/
                     SELECT ros_detalhe
                     FROM ros
                     INNER JOIN operacoes op
                      ON(ros.operacoes_ope_cod = op.ope_cod)
                     WHERE ros.cobranca_cob_cod = co.cob_cod
                     ORDER BY ros.ros_cod DESC LIMIT 1
                    )AS ult_ros_detalhe

                    FROM acordos ac
                    INNER JOIN cobrancas co
                    ON(co.cob_cod = ac.cobranca_cob_cod AND (ac.aco_emissao BETWEEN '$dataDe' AND '$dataAte') AND (co.cob_remocao NOT IN (1, 3)))
                    INNER JOIN ros
                    ON(ros.cobranca_cob_cod = ac.cobranca_cob_cod)
                    INNER JOIN inadimplentes i
                    ON(i.ina_cod = co.inadimplentes_ina_cod)
                    INNER JOIN credores cre
                    ON(cre.cre_cod = ac.cobranca_cob_cod)
                    $filtro GROUP BY ac.cobranca_cob_cod ORDER BY ac.aco_emissao
                ");
        $todosAcordos = $query->result();

/****************** FIM DOS ACORDOS COM A DATA DE CADASTRO NO PER�ODO SELECIONADO ******************/

        $codCob = 0;//c�digo da cobran�a
        $dezUltmosRos = array();//Os 10 �ltimos ROs de cada d�vida trabalhada e de cada acordo.

        /*PEGANDO OS 10 �LTIMOS ROs DE CADA D�VIDA E DE CADA ACORDO*/
        if(sizeof($dividasTrabalhadas) != 0)
            {
            foreach($dividasTrabalhadas as $divTrabalhada)
                {
                $codCob = $divTrabalhada->cobranca_cob_cod;
                $query = $this->db->query
                        ("
                            SELECT ros.ros_data, ros.ros_hora, op.ope_nome
                            FROM ros
                            INNER JOIN operacoes op
                            ON(ros.operacoes_ope_cod = op.ope_cod)
                            WHERE ros.cobranca_cob_cod = $codCob ORDER BY ros.ros_cod DESC LIMIT 10
                        ");
                $dezUltmosRos[$codCob] = $query->result();
                }
            }

        if(sizeof($todosAcordos) != 0)
            {
            /*PEGANDO OS 10 �LTIMOS ROs DE CADA ACORDO*/
            foreach ($todosAcordos as $todAcord)
                {
                $codCob = $todAcord->cobranca_cob_cod;
               $query = $this->db->query
                        ("
                            SELECT ros.ros_data, ros.ros_hora, op.ope_nome
                            FROM ros
                            INNER JOIN operacoes op
                            ON(ros.operacoes_ope_cod = op.ope_cod)
                            WHERE ros.cobranca_cob_cod = $codCob ORDER BY ros.ros_cod DESC LIMIT 10
                        ");
                $dezUltmosRos[$codCob] = $query->result();
                }
            
            }

        return $cobrancasERos = array
            (
            "dividasVirgens"=> $dividasVirgens,
            "dividasTrabalhadas"=> $dividasTrabalhadas,
            "todosAcordos"=> $todosAcordos,
            "dezUltimosRos"=> $dezUltmosRos/*das d�vidas trabalhadas e dos acordos*/
            );
    }

    
    function getRelGer05($filtro, $codCredor, $codRecuperador){

/*PASSO 1: PEGA AS COBRAN�AS QUE RECEBERAM MOVIMENTA��O DE RO NO PER�ODO SELECIONADO. (O resultado � colocado na cl�usula NOT IN do pr�ximo passo para pegar as cobran�as que n�o receberam movimenta��o de RO)*/
        $query = $this->db->query("
            SELECT ros.cobranca_cob_cod
            FROM ros
            INNER JOIN cobrancas co
            ON(ros.cobranca_cob_cod = co.cob_cod AND (co.cob_remocao NOT IN(1, 3)))
            $filtro GROUP BY ros.cobranca_cob_cod;
            ");
        
        $resultPrimeiraPesquisa = $query->result(); //cobsComRo = cobran�as com ro
        $tamanhoArray = sizeof($resultPrimeiraPesquisa);//Tamanho do array que recebe o resultado da pesquisa do PASSO 1
        $i = 0;
        $cobsComRo = "";
        
/*----------------- VERIFICANDO SE A CONSULTA DO PASSO 1 ENCONTROU ALGO PARA MONTAR O FILTRO DO PASSO 2 -----------------*/
        if(sizeof($resultPrimeiraPesquisa) == 0 && $codCredor == "" && $codRecuperador == "")
            {
            //die("Filtro 1: N�o encontrou nada. Procurando por cobran�as de todos os credores e todos os recuperadores");
            $filtro = "";
            }
            else if(sizeof($resultPrimeiraPesquisa) == 0 && $codCredor == "" && $codRecuperador != "")
                {
                //die("Filtro 2: N�o encontrou nada. Procurando por cobran�as de todos os credores do recuperador selecionado");
                $filtro = "WHERE cre.usuarios_responsavel_cod = $codRecuperador";
                }
                else if(sizeof($resultPrimeiraPesquisa) == 0 && $codCredor != "" && $codRecuperador == "")
                    {
                    //die("Filtro 3: N�o encontrou nada. Procurando por cobran�as do credor selecionado");
                    $filtro = "WHERE cre.cre_cod = $codCredor";
                    }
                    else//SE O PASSO 1 RETORNAR ALGUM RESULTADO
                        {
                        do//quando terminar o do while, a vari�vel $cobsComRo fica nesse padr�o. (EX: $cobsComRo = 3, 8, 76) onde os n�meros s�o os c�digos das cobran�as com RO no per�odo selecionado
                            {
                            $cobsComRo .= $resultPrimeiraPesquisa[$i]->cobranca_cob_cod;
                            if($i < ($tamanhoArray - 1))
                                {
                                 $cobsComRo .= ", ";
                                }
                            $i++;
                            }while($i < $tamanhoArray);
                        /*----------- MONTANDO O FILTRO PARA A SEGUNDA PESQUISA. QUANDO A CONSULTA DO PASSO 1 RETORNAR ALGUM VALOR -----------*/
                        if($codCredor == "" && $codRecuperador == "")
                            {
                            //die("Filtro 1: Encontrou. Procurando por cobran�as de todos os credores e todos os recuperadores");
                            $filtro = "WHERE co.cob_cod NOT IN ($cobsComRo)"; 
                            }
                            else if($codCredor == "" && $codRecuperador != "")
                                {
                                //die("Filtro 2: Encontrou. Procurando por cobran�as de todos os credores do recuperador selecionado");
                                $filtro = "WHERE co.cob_cod NOT IN ($cobsComRo) AND cre.usuarios_responsavel_cod = $codRecuperador";
                                }
                                else
                                    {
                                    //die("Filtro 3: Encontrou. Procurando por cobran�as do credor selecionado");
                                    $filtro = "WHERE co.cob_cod NOT IN ($cobsComRo) AND cre.cre_cod = $codCredor";
                                    }
                        }
/*---------------------------------------- FIM DA VERIFICA��O ----------------------------------------*/
/*---------------------------PASSO 2: PEGANDO AS COBRAN�AS QUE N�O RECEBERAM MOVIMENTA��O DE RO NO PER�ODO SELECIONADO ---------------------------*/

        $query = $this->db->query
                ("
                    SELECT co.cob_cod, i.ina_nome, cre.cre_nome_fantasia, d.div_cadastro, aco.aco_emissao,
                    (
                     /*Detalhes do �ltimo RO*/
                     SELECT ros_detalhe
                     FROM ros
                       INNER JOIN operacoes op
                         ON(ros.operacoes_ope_cod = op.ope_cod)
                     WHERE ros.cobranca_cob_cod = co.cob_cod
                     ORDER BY ros.ros_cod DESC LIMIT 1
                    )AS ult_ros_detalhe
                    FROM cobrancas co
                    INNER JOIN credores cre
                    ON((co.credor_cre_cod = cre.cre_cod) AND (co.cob_remocao NOT IN (1, 3)))
                    INNER JOIN inadimplentes i
                    ON(co.inadimplentes_ina_cod = i.ina_cod)
                    LEFT JOIN dividas d
                    ON(co.cob_cod = d.cobranca_cob_cod)
                    LEFT JOIN acordos aco
                    ON(co.cob_cod = aco.cobranca_cob_cod)
                    $filtro ORDER BY i.ina_nome;
                ");
        $resultadoSegundaPesquisa = $query->result();
/*---------------------- FIM DO PASSO 2 ----------------------*/
        
        if(sizeof($resultadoSegundaPesquisa) == 0)//se a segunda pesquisa n�o encontrar nada
            {
            return 0;
            }
/*------------------------ PEGA AS 10 �LTIMOS ROs DE CADA COBRAN�A ENCONTRADA NA SEGUNDA PESQUISA ------------------------*/

        $cobCod = 0;// vari�vel que armazena o c�digo da cobran�a, para pegar os 10 �ltimos ROS e usar esse mesmo c�digo como n�mero de �ndice no array com os 10 �ltimos ROS

        foreach ($resultadoSegundaPesquisa as $resSegPesq)
            {
            $cobCod = $resSegPesq->cob_cod;//c�digo de cada cobran�a
            $query = $this->db->query
                    ("
                        SELECT ros.ros_data, ros.ros_hora, op.ope_nome
                        FROM ros
                        INNER JOIN operacoes op
                        ON(ros.operacoes_ope_cod = op.ope_cod)
                        WHERE ros.cobranca_cob_cod = $cobCod ORDER BY ros.ros_cod DESC LIMIT 10
                    ");
            $dezUltimosRo[$cobCod] = $query->result();//Array com os 10 �ltimos ROS de cada cobran�a
            }

            return $cobrancasERos = array ($resultadoSegundaPesquisa, $dezUltimosRo);//Retornando um array com as cobran�as e seus 10 �ltimos ROS. cobrancasERos -> Cobran�as e os 10 �ltimos ROS
    }

    function getRelGer06($filtro, $dataDe, $dataAte){//Pega os recebimentos de acordo com o filtro escolhido

/*############################### PASSO 1: PEGA OS RECEBIMENTOS ###############################*/
        $query = $this->db->query
                ("
                    SELECT rece.reb_cod, rece.reb_parcelas, i.ina_nome, cre.cre_nome_fantasia, rece.reb_data, u.usu_usuario_sis, rece.reb_baixador, rece.reb_valor
                    FROM recebimentos rece
                    INNER JOIN inadimplentes i
                    ON(i.ina_cod = rece.inadimplentes_ina_cod)
                    INNER JOIN credores cre
                    ON(cre.cre_cod = rece.credor_cre_cod)
                    INNER JOIN acordos aco
                    ON(aco.aco_cod = rece.acordos_aco_cod)
                    INNER JOIN usuarios u
                    ON(u.usu_cod = rece.usuarios_usu_cod)
                    WHERE (rece.reb_data BETWEEN '$dataDe' AND '$dataAte') $filtro ORDER BY cre.cre_nome_fantasia, rece.reb_cod
                ");
        $recebimentos = $query->result();
/*############################### FIM DO PASSO 1 ###############################*/

        if(sizeof($recebimentos) == 0)
            {
            //Se n�o encontrar nenhum recebimento retorna 0
            return 0;
            }


        $codRecebmento = 0;//c�digo do recebimento
        $codParcRecebidas = 0; //c�digos das parcelas recebidas

/* ************************** PASSO 2: PEGA AS PARCELAS RECEBIDAS ************************** */
        foreach ($recebimentos as $receb)
            {
            $codRecebmento = $receb->reb_cod;//c�digos dos recebimentos
            $codParcRecebidas = str_replace('-',',',$receb->reb_parcelas); //c�digos das parcelas recebidas
            if($codParcRecebidas == 0){continue;}//est� linha est� aqui poque tinha recebimento com reb_parcelas null coloquei essa linha s� para n�o dar pau no teste, depois do sistema pronto pode apagar essa linha, pois n�o vai existir recebimento com parcela null.
            $query = $this->db->query
                    ("
                        SELECT  pac.acordos_aco_cod,
                          (
                           CASE pac.paa_saldo
                            WHEN 0.00 THEN CONCAT((SELECT pac.paa_parcela LIMIT 1),'(P)')
                            ELSE CONCAT((SELECT pac.paa_parcela LIMIT 1),'(I)')
                            END
                          ) AS recebimento
                        FROM par_acordos pac
                        INNER JOIN acordos aco
                        ON(aco.aco_cod = pac.acordos_aco_cod)
                        INNER JOIN cobrancas co
                        ON(co.cob_cod = aco.cobranca_cob_cod)
                        INNER JOIN credores cre
                        ON(cre.cre_cod = co.credor_cre_cod)
                        WHERE pac.paa_cod IN ($codParcRecebidas)
                        
                    ");
            $parcRecebidas[$codRecebmento] = $query->result();//parcelas recebidas
            }
/* ************************************ FIM DO  PASSO 2 ************************************ */

            return $recebimentos_Parcelas = array("recebimentos" => $recebimentos, "parcelasRecebidas" => $parcRecebidas);

    }

    function getRelGer07($filtro, $dataDe, $dataAte){//mostra que parte do sistema o credor acessou

        $query = $this->db->query
                ("
                    SELECT cre.cre_nome_fantasia, i.ina_nome, usu.usu_usuario_sis,
                     (
                      SELECT SUBSTRING_INDEX(ace.ace_data_hora, ' ', 1)/*pega tudo at� encontrar o primeiro espa�o*/
                     )AS _data,
                     (
                      SELECT RIGHT (ace.ace_data_hora, 8)/*pega 8 caraceteres come�ando pela direita*/
                     )AS hora
                    FROM acessos ace
                    INNER JOIN credores cre
                    ON(cre.cre_cod = ace.credores_cre_cod)
                    INNER JOIN inadimplentes i
                    ON(i.ina_cod = ace.inadimplentes_ina_cod)
                    INNER JOIN usuarios usu
                    ON(usu.usu_cod = cre.usuarios_responsavel_cod)
                    WHERE SUBSTRING_INDEX(ace.ace_data_hora, ' ', 1) BETWEEN '$dataDe' AND '$dataAte' $filtro
                    ORDER BY cre.cre_nome_fantasia, i.ina_nome
                ");
        return $query->result();
    }

    //---------------------------------------------------------------------|
    //----------------- RELAT�RIOS DE PRESTA��O DE CONTAS -----------------|
    //---------------------------------------------------------------------|
    
        function rel_prest01($filtro,$dataDe,$dataAte) {
        $query = $this->db->query("
                SELECT 
                R.reb_cod,
                A.aco_tipo,
                C.cob_setor,
                R.reb_tipo,
                R.reb_data,
                A.aco_total_parcelado,
                A.aco_taxa_honorario,
                (SELECT SUM(PA.paa_saldo) FROM par_acordos PA WHERE PA.acordos_aco_cod = A.aco_cod) AS pendencia,
                R.reb_valor,
                R.reb_desconto,
                R.reb_parcelas,
                A.aco_qtd_parc,
                RP.rep_valor,
                RP.rep_nome,
                I.ina_nome,
                I.ina_cpf_cnpj
                 FROM recebimentos R 
                INNER JOIN acordos A ON (A.aco_cod=R.acordos_aco_cod) 
                INNER JOIN cobrancas C ON (A.cobranca_cob_cod = C.cob_cod) 
                INNER JOIN repasses RP ON (RP.rep_cod = C.repasses_rep_cod)
                INNER JOIN inadimplentes I ON (I.ina_cod = R.inadimplentes_ina_cod)
                WHERE R.reb_data BETWEEN '$dataDe' AND '$dataAte' $filtro ORDER BY R.reb_data,R.reb_cod ASC
            ");
        return $query->result();
    }
    
    function getParcInfo($cod){
        $query = $this->db->query("SELECT PAA.paa_parcela,PAA.paa_situacao FROM par_acordos PAA WHERE PAA.paa_cod = $cod");
        return $query->result();
    }
    
    function getCredInfo($cod){
        $query = $this->db->query("SELECT C.cre_logo,C.cre_nome_fantasia,C.cre_cpf_cnpj,C.cre_endereco,C.cre_fone1,C.cre_cidade FROM credores C WHERE C.cre_cod = $cod");
        return $query->row();
    }
    function getRelPrest03($filtro, $dataDe, $dataAte){
        /*Pega os acordos e previs�o a vencer no per�odo selecionado (de hoje at� a data escolhida).
         * Se o inadimplente tiver d�vidas diferentes para o mesmo credor, o relat�rio mostrar� todas
         * as d�vidas como uma s�, somar� a quantidade de parcelas de todas as d�vidas e o valor de devido de todas d�vidas.
         */
        $query = $this->db->query
                ("
                    SELECT co.cob_cod, i.ina_nome, cre.cre_nome_fantasia, COUNT(p_ac.paa_parcela) AS qtd_parcelas, SUM(p_ac.paa_valor) AS soma_parcelas,
                        (
                        /*Pega a pr�xima parcela a vencer*/
                        SELECT parac.paa_vencimento
                          FROM par_acordos parac
                          WHERE parac.acordos_aco_cod = ac.aco_cod AND parac.paa_situacao = 0
                          ORDER BY parac.paa_cod ASC LIMIT 1
                        ) AS proxima_parcela
                    FROM cobrancas co
                    INNER JOIN acordos ac
                    ON(co.cob_cod = ac.cobranca_cob_cod AND (co.cob_remocao NOT IN (1, 3)))
                    INNER JOIN par_acordos p_ac
                    ON(ac.aco_cod = p_ac.acordos_aco_cod AND (p_ac.paa_vencimento BETWEEN '$dataDe' AND '$dataAte') AND p_ac.paa_situacao = 0)
                    INNER JOIN inadimplentes i
                    ON(co.inadimplentes_ina_cod = i.ina_cod)
                    INNER JOIN credores cre
                    ON(co.credor_cre_cod = cre.cre_cod)
                    $filtro
                    GROUP BY cre.cre_nome_fantasia ORDER BY i.ina_nome
                ");
        return $query->result();
    }

    //|----------------------------------------------------------------------|
    //|------------------- RELAT�RIOS DE ACESSO DO CREDOR -------------------|
    //|----------------------------------------------------------------------|

    function getRelAce01($filtro, $dataDe, $dataAte){

        $query = $this->db->query
                ("
                    SELECT cre.cre_nome_fantasia, i.ina_nome, usu.usu_usuario_sis,
                     (
                      SELECT SUBSTRING_INDEX(ace.ace_data_hora, ' ', 1)/*pega tudo at� encontra o primeiro espa�o*/
                     )AS _data,
                     (
                      SELECT RIGHT (ace.ace_data_hora, 8)/*pega 8 caraceteres come�ando pela direita*/
                     )AS hora

                    FROM acessos ace
                    INNER JOIN credores cre
                    ON(cre.cre_cod = ace.credores_cre_cod AND cre.cre_ativo = 1)
                    INNER JOIN inadimplentes i
                    ON(i.ina_cod = ace.inadimplentes_ina_cod)
                    INNER JOIN usuarios usu
                    ON(usu.usu_cod = cre.usuarios_responsavel_cod)
                    WHERE SUBSTRING_INDEX(ace.ace_data_hora, ' ', 1) BETWEEN '$dataDe' AND '$dataAte' $filtro
                    ORDER BY cre.cre_nome_fantasia, i.ina_nome
                ");
        return $query->result();
    }

    
}
?>