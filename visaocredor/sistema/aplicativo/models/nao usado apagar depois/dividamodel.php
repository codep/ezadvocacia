<?php

class Dividamodel extends Model {

    var $table = ''; // TABELA PRINCIPAL

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getInadDados($cod) {
        $query = $this->db->query("SELECT * FROM inadimplentes I WHERE I.ina_cod='$cod'");
        return $query->row();
    }

    function getInadParentes($cod) {
        $query = $this->db->query("SELECT * FROM parentes P WHERE P.inadimplentes_ina_cod='$cod'");
        return $query->result();
    }

    function getCredoresComDividas($cod) {
        $query = $this->db->query("SELECT DISTINCT C.credor_cre_cod,CR.cre_nome_fantasia FROM cobrancas C RIGHT JOIN dividas D ON(D.cobranca_cob_cod=C.cob_cod) INNER JOIN credores CR ON (CR.cre_cod=C.credor_cre_cod) WHERE C.inadimplentes_ina_cod=$cod AND C.cob_setor='ADM' AND C.cob_status != '99' AND C.cob_remocao != '1'");
        return $query->result();
    }

    function getCredoresComAcordos($cod) {
        $query = $this->db->query("SELECT DISTINCT C.credor_cre_cod,CR.cre_nome_fantasia FROM cobrancas C RIGHT JOIN acordos A ON(A.cobranca_cob_cod=C.cob_cod) INNER JOIN credores CR ON (CR.cre_cod=C.credor_cre_cod) WHERE C.inadimplentes_ina_cod=$cod AND C.cob_setor='ADM' AND C.cob_status != '99' AND C.cob_remocao != '1'");
        return $query->result();
    }

    function getCredoresComJudicial($cod) {
        $query = $this->db->query("SELECT DISTINCT C.credor_cre_cod,CR.cre_nome_fantasia FROM cobrancas C INNER JOIN credores CR ON (CR.cre_cod=C.credor_cre_cod) WHERE C.inadimplentes_ina_cod=$cod AND C.cob_setor='JUD' AND C.cob_status != '99' AND C.cob_remocao != '1'");
        return $query->result();
    }

    function getDividasCredInad($inaCod, $creCod) {
        $query = $this->db->query("SELECT D.*,C.cob_cod FROM cobrancas C RIGHT JOIN dividas D ON(D.cobranca_cob_cod=C.cob_cod) WHERE C.inadimplentes_ina_cod=$inaCod AND C.credor_cre_cod=$creCod  AND C.cob_setor='ADM' AND C.cob_status != '99' AND C.cob_remocao != '1'");
        return $query->result();
    }

    function getDividasCredInadJUD($inaCod, $creCod) {
        $query = $this->db->query("SELECT C.cob_status,C.cob_cod,D.* FROM cobrancas C RIGHT JOIN dividas D ON(D.cobranca_cob_cod=C.cob_cod) WHERE C.inadimplentes_ina_cod=$inaCod AND C.credor_cre_cod=$creCod AND C.cob_setor='JUD' AND C.cob_status != '99' AND C.cob_remocao != '1'");
        return $query->result();
    }

    function getAcordosCredInad($inaCod, $creCod) {
        $query = $this->db->query("SELECT A.*,C.cob_cod FROM cobrancas C RIGHT JOIN acordos A ON(A.cobranca_cob_cod=C.cob_cod) WHERE C.inadimplentes_ina_cod=$inaCod AND C.credor_cre_cod=$creCod AND C.cob_setor='ADM' AND C.cob_status != '99' AND C.cob_remocao != '1'");
        return $query->result();
    }

    function getAcordosCredInadJUD($inaCod, $creCod) {
        $query = $this->db->query("SELECT C.cob_status,C.cob_cod,A.* FROM cobrancas C RIGHT JOIN acordos A ON(A.cobranca_cob_cod=C.cob_cod) WHERE C.inadimplentes_ina_cod=$inaCod AND C.credor_cre_cod=$creCod  AND C.cob_setor='JUD' AND C.cob_status != '99' AND C.cob_remocao != '1'");
        return $query->result();
    }

    function getAcordosJudRecemCriados($inaCod) {
        $query = $this->db->query("SELECT
                                      CE.cre_nome_fantasia,
                                      A.aco_valor_original,
                                      A.aco_emissao
                                    FROM cobrancas CO
                                      INNER JOIN credores CE
                                        ON (CE.cre_cod = CO.credor_cre_cod)
                                      INNER JOIN acordos A
                                        ON (A.cobranca_cob_cod = CO.cob_cod)
                                    WHERE CO.cob_status = 99
                                        AND CO.cob_setor = 'JUD' AND CO.inadimplentes_ina_cod = $inaCod");
        return $query->result();
    }

    function getParcelasDividas($divCod) {
        $query = $this->db->query("SELECT * FROM par_dividas PA WHERE PA.dividas_div_cod=$divCod");
        return $query->result();
    }

    function getParcelasAcordos($divCod) {
        $query = $this->db->query("SELECT * FROM par_acordos PA WHERE PA.acordos_aco_cod=$divCod");
        return $query->result();
    }

    function getUsuariosNomeCod() {
        $query = $this->db->query('SELECT U.usu_cod, U.usu_usuario_sis FROM usuarios U ORDER BY U.usu_usuario_sis');
        return $query->result();
    }

    function getRepasses() {
        $query = $this->db->query('select * from repasses order by rep_cod');
        return $query->result();
    }

    function getCidades() {
        $query = $this->db->query('SELECT DISTINCT c.cre_cidade from credores c ORDER BY c.cre_cidade');
        return $query->result();
    }

    function getAtivos($codigo) {
        $query = $this->db->query('SELECT FX_GET_ATIVOS(' . $codigo . ') as ativos;');
        return $query->row();
    }

    function getCredores($cidade) {
        if ($cidade == '' || $cidade == 'TODOS')
            $query = $this->db->query('SELECT * FROM credores c where c.cre_ativo = 1');
        else
            $query = $this->db->query('SELECT * FROM credores c where c.cre_ativo = 1 AND c.cre_cidade = "' . $cidade . '"');
        return $query->result();
    }

    function getCredor($cod) {
        $query = $this->db->query('SELECT * FROM credores c where c.cre_ativo=1 AND c.cre_cod=' . $cod);
        return $query->row();
    }

    function getCobDadosRecebimento($cod) {
        $query = $this->db->query("SELECT A.aco_cod,C.credor_cre_cod,C.inadimplentes_ina_cod,U.usu_usuario_sis FROM cobrancas C 
                                    INNER JOIN usuarios U ON (U.usu_cod = C.usuarios_usu_cod) 
                                    INNER JOIN acordos A ON (A.cobranca_cob_cod=C.cob_cod)  WHERE C.cob_cod=$cod");
        return $query->row();
    }

    function getCobDadosUltimoRecebimento($acoCod, $creCod, $inaCod, $data, $valor, $desconto) {
        $query = $this->db->query("SELECT * FROM recebimentos R WHERE R.acordos_aco_cod=$acoCod AND R.credor_cre_cod=$creCod AND R.inadimplentes_ina_cod=$inaCod AND R.reb_data='$data' AND R.reb_valor='$valor' AND R.reb_desconto='$desconto'");
        return $query->row();
    }

    function getEstados() {
        $query = $this->db->query('SELECT DISTINCT C.cid_estado FROM cidades C ORDER BY C.cid_estado ASC');
        return $query->result();
    }

    function getCidadesBrasil($uf) {
        $query = $this->db->query('SELECT DISTINCT C.cid_nome FROM cidades C WHERE c.cid_estado="' . $uf . '" ORDER BY C.cid_nome ASC');
        return $query->result();
    }

    function getBancos() {
        $query = $this->db->query('SELECT * FROM bancos ORDER BY banco_nome ASC');
        return $query->result();
    }

    function getAcoCodigo($cod) {
        $query = $this->db->query("SELECT * FROM acordos A WHERE A.cobranca_cob_cod=$cod");
        return $query->row();
    }

    function insert($dados=array()) {
        if ($this->db->insert('credores', $dados)) {
            return true;
        } else {
            return false;
        }
    }

    function getCobCad($sync) {
        $query = $this->db->query("SELECT C.cob_cod FROM cobrancas C WHERE C.cob_sync='$sync'");
        return $query->row();
    }

    function getDivCad($cod) {
        $query = $this->db->query("SELECT D.div_cod FROM dividas D WHERE D.cobranca_cob_cod='$cod'");
        return $query->row();
    }
    
    function getAcoCad($cod) {
        $query = $this->db->query("SELECT A.aco_cod FROM acordos A WHERE A.cobranca_cob_cod='$cod'");
        return $query->row();
    }

    function getSupervisores() {
        $query = $this->db->query("SELECT U.usu_nome,U.usu_cod,GU.gru_titulo FROM grupo_usuarios GU INNER JOIN usuarios U ON (U.grupo_usuarios_gru_cod=GU.gru_cod) WHERE (GU.gru_cod = '1') OR (GU.gru_cod = '2') OR (GU.gru_titulo = 'Administrador')");
        return $query->result();
    }

    /* -- LISTAR TODAS AS COBRANCAS -- */

    function getListarDividas($filtro) {
        $query = $this->db->query(
                        "SELECT c.cob_cod,div_cod, cobranca_cob_cod, i.ina_cod, i.ina_nome, cr.cre_nome_fantasia, div_emissao, div_cadastro, div_total
                           FROM dividas d
                             INNER JOIN cobrancas c
                               ON(c.cob_cod = d.cobranca_cob_cod)
                             INNER JOIN inadimplentes i
                               ON(i.ina_cod = c.inadimplentes_ina_cod)
                             INNER JOIN credores cr
                               ON(cr.cre_cod = c.credor_cre_cod)
                                 $filtro");
        return $query->result();
    }

    /* --- SELECTS TODAS AS MINHAS COBRAN�AS ADM OU AS COBRAN�AS ADM DE TODOS USU�RIOS --- */

    function getMinhasOuTodasCobrancasVirgensAdm($pesquisarPor, $minhaOuTodas) {
        $query = $this->db->query(
                        "SELECT div_cod, co.cob_cod, i.ina_nome, i.ina_cod, cre.cre_nome_fantasia, div_total,
                        (SELECT OP.ope_nome FROM ros RO INNER JOIN operacoes OP ON(OP.ope_cod=RO.operacoes_ope_cod) WHERE RO.cobranca_cob_cod=co.cob_cod ORDER BY RO.ros_cod DESC LIMIT 1) AS ultimo_ro
                           FROM dividas d
                             INNER JOIN cobrancas co
                               ON(d.cobranca_cob_cod = co.cob_cod AND(co.cob_remocao = 0))
                             INNER JOIN inadimplentes i
                               ON(co.inadimplentes_ina_cod = i.ina_cod)
                             INNER JOIN credores cre
                               ON(co.credor_cre_cod = cre.cre_cod)
                         WHERE (ina_nome LIKE '%$pesquisarPor%' OR cre_nome_fantasia LIKE '%$pesquisarPor%')
                           AND co.cob_setor = 'ADM'
                           AND d.div_virgem = '1'
                           $minhaOuTodas");
        return $query->result();
    }

    function getMinhasOuTodasCobrancasEmAndamentoAdm($pesquisarPor, $minhaOuTodas) {
;
        $query = $this->db->query(
                        "SELECT div_cod, co.cob_cod, i.ina_cod, i.ina_nome, cre.cre_nome_fantasia, div_total,
                        (SELECT OP.ope_nome FROM ros RO INNER JOIN operacoes OP ON(OP.ope_cod=RO.operacoes_ope_cod) WHERE RO.cobranca_cob_cod=co.cob_cod ORDER BY RO.ros_cod DESC LIMIT 1) AS ultimo_ro
                           FROM dividas d
                             INNER JOIN cobrancas co
                               ON(d.cobranca_cob_cod = co.cob_cod AND(co.cob_remocao = 0))
                             INNER JOIN inadimplentes i
                               ON(co.inadimplentes_ina_cod = i.ina_cod)
                             INNER JOIN credores cre
                               ON(co.credor_cre_cod = cre.cre_cod)
                         WHERE (ina_nome LIKE '%$pesquisarPor%' OR cre_nome_fantasia LIKE '%$pesquisarPor%')
                           AND co.cob_setor = 'ADM'
                           AND d.div_virgem = '0'
                           $minhaOuTodas");
        return $query->result();
    }

    function getMinhasOuTodasCobrancasEmAcordoAdm($pesquisarPor, $minhaOuTodas) {
        $query = $this->db->query(
                        "SELECT aco_cod, co.cob_cod, i.ina_cod, i.ina_nome, cr.cre_nome_fantasia, aco_valor_atualizado,
                        (SELECT OP.ope_nome FROM ros RO INNER JOIN operacoes OP ON(OP.ope_cod=RO.operacoes_ope_cod) WHERE RO.cobranca_cob_cod=co.cob_cod ORDER BY RO.ros_cod DESC LIMIT 1) AS ultimo_ro
                           FROM acordos ac
                             INNER JOIN cobrancas co
                               ON(ac.cobranca_cob_cod = co.cob_cod AND(co.cob_remocao = 0))
                             INNER JOIN inadimplentes i
                               ON(co.inadimplentes_ina_cod = i.ina_cod)
                             INNER JOIN credores cr
                               ON(co.credor_cre_cod = cr.cre_cod)
                         WHERE (ina_nome LIKE '%$pesquisarPor%' OR cre_nome_fantasia LIKE '%$pesquisarPor%')
                           AND aco_procedencia = 'ADM'
                           $minhaOuTodas");
        return $query->result();
    }

    function getMinhasOuTodasCobrancasEmAtrasoAdm($pesquisarPor, $minhaOuTodas, $dataHoje) {
        $query = $this->db->query(
                        "SELECT div_cod, co.cob_cod, i.ina_cod, i.ina_nome, cr.cre_nome_fantasia, div_total, div_venc_inicial
                           FROM dividas d
                             INNER JOIN cobrancas co
                               ON(d.cobranca_cob_cod = co.cob_cod AND(co.cob_remocao = 0))
                             INNER JOIN inadimplentes i
                               ON(co.inadimplentes_ina_cod = i.ina_cod)
                             INNER JOIN credores cr
                               ON( co.credor_cre_cod = cr.cre_cod)
                         WHERE (ina_nome LIKE '%$pesquisarPor%' OR cre_nome_fantasia LIKE '%$pesquisarPor%')
                           AND co.cob_setor = 'ADM'
                           AND d.div_venc_inicial <= '$dataHoje'
                           $minhaOuTodas");
        return $query->result();
    }

    /* --- FIM SELECTS TODAS AS MINHAS COBRAN�AS ADM OU AS COBRAN�AS ADM DE TODOS USU�RIOS --- */

    function getCobrancas($cre_cod) {
        $query = $this->db->query('select * from cobrancas where credor_cre_cod = ' . $cre_cod);
        return $query->result();
    }

    function remanejarPorCredor($rec_novo, $cob_cod) {
        $this->db->where('cob_cod', $cob_cod);
        $dados = array(
            'usuarios_usu_cod' => $rec_novo
        );
        $this->db->update('cobrancas', $dados);
        //equivalente �
//        $query = "update cobrancas set usuarios_usu_cod = $rec_novo where cob_cod = $cob_cod";
    }
    
    function getCobrancasDadosEnvJudDivida($cobCod){
        $query = $this->db->query("SELECT *
                                    FROM cobrancas C
                                      INNER JOIN dividas D
                                        ON (D.cobranca_cob_cod = C.cob_cod)
                                    WHERE C.cob_cod = $cobCod");
        return $query->row();
    }

}

?>