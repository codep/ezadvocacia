<?php

class Mensagens_model extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getUsuarios() {
        /* pega todos os usu�rio do sistema */
        $query = $this->db->query('SELECT usu_cod, usu_usuario_sis, usu_nome FROM usuarios ORDER BY usu_nome ASC');
        return $query->result();
    }

    function setNovaMensagem($dados) {
        /* Recebe os dados da nova mensagem e grava no banco */
        $this->db->insert('mensagens', $dados);
    }

    function getMinhasMensagensEnviadas($usuarioDaSessao) {
        /* pega todas as mensagens que o usu�rio da sess�o enviou (CAIXA DE SAIDA) */
        $query = $this->db->query("SELECT U.usu_usuario_sis, men_cod, men_titulo, men_texto, men_tipo, men_data, men_lida FROM mensagens M INNER JOIN usuarios U ON (U.usu_cod=M.usuarios_usu_cod) AND men_remetente = '$usuarioDaSessao' ORDER BY men_data ASC ");
        //die("$usuarioDaSessao");
        //print_r( $query->result());//die("");
        return $query->result();
    }

    function getMinhasMensagensRecebidas($codUsuariDaSessao) {
        /* Pega todas as mensagens do usu�rio da sess�o ordenando pela data da antiga para a mais nova (para usar na caixa de entrada) */
        $query = $this->db->query("SELECT * FROM mensagens M WHERE M.usuarios_usu_cod = $codUsuariDaSessao ORDER BY M.men_lida ASC,M.men_tipo ASC, M.men_data ASC");
        return $query->result();
    }

    function setMarcarComoLida($codMensagem) {
        /* pega o c�digo da mensagem (caixa de entrada) e muda a coluna men_lida de 0 para 1 */
        $query = $this->db->query("UPDATE mensagens SET men_lida = '1' WHERE men_cod = $codMensagem");
        return $this->db->affected_rows();
    }

    function excluirMensagem($codMensagem) {
        /* recebe o c�digo da mensagem e apaga a mensagem e retorna a quantidade de linhas afetadas no banco,
         * esse retorno � usado para dar mensagem de sucesso ou erro no controlador 
         */
        $this->db->where("men_cod", $codMensagem);
        $resultado = $this->db->delete("mensagens");
        return $this->db->affected_rows();
    }

    function getUsuarioResposta($cod) {
        $query = $this->db->query("SELECT U.usu_cod,U.usu_nome,M.men_titulo FROM mensagens M INNER JOIN usuarios U ON (U.usu_nome LIKE M.men_remetente) WHERE M.men_cod=$cod");
        return $query->row();
    }

}

?>