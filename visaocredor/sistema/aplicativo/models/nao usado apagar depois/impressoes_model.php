<?php

class Impressoes_model extends Model {

    var $table = ''; // TABELA PRINCIPAL

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getRecDados($cod){
        $query = $this->db->query("SELECT R.rec_cod,RB.reb_tipo,R.rec_valor,I.ina_nome, I.ina_cpf_cnpj,C.cre_razao_social,R.rec_obs,U.usu_nome AS recebedor,
                                    (SELECT U2.usu_nome FROM recibos R2 INNER JOIN recebimentos RB2 ON(RB2.reb_cod=R2.recebimentos_reb_cod) INNER JOIN usuarios U2 ON(U2.usu_usuario_sis=RB2.reb_baixador) WHERE R2.rec_cod='$cod') AS baixador
                                    FROM recibos R
                                    INNER JOIN inadimplentes I ON (I.ina_cod=R.inadimplentes_ina_cod)
                                    INNER JOIN recebimentos RB ON(RB.reb_cod=R.recebimentos_reb_cod)
                                    INNER JOIN credores C ON(C.cre_cod=RB.credor_cre_cod)
                                    INNER JOIN usuarios U ON(U.usu_cod=RB.usuarios_usu_cod)
                                    WHERE R.rec_cod='$cod'");
        return $query->row();
    }

}

?>