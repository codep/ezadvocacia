<?php
class Repasse_model extends Model {
    function  __construct()
    {
        // Call the Model constructor
        parent::Model();
    }

    function getRepasses(){
        $query = $this->db->query('SELECT * FROM repasses ORDER BY rep_nome ASC ');
        return $query->result();
    }
    function getUltimoRepasse(){
        $query = $this->db->query("SELECT * FROM repasses ORDER BY rep_cod DESC LIMIT 1");
        return $query->row();
    }
    

}

?>