<?php

class Recebimentos_model extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getCredores() {
        $query = $this->db->query(
                        'SELECT cre_cod, cre_nome_fantasia
                           FROM credores ORDER BY cre_nome_fantasia ASC');
        return $query->result();
    }

    /* cada fun��o � usada nas abas (hoje, semana, mes, pr�ximo m�s) nas views rec_listar, rec_listar_todos e rec_listar_previs�es */

    /* -----IN�CIO SELECTS MEUS RECEBIMENTOS----- */

    function getMeusRecebimentosHoje($filtroCredor, $dataDeHoje) {
        $query = $this->db->query(
                        "SELECT A.cobranca_cob_cod, I.ina_nome, C.cre_nome_fantasia, R.reb_data,R.reb_valor, R.reb_desconto,RB.rec_cod
                           FROM recebimentos R
                             INNER JOIN inadimplentes I
                               ON(I.ina_cod=R.inadimplentes_ina_cod)
                             INNER JOIN credores C
                               ON(C.cre_cod=R.credor_cre_cod) 
                             INNER JOIN acordos A
                               ON(A.aco_cod=R.acordos_aco_cod)
                               LEFT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=R.reb_cod)
                             $filtroCredor
                                 AND R.reb_data = '$dataDeHoje'");
        return $query->result();
    }

    function getMeusRecebimentosSemana($filtroCredor, $primeiroDiaDaSemana, $ultimoDiaDaSemana) {
        $query = $this->db->query(
                        "SELECT A.cobranca_cob_cod, I.ina_nome, C.cre_nome_fantasia, R.reb_data,R.reb_valor, R.reb_desconto,RB.rec_cod
                           FROM recebimentos R
                             INNER JOIN inadimplentes I
                               ON(I.ina_cod=R.inadimplentes_ina_cod)
                             INNER JOIN credores C
                               ON(C.cre_cod=R.credor_cre_cod) 
                             INNER JOIN acordos A
                               ON(A.aco_cod=R.acordos_aco_cod)
                               LEFT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=R.reb_cod)
                                $filtroCredor
                                 AND R.reb_data >= '$primeiroDiaDaSemana'
                                 AND R.reb_data<= '$ultimoDiaDaSemana' ");
        return $query->result();
    }

    function getMeusRecebimentosMes($filtroCredor, $mesAtual, $anoAtual) {
        $query = $this->db->query(
                        "SELECT A.cobranca_cob_cod, I.ina_nome, C.cre_nome_fantasia, R.reb_data,R.reb_valor, R.reb_desconto,RB.rec_cod
                           FROM recebimentos R
                             INNER JOIN inadimplentes I
                               ON(I.ina_cod=R.inadimplentes_ina_cod)
                             INNER JOIN credores C
                               ON(C.cre_cod=R.credor_cre_cod)
                             INNER JOIN acordos A
                               ON(A.aco_cod=R.acordos_aco_cod)
                                LEFT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=R.reb_cod) $filtroCredor
                                 AND R.reb_data >= '$anoAtual-$mesAtual-1'
                                 AND R.reb_data<= '$anoAtual-$mesAtual-31'");
        return $query->result();
    }

    function getMeusRecebimentosProximoMes($filtroCredor, $proximoMes, $anoAtual) {
        $query = $this->db->query(
                        "SELECT A.cobranca_cob_cod, I.ina_nome, C.cre_nome_fantasia, R.reb_data,R.reb_valor, R.reb_desconto,RB.rec_cod
                           FROM recebimentos R
                             INNER JOIN inadimplentes I
                               ON(I.ina_cod=R.inadimplentes_ina_cod)
                             INNER JOIN credores C
                               ON(C.cre_cod=R.credor_cre_cod)
                             INNER JOIN acordos A
                               ON(A.aco_cod=R.acordos_aco_cod)
                                LEFT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=R.reb_cod)
                                $filtroCredor
                                 AND R.reb_data >= '$anoAtual-$proximoMes-1'
                                 AND R.reb_data<= '$anoAtual-$proximoMes-31'");
        return $query->result();
    }

    /* ----- FIM MEUS RECEBIMENTOS ----- */


    /* ----- INICIO TODOS OS RECEBIMENTOS ----- */

    function getTodosRecebimentosHoje($filtroCredor, $dataDeHoje) {
        $query = $this->db->query(
                        "SELECT A.cobranca_cob_cod, I.ina_nome, C.cre_nome_fantasia, R.reb_data, R.usuarios_usu_cod, R.reb_valor, R.reb_desconto, U.usu_nome,RB.rec_cod
                           AS recuperador, R.reb_baixador
                         FROM recebimentos R
                           INNER JOIN inadimplentes I
                             ON(I.ina_cod=R.inadimplentes_ina_cod)
                           INNER JOIN credores C
                             ON(C.cre_cod=R.credor_cre_cod)
                           INNER JOIN usuarios U
                             ON(U.usu_cod=R.usuarios_usu_cod)
                             LEFT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=R.reb_cod)
                           INNER JOIN acordos A
                               ON(A.aco_cod = R.acordos_aco_cod) 
                        $filtroCredor = '$dataDeHoje'
                          ORDER BY C.cre_nome_fantasia ASC");
        return $query->result();
    }

    function getTodosRecebimentosSemana($filtroCredor, $primeiroDiaDaSemana, $ultimoDiaDaSemana) {
        $query = $this->db->query(
                        "SELECT A.cobranca_cob_cod, I.ina_nome, C.cre_nome_fantasia, R.reb_data, R.usuarios_usu_cod, R.reb_valor, R.reb_desconto, U.usu_nome AS recuperador, R.reb_baixador,RB.rec_cod
                           FROM recebimentos R
                             INNER JOIN inadimplentes I
                               ON(I.ina_cod=R.inadimplentes_ina_cod)
                             INNER JOIN credores C
                               ON(C.cre_cod=R.credor_cre_cod)
                             INNER JOIN usuarios U
                               ON(U.usu_cod=R.usuarios_usu_cod)
                               LEFT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=R.reb_cod)
                             INNER JOIN acordos A
                               ON(A.aco_cod = R.acordos_aco_cod)
                         $filtroCredor >= '$primeiroDiaDaSemana'
                           AND R.reb_data <= '$ultimoDiaDaSemana'
                             ORDER BY R.reb_data ASC, C.cre_nome_fantasia ASC");
        return $query->result();
    }

    function getTodosRecebimentosMes($filtroCredor, $mesAtual, $anoAtual) {
        $query = $this->db->query(
                        "SELECT A.cobranca_cob_cod, I.ina_nome, C.cre_nome_fantasia, R.reb_data, R.usuarios_usu_cod, R.reb_valor, R.reb_desconto,U.usu_nome AS recuperador, R.reb_baixador,RB.rec_cod
                           FROM recebimentos R
                             INNER JOIN inadimplentes I
                               ON(I.ina_cod=R.inadimplentes_ina_cod)
                             INNER JOIN credores C
                               ON(C.cre_cod=R.credor_cre_cod)
                             INNER JOIN usuarios U
                               ON(U.usu_cod=R.usuarios_usu_cod)
                               LEFT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=R.reb_cod)
                             INNER JOIN acordos A
                               ON(A.aco_cod = R.acordos_aco_cod)
                         $filtroCredor >= '$anoAtual-$mesAtual-01'
                        AND R.reb_data <= '$anoAtual-$mesAtual-31'
                          ORDER BY R.reb_data, C.cre_nome_fantasia ASC");
        return $query->result();
    }

    function getTodosRecebimentosProxMes($filtroCredor, $proximoMes, $anoAtual) {
        $query = $this->db->query(
                        "SELECT A.cobranca_cob_cod, I.ina_nome, C.cre_nome_fantasia, R.reb_data, R.usuarios_usu_cod, R.reb_valor, R.reb_desconto,U.usu_nome AS recuperador, R.reb_baixador,RB.rec_cod
                           FROM recebimentos R
                             INNER JOIN inadimplentes I
                               ON(I.ina_cod=R.inadimplentes_ina_cod)
                             INNER JOIN credores C
                               ON(C.cre_cod=R.credor_cre_cod)
                             INNER JOIN usuarios U
                               ON(U.usu_cod=R.usuarios_usu_cod)
                               LEFT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=R.reb_cod)
                             INNER JOIN acordos A
                               ON(A.aco_cod = R.acordos_aco_cod)
                         $filtroCredor >= '$anoAtual-$proximoMes-01'
                        AND R.reb_data <= '$anoAtual-$proximoMes-31'
                          ORDER BY R.reb_data, C.cre_nome_fantasia");
        return $query->result();
    }

    /* |^ -----FIM TODOS RECEBIMENTOS----- |^ */


    /*  ----- IN�CIO SELECTS PREVIS�ES ----- */

    function getPrevisoesHoje($filtroCredor, $dataDeHoje) {
        $query = $this->db->query(
                        "SELECT a.cobranca_cob_cod, i.ina_nome, cr.cre_nome_fantasia, r.reb_data, u.usu_nome AS recuperador, r.reb_baixador, r.reb_valor, r.reb_desconto,RB.rec_cod
                           FROM recebimentos r
                             INNER JOIN inadimplentes i
                               ON(r.inadimplentes_ina_cod = i.ina_cod)
                             INNER JOIN credores cr
                               ON(r.credor_cre_cod = cr.cre_cod)
                             INNER JOIN usuarios u
                               ON(r.usuarios_usu_cod = u.usu_cod)
                               LEFT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=r.reb_cod)
                             INNER JOIN acordos a
                               ON(a.aco_cod = r.acordos_aco_cod) 
                         WHERE r.reb_tipo = 2
                           AND r.reb_data = '$dataDeHoje'
                           $filtroCredor
                             ORDER BY r.reb_data ASC, r.credor_cre_cod ASC");
        return $query->result();
    }

    function getPrevisoesSemana($filtroCredor, $primeiroDiaDaSemana, $ultimoDiaDaSemana) {
        $query = $this->db->query(
                        "SELECT a.cobranca_cob_cod, i.ina_nome, cr.cre_nome_fantasia, r.reb_data, u.usu_nome AS recuperador, r.reb_baixador, r.reb_valor, r.reb_desconto,RB.rec_cod
                           FROM recebimentos r
                             INNER JOIN inadimplentes i
                               ON(r.inadimplentes_ina_cod = i.ina_cod)
                             INNER JOIN credores cr
                               ON(r.credor_cre_cod = cr.cre_cod)
                             INNER JOIN usuarios u
                               ON(r.usuarios_usu_cod = u.usu_cod)
                               LEFT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=r.reb_cod)
                             INNER JOIN acordos a
                               ON(a.aco_cod = r.acordos_aco_cod)
                         WHERE r.reb_tipo = 2
                           AND r.reb_data >= '$primeiroDiaDaSemana'
                           AND r.reb_data <= '$ultimoDiaDaSemana'
                           $filtroCredor
                             ORDER BY r.reb_data ASC, r.credor_cre_cod ASC");
        return $query->result();
    }

    function getPrevisoesMes($filtroCredor, $mesAtual, $anoAtual) {
        $query = $this->db->query(
                        "SELECT a.cobranca_cob_cod, i.ina_nome, cr.cre_nome_fantasia, r.reb_data, u.usu_nome AS recuperador, r.reb_baixador, r.reb_valor, r.reb_desconto,RB.rec_cod
                           FROM recebimentos r
                             INNER JOIN inadimplentes i
                               ON(r.inadimplentes_ina_cod = i.ina_cod)
                             INNER JOIN credores cr
                               ON(r.credor_cre_cod = cr.cre_cod)
                             INNER JOIN usuarios u
                               ON(r.usuarios_usu_cod = u.usu_cod)
                               LEFT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=r.reb_cod)
                             INNER JOIN acordos a
                               ON(a.aco_cod = r.acordos_aco_cod)
                         WHERE r.reb_tipo = 2
                           AND r.reb_data >= '$anoAtual-$mesAtual-01'
                           AND r.reb_data <= '$anoAtual-$mesAtual-31'
                           $filtroCredor
                             ORDER BY r.reb_data ASC, r.credor_cre_cod ASC");
        return $query->result();
    }

    function getPrevisoesProxMes($filtroCredor, $proximoMes, $anoAtual) {
        $query = $this->db->query(
                        "SELECT a.cobranca_cob_cod, i.ina_nome, cr.cre_nome_fantasia, r.reb_data, u.usu_nome AS recuperador, r.reb_baixador, r.reb_valor, r.reb_desconto,RB.rec_cod
                           FROM recebimentos r
                             INNER JOIN inadimplentes i
                               ON(r.inadimplentes_ina_cod = i.ina_cod)
                             INNER JOIN credores cr
                               ON(r.credor_cre_cod = cr.cre_cod)
                             INNER JOIN usuarios u
                               ON(r.usuarios_usu_cod = u.usu_cod)
                               LEFT JOIN recibos RB
				ON(RB.recebimentos_reb_cod=r.reb_cod)
                             INNER JOIN acordos a
                               ON(a.aco_cod = r.acordos_aco_cod)
                         WHERE r.reb_tipo = 2
                           AND r.reb_data >= '$anoAtual-$proximoMes-01'
                           AND r.reb_data <= '$anoAtual-$proximoMes-31'
                           $filtroCredor
                             ORDER BY r.reb_data ASC, r.credor_cre_cod ASC");
        return $query->result();
    }

    /* ----- FIM SELECTS PREVIS�ES ----- */
}

?>