<?php
class Inadimplente_model extends Model {
    function  __construct()
    {
        // Call the Model constructor
        parent::Model();
    }


    function verificaRecuperador($inaCod, $recupCod, $usuCreCod){
        $query = $this->db->query
                ("
                    SELECT co.cob_cod FROM cobrancas co WHERE co.usuarios_usu_cod = $recupCod AND co.inadimplentes_ina_cod = $inaCod AND co.credor_cre_cod = $usuCreCod AND co.cob_remocao IN(0, 4) LIMIT 1
                ");
        return sizeof($query->result());//sempre vai retornar 0 ou 1
    }
    
    function getRows($usuCreCod){//pega a quantidade de inadimplentes do credor logado para usar na pagina��o.
        $query = $this->db->query
                ("
                    SELECT DISTINCT
                      ina.ina_cod
                    FROM inadimplentes ina
                      INNER JOIN cobrancas co
                        ON ((ina.ina_cod = co.inadimplentes_ina_cod)
                            AND co.cob_remocao = 0)
                      INNER JOIN credores cre
                        ON ((cre.cre_cod = co.credor_cre_cod)
                            AND cre.cre_cod = $usuCreCod)
                ");
        return sizeof($query->result());
    }

    function getInadimplentes($usuCreCod, $ini, $fim){//pega os inadimplentes que t�m cobran�as ativas do credor logado
        $query = $this->db->query
                ("
                    SELECT DISTINCT ina.ina_cod, ina.ina_nome, ina.ina_cpf_cnpj, ina.ina_cidade, cre.usuarios_responsavel_cod
                    FROM inadimplentes ina
                    INNER JOIN cobrancas co
                    ON((ina.ina_cod = co.inadimplentes_ina_cod) AND co.cob_remocao = 0)
                    INNER JOIN credores cre
                    ON ((cre.cre_cod = co.credor_cre_cod) AND cre.cre_cod = $usuCreCod)
                    ORDER BY ina.ina_nome ASC LIMIT $ini, $fim
                ");
        return $query->result();
        }
    function getInadDados($inaCod) {
        $query = $this->db->query("SELECT * FROM inadimplentes I WHERE I.ina_cod='$inaCod'");
        return $query->row();
    }
    function getDividasAdm($inaCod, $usuCreCod) {//pega as d�vidas adm e suas parcelas
        $dividasAdm;//Nessa vari�vel ser� atribu�do o array com as d�vidas e suas parcelas

//        $query = $this->db->query//pegando as d�vidas adm
//                ("
//                    SELECT
//                      co.cob_cod, co.usuarios_usu_cod, co.cob_avaliacao,
//                      di.div_cod, di.div_documento, di.div_emissao, di.div_cadastro, SUM(pd.pad_valor) AS div_total
//                    FROM dividas di
//                      INNER JOIN cobrancas co
//                        ON ((di.cobranca_cob_cod = co.cob_cod)
//                            AND co.cob_setor = 'ADM' AND co.cob_remocao = 0)
//                      INNER JOIN inadimplentes ina
//                        ON (co.inadimplentes_ina_cod = ina.ina_cod)
//                      INNER JOIN par_dividas pd
//                        ON  pd.dividas_div_cod = di.div_cod
//                    WHERE ina.ina_cod = $inaCod
//                        AND co.credor_cre_cod = $usuCreCod
//                ");
        $query = $this->db->query("
            SELECT D.*,C.cob_cod, C.cob_avaliacao 
            FROM cobrancas C 
            RIGHT JOIN dividas D 
            ON(D.cobranca_cob_cod=C.cob_cod) 
            WHERE C.inadimplentes_ina_cod=$inaCod 
            AND C.credor_cre_cod=$usuCreCod  
            AND C.cob_setor='ADM' 
            AND C.cob_recem_enviada = 0 
            AND C.cob_remocao != '1' 
            AND C.cob_remocao != '4' 
            AND C.cob_remocao != '2'");
        
        $dividasAdm = $query->result();
        
        if(sizeof($dividasAdm) != 0){//se o inadimplente selecionado do credor logado tiver alguma d�vida
            foreach($dividasAdm as $divs){//para cada d�vida encontrada pega as informa��es da tabela par_dividas.
               $query =  $this->db->query("SELECT * FROM par_dividas pd WHERE pd.dividas_div_cod = $divs->div_cod");
                $divs->par_divida = $query->result();
            }
        }
        return $dividasAdm;
    }
    function getAcordosAdm($inaCod, $usuCreCod) {//Pega os acordos adm do inadimplente selecionado do credor logado
        $acordosdAdm;//Nessa vari�vel ser� atribu�do o array com os acordos e suas parcelas
        $dataHoje = date('Y-m-d'); //usada para ver as parcelas estao vencida ou nao
        
        $query = $this->db->query
                ("
                    SELECT
                      co.cob_cod, co.usuarios_usu_cod, co.cob_avaliacao,
                      CASE ac.aco_procedencia  WHEN 'ADM' THEN 'Administrativo' WHEN 'JUD' THEN 'Judicial' END AS aco_procedencia,
                      CASE ac.aco_tipo
                        WHEN 1 THEN 'Firmado'
                        WHEN 2 THEN 'Promessa'
                        WHEN 3 THEN 'Rec�m-enviado jud'
                        WHEN 4 THEN 'Renegocia��o'
                        ELSE 'Desconhecido'
                        END AS aco_tipo,
                      ac.aco_cod, ac.cobranca_cob_cod, ac.aco_emissao, ac.aco_valor_original, ac.aco_valor_atualizado
                    FROM acordos ac
                      INNER JOIN cobrancas co
                        ON (ac.cobranca_cob_cod = co.cob_cod
                            AND co.cob_remocao = 0
                            AND co.cob_setor = 'ADM')
                      INNER JOIN inadimplentes ina
                        ON (co.inadimplentes_ina_cod = ina.ina_cod)
                    WHERE ina.ina_cod = $inaCod
                        AND co.credor_cre_cod = $usuCreCod
                ");
        $acordosdAdm =  $query->result();
        
        if(sizeof($acordosdAdm) != 0){//pegando as informacoes da tabela par_acordos de cada acordo encontrado
            foreach ($acordosdAdm as $aco){
                $query = $this->db->query
                        ("
                            SELECT
                             pa.paa_cod, pa.paa_parcela, pa.paa_vencimento, pa.paa_valor, pa.paa_saldo,
                             (pa.paa_valor - pa.paa_saldo) AS valor_descontado,
                             CASE pa.paa_situacao
                              WHEN 1 THEN 'pago'
                              WHEN 0 THEN IF((paa_vencimento < CURRENT_DATE), 'vencida', 'em aberto')
                              ELSE 'desconhecido' END AS situacao
                            FROM par_acordos pa WHERE pa.acordos_aco_cod = $aco->aco_cod ORDER BY pa.paa_cod ASC;
                        ");
                $aco->par_acordo = $query->result();
                /*--------------- Verificando se a parcela est� vencida ou est� em dia ---------------*/
                foreach($aco->par_acordo as $pac){
                    if($pac->situacao == 'pago'){
                        $pac->corCss = 'blue';
                    }else if($pac->situacao == 'vencida'){
                            $pac->corCss = 'red';//adicionando a posicao corCss no array de parcelas de acordo
                            if($pac->paa_saldo != $pac->paa_valor){
                                $pac->situacao = 'parcial';
                            }
                        }else if($pac->situacao == 'em aberto'){
                            $pac->corCss = 'black';
                            if($pac->paa_saldo != $pac->paa_valor){
                                $pac->corCss = 'blue';
                            }
                        }

                        /* ************* verificando a data do �ltimo recebimentod a parcela ************* */
                        $query = $this->db->query("SELECT rec.reb_data FROM recebimentos rec WHERE rec.reb_parcelas LIKE '%$pac->paa_cod%' ORDER BY rec.reb_cod DESC LIMIT 1");
                        $pac->dataUltimoPagamento = ($query->row() != null) ? convData($query->row()->reb_data, 'd') : '' ;
                        /* ************* fim verificando a data do �ltimo recebimentod a parcela ************* */
                    }
                /*--------------- fim Verificando se a parcela est� vencida ou est� em dia ---------------*/
            }
        }
        return $acordosdAdm;
    }
    function getAcordsJudRecemEnviados($usuCreCod, $inaCod) {

        $query = $this->db->query
                ("
                    SELECT 
                        co.cob_cod,
                        cre.cre_nome_fantasia,
                        ac.aco_cod, ac.aco_valor_original, ac.aco_emissao, ac.aco_cod
                    FROM cobrancas co
                    INNER JOIN credores cre
                        ON(cre.cre_cod = co.credor_cre_cod)
                    INNER JOIN acordos ac
                        ON(ac.cobranca_cob_cod = co.cob_cod)
                    WHERE co.cob_setor = 'JUD' AND co.cob_status = 99 AND co.cob_remocao IN(0, 4) AND co.credor_cre_cod = $usuCreCod AND co.inadimplentes_ina_cod = $inaCod
                ");
        return $query->result();
    }
    function getAcordsJudFirmados($usuCreCod, $inaCod) {
        $acordsJudFirmados; // vai receber os acordos firmados e as informacoes da par_acodos
        $dataHoje = date('Y-m-d'); //usado para verificar se a parcela est� vencida ou nao
        $query = $this->db->query
                ("
                    SELECT
                          co.cob_cod, co.cob_avaliacao, co.usuarios_usu_cod,
                          cre.cre_nome_fantasia,
                          CASE ac.aco_procedencia  WHEN 'ADM' THEN 'Administrativo' WHEN 'JUD' THEN 'Judicial' WHEN 'REA' THEN 'Renegocia��o' END AS aco_procedencia,
                          CASE ac.aco_tipo WHEN 1 THEN 'Firmado' WHEN 2 THEN 'Promessa' WHEN 3 THEN 'Rec�m enviad. ao jud.' WHEN 4 THEN 'Renegocia��o' ELSE 'Desconhecido' END AS aco_tipo,
                          ac.aco_cod, ac.aco_valor_original,ac.aco_emissao, ac.aco_valor_original, ac.aco_valor_atualizado
                    FROM cobrancas co
                    INNER JOIN credores cre
                        ON(cre.cre_cod = co.credor_cre_cod)
                    INNER JOIN acordos ac
                        ON(ac.cobranca_cob_cod = co.cob_cod)
                    WHERE co.cob_setor = 'JUD' AND co.cob_status != 99 AND co.cob_remocao IN(0, 4) AND co.credor_cre_cod = $usuCreCod AND co.inadimplentes_ina_cod = $inaCod
                ");
        $acordsJudFirmados = $query->result();

        if(sizeof($acordsJudFirmados) != 0){
            foreach ($acordsJudFirmados as $acordo){//pegando as informacoes da tabela par_acordos de cada acordo encontrado
                $query = $this->db->query
                        ("
                            SELECT
                             pa.paa_cod, pa.paa_situacao,pa.paa_parcela, pa.paa_vencimento, pa.paa_valor, pa.paa_saldo, pa.paa_situacao,
                             (pa.paa_valor - pa.paa_saldo)AS valor_descontado,
                             CASE pa.paa_situacao
                              WHEN 1 THEN 'pago'
                              WHEN 0 THEN IF((paa_vencimento < CURRENT_DATE), 'vencida', 'em aberto')
                              ELSE 'desconhecido' END AS situacao
                            FROM par_acordos pa WHERE pa.acordos_aco_cod = $acordo->aco_cod ORDER BY pa.paa_cod
                        ");
                $acordo->par_acordo = $query->result();

                /*--------------- Verificando se a parcela est� vencida ou est� em dia ---------------*/
                foreach ($acordo->par_acordo as $acoParcela){
                    if($acoParcela->situacao == 'pago'){
                        $acoParcela->corCss = 'blue';
                }else if($acoParcela->situacao == 'vencida'){
                        $acoParcela->corCss = 'red';//adicionando a posicao corCss no array
                        if($acoParcela->paa_saldo != $acoParcela->paa_valor){
                            $acoParcela->situacao = 'parcial';
                        }
                    }else if($acoParcela->situacao == 'em aberto'){
                        $acoParcela->corCss = 'black';
                        if($acoParcela->paa_saldo != $acoParcela->paa_valor){
                            $acoParcela->situacao = 'parcial';
                            $acoParcela->corCss = 'blue';
                        }
                    }

                     /* ************* verificando a data do �ltimo recebimentod a parcela ************* */
                $query = $this->db->query("SELECT rec.reb_data FROM recebimentos rec WHERE rec.reb_parcelas LIKE '%$acoParcela->paa_cod%' ORDER BY rec.reb_cod DESC LIMIT 1");
                $acoParcela->dataUltimoPagamento = ($query->row() != null) ? convData($query->row()->reb_data, 'd') : '' ;
                /* ************* fim verificando a data do �ltimo recebimentod a parcela ************* */
                }
                /*--------------- fim Verificando se a parcela est� vencida ou est� em dia ---------------*/

            }
        }
        return $acordsJudFirmados;
    }
    function getCodGerador($cod) {
        $query = $this->db->query("SELECT A.aco_cob_cod_geradora FROM acordos A WHERE A.aco_cod = $cod");
        return $query->row();
    }

    function getCodAcordoGerador($codGerador) {
        $query = $this->db->query("SELECT A.aco_cod FROM cobrancas C INNER JOIN acordos A ON (A.cobranca_cob_cod = C.cob_cod) WHERE C.cob_cod = $codGerador");
        return $query->row();
    }

    function getCodDividaGerador($codGerador) {
        $query = $this->db->query("SELECT D.div_cod FROM cobrancas C INNER JOIN dividas D ON (D.cobranca_cob_cod = C.cob_cod) WHERE C.cob_cod = $codGerador");
        return $query->row();
    }

    function getCobrancaDadosDiv($divCod) {
        $query = $this->db->query("SELECT
                                      CR.cre_pessoa,
                                      CR.cre_nome_fantasia,
                                      CR.cre_cpf_cnpj,
                                      R.rep_nome,
                                      I.ina_pessoa,
                                      I.ina_nome,
                                      I.ina_cpf_cnpj,
                                      D.div_documento      AS documento,
                                      D.div_banco          AS banco,
                                      D.div_agencia        AS agencia,
                                      D.div_alinea         AS alinea,
                                      D.div_emissao        AS emissao,
                                      D.div_info           AS informacoes,
                                      U.usu_nome,
                                      C.cob_cod
                                    FROM dividas D
                                      INNER JOIN cobrancas C
                                        ON (C.cob_cod = D.cobranca_cob_cod)
                                      INNER JOIN credores CR
                                        ON (CR.cre_cod = C.credor_cre_cod)
                                      INNER JOIN inadimplentes I
                                        ON (I.ina_cod = C.inadimplentes_ina_cod)
                                      INNER JOIN repasses R
                                        ON (R.rep_cod = C.repasses_rep_cod)
                                      INNER JOIN usuarios U
                                        ON (U.usu_cod = C.usuarios_usu_cod)
                                    WHERE D.div_cod = '$divCod'");
        return $query->row();
    }

    function getCobrancaDadosAco($acoCod) {
        $query = $this->db->query("SELECT
                                  CR.cre_pessoa,
                                  CR.cre_nome_fantasia,
                                  CR.cre_cpf_cnpj,
                                  R.rep_nome,
                                  I.ina_pessoa,
                                  I.ina_nome,
                                  I.ina_cpf_cnpj,
                                  ('')                 AS documento,
                                  ('')                 AS banco,
                                  ('')                 AS agencia,
                                  ('')                 AS alinea,
                                  A.aco_emissao        AS emissao,
                                  ('')                 AS informacoes,
                                  U.usu_nome,
                                  C.cob_cod
                                FROM acordos A
                                  INNER JOIN cobrancas C
                                    ON (C.cob_cod = A.cobranca_cob_cod)
                                  INNER JOIN credores CR
                                    ON (CR.cre_cod = C.credor_cre_cod)
                                  INNER JOIN inadimplentes I
                                    ON (I.ina_cod = C.inadimplentes_ina_cod)
                                  INNER JOIN repasses R
                                    ON (R.rep_cod = C.repasses_rep_cod)
                                  INNER JOIN usuarios U
                                    ON (U.usu_cod = C.usuarios_usu_cod)
                                WHERE A.aco_cod = '$acoCod'");
        return $query->row();
    }

    function getCobrancaParcelasDiv($divCod) {
        $query = $this->db->query("SELECT
                                  PD.pad_doc_num    AS docnum,
                                  PD.pad_par_num    AS parnum,
                                  PD.pad_valor      AS parvalor,
                                  PD.pad_vencimento AS parvenc
                                FROM par_dividas PD
                                WHERE PD.dividas_div_cod = $divCod");
        return $query->result();
    }

    function getCobrancaParcelasAco($acoCod) {
        $query = $this->db->query("SELECT
                                  PA.paa_cod    AS docnum,
                                  PA.paa_parcela    AS parnum,
                                  PA.paa_saldo      AS parvalor,
                                  PA.paa_vencimento AS parvenc
                                FROM par_acordos PA
                                WHERE PA.acordos_aco_cod = $acoCod");
        return $query->result();
    }

    function _getRos($cobsCods) {
        
        $outrosRos = $this->db->query("SELECT A.`aco_cob_cod_geradora` FROM acordos A WHERE A.`cobranca_cob_cod` IN ($cobsCods)")->result();
        
        foreach ($outrosRos as $ro) {
            $cobsCods .= ','.$ro->aco_cob_cod_geradora;
        }
        $query = $this->db->query
                ("
                    SELECT
                     ro.ros_data, ro.ros_detalhe,
                     (SELECT op.ope_nome FROM operacoes op WHERE op.ope_cod = ro.operacoes_ope_cod) AS operacao,
                     (SELECT usu.usu_usuario_sis FROM usuarios usu WHERE usu.usu_cod = ro.usuarios_usu_cod) AS recuperador
                    FROM ros ro WHERE ro.cobranca_cob_cod IN($cobsCods) ORDER BY ro.`ros_data` DESC
                ");
        return $query->result();
    }
}

?>