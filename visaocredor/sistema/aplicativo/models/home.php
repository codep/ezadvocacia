<?php

class Home extends Controller {

	function Home()
	{
            parent::Controller();
            
            // CONFIGURA��ES DE PAGINA
            $this->data=array(
                'title'=>'Recupera - Sistema de cobran�a - Home', // 90 o Google ir� cortar o seu t�tulo no caracter 63.
                'description'=>'', //250 o snippet do Google ir� cortar a sua descri��o no caracter 160.
                'keywords'=>'' //200
            );

            $this->load->model('Homemodel', 'md');
            
	}
	
	function index()
	{
            $this->inicore->addcss(array('reset','style','style_fixed','colors/blue')); // CSS HOME
            $this->load->helper("funcoes_helper");
            $this->inicore->addjs(array('jquery-1.4.2.min.js','jquery-ui-1.8.custom.min.js','jquery.ui.selectmenu.js','jquery.flot.min.js','tiny_mce/jquery.tinymce.js','smooth.js','smooth.menu.js','smooth.table.js','smooth.form.js','smooth.dialog.js','smooth.autocomplete.js'));

//------------------------------------------------------------------------------
            include 'testar_conexao.php';
//------------------------------------------------------------------------------

            $this->inicore->loadSidebar();

            
            
            $this->inicore->loadview('home',$this->data);

	}
}
