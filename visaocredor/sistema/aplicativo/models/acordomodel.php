<?php

class Acordomodel extends Model {

    var $table = ''; // TABELA PRINCIPAL

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getCobDados($cod) {
        $query = $this->db->query("SELECT
                                  I.ina_nome,
                                  CR.cre_juros,
                                  CR.cre_honorario,
                                  C.repasses_rep_cod,
                                  I.ina_cod,
                                  CR.cre_cod,
                                  C.usuarios_usu_cod,
                                  C.cob_prazo,
                                  C.cob_cod
                                FROM cobrancas C
                                  INNER JOIN inadimplentes I
                                    ON (I.ina_cod = C.inadimplentes_ina_cod)
                                  INNER JOIN credores CR
                                    ON (CR.cre_cod = C.credor_cre_cod)
                                  INNER JOIN repasses R
                                    ON (R.rep_cod = C.repasses_rep_cod)
                                WHERE C.cob_cod = $cod ");
        return $query->row();
    }

    function getParcDados($filtro) {
        $query = $this->db->query("SELECT *
                                    FROM par_dividas PD
                                    $filtro
                                    ORDER BY PD.pad_vencimento asc");
        return $query->result();
    }

    function getCobCod($filtro) {
        $query = $this->db->query("SELECT C.cob_cod
                                FROM par_dividas PD
                                  INNER JOIN dividas D
                                    ON (D.div_cod = PD.dividas_div_cod)
                                  INNER JOIN cobrancas C
                                    ON (C.cob_cod = D.cobranca_cob_cod)
                                $filtro
                                GROUP BY C.cob_cod");
        return $query->row();
    }
    
    function getCobCad($sync) {
        $query = $this->db->query("SELECT C.cob_cod FROM cobrancas C WHERE C.cob_sync='$sync'");
        return $query->row();
    }
    function getAcoCad($cod) {
        $query = $this->db->query("SELECT A.aco_cod FROM acordos A WHERE A.cobranca_cob_cod='$cod'");
        return $query->row();
    }
}

?>