<?php
class Usuarios_model extends Model {
    function  __construct()
    {
        // Call the Model constructor
        parent::Model();
    }

    function getUsuarios(){
        $query = $this->db->query('SELECT U.*,G.gru_titulo FROM usuarios U INNER JOIN grupo_usuarios G ON(G.gru_cod=U.grupo_usuarios_gru_cod) WHERE usu_ativo = 1 ORDER BY U.usu_nome ASC');
        return $query->result();
    }
    function getGrupos(){
        $query = $this->db->query('SELECT G.gru_cod,G.gru_titulo FROM grupo_usuarios G ORDER BY G.gru_titulo');
        return $query->result();
    }
    function getEstados(){
        $query = $this->db->query('SELECT DISTINCT C.cid_estado FROM cidades C ORDER BY C.cid_estado ASC');
        return $query->result();
    }
    function getUsuario($cod){
        $query = $this->db->query('SELECT U.*, G.* FROM  usuarios U INNER JOIN grupo_usuarios G ON (U.grupo_usuarios_gru_cod = G.gru_cod) WHERE U.usu_cod =' .$cod);
        return $query->row();
    }
    function getContasDetalhes($cod){
        $query = $this->db->query('SELECT U.*,G.gru_titulo FROM usuarios U INNER JOIN grupo_usuarios G ON(G.gru_cod=U.grupo_usuarios_gru_cod) WHERE U.usu_cod='.$cod);
        return $query->row();
    }
}

?>