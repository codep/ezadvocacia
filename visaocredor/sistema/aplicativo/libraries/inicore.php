<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inicore {

    private $CI;

    function __constructor() {
        $this->CI = & get_instance();
    }

    public function loadview($pagina, $data=array()) 
    {
        // VERIFICANDO SE FOI INFORMADO UM TEMPLATE
        if (!is_string($pagina)) {
            show_error('N�o foi poss��vel carregar o layout.');
        }
        $CI = & get_instance(); // CI SUPER OBJETO
        // JUNTANDO ARRAYS
        $CI->data = array_merge($CI->data, $data);
        //verifica se foi passado alguma mensagem para a view e pega ela
        $this->getMensagem();

        // VARIAVEIS UTILIZADAS NO SITE
        $CI->data['flash'] = base_url() . 'sistema/aplicativo/flash/';
        $CI->data['imgdb'] = base_url() . 'sistema/aplicativo/imgdb/';
        $CI->data['img'] = base_url() . 'sistema/aplicativo/style/img/';
        $CI->data['data_atual'] = date('d/m/Y');
        $CI->data['download'] = base_url() . 'sistema/aplicativo/download/';

        if (!isset($CI->data['jshtml']))
            $CI->data['jshtml'] = '';
        if (!isset($CI->data['csshtml']))
            $CI->data['csshtml'] = '';
        // INICIANDO PROCESSO DE CONSTRU��O DO LAYOUT
        // CARREGANDO O CABE�ALHO
        $CI->layout['header'] = $CI->load->view('estrutura/header', $CI->data, true);
        // CARREGA O RODAP�
        $CI->layout['footer'] = $CI->load->view('estrutura/footer', $CI->data, true);
        // CARREGANDO O CONTE�DO DA P�GINA
        $CI->layout['conteudo'] = $CI->load->view('template/' . $pagina, $CI->data, true);

        // CARREGANDO A ESTRUTURA (algumas p�ginas tem estrutura sem o cabe�alho
        // e o rodap� portanto � testado a p�gina solicitada para saber qual estrutura carregar
        if (($pagina == 'login') || ($pagina == 'conf_exclusao') || ($pagina == 'mens_padrao')) {
            $CI->load->view('estrutura/logestrutura', $CI->layout);
        } else if (
                ($pagina == 'imp_recibo')
                || ($pagina == 'rel_adm01')
                || ($pagina == 'rel_adm02')
                || ($pagina == 'rel_adm03')
                || ($pagina == 'rel_adm04')
                || ($pagina == 'rel_jur01')
                || ($pagina == 'rel_jur02')
                || ($pagina == 'rel_jur03')
                || ($pagina == 'rel_ro01')
                || ($pagina == 'rel_ger01')
                || ($pagina == 'rel_ger02')
                || ($pagina == 'rel_ger03')
                || ($pagina == 'rel_ger04')
                || ($pagina == 'rel_ger05')
                || ($pagina == 'rel_ger06')
                || ($pagina == 'rel_ger07')
                || ($pagina == 'rel_prest01')
                || ($pagina == 'rel_prest03')
                || ($pagina == 'rel_ace01')
                
                )
        {
            $CI->load->view('estrutura/impestrutura', $CI->layout);
        } else {
            $CI->load->view('estrutura/estrutura', $CI->layout);
        }
    }

    public function addjs($param) {
        if (count($param) == 0)
            return false;
        $CI = & get_instance();
        $jstemp = '';
        foreach ($param as $item) {
            if (strpos($item, '.js') === false)
                $item.='.js';
            // VERIFICANDO EM QUAL PASTA BUSCAR
            if (is_file(APPPATH . 'jscript/' . $item)) {
                $jstemp.='<script type="text/javascript" src="' . base_url() . 'sistema/aplicativo/jscript/' . $item . '"></script>';
            } else {
                $jstemp.='<script type="text/javascript" src="' . base_url() . 'sistema/aplicativo/jscript/plugin/' . $item . '"></script>';
            }
        }
        if (!isset($CI->data['jshtml']))
            $CI->data['jshtml'] = $jstemp;
        else
            $CI->data['jshtml'].=$jstemp;
        return true;
    }

    public function addcss($param) {
        if (count($param) == 0)
            return false;
        $CI = & get_instance();
        $csstemp = '';
        foreach ($param as $item) {
            if (strpos($item, '.css') === false)
                $item.='.css';
            // VERIFICANDO EM QUAL PASTA BUSCAR
            if (is_file(APPPATH . 'style/css/' . $item)) {
                $csstemp.= '<link rel="stylesheet" type="text/css" href="' . base_url() . 'sistema/aplicativo/style/css/' . $item . '" />';
            } else {
                $csstemp.= '<link rel="stylesheet" type="text/css" href="' . base_url() . 'sistema/aplicativo/jscript/plugin/' . $item . '" />';
            }
        }
        if (!isset($CI->data['csshtml']))
            $CI->data['csshtml'] = $csstemp;
        else
            $CI->data['csshtml'].=$csstemp;
        return true;
    }

    public function addflash($id, $conf) {
        if (count($conf) == 0 || !$id)
            return false;
        // MONTANDO O HTML
        $flashhtml = "
            <script type='text/javascript'>
                $(document).ready(function(){
                    $('#$id').flash({
        ";
        foreach (array_keys($conf) as $item) {
            // VERIFICANDO SE O ITEM � ARRAY
            if (is_array($conf[$item])) {
                $c = 0;
                $tarray = count($conf[$item]);
                // PERCORRENDO A ARRAY E PEGANDO VALORES
                foreach (array_keys($conf[$item]) as $vars) {
                    $c++;
                    if ($c == 1
                        )$flashhtml.="$item:{";
                    $flashhtml.=$c < $tarray ? "$vars: '" . $conf[$item][$vars] . "'," : "$vars: '" . $conf[$item][$vars] . "'";
                }
                $flashhtml.="},";
            } else {
                $flashhtml.="
                    $item: '$conf[$item]',
                ";
            }
        }
        $flashhtml .= "
                    wmode: 'transparent'},
                    {expressInstall: true}
                );
            });
        </script>
        ";
        return $flashhtml;
    }
    function loadSidebar() { // esta fun��o servira para carregar uma sidebar para cada pagina
        $CI = & get_instance(); // pega a instancia retorna o superobjeto
        $CI->data['img'] = base_url() . 'sistema/aplicativo/style/img/'; //dita o caminho das imagens para o navegador encontrar as imagens
        // aqui ele gera o codigo apartir do arquivo dentro da pasta template e atribui tudo a data, que eh carregado no site
        //depois disto temos que ir  na view para carregar a sidebar
        $CI->data['sidebar'] = $CI->load->view('estrutura/sidebar', $CI->data, true);
    }
    /*     * ************************************************************************
     * CRIA UMA MENSAGEM DE RETORNO
     * @PARAMETRO
     *  $tipo   - qual o tipo de mensagem
     *          ex.: success
     *               error
     *               warning
     *               notice
     *  $msg    - mensagem para o usu�rio
     *  $pers   - se deve manter mensagem para pr�xima tela
     * ************************************************************************ */

    public function setMensagem($tipo, $msg, $pers=true) {
        $this->CI = & get_instance();
        $img = base_url() . 'sistema/aplicativo/style/img/';
        //monta o html da menssagem de acordo com o que foi passado nos parametros
        $mensagem = "<div id=\"box-messages\" style=\"margin-bottom: 10px;\">
                <div class=\"messages\">
                    <div id=\"message-$tipo\" class=\"message message-$tipo\">
                        <div class=\"image\">
                            <img src=\"$img" . "icons/$tipo.png\" alt=\"$tipo\" height=\"32\" />
                        </div>
                        <div class=\"text\" style=\"width: 245px;\">
                            <h6>$msg</h6>
                        </div>
                        <div class=\"dismiss\">
                            <a href=\"#message-$tipo\"></a>
                        </div>
                    </div>
                </div>
            </div>";
            //ve se deve manter a mensagem para a proxima tela
        if ($pers == true) {
            $this->CI->session->set_flashdata('mensagem', $mensagem);
        } else {
            $this->CI->data['mensagem'] = $mensagem;
        }
    }
    public function getMensagem($pers=false) {//func�ao que pega a mensagem que foi passada e adiciona a uma vari�vel
        $this->CI = & get_instance();
        $getFlashData = $this->CI->session->flashdata('mensagem');
        if (!isset($this->CI->data['mensagem'])) {
            $this->CI->data['mensagem'] = $getFlashData;
        }
        if ($pers == true)
            $this->CI->session->keep_flashdata('mensagem');
    }
}
?>