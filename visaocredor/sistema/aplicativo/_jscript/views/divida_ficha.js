<script type="text/javascript">
    $(document).ready(function(){

        $('.accordion').accordion({
            autoheight:false
        });

        $('input[name="valorRecebido"]').priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });

        $('input[name="desconto"]').priceFormat({
            prefix: "",
            centsSeparator: ",",
            thousandsSeparator: "."
        });

        v1 = '';
        base_url = "<?php echo base_url(); ?>";
        $('.fechar').click(function(e){
            e.preventDefault();
            $('#dialog-form').dialog('close');
        });

        $('.fechar2').click(function(e){
            e.preventDefault();
            $('#dialog-form2').dialog('close');
        });

        $('.dialog-message-open').click(function(){
            $('#cobCod').attr('value', $(this).attr('id'));
        });

        $(".dialog-form-open2").click(function () {
            $("#dialog-form2").dialog("open");
            return false;
        });

        $(".ACHeader").click(function () {
            $('#cobCodJud').attr('value', $(this).attr('id'));
        });

        $(".ACHeaderAcordo").click(function () {
            $('#cobCodAco').attr('value', $(this).attr('id'));
            $('#cobCodRec').attr('value', $(this).attr('id'));
        });

        $('.telefone').mask('(99)9999-9999');

        //------------------------------------------------------------------------------
        //---------------------------Fun��o para validar CNPJ---------------------------
        //------------------------------------------------------------------------------
        function validaCNPJ() {
            CNPJ = $('input[name="cpf_cnpj"]').val();
            erro = new String;
            if (CNPJ.length < 18) erro += "� necessario preencher corretamente o n�mero do CNPJ! \n\n";
            if ((CNPJ.charAt(2) != ".") || (CNPJ.charAt(6) != ".") || (CNPJ.charAt(10) != "/") || (CNPJ.charAt(15) != "-")){
                if (erro.length == 0) erro += "� necess�rio preencher corretamente o n�mero do CNPJ! \n\n";
            }
            //substituir os caracteres que n�o s�o n�meros
            if(document.layers && parseInt(navigator.appVersion) == 4){
                x = CNPJ.substring(0,2);
                x += CNPJ. substring (3,6);
                x += CNPJ. substring (7,10);
                x += CNPJ. substring (11,15);
                x += CNPJ. substring (16,18);
                CNPJ = x;
            } else {
                CNPJ = CNPJ. replace (".","");
                CNPJ = CNPJ. replace (".","");
                CNPJ = CNPJ. replace ("-","");
                CNPJ = CNPJ. replace ("/","");
            }
            var nonNumbers = /\D/;
            if (nonNumbers.test(CNPJ)) erro += "A verifica��o de CNPJ suporta apenas n�meros! \n\n";
            var a = [];
            var b = new Number;
            var c = [6,5,4,3,2,9,8,7,6,5,4,3,2];
            for (i=0; i<12; i++){
                a[i] = CNPJ.charAt(i);
                b += a[i] * c[i+1];
            }
            if ((x = b % 11) < 2) { a[12] = 0 } else { a[12] = 11-x }
            b = 0;
            for (y=0; y<13; y++) {
                b += (a[y] * c[y]);
            }
            if ((x = b % 11) < 2) { a[13] = 0; } else { a[13] = 11-x; }
            if ((CNPJ.charAt(12) != a[12]) || (CNPJ.charAt(13) != a[13])){
                erro +="CNPJ INV�LIDO, Por favor verifique!";
            }
            if (erro.length > 0){
                alert(erro);
                return false;
            }
            return true;
        }
        //------------------------------------------------------------------------------
        //---------------------------Fun��o para validar CPF----------------------------
        //------------------------------------------------------------------------------
        function validaCPF() {
            cpf = $('input[name="cpf_cnpj"]').val();

            cpf = cpf.replace(".", "");
            cpf = cpf.replace(".", "");
            cpf = cpf.replace("-", "");

            erro = new String;
            if (cpf.length < 11) erro += "Sao necessarios 11 digitos para verificacao do CPF! \n\n";
            var nonNumbers = /\D/;
            if (nonNumbers.test(cpf)) erro += "A verificacao de CPF suporta apenas numeros! \n\n";
            if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999"){
                erro += "Numero de CPF invalido!"
            }
            var a = [];
            var b = new Number;
            var c = 11;
            for (i=0; i<11; i++){
                a[i] = cpf.charAt(i);
                if (i < 9) b += (a[i] * --c);
            }
            if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
            b = 0;
            c = 11;
            for (y=0; y<10; y++) b += (a[y] * c--);
            if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
            if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10])){
                erro +="Numero de CPF invalido!, Por Favor confira";
            }
            if (erro.length > 0){
                alert(erro);
                return false;
            }
            return true;
        }
        //------------------------------------------------------------------------------
        //--------------------------- Fim das Valida��es -------------------------------
        //------------------------------------------------------------------------------

        //SE O RADIO PESSOA JUR�DICA VIER MARCADO
        if($('input[name="pessoa"][value="j"]').is(':checked'))
        {
            //MUDA O LABEL DO PARA NOME FANTASIA
            $('label[for="nome"]').text('Nome Fantasia:');
            //DESMARCAR O SEXO
            $("input[name='sexo']").each(function() {
                //desmarcar
                this.checked = false;
            });
            //ATRIBUI A LETRA J A VARIAVEL PESSOA QUE SERA USADA NA VERIFICA��O DO CPF/CNPJ
            pessoa='j';
            //DESMARCAR ESTADO CIVIL AINDA N�O FUNCIONA
            $('#est_civil option[value="0"]').attr('selected', true);
            //RESETAR CPF/CNPJ
            $('#cpf_cnpj').html('<input style="width: 124px;" type="text" name="cpf_cnpj"/>');
            //HABILITAR CAMPO E APLICAR M�SCARA PARA CPF
            $('input[name="cpf_cnpj"]').attr('disabled',false);
            $('input[name="cpf_cnpj"]').mask('99.999.999/9999-99');
            $('input[name="cpf_cnpj"]').attr('value', '<?php echo $inadDados->ina_pessoa == "j" ? "$inadDados->ina_cpf_cnpj" : ""; ?>');
        }
        //SE O RADIO PESSOA F�SICA VIER MARCADO
        if($('input[name="pessoa"][value="f"]').is(':checked'))
        {
            //ATRIBUI A LETRA F A VARIAVEL PESSOA QUE SERA USADA NA VERIFICA��O DO CPF/CNPJ
            pessoa='f';
            //ALTERA O LABEL DE NOME FANTASIA PARA NOME
            $('label[for="nome"]').text('Nome:');
            $('#sexo_est_civil').css('padding-top','0px');
            $('input[name="cpf_cnpj"]').mask('999.999.999-99');
        }
        //AO CLICAR EM PESSOA JURIDICA
        $('input[name="pessoa"][value="j"]').click(function() {
            //ATRIBUI A LETRA J A VARIAVEL PESSOA QUE SERA USADA NA VERIFICA��O DO CPF/CNPJ
            pessoa='j';
            $('label[for="nome"]').text('Nome Fantasia:');
            //DESMARCAR O SEXO
            $("input[name='sexo']").each(function() {
                //desmarcar
                this.checked = false;
            });
            //DESMARCAR ESTADO CIVIL AINDA N�O FUNCIONA
            $('#est_civil option[value="0"]').attr('selected', true);
            //RESETAR CPF/CNPJ
            $('#cpf_cnpj').html('<input style="width: 124px;" type="text" name="cpf_cnpj"/>');
            //HABILITAR CAMPO E APLICAR M�SCARA PARA CPF
            $('input[name="cpf_cnpj"]').attr('disabled',false).mask('99.999.999/9999-99');
            $('input[name="cpf_cnpj"]').attr('value', '<?php echo $inadDados->ina_pessoa == "j" ? "$inadDados->ina_cpf_cnpj" : ""; ?>');
        });
        //AO CLICAR EM PESSOA F�SICA
        $('input[name="pessoa"][value="f"]').click(function() {
            //ATRIBUI A LETRA F A VARIAVEL PESSOA QUE SERA USADA NA VERIFICA��O DO CPF/CNPJ
            pessoa='f';
            //SETA NULO NO INPUT RAZ�O SOCIAL
            $('input[name="razao_social"]').val('<?php echo $inadDados->ina_pessoa == "j" ? "$inadDados->ina_razao_social" : ""; ?>');
            //ALTERA O LABEL DE NOME FANTASIA PARA NOME
            $('label[for="nome"]').text('Nome:');
            //RESETAR CPF/CNPJ
            $('#cpf_cnpj').html('<input style="width: 124px;" type="text" name="cpf_cnpj" />');
            //APLICAR M�SCARA PARA CNPJ
            $('input[name="cpf_cnpj"]').attr('disabled',false).mask('999.999.999-99');
            $('input[name="cpf_cnpj"]').attr('value', '<?php echo $inadDados->ina_pessoa == "f" ? "$inadDados->ina_cpf_cnpj" : ""; ?>');
        });
        //VALIDA��O DOS CAMPOS OBRIGAT�RIOS DO FURMULARIO DE INADIMPLENTE
        $('#formUP').submit(function(){
            //SE O USU�RIO DIGITOU CPF OU CNPJ A VALIDA��O DEVE SER FEITA
            if(($('input[name="cpf_cnpj"]').val())!='')
            {
                //SE A VARIAVEL PESSOA FOR F(F�SICA) ENT�O VALIDA O CPF SEN�O VALIDA O CNPJ
                if(pessoa=='f'){
                    //ATRIBUI O RETURN DA FUN��O SE FOR FALSO N�O DEIXA ENVIAR O FORM
                    ret = validaCPF();
                    if(ret==false){
                        return false;
                    }
                }
                else if(pessoa=='j')
                {
                    ret = validaCNPJ();
                    if(ret==false){
                        return false;
                    }
                }
            }
            //TESTA PARA VER SE OS CAMPOS OBRIGAT�RIOS EST�O PREENCHIDOS
            var pessoaVazio = true;
            $('input[name="pessoa"]').each(function() {
                if ( $(this).is(':checked') ) {
                    pessoaVazio = false;
                }
            });
            if($('input[name="nome_fantasia"]').val() == '')
            {
                $('input[name="nome_fantasia"]').focus();
                return false;
            }
            else if(pessoaVazio)
            {
                alert('Indique pessoa F�SICA ou JUR�DICA.');
                return false;
            }
            else if($('#cidade option:selected').val() == '' || $('#uf option:selected').val() == '')
            {
                alert('Selecione uma CIDADE e um ESTADO');
                return false;
            }
            else
            {
                var marcado = false;
            }
        });
        //VALDA��O DOS CAMPOS OBRIGATORIOS DO FORMULARIO DE PARENTES
        $('#formPAR').submit(function(){
            if($('input[name="parentesco_01_nome"]').val() == '')
            {
                $('input[name="parentesco_01_nome"]').focus();
                return false;
            }
            $('#par_nome').attr('disabled',false);
            $('#par_fone').attr('disabled',false);
        });

        //VALDA��O DOS CAMPOS OBRIGATORIOS DO FORMULARIO DE BENS
        $('#formBEM').submit(function(){
            if($('input[name="titulo"]').val() == '')
            {
                $('input[name="titulo"]').focus();
                return false;
            }
        });

        //LISTANDO AS CIDADES
        $('select[id="ina_uf"]').change(function(){
            popularCidade('ina_');
        });
        $('select[id="con_uf"]').change(function(){
            //            alert('Entrou no: PAI');
            popularCidade('con_');
        });
        $('select[id="pai_uf"]').change(function(){
            popularCidade('pai_');
        });
        $('select[id="mae_uf"]').change(function(){
            popularCidade('mae_');
        });
        //------------------------------------------------------------------------------
        //---------------------------- Op��es da d�vida --------------------------------
        //------------------------------------------------------------------------------
        $('.formDividas').submit(function(e){
            e.preventDefault();
            aux = $('select[name="opcoesDivida"]').val();
            if(aux=='0'){
                alert('Voc� precisa escolher uma op��o!');
                return false;
            }else if(aux=='novoRO'){
                cods = $(this).attr('id');
                alert('Novo RO: '+cods);
                window.location.href=base_url+'ro/novo/cod:'+cods;
                return false;
            }else if(aux=='acordo'){
                alert('Acordo');
            }else if(aux=='judicial'){
                $("#dialog-form2").dialog("open");
                return false;
            }else{
                return false;
            }
            return false;
        });

        $('.formAcordos').submit(function(e){
            e.preventDefault();
//            aux = $('select[name="opcoesAcordos"]').val();
            aux = $(this).find(':eq(2)').val();

            if(aux=='0'){
                alert('Voc� precisa escolher uma op��o!');
                return false;
            }else if(aux=='novoRO'){

                alert('Novo RO');
            }else if(aux=='acordo'){
                alert('Acordo');
            }else if(aux=='judicial'){
                alert('Judicial');
            }else if(aux=='extincao'){
                $("#dialog-form3").dialog("open");
                return false;
            }else if(aux=='receber'){
                //ZERANDO OS VALORES DO FORM
                $('#dados_ch').val('');
                $('.tipoValor').each(function(){
                    $(this).removeAttr('checked');
                });
                $('#valorRecebido').val('');
                $('#desconto').val('');
                $('#btsReceber').hide();
                $('#camposReceber').hide();

                //CAPTURANDO OS DADOS DAS PARCELAS
                par_cod = '';

                $("#dialog-form").dialog("open");
                return false;
            }else{
                alert("HADOUKEN!!!>>>> "+aux);
                return false;
            }


            return false;
        });

        $('.formJudicial').submit(function(e){
            e.preventDefault();
            aux = $('select[name="opcoesJudicial"]').val();
            if(aux=='0'){
                alert('Voc� precisa escolher uma op��o!');
                return false;
            }else if(aux=='novoRO'){

                alert('Novo RO');
            }else if(aux=='acordo'){
                alert('Acordo');
            }else if(aux=='judicial'){
                alert('Judicial');
            }else{
                alert("HADOUKEN!!!>>>> "+aux);
                return false;
            }
            alert("HADOUKEN!!!>>>> "+aux);

            return false;
        });

        //--------------------------- VALIDA��ES -------------------------------

        $('#formExtAcordo').submit(function(e){
            if($('input[name="devdoc_mot"]').val() == ''){
                alert('Aten��o, O motivo deve ser preenchido!');
                $('input[name="devdoc_mot"]').focus();
                return false;
            }else if($('input[name="renegociacao_mot"]').val() == ''){
                alert('Aten��o, O motivo deve ser preenchido!');
                $('input[name="renegociacao_mot"]').focus();
                return false;
            }else if($('input[name="reabilitacao_mot"]').val() == ''){
                alert('Aten��o, O motivo deve ser preenchido!');
                $('input[name="reabilitacao_mot"]').focus();
                return false;
            }else if($('input[name="ext_jud_mot"]').val() == ''){
                alert('Aten��o, O motivo deve ser preenchido!');
                $('input[name="ext_jud_mot"]').focus();
                return false;
            }
        });

        //----------------------------------------------------------------------
        //----------------------------------------------------------------------
        function popularCidade(entidade)
        {

            //            alert('Entrou na Fun��o: '+entidade);
            //$('select[id="uf"]').change(function(){

            v1 = '';
            //captura os options selecionados
            v1 = $('select[id="'+entidade+'uf"] option:selected').text();
            if(v1 == '') v1='UF';
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() . 'inadimplente/listar_cidades'; ?>",
                data: 'v1='+v1,
                dataType: 'json',
                error: function(xhr, status, er) {
                    alert('C�DIGO DE ERRO: '+xhr.status+' - '+xhr.statusText+' | TIPO DO ERRO: '+er);
                },
                success: function(retorno)
                {
                    $('#'+entidade+'cidade').html('<option value="">SELECIONE UMA CIDADE</option>');//zera os valores antigos e escreve no elemento o valor selecione
                    $qtd = '';
                    $valor = '';
                    $.each(retorno.cidadesbrasil, function(i, v){//para cada �ndice retornado executa a fun��o
                        if(v.cidadeatual != ''){
                            $('#'+entidade+'cidade-button').fadeOut('normal',function(){
                                //ESCONDE O OPTION INICIAL SEM VALOR
                            });
                            $('#'+entidade+'cidade').append('<option value="'+unescape(v.cidadeatual)+'">'+unescape(v.cidadeatual)+'</option>');//unescape descriptografa o padr�o URL de dados retornado no PHP
                        }
                        $qtd = i;
                        $valor = v.cidadeatual;
                    });
                    if ($qtd != 0 && $valor != ''){
                        $('#'+entidade+'cidade').fadeIn('normal', function(){
                            //CIDADES CARREGADAS COM SUCESSO
                        });
                    }
                    else
                    {
                        alert('Voc� deve selecionar um ESTADO v�lido.');
                        $('#'+entidade+'cidade').html('<option value=""><-- SELECIONE UM ESTADO</option>');//zera os valores antigos e escreve no elemento o valor selecione
                    }
                }
            });
        }
        base_url = "<?php echo base_url(); ?>";
        $('#camposReceber').hide();
        $('#btsReceber').hide();
        $('.tipoValor').click(function(){
            $('#camposReceber').fadeIn();
        });
        $('#atualizar').css('cursor','pointer').click(function(){
            $('#btsReceber').fadeIn();
        });

    });
</script>